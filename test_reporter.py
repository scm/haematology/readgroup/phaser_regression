from phaser_regression import test_result
import re

def create(reporters, input, result, prefix, disable_sections, disable_tests_in):

    report = {}

    for r in reporters:
        if r.__name__ in disable_sections:
            continue

        omitteds = disable_tests_in.get( r.__name__, {} )
        section = r( input = input, result = result, prefix = prefix )

        report[ r.__name__ ] = dict(
          ( k, v ) for ( k, v ) in section.items() if k not in omitteds
          )

    return report


def printout(report, stream):

    for ( header, section ) in report.items():
        stream.write( "Section: %s\n" % header )

        for ( name, value ) in section.items():
            stream.write( "    %s: %s\n" % ( name, value ) )

        stream.write( "\n" )


def symmetry_information(input, result, prefix):

    ( a, b, c, alpha, beta, gamma ) = result.getUnitCell()

    return {
#       "SPACE_GROUP": test_result.SpaceGroup.from_number( number = result.getSpaceGroupNum() ),
        "UNIT_CELL": test_result.List( values = [
            test_result.CellAxis( a ),
            test_result.CellAxis( b ),
            test_result.CellAxis( c ),
            test_result.CellAngle( alpha ),
            test_result.CellAngle( beta ),
            test_result.CellAngle( gamma ),
            ] ),
        }


def mr_dataset_statistics(input, result, prefix):

    return {
        "MILLER_SIZE": test_result.Integer( value = len( result.getMiller() ) ),
        "F_SIZE": test_result.Integer( value = len( result.getF() ) ),
        "SIGF_SIZE": test_result.Integer( value = len( result.getSIGF() ) ),
        }


def anisotropy_correction(input, result, prefix):

    return {
        "PRINCIPAL_COMPONENTS": test_result.List( values = [
            test_result.AnisotropyPrincipalComponent( value = v ) for v in result.getEigenBs()
            ] ),
        "WILSON_B": test_result.WilsonB( result.getWilsonB() ),
        "WILSON_K": test_result.WilsonK( result.getWilsonK() ),
        }


def rotation_function(input, result, prefix):

    ensemble_named = dict( [ ( i.name, i ) for i in input.ensembles() ] )
    space_group_number = 1
    cell = result.getUnitCell()
    return {
        "SOLUTION_COUNT": test_result.Integer( len( result.getRFZ() ) ),
        "SOLUTION_ANNOTATION":
            test_result.Text( result.getDotRlist()[0].ANNOTATION ),
        "SOLUTIONS": test_result.List(
            values = [
                test_result.RotationPeak(
                    ensemble = ensemble_named[ sol.MODLID ],
                    space_group_hall = result.getHall(),
                    cell = cell,
                    angles = sol.EULER
                    )
                for sol in result.getDotRlist()[0].RLIST
                ]
            ),
        "SOLUTION_Z_SCORES": test_result.List(
            values = [ test_result.ZScore( value = v ) for v in result.getRFZ() ]
            ),
        "SOLUTION_SCORES": test_result.List(
            values = [ test_result.RFScore( value = v ) for v in result.getRF() ]
            ),
        }


def solutions(input, result, prefix):

    ensemble_named = dict( [ ( i.name, i ) for i in input.ensembles() ] )
    space_group_number = 1
    cell = result.getUnitCell()
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    import re
    ensname = re.compile( "([^\[]*)" )
    return {
        "SOLUTION_COUNT": test_result.Integer( len( result.getDotSol() ) ),
        "SOLUTION_ANNOTATIONS": test_result.List(
            values = [ test_result.Text( sol.ANNOTATION )
                for sol in result.getDotSol() ]
            ),
        "SOLUTIONS": test_result.List(
            values = [
                test_result.Solution(
                    space_group_hall = sol.HALL,
                    cell = cell,
                    peaks = [
                        ( ensemble_named[ ensname.search( nd.MODLID ).group(1) ], nd.getEuler(), nd.TRA )
                        for nd in sol.KNOWN
                        ]
                    )
                for sol in result.getDotSol()
                ]
            ),
        "BFACTOR": test_result.List(
            values = [
                test_result.List(
                    values = [ test_result.Bfactor( value = nd.BFAC ) for nd in sol.KNOWN ]
                    )
                for sol in result.getDotSol()
                ]
            ),
        }

def vrms_refinement(input, result, prefix):

    vrms_ensemble_list = []
    vrms_rms_list = []

    import re
    bracketed = re.compile( "(.*)\[\d+\]$" )

    for sol in result.getDotSol():
        squarebracketkeys = set()

        for key in sol.NEWVRMS.keys():
            match = bracketed.search( key )

            if match:
                squarebracketkeys.add( match.group( 1 ) )

        vrms_ensemble_list.append(
            test_result.List(
                values = [
                    test_result.Text( value = key ) for key in sol.NEWVRMS.keys()
                    if key not in squarebracketkeys
                    ]
                )
            )
        vrms_rms_list.append(
            test_result.List(
                values = [
                    test_result.List(
                        values = [ test_result.VRMS( value = value ) for value in vector ]
                        )
                    for ( key, vector ) in sol.NEWVRMS.items()
                    if key not in squarebracketkeys
                    ]
                )
            )

    return {
        "VRMS_ENSEMBLE": test_result.List( values = vrms_ensemble_list ),
        "VRMS_RMS": test_result.List( values = vrms_rms_list ),
        }


def pruning_fraction(input, result, prefix):

    import iotbx.pdb, os.path
    fname = "%s.1.pdb" % prefix
    report = {
        "PRUNE_FILE": test_result.Text( value = os.path.basename( fname ) ),
        }

    try:
        inp = iotbx.pdb.input( fname )
        report [ "PRUNE_FRACTION" ] = test_result.PruneFraction(
            value = float( len( [a for a in inp.atoms() if 0 < a.occ] ) ) / len( inp.atoms() )
            )

    except Exception:
        pass

    return report

#return PRUNING_SCORE must be unique for each def
def pruning_scores(input, result, prefix):

    return {
        "PRUNING_SCORE": test_result.List(
            values = [ test_result.LLGScore( value = v ) for v in result.getValues() ]
            ),
        }


def translation_function(input, result, prefix):

    return {
        "SOLUTION_Z_SCORES": test_result.List(
            values = [ test_result.ZScore( value = v ) for v in result.getTFZ() ]
            ),
        "SOLUTION_SCORES": test_result.List(
            values = [ test_result.TFScore( value = v ) for v in result.getTF() ]
            ),
        }


def gyre_rottra(input, result, prefix):

    s = re.compile(""".+ DROT \s+ ( -?\d+\.\d* ) .+ ( -?\d+\.\d* ) .+ ( -?\d+\.\d* )
                     \s+ DTRA \s+ ( -?\d+\.\d* ) .+ ( -?\d+\.\d* ) .+ ( -?\d+\.\d* ) \s*
                   """, re.MULTILINE | re.VERBOSE )
    dct = {}
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    if len( s.findall( result.getDotSol().unparse() ) ) > 0:
        gyreres = s.findall( result.getDotSol().unparse() )
    else:
# Because no SOLU_GYRE regex match is provided in phaser/test_phaser_parsers.SolutionFileParser
# we parse gyre solution file here directly
        if len( s.findall( result.getDotRlist().unparse() ) ) > 0:
           gyreres = s.findall( result.getDotRlist().unparse() )
        else:
           gyreres = s.findall( result.rawsolstr ) # if running from keyword script where we have no SolutionFileParser

    rottra = [ [ test_result.GyreAngle( float( drot_dtra[0] ) ),
                  test_result.GyreAngle( float( drot_dtra[1] ) ),
                  test_result.GyreAngle( float( drot_dtra[2] ) ),
                  test_result.GyreTrans( float( drot_dtra[3] ) ),
                  test_result.GyreTrans( float( drot_dtra[4] ) ),
                  test_result.GyreTrans( float( drot_dtra[5] ) ),
                ] for drot_dtra in gyreres ]
    for i, e in enumerate( rottra ):
        dct["GYRATION_%d" %i] = test_result.List( values = e )

    return dct



def gyre_scores(input, result, prefix):
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    return {
        "GYRE_LLG_SCORES": test_result.List(
            values = [ test_result.LLGScore( value = v ) for v in result.getValues() ]
            ),
        }


def auto_pak_rnp_function(input, result, prefix):
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    return {
        "SOLUTION_SCORES": test_result.List(
            values = [ test_result.LLGScore( value = v ) for v in result.getValues() ]
            ),
        }



def sceds_score(input, result, prefix):
    """
   Try matching SCEDS components in table in log file such as:

   Best Domain Division
   --------------------
   SCEDS = ws*Sphericity + wc*Continuity + we*Equality + wd*Density
         = 4*0.95 + 0*0.50 + 1*0.76 + 1*0.79
         = 3.81 + 0.00 + 0.76 + 0.79
         = 5.36

    """
    s = re.compile(""" ( ^ \s+ Best \s Domain \s Division \n
        \s+ -+ \n .*? )
         ( <!--SUMMARY_BEGIN-->   # present in log file but not in python log string
         |
        -{12} .*
        OUTPUT \s FILES .*
        -{12} )
          """, re.MULTILINE |  re.DOTALL | re.VERBOSE )

    scedstbl = s.search( result.logfile() ).group(1)

    return {
        "SCEDS_TABLE":  test_result.Text( value = scedstbl )
        }


def ep_dataset_statistics(input, result, prefix):

    return {
        "MILLER_SIZE": test_result.Integer( value = len( result.getMiller() ) ),
        }


def ep_phasing_statistics(input, result, prefix):

    import os.path
    ( a, b, c, alpha, beta, gamma ) = result.getUnitCell()
    input.data.load()
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    return {
        "ATOMS_COUNT": test_result.Integer( value = len( result.getTopAtoms() ) ),
        "ATOMS": test_result.AtomSet(
            atoms = [
                test_result.Atom(
                    element = s.scattering_type,
                    coordinates = s.site,
                    occupancy = s.occupancy,
                    uiso = s.u_iso,
                    )
                for s in result.getTopAtoms()
                ],
            cell = input.data.unit_cell,
            space_group = input.data.space_group,
            ),
        "LLG": test_result.SAD_LLG( value = result.getLogLikelihood() ),
        }

def ep_phasing_file(input, result, prefix):

    import os.path
    ( a, b, c, alpha, beta, gamma ) = result.getUnitCell()
    input.data.load()
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    return {
        "OUTPUT_MTZ_FILE": test_result.Text(
            value = os.path.basename( result.getTopMtzFile() )
            ),
        "OUTPUT_PDB_FILE": test_result.Text(
            value = os.path.basename( result.getTopPdbFile() )
            ),
        "UNIT_CELL": test_result.List( values = [
            test_result.CellAxis( a ),
            test_result.CellAxis( b ),
            test_result.CellAxis( c ),
            test_result.CellAngle( alpha ),
            test_result.CellAngle( beta ),
            test_result.CellAngle( gamma ),
            ] ),
        }

def sad_specific_statistics(input, result, prefix):

    import os.path
    elem = set( result.getTopAtoms().extract_scattering_types() )

    return {
        "F_ELEM": test_result.List(
            values = [ test_result.Text( value = v ) for v in elem ]
            ),
        "F_PRIME": test_result.List(
            values = [
                test_result.ScatteringFactor( value = result.getFp( e, False, 0 ) )
                for e in elem
                ]
            ),
        "F_DOUBLEPRIME": test_result.List(
            values = [
                test_result.ScatteringFactor( value = result.getFdp( e, False, 0 ) )
                for e in elem
                ]
            ),
        #"DIRECT_HAND_SPACE_GROUP": test_result.SpaceGroup.from_symbol(
        #    value = result.getHand().getSpaceGroupName()
        #    ),
        #"INVERTED_HAND_SPACE_GROUP": test_result.SpaceGroup.from_symbol(
        #    value = result.getHand( True ).getSpaceGroupName()
        #    ),
        }


class SectionComparison(object):
    """
    Comparison result of two reports (created by the same reporter)
    """

    def __init__(self, left, leftrefstr, right, rightrefstr, method, disabled_tests = set()):

        self.disabled_tests = disabled_tests

        lefts = set( left.keys() )
        rights = set( right.keys() )
        commons = lefts & rights
        self.left_only = dict(
            [ ( k, left[ k ] ) for k in lefts.difference( commons, self.disabled_tests ) ]
            )
        self.right_only = dict(
            [ ( k, right[ k ] ) for k in rights.difference( commons, self.disabled_tests ) ]
            )

        self.evaluation_for = {}

        for c in commons:
            if c in self.disabled_tests:
                continue

            self.evaluation_for[ c ] = test_result.Comparison(
                left = left[ c ],
                leftrefstr = leftrefstr,
                right = right[ c ],
                rightrefstr = rightrefstr,
                method = method
                )


    def tests(self):

        return ( len( self.evaluation_for.keys() ) + len( self.left_only )
            + len( self.right_only ) )


    def failures(self):

        return len(
            [ h for ( h, r ) in self.evaluation_for.items() if not r.equals ]
            )


    def unknowns(self):

        return len( self.left_only ) + len( self.right_only )


    def disableds(self):

        return len( self.disabled_tests )


    def get_text_summary(self):

        return "Tests: %d, failures: %d, unknowns: %d, disableds: %d\n" % (
            self.tests(),
            self.failures(),
            self.unknowns(),
            self.disableds(),
            )


    def __str__(self):
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        results = []

        for ( heading, result ) in self.evaluation_for.items():
            results.append( "Result: %s" % heading )
            results.append(
                "%s%s" % ( str( result ), " EQUAL\n" if result.equals else " DIFFER\n" )
                )

        for ( array, side ) in [ ( self.left_only, "left" ), ( self.right_only, "right" ) ]:
            if array:
                results.append( "Results present in %s set only" % side )
                results.append(
                    "\n".join(
                        "%d. %s" % ( c, n ) for ( c, n ) in enumerate( array, start = 1 )
                        )
                    )

        if self.disabled_tests:
            results.append( "Disabled: %s" % ", ".join( self.disabled_tests ) )

        return "\n".join( results )


class Comparison(object):
    """
    Comparison of two reports
    """

    def __init__(
      self,
      left,
      leftversion,
      right,
      rightversion,
      method = "weak_comparison",
      disabled_sections = [],
      disabled_tests_in = {},
      ):

        self.disabled_sections = disabled_sections
        if left.get('Version'): # no version comparison
            del left['Version']
        if right.get('Version'):
            del right['Version']

        lefts = set( left.keys() )
        rights = set( right.keys() )
        self.leftversion = leftversion
        self.rightversion = rightversion

        commons = lefts & rights
        self.left_only = [
            ( k, left[ k ] ) for k in lefts.difference( commons, self.disabled_sections )
            ]
        self.right_only = [
            ( k, right[k] ) for k in rights.difference( commons, self.disabled_sections )
            ]

        self.evaluation_for = {}
        self.leftrefstr = "{*}: "
        self.rightrefstr = "{#}: "
        self.versionstrings = ""
        self.versionstrings = "-" * 80 + "\n" + self.leftrefstr + self.leftversion + \
            "\n" + self.rightrefstr + self.rightversion + "\n" + "-" * 80
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        for c in commons:
            if c in self.disabled_sections:
                continue

            self.evaluation_for[ c ] = SectionComparison(
                left = left[ c ],
                leftrefstr = self.leftrefstr,
                right = right[ c ],
                rightrefstr = self.rightrefstr,
                method = method,
                disabled_tests = disabled_tests_in.get( c, [] ),
                )

        self.failed_tests_str = ""
        for v in self.evaluation_for.values():
            for k, i in v.evaluation_for.items():
                if not i.equals:
                    self.failed_tests_str += str(i) + "\n\n"

    def tests(self):

        return sum( [ sc.tests() for sc in self.evaluation_for.values() ] ) + self.unknowns()


    def failures(self):

        return sum( [ sc.failures() for sc in self.evaluation_for.values() ] )


    def unknowns(self):

        return (
            sum( [ len( s[1] ) for s in self.left_only + self.right_only ] )
            + sum( [ sc.unknowns() for sc in self.evaluation_for.values() ] )
            )


    def disableds(self):

        return sum( [ sc.disableds() for sc in self.evaluation_for.values() ] )


    def passed(self):

        return ( self.failures() + self.unknowns() ) == 0


    def get_text_summary(self):

        return self.versionstrings + \
            "\nSUMMARY:\nTests: %d, failures: %d, unknowns: %d, disabled: %d sections, %d tests" % (
            self.tests(),
            self.failures(),
            self.unknowns(),
            len( self.disabled_sections ),
            self.disableds(),
            )


    def get_failed_summary(self):

        results = [self.failed_tests_str]
        if self.failed_tests_str:
            results.append( format_secondary_heading( text = self.leftrefstr + self.leftversion ) )
            results.append( format_secondary_heading( text = self.rightrefstr + self.rightversion ) )

        return "\n".join( results )


    def __str__(self):

        results = []

        if self.evaluation_for:
            results.append( format_primary_heading( text = "Comparisons" ) )

            for ( heading, report ) in self.evaluation_for.items():
                results.append( format_secondary_heading( text = heading ) )
                results.append( str( report ) )

            results.append( "" )

        for ( array, side ) in [ ( self.left_only, "left" ), ( self.right_only, "right" ) ]:
            if array:
                results.append(
                    format_primary_heading( text = "Reports present in %s set only" % side )
                    )
                results.append(
                    "\n".join(
                        "%d. %s" % ( c, k ) for ( c, ( k, r ) ) in enumerate( array, start = 1 )
                        )
                    )
                results.append( "" )

        if self.disabled_sections:
            results.append( format_primary_heading( text = "Disabled reports" ) )
            results.append( "\n".join( self.disabled_sections ) )
            results.append( "" )

        #results.append( self.versionstrings)
        results.append ( "== End-of-comparisons ==" )

        return "\n".join( results )


def format_primary_heading(text):

    separator = "=" * ( len( text ) + 1 )
    return "%s\n%s:\n%s" % ( separator, text, separator )


def format_secondary_heading(text):

    return "\n%s:\n%s" % ( text, "-" * ( len( text ) + 1 ) )
