
# Script for building phaser and running regression test
# Assumes access to account www-structmed@arctic-1 where test results are copied to web page
# If running as a cron job make sure path to compiler and other environment variables are set
# Mac bash users may have to source /etc/profile as to ensure correct environment for the compiler.
# crontab example for running at 1am every day:
#  0 1 * * *  /bin/bash -c "source /etc/profile && python /Users/phaserbuilder/PhaserNightlyBuild/nightlybuildscript.py" 1> /Users/phaserbuilder/PhaserNightlyBuild/crontab.log 2>&1

# Get this file on the PC that will run the regression tests with the command:
# curl https://git.uis.cam.ac.uk/x/cimr-phaser/phaser_regression.git/blob_plain/HEAD:/nightlybuildscript.py > nightlybuildscript.py
# then run it with the command
# python nightlybuildscript.py

# If running as a cron job make sure path to compiler and other environment variables are set
# Mac bash users may have to source /etc/profile as to ensure correct environment for the compiler.
#
# Crontab example for running at 1am every day:
#  0 1 * * *  /bin/bash -c "source /etc/profile && python /Users/phaserbuilder/PhaserNightlyBuild/nightlybuildscript.py" 1> /Users/phaserbuilder/PhaserNightlyBuild/crontab.log 2>&1
#
# Windows Task Scheduler action example:
# program: C:\Windows\System32\cmd.exe
# arguments: /c "C:\Microsoft\Visual C++ for Python\9.0\vcvarsall.bat" amd64 && python C:\Users\phaserbuilder\PhaserNightlyBuild\nightlybuildscript.py 2>&1 C:\Users\phaserbuilder\task.log
from __future__ import print_function

import os, sys, re, stat
import os.path
import subprocess
import platform, datetime, shutil, time
import multiprocessing

def mysubproc(command, tfile=None):
  print(command)
  if tfile:
    tfile.write(command + '\n')
    merr = subprocess.STDOUT
  else:
    merr = subprocess.PIPE
  proc = subprocess.Popen(
    command,
    shell=True,
    universal_newlines=True,
    stdout = subprocess.PIPE,
    stderr = merr
  )
  outs, errs = proc.communicate()
  if tfile:
    tfile.write(str(outs) + "\n" + str(errs) + "\n")
    tfile.flush()
  else:
    print( str(outs) + "\n" + str(errs) + "\n") # + "\n" + errs
  return outs, errs


def change_permissions_recursive(path, mode):
  for dir, dirs, files in os.walk(path, topdown=False):
    os.chmod(dir, mode)
    for file in files:
      os.chmod(os.path.join(dir, file), mode)

webaccount = "www-structmed@arctic-7"

def UpdateContentsOnWebsite():
  print( mysubproc('ssh ' + webaccount +
   ' "cd /var/www/html/htdocs/Local/PhaserNightly && python MakeRegressionTestContents.py"') )

def CheckIfUp2Date(localdir, remoteURL):
  curdir = os.getcwd()
  if not os.path.exists(localdir):
    return 43
  os.chdir(localdir)
  localtnghashcmd ='git log -n1 --format=format:"%H"'
  remotetnghashcmd = 'git ls-remote %s HEAD' %remoteURL
  proc = subprocess.Popen(localtnghashcmd, shell=True, universal_newlines=True, 
                          stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
  # emits a string like "d6adeed5051ed56548bda2f5aaef01f39707d1cf"
  localtnghash, errs = proc.communicate()
  localhashstr = str(localtnghash)
  proc = subprocess.Popen(remotetnghashcmd, shell=True, universal_newlines=True,
                          stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
  # emits a string like "d6adeed5051ed56548bda2f5aaef01f39707d1cf        HEAD"
  remotetnghash, errs = proc.communicate()
  remotehashstr = str(remotetnghash)
  # check for successful remote access
  if "HEAD" not in remotehashstr:
    print(remotehashstr) # should contain reason why remote access didn't work
    return 0 # return 0 to avoid sending emails
  os.chdir(curdir)
  print(localhashstr + ", " + remotehashstr)
  if localhashstr in remotehashstr:
    print("%s is up to date" %localdir)
    return 0
  print("%s is out of date" %localdir)
  return 42

if "cleanbuild" not in sys.argv:
  if CheckIfUp2Date(sys.argv[2], sys.argv[3] ) == 0:
    #time.sleep(12) # for debugging
    sys.exit()

pyexe = '"' + sys.executable + '"' 
nproc = multiprocessing.cpu_count() -1 # allow a spare CPU to idle
builddir = os.getcwd()

outs, errs = mysubproc("g++ -dumpversion")
version = outs.strip()
GCCversion = ""
longGCCversion = ""
if version:
  outs, errs  = mysubproc("g++ -dumpmachine")
  GCCversion = "gcc" + version + "_" + outs.strip()
  outs, errs = mysubproc("g++ --version")
  longGCCversion = outs.strip()

outs, errs  = mysubproc("cl.exe")
mstr = errs.strip() # cl.exe prints version info to stderr rather than stdout
version = re.findall("Version ([0-9\.]+)", mstr)
MSVCversion = ""
longMSVCversion = ""
if version:
  MSVCversion = "msvc" + version[0]
  longMSVCversion = mstr

compiler = MSVCversion
if not MSVCversion:
  compiler = GCCversion

# make string of ostype and platform for filenames
ostype = platform.platform() + "_" + compiler

if not os.path.exists("Current"):
  os.mkdir("Current")

logdir = os.path.join(builddir, "Logfiles")
regdir = os.path.join(builddir, "Current", "tests", "phaser_regression")
testsdir = os.path.join(builddir, "Current", "tests")
if "python3" in sys.argv:
  pyversion = "38"
else:
  pyversion = "27"
websitedir = "/var/www/html/htdocs/Local/PhaserNightly/Current_" + ostype + "_Py" + pyversion

os.chdir("Current")

td = datetime.datetime.today().timetuple()
thistime = "%d_%d_%d_%s" %(td[2], td[1], td[0], ostype + "_Py" + pyversion )

if "cleanbuild" in sys.argv:
# clean up first
  if os.path.exists(logdir):
    shutil.rmtree(logdir)
  if os.path.exists(regdir):
    shutil.rmtree(regdir)
  if os.path.exists("conda_base"):
    shutil.rmtree("conda_base")
  if os.path.exists("base"):
    shutil.rmtree("base")
  if os.path.exists("base_tmp"):
    shutil.rmtree("base_tmp")
  if os.path.exists("build"):
    shutil.rmtree("build")

# delete current log files on website
#p =mysubproc('ssh ' + webaccount + ' rm %s/*"' %websitedir).wait()
# upload a new buildlog.txt stating that builds have now been started
mfile = open("buildlog.txt","w")
mfile.write("Bootstrap build started on %s\n" % time.asctime( time.localtime(time.time())) )
# print environment variables to log file
for k,v in os.environ.items():
  mfile.write( k + "=" + v + "\n")

mfile.write("Using compiler:\n")
mfile.write(longMSVCversion +"\n")
mfile.write(longGCCversion +"\n")

mfile.close()
mysubproc('ssh ' + webaccount + ' mkdir %s' %websitedir )
mysubproc('scp buildlog.txt ' + webaccount + ':%s/.' %websitedir )

mfile = open("buildlog.txt","a")
# Update contents.html on the website. First get MakeRegressionTestContents.py from phaser_regression repo
mysubproc('curl https://gitlab.developers.cam.ac.uk/scm/haematology/readgroup/phaser_regression/-/raw/master/MakeRegressionTestContents.py  > MakeRegressionTestContents.py', mfile)
mysubproc('scp MakeRegressionTestContents.py ' + webaccount + ':/var/www/html/htdocs/Local/PhaserNightly/.')

UpdateContentsOnWebsite()

# get bootstrap.py from cctbx github repo
mysubproc('curl https://raw.githubusercontent.com/cctbx/cctbx_project/master/libtbx/auto_build/bootstrap.py > bootstrap.py', mfile)

# clone repos, install conda
if os.path.exists("build"): # just update latest build
  mysubproc(pyexe + ' bootstrap.py --builder=voyager --use_conda --verbose --python=%s --nproc=%s update build ' %(pyversion, nproc), mfile)
else: # do a clean build
  mysubproc(pyexe + ' bootstrap.py --builder=voyager --use_conda --verbose --python=%s --nproc=%s' %(pyversion, nproc), mfile)
# upload complete buildlog file
mfile.close()
mysubproc('scp buildlog.txt ' + webaccount + ':%s/.' %websitedir )

# do a few regression tests here
if os.path.exists("tests"):
  shutil.rmtree("tests")
mfile = open("regressionlog.txt","w")
mysubproc(pyexe + ' bootstrap.py --builder=voyager --use_conda --verbose --python=%s --nproc=%s tests ' %(pyversion, nproc), mfile)
# upload complete log file from voyager tests only
mfile.close()
mysubproc('scp regressionlog.txt ' + webaccount + ':%s/.' %websitedir )
shutil.copy(os.path.join("tests", "voyager_regression","run_tests_parallel_zlog"), "voyager_tests.log" )
shutil.copy(os.path.join("tests", "phaser_regression","run_tests_parallel_zlog"), "phaser_tests.log" )

UpdateContentsOnWebsite()

curdir = os.getcwd()
os.chdir(os.path.join("modules","phasertng"))
gittagcmd = 'git describe --tags'
proc = subprocess.Popen(gittagcmd, shell=True, universal_newlines=True, 
                        stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
gittag, errs = proc.communicate()
gitlogcmd = 'git log -n1'
proc = subprocess.Popen(gitlogcmd, shell=True, universal_newlines=True, 
                        stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
gitlog, errs = proc.communicate()
os.chdir(curdir)

phaser_phasertnglog = gitlog

os.chdir(os.path.join("modules","phaser"))
gittagcmd = 'git describe --tags'
proc = subprocess.Popen(gittagcmd, shell=True, universal_newlines=True, 
                        stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
gittag, errs = proc.communicate()
gitlogcmd = 'git log -n1'
proc = subprocess.Popen(gitlogcmd, shell=True, universal_newlines=True, 
                        stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
gitlog, errs = proc.communicate()
os.chdir(curdir)
phaser_phasertnglog += "\n" + gitlog

def EmailMessage(fname, findwordstr, bootstraptype):
  logweblink = "https://www-structmed.cimr.cam.ac.uk/Local/PhaserNightly/Current_" + ostype + "_Py" + pyversion + "/%s" %fname
  found = -1
  recipients = "ajm201@cam.ac.uk,rjr27@cam.ac.uk,rdo20@cam.ac.uk"
  userlogin = "phaserbuilder"
  smtpserver = "smtp.gmail.com"
  fromemail = "phaserbuilder@gmail.com"
  verb = "failed"
  description = "Failure"

  with open(fname, "r") as f:
    logstr = f.read()
    found = logstr.find(findwordstr) # should really be done with regexp which is more robust
  if found > 1: # only email these on success
    verb = "succeeded"
    description = "Success"
    recipients = "rjr27@cam.ac.uk,rdo20@cam.ac.uk"
    #recipients = "rdo20@cam.ac.uk"

  with open("%s_ret.txt" %bootstraptype, "w") as f:
    f.write("""Phasertng bootstrap %s for windows %s. Inspect log file
  %s
  for details.

  %s
  """ %(bootstraptype, verb, logweblink, phaser_phasertnglog))
  emailerfname = os.path.join(os.path.dirname(os.path.abspath(__file__)), "SendEmailMessage.py")
  mysubproc('"%s" %s %s_ret.txt ' %(sys.executable, emailerfname, bootstraptype) + \
   '"Phasertng %s %s %s" %s %s %s %s' %(bootstraptype, description, gittag.strip(), userlogin, smtpserver, fromemail, recipients))
 
 
EmailMessage("buildlog.txt", "Bootstrap success: ", "build")
EmailMessage("voyager_tests.log", "Failures                     : 0", "voyager_tests")
EmailMessage("phaser_tests.log", "Failures                     : 0", "phaser_tests")
 
# copy log files to have date and platform in their names
try:
  os.mkdir(logdir)
except Exception as m:
  pass

shutil.copy("buildlog.txt", os.path.join(logdir, "buildlog_%s.txt" %thistime) )
mysubproc('scp buildlog.txt  ' + webaccount + ':%s/.' %websitedir )
shutil.copy("regressionlog.txt", os.path.join(logdir, "regressionlog_%s.txt" %thistime) )
mysubproc('scp regressionlog.txt  ' + webaccount + ':%s/.' %websitedir )
shutil.copy("voyager_tests.log", os.path.join(logdir, "voyager_tests.log_%s.txt" %thistime) )
mysubproc('scp voyager_tests.log  ' + webaccount + ':%s/.' %websitedir )
shutil.copy("phaser_tests.log", os.path.join(logdir, "phaser_tests.log_%s.txt" %thistime) )
mysubproc('scp phaser_tests.log  ' + webaccount + ':%s/.' %websitedir )

mysubproc('scp -r ' + logdir + '/* ' + webaccount + ':/var/www/html/htdocs/Local/PhaserNightly/Past/.')
# delete files older than 60 days on website
mysubproc('ssh ' + webaccount + ' "cd /var/www/html/htdocs/Local/PhaserNightly/Past && find . -mtime +60 | xargs rm -Rf"')

UpdateContentsOnWebsite()
