#!/bin/bash
#

# Script for building phaser and running regression test
# Assumes access to account www-structmed@arctic-1 where test results are copied to web page

# if running as a cron job make sure path and other environment variables are set
source /etc/profile

cd $HOME/NightlyBuild
mkdir Current
mkdir Logfiles
logdir=$HOME/NightlyBuild/Logfiles
regdir=$HOME/NightlyBuild/Current/tests/phaser_regression.regression
websitedir=/var/www/html/htdocs/Local/PhaserNightly/Current_${OSTYPE}
cd Current

thistime=`date +%d_%m_%Y`_${OSTYPE}

# clean up first
rm -rf ${logdir}/*
rm -rf ${regdir}/*
rm -rf base base_tmp build tests/phaser_regression.regression/*

# delete current log files on website
ssh www-structmed@arctic-1 "rm ${websitedir}/*"
# upload a new buildlog.txt stating that builds have now been started
echo -e "Bootstrap build started on `date +%c`\n" > buildlog.txt
# print environment variables for debugging
(set -o posix; set) | tee -a buildlog.txt
export | tee -a buildlog.txt
echo -e "\n" >> buildlog.txt

scp buildlog.txt www-structmed@arctic-1:${websitedir}/.
# update contents.html on the website
ssh www-structmed@arctic-1 "cd /var/www/html/htdocs/Local/PhaserNightly && python MakeRegressionTestContents.py"

# get bootstrap.py from cctbx github repo
curl https://raw.githubusercontent.com/cctbx/cctbx_project/master/libtbx/auto_build/bootstrap.py > bootstrap.py | tee -a buildlog.txt
# build phaser
python bootstrap.py --builder=phaser --nproc=8 | tee -a buildlog.txt
# upload complete buildlog file
scp buildlog.txt www-structmed@arctic-1:${websitedir}/.

# test phaser
# upload new terse_regression.txt file stating that test have been started
echo -e "Regression tests started on `date +%c`\n" > ${regdir}/terse_regression.txt
scp ${regdir}/terse_regression.txt www-structmed@arctic-1:${websitedir}/.
# update contents.html on the website
ssh www-structmed@arctic-1 "cd /var/www/html/htdocs/Local/PhaserNightly && python MakeRegressionTestContents.py"
# run tests
python bootstrap.py --builder=phaser --nproc=8 tests | tee -a ${regdir}/terse_regression.txt

# copy log files to have date and platform in their names
cp buildlog.txt ${logdir}/buildlog_${thistime}.txt
cp ${regdir}/terse_regression.txt ${logdir}/terse_regression_${thistime}.txt
cp ${regdir}/PhaserRegressionOutPut.txt ${logdir}/regression_${thistime}.txt
cp ${regdir}/PhaserRegressionOutPut.html ${logdir}/regression_${thistime}.html

# scp log files to website in folders tagged with platform
scp -r ${regdir}/* www-structmed@arctic-1:${websitedir}/.
scp buildlog.txt www-structmed@arctic-1:${websitedir}}/.
# scp logfiles with date tags into Past/ folder on website
scp -r ${logdir}/* www-structmed@arctic-1:/var/www/html/htdocs/Local/PhaserNightly/Past/.

# update contents.html on the website
ssh www-structmed@arctic-1 "cd /var/www/html/htdocs/Local/PhaserNightly && python MakeRegressionTestContents.py"
