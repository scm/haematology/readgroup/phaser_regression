
# Script for building phaser and running regression test
# Assumes access to account www-structmed@arctic-1 where test results are copied to web page
# If running as a cron job make sure path to compiler and other environment variables are set
# Mac bash users may have to source /etc/profile as to ensure correct environment for the compiler.
# crontab example for running at 1am every day:
#  0 1 * * *  /bin/bash -c "source /etc/profile && python /Users/phaserbuilder/PhaserNightlyBuild/nightlybuildscript.py" 1> /Users/phaserbuilder/PhaserNightlyBuild/crontab.log 2>&1

# Get this file on the PC that will run the regression tests with the command:
# curl https://git.uis.cam.ac.uk/x/cimr-phaser/phaser_regression.git/blob_plain/HEAD:/nightlybuildscript.py > nightlybuildscript.py
# then run it with the command
# python nightlybuildscript.py

# If running as a cron job make sure path to compiler and other environment variables are set
# Mac bash users may have to source /etc/profile as to ensure correct environment for the compiler.
#
# Crontab example for running at 1am every day:
#  0 1 * * *  /bin/bash -c "source /etc/profile && python /Users/phaserbuilder/PhaserNightlyBuild/nightlybuildscript.py" 1> /Users/phaserbuilder/PhaserNightlyBuild/crontab.log 2>&1
#
# Windows Task Scheduler action example:
# program: C:\Windows\System32\cmd.exe
# arguments: /c "C:\Microsoft\Visual C++ for Python\9.0\vcvarsall.bat" amd64 && python C:\Users\phaserbuilder\PhaserNightlyBuild\nightlybuildscript.py 2>&1 C:\Users\phaserbuilder\task.log
from __future__ import print_function

import os, sys, re, stat
import os.path
import subprocess
import platform, datetime, shutil, time
import multiprocessing

def mysubproc(command, tfile=None):
  print(command)
  if tfile:
    tfile.write(command + '\n')
    merr = subprocess.STDOUT
  else:
    merr = subprocess.PIPE
  proc = subprocess.Popen(
    command,
    shell=True,
    universal_newlines=True,
    stdout = subprocess.PIPE,
    stderr = merr
  )
  outs, errs = proc.communicate()
  if tfile:
    tfile.write(str(outs) + "\n" + str(errs) + "\n")
    tfile.flush()
  else:
    print( str(outs) + "\n" + str(errs) + "\n") # + "\n" + errs
  return outs, errs


def change_permissions_recursive(path, mode):
  for dir, dirs, files in os.walk(path, topdown=False):
    os.chmod(dir, mode)
    for file in files:
      os.chmod(os.path.join(dir, file), mode)


webaccount = "www-structmed@arctic-7"

def UpdateContentsOnWebsite():
  print( mysubproc('ssh ' + webaccount +
   ' "cd /var/www/html/htdocs/Local/PhaserNightly && python MakeRegressionTestContents.py"') )


pyexe = '"' + sys.executable + '"' 
nproc = multiprocessing.cpu_count() -1 # allow a spare CPU to idle
builddir = os.getcwd()

outs, errs = mysubproc("g++ -dumpversion")
version = outs.strip()
GCCversion = ""
longGCCversion = ""
if version:
  outs, errs  = mysubproc("g++ -dumpmachine")
  GCCversion = "gcc" + version + "_" + outs.strip()
  outs, errs = mysubproc("g++ --version")
  longGCCversion = outs.strip()

outs, errs  = mysubproc("cl.exe")
mstr = errs.strip() # cl.exe prints version info to stderr rather than stdout
version = re.findall("Version ([0-9\.]+)", mstr)
MSVCversion = ""
longMSVCversion = ""
if version:
  MSVCversion = "msvc" + version[0]
  longMSVCversion = mstr

compiler = MSVCversion
if not MSVCversion:
  compiler = GCCversion

# make string of ostype and platform for filenames
ostype = platform.platform() + "_" + compiler

if not os.path.exists("Current"):
  os.mkdir("Current")

logdir = os.path.join(builddir, "Logfiles")
regdir = os.path.join(builddir, "Current", "tests", "phaser_regression")
testsdir = os.path.join(builddir, "Current", "tests")
if "python3" in sys.argv:
  pyversion = "38"
else:
  pyversion = "27"
websitedir = "/var/www/html/htdocs/Local/PhaserNightly/Current_" + ostype + "_Py" + pyversion


os.chdir("Current")

td = datetime.datetime.today().timetuple()
thistime = "%d_%d_%d_%s" %(td[2], td[1], td[0], ostype + "_Py" + pyversion )

# clean up first
if os.path.exists(logdir):
  shutil.rmtree(logdir)

if os.path.exists(regdir):
  shutil.rmtree(regdir)

if os.path.exists("conda_base"):
  shutil.rmtree("conda_base")

if os.path.exists("mc3"):
  shutil.rmtree("mc3")

if os.path.exists("base"):
  shutil.rmtree("base")

if os.path.exists("base_tmp"):
  shutil.rmtree("base_tmp")

if os.path.exists("build"):
  shutil.rmtree("build")

# delete current log files on website
#p =mysubproc('ssh ' + webaccount + ' rm %s/*"' %websitedir).wait()
# upload a new buildlog.txt stating that builds have now been started
mfile = open("buildlog.txt","w")
mfile.write("Bootstrap build started on %s\n" % time.asctime( time.localtime(time.time())) )
# print environment variables to log file
for k,v in os.environ.items():
  mfile.write( k + "=" + v + "\n")

mfile.write("Using compiler:\n")
mfile.write(longMSVCversion +"\n")
mfile.write(longGCCversion +"\n")

mfile.close()
mysubproc('ssh ' + webaccount + ' mkdir %s' %websitedir )
mysubproc('scp buildlog.txt ' + webaccount + ':%s/.' %websitedir )
mfile = open("buildlog.txt","a")

# Update contents.html on the website. First get MakeRegressionTestContents.py from phaser_regression repo
mysubproc('curl https://gitlab.developers.cam.ac.uk/scm/haematology/readgroup/phaser_regression/-/raw/master/MakeRegressionTestContents.py  > MakeRegressionTestContents.py', mfile)
mysubproc('scp MakeRegressionTestContents.py ' + webaccount + ':/var/www/html/htdocs/Local/PhaserNightly/.')

UpdateContentsOnWebsite()

# get bootstrap.py from cctbx github repo
mysubproc('curl https://raw.githubusercontent.com/cctbx/cctbx_project/master/libtbx/auto_build/bootstrap.py > bootstrap.py', mfile)

# build phaser
mysubproc(pyexe + ' bootstrap.py --builder=phaser --use_conda --verbose --python=%s --nproc=%s' %(pyversion, nproc), mfile)

# upload complete buildlog file
mfile.close()
mysubproc('scp buildlog.txt ' + webaccount + ':%s/.' %websitedir )

# do the quick regression tests
outs, errs = mysubproc(pyexe + ' bootstrap.py --builder=phaser --use_conda --nproc=%s tests' %nproc)
tstlog = os.path.join(regdir,"run_tests_parallel_zlog")
mysubproc('scp ' + tstlog + ' ' + webaccount + ':%s/run_tests_parallel_zlog.txt' %websitedir)
#import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )

# upload new terse_regression.txt file stating that test have been started
rfile = open( 'terse_regression.txt', "w" )
rfile.write("Regression tests started on %s\n" % time.asctime( time.localtime(time.time())) )
rfile.close()
mysubproc('scp terse_regression.txt ' + webaccount + ':%s/.' %websitedir)
UpdateContentsOnWebsite()
# run tests
tfile = open( 'terse_regression.txt', "a" )
os.chdir("tests")
longtestcmd = os.path.join("..", "build", "bin","phaser_regression.regression") + ' all -o terse_failed -n %s' %nproc
#for testing long tests
#longtestcmd = os.path.join("..", "build", "bin","phaser_regression.regression") + ' tst_auto_toxd -o terse_failed -n %s' %nproc
mysubproc(longtestcmd, tfile)
os.chdir("..")
tfile.close()
# regdir is created by regression tests
# copy log files to have date and platform in their names
os.mkdir(logdir)
shutil.copy("buildlog.txt", os.path.join(logdir, "buildlog_%s.txt" %thistime) )
shutil.copy("terse_regression.txt", os.path.join(logdir, "terse_regression_%s.txt" %thistime) )
shutil.copy(tstlog, os.path.join(logdir, "run_tests_parallel_zlog_%s.txt" %thistime) )
shutil.copy(os.path.join(testsdir, "PhaserRegressionOutPut.txt"), os.path.join(logdir, "regression_%s.txt" %thistime) )
shutil.copy(os.path.join(testsdir, "PhaserRegressionOutPut.html"), os.path.join(logdir, "regression_%s.html" %thistime) )
# scp log files to website in folders tagged with platform
mysubproc('scp ' + os.path.join(testsdir, "PhaserRegressionOutPut.txt") + ' ' + webaccount + ':%s/.' %websitedir )
mysubproc('scp ' + os.path.join(testsdir, "PhaserRegressionOutPut.html") + ' ' + webaccount + ':%s/.' %websitedir )
mysubproc('scp buildlog.txt  ' + webaccount + ':%s/.' %websitedir )
mysubproc('scp terse_regression.txt  ' + webaccount + ':%s/.' %websitedir )
# scp logfiles with date tags into Past/ folder on website every 5 days, i.e. day of month modulo 5
#if time.localtime( time.time() )[2] % 5 == 0:
mysubproc('scp -r ' + logdir + '/* ' + webaccount + ':/var/www/html/htdocs/Local/PhaserNightly/Past/.')
# delete files older than 60 days on website
mysubproc('ssh ' + webaccount + ' "cd /var/www/html/htdocs/Local/PhaserNightly/Past && find . -mtime +60 | xargs rm -Rf"')

UpdateContentsOnWebsite()
#import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
