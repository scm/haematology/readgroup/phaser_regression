from phaser_regression import test_phaser_log_parser
from phaser_regression import test_reporter

import hashlib
import os.path

# get the longest common sub-path between filenames such as
# \home\airlie\phenix_builds\phenix-1.10-2155\modules\phaser_regression\data\regression\1d0d_b.pdb
# and c:\busers\oeffner\phenix\dev-2063-working\modules\phaser_regression\data\regression\1d0d_b.pdb
# which would be \modules\phaser_regression\data\regression\1d0d_b.pdb
def getcommonsuffix(data):

    substr = ''
    if len(data) > 1 and len(data[0]) > 0:
        for i in range(len(data[0])):
            for j in range(len(data[0])-i+1):
                if j > len(substr) and all(data[0][i:i+j] in x for x in data):
                    substr = data[0][i:i+j]

    return substr


# Template converters
def identity(setting):

    return setting


def boolean_to_on_off(setting):

    if setting is True:
        return "ON"

    elif setting is False:
        return "OFF"

    else:
        raise RuntimeError( "Invalid value %s for boolean" % setting[0])


def ensemble_or_connected(setting):

    return " OR ".join( "ENSEMBLE %s" % e for e in setting )


def dot_sol(setting):

    return setting.unparse()


class Composite(object):
    """
    Converter for composite settings
    """

    def __init__(self, individuals):

        self.individuals = individuals


    def __call__(self, setting):

        if len( setting ) != len( self.individuals ):
            print( len(setting), len(self.individuals))
            raise RuntimeError("Setting conversion: incorrect setting size")

        return tuple(
            [ conv( setting = s ) for ( conv, s ) in zip( self.individuals, setting ) ]
            )


# Keywords
class Simple(object):
    """
    Simple Phaser keyword-value pairs
    """

    def __init__(self, setter, setting, template, converter = None):

        self.setter = setter
        self.setting = setting
        self.template = template

        if converter is None:
            converter = Composite( individuals = [ identity ] * len( setting ) )

        self.converter = converter


    def apply_script(self, input):

        input.append( self.template % self.converter( setting = self.setting ) )


    def apply_python(self, input):

        getattr( input, self.setter )( *self.setting )


def Resolution(high, low = None):

    if low is None:
        setter = "setHIRES"
        template = "RESOLUTION %s"
        setting = ( high, )

    else:
        setter = "setRESO"
        template = "RESOLUTION %s %s"
        setting = ( high, low )

    return Simple( setter = setter, template = template, setting = setting )


def Root(prefix):

    return Simple( setter = "setROOT", template = "ROOT %s", setting = ( prefix, ) )


def Jobs(ncpus):

    return Simple( setter = "setJOBS", template = "JOBS %s", setting = ( ncpus, ) )


def Hklout(create):

    return Simple( setter = "setHKLO", template = "HKLOUT %s", setting = ( create, ) )


def Search(ensembles, count):

    return Simple(
        setter = "addSEAR_ENSE_OR_ENSE_NUM",
        template = "SEARCH %s NUM %s",
        setting = ( [ e.name for e in ensembles ], count ),
        converter = Composite( individuals = [ ensemble_or_connected, identity ] ),
        )


def Solution(string):

    from phaser.test_phaser_parsers import SolutionFileParser
    return Simple(
        setter = "setSOLU",
        template = "%s",
        setting = ( SolutionFileParser.parse_string( string ).get_dot_sol(), ),
        converter = Composite( individuals = [ dot_sol, ] ),
        )


def Wavelength(value):

    return Simple( setter = "setWAVE", template = "WAVELENGTH %.5f", setting = ( value, ) )


def LLGComplete_Complete(value):

    return Simple(
        setter = "setLLGC_COMP",
        template = "LLGCOMPLETE COMPLETE %s",
        setting = ( value, ),
        converter = Composite( individuals = [ boolean_to_on_off ] )
        )


def LLGComplete_Ncycle(n):

    return Simple(
        setter = "setLLGC_NCYC",
        template = "LLGCOMPLETE NCYC %s",
        setting = ( n, ),
        )


def LLGComplete_Element(element):

    return Simple(
        setter = "addLLGC_ELEM",
        template = "LLGCOMPLETE ELEMENT %s",
        setting = ( element, ),
        )


def LLGComplete_Sigma(sigma):

    return Simple(
        setter = "setLLGC_SIGM",
        template = "LLGCOMPLETE SIGMA %s",
        setting = ( sigma, ),
        )


def LLGComplete_Method(method):

    if method not in [ "IMAGINARY", "ATOMTYPE" ]:
        raise ValueError( "Unknown LLGComplete method: %s" % method)

    return Simple(
        setter = "setLLGC_METH",
        template = "LLGCOMPLETE METHOD %s",
        setting = ( method, ),
        )


def AtomPdb(xtal, filename):

    return Simple(
        setter = "setATOM_PDB",
        template = 'ATOM CRYSTAL %s PDB "%s"',
        setting = ( xtal, filename, )
        )


def AtomBfactorWilson(value):

    return Simple(
        setter = "setATOM_CHAN_BFAC_WILS",
        template = "ATOM CHAN BFAC WILS %s",
        setting = ( value, ),
        converter = Composite( individuals = [ boolean_to_on_off ] )
        )


def Scattering(type, fp, fdp, fix):

    return Simple(
        setter = "addSCAT",
        template = "SCATTERING TYPE %s FP=%s FDP=%s FIX %s",
        setting = ( type, fp, fdp, fix )
        )


def MacMR_Name(name):

    if name not in [ "DEFAULT", "CUSTOM", "OFF", "ALL" ]:
        raise ValueError( "Unknown protocol name: %s" % name)

    return Simple(
        setter = "setMACM_PROT",
        template = "MACMR PROTOCOL %s",
        setting = ( name, )
        )

def MacA_Name(name):

    if name not in [ "DEFAULT", "CUSTOM", "OFF", "ALL" ]:
        raise ValueError( "Unknown protocol name: %s" % name)

    return Simple(
        setter = "setMACA_PROT",
        template = "MACA PROTOCOL %s",
        setting = ( name, )
        )

def MacO_Name(name):

    if name not in [ "DEFAULT", "CUSTOM", "OFF", "ALL" ]:
        raise ValueError( "Unknown protocol name: %s" % name)

    return Simple(
        setter = "setMACO_PROT",
        template = "MACO PROTOCOL %s",
        setting = ( name, )
        )


def MacANO_Set(aniso, bins, solk, solb, ncyc = 8, minimizer = "BFGS"):

    if minimizer not in [ "BFGS", "NEWTON", "DESCENT" ]:
        raise ValueError( "Unknown minimizer: %s" % minimizer)

    return Simple(
        setter = "addMACA",
        template = "MACANO ANISO %s BINS %s SOLK %s SOLB %s NCYC %s",
        setting = ( aniso, bins, solk, solb, ncyc ),
        converter = Composite(
            individuals = [ boolean_to_on_off,
                            boolean_to_on_off,
                            boolean_to_on_off,
                            boolean_to_on_off,
                            identity
                            ]
            )
        )


def MacMR_Set(rot, tra, bfac, vrms, cell = False , ofac = False, last_only = False, ncyc = 8, minimizer = "BFGS"):

    if minimizer not in [ "BFGS", "NEWTON", "DESCENT" ]:
        raise ValueError( "Unknown minimizer: %s" % minimizer)

    return Simple(
        setter = "addMACM",
        template = "MACMR ROT %s TRA %s BFAC %s VRMS %s CELL %s OFAC %s LAST %s NCYC %s",
        setting = ( rot, tra, bfac, vrms, cell, ofac, last_only, ncyc ),
        converter = Composite(
            individuals = [ boolean_to_on_off,
                            boolean_to_on_off,
                            boolean_to_on_off,
                            boolean_to_on_off,
                            boolean_to_on_off,
                            boolean_to_on_off,
                            boolean_to_on_off,
                            identity
                            ]
            )
        )

def PurgeRot(mode):

    setbool = True
    if mode in ["OFF" ]:
      setbool = False

    return Simple(
        setter = "setPURG_ROTA_ENAB",
        template = "PURGE ROT ENABLE %s",
        setting = ( setbool, ),
        )

def PurgeRotNum(mode):

    return Simple(
        setter = "setPURG_ROTA_NUMB",
        template = "PURGE ROT NUM %s",
        setting = ( mode, ),
        )

def SearchMethod(mode):

    if mode not in [ "FULL", "FAST" ]:
        raise ValueError( "Unknown search method: %s" % mode)

    return Simple(
        setter = "setSEAR_METH",
        template = "SEARCH METHOD %s",
        setting = ( mode, ),
        )

def Debug(mode):

    setbool = True
    if mode in ["OFF" ]:
      setbool = False

    return Simple(
        setter = "setDEBU",
        template = "DEBUG %s",
        setting = ( setbool, ),
        )

def ZscoreStop(mode):

    setbool = True
    if mode in ["OFF" ]:
      setbool = False

    return Simple(
        setter = "setZSCO_STOP",
        template = "ZSCORE STOP %s",
        setting = ( setbool, ),
        )


def Peak_Rota_Down(setting):

    return Simple(
        setter = "setPEAK_ROTA_DOWN",
        template = "PEAKS ROT DOWN %s",
        setting = ( setting, )
        )


def TargetRot(setting):

    if setting not in [ "FAST", "BRUTE" ]:
        raise ValueError( "Unknown TARGET ROT target: %s" % setting)

    return Simple(
        setter = "setTARG_ROTA",
        template = "TARGET ROT %s",
        setting = ( setting, )
        )


def TargetTra(setting):

    if setting not in [ "FAST", "BRUTE", "PHASED" ]:
        raise ValueError( "Unknown TARGET TRA target: %s" % setting)

    return Simple(
        setter = "setTARG_TRAN",
        template = "TARGET TRA %s",
        setting = ( setting, )
        )


# More complex keywords
class MRReflectionData(object):
    """
    Input reflection data for molecular replacement
    """

    def __init__(self, file_name, ip = None, sigip = None, fp = None, sigfp = None, fmap = None, phmap = None):

        self.file_name = file_name
        self.ip = ip
        self.sigip = sigip
        self.fp = fp
        self.sigfp = sigfp
        self.fmap = fmap
        self.phmap = phmap
        self.loaded = False
        self.space_group = None
        self.unit_cell = None
        self.data = None


    def load(self):

        import phaser

        input = phaser.InputMR_DAT()
        input.setMUTE( True )

        input.setHKLI( self.file_name )
        print( self.ip, self.sigip, self.fp, self.sigfp)
        if self.ip:
          input.setLABI_I_SIGI( self.ip, self.sigip )
        else:
          input.setLABI_F_SIGF( self.fp, self.sigfp )

        if self.fmap and self.phmap:
            input.setLABI_FWT( self.fmap )
            input.setLABI_PHWT( self.phmap )

        result = phaser.runMR_DAT( input )

        if result.Failed():
            raise RuntimeError( result.logfile())

        self.space_group = result.getSpaceGroupName()
        self.unit_cell = result.getUnitCell()
        self.data = result.getDATA()
        self.loaded = True


    def apply_script(self, input):

        input.extend(
            [
                'HKLIN "%s"' % self.file_name,
            ]
        )
        if self.ip:
            input.extend(
                [
                    "LABIN I=%s SIGI=%s" % ( self.ip, self.sigip ),
                ]
            )
        else:
            input.extend(
                [
                    "LABIN F=%s SIGF=%s" % ( self.fp, self.sigfp ),
                ]
            )
        if self.fmap and self.phmap:
            input.extend(
                [
                    "LABIN FMAP=%s PHMAP=%s" % ( self.fmap, self.phmap ),
                ]
            )


    def apply_python_reflections(self, input):

        if not self.loaded:
            self.load()

        assert self.loaded
        input.setREFL_DATA( self.data )


    def apply_python_symmetry(self, input):

        if not self.loaded:
            self.load()

        assert self.loaded
        input.setSPAC_NAME( self.space_group )
        input.setCELL6( self.unit_cell )


class EPReflectionData(object):
    """
    Input reflection data for experimental phasing
    """

    def __init__(self, file_name, labels_for):

        self.file_name = file_name
        self.labels_for = labels_for
        self.loaded = False
        self.space_group = None
        self.unit_cell = None
        self.indices = []
        self.data_for = {}
        self.crysdata = None


    def load(self):

        if self.loaded:
            return

        import phaser

        input = phaser.InputEP_DAT()
        input.setMUTE( True )

        input.setHKLI( self.file_name )

        for ( ( xtal, wave ), labels ) in self.labels_for.items():
            input.addCRYS_ANOM_LABI( xtal, wave, *labels )

        result = phaser.runEP_DAT( input )

        if result.Failed():
            raise RuntimeError( result.logfile())

        self.space_group = result.getSpaceGroupName()
        self.unit_cell = result.getUnitCell()
        self.indices = result.getMiller()
        self.crysdata = result.getCrysData()

        for ( xtal, wave ) in self.labels_for:
            self.data_for[ ( xtal, wave ) ] = (
                result.getFpos( xtal, wave ),
                result.getSIGFpos( xtal, wave ),
                result.getPpos( xtal, wave ),
                result.getFneg( xtal, wave ),
                result.getSIGFneg( xtal, wave ),
                result.getPneg( xtal, wave ),
                )

        self.loaded = True


    def apply_script(self, input):

        input.append( 'HKLIN "%s"' % self.file_name )
        input.extend(
            [
                "CRYSTAL %s DATASET %s LABIN F+ = %s SIGF+ = %s F- = %s SIGF- = %s" % ( key + value )
                for ( key, value ) in self.labels_for.items()
                ]
            )


#    def apply_python_reflections(self, input):
#
#        self.load()
#
#        assert self.loaded
#        input.setCRYS_MILLER( self.indices )
#
#        for ( ( xtal, wave ), data ) in self.data_for.items():
#            input.addCRYS_ANOM_DATA( xtal, wave, *data )

    def apply_python_reflections(self, input):

        if not self.loaded:
            self.load()

        assert self.loaded
        input.setCRYS_DATA( self.crysdata )

    def apply_python_symmetry(self, input):

        self.load()

        assert self.loaded
        input.setSPAC_NAME( self.space_group )
        input.setCELL6( self.unit_cell )


class Component(object):
    """
    This class holds valid composition definitions
    """

    def __init__(self, setter, template, settings):

        self.setter = setter
        self.template = template
        self.settings = settings


    def apply_python(self, input):

        getattr( input, self.setter )( *self.settings )


    def apply_script(self, input):

        input.append( "COMPOSITION %s" % ( self.template % self.settings ) )


    @classmethod
    def protein_mw_num(cls, mw, num):

        return cls(
            setter = "addCOMP_PROT_MW_NUM",
            template = "PROTEIN MW %s NUM %s",
            settings = ( mw, num ),
            )


    @classmethod
    def protein_seq_num(cls, seq, num):

        return cls(
            setter = "addCOMP_PROT_SEQ_NUM",
            template = 'PROTEIN SEQ "%s" NUM %s',
            settings = ( seq, num ),
            )


    @classmethod
    def composition_by(cls, mode):

        if mode not in [ "AVERAGE", "SOLVENT", "ASU" ]:
            raise ValueError("Unknown composition_by setting: %s" % mode)

        return cls(
            setter = "setCOMP_BY",
            template = "BY %s",
            settings = ( mode, ),
            )


class EqualityTester(object):
    """
    for a more descriptive error message when ensemble objects differ
    """
    def __init__(self):
        self.uneqstr = ""

    def equal(self, l, r):
        # if comparing paths then deal with full local paths accidentally being stored
        if type( l ) is str and len( l.split(os.path.sep) ) > 1 : # then it's a path
            if len( getcommonsuffix( [ l, r ]).split(os.path.sep) ) < 2:
                self.uneqstr = "%s differs from %s" % (l, r)
                return False # must have at least one subdirectory the same
            return True

        if l != r:
            self.uneqstr = "%s differs from %s" % (l, r)
            return False

        return True


class EnsemblePDBFile(EqualityTester):
    """
    PDBFile subkeyword
    """

    def __init__(self, module_name, file_name, setter, keyword, value):

        self.module_name = module_name
        self.file_name = os.path.normcase( file_name )
        self.setter = setter
        self.keyword = keyword
        self.value = value
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        #self.md5 = hashlib.md5( open(self.file_name, "rb").read() ).hexdigest()


    def file_path(self):

      import libtbx.load_env

      module_path = libtbx.env.dist_path( self.module_name )
      return os.path.join( module_path, self.file_name )


    def set_on_python_object(self, input, name):

        import os.path
        getattr( input, self.setter )(
            name,
            self.file_path(),
            self.value,
            True,
            )


    def script_keyword(self):

        import os.path
        return 'PDBFILE "%s" %s %s' % (
            self.file_path(),
            self.keyword,
            self.value
            )


    def root(self):

        import iotbx.pdb
        import os.path
        return iotbx.pdb.input( self.file_path() ).construct_hierarchy()


    def __eq__(self, other):
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        """
        filematches = True
        if hasattr(other, "md5"):
            filematches = self.equal( self.md5, other.md5)
        else:
            filematches = self.equal( self.file_name, other.file_name )
        """
        return ( self.equal( self.__class__, other.__class__ )
            #and filematches
            and self.equal( self.setter, other.setter )
            and self.equal( self.keyword, other.keyword )
            and self.equal( self.value, other.value ) )


    def __ne__(self, other):

        return not ( self == other )


    def __setstate__(self, dict):

        self.__dict__ = dict

        import os.path
        self.file_name = os.path.normcase( self.file_name )


    @classmethod
    def Id(cls, module_name, file_name, value):

        return cls(
            module_name = module_name,
            file_name = file_name,
            setter = "addENSE_PDB_ID",
            keyword = "IDENTITY",
            value = value
            )


    @classmethod
    def Rms(cls, module_name, file_name, value):

        return cls(
            module_name = module_name,
            file_name = file_name,
            setter = "addENSE_PDB_RMS",
            keyword = "RMS",
            value = value
            )

# TODO: move EnsembleFragment up on level with EnsembleModel as they are logically on par with one another
class EnsembleFragment(EqualityTester):
    """
    PDBFile subkeyword
    """

    def __init__(self, setter, keyword, value, rms):

        self.value = value
        self.keyword = keyword
        self.rms = rms
        self.setter = setter


    def set_on_python_object(self, input, name):

        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        getattr( input, self.setter )(
            name,
            self.value,
            self.rms,
            )


    def script_keyword(self):

        return '%s %s RMS %s' % (
            self.keyword,
            self.value, # length when HELIX, atom type when ATOM
            self.rms
            )


    def root(self):
        import iotbx.pdb # return empty hierarchy for the sake of EnsembleModel.match_info()
        return iotbx.pdb.hierarchy.root()


    def __eq__(self, other):

        return ( self.equal( self.__class__, other.__class__ )
            and self.equal( self.setter, other.setter )
            and self.equal( self.rms, other.rms )
            and self.equal( self.value, other.value ) )


    def __ne__(self, other):

        return not ( self == other )


    def __setstate__(self, dict):

        self.__dict__ = dict


    @classmethod
    def Helix(cls, length, rms):

        return cls(
            setter = "addENSE_HELI",
            keyword = "HELIX",
            value = length,
            rms = rms,
            )


    @classmethod
    def Atom(cls, type, rms):

        return cls(
            setter = "addENSE_ATOM",
            keyword = "ATOM",
            value = type,
            rms = rms,
            )



class EnsembleModel(EqualityTester):
    """
    Ensemble keyword
    """

    def __init__(self, name, descriptions):

        assert descriptions
        self.name = name
        self.descriptions = descriptions


    def apply_python(self, input):
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        for d in self.descriptions:
            d.set_on_python_object( input = input, name = self.name )


    def apply_script(self, input):
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )

        input.append(
#           "ENSEMBLE %s %s" % (
#               self.name,
#               " ".join( d.script_keyword() for d in self.descriptions )
#               )
            " ".join("ENSEMBLE %s %s\n" % (self.name, d.script_keyword()) for d in self.descriptions ))


    def match_info(self):

        import iotbx.pdb
        from phaser import template_new as template
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        if len(self.descriptions[0].root().atoms()):
            return template.EnsembleInfo( root = self.descriptions[0].root() )
        else:
            return None


    def __eq__(self, other):

        if self.descriptions != other.descriptions:
            return False
            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
            #for i, d in enumerate(self.descriptions):
            #    if not self.equal( d.file_name, other.descriptions[i].file_name ):
            #        return False

        return ( self.equal( self.__class__, other.__class__ )
            and self.equal( self.name, other.name ) )


    def __ne__(self, other):

        return not ( self == other )


class EnsembleMap(EqualityTester):
    """
    MAPFile subkeyword
    """

    def __init__(self, name, module_name, hklin, f, phi, extent, rms, centre, pmw, nmw, scale):

        import os.path
        self.name = name
        self.module_name = module_name
        self.file_name = os.path.normcase( hklin )
        self.f = f
        self.phi = phi
        self.extent = extent
        self.rms = rms
        self.centre = centre
        self.pmw = pmw
        self.nmw = nmw
        self.scale = scale


    def file_path(self):

      import libtbx.load_env

      module_path = libtbx.env.dist_path( self.module_name )
      return os.path.join( module_path, self.file_name )


    def apply_python(self, input):

        import os.path
        input.addENSE_MAP(
            self.name,
            self.file_path(),
            self.f,
            self.phi,
            self.extent,
            self.rms,
            self.centre,
            self.pmw,
            self.nmw,
            self.scale,
            )


    def apply_script(self, input):

        import os.path
        input.append(
            "ENSEMBLE %s %s %s %s %s %s %s %s %s %s" % (
                self.name,
                'HKLIN "%s"' % self.file_path(),
                "F=%s" % self.f,
                "PHI=%s" % self.phi,
                "EXTENT %7.4f %7.4f %7.4f" % self.extent,
                "RMS %7.4f" % self.rms,
                "CENTRE %7.4f %7.4f %7.4f" % self.centre,
                "PROTEIN MW %s" % self.pmw,
                "NUCLEIC MW %s" % self.nmw,
                "CELL SCALE %s" % self.scale,
                )
            )


    def match_info(self):

        from phaser import template_new as template
        return template.MapEnsembleInfo(
            centre = self.centre,
            extent = self.extent
            )


    def __eq__(self, other):
        return ( self.equal( self.__class__, other.__class__ )
            #and self.equal( self.file_name, other.file_name )
            and self.equal( self.f, other.f )
            and self.equal( self.phi, other.phi )
            and self.equal( self.extent,  other.extent)
            and self.equal( self.rms, other.rms )
            and self.equal( self.centre,  other.centre )
            and self.equal( self.pmw , other.pmw )
            and self.equal( self.nmw, other.nmw )
            and self.equal( self.scale, other.scale )
            )


    def __ne__(self, other):

        return not ( self == other )


    def __setstate__(self, dict):

        self.__dict__ = dict

        import os.path
        self.file_name = os.path.normcase( self.file_name )



class Rotate(object):
    """
    ROTATE keyword
    """

    def __init__(self, mode, data):

        self.mode = mode
        self.data = data


    def apply_python(self, input):

        self.SETTER_FOR[ self.mode ][0]( self, input = input )


    def apply_script(self, input):

        self.SETTER_FOR[ self.mode ][1]( self, input = input )


    def full_python(self, input):

        input.setROTA_VOLU( "FULL" )


    def full_script(self, input):

        input.append( "ROTATE VOLUME FULL" )


    def around_python(self, input):

        ( euler, range ) = self.data
        input.setROTA_VOLU( "AROUND" )
        input.setROTA_EULE( euler )
        input.setROTA_RANG( range )


    def around_script(self, input):

        ( euler, range ) = self.data
        input.append(
            "ROTATE VOLUME AROUND EULER %s RANGE %.2f" % (
                "%8.3f %8.3f %8.3f" % euler,
                range
                ),
            )


    SETTER_FOR = {
        "full": ( full_python, full_script ),
        "around": ( around_python, around_script ),
        }


    @classmethod
    def Full():

        return cls( mode = "full", data = None )


    @classmethod
    def Around(cls, euler, range):

        return cls( mode = "around", data = ( euler, range ) )


class Translate(object):
    """
    TRANSLATE keyword
    """

    def __init__(self, mode, data):

        self.mode = mode
        self.data = data


    def apply_python(self, input):

        self.SETTER_FOR[ self.mode ][0]( self, input = input )


    def apply_script(self, input):

        self.SETTER_FOR[ self.mode ][1]( self, input = input )


    def full_python(self, input):

        input.setTRAN_VOLU( "FULL" )


    def full_script(self, input):

        input.append( "TRANSLATE VOLUME FULL" )


    def around_python(self, input):

        ( point, range, frac ) = self.data
        input.setTRAN_VOLU( "AROUND" )
        input.setTRAN_POIN( point )
        input.setTRAN_RANG( range )
        input.setTRAN_FRAC( frac )


    def around_script(self, input):

        ( point, range, frac ) = self.data
        input.append(
            "TRANSLATE VOLUME AROUND %s POINT %s RANGE %s" % (
                "FRAC" if frac else "ORTH",
                "%8.3f %8.3f %8.3f" % point,
                range
                )
            )

    SETTER_FOR = {
        "full": ( full_python, full_script ),
        "around": ( around_python, around_script ),
        }

    @classmethod
    def Full(cls):

        return cls( mode = "full", data = None )


    @classmethod
    def Around(cls, point, range, frac):

        return cls( mode = "around", data = ( point, range, frac ) )


class SgAlternative(object):
    """
    SGAL keyword
    """

    def __init__(self, mode, data):

        self.mode = mode
        self.data = data


    def apply_python(self, input):

        input.setSGAL_SELE( self.mode )

        for sg in self.data:
            input.addSGAL_NAME( sg )


    def apply_script(self, input):

        input.append( "SGALTERNATIVE SELECT %s" % self.mode )
        input.extend( [ "SGALTERNATIVE TEST %s" % sg for sg in self.data ] )

    @classmethod
    def All(cls):

        return cls( mode = "ALL", data = () )

    @classmethod
    def Hand(cls):

        return cls( mode = "HAND", data = () )

    @classmethod
    def List(cls, test):

        return cls( mode = "LIST", data = test )

    @classmethod
    def Current(cls):

        return cls( mode = "NONE", data = () )


class AtomElement(object):
    """
    ATOM ELEMENT keyword
    """

    def __init__(self, xtal, element, coords, occ):

        self.xtal = xtal
        self.element = element
        self.coords = coords
        self.occ = occ


    def apply_python(self, input):

        ( x, y, z ) = self.coords
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        input.addATOM( self.xtal.encode(), self.element.encode(), x, y, z, self.occ )


    def apply_script(self, input):

        input.append(
            "ATOM CRYSTAL %s ELEMENT %s FRAC %s OCC %.5f" % (
                self.xtal,
                self.element,
                " ".join( "%.5f" % v for v in self.coords ),
                self.occ,
                )
            )


    @classmethod
    def from_atom_records(cls, text):

        from phaser.test_phaser_parsers import AtomFileParser
        data = AtomFileParser.parse_string( atom_string = text )
        return [
            cls(
                xtal = a.xtal,
                element = a.elem,
                coords = ( a.x, a.y, a.z ),
                occ = a.occ
                )
            for a in data.atoms
            ]


class Partial(object):
    """
    Phaser PARTIAL keyword
    """

    SETTER_FOR_MODE = {
        "PDB": "setPART_PDB",
        "HKLIN": "setPART_HKLI",
        }

    KEYWORD_FOR_ERRORDEF = {
        "IDENTITY": "ID",
        "RMS": "RMS",
        }

    def __init__(self, mode, file_name, errordef, value):

        assert mode in self.SETTER_FOR_MODE
        assert errordef in self.KEYWORD_FOR_ERRORDEF
        self.mode = mode
        self.file_name = file_name
        self.errordef = errordef
        self.value = value


    def apply_python(self, input):

        setter = self.SETTER_FOR_MODE[ self.mode ]
        getattr( input, setter )( self.file_name )
        keyword = self.KEYWORD_FOR_ERRORDEF[ self.errordef ]
        input.setPART_VARI( keyword )
        input.setPART_DEVI( self.value )


    def apply_script(self, input):

        input.append(
            'PARTIAL %s "%s" %s %.3f' % (
                self.mode,
                self.file_name,
                self.errordef,
                self.value
                )
            )


class Mode(object):
    """
    Phaser mode
    """

    KEYWORDS = ( "ANO", "MR_FRF", "MR_FTF", "MR_PAK", "SCEDS",
        "MR_RNP", "GYRE", "MR_AUTO", "EP_AUTO", "EP_SAD", "PRUNE", "MR_ATOM")
    INPUT_OBJECT_FOR = None
    RUN_FUNCTION_FOR = None
    LOG_PARSER_FOR = None

    @classmethod
    def get_input_object_for(cls, keyword):

        if cls.INPUT_OBJECT_FOR is None:
            import phaser
            input_classes = (
                phaser.InputANO,
                phaser.InputMR_FRF,
                phaser.InputMR_FTF,
                phaser.InputMR_PAK,
                phaser.InputNMA,
                phaser.InputMR_RNP,
                phaser.InputMR_RNP,
                phaser.InputMR_AUTO,
                phaser.InputEP_AUTO,
                phaser.InputEP_SAD,
                phaser.InputMR_OCC,
                phaser.InputMR_ATOM,
                )
            assert len( cls.KEYWORDS ) == len( input_classes )
            cls.INPUT_OBJECT_FOR = dict( zip( cls.KEYWORDS, input_classes ) )

        return cls.INPUT_OBJECT_FOR[ keyword ]


    def __init__(self, keyword):

        if keyword not in self.KEYWORDS:
            raise RuntimeError( "Unknown mode: %s" % keyword)

        self.keyword = keyword


    def script_input(self, ncpus = 1):

        return [ "MODE %s" % self.keyword, "JOBS %s" % ncpus ]


    def parse_log(self, root):
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        return self.get_log_parser_for( keyword = self.keyword )( root = root )


    def python_input(self, ncpus = 1):

        input = self.get_input_object_for( keyword = self.keyword )()

        if hasattr( input, "setJOBS" ):
            input.setJOBS( ncpus )

        return input


    def python_run(self, input, stream):

        import phaser
        output = phaser.Output()
        output.setPackagePhenix( file_object = stream )
        return self.get_run_function_for( keyword = self.keyword )( input, output )


    def apply_python_xray_data(self, input, data):
        if self.keyword == "SCEDS":
            return

        data.apply_python_symmetry( input = input )

        if self.keyword != "MR_PAK":
            data.apply_python_reflections( input = input )


    def apply_script_xray_data(self, input, data):

        data.apply_script( input = input )


    @classmethod
    def get_run_function_for(cls, keyword):

        if cls.RUN_FUNCTION_FOR is None:
            import phaser
            run_functions = (
                phaser.runANO,
                phaser.runMR_FRF,
                phaser.runMR_FTF,
                phaser.runMR_PAK,
                phaser.runSCEDS,
                phaser.runMR_RNP,
                phaser.runMR_GYRE,
                phaser.runMR_AUTO,
                phaser.runEP_AUTO,
                phaser.runEP_SAD,
                phaser.runMR_OCC,
                phaser.runMR_ATOM,
                )
            assert len( cls.KEYWORDS ) == len( run_functions )
            cls.RUN_FUNCTION_FOR = dict( zip( cls.KEYWORDS, run_functions ) )

        return cls.RUN_FUNCTION_FOR[ keyword ]


    @classmethod
    def get_log_parser_for(cls, keyword):

        if cls.LOG_PARSER_FOR is None:
            from phaser_regression import test_phaser_log_parser
            log_parsers = (
                test_phaser_log_parser.ScriptResultANO,
                test_phaser_log_parser.ScriptResultMR_FRF,
                test_phaser_log_parser.ScriptResultMR_FTF,
                test_phaser_log_parser.ResultMR.PAK_from_root,
                test_phaser_log_parser.ResultNMA.SCEDS_from_root,
                test_phaser_log_parser.ResultMR.RNP_from_root,
                test_phaser_log_parser.ResultGYRE.GYRE_from_root,
                test_phaser_log_parser.ResultMR.RNP_from_root,
                test_phaser_log_parser.ScriptResultEP,
                test_phaser_log_parser.ScriptResultEP,
                test_phaser_log_parser.ResultMR.OCC_from_root,
                test_phaser_log_parser.ScriptResultEP,
                )
            assert len( cls.KEYWORDS ) == len( log_parsers )
            cls.LOG_PARSER_FOR = dict( zip( cls.KEYWORDS, log_parsers ) )
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        return cls.LOG_PARSER_FOR[ keyword ]


# Input
class InputData(object):
    """
    Input data for a job
    """

    def __init__(
        self,
        name,
        mode,
        data,
        keywords,
        reporters,
        resfile,
        disable_sections,
        disable_tests_in,
        ):

        self.name = name
        self.mode = mode
        self.data = data
        self.keywords = keywords
        self.reporters = reporters
        self.resfile = resfile
        self.disable_sections = set( disable_sections )
        self.disable_tests_in = dict(
            ( k, set( v ) ) for ( k, v ) in disable_tests_in.items()
            )

    def evaluate(self, input, result, prefix):

        if result.Failed():
            raise RuntimeError( result.ErrorMessage())
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        return test_reporter.create(
            reporters = self.reporters,
            input = input,
            result = result,
            prefix = prefix,
            disable_sections = self.disable_sections,
            disable_tests_in = self.disable_tests_in,
            )


    def ensembles(self):

        return [ e for e in self.keywords if isinstance( e, ( EnsembleModel, EnsembleMap ) ) ]


    def apply_python_xray_data(self, input):

        self.mode.apply_python_xray_data( input = input, data = self.data )


    def apply_script_xray_data(self, input):

        self.mode.apply_script_xray_data( input = input, data = self.data )


    @classmethod
    def anisotropy_correction(
        cls,
        launcher,
        data,
        extra_keywords = [],
        disable_sections = [],
        disable_tests_in = {},
        ):

        return cls(
            name = launcher,
            mode = Mode( keyword = "ANO" ),
            data = data.data_mr,
            keywords = extra_keywords,
            reporters = (
                test_reporter.symmetry_information,
                test_reporter.mr_dataset_statistics,
                test_reporter.anisotropy_correction,
                ),
            resfile = results_file( launcher = launcher ),
            disable_sections = disable_sections,
            disable_tests_in = disable_tests_in,
            )


    @classmethod
    def fast_rotation_function(
        cls,
        launcher,
        data,
        search,
        extra_keywords = [],
        disable_sections = [],
        disable_tests_in = {},
        ):

        keywords = []
        keywords.extend( data.components )
        keywords.extend( data.ensembles )
        keywords.append( Search( ensembles = [ search ], count = 1 ) )
        keywords.extend( extra_keywords )
        return InputData(
            name = launcher,
            mode = Mode( keyword = "MR_FRF" ),
            data = data.data_mr,
            keywords = keywords,
            reporters = (
                test_reporter.symmetry_information,
                test_reporter.rotation_function,
                ),
            resfile = results_file( launcher = launcher ),
            disable_sections = disable_sections,
            disable_tests_in = disable_tests_in,
            )

    @classmethod
    def brute_rotation_function(
        cls,
        launcher,
        data,
        search,
        extra_keywords = [],
        disable_sections = [],
        disable_tests_in = {},
        ):

        keywords = [ TargetRot( setting = "BRUTE" ) ]
        keywords.extend( data.components )
        keywords.extend( data.ensembles )
        keywords.append( Search( ensembles = [ search ], count = 1 ) )
        keywords.extend( extra_keywords )
        return InputData(
            name = launcher,
            mode = Mode( keyword = "MR_FRF" ),
            data = data.data_mr,
            keywords = keywords,
            reporters = (
                test_reporter.symmetry_information,
                test_reporter.rotation_function,
                ),
            resfile = results_file( launcher = launcher ),
            disable_sections = disable_sections,
            disable_tests_in = disable_tests_in,
            )

    @classmethod
    def fast_translation_function(
        cls,
        launcher,
        data,
        rlist,
        extra_keywords = [],
        disable_sections = [],
        disable_tests_in = {},
        ):

        keywords = []
        keywords.extend( data.components )
        keywords.extend( data.ensembles )
        keywords.append( Solution( string = rlist ) )
        keywords.extend( extra_keywords )
        return InputData(
            name = launcher,
            mode = Mode( keyword = "MR_FTF" ),
            data = data.data_mr,
            keywords = keywords,
            reporters = (
                test_reporter.symmetry_information,
                test_reporter.solutions,
                test_reporter.translation_function,
                ),
            resfile = results_file( launcher = launcher ),
            disable_sections = disable_sections,
            disable_tests_in = disable_tests_in,
            )

    @classmethod
    def brute_translation_function(
        cls,
        launcher,
        data,
        rlist,
        extra_keywords = [],
        disable_sections = [],
        disable_tests_in = {},
        ):

        keywords = [ TargetTra( setting = "BRUTE" ) ]
        keywords.extend( data.components )
        keywords.extend( data.ensembles )
        keywords.append( Solution( string = rlist ) )
        keywords.extend( extra_keywords )
        return InputData(
            name = launcher,
            mode = Mode( keyword = "MR_FTF" ),
            data = data.data_mr,
            keywords = keywords,
            reporters = (
                test_reporter.symmetry_information,
                test_reporter.solutions,
                test_reporter.translation_function,
                ),
            resfile = results_file( launcher = launcher ),
            disable_sections = disable_sections,
            disable_tests_in = disable_tests_in,
            )

    @classmethod
    def packing_function(
        cls,
        launcher,
        data,
        sol,
        extra_keywords = [],
        disable_sections = [],
        disable_tests_in = {},
        ):

        keywords = []
        keywords.extend( data.ensembles )
        keywords.append( Solution( string = sol ) )
        keywords.extend( extra_keywords )
        return InputData(
            name = launcher,
            mode = Mode( keyword = "MR_PAK" ),
            data = data.data_mr,
            keywords = keywords,
            reporters = (
                test_reporter.symmetry_information,
                test_reporter.solutions,
                test_reporter.auto_pak_rnp_function,
                ),
            resfile = results_file( launcher = launcher ),
            disable_sections = disable_sections,
            disable_tests_in = disable_tests_in,
            )

    @classmethod
    def gyre_refinement_function(
        cls,
        launcher,
        data,
        rlist,
        extra_keywords = [],
        disable_sections = [],
        disable_tests_in = {},
        ):

        keywords = []
        keywords.extend( data.ensembles )
        keywords.append( Solution( string = rlist ) )
        keywords.extend( data.components )
        keywords.extend( extra_keywords )
        return InputData(
            name = launcher,
            mode = Mode( keyword = "GYRE" ),
            data = data.data_mr,
            keywords = keywords,
            reporters = (
                test_reporter.gyre_rottra,
                test_reporter.gyre_scores,
                ),
            resfile = results_file( launcher = launcher ),
            disable_sections = disable_sections,
            disable_tests_in = disable_tests_in,
            )

    @classmethod
    def refinement_and_phasing_function(
        cls,
        launcher,
        data,
        sol,
        extra_keywords = [],
        disable_sections = [],
        disable_tests_in = {},
        ):

        keywords = []
        keywords.extend( data.ensembles )
        keywords.append( Solution( string = sol ) )
        keywords.extend( data.components )
        keywords.extend( extra_keywords )
        return InputData(
            name = launcher,
            mode = Mode( keyword = "MR_RNP" ),
            data = data.data_mr,
            keywords = keywords,
            reporters = (
                test_reporter.symmetry_information,
                test_reporter.solutions,
                test_reporter.vrms_refinement,
                test_reporter.auto_pak_rnp_function,
                ),
            resfile = results_file( launcher = launcher ),
            disable_sections = disable_sections,
            disable_tests_in = disable_tests_in,
            )

    @classmethod
    def pruning(
        cls,
        launcher,
        data,
        sol,
        extra_keywords = [],
        disable_sections = [],
        disable_tests_in = {},
        ):

        keywords = [ MacO_Name( name = "DEFAULT" ) ]
        keywords.extend( data.ensembles )
        keywords.append( Solution( string = sol ) )
        keywords.extend( extra_keywords )
        return InputData(
            name = launcher,
            mode = Mode( keyword = "PRUNE" ),
            data = data.data_mr,
            keywords = keywords,
            reporters = (
                test_reporter.pruning_scores,
                test_reporter.pruning_fraction,
                ),
            resfile = results_file( launcher = launcher ),
            disable_sections = disable_sections,
            disable_tests_in = disable_tests_in,
            )


    @classmethod
    def log_likelihood_gain_function(
        cls,
        launcher,
        data,
        sol,
        extra_keywords = [],
        disable_sections = [],
        disable_tests_in = {},
        ):

        keywords = [ MacMR_Name( name = "OFF" ) ]
        keywords.extend( data.ensembles )
        keywords.append( Solution( string = sol ) )
        keywords.extend( extra_keywords )
        return InputData(
            name = launcher,
            mode = Mode( keyword = "MR_RNP" ),
            data = data.data_mr,
            keywords = keywords,
            reporters = (
                test_reporter.symmetry_information,
                test_reporter.solutions,
                test_reporter.vrms_refinement,
                test_reporter.auto_pak_rnp_function,
                ),
            resfile = results_file( launcher = launcher ),
            disable_sections = disable_sections,
            disable_tests_in = disable_tests_in,
            )

    @classmethod
    def mr_auto_function(
        cls,
        launcher,
        data,
        searches,
        perform_pruning_check = False,
        extra_keywords = [],
        disable_sections = [],
        disable_tests_in = {},
        ):

        keywords = []
        keywords.extend( data.components )
        keywords.extend( data.ensembles )
        keywords.extend( searches )
        keywords.extend( extra_keywords )
        reporters = (
            test_reporter.symmetry_information,
            test_reporter.solutions,
            test_reporter.vrms_refinement,
            test_reporter.auto_pak_rnp_function,
            )

        if perform_pruning_check:
            reporters += ( test_reporter.pruning_fraction, )

        return InputData(
            name = launcher,
            mode = Mode( keyword = "MR_AUTO" ),
            data = data.data_mr,
            keywords = keywords,
            reporters = reporters,
            resfile = results_file( launcher = launcher ),
            disable_sections = disable_sections,
            disable_tests_in = disable_tests_in,
            )

    @classmethod
    def ep_sad_function(
        cls,
        launcher,
        data,
        initials,
        extra_keywords = [],
        disable_sections = [],
        disable_tests_in = {},
        ):

        keywords = []
        keywords.extend( data.components )
        keywords.append( data.wavelength )
        keywords.extend( initials )
        keywords.extend( extra_keywords )
        return InputData(
            name = launcher,
            mode = Mode( keyword = "EP_SAD" ),
            data = data.data_ep,
            keywords = keywords,
            reporters = (
                test_reporter.ep_dataset_statistics,
                test_reporter.ep_phasing_statistics,
#               test_reporter.ep_phasing_file,
                test_reporter.sad_specific_statistics,
                ),
            resfile = results_file( launcher = launcher ),
            disable_sections = disable_sections,
            disable_tests_in = disable_tests_in,
            )

    @classmethod
    def ep_auto_function(
        cls,
        launcher,
        data,
        initials,
        extra_keywords = [],
        disable_sections = [],
        disable_tests_in = {},
        ):

        keywords = []
        keywords.extend( data.components )
        keywords.append( data.wavelength )
        keywords.extend( initials )
        keywords.extend( extra_keywords )
        return InputData(
            name = launcher,
            mode = Mode( keyword = "EP_AUTO" ),
            data = data.data_ep,
            keywords = keywords,
            reporters = (
                test_reporter.ep_dataset_statistics,
                test_reporter.ep_phasing_statistics,
#               test_reporter.ep_phasing_file,
                ),
            resfile = results_file( launcher = launcher ),
            disable_sections = disable_sections,
            disable_tests_in = disable_tests_in,
            )

    @classmethod
    def mr_atom_function(
        cls,
        launcher,
        data,
        searches,
        perform_pruning_check = False,
        extra_keywords = [],
        disable_sections = [],
        disable_tests_in = {},
        ):

        keywords = []
        keywords.extend( data.components )
        keywords.extend( data.ensembles )
        keywords.extend( searches )
        keywords.extend( extra_keywords )
        reporters = (
            test_reporter.ep_dataset_statistics,
            test_reporter.ep_phasing_statistics,
            #test_reporter.symmetry_information,
            #test_reporter.solutions,
            #test_reporter.vrms_refinement,
            #test_reporter.auto_pak_rnp_function,
            )

        return InputData(
            name = launcher,
            mode = Mode( keyword = "MR_ATOM" ),
            data = data.data_mr,
            keywords = keywords,
            reporters = reporters,
            resfile = results_file( launcher = launcher ),
            disable_sections = disable_sections,
            disable_tests_in = disable_tests_in,
            )

    @classmethod
    def sceds(
        cls,
        launcher,
        data,
        search,
        extra_keywords = [],
        disable_sections = [],
        disable_tests_in = {},
        ):

        keywords = [ ]
        keywords.extend( data.ensembles )
        keywords.extend( extra_keywords )
        return InputData(
            name = launcher,
            mode = Mode( keyword = "SCEDS" ),
            data = data.data_mr,
            keywords = keywords,
            reporters = (
                test_reporter.sceds_score,
                ),
            resfile = results_file( launcher = launcher ),
            disable_sections = disable_sections,
            disable_tests_in = disable_tests_in,
            )

# Helper functions
def results_file(launcher):

    import os.path
    ( root, ext ) = os.path.splitext( launcher )

    return "%s" % root
