class Comparison(object):
    """
    Comparison of two results
    """

    def __init__(self, left, leftrefstr, right, rightrefstr, method):

        self.left = left
        self.right = right
        self.leftrefstr = leftrefstr
        self.rightrefstr = rightrefstr

        if ( self.left.__class__ == self.right.__class__ ):
            ( self.equals, self.reason ) = getattr( left, method )( other = right )

        else:
            self.equals = False
            self.reason = "different type"


    def __str__(self):
        """
        if hasattr(self.left, "values"):
            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
            mstr = ""
            for i,v in enumerate(self.left.values):
                mstr += str(v) + " \t " + str(self.right.values[i]) + "\n"
            return mstr
        """
        return "%s%s %s%s%s%s" % (
            self.leftrefstr,
            self.left,
            "==\n" if self.equals else "!=\n",
            self.rightrefstr,
            self.right,
            "" if self.equals else " <%s>" % self.reason
            )


class Integer(object):
    """
    Result as integer
    """

    def __init__(self, value):

        self.value = value


    def equality(self, other):

        if self.value == other.value:
            return ( True, None )

        else:
            return ( False, "%s different from %s" %(self.value, other.value) )


    strong_equality = equality
    weak_equality = equality

    def html(self):

        return str( self )


    def __str__(self):

        return "%s(value = %s)" % ( self.__class__.__name__, self.value )


class Text(Integer):
    """
    Text result
    """
    def equality(self, other):

        if self.value == other.value:
            return ( True, None )

        else:
            return ( False, "\n%s\ndifferent from\n%s" %(self.value, other.value) )

    strong_equality = equality
    weak_equality = equality



class Float(object):
    """
    Result as float
    """

    def __init__(self, value):

        self.value = value


    def equality(self, other, tolerance):
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        if abs( self.value - other.value ) <= tolerance:
            return ( True, None )

        else:
            return ( False, "%s outside tolerance (%s) of %s" % (self.value, tolerance, other.value) )


    def weak_equality(self, other):

        return self.equality( other = other, tolerance = self.weak )


    def strong_equality(self, other):

        return self.equality( other = other, tolerance = self.strong )


    def html(self):

        return str( self )


    def __str__(self):

        return "%s(value = %s)" % (
            self.__class__.__name__,
            self.format % self.value,
            )


class RelativeFloat(Float):
    """
    Result with a tolerance scaled to the target value
    """

    def equality(self, other, tolerance):

        abstol = abs(tolerance * self.value)

        if abs( self.value - other.value ) <= abstol:
            return ( True, None )

        else:
            return (
                False,
                "%s outside tolerance (%s%%) of %s" % (self.value, self.format % ( tolerance * 100 ), other.value ),
                )


class List(object):
    """
    A list of values
    """

    def __init__(self, values):

        self.values = values


    def equality(self, other, method):
        if len( self.values ) != len( other.values ):
            return ( False, "unequal number of elements" )

        for ( index, ( v1, v2 ) ) in enumerate( zip( self.values, other.values ) ):
            if v1.__class__ != v2.__class__:
                return ( False, "element %d: different type" % index )

            comp = getattr( v1, method )( other = v2 )

            if not comp[0]:
                #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                return ( False, "element %d: %s" % ( index, comp[1] ) )

        return ( True, None )


    def weak_equality(self, other):

        return self.equality( other = other, method = "weak_equality" )


    def strong_equality(self, other):

        return self.equality( other = other, method = "strong_equality" )


    def html(self):

        return "<table>\n%s\n</table>" % "\n".join(
            "  <tr><td>%s</td></tr>" % v.html() for v in self.values
            )



    def __str__(self):

        if len( self.values ) < 11:
            return "%s( [ %s ] )" % (
                self.__class__.__name__,
                ", ".join( str( v ) for v in self.values ),
                )

        else:
            return "%s( [ %s ... <%s elements not shown> ] )" % (
                self.__class__.__name__,
                ", ".join( str( v ) for v in self.values[:10] ),
                len( self.values ) - 10,
                )


class CellAxis(Float):
    """
    Unit cell axis length
    """

    strong = 1.0E-2
    weak = strong
    format = "%.3f"


class CellAngle(Float):
    """
    Unit cell angle
    """

    strong = 1.0E-2
    weak = strong
    format = "%.2f"


class Resolution(Float):
    """
    Resolution
    """

    strong = 1.0E-2
    weak = strong
    format = "%.2f A"


class SpaceGroup(Integer):
    """
    Space group
    """

    def __init__(self, number, symbol):

        super( SpaceGroup, self ).__init__( value = number )
        self.symbol = symbol


    def sgtbx_info(self):

        from cctbx import sgtbx
        return sgtbx.space_group_info( number = self.value )


    def __str__(self):

        return "%s(number = %d, HM-symbol = %s)" % (
            self.__class__.__name__,
            self.value,
            self.symbol
            )


    @classmethod
    def from_number(cls, number):

        from cctbx import sgtbx

        try:
            sgtype = sgtbx.space_group_info( number = number ).type()

        except RuntimeError as e:
            value = 0
            symbol = "UNKNOWN"

        else:
            value = sgtype.number()
            symbol = sgtype.universal_hermann_mauguin_symbol()

        return cls( number = value, symbol = symbol )


    @classmethod
    def from_symbol(cls, value):

        from cctbx import sgtbx

        try:
            sgtype = sgtbx.space_group( value ).type()

        except RuntimeError as e:
            number = 0
            symbol = "UNKNOWN"

        else:
            number = sgtype.number()
            symbol = sgtype.universal_hermann_mauguin_symbol()

        return cls( number = number, symbol = symbol )


class WilsonB(Float):
    """
    Wilson B-factor
    """

    strong = 1.0E-4
    weak = 1.0E-4
    format = "%.6f"


class WilsonK(Float):
    """
    Wilson K-value
    """

    strong = 1.0E-3
    weak = 1.0E-3
    format = "%.5f"


class AnisotropyPrincipalComponent(Float):
    """
    Anisotropy principal component
    """

    strong = 1.0E-3
    weak = 1.0E-3
    format = "%.5f"


class ZScore(Float):
    """
    ZScore
    """

    strong = 1.0E-2
    weak = 1.0E-2
    format = "%.4f"


class Score(Float):
    """
    Score
    """

    strong = 1.0E-1
    weak = 1.0E-1
    format = "%.3f"


class RFScore(Float):
    """
    Rotation function score
    """

    strong = 1.0E-1
    weak = 1.0E-1
    format = "%.3f"


class TFScore(Float):
    """
    Translation function score
    """

    strong = 1.0E-1
    weak = 2.0E-1
    format = "%.3f"


class LLGScore(Float):
    """
    LLG score
    """

    strong = 1.0
    weak = 1.0
    format = "%.2f"


class Bfactor(Float):
    """
    B-factor
    """

    strong = 1.0
    weak = 3.0
    format = "%.3f"


class VRMS(Float):
    """
    VRMS
    """

    strong = 5.0E-2
    weak = 5.0E-2
    format = "%.3f"


class GyreAngle(Float):
    """
    Gyre angle
    """

    strong = 1.0
    weak = 1.0
    format = "%.2f"


class GyreTrans(Float):
    """
    Gyre translation
    """

    strong = 1.0E-2
    weak = strong
    format = "%.3f"


class PruneFraction(Float):
    """
    Pruning fraction
    """

    strong = 5.0E-2
    weak = 5.0E-2
    format = "%.3f"


class RotationPeak(object):
    """
    Rotation peak
    """

    dmin = 2.5

    strong = 1.0
    weak = 1.0

    def __init__(self, ensemble, space_group_hall, cell, angles):

        self.ensemble = ensemble
        self.space_group_hall = space_group_hall
        self.cell = cell
        self.angles = angles


    def unit_cell(self):

        return List(
            [
                CellAxis( self.cell[0] ),
                CellAxis( self.cell[1] ),
                CellAxis( self.cell[2] ),
                CellAngle( self.cell[3] ),
                CellAngle( self.cell[4] ),
                CellAngle( self.cell[5] ),
                ]
            )


    def space_group(self):

        return SpaceGroup.from_symbol( value = self.space_group_hall )


    def resolution(self):

        return Resolution( value = self.dmin )


    def crystal_info(self):

        from phaser import template_new as template
        from cctbx import uctbx
        return template.CrystalInfo(
            space_group = self.space_group().sgtbx_info().group(),
            cell = uctbx.unit_cell( parameters = self.cell ),
            dmin = self.dmin
            )


    def ensemble_info(self):

        return self.ensemble.match_info()


    def match_tolerance(self):

        from phaser import template_new as template
        return template.MatchTolerance.from_ensemble(
            crystal = self.crystal_info(),
            ensemble = self.ensemble_info()
            )


    def rotation(self):

        import scitbx.matrix
        import scitbx.math
        return scitbx.matrix.sqr(
            scitbx.math.euler_angles_zyz_matrix( *self.angles )
            )


    def equality(self, other, multiplier):

        if self.ensemble != other.ensemble:
            import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
            return ( False, "different ensemble: %s" % self.ensemble.uneqstr )

        ( equals, message ) = self.unit_cell().weak_equality(
            other = other.unit_cell()
            )

        if not equals:
            return ( False, "different cell" )

        ( equals, message ) = self.space_group().equality(
            other = other.space_group()
            )


        if not equals:
            print (self.space_group())
            print (other.space_group())
            return ( False, "different space group" )

        ( equals, message ) = self.resolution().weak_equality(
            other = other.resolution()
            )

        if not equals:
            return ( False, "different resolution" )

        try:
            ensemble_info = self.ensemble_info()
            tolerance = multiplier * self.match_tolerance()
        except Exception: # in case EnsemblePDBFile.file_name is valid only on another PC
            ensemble_info = other.ensemble_info()
            tolerance = multiplier * other.match_tolerance()

        crystal_info = self.crystal_info()
        left_r = self.rotation()
        right_r = other.rotation()
        from phaser import template_new as template

        for orth_rotation in crystal_info.orth_rotations:
            for ( pgop, difference ) in template.rotation_differences_between(
                left = left_r,
                right = orth_rotation * right_r,
                ensemble = ensemble_info
                ):
                if difference < tolerance.rotation:
                    return ( True, None )

        return ( False, "Non-matching angles" )


    def weak_equality(self, other):

        return self.equality( other = other, multiplier = self.weak )


    def strong_equality(self, other):

        return self.equality( other = other, multiplier = self.strong )


    def html(self):

        return "<pre>%s(\n  ensemble = %s,\n  rotation = ( %.2f, %.2f, %.2f )\n  )</pre>" % (
            self.__class__.__name__,
            self.ensemble.name,
            self.angles[0],
            self.angles[1],
            self.angles[2],
            )


    def __str__(self):

        return "%s(ensemble = %s,rotation = ( %.2f, %.2f, %.2f ))" % (
            self.__class__.__name__,
            self.ensemble.name,
            self.angles[0],
            self.angles[1],
            self.angles[2],
            )


class Solution(object):
    """
    Translation peak
    """

    strong = 1.0
    weak = 1.0

    dmin = 2.5

    def __init__(self, space_group_hall, cell, peaks):

        self.space_group_hall = space_group_hall
        self.cell = cell
        self.peaks = peaks


    def unit_cell(self):

        return List(
            [
                CellAxis( self.cell[0] ),
                CellAxis( self.cell[1] ),
                CellAxis( self.cell[2] ),
                CellAngle( self.cell[3] ),
                CellAngle( self.cell[4] ),
                CellAngle( self.cell[5] ),
                ]
            )


    def space_group(self):

        return SpaceGroup.from_symbol( value = self.space_group_hall )


    def resolution(self):

        return Resolution( value = self.dmin )


    def crystal_info(self):

        from phaser import template_new as template
        from cctbx import uctbx
        return template.CrystalInfo(
            space_group = self.space_group().sgtbx_info().group(),
            cell = uctbx.unit_cell( parameters = self.cell ),
            dmin = self.dmin
            )


    def ensembles(self):

        return [ e for ( e, r, t ) in self.peaks ]


    def parameters(self):

        import scitbx.matrix
        import scitbx.math
        return [
            ( scitbx.matrix.sqr( scitbx.math.euler_angles_zyz_matrix( *r ) ), t )
            for ( e, r, t ) in self.peaks
            ]


    def rotations(self):

        return [ r for ( e, r, t ) in self.peaks ]


    def translations(self):

        return [ t for ( e, r, t ) in self.peaks ]


    def ensemble_infos(self):

        return [ e.match_info() for ( e, r, t ) in self.peaks ]


    def weak_equality(self, other):

        return self.equality( other = other, tolerance = self.weak )


    def strong_equality(self, other):

        return self.equality( other = other, tolerance = self.strong )


    @staticmethod
    def peak_parameters(cell, ensemble_infos, peak_parameters):

        assert len( ensemble_infos ) == len( peak_parameters )
        from phaser import template_new as template
        return [
            template.PeakInfo.from_phaser_solution(
                rotation = r,
                translation = t,
                ensemble = ei,
                cell = cell
                )
            for ( ei, ( r, t ) )
            in zip( ensemble_infos, peak_parameters ) if ei
            ]


    def equality(self, other, tolerance):

        ( equals, message ) = self.unit_cell().weak_equality(
            other = other.unit_cell()
            )

        if not equals:
            return ( False, message )

        ( equals, message ) = self.space_group().equality(
            other = other.space_group()
            )

        if not equals:
           print( "Expected: %s" % self.space_group())
           print( "Got: %s" % other.space_group())

           return ( False, message )

        ( equals, message ) = self.resolution().weak_equality(
            other = other.resolution()
            )

        if not equals:
            return ( False, message )

        if len( self.peaks ) != len( other.peaks ):
            return ( False, "different number of peaks" )

        for ( index, ( l_ei, r_ei ) ) in enumerate(
            zip( self.ensembles(), other.ensembles() )
            ):
            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
            if l_ei != r_ei:
                return ( False, "%d: different ensemble, %s" % (index, l_ei.uneqstr)  )

        try:
            ensinf = self.ensemble_infos()
        except Exception as e: # in case EnsemblePDBFile.file_name is valid only on another PC
            ensinf = other.ensemble_infos()

        crystal_info = self.crystal_info()
        left = self.peak_parameters(
            cell = crystal_info.cell,
            ensemble_infos = ensinf,
            peak_parameters = self.parameters()
            )
        right = self.peak_parameters(
            cell = crystal_info.cell,
            ensemble_infos = ensinf,
            peak_parameters = other.parameters()
            )

        # in case of comparing helix ensembles left and right will be empty
        if not left and not right:
            return ( True, None )

        from phaser import template_new as template
        if template.is_symmetry_equivalent(
            left = left,
            right = right,
            crystal = crystal_info,
            origins = template.plausible_origin_shifts(
                left = left,
                right = right,
                crystal = crystal_info,
                multiplier = 1.0
                ),
            ):
            return ( True, None )

        return ( False, "Non-matching peaks" )


    def html(self):

        return "%s(<br />%s<br />)" % (
            self.__class__.__name__,
            ",<br />".join(
                "<pre>  Molecule(\n%s,\n%s,\n%s\n    )</pre>" % (
                    "    ensemble = %s" % e.name,
                    "    rotation = ( %.2f, %.2f, %.2f )" % r,
                    "    translation = ( %.5f, %.5f, %.5f )" % t,
                    )
                for ( e, r, t ) in self.peaks
                ),
            )


    def __str__(self):

        return "%s(%s)" % (
            self.__class__.__name__,
            ", ".join(
                "Molecule(%s, %s, %s, %s)" % (
                    "space_group = '%s'" % self.space_group().symbol,
                    "ensemble = %s" % e.name,
                    "rotation = ( %.2f, %.2f, %.2f )" % r,
                    "translation = ( %.5f, %.5f, %.5f )" % t,
                    )
                for ( e, r, t ) in self.peaks
                ),
            )


class SAD_LLG(Float):
    """
    Phaser SAD LLG
    """

    strong = 2.0
    weak = 2.0
    format = "%.1f"


class ScatteringFactor(Float):
    """
    SAD f' and f"
    """

    strong = 5.0E-2
    weak = 5.0E-2
    format = "%.2f"


class Occupancy(Float):
    """
    Atom occupancy
    """

    strong = 1.0E-1
    weak = 1.0E-1
    format = "%.3f"


class Uiso(Float):
    """
    Atom Uiso
    """

    strong = 5.0E-2
    weak = 5.0E-2
    format = "%.3f"


class Atom(object):
    """
    Atom description
    """

    COORDINATE = 1E-4

    def __init__(self, element, coordinates, occupancy, uiso):

        self.element = element
        self.coordinates = coordinates
        self.occupancy = value = occupancy
        self.uiso = uiso


    def __str__(self):

        return "Atom '%s'(%s, %s, %s)" % (
            self.element,
            "coords = ( %.5f, %.5f, %.5f )" % self.coordinates,
            "occ = %.3f" % self.occupancy,
            "uiso = %.3f" % self.uiso,
            )


class AtomSet(object):
    """
    A set of atoms
    """

    strong = 1E-2
    weak = 1E-2

    def __init__(self, atoms, cell, space_group):

        self.atoms = atoms
        self.space_group = space_group
        self.cell = cell


    def crystal_info(self):

        from phaser import matching
        from cctbx import uctbx
        from cctbx import sgtbx
        return matching.CrystalInfo(
            space_group = sgtbx.space_group_info( symbol = self.space_group ).group(),
            cell = uctbx.unit_cell( parameters = self.cell ),
            )


    def strong_equality(self, other):

        return self.equality( other = other, type = "strong" )


    def weak_equality(self, other):

        return self.equality( other = other, type = "weak" )


    def equality(self, other, type):

        from phaser import matching
        import scitbx.matrix

        elements = set(
            [ a.element for a in self.atoms ] + [ a.element for a in other.atoms ]
            )
        crystal = self.crystal_info()
        tolerance = getattr( self, type )

        for elem in elements:
            elem_self = [ a for a in self.atoms if a.element == elem ]
            elem_other = [ a for a in other.atoms if a.element == elem ]

            if len( elem_self ) != len( elem_other ):
                return ( False, "different number of atoms for element '%s'" % elem )

            pi_self = [
                matching.Position.new(
                    centre = scitbx.matrix.col( a.coordinates ),
                    crystal = crystal,
                    tolerance = tolerance,
                    detail = matching.cached_position_differences
                    )
                for a in elem_self
                ]
            pi_other = [
                matching.Position.new(
                    centre = scitbx.matrix.col( a.coordinates ),
                    crystal = crystal,
                    tolerance = tolerance
                    )
                for a in elem_other
                ]

            atom_for_self = dict( zip( pi_self, elem_self ) )
            atom_for_other = dict( zip( pi_other, elem_other ) )

            pairs = matching.overlap( left = pi_self, right = pi_other )

            for ( match_self, match_other ) in pairs:
                if match_self is None or match_other is None:
                    continue
                    """
                    return (
                        False,
                        "unpaired atom (site_mismatch): %s vs %s" % (
                            atom_for_self[ match_self ] if match_self else None,
                            atom_for_other[ match_other ] if match_other else None,
                            )
                        )
                    """

                occ_self = Occupancy( value = atom_for_self[ match_self ].occupancy )
                occ_other = Occupancy( value = atom_for_other[ match_other ].occupancy )

                ( equals, message ) = occ_self.equality(
                    other = occ_other,
                    tolerance = getattr( Occupancy, type )
                    )

                if not equals:
                    return (
                        False,
                        "atom pair (occupancy mismatch): %s vs %s" % (
                            atom_for_self[ match_self ],
                            atom_for_other[ match_other ],
                            )
                        )

                uiso_self = Uiso( value = atom_for_self[ match_self ].uiso )
                uiso_other = Uiso( value = atom_for_other[ match_other ].uiso )

                ( equals, message ) = uiso_self.equality(
                    other = uiso_other,
                    tolerance = getattr( Uiso, type )
                    )

                if not equals:
                    return (
                        False,
                        "atom pair (uiso mismatch): %s vs %s" % (
                            atom_for_self[ match_self ],
                            atom_for_other[ match_other ],
                            )
                        )

        return ( True, None )


    def html(self):

        return "%s(<br />%s<br />)" % (
            self.__class__.__name__,
            ",<br />".join(
                "<pre>  Atom <b>%s</b>(\n%s,\n%s,\n%s\n    )</pre>" % (
                    a.element,
                    "    coords = ( %.5f, %.5f, %.5f )" % a.coordinates,
                    "    occ = %.3f" % a.occupancy,
                    "    uiso = %.4f" % a.uiso,
                    )
                for a in self.atoms
                ),
            )


    def __str__(self):

        return "%s(%s)" % (
            self.__class__.__name__,
            ", ".join( str( a ) for a in self.atoms ),
            )
