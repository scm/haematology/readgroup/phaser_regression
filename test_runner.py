import argparse
import operator
from io import StringIO
import platform
import datetime


if platform.sys.version_info[0] > 2:
    suffix = '.py3result'
else:
    suffix = '.py2result'



def positive_int(string):

    value = int( string )

    if value < 0:
        raise argparse.ArgumentTypeError( "Positive integer needed" )

    return value


def get_engines():

    return [
        "all",
        "python",
        "script"
        ]

def get_known_engines():

    from phaser_regression import test_run_engine
    return {
        "python": test_run_engine.python_safe,
        "script": test_run_engine.script,
        }

def get_script_engines():

    from phaser_regression import test_run_engine
    return {
        "script": test_run_engine.script,
        }

def get_python_engines():

    from phaser_regression import test_run_engine
    return {
        "python": test_run_engine.python_safe,
        #"python": test_run_engine.python_basic, # enable to get output when debugging python script
        }

def run_test(input, args):

    parser = argparse.ArgumentParser( description = "Run test" )

    mode_handler_for = {
        "update": update_expected_results,
        "display": display_expected_results,
        "test": test_and_compare_expected_results,
        }

    parser.add_argument(
        "mode",
        action = "store",
        choices = mode_handler_for.keys(),
        help = "Run mode"
        )
    parser.add_argument(
        "-e", "--engine",
        action = "store",
        choices = get_known_engines().keys(),
        default = sorted( get_known_engines() )[0],
        help = "Execution mode"
        )
    parser.add_argument( "-n", "--ncpus", type = positive_int, default = 1 )
    parser.add_argument( "-s", "--suffix",
                        action = "store",
                        help = "Suffix on results file. Default is '%s'" %suffix,
                        type = str,
                        default = "%s" %suffix
                       )

    # Parse arguments
    params = parser.parse_args( args )

    import sys
    stream = sys.stdout
    stream.write( "## Starting test runner with: %s ##\n" % input.name )
    stream.write( "Mode: %s\n" % params.mode )

    mode_handler_for[ params.mode ](
        input = input,
        stream = stream,
        params = params
        )

    stream.write( "## Test runner finished with: %s ##\n" % input.name )


def execute_test(input, engine, stream, ncpus, resfilesuffix):

    import tempfile
    folder = tempfile.mkdtemp()
    stream.write( "Temporary folder: %s\n" % folder )

    try:
        ( report, runtime ) = engine(
            input = input,
            stream = stream,
            tmp_dir = folder,
            ncpus = ncpus,
            resfilesuffix = resfilesuffix,
            )

    finally:
        stream.write( "Temporary folder contents:\n" )
        stream.write( "%s:\n" % folder )

        import os
        stream.write(
            "%s\n" % "\n".join( "\t%s" % fname for fname in os.listdir( folder ) )
            )
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )

        stream.write( "Cleaning up temporary folder..." )

        import shutil
        shutil.rmtree( folder, ignore_errors = True )

        stream.write( "done\n" )

    stream.write( "Runtime: %s s\n" % runtime )

    return ( report , runtime )


def update_expected_results(input, stream, params):

    from phaser_regression import test_run_engine
    resfilesuffix = params.suffix
    ( result, runtime ) = execute_test(
        input = input,
        engine = test_run_engine.python_basic,
        stream = stream,
        ncpus = params.ncpus,
        resfilesuffix = resfilesuffix
        )
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    stream.write( "Saving expected results to %s\n" % (input.resfile + resfilesuffix) )
    result["Version"] = { "Info": test_run_engine.get_phaser_version() }
    import pickle
    outfile = open( input.resfile + resfilesuffix, "w+b" )
    pickle.dump( result, outfile )
    outfile.close()


def display_expected_results(input, stream, params):

    import pickle
    resfilesuffix = params.suffix
    stream.write( "Reference file: %s\n" % (input.resfile + resfilesuffix ) )
    if platform.sys.version_info[0] > 2:
        report = pickle.load( open( input.resfile + resfilesuffix, "r+b" ) )
    else:
        report = pickle.load( open( input.resfile + resfilesuffix, "r" ) )
    from phaser_regression import test_reporter
    test_reporter.printout( report = report, stream = stream )


def run_test_and_compare(input, stream, engine, ncpus, resfilesuffix):

    ( result, runtime ) = execute_test(
        input = input,
        engine = engine,
        stream = stream,
        ncpus = ncpus,
        resfilesuffix = resfilesuffix,
        )

    from phaser_regression import test_run_engine
    resversion = test_run_engine.get_phaser_version()

    import pickle
    resfname = input.resfile + resfilesuffix
    if platform.sys.version_info[0] > 2:
        p = pickle.load( open( resfname, "r+b" ) )
    else:
        p = pickle.load( open( resfname, "r" ) )
    import os.path
    import datetime
    modtime = os.path.getmtime( resfname )
    basefname = os.path.basename(resfname)
    if not p.get("Version"): # post-Gabor we now store version info in the pickle file
      p["Version"] = { "Info": str( datetime.datetime.fromtimestamp( modtime ) ) }

    from phaser_regression import test_reporter
    comp = test_reporter.Comparison(
        left = p,
        leftversion = "Previous version: " + p["Version"]["Info"].decode("utf-8"),
        right = result,
        rightversion = "This version:  " + resversion.decode("utf-8"),
        method = "weak_equality",
        disabled_sections = input.disable_sections,
        disabled_tests_in = input.disable_tests_in,
        )

    return ( comp, runtime )


def test_and_compare_expected_results(input, stream, params):

    error = False

    try:
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        resfilesuffix = params.suffix
        ( comp, runtime ) = run_test_and_compare(
            input = input,
            stream = stream,
            engine = get_known_engines()[ params.engine ],
            ncpus = params.ncpus,
            resfilesuffix = resfilesuffix,
            )

        stream.write( "Reference file: %s\n" % (input.resfile + resfilesuffix) )
        stream.write( "%s\n" % comp )
        stream.write( "%s\n" % comp.get_text_summary() )

        if 0 < comp.failures() or 0 < comp.unknowns():
            error = True

    except Exception as e:
        import traceback
        stream.write( "Full traceback:\n%s" % traceback.format_exc() )
        error = True

    stream.write( "ERROR\n" if error else "OK\n" )


def verbosity_full(output, comparison, stream):

    stream.write( "Output:\n%s\n\n" % output )
    stream.write( "%s\n\n" % comparison )
    stream.write( "%s\n\n" % comparison.get_text_summary() )


def verbosity_normal(output, comparison, stream):

    stream.write( "%s\n\n" % comparison )
    stream.write( "%s\n\n" % comparison.get_text_summary() )


def verbosity_terse(output, comparison, stream):

    stream.write( "%s\n\n" % comparison.get_text_summary() )


def verbosity_silent(output, comparison, stream):

    pass




class TextOutput(object):
    """
    Text output
    """

    def __init__(self, filter, stream, fstream, suite):

        self.filter = filter
        self.stream = stream
        self.fstream = fstream
        self.suite = suite
        self.summary = []
        self.current = None

        self.stream.write( "## Running %s ##\nPlatform: %s\n" % (self.suite, platform.platform()))
        self.stream.write( "Date: %s\n\n" % str(datetime.datetime.now()) )
        self.fstream.write( "## Running %s## \nPlatform: %s\n" % (self.suite, platform.platform()))
        self.fstream.write( "Date: %s\n\n" % str(datetime.datetime.now()) )


    def heading(self, name, engine):

        assert self.current is None

        formatted = "Test %d: %s - %s" % ( len( self.summary ) + 1, name, engine )
        separator = "=" * len( formatted )
        self.stream.write( "\n".join( [ separator, formatted, separator ] ) )
        self.stream.write( "\n\n" )
        self.fstream.write( "\n".join( [ separator, formatted, separator ] ) )
        self.fstream.write( "\n\n" )
        self.current = ( name, engine )
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )



    def add_error_entry(self, full, short, output = None, traceback = None):
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        assert self.current is not None
        # don't dump full phaser output if we are terse
        # workaround for checking if self.filter is verbosity_terse() or verbosity_failed()
        if output and "verbosity_terse" not in str(self.filter) and "verbosity_failed" not in str(self.filter):
            self.stream.write( "Output:\n%s\n" % output )
            self.fstream.write( "Output:\n%s\n" % output )

        if traceback:
            self.stream.write( "Full traceback:\n%s\n" % traceback )
            self.fstream.write( "Full traceback:\n%s\n" % traceback )

        self.stream.write( "ERROR: %s\n\n" % full )
        self.fstream.write( "ERROR: %s\n\n" % full )
        self.summary.append( ( "%s - %s" % self.current, False, short, 0 ) )
        self.current = None


    def add_valid_entry(self, output, comparison, passed, runtime):

        assert self.current is not None
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )

        self.filter( self, output = output, comparison = comparison )
        self.stream.write("OK\n\n" if passed else "ERROR: incorrect results\n\n" )
        self.fstream.write("OK\n\n" if passed else "ERROR: incorrect results\n\n" )
        self.summary.append(
            (
                "%s - %s" % self.current,
                passed,
                "" if passed else "%d failures, %d unknowns" % (
                    comparison.failures(),
                    comparison.unknowns(),
                    ),
                runtime,
                )
            )
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        self.current = None


    def status(self):

        return all( p for ( i, p, m, r ) in self.summary )


    def finalize(self):

        assert self.current is None

        for s in [self.stream, self.fstream]:
            s.write( "SUMMARY of tests:\n" )
            data = enumerate( self.summary, start = 1 )

            for ( index, ( identifier, passed, message, runtime ) ) in data:
                s.write(
                    "%d: %s - %s\n" % (
                        index,
                        identifier,
                        "OK" if passed else "ERROR (%s)" % message,
                        )
                    )

            s.write( "\n" )
            s.write(
                "All tests status: %s\n" % ( "OK" if self.status() else "ERROR" )
                )
            s.write( "\n" )
            s.write( "## Test suite %s finished ##\n" % self.suite )
            s.close()



    def verbosity_full(self, output, comparison):

        self.stream.write( "Output:\n%s\n\n" % output )
        self.stream.write( "%s\n\n" % comparison )
        self.stream.write( "%s\n\n" % comparison.get_text_summary() )
        self.fstream.write( "Output:\n%s\n\n" % output )
        self.fstream.write( "%s\n\n" % comparison )
        self.fstream.write( "%s\n\n" % comparison.get_text_summary() )


    def verbosity_normal(self, output, comparison):

        self.stream.write( "%s\n\n" % comparison )
        self.stream.write( "%s\n\n" % comparison.get_text_summary() )
        self.fstream.write( "%s\n\n" % comparison )
        self.fstream.write( "%s\n\n" % comparison.get_text_summary() )


    def verbosity_terse(self, output, comparison):

        self.stream.write( "%s\n\n" % comparison.get_text_summary() )
        self.fstream.write( "%s\n\n" % comparison )
        self.fstream.write( "%s\n\n" % comparison.get_text_summary() )


    def verbosity_failed(self, output, comparison):
        self.stream.write( "%s\n\n" % comparison.get_failed_summary() )
        self.fstream.write( "%s\n\n" % comparison )
        self.fstream.write( "%s\n\n" % comparison.get_text_summary() )


    def verbosity_silent(self, output, comparison):

        pass
        self.fstream.write( "%s\n\n" % comparison )
        self.fstream.write( "%s\n\n" % comparison.get_text_summary() )


    @classmethod
    def verbose(cls, stream, fstream, suite):

        return cls( filter = cls.verbosity_full, stream = stream, fstream = fstream, suite = suite )


    @classmethod
    def normal(cls, stream, fstream, suite):

        return cls( filter = cls.verbosity_normal, stream = stream, fstream = fstream, suite = suite )


    @classmethod
    def terse(cls, stream, fstream, suite):

        return cls( filter = cls.verbosity_terse, stream = stream, fstream = fstream, suite = suite )


    @classmethod
    def terse_failed(cls, stream, fstream, suite):

        return cls( filter = cls.verbosity_failed, stream = stream, fstream = fstream, suite = suite )


    @classmethod
    def silent(cls, stream, fstream, suite):

        return cls( filter = cls.verbosity_silent, stream = stream, fstream = fstream, suite = suite )


class HTMLOutput(object):
    """
    HTML output
    """

    def __init__(self, stream, suite):

        self.stream = stream
        self.summary = []
        self.suite = suite
        self.current = None
        self.strstream = StringIO()


        self.stream.write( "<html>\n" )
        self.stream.write( "<body>\n" )
        self.stream.write( "<a name = \"title\" />" )
        self.stream.write(
            "<h1>Running test suite <span style = \"color:blue\" >%s</span></h1>\n" % self.suite
            )
        self.stream.write( "<h3>Date: %s, " % str(datetime.datetime.now()) )
        self.stream.write( " Platform: %s</h3>" % platform.platform() )

    def heading(self, name, engine):

        assert self.current is None
        identifier = "%s_%s" % ( name, engine )
        formatted = "Test %d: %s - %s" % (
            len( self.summary ) + 1,
            name,
            engine
            )
        self.strstream.write(
            u"<a name = \"%s\"/><h2>%s</h2>\n" % ( identifier, formatted )
            )
        self.current = ( name, engine, identifier )



    def add_error_entry(self, full, short, output = None, traceback = None):

        assert self.current is not None
        identifier = self.current[2]
        self.strstream.write( u"<b style = \"color:red\" >ERROR</b>\n" )
        self.strstream.write( u"<p>Jump to " )

        if output:
            self.strstream.write(
                u"<a href=\"#%s_output_error\">output</a>, " % identifier
                )

        if traceback:
            self.strstream.write(
                u"<a href=\"#%s_traceback\">traceback</a> or " % identifier
                )

        self.strstream.write(
            u"<a href=\"#%s_message\">message</a></p>\n" % identifier
            )

        if output:
            self.strstream.write(
                u"<a name = \"%s_output_error\"/ ><h3>Output:</h3>\n" % identifier
                )
            self.strstream.write( u"<pre>\n%s\n</pre>\n" % output )
            self.strstream.write(
                u"<p>Back to <a href=\"#%s\">test</a></p>" % identifier
                )

        if traceback:
            self.strstream.write(
                u"<a name = \"%s_traceback\"/ ><h3>Full traceback:</h3>\n" % identifier
                )
            self.strstream.write( u"<pre>%s\n</pre>\n" % traceback )
            self.strstream.write(
                u"<p>Back to <a href=\"#%s\">test</a></p>" % identifier
                )

        self.strstream.write( u"<a name = \"%s_message\"/ >" % identifier )
        self.strstream.write( u"<b style = \"color:red\" >ERROR: %s</b>\n" % full )
        self.summary.append( ( self.current, False, short, 0 ) )
        self.current = None


    def add_valid_entry(self, output, comparison, passed, runtime):

        assert self.current is not None
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        identifier = self.current[2]
        self.strstream.write(
            u"<p>Jump to "
            + "<a href=\"#%s_output\">output</a>, " % identifier
            + "<a href=\"#%s_comparison\">comparison</a>, " % identifier
            + "<a href=\"#%s_summary\">summary</a> or " % identifier
            + "<a href=\"#overview\">Overview of tests</a></p>\n"
            )
        self.strstream.write(
            u"<a name = \"%s_output\"/><h3>Output:</h3>\n" % identifier
            )
        self.strstream.write( u"<pre>\n%s\n</pre>\n" % output )
        self.strstream.write(
            u"<p>Back to <a href=\"#%s\">test</a></p>" % identifier
            )
        self.strstream.write(
            u"<a name = \"%s_comparison\"/><h3>Comparison:</h3>\n" % identifier
            )
        self.strstream.write( u"<h4>" + comparison.leftversion + "</h4>" + \
                          "<h4>" + comparison.rightversion  + "</h4>")
        self.strstream.write( u"<p>Sections:</p>\n" )
        self.strstream.write( u"<ul>\n" )

        tables = (
          list( comparison.evaluation_for )
          + [ h for ( h, r ) in comparison.left_only ]
          + [ h for ( h, r ) in comparison.right_only ]
          + list( comparison.disabled_sections )
          )

        for ( index, heading ) in enumerate( tables ):
            self.strstream.write(
                u"<li><a href = \"#%s_comparison_%d\" />%s</a></li>\n" % ( identifier, index, heading )
                )

        self.strstream.write( u"</ul>\n" )

        for ( index, heading ) in enumerate( comparison.evaluation_for ):
            self.strstream.write(
                u"<a name = \"%s_comparison_%d\" /><h4>%s</h4>\n" % ( identifier, index, heading )
                )

            self.strstream.write( u"<table border = \"1\">\n" )
            self.strstream.write( u"  <tr>\n" )
            self.strstream.write( u"    <th>Name</th>\n" )
            self.strstream.write( u"    <th>Previous version</th>\n" )
            self.strstream.write( u"    <th>This version</th>\n" )
            self.strstream.write( u"    <th>Status</th>\n" )
            self.strstream.write( u"  </tr>\n" )

            result_for = comparison.evaluation_for[ heading ].evaluation_for

            for ( name, result ) in result_for.items():
                self.strstream.write( u"  <tr>\n" )
                self.strstream.write( u"    <td>%s</td>\n" % name )
                self.strstream.write( u"    <td>%s</td>\n" % result.left.html() )
                self.strstream.write( u"    <td>%s</td>\n" % result.right.html() )
                self.strstream.write(
                    u"    <td style = \"background-color:%s;color:white;text-align:center\" >%s</td>\n" % (
                        ( "green", "OK" ) if result.equals else ( "red", result.reason )
                        )
                    )
                self.strstream.write( u"  </tr>\n" )

            for ( name, value ) in comparison.evaluation_for[ heading ].left_only.items():
                self.strstream.write( u"  <tr>\n" )
                self.strstream.write( u"    <td>%s</td>\n" % name )
                self.strstream.write( u"    <td>%s</td>\n" % value.html() )
                self.strstream.write( u"    <td style = \"color: grey\" >NOT FOUND</td>\n" )
                self.strstream.write(
                    u"    <td style = \"background-color:red;color:white;text-align:center\" >MISSING</td>\n"
                    )
                self.strstream.write( u"  </tr>\n" )

            for ( name, value ) in comparison.evaluation_for[ heading ].right_only.items():
                self.strstream.write( u"  <tr>\n" )
                self.strstream.write( u"    <td>%s</td>\n" % name )
                self.strstream.write( u"    <td style = \"color: grey\" >NOT FOUND</td>\n" )
                self.strstream.write( u"    <td>%s</td>\n" % value.html() )
                self.strstream.write(
                    u"    <td style = \"background-color:red;color:white;text-align:center\" >MISSING</td>\n"
                    )
                self.strstream.write( u"  </tr>\n" )

            self.strstream.write( u"</table>\n" )

            if comparison.evaluation_for[ heading ].disabled_tests:
                self.strstream.write( u"<ul>\n" )

                for t in comparison.evaluation_for[ heading ].disabled_tests:
                    self.strstream.write( u"  <li>Test %s disabled</li>" % t )

                self.strstream.write( u"</ul>\n" )

            self.strstream.write(
                u"<p>Back to <a href=\"#%s_comparison\">comparisons</a></p>" % identifier
                )

        for ( index, ( heading, report ) ) in enumerate(
            comparison.left_only,
            start = len( comparison.evaluation_for )
            ):
            self.strstream.write(
                u"<a name = \"%s_comparison_%d\" /><h4>%s</h4>\n" % ( identifier, index, heading )
                )

            self.strstream.write( u"<table border = \"1\">\n" )
            self.strstream.write( u"  <tr>\n" )
            self.strstream.write( u"    <th>Name</th>\n" )
            self.strstream.write( u"    <th>Expected</th>\n" )
            self.strstream.write( u"    <th>Got</th>\n" )
            self.strstream.write( u"    <th>Status</th>\n" )
            self.strstream.write( u"  </tr>\n" )

            for ( name, value ) in report.items():
                self.strstream.write( u"  <tr>\n" )
                self.strstream.write( u"    <td>%s</td>\n" % name )
                self.strstream.write( u"    <td>%s</td>\n" % value.html() )
                self.strstream.write( u"    <td style = \"color: grey\" >NOT FOUND</td>\n" )
                self.strstream.write(
                    "    <td style = \"background-color:red;color:white;text-align:center\" >MISSING</td>\n"
                    )
                self.strstream.write( u"  </tr>\n" )

            self.strstream.write( u"</table>\n" )
            self.strstream.write(
                u"<p>Back to <a href=\"#%s_comparison\">comparisons</a></p>" % identifier
                )

        for ( index, ( heading, report ) ) in enumerate(
            comparison.right_only,
            start = len( comparison.evaluation_for ) + len( comparison.left_only )
            ):
            self.strstream.write(
                u"<a name = \"%s_comparison_%d\" /><h4>%s</h4>\n" % ( identifier, index, heading )
                )

            self.strstream.write( u"<table border = \"1\">\n" )
            self.strstream.write( u"  <tr>\n" )
            self.strstream.write( u"    <th>Name</th>\n" )
            self.strstream.write( u"    <th>Expected</th>\n" )
            self.strstream.write( u"    <th>Got</th>\n" )
            self.strstream.write( u"    <th>Status</th>\n" )
            self.strstream.write( u"  </tr>\n" )

            for ( name, value ) in report.items():
                self.strstream.write( u"  <tr>\n" )
                self.strstream.write( u"    <td>%s</td>\n" % name )
                self.strstream.write( u"    <td style = \"color: grey\" >NOT FOUND</td>\n" )
                self.strstream.write( u"    <td>%s</td>\n" % value.html() )
                self.strstream.write(
                    u"    <td style = \"background-color:red;color:white;text-align:center\" >MISSING</td>\n"
                    )
                self.strstream.write( u"  </tr>\n" )

            self.strstream.write( u"</table>\n" )
            self.strstream.write(
                "<p>Back to <a href=\"#%s_comparison\">comparisons</a></p>" % identifier
                )

        for ( index, heading ) in enumerate(
            comparison.disabled_sections,
            start = len( comparison.evaluation_for ) + len( comparison.left_only ) + len( comparison.right_only )
            ):
            self.strstream.write(
                u"<a name = \"%s_comparison_%d\" /><h4>%s</h4>\n" % ( identifier, index, heading )
                )
            self.strstream.write( u"<p>REPORT DISABLED</p>" )
            self.strstream.write(
                u"<p>Back to <a href=\"#%s_comparison\">comparisons</a></p>" % identifier
                )

        self.strstream.write(
            u"<p>Back to <a href=\"#%s\">test</a></p>" % identifier
            )
        self.strstream.write(
            u"<a name = \"%s_summary\"/><h3>Summary:</h3>\n" % identifier
            )
        self.strstream.write( u"<pre>\n%s\n</pre>\n" % comparison.get_text_summary() )
        self.strstream.write(
            u"<p>Back to <a href=\"#%s\">test</a></p>" % identifier
            )
        self.strstream.write(
            u"<b style = \"color:%s\" >%s</b>\n" % (
                ( "green", "OK" ) if passed else ( "red","ERROR: incorrect results" )
                )
            )
        self.summary.append(
            (
                self.current,
                passed,
                "%d failures, %d unknowns" % ( comparison.failures(), comparison.unknowns() ),
                runtime,
                )
            )
        self.current = None


    def status(self):

        return all( p for ( i, p, m, r ) in self.summary )


    def finalize(self):

        assert self.current is None
        # The OVERVIEW section is composed here at the end when we know all test results
        # that have been appended in add_valid_entry() and add_error_entry()
        self.stream.write(
            "<a name = \"overview\"/><h2>OVERVIEW of tests</h2>\n"
            )
        self.stream.write( "<p>Back to <a href=\"#title\">title</a></p>" )
        self.stream.write( "<table border = \"1\">\n" )
        self.stream.write( "  <tr>\n" )
        self.stream.write( "    <th>Index</th>\n" )
        self.stream.write( "    <th>Name</th>\n" )
        self.stream.write( "    <th>Execution</th>\n" )
        self.stream.write( "    <th>Status</th>\n" )
        self.stream.write( "  </tr>\n" )
        data = enumerate( self.summary, start = 1 )

        for ( index, ( ( name, eng, ident ), passed, mess, runt ) ) in data:
            self.stream.write( "  <tr>\n" )
            self.stream.write(
                "    <td><a href = \"#%s\">%d</a></td>\n" % ( ident, index )
                )
            self.stream.write( "    <td>%s</td>\n" % name )
            self.stream.write( "    <td>%s</td>\n" % eng )
            self.stream.write(
                "    <td style = \"background-color:%s;color:white;text-align:center\" >%s</td>\n" % (
                    ( "green", "OK" ) if passed else ( "red", mess )
                    )
                )
            self.stream.write( "  </tr>\n" )

        self.stream.write( "</table>\n" )
        self.stream.write(
            "<p style = \"font-weight:bold\" >All tests status: <span style = \"color:%s\" >%s</span></p>\n" % (
                ( "green", "OK" ) if self.status() else ( "red", "ERROR" )
                )
            )
        self.stream.write(
            "<h2>Test suite <span style = \"color:blue\" >%s</span> finished</h2>\n" % self.suite
            )
        # append the test results stored in strstream to stream after OVERVIEW
        self.stream.write( self.strstream.getvalue() )
        self.strstream.close()

        self.stream.write( "</body>\n" )
        self.stream.write( "</html>\n" )
        self.stream.close()


# Suites
def phaser_regression_tests():

    import libtbx.load_env
    import os.path
    import glob
    from functools import reduce

    root = libtbx.env.under_dist( "phaser_regression", "regression" )
    tests = [ os.path.splitext( os.path.basename( fn ) )[0] for fn
        in glob.glob( os.path.join( root, "tst*.py" ) ) ]
    tests_in = dict( [ ( t, [ t ] ) for t in tests ] )

    tests_in[ "anisotropy" ] = [
        "tst_anisotropy_beta_blip",
        "tst_anisotropy_iod",
        "tst_anisotropy_toxd",
        ]
    tests_in[ "frf"] = [
        "tst_frf_toxd",
        "tstl_frf_iod",
        ]
    tests_in[ "brf" ] = [
        "tst_brf_beta_blip",
        "tst_brf_toxd",
        ]
    tests_in[ "ftf" ] = [
        "tst_ftf_toxd",
        ]
    tests_in[ "btf" ] = [
        "tst_btf_beta_blip",
        "tst_btf_toxd",
        ]
    tests_in[ "pak" ] = [
        "tst_pak_toxd",
        ]
    tests_in[ "rnp" ] = [
        "tst_rnp_barnase",
        "tst_rnp_toxd",
        "tst_rnp_no_bref_toxd",
        ]
    tests_in[ "llg" ] = [
        "tst_llg_toxd",
        ]
    tests_in[ "auto" ] = [
        "tst_auto_toxd",
        "tst_auto_ed_toxd",
        "tstl_auto_beta_blip",
        "tstl_auto_glykos",
        ]
    tests_in[ "ep" ] = [
        "tst_ep_atoms_iod",
        "tst_ep_atoms_partial_iod",
        "tst_ep_partial_iod",
        "tst_ep_partial_ix_iod",

        "tstb_ep_trypse",
        "tstb_ep_auto_trypse",

        "tstb_ep_apt_br",
        "tstb_ep_p9",
        "tstb_ep_rusti_cu_s",
        "tstb_ep_rusti_s",
        "tstb_ep_sav_ca_s_cl",
        "tstb_ep_sav_cl",
        "tstb_ep_ssadins",
        "tstb_ep_ybmbp",
        ]

    tests_in[ "mr" ] = [
        "tst_auto_ed_toxd",
        "tst_auto_toxd",
        "tst_brf_beta_blip",
        "tst_brf_toxd",
        "tst_btf_beta_blip",
        "tst_btf_toxd",
        "tst_frf_toxd",
        "tst_ftf_toxd",
        "tst_llg_toxd",
        "tst_pak_toxd",
        "tst_rnp_barnase",
        "tst_rnp_no_bref_toxd",
        "tst_rnp_toxd",
        "tstl_frf_iod",
        "tstl_auto_beta_blip",
        "tstl_fast_beta_blip",
        "tstl_auto_glykos",
        ]

    tests_in[ "toxd" ] = [
        "tst_anisotropy_toxd",
        "tst_auto_ed_toxd",
        "tst_auto_toxd",
        "tst_brf_toxd",
        "tst_btf_toxd",
        "tst_frf_toxd",
        "tst_ftf_toxd",
        "tst_llg_toxd",
        "tst_pak_toxd",
        "tst_rnp_toxd",
        "tst_rnp_no_bref_toxd",
        ]
    tests_in[ "betablip" ] = [
        "tst_anisotropy_beta_blip",
        "tst_brf_beta_blip",
        "tst_btf_beta_blip",
        "tstl_auto_beta_blip",
        "tstl_fast_beta_blip",
        ]
    tests_in[ "iod" ] = [
        "tst_anisotropy_iod",
        "tst_ep_atoms_iod",
        "tst_ep_atoms_partial_iod",
        "tst_ep_partial_iod",
        "tst_ep_partial_ix_iod",
        "tstl_frf_iod",
        ]
    tests_in[ "benchmark" ] = [
        "tstb_ep_apt_br",
        "tstb_ep_p9",
        "tstb_ep_rusti_cu_s",
        "tstb_ep_rusti_s",
        "tstb_ep_sav_ca_s_cl",
        "tstb_ep_sav_cl",
        "tstb_ep_ssadins",
        "tstb_ep_trypse",
        "tstb_ep_auto_trypse",
        "tstb_ep_ybmbp",
        ]
    tests_in[ "cci" ] = [
        "tstb_ep_trypse",
        "tst_auto_toxd",
        ]

    tests_in[ "all" ] = set( reduce( operator.add, tests_in.values() ) )
    tests_in[ "short" ] = [
        t for t in tests_in[ "all" ] if t[:4] != "tstl"
        ]
    tests_in[ "long" ] = [
        t for t in tests_in[ "all" ] if t[:4] == "tstl"
        ]
    tests_in[ "cimr" ] = [
        t for t in tests_in[ "all" ] if t != "tstb_ep_sav_ca_s_cl"
        ]

    # for quick debugging
    """tests_in[ "mytests"] = [
        "tst_rnp_toxd",
        "tst_pak_toxd",
        "tst_ep_partial_ix_iod",
        "tst_pak_toxd",
        "tst_gyre_beta_blip",
        ]
    """

    return ( root, tests_in )


def run_multiple_tests(args):

    parser = argparse.ArgumentParser( description = "Run test suite" )

    # Add tests in regression folder as standalone tests
    ( REGRESSION, tests_in ) = phaser_regression_tests()

    output_handler_for = {
        "full": TextOutput.verbose,
        "normal": TextOutput.normal,
        "terse": TextOutput.terse,
        "terse_failed": TextOutput.terse_failed,
        "silent": TextOutput.silent,
        "html": HTMLOutput,
        }

    parser.add_argument(
        "suite",
        action = "store",
        choices = tests_in.keys(),
        help = "Test suite to run"
        )
    parser.add_argument(
        "-o", "--output",
        action = "store",
        choices = output_handler_for.keys(),
        default = "normal",
        help = "Output verbosity"
        )
    parser.add_argument( "-f", "--outputfile", type = str, default = "PhaserRegressionOutPut" )
    parser.add_argument( "-n", "--ncpus", type = positive_int, default = 1 )
    parser.add_argument( "-s", "--suffix",
                        action = "store",
                        help = "Suffix on results file. Default is '%s'" %suffix,
                        type = str,
                        default = "%s" %suffix
                        )
    parser.add_argument(
        "-e", "--engine",
        action = "store",
        choices = get_engines(),
        default = sorted( get_engines() )[0],
        help = "Execution mode"
        )

    # Parse arguments
    params = parser.parse_args( args )

    # Add regression test directory to sys.path
    import sys
    sys.path.append( REGRESSION )

    import tempfile
    import importlib
    import itertools

    txtoutput = output_handler_for[ params.output ](
        stream = sys.stdout,
        fstream = open(params.outputfile + ".txt", "w"),
        suite = params.suite
        )

    htmloutput = output_handler_for[ "html" ](
        stream = open(params.outputfile + ".html", "w"),
        suite = params.suite
        )


    if params.engine == "all":
      method_for = get_known_engines()
    if params.engine == "script":
      method_for = get_script_engines()
    if params.engine == "python":
      method_for = get_python_engines()

    combinations = itertools.product(
        tests_in[ params.suite ],
        method_for.keys()
        )

    for ( name, engine ) in combinations:
        for output in [txtoutput, htmloutput]:
            output.heading( name = name, engine = engine )

        try:
            m = importlib.import_module( name )

        except ImportError as e:
            for output in [txtoutput, htmloutput]:
                output.add_error_entry(
                    full = e,
                    short = "error loading test"
                    )
            continue

        outstream = tempfile.TemporaryFile(mode='w+t')

        try:
            ( comp, runtime ) = run_test_and_compare(
                input = m.INPUT,
                stream = outstream,
                engine = method_for[ engine ],
                ncpus = params.ncpus,
                resfilesuffix = params.suffix
                )
            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
            outstream.seek( 0 )
            for output in [txtoutput, htmloutput]:
                output.add_valid_entry(
                    output = outstream.read().replace("\r\n", "\n"), # no Windows linebreaks
                    comparison = comp,
                    passed = comp.passed(),
                    runtime = runtime
                    )

        except Exception as e:
            import traceback
            for output in [txtoutput, htmloutput]:
                outstream.seek( 0 )
                output.add_error_entry(
                    full = e,
                    short = e,
                    output = outstream.read().replace("\r\n", "\n"), # no Windows linebreaks
                    traceback = traceback.format_exc()
                    )
            continue

    sys.path.remove( REGRESSION )
    for output in [txtoutput, htmloutput]:
        output.finalize()

    return output.status()
