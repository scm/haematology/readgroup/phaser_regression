
from __future__ import print_function
from __future__ import division

from libtbx import easy_run
import libtbx.load_env
import sys, os, os.path
from phaser_regression import additional_regressions


philtxt ="""
gyre_and_gimble {
  gyre = True
  gimble = True
  moving {
    modlid = "moving"
    pdbin = %s
    rms = 1.1
    domain {
      selection = "(chain A and resseq 63:210)"
      chain_id = "A"
    }
    domain {
      selection = "(chain A and resseq 211:291 or resseq 26:62)"
      chain_id = "B"
    }
  }
  hklin = %s
  fileroot = "1BUE_AsolGyreGimbleRms1.1"
  njobs = 8
  mute=False
}

"""



def exercise(dirty=False) :
  dpath = os.path.join(libtbx.env.under_dist("phaser_regression", "additional_regressions"), "gyre_and_gimble")
  modelfname = os.path.join(dpath, "sculpt_1BUE_A.pdb")
  mtzfname = os.path.join(dpath, "1tdg.mtz")

  with open("GyreGimblePhil.txt","w") as f:
    f.write(philtxt %(modelfname, mtzfname) )

  strargs = "phaser.gyre_and_gimble GyreGimblePhil.txt"
  result = easy_run.fully_buffered(strargs)
  if not result.return_code == 0:
    nlines = 50
    for line in result.stdout_lines[-nlines:]:
      print(line)
    for line in result.stderr_lines[-nlines:]:
      print(line)
    print("Runtime error " + str(result.return_code))
  assert result.return_code == 0

  fname1 = os.path.join(dpath, "ReferenceGyreGimble.pdb")
  fname2 =  "1BUE_AsolGyreGimbleRms1.1.1.pdb"
  ret = additional_regressions.PdbFcalcCorrelation(fname1, fname2)
  difftxtlst = additional_regressions.DifflistFiles(fname1, fname2,
                                                    ignorestring="REMARK PHASER REVISION")
  rev1 = additional_regressions.ReadPhaserRevisionRemark(fname1)
  rev2 = additional_regressions.ReadPhaserRevisionRemark(fname2)

  if not dirty:
    # tidy up
    tmpfiles = ["1BUE_AsolGyreGimbleRms1.1.1.pdb", "1BUE_AsolGyreGimbleRms1.1.1.mtz", "GyreGimblePhil.txt" ]
    for f in tmpfiles:
      try:
        os.remove(f)
      except Exception as e:
        pass

  if ( ret != (1.0, 1.0) ):
    sys.stdout.writelines(rev1)
    sys.stdout.writelines(rev2)
    sys.stdout.writelines(difftxtlst)
  assert ( ret == (1.0, 1.0)  )

if (__name__ == "__main__"):
  if len(sys.argv) > 1:
    exercise(sys.argv[1] == "dirty")
  else:
    exercise()
  print("OK")
