
from __future__ import print_function
from __future__ import division

from libtbx import easy_run
import libtbx.load_env
import sys, os, os.path
from phaser_regression import additional_regressions


def exercise(dirty=False) :
  dpath = os.path.join(libtbx.env.under_dist("phaser_regression", "additional_regressions"), "ensembler")
  model1 = os.path.join(dpath, "1BUN_B.pdb")
  model2 = os.path.join(dpath, "3CI7_A.pdb")
  alignfn = os.path.join(dpath, "mult.aln")

  strargs = "phaser.ensembler model=%s model=%s alignment=%s root=test_ensembler" %(model1, model2, alignfn)
  result = easy_run.fully_buffered(strargs)
  if not result.return_code == 0:
    nlines = 50
    for line in result.stdout_lines[-nlines:]:
      print(line)
    for line in result.stderr_lines[-nlines:]:
      print(line)
    print("Runtime error " + str(result.return_code))
  assert result.return_code == 0

  fname1 = "test_ensembler_merged.pdb" # resultant file emerging from running ensembler
  fname2 =  os.path.join(dpath, "ensembler_merged.pdb") # reference file to compare with
  ret = additional_regressions.PdbFcalcCorrelation(fname1, fname2)
  difftxtlst = additional_regressions.DifflistFiles(fname1, fname2,
                                                    ignorestring="REMARK PHASER REVISION")
  rev1 = additional_regressions.ReadPhaserRevisionRemark(fname1)
  rev2 = additional_regressions.ReadPhaserRevisionRemark(fname2)

  # tidy up
  if not dirty:
    tmpfiles = ["test_ensembler_merged.pdb" ]
    for f in tmpfiles:
      try:
        os.remove(f)
      except Exception as e:
        pass

  if ( ret != (1.0, 1.0) ):
    sys.stdout.writelines(rev1)
    sys.stdout.writelines(rev2)
    sys.stdout.writelines(difftxtlst)
  assert ( ret == (1.0, 1.0) )

if (__name__ == "__main__"):
  if len(sys.argv) > 1:
    exercise(sys.argv[1] == "dirty")
  else:
    exercise()
  print("OK")
