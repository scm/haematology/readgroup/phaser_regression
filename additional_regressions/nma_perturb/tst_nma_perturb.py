
from __future__ import print_function
from __future__ import division

from libtbx import easy_run
from phaser.AltOrigSymmatesEngine import GetRMSDfromAtoms
from iotbx import pdb
import libtbx.load_env
import sys, os, os.path
from phaser_regression import additional_regressions


def exercise(dirty=False) :
  dpath = os.path.join(libtbx.env.under_dist("phaser_regression", "additional_regressions"), "nma_perturb")
  modelfname = os.path.join(dpath, "1npi.pdb")

  strargs = "phaser.nma_perturb file_name=%s" %modelfname
  result = easy_run.fully_buffered(strargs)
  if not result.return_code == 0:
    nlines = 50
    for line in result.stdout_lines[-nlines:]:
      print(line)
    for line in result.stderr_lines[-nlines:]:
      print(line)
    print("Runtime error " + str(result.return_code))
  assert result.return_code == 0

  fname1 = "1npi_NMA.nma.pdb"
  fname2 = os.path.join(dpath, "reference.pdb")
  referencehierarchy = pdb.input(file_name= fname2).construct_hierarchy()
  NMAhierarchy = pdb.input(file_name= fname1).construct_hierarchy()

  rev1 = additional_regressions.ReadPhaserRevisionRemark(fname1)
  rev2 = additional_regressions.ReadPhaserRevisionRemark(fname2)

  if not dirty:
    # tidy up
    tmpfiles = ["1npi_NMA.log", "1npi_NMA.mat", "1npi_NMA.nma.pdb", ".phaser_result.pkl" ]
    for f in tmpfiles:
      try:
        os.remove(f)
      except Exception as e:
        pass

  assert len(referencehierarchy.models()) == len(NMAhierarchy.models())
  # get RMSD based difference of models paired up and print diffs if the RMSD > 0.001
  for i,model in enumerate(NMAhierarchy.models()):
    atompairs = list(zip(referencehierarchy.models()[i].atoms(), model.atoms()))
    ret = ( GetRMSDfromAtoms(atompairs) < 0.001)
    if ret == False:
      m1 = pdb.hierarchy.root()
      m1.append_model(model.detached_copy() )
      m2 = pdb.hierarchy.root()
      m2.append_model(referencehierarchy.models()[i].detached_copy() )
      difftxtlst = additional_regressions.DifflistLines(m1.as_pdb_string().splitlines(),
                                                        m2.as_pdb_string().splitlines(),
                                                        fromfile='%s %d'%(fname1,(i+1)),
                                                        tofile=fname2 + ' model %d' %(i+1))

      if difftxtlst != []:
        sys.stdout.writelines(rev1)
        sys.stdout.writelines(rev2)
        sys.stdout.write("model %d:\n" %(i+1) )
        sys.stdout.write("\n".join(difftxtlst) + "\n\n")
    assert ret==True

if (__name__ == "__main__"):
  if len(sys.argv) > 1:
    exercise(sys.argv[1] == "dirty")
  else:
    exercise()
  print("OK")
