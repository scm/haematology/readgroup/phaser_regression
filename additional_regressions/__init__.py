from iotbx import pdb
from cctbx.array_family import flex
import difflib


def PdbFcalcCorrelation(pdbfname1, pdbfname2, resolution=3.0):
  xray_structures1 = pdb.input(file_name=pdbfname1).xray_structures_simple()
  xray_structures2 = pdb.input(file_name=pdbfname2).xray_structures_simple()
  assert len(xray_structures1) == len(xray_structures2) # same number of models in both pdb files
  amplcc = 0.0
  phasecc = 0.0
  for i,xray_structure1 in enumerate(xray_structures1):
    fcalc1 = xray_structure1.structure_factors( d_min=resolution).f_calc()
    fcalc2 = xray_structures2[i].structure_factors( d_min=resolution).f_calc()
    amplcorrel = flex.linear_correlation(x=fcalc1.amplitudes().data(), y=fcalc2.amplitudes().data(), epsilon=1.e-6)
    phasecorrel = flex.linear_correlation(x=fcalc1.phases().data(), y=fcalc2.phases().data(), epsilon=1.e-6)
    amplcc += amplcorrel.coefficient()
    phasecc += phasecorrel.coefficient()
  amplcc /= len(xray_structures1)
  phasecc /= len(xray_structures1)
  return amplcc, phasecc


def DifflistFiles(fname1, fname2, ignorestring=''):
  with open(fname1,"r") as f1:
    lns1 = f1.readlines()
  with open(fname2,"r") as f2:
    lns2 = f2.readlines()
  return DifflistLines(lns1, lns2, ignorestring=ignorestring)


def DifflistLines(lns1, lns2,  fromfile='', tofile='', ignorestring='' ):
  if ignorestring != "":
    lns1 = [ e for e in lns1 if ignorestring not in e ]
    lns2 = [ e for e in lns2 if ignorestring not in e ]

  lns2 = [ e.replace("\r\n", "\n") for e in lns2 ] # by converting both files to unix line endings
  lns2 = [ line.strip() for line in lns2 ]
  lns2 = [ line for line in lns2 if line ]
  lns1 = [ e.replace("\r\n", "\n") for e in lns1 ] # make indifferent to line endings
  lns1 = [ line.strip() for line in lns1 ]
  lns1 = [ line for line in lns1 if line ]
  return list(difflib.unified_diff( lns1, lns2, fromfile, tofile ))


def ReadPhaserRevisionRemark(fname):
  with open(fname,"r") as f1:
    lns = f1.readlines()
  revisionremark = [ e for e in lns if "REMARK PHASER REVISION" in e ]
  return revisionremark
