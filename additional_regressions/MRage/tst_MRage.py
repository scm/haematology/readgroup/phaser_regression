
from __future__ import print_function
from __future__ import division

from libtbx import easy_run
import libtbx.load_env
import sys, os, os.path
from phaser_regression import additional_regressions


philtxt = """
hklin = 1bso.mtz
labin = FOBS_X,SIGFOBS_X
crystal_symmetry {
  unit_cell = 54.026 54.026 112.183 90 90 120
  space_group = P 32 2 1
}
output {
  root = testMRage_MR
  max_solutions_to_write = 1
  gui_base_dir = .
}
composition {
  component {
    sequence = 1BSO_ChainA.seq
    ensemble {
      coordinates {
        pdb = "sculpt_1GM6_A.pdb"
        identity = 0.1900
      }
    }
  }
}
queue {
  cpus = 10
}
"""

philtxt2 = """
hklin = %s
labin = F,SIGF
crystal_symmetry {
  unit_cell = 134.721 31.02 101.258 90 130.658 90
  space_group = C 1 2 1
}
output {
  root = testMRage_MR
  max_solutions_to_write = 1
  gui_base_dir = .
}
composition {
  component {
    sequence = %s
    homology {
      file_name = %s
    }
  }
}
queue {
  cpus = 8
}
"""



def exercise(dirty=False) :
  dpath = os.path.join(libtbx.env.under_dist("phaser_regression", "additional_regressions"), "MRage")
  hmlgfn = os.path.join(dpath, "T0971.hhr")
  seqfn = os.path.join(dpath, "6d34.fasta")
  mtzfname = os.path.join(dpath, "6d34.mtz")

  # use local mirror pdb to avoid downloading structures
  os.environ["PDB_MIRROR_PDB"] = os.path.join(dpath, "..", "PDBmirror")

  with open("MRagePHILinput2.txt","w") as f:
    f.write(philtxt2 %(mtzfname, seqfn, hmlgfn) )

  strargs = "phaser.MRage MRagePHILinput2.txt"
  result = easy_run.fully_buffered(strargs)
  if not result.return_code == 0:
    nlines = 50
    for line in result.stdout_lines[-nlines:]:
      print(line)
    for line in result.stderr_lines[-nlines:]:
      print(line)
    print("Runtime error " + str(result.return_code))
  assert result.return_code == 0

  fname1 = os.path.join(dpath, "reference.pdb")
  fname2 =  "testMRage_MR_C121_1.1.pdb"
  ret = additional_regressions.PdbFcalcCorrelation(fname1, fname2)
  difftxtlst = additional_regressions.DifflistFiles(fname1, fname2,
                                                    ignorestring="REMARK PHASER REVISION")
  rev1 = additional_regressions.ReadPhaserRevisionRemark(fname1)
  rev2 = additional_regressions.ReadPhaserRevisionRemark(fname2)

  if ( ret != (1.0, 1.0) ):
    sys.stdout.writelines(rev1)
    sys.stdout.writelines(rev2)
    sys.stdout.writelines(difftxtlst)

  if not dirty:
    tmpfiles = ["2IMJ_A.pdb", "3EBT_A.pdb","3FGY_A.pdb", "sculpt_2IMJ_A_T0971_hit3_12.pdb",
      "sculpt_3EBT_A_T0971_hit1_12.pdb", "sculpt_3FGY_A_T0971_hit2_12.pdb", "MRagePHILinput2.txt",
      "testMRage_MR_C121_1.1.pdb", "testMRage_MR_C121_1.1.mtz", "testMRage_MR_C121_results.pkl"]
    for f in tmpfiles:
      try:
        os.remove(f)
      except Exception as e:
        pass

  assert ( ret == (1.0, 1.0) )

if (__name__ == "__main__"):
  if len(sys.argv) > 1:
    exercise(sys.argv[1] == "dirty")
  else:
    exercise()
  print("OK")
