
from __future__ import print_function
from __future__ import division

from libtbx import easy_run
from iotbx import pdb
import libtbx.load_env
import sys, os, os.path
from phaser_regression import additional_regressions



philtxt = """
input {
  structure {
    model {
      file_name = %s
      errors {
        file_name = %s
        chain_ids = A
      }
    }
    model {
      file_name = %s
      errors {
        file_name = %s
        chain_ids = A
      }
    }
    model {
      file_name = %s
      errors {
        file_name = %s
        chain_ids = A
      }
    }
  }
  homology_search {
    file_name = %s
  }
}
output {
  gui_output_dir = .
}
sculpting {
  dna {
    mainchain {
      polishing {
        use = *remove_short
      }
    }
  }
  rna {
    mainchain {
      polishing {
        use = *remove_short
      }
    }
  }
}

"""

def exercise(dirty=False) :
  dpath = os.path.join(libtbx.env.under_dist("phaser_regression", "additional_regressions"), "sculpt_ensembler")
  m1 = os.path.join(dpath, "2IMJ_A.pdb")
  m2 = os.path.join(dpath, "3EBT_A.pdb")
  m3 = os.path.join(dpath, "3FGY_A.pdb")
  e1 = os.path.join(dpath, "2IMJ_A.err")
  e2 = os.path.join(dpath, "3EBT_A.err")
  e3 = os.path.join(dpath, "3FGY_A.err")
  h = os.path.join(dpath, "T0971.hhr")

  with open("sculpt_ensemblePHILinput.txt","w") as f:
    f.write(philtxt %(m1, e1, m2, e2, m3, e3, h) )

  strargs = "phenix.sculpt_ensemble sculpt_ensemblePHILinput.txt"
  result = easy_run.fully_buffered(strargs)
  if not result.return_code == 0:
    nlines = 50
    for line in result.stdout_lines[-nlines:]:
      print(line)
    for line in result.stderr_lines[-nlines:]:
      print(line)
    print("Runtime error " + str(result.return_code))
  assert result.return_code == 0

  fname1 = os.path.join(dpath,"reference.pdb")
  fname2 =  "sculpt_ensemble_merged.pdb"
  ret = additional_regressions.PdbFcalcCorrelation(fname1, fname2)

  referencehierarchy = pdb.input(file_name= fname1).construct_hierarchy()
  testhierarchy = pdb.input(file_name= fname2).construct_hierarchy()
  rev1 = additional_regressions.ReadPhaserRevisionRemark(fname1)
  rev2 = additional_regressions.ReadPhaserRevisionRemark(fname2)

  if not dirty:
    # tidy up
    tmpfiles = [fname2, "sculpt_ensemblePHILinput.txt" ]
    for f in tmpfiles:
      try:
        os.remove(f)
      except Exception as e:
        pass

  assert result.return_code == 0
  assert len(referencehierarchy.models()) == len(testhierarchy.models())
  # Compare pdb files model by model. This also makes test ignore what line endings are used
  for i,model in enumerate(testhierarchy.models()):
    m1 = pdb.hierarchy.root()
    m1.append_model(model.detached_copy() )
    m2 = pdb.hierarchy.root()
    m2.append_model(referencehierarchy.models()[i].detached_copy() )
    difftxtlst = additional_regressions.DifflistLines(m1.as_pdb_string().splitlines(),
                                        m2.as_pdb_string().splitlines(),
                                        fromfile=fname1, tofile=fname2)
    if ( ret != (1.0, 1.0)  and difftxtlst != [] ):
      sys.stdout.writelines(rev1)
      sys.stdout.writelines(rev2)
      sys.stdout.write("In model %d:\n" %(i+1) )
      sys.stdout.write("\n".join(difftxtlst) + "\n\n")
    assert ( ret == (1.0, 1.0) )


if (__name__ == "__main__"):
  if len(sys.argv) > 1:
    exercise(sys.argv[1] == "dirty")
  else:
    exercise()
  print("OK")
