
from __future__ import print_function
from __future__ import division

from libtbx import easy_run
import sys, difflib, os, csv, os.path
import libtbx.load_env
from phaser_regression import additional_regressions


def exercise(dirty=False) :
  dpath = os.path.join(libtbx.env.under_dist("phaser_regression", "additional_regressions"), "proq2_pdb_to_sculptor_error_file")
  alnfname = os.path.join(dpath, "myalign.aln")
  sculptfname = os.path.join(dpath, "sculpt_3EBT_A_T0971_hit1_12.pdb")
  pdbfname = os.path.join(dpath, "3EBT_A1.pdb")

  strargs = "phaser.proq2_pdb_to_sculptor_error_file %s %s %s" %(alnfname, sculptfname, pdbfname)
  result = easy_run.fully_buffered(strargs)
  if not result.return_code == 0:
    nlines = 50
    for line in result.stdout_lines[-nlines:]:
      print(line)
    for line in result.stderr_lines[-nlines:]:
      print(line)
    print("Runtime error " + str(result.return_code))
  assert result.return_code == 0
  # compare output file with reference, both of which are csv format
  fname1 = os.path.join(dpath, "reference.err")
  fname2 =  "3EBT_A1_A.err"
  with open(fname1,"r") as f1:
    csv1 = list(csv.reader(f1))
    str1 = [ ", ".join(r) + "\n" for r in csv1]
  with open(fname2,"r") as f2:
    csv2 = list(csv.reader(f2))
    str2 = [ ", ".join(r) + "\n" for r in csv2]

  difftxtlst = additional_regressions.DifflistLines(str1, str2, fromfile=fname1, tofile="3EBT_A1_A.err")

  if not dirty:
    # tidy up
    tmpfiles = ["3EBT_A1_A.err" ]
    for f in tmpfiles:
      try:
        os.remove(f)
      except Exception as e:
        pass

  if difftxtlst != []:
    sys.stdout.writelines(difftxtlst)
  assert result.return_code == 0
  assert difftxtlst == []

if (__name__ == "__main__"):
  if len(sys.argv) > 1:
    exercise(sys.argv[1] == "dirty")
  else:
    exercise()
  print("OK")
