#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      oeffner
#
# Created:     26/06/2012
# Copyright:   (c) oeffner 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

from __future__ import print_function

from libtbx import easy_run
from iotbx import pdb
import re
import libtbx.load_env
import os, sys, zipfile, os.path
import shutil
from phaser_regression import additional_regressions


# Each element in the test list is a PHIL input with corresponding files in the compressed
# find_alt_orig_sym_mate_data folder. A list element is converted to a text file that is used as
# PHIL input for phenix.find_alt_orig_sym_mate. 
# For each test a file named reference#.pdb exists. The REMARK FAMOS annotation in this file is 
# extracted from the output file and compared to the corresponding annotation in the test#.pdb 
# file that is prroduced by the tests. If they are not approximately equal the test will fail 
# with an assert.


testslst = [\
#0
r"""
AltOrigSymMates.moving.model="2z0d.cif"
AltOrigSymMates.fixed.model="Strider{3ECI_A0,2P82_A0}3ECI_A0,2P82_A0.1.pdb"
AltOrigSymMates.verbose = True
AltOrigSymMates.debug = True
AltOrigSymMates.outputfileprefix = test0
AltOrigSymMates.write_cif = True
"""
,
#1
r"""
AltOrigSymMates.fixed.mrsolution
{
  solfname = "Strider{3ECI_A0,2P82_A0}3ECI_A0,2P82_A0.sol"
  ensembles
  {
    name = "MR_2P82_A0"
    model = "sculpt_2P82_A0.pdb"
  }
  ensembles
  {
    name = "MR_3ECI_A0"
    model = "sculpt_3ECI_A0.pdb"
  }
}

AltOrigSymMates.moving.model="2z0d.cif"
AltOrigSymMates.verbose = True
AltOrigSymMates.outputfileprefix = test1
"""
,
#2
r"""
AltOrigSymMates.fixed.mrsolution
{
  solfname = "Strider{3ECI_A0,2P82_A0}3ECI_A0,2P82_A0.sol"
  ensembles
  {
    name = "MR_2P82_A0"
    model = "sculpt_2P82_A0.pdb"
  }
  ensembles
  {
    name = "MR_3ECI_A0"
    model = "sculpt_3ECI_A0.pdb"
  }
}
AltOrigSymMates.moving.mrsolution
{
  solfname = "Strider{2ZPN_A0,2P82_A0}2ZPN_A0,2P82_A0.sol"
  ensembles
  {
    name = "MR_2P82_A0"
    model = "sculpt_2P82_A0.pdb"
  }
  ensembles
  {
    name = "MR_2ZPN_A0"
    model = "sculpt_2ZPN_A0.pdb"
  }
}
AltOrigSymMates.spacegroupfname = "2Z0D-sf.cif"
AltOrigSymMates.verbose = True
AltOrigSymMates.nproc = 4
AltOrigSymMates.quiet = True
AltOrigSymMates.outputfileprefix = test2
AltOrigSymMates.write_cif = True
"""
,
#3
r"""
AltOrigSymMates.moving.model="P05_MR.1.pdb"
AltOrigSymMates.fixed.model="3sv2.pdb"
AltOrigSymMates.verbose = True
AltOrigSymMates.outputfileprefix = test3
"""
,
#4
r"""
AltOrigSymMates.moving.model="1se8.pdb"
AltOrigSymMates.fixed.model="Strider{1Z9F_A}1Z9F_A.1.pdb"
AltOrigSymMates.verbose = True
AltOrigSymMates.outputfileprefix = test4
AltOrigSymMates.write_cif = True
"""
,
#5
r"""
AltOrigSymMates.moving.model="1tfx.cif"
AltOrigSymMates.fixed.model="3RP2_A_1DTX_A.1.pdb"
AltOrigSymMates.movehetatms = True
AltOrigSymMates.outputfileprefix = test5
AltOrigSymMates.write_cif = True
"""
,
#6
r"""
AltOrigSymMates.moving.model="1tfx.cif"
AltOrigSymMates.fixed.model="Ellie1BBR_H_3D65_I.pdb"
AltOrigSymMates.verbose = True
AltOrigSymMates.outputfileprefix = test6
"""
,
#7
r"""
AltOrigSymMates.moving.model="1g9v.pdb"
AltOrigSymMates.fixed.model="RQ3_MR.1.pdb"
AltOrigSymMates.movehetatms = True
AltOrigSymMates.outputfileprefix = test7
"""
,
#8
r"""
AltOrigSymMates.moving.model="4CK_data.pdb"
AltOrigSymMates.fixed.model="hetatm_among_atoms.pdb"
AltOrigSymMates.movehetatms = True
AltOrigSymMates.outputfileprefix = test8
AltOrigSymMates.write_cif = True
"""
,
#9
r"""
AltOrigSymMates.fixed.mrsolution
{
  solfname = "AragonStriderGandalfFamos.sol"
  ensembles
  {
    name = "MR_1WZA_A"
    model = "sculpt_1WZA_A.pdb"
  }
}
AltOrigSymMates.moving.mrsolution
{
  solfname = "Ranger1WZA_A.sol"
  ensembles
  {
    name = "MR_1WZA_A"
    model = "sculpt_1WZA_A.pdb"
  }
}
AltOrigSymMates.spacegroupfname="1cxf.mtz"
AltOrigSymMates.writepdbfiles = False
AltOrigSymMates.goodmatch = 0.8
AltOrigSymMates.nproc =8
AltOrigSymMates.outputfileprefix = test9
"""
,
#10
r"""
AltOrigSymMates.moving.model="test1s.pdb"
AltOrigSymMates.fixed.model="test2s.pdb"
AltOrigSymMates.movehetatms = True
AltOrigSymMates.outputfileprefix = test10
AltOrigSymMates.write_cif = True
"""
,
#11, 6 chains where a floating origin is ambiguous
r"""
AltOrigSymMates.moving.model="b-CA-mr_phaser.1.pdb"
AltOrigSymMates.fixed.model="2a8d.cif"
AltOrigSymMates.verbose=True
AltOrigSymMates.outputfileprefix = test11
"""
,
#12, P 1 case
r"""
AltOrigSymMates.moving.model = "6myf_A_P1-shift.pdb"
AltOrigSymMates.fixed.model = "6myf_A-P1.pdb"
AltOrigSymMates.outputfileprefix = test12
AltOrigSymMates.write_cif = True
"""
,
#13, MR solution with multiple chains placed on different symmetry positions but 
#    all matching different parts of the same target chain
r"""
AltOrigSymMates.moving.model = "1W8O_phaser.1.pdb"
AltOrigSymMates.fixed.model = "1w8o.pdb"
AltOrigSymMates.outputfileprefix = test13
"""
,
]


def extract_famos_remarks(pdbfname):
  famosremarks = ""
  if os.path.exists(pdbfname + ".pdb"):
    fname = pdbfname + ".pdb"
  else:
    fname = pdbfname + ".cif"
  with open( fname, "r") as pdbfile:
    for line in pdbfile.readlines():
      if "REMARK FAMOS " in line:
        famosremarks += line
  # The following regexp works both for REMARK FAMOS in pdb files and for the FAMOS remark 
  # in the _software.description field in cif files emitted by AltOrigSymMates.
  matches = re.findall("\s* FAMOS \s*  '? (\d+\.?\d*) \s* (\d+\.?\d*) \s* (\w+) \s* (\w+) \s* " + \
   "(\(\S+\,\S+\,\S+\)) \s* (\(\-?\d+\.?\d*\, \s* \-?\d+\.?\d*\, \s* \-?\d+\.?\d*\)) '? ",
   famosremarks, re.MULTILINE |re.VERBOSE )
  #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
  return matches


def tryfloat(val):
  if isinstance(val, tuple) or isinstance(val, list):
    val = list(val)
    for i,v in enumerate(val):
      val[i] = tryfloat(v)
  else:
    try:
      val = float(val)
    except ValueError as e:
      pass
  return val


def AreFamosAnnotationsApproxEqual(annos1, annos2, verbose=False):
  chk1 = (len(annos1) == len(annos2))
  anns1 = tryfloat(annos1)
  anns2 = tryfloat(annos2)
  #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
  for anno1 in anns1:
    for anno2 in anns2:
      if anno1[2:4] == anno2[2:4]: # matching pairs of chainids
        chk2 = (abs(anno1[0]-anno2[0]) < 0.001) # mathcing MLAD values
        chk3 = (abs(anno1[1]-anno2[1]) < 0.001) # matching RMSD values
        chk4 = (anno1[4] == anno2[4]) # mathcing symop
        chk5 = (anno1[5][0]==anno2[5][0] and anno1[5][1]==anno2[5][1] and anno1[5][2]==anno2[5][2]) # matching altorig
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        if "verbose" in sys.argv and not (chk1 and chk2 and chk3 and chk4 and chk5):
          print(str(annos1))
          print(" != ")
          print(str(annos2))
          return False
  return True


def exercise(j, tst):
  fname = "testphil%d.txt" %j
  with open(fname, "w") as fh:
    fh.write(tst)
  strarg = "phenix.find_alt_orig_sym_mate %s" %fname
  if "verbose" in sys.argv:
    print("*" * len(strarg))
    print(strarg)
    print("*" * len(strarg))
    sys.stdout.flush()
    assert ( easy_run.call(command=strarg)  == 0 )
  else:
    result = easy_run.fully_buffered(strarg)
    if not result.return_code == 0:
      nlines = 50
      for line in result.stdout_lines[-nlines:]:
        print(line)
      for line in result.stderr_lines[-nlines:]:
        print(line)
      print("Runtime error " + str(result.return_code))
    assert result.return_code == 0
  refFamosAnnotation = extract_famos_remarks("reference%d" %j)
  testFamosAnnotation = extract_famos_remarks("test%d" %j)

  if not AreFamosAnnotationsApproxEqual( refFamosAnnotation, testFamosAnnotation):
    print( "".join( additional_regressions.ReadPhaserRevisionRemark("reference%d.pdb" %j)))
    print( "".join( additional_regressions.ReadPhaserRevisionRemark("test%d.pdb" %j)))
    assert False


if __name__ == '__main__':
  if "verbose" in sys.argv:
    print(" To retain output files use the command line argument 'dirty'")
  cleanup = True
  if "dirty" in sys.argv:
    cleanup = False

  testdir = "test_find_alt_orig_sym_mate"
  # copy models, data, reference models and test input
  inputfpath = os.path.join(libtbx.env.under_dist("phaser_regression", "additional_regressions"),
   "find_alt_orig_sym_mate", "input_files")
  shutil.rmtree(testdir, ignore_errors=True)
  shutil.copytree(inputfpath, testdir)
  os.chdir(testdir)
  if "all" in sys.argv:
    # run each of the phil input from the list
    for i,test in enumerate(testslst):
      exercise(i, test)
  else:
    i = None
    for arg in sys.argv:
      try:
        i = int(arg)
      except Exception as e:
        pass
    if i is not None:
      exercise(i, testslst[i])
    else: # just take a few selected test
      exercise(1, testslst[1])
      exercise(2, testslst[2])
      exercise(1, testslst[5])
      exercise(11, testslst[11])
      exercise(11, testslst[13])
      #print("Invalid test script specified. Must be \"all\" or a script number from 0 up to %d" %(len(testslst)-1))

  os.chdir("..")
  if cleanup: # now clean up
    shutil.rmtree(testdir)
  print("OK")
