
from __future__ import print_function
from __future__ import division

from libtbx import easy_run
import libtbx.load_env
import sys, os, os.path
from phaser_regression import additional_regressions


def exercise(dirty=False) :
  dpath = os.path.join(libtbx.env.under_dist("phaser_regression", "additional_regressions"), "extract_alignments")
  hmlgfn = os.path.join(dpath, "T0971.hhr")

  strargs = "phaser.extract_alignments homology_search=%s prefix=test" %hmlgfn
  result = easy_run.fully_buffered(strargs)
  if not result.return_code == 0:
    nlines = 50
    for line in result.stdout_lines[-nlines:]:
      print(line)
    for line in result.stderr_lines[-nlines:]:
      print(line)
    print("Runtime error " + str(result.return_code))
  assert result.return_code == 0

  fname1 = "test_1_3EBT_A.aln"
  fname2 =  os.path.join(dpath, "T0971_1_3EBT_A.aln")
  difftxtlst = additional_regressions.DifflistFiles(fname1, fname2)

  # tidy up
  if not dirty:
    tmpfiles = ["test_1_3EBT_A.aln" ]
    for f in tmpfiles:
      try:
        os.remove(f)
      except Exception as e:
        pass

  if difftxtlst != []:
    sys.stdout.writelines(difftxtlst)
  assert difftxtlst == []

if (__name__ == "__main__"):
  if len(sys.argv) > 1:
    exercise(sys.argv[1] == "dirty")
  else:
    exercise()
  print("OK")
