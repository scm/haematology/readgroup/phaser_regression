from __future__ import print_function
from __future__ import division

from libtbx import easy_run
import libtbx.load_env
import sys, os, os.path
from phaser_regression import additional_regressions


def exercise(dirty=False) :
  dpath = os.path.join(libtbx.env.under_dist("phaser_regression", "additional_regressions"), "sculptor_new")
  modelfname = os.path.join(dpath, "1KJT.pdb")
  alnfname = os.path.join(dpath, "clust.aln")

  strargs = "phaser.sculptor_new input.model.file_name=%s output.root=test_sculptor_new " %modelfname + \
   "input.alignment.file_name=%s input.model.selection=\"chain 'A'\"" %alnfname
  result = easy_run.fully_buffered(strargs)
  if not result.return_code == 0:
    nlines = 50
    for line in result.stdout_lines[-nlines:]:
      print(line)
    for line in result.stderr_lines[-nlines:]:
      print(line)
    print("Runtime error " + str(result.return_code))
  assert result.return_code == 0

  fname1 = "test_sculptor_new_1KJT.pdb"
  fname2 =  os.path.join(dpath, "sculptor_new_1KJT.pdb")
  ret = additional_regressions.PdbFcalcCorrelation(fname1, fname2)
  difftxtlst = additional_regressions.DifflistFiles(fname1, fname2,
                                                    ignorestring="REMARK PHASER REVISION")
  rev1 = additional_regressions.ReadPhaserRevisionRemark(fname1)
  rev2 = additional_regressions.ReadPhaserRevisionRemark(fname2)

  # tidy up
  if not dirty:
    tmpfiles = ["test_sculptor_new_1KJT.pdb" ]
    for f in tmpfiles:
      try:
        os.remove(f)
      except Exception as e:
        pass

  if ( ret != (1.0, 1.0) ):
    sys.stdout.writelines(rev1)
    sys.stdout.writelines(rev2)
    sys.stdout.writelines(difftxtlst)
  assert ( ret == (1.0, 1.0) )
  return 0

if (__name__ == "__main__"):
  if len(sys.argv) > 1:
    exercise(sys.argv[1] == "dirty")
  else:
    exercise()
  print("OK")
