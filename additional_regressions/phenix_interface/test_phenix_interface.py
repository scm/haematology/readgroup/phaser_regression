from __future__ import print_function
from __future__ import division
from phaser.phenix_interface import driver
from phaser import phenix_interface
import libtbx.load_env
from libtbx import easy_pickle
import libtbx.utils
import shutil
import os
import sys
if sys.version_info.major > 2:
  from io import StringIO
else:
  from cStringIO import StringIO


libtbx.utils.disable_tracebacklimit = True

class test_cb (object) :
  def __init__ (self) :
    self.messages = []

  def __call__ (self, message, data, accumulate=True, cached=True) :
    self.messages.append(message)

def copy_file_to_cwd (file_name) :
  full_path = libtbx.env.find_in_repositories(
    relative_path="phaser/test/%s" % file_name,
    test=os.path.isfile)
  if (full_path is None) :
    full_path = libtbx.env.find_in_repositories(
      relative_path="phaser/tutorial/%s" % file_name,
      test=os.path.isfile)
  assert (full_path is not None)
  dest_path = os.path.join(os.getcwd(), file_name)
  shutil.copy(full_path, dest_path)

def exercise_mr (args) :
  input_files = ["beta.pdb", "blip.pdb", "beta_blip_P3221.mtz"]
  for file_name in input_files :
    copy_file_to_cwd(file_name)
  eff_file = libtbx.env.find_in_repositories(
    relative_path="phaser/test/phenix_interface/beta_blip.eff",
    test=os.path.isfile)
  out = StringIO()
  driver.run(args=["--show_defaults"], out=out)
  out2 = StringIO()
  phenix_interface.master_phil().show(out=out2)
  assert out.getvalue() == out2.getvalue()
  if ("--verbose" in args) :
    out = sys.stdout
  else :
    out = StringIO()
  cb = test_cb()
  libtbx.call_back.register_handler(cb)
  driver.run(args=[eff_file,"--show_script","--test_mode"],
    out=out)
  if (not "--no-run" in args) :
    if ("--verbose" in args) :
      out = sys.stdout
    else :
      out = StringIO()
    result = driver.run(args=[eff_file,], out=out)
    result_ = result.get_phaser_object()
#   assert len(cb.messages) > 1000
    assert result_.getTopLLG() > 200 # ~256.6
    assert os.path.isfile(result_.getTopPdbFile())
    easy_pickle.dump("mr.pkl", result)
    result2 = easy_pickle.load("mr.pkl")
    assert result2.get_phaser_object().getTopLLG() > 200
    # run 1: beta only
    eff_file = libtbx.env.find_in_repositories(
      relative_path="phaser/test/phenix_interface/beta_blip1.eff",
      test=os.path.isfile)
    result = driver.run(args=[eff_file,], out=out)
    easy_pickle.dump("beta_solution.pkl", result)
    # run 2: start from partial solution (as pickle file)
    eff_file = libtbx.env.find_in_repositories(
      relative_path="phaser/test/phenix_interface/beta_blip2.eff",
      test=os.path.isfile)
    result = driver.run(args=[eff_file,], out=out)
    result_ = result.get_phaser_object()
#   assert len(cb.messages) > 1000
    assert result_.getTopLLG() > 200 # ~256.6
    assert os.path.isfile(result_.getTopPdbFile())
    easy_pickle.dump("mr.pkl", result)
    result2 = easy_pickle.load("mr.pkl")
    assert result2.get_phaser_object().getTopLLG() > 200

def exercise_sad (args) :
  input_files = ["S-insulin_hyss.pdb", "S-insulin.mtz", "S-insulin.seq"]
  for file_name in input_files :
    copy_file_to_cwd(file_name)
  eff_file = libtbx.env.find_in_repositories(
    relative_path="../phaser/test/phenix_interface/insulin.eff",
    test=os.path.isfile)
  assert (eff_file is not None)
  for file_name in ["insulin.mtz", "insulin_data.mtz"] :
    if os.path.exists(file_name) :
      os.remove(file_name)
  if ("--verbose" in args) :
    out = sys.stdout
  else :
    out = StringIO()
  out = StringIO()
  driver.run(args=[eff_file,"--show_script","--test_mode"],
    out=out)
  if (not "--no-run" in args) :
    if ("--verbose" in args) :
      out = sys.stdout
    else :
      out = StringIO()
    result = driver.run(args=[eff_file, "--test_mode"], out=out)
    easy_pickle.dump("sad.pkl", result)
    assert os.path.isfile("insulin.1.mtz")

def exercise_sad_simple (args) :
  from phaser import tst_phenix_interface_ep
  tst_phenix_interface_ep.exercise(verbose=("--verbose" in args))

def exercise_mr_simple (args) :
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/wizards/command_line_misc_tests/test_phaser/coords.pdb",
    test=os.path.isfile)
  sca_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/wizards/command_line_misc_tests/test_phaser/native.sca",
    test=os.path.isfile)
  seq_file = libtbx.env.find_in_repositories(
    relative_path="phenix_regression/wizards/command_line_misc_tests/test_phaser/seq.dat",
    test=os.path.isfile)
  if (pdb_file is None) :
    return
  args = [pdb_file, sca_file, seq_file, "model_rmsd=0.85", "root=simple","tncs.nmol=1",
          "--test_mode"]
  if ("--verbose" in args) :
    out = sys.stdout
  else :
    out = StringIO()
  result = driver.run(args=args,
    out=out)
  assert os.path.isfile("simple.1.pdb") and os.path.isfile("simple.1.mtz")

def exercise (args) :
  from phaser import phenix_interface
  master_phil = phenix_interface.master_phil(validate=True)
  libtbx.phil.interface.validate_choice_captions(master_phil)
  pdb_raw = """\
REMARK PHASER ENSEMBLE MODEL 0 ID 27.1
CRYST1   21.937    4.866   23.477  90.00 107.08  90.00 P 1 21 1      2
ORIGX1      1.000000  0.000000  0.000000        0.00000
ORIGX2      0.000000  1.000000  0.000000        0.00000
ORIGX3      0.000000  0.000000  1.000000        0.00000
SCALE1      0.045585  0.000000  0.014006        0.00000
SCALE2      0.000000  0.205508  0.000000        0.00000
SCALE3      0.000000  0.000000  0.044560        0.00000
ATOM      1  N   GLY A   1      -9.009   4.612   6.102  1.00 16.77           N
ATOM      2  CA  GLY A   1      -9.052   4.207   4.651  1.00 16.57           C
ATOM      3  C   GLY A   1      -8.015   3.140   4.419  1.00 16.16           C
ATOM      4  O   GLY A   1      -7.523   2.521   5.381  1.00 16.78           O
ATOM      5  N   ASN A   2      -7.656   2.923   3.155  1.00 15.02           N
ATOM      6  CA  ASN A   2      -6.522   2.038   2.831  1.00 14.10           C
ATOM      7  C   ASN A   2      -5.241   2.537   3.427  1.00 13.13           C
ATOM      8  O   ASN A   2      -4.978   3.742   3.426  1.00 11.91           O
ATOM      9  CB  ASN A   2      -6.346   1.881   1.341  1.00 15.38           C
ATOM     10  CG  ASN A   2      -7.584   1.342   0.692  1.00 14.08           C
ATOM     11  OD1 ASN A   2      -8.025   0.227   1.016  1.00 17.46           O
ATOM     12  ND2 ASN A   2      -8.204   2.155  -0.169  1.00 11.72           N
ATOM     13  N   ASN A   3      -4.438   1.590   3.905  1.00 12.26           N
ATOM     14  CA  ASN A   3      -3.193   1.904   4.589  1.00 11.74           C
ATOM     15  C   ASN A   3      -1.955   1.332   3.895  1.00 11.10           C
ATOM     16  O   ASN A   3      -1.872   0.119   3.648  1.00 10.42           O
ATOM     17  CB  ASN A   3      -3.259   1.378   6.042  1.00 12.15           C
ATOM     18  CG  ASN A   3      -2.006   1.739   6.861  1.00 12.82           C
ATOM     19  OD1 ASN A   3      -1.702   2.925   7.072  1.00 15.05           O
ATOM     20  ND2 ASN A   3      -1.271   0.715   7.306  1.00 13.48           N
"""
  open("tst_phaser_interface.pdb", "w").write(pdb_raw)
  import iotbx.pdb
  pdb_in = iotbx.pdb.input("tst_phaser_interface.pdb")
  from phaser.phenix_interface import check_for_model_identity_remark
  n_cards = check_for_model_identity_remark(pdb_in)
  assert (n_cards == 1)
  exercise_mr_simple(args)
  exercise_sad_simple(args)
  if (not "--quick" in args) :
    exercise_mr(args)
    exercise_sad(args)
  print("OK")

if __name__ == "__main__" :
  if (not os.path.isdir("tst_phenix_interface")) :
    os.mkdir("tst_phenix_interface")
  os.chdir("tst_phenix_interface")
  exercise(sys.argv[1:])
  os.chdir("..")
  shutil.rmtree("tst_phenix_interface")

