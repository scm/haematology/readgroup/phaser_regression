
from __future__ import print_function
from __future__ import division

from libtbx import easy_run
from phaser.AltOrigSymmatesEngine import GetRMSDfromAtoms
from iotbx import pdb
import libtbx.load_env
import sys, os, os.path
from phaser_regression import additional_regressions


def exercise(dirty=False) :
  dpath = os.path.join(libtbx.env.under_dist("phaser_regression", "additional_regressions"), "sceds")
  modelfname = os.path.join(dpath, "1npi.pdb")

  strargs = "phaser.sceds root=test file_name=%s" %modelfname
  result = easy_run.fully_buffered(strargs)
  if not result.return_code == 0:
    nlines = 50
    for line in result.stdout_lines[-nlines:]:
      print(line)
    for line in result.stderr_lines[-nlines:]:
      print(line)
    print("Runtime error " + str(result.return_code))
  assert result.return_code == 0

  fname1 = "test.sceds.pdb" # resultant file emerging from running ensembler
  fname2 =  os.path.join(dpath, "reference.pdb") # reference file to compare with
  ret = additional_regressions.PdbFcalcCorrelation(fname1, fname2)
  difftxtlst = additional_regressions.DifflistFiles(fname1, fname2,
                                                    ignorestring="REMARK PHASER REVISION")
  rev1 = additional_regressions.ReadPhaserRevisionRemark(fname1)
  rev2 = additional_regressions.ReadPhaserRevisionRemark(fname2)

  # tidy up
  if not dirty:
    tmpfiles = ["test.sceds.pdb", "test_selections.phil", "test.log" ]
    for f in tmpfiles:
      try:
        os.remove(f)
      except Exception as e:
        pass

  if ( ret != (1.0, 1.0) ):
    sys.stdout.writelines(rev1)
    sys.stdout.writelines(rev2)
    sys.stdout.writelines(difftxtlst)
  assert ( ret == (1.0, 1.0) )

if (__name__ == "__main__"):
  if len(sys.argv) > 1:
    exercise(sys.argv[1] == "dirty")
  else:
    exercise()
  print("OK")
