from phaser.phenix_adaptors import sad_target
from libtbx.utils import user_plus_sys_time
from libtbx import easy_pickle
from libtbx import easy_run
import time
import sys

class opcontrol(object):

  def __init__(self, flag, delay=0.5):
    self.flag = flag
    self.delay = delay
    if (self.flag):
      easy_run.call("opcontrol_start")
      time.sleep(self.delay)

  def shutdown(self):
    if (self.flag):
      easy_run.call("opcontrol_shutdown")
      time.sleep(self.delay)

def run(args):
  assert len(args) == 0
  f_obs, r_free_flags = easy_pickle.load("data_adaptor.pickle")
  xray_structure = easy_pickle.load("xray_structure.pickle")
  xray_structure.show_summary()
  f_calc = easy_pickle.load("f_calc.pickle")
  print "have data"
  timer = user_plus_sys_time()
  data_adaptor = sad_target.data_adaptor(
    f_obs=f_obs, r_free_flags=r_free_flags, verbose=True)
  print "have data_adaptor %.2f" % timer.elapsed()
  oc = opcontrol(0)
  timer = user_plus_sys_time()
  target = data_adaptor.target(xray_structure=xray_structure)
  oc.shutdown()
  print "have target %.2f" % timer.elapsed()
  timer = user_plus_sys_time()
  target.set_f_calc(f_calc=f_calc)
  print "done set_f_calc %.2f" % timer.elapsed()
  oc = opcontrol(0)
  timer = user_plus_sys_time()
  target.refine_variance_terms()
  oc.shutdown()
  print "have refine_variance_terms %.2f" % timer.elapsed()
  oc = opcontrol(0)
  timer = user_plus_sys_time()
  target.refine_variance_terms()
  oc.shutdown()
  print "have refine_variance_terms again %.2f" % timer.elapsed()
  timer = user_plus_sys_time()
  print "target.functional():", target.functional(use_working_set=True)
  print "have functional %.2f" % timer.elapsed()
  timer = user_plus_sys_time()
  print "target.gradients():", target.gradients()
  print "have gradients %.2f" % timer.elapsed()

if (__name__ == "__main__"):
  run(sys.argv[1:])
