import phaser
import phaser.phenix_adaptors.sad_target
from cctbx.development import debug_utils
from cctbx.development import random_structure
from cctbx import xray
from cctbx import miller
from cctbx import crystal
import cctbx.xray.structure_factors
from cctbx.array_family import flex
from libtbx.utils import format_cpu_times
#from libtbx.utils import null_out # XXX 2006-10-11 backward compatibility
class null_out(object):
  def isatty(self): return False
  def close(self): pass
  def flush(self): pass
  def write(self, str): pass
  def writelines(self, sequence): pass
import random
import sys

if (1):
  random.seed(0)
  flex.set_random_seed(0)

class refine_sad_input_descrambler:

  def __init__(self,
        space_group,
        unit_cell,
        indices,
        r_free_flags,
        f_obs_p,
        f_obs_m,
        sigmas_p,
        sigmas_m,
        present_p,
        present_m,
        output):
    self.sel = present_p.concatenate(present_m)
    self.miller_set = miller.set(
      crystal_symmetry=crystal.symmetry(
        space_group=space_group,
        unit_cell=unit_cell),
      anomalous_flag=True,
      indices=indices.concatenate(-indices).select(self.sel))
    self.f_obs = self.miller_set.array(
      data=f_obs_p.concatenate(f_obs_m).select(self.sel),
      sigmas=sigmas_p.concatenate(sigmas_m).select(self.sel)).map_to_asu()

  def process_f_calc(self, p, m):
    self.f_calc = self.miller_set.array(
      data=p.concatenate(m).select(self.sel)).map_to_asu()

  def process_r_free_flags(self, pm):
    self.r_free_flags = self.miller_set.array(
      data=~pm.concatenate(pm).select(self.sel)).map_to_asu()

def finite_difference_gradients(
      space_group,
      adaptor,
      use_working_set,
      refine_sad,
      f_calc,
      grads_fc_ana,
      max_true_comparison=10,
      eps=1.e-3,
      log=None):
  ga_dict = {}
  for h,g in zip(grads_fc_ana.miller, grads_fc_ana.dL_by_dFC):
    ga_dict[h] = g
  is_centric = space_group.is_centric
  phase_restriction = space_group.phase_restriction
  #
  def show_compare(pm, h, f):
    if (pm == "-"): h = tuple([-v for v in h])
    pr = phase_restriction(h)
    a = ga_dict.get(h)
    print >> log, pm+"   ", h,
    if (pr.is_centric()): print >> log, "centric", pr.ht_angle(deg=True),
    print >> log
    print >> log, pm+"   sad a:", a
    print >> log, pm+"   sad f:", f
    if (a is None): a = 0j
    d = abs(a-f)
    if (d > 1.e-5):
      if (d > 1.e-3): print >> log, pm+"SAD ANA/FIN ERROR",
      else:           print >> log, pm+"SAD ANA/FIN MISMATCH",
      if (is_centric(h)): print >> log, "centric",
      print >> log, d
  #
  mi = adaptor.indices
  fc_p, fc_m = adaptor.split_plus_minus(f_calc.data())
  fl_p = adaptor.r_free_flags_p
  fl_m = adaptor.r_free_flags_m
  fp_p = adaptor.present_p
  fp_m = adaptor.present_m
  assert fc_p.size() == mi.size()
  assert fc_m.size() == mi.size()
  assert fp_p.size() == mi.size()
  assert fp_m.size() == mi.size()
  n_true_comparisons = 0
  for i in flex.random_permutation(size=mi.size()):
    for pm,fc_x,fl_x,fp_x in [("+", fc_p, fl_p, fp_p),
                              ("-", fc_m, fl_m, fp_m)]:
      if (fl_x[i] == use_working_set): continue
      if (not fp_x[i]):
        g_ab = 0j
      else:
        fc0 = fc_x[i]
        gs = []
        for part in [0,1]:
          fs = []
          for signed_eps in [eps, -eps]:
            if (part == 0):
              fc_x[i] = complex(fc0.real+signed_eps, fc0.imag)
            else:
              fc_x[i] = complex(fc0.real, fc0.imag+signed_eps)
            refine_sad.setFC(fc_p, flex.conj(fc_m))
            fs.append(refine_sad.targetFn())
            fc_x[i] = fc0
          print >> log, pm+" ", fs
          gs.append((fs[0]-fs[1])/(2*eps))
        g_ab = complex(*gs)
        n_true_comparisons += 1
      show_compare(pm, h=mi[i], f=g_ab)
      if (    max_true_comparison is not None
          and n_true_comparisons == max_true_comparison):
        return

max_relative_errors = flex.double()

def run_call_back(
      flags,
      space_group_info,
      n_scatterers=3,
      d_min=2,
      finite_eps=1.e-5,
      skip_centric_space_groups=False):
  if (flags.Verbose):
    log = sys.stdout
  else:
    log = null_out()
  if (skip_centric_space_groups and space_group_info.group().is_centric()):
    print >> log, "Skipping centric space group:", space_group_info
    print >> log
    return
  structure_ideal = random_structure.xray_structure(
    space_group_info=space_group_info,
    n_scatterers=n_scatterers,
    elements="random",
    volume_per_atom=500,
    general_positions_only=True,
    random_f_prime_d_min=min(1, d_min),
    random_f_double_prime=True,
    use_u_aniso=False,
    use_u_iso=True,
    random_u_iso=True,
    random_u_iso_scale=0.3,
    random_occupancy=False)
  structure_ideal.show_summary(f=log).show_scatterers(f=log)
  #
  structure_shake = structure_ideal.random_modify_parameters(
    parameter_name="site")
  structure_shake.show_summary(f=log).show_scatterers(f=log)
  print >> log, "rms difference ideal/shake: %.6g" \
    % structure_ideal.rms_difference(structure_shake)
  #
  f_obs = abs(structure_ideal.structure_factors(
    d_min=d_min, algorithm="direct", cos_sin_table=False).f_calc())
  f_obs = f_obs.customized_copy(sigmas=flex.sqrt(f_obs.data()))
  f_obs = f_obs.select(
    flex.random_bool(size=f_obs.indices().size(), threshold=0.95))
  f_calc = f_obs.structure_factors_from_scatterers(
    xray_structure=structure_shake,
    algorithm="direct", cos_sin_table=False).f_calc()
  r_free_flags = f_obs.generate_r_free_flags()
  print >> log, "\nR-free flags: total= %5d work= %5d test= %5d "%(
    r_free_flags.data().size(),
    r_free_flags.data().count(False),
    r_free_flags.data().count(True))
  #
  data_adaptor = phaser.phenix_adaptors.sad_target.data_adaptor(
    f_obs=f_obs, r_free_flags=r_free_flags, verbose=flags.Verbose)
  #
  descrambled = refine_sad_input_descrambler(
    *data_adaptor.refine_sad_input_tuple(xray_structure=structure_shake))
  assert descrambled.f_obs.indices().size() == f_obs.indices().size()
  cmn = descrambled.f_obs.common_set(f_obs)
  assert cmn.indices().all_eq(f_obs.indices())
  assert flex.max(flex.abs(cmn.data()-f_obs.data())) < 1.e-8
  if (f_obs.sigmas() is not None):
    assert flex.max(flex.abs(cmn.sigmas()-f_obs.sigmas())) < 1.e-8
  p, m = data_adaptor.split_plus_minus(f_calc.data())
  descrambled.process_f_calc(p=p, m=m)
  cmn = descrambled.f_calc.common_set(f_obs)
  assert cmn.indices().all_eq(f_obs.indices())
  assert flex.max(flex.abs(cmn.data() - f_calc.data())) < 1.e-8
  descrambled.process_r_free_flags(pm=data_adaptor.r_free_flags_pm)
  cmn = descrambled.r_free_flags.common_set(f_obs)
  assert cmn.indices().all_eq(f_obs.indices())
  assert cmn.data().all_eq(r_free_flags.data())
  #
  refine_variance_terms = not flags.unrefined_variance_terms
  if (space_group_info.group().is_centric()):
    # to avoid issue in Minimizer.cc CVS revision 1.39.2.3
    refine_variance_terms = False
  #
  target_obj = data_adaptor.target(xray_structure=structure_shake)
  target_obj.set_f_calc(f_calc=f_calc)
  target_obj.reject_outliers()
  scaleK = target_obj.refine_sad_instance.get_refined_scaleK()
  if (refine_variance_terms and scaleK < 0):
    target_obj.refine_variance_terms()
  for use_working_set in [True, False]:
    target_obj.functional(use_working_set=use_working_set)
    #
    grads_fc_ana_raw = target_obj.gradients_raw()
    assert grads_fc_ana_raw.miller.size() == grads_fc_ana_raw.dL_by_dFC.size()
    if (use_working_set):
      f_obs_s = f_obs.select(~r_free_flags.data())
    else:
      f_obs_s = f_obs.select(r_free_flags.data())
    grads = miller.array(
      miller_set=miller.set(
        crystal_symmetry=structure_shake.crystal_symmetry(),
        indices=grads_fc_ana_raw.miller,
        anomalous_flag=True),
      data=grads_fc_ana_raw.dL_by_dFC)
    assert f_obs_s.map_to_asu().indices().all_eq(f_obs_s.indices())
    assert f_obs_s.is_unique_set_under_symmetry()
    assert grads.map_to_asu().indices().all_eq(grads.indices())
    assert grads.is_unique_set_under_symmetry()
    assert f_obs_s.match_indices(grads).singles(1).size() == 0
    del f_obs_s
    del grads
    #
    finite_difference_gradients(
      space_group=f_obs.space_group(),
      adaptor=target_obj.data_adaptor,
      use_working_set=target_obj.use_working_set,
      refine_sad=target_obj.refine_sad_instance,
      f_calc=f_calc,
      grads_fc_ana=grads_fc_ana_raw,
      max_true_comparison=50,
      log=log)
    #
    xray.set_scatterer_grad_flags(
      scatterers=structure_shake.scatterers(),
      site=True,
      u_iso=False,
      u_aniso=False,
      occupancy=False,
      fp=False,
      fdp=False)
    log = sys.stdout
    da_db, daa_dbb_dab = target_obj.derivatives(curvs=True)
    if (target_obj.number_of_outliers != 0):
      print >> log, "phaser outliers:", target_obj.number_of_outliers
    grads_sites_ana = xray.structure_factors.gradients_direct(
      xray_structure=structure_shake,
      u_iso_refinable_params=None,
      miller_set=da_db,
      d_target_d_f_calc=da_db.data(),
      n_parameters=0).d_target_d_site_cart()
    del da_db, daa_dbb_dab
    #
    uc = structure_shake.unit_cell()
    frac = uc.fractionalize
    orth = uc.orthogonalize
    grads_sites_fin = flex.vec3_double()
    grads_sites_fin.reserve(structure_shake.scatterers().size())
    for scatterer in structure_shake.scatterers():
      site_frac = scatterer.site
      site_cart = orth(site_frac)
      gs = []
      for ic in [0,1,2]:
        site_cart_eps = list(site_cart)
        fs = []
        for signed_eps in [finite_eps, -finite_eps]:
          site_cart_eps[ic] = site_cart[ic] + signed_eps
          scatterer.site = frac(site_cart_eps)
          f_calc_eps = f_obs.structure_factors_from_scatterers(
            xray_structure=structure_shake,
            algorithm="direct", cos_sin_table=False).f_calc()
          p, m = target_obj.data_adaptor.split_plus_minus(
            data=f_calc_eps.data())
          target_obj.refine_sad_instance.setFC(p, flex.conj(m))
          fs.append(target_obj.refine_sad_instance.targetFn())
        gs.append((fs[0]-fs[1])/(2*finite_eps))
      scatterer.site = site_frac
      grads_sites_fin.append(gs)
    #
    print >> log, f_obs.space_group_info()
    gm = flex.max(flex.abs(grads_sites_ana.as_double()))
    em = 0
    for a,f in zip(grads_sites_ana, grads_sites_fin):
      print >> log, "a:", a
      print >> log, "f:", f
      if (gm != 0):
        e = tuple([abs(x-y)/gm for x,y in zip(a,f)])
        print >> log, "e:", e
        em = max(em, max(e))
      print >> log
    if (f_obs.space_group().is_centric()):
      print >> log, "C",
    else:
      print >> log, "A",
    print >> log, "max relative error  %.6f  %%centrics=%.2f  %s" % (
      em,
      f_obs.centric_flags().data().count(True)
        * 100. / f_obs.size(),
      f_obs.space_group_info())
    max_relative_errors.append(em)
    print >> log

def run(args):
  debug_utils.parse_options_loop_space_groups(
    argv=args,
    call_back=run_call_back,
    keywords=("unrefined_variance_terms",),
    show_cpu_times=False)
  print "max relative error statistics:"
  stats = max_relative_errors.min_max_mean()
  stats.show(prefix="  ")
  if (stats.n != 0):
    assert stats.max < 1.e-3
  print format_cpu_times()

if (__name__ == "__main__"):
  run(sys.argv[1:])
