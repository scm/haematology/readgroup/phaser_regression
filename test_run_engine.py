from phaser_regression import test_phaser_keyword as keyword

import os, sys
from datetime import datetime

def get_prefix(tmp_dir):

    return os.path.join( tmp_dir, "PHASER" )

def python(input, stream, tmp_dir, ncpus, resfilesuffix):

    if not os.path.isdir( tmp_dir ):
        print( "Creating temporary directory %s..." % tmp_dir)
        os.mkdir( tmp_dir )

    instructions = input.mode.python_input( ncpus = ncpus )
    input.apply_python_xray_data( input = instructions )

    for kw in input.keywords:
        kw.apply_python( input = instructions )

    prefix = get_prefix( tmp_dir = tmp_dir )
    keyword.Root( prefix = prefix ).apply_python( input = instructions )

    stream.write( "Executing input...\n" )
    start = datetime.now()
    result = input.mode.python_run( input = instructions, stream = stream )
    stream.write( "...complete\n" )
    runtime = datetime.now() - start

    report = input.evaluate( input = input, result = result, prefix = prefix )
    return ( report, runtime )


def python_basic(input, stream, tmp_dir, ncpus, resfilesuffix):

    stream.write( "# Running test using phaser python interface (basic mode) #\n" )
    return python( input = input, stream = stream, tmp_dir = tmp_dir,
                    ncpus = ncpus,
                    resfilesuffix = resfilesuffix )


def python_safe(input, stream, tmp_dir, ncpus, resfilesuffix):

    stream.write( "# Running test with phaser python interface using crash-safe mode #\n" )
    import pickle
    import tempfile
    ( handle, infile ) = tempfile.mkstemp( suffix = ".in.pickle", dir = tmp_dir )
    ( handle, outfile ) = tempfile.mkstemp( suffix = ".out.pickle", dir = tmp_dir )

    instream = open( infile, "w+b" )
    pickle.dump( ( input, tmp_dir, ncpus, outfile, resfilesuffix ), instream )
    instream.close()
    stream.flush()

    import subprocess
    import sys
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    process = subprocess.Popen(
        ( sys.executable, __file__, infile ),
        stdout = stream,
        stderr = subprocess.STDOUT,
        )

    ( log, err ) = process.communicate()

    if process.returncode != 0:
        raise RuntimeError( "Error exit from process")

    stream.write( "Reading output..." )

    try:
        result = pickle.load( open( outfile, "r+b" ) )

    except pickle.UnpicklingError as e:
        raise RuntimeError( "Unexpected output: %s" % e)

    stream.write( "complete\n" )

    return result


def get_phaser_version():
    #return "waffle wibble"
    import libtbx.load_env
    bin_path = libtbx.env.under_build( "bin" )
    phaser_exe = None

    for trial_phaser in [ "phenix.phaser", "phenix.phaser.bat", "phaser", "phaser.bat" ]:
        if os.path.exists( os.path.join( bin_path, trial_phaser ) ):
            phaser_exe = os.path.join( bin_path, trial_phaser )
            break

    else:
        raise RuntimeError( "No phaser executable was found in %s" % bin_path)

    import subprocess
    process = subprocess.Popen([ phaser_exe, "--version"],
                                stdin = subprocess.PIPE,
                                stdout = subprocess.PIPE,
                                stderr = subprocess.STDOUT,
                              )

    version =  process.communicate()[0]
    # return something like 'Phaser-2.7.16 (svn 7793) (commits: 6847, SHA-1: 75ff0fc... , branch: master)'
    return version.splitlines()[0]



def script(input, stream, tmp_dir, ncpus, resfilesuffix):

    stream.write( "# Running test using phaser executable #\n" )

    if not os.path.isdir( tmp_dir ):
        stream.write( "Creating temporary directory %s...\n" % tmp_dir )
        os.mkdir( tmp_dir )

    instructions = input.mode.script_input( ncpus = ncpus )
    input.apply_script_xray_data( input = instructions )
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    for kw in input.keywords:
        kw.apply_script( input = instructions )

    prefix = get_prefix( tmp_dir = tmp_dir )
    keyword.Root( prefix = get_prefix( tmp_dir = tmp_dir ) ) \
        .apply_script( input = instructions )

    import libtbx.load_env
    bin_path = libtbx.env.under_build( "bin" )
    phaser_exe = None
    stream.write( "Selecting phaser executable...\n" )

    for trial_phaser in [ "phenix.phaser", "phenix.phaser.bat", "phaser", "phaser.bat" ]:
        if os.path.exists( os.path.join( bin_path, trial_phaser ) ):
            phaser_exe = os.path.join( bin_path, trial_phaser )
            break

    else:
        raise RuntimeError( "No phaser executable was found in %s" % bin_path)

    stream.write( 'Selected: "%s"\n' % phaser_exe )
    assert os.path.exists( phaser_exe )

    import subprocess

    script = "\n".join( instructions )
    stream.write( "Input:\n" )
    stream.write( "%s\n" % script )
    stream.write( "Executing input...\n" )
    logstream = open( "%s.log" % prefix, "wb" )
    start = datetime.now()

    process = subprocess.Popen(
        [ phaser_exe ],
        stdin = subprocess.PIPE,
        stdout = subprocess.PIPE,
        stderr = subprocess.STDOUT,
        )
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    bstr = u"\n".join( instructions )
    if sys.version_info[0] > 2:
        process.stdin.write( bstr.encode() )
    else:
        process.stdin.write( bstr )
    process.stdin.close()

    for l in process.stdout:
        stream.write( l.decode("utf-8") )
        logstream.write( l )

    runtime = datetime.now() - start
    logstream.close()
    process.wait()

    if process.returncode != 0:
        raise RuntimeError("Error exit from process, errorcode %s" % process.returncode)

    stream.write( "...complete\n" )
    stream.write( "Parsing output..." )

    try:
        result = input.mode.parse_log( root = prefix )

    except ValueError as e:
        raise RuntimeError( "Unexpected output: %s" % e)

    stream.write( "complete\n" )
    report = input.evaluate( input = input, result = result, prefix = prefix )
    return ( report, runtime )


if __name__ == "__main__":
    import sys

    if len( sys.argv ) != 2:
        raise RuntimeError( "Expected 1 argument, got %d" % ( len( sys.argv ) - 1 ))

    import pickle
    ( input, tmp_dir, ncpus, outfile, resfilesuffix ) = pickle.load( open( sys.argv[1], "r+b" ) )
    result = python(
        input = input,
        stream = sys.stdout,
        tmp_dir = tmp_dir,
        ncpus = ncpus,
        resfilesuffix = resfilesuffix
        )

    outstream = open( outfile, "w+b" )
    pickle.dump( result, outstream )
    outstream.close()
