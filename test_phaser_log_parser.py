from phaser.test_phaser_parsers import SolutionFileParser, AtomFileParser

import os.path
import re
import operator

INT = r"\d+"
FLOAT = r"-? \d+ \. \d*"
TF = r"(?:YES)|(?:NO)"
ANYTHING = r".*"
NA = r"(?: [0123456789.na/]* )"
INT_OR_NOTHING = r"(?: \d+ )|(?: -+ )|(?: [1Top/*/]* )"

def get_regular_expression_for(format):

    return re.compile(
        r"^ \s* " + r" \s+ ".join( "( %s )" % f for f  in format ) + r" \s* $",
        re.VERBOSE
        )


def extract_or_raise(regex, text, transformation, message):

    match = regex.search( text )

    if not match:
        raise RuntimeError( message)

    return transformation( groups = match.groups() )


def get_log_section(heading, log, index = 0):

    match = re.findall(
        "^(\*\*\* %s .*?EXIT STATUS: SUCCESS)$" % heading,
        log,
        re.MULTILINE | re.DOTALL
        )

    if len( match ) <= index:
        raise RuntimeError( "Section %s not found in log" % heading)

    return match[ index ]


def get_last_log_section(heading, log):

    matches = re.findall(
        "(\*\*\* %s .*?Finished:)" % heading,
        log,
        re.MULTILINE | re.DOTALL
        )

    if not matches:
        raise RuntimeError( "Section %s not found in log" % heading)

    return matches[ -1 ]


def extract_and_parse_table(section, table_format, line_format):

    table = table_format.search( section )

    if not table:
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        raise RuntimeError( "No table in log file matches the regular expression below:\n" \
          + table_format.pattern)

    results = []
    # Non-default line separator is stored in table.group(2). This is useful for
    # (old) gyre table entries in log file which are separated by ---- since we don't know how many
    # lines of ensembles are between each separator. See ResultGYRE.GYRE_TABLE
    # Now gyre uses loggraph output, as it is much simpler
    if len(table.groups()) > 1:
        sep = table.group(2)
    else:
        sep = "\n"
    #s1 = line_iterator( text = table.group( 1 ) )
    #s2 = table_section_iterator( tabletxt = table.group( 1 ), sep = sep )
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    for line in table_section_iterator( tabletxt = table.group( 1 ), sep = sep ):
    #for line in line_iterator( text = table.group( 1 ) ):
        match = line_format.search( line )
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )

        if not match:
            raise RuntimeError( "Table section:\n%s\ndoes not match expected format:\n%s\n" % (
                line,
                line_format.pattern,
                ))

        results.append( match.groups() )
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    return results

# deprecated, use table_section_iterator
def line_iterator(text):

    for l in text.splitlines():
        stripped = l.strip()

        if not stripped:
            continue

        yield l


def table_section_iterator(tabletxt, sep = "\n"):
# Split table into a list of strings separated with sep.
# Return an iterator of this list.
# Default separator returns just single lines of table.
# Sep may be more than one character
    for sec in re.split(sep, tabletxt, flags = re.MULTILINE ):

        stripped = sec.strip()

        if not stripped:
            continue

        yield sec



class MRReflectionArrayHolder(object):

    def getF(self):

        return self.getMiller()


    def getSIGF(self):

        return self.getMiller()


class OutputFileProducer(object):

    OUTPUT_FILES_EXTRACT = re.compile(
        r"""
        ^ OUTPUT \s+ FILES \s+
        -+ \s+
        ( .*?)
        <!--SUMMARY_END-->
        """,
        re.MULTILINE | re.DOTALL | re.VERBOSE
        )

    # Internal methods
    def get_output_files(self):

        return [ filename
            for filename
            in self.OUTPUT_FILES_EXTRACT.search(
                self.get_log_section( self.JOBNAME )
                ).group(1).split() ]

    def get_output_files_with_extension(self, extension):

        return [ filename for filename in self.get_output_files()
            if filename.endswith( extension ) ]


class StatisticsProducer(object):

    # Internal methods
    def extract_statistics(self):

        result = self.STATISTICS_EXTRACT.search(
            self.get_log_section( self.JOBNAME )
            )

        assert result

        return result.group(1)


class ScriptResult(object):
    """
    Phaser logfile processing functionality
    """

    SPACE_GROUP_SYMBOLS_EXTRACT = re.compile(
        r"""
        ^ \s+ Space-Group \s+ Name \s+ \( Hall \s+ Symbol \):
        ( [^(]+ ) \(
        ( [^)]+ )
        \)
        """,
        re.MULTILINE | re.VERBOSE
        )

    SPACE_GROUP_NUMBER_EXTRACT = re.compile(
        r"""
        ^ \s+ Space-Group \s+ Number: \s+ ( \d+ ) \n
        """,
        re.MULTILINE | re.VERBOSE
        )

    UNIT_CELL_EXTRACT = re.compile(
        r"""
        ^ \s+ Unit \s+ Cell:
        ( [\d\. ]+ )
        \n
        """,
        re.MULTILINE | re.VERBOSE
        )

    READ_REFLECTIONS_COUNT_EXTRACT = re.compile(
        r"""
        ^ \s+
        Number \s+ of \s+ Reflections \s+ in \s+ Selected \s+
        Resolution \s+ Range: \s+
        (\d+)
        $
        """,
        re.MULTILINE | re.VERBOSE
        )

    READ_DATA_HEADING = "Phaser Module: READ DATA FROM MTZ FILE"

    LOG_SECTION = r"^(\*\*\* %s .*?EXIT STATUS: SUCCESS)$"


    def __init__(self, root):

        log_file = "%s.log" % root
        assert os.path.isfile( log_file )

        self.log = open( log_file ).read()

        self.root = root
        self.status = not self.Failed()

        if self.status:
            self.reflection_count = self.get_reflection_count()
            self.process()


    # Internal methods
    def get_log_section(self, heading):

        match = re.search(
            self.LOG_SECTION % heading,
            self.log,
            re.MULTILINE | re.DOTALL
            )

        if match:
            return match.group(1)

        else:
            return None


    def get_space_group_symbols(self):

        match = self.SPACE_GROUP_SYMBOLS_EXTRACT.search(
            self.get_log_section( self.READ_DATA_HEADING )
            )
        assert match

        return ( match.group(1).strip(), match.group(2) )


    def get_reflection_count(self):

        match = self.READ_REFLECTIONS_COUNT_EXTRACT.search(
            self.get_log_section( self.READ_DATA_HEADING )
            )
        assert match

        return int( match.group(1) )


    # Public methods
    def getSpaceGroupHall(self):

        return self.get_space_group_symbols()[1]

    def getHall(self):

        return self.getSpaceGroupHall()


    def getSpaceGroupName(self):

        return self.get_space_group_symbols()[0]


    def getSpaceGroupNum(self):

        match = self.SPACE_GROUP_NUMBER_EXTRACT.search(
            self.get_log_section( self.READ_DATA_HEADING )
            )

        assert match

        return int( match.group(1) )


    def getUnitCell(self):

        match = self.UNIT_CELL_EXTRACT.search(
            self.get_log_section( self.READ_DATA_HEADING )
            )
        assert match

        return tuple(
                [ float( number ) for number in match.group(1).split() ] )


    def Failed(self):

        return not self.get_log_section( self.JOBNAME )


    def getMiller(self):

        return [ None ] * self.reflection_count


    def logfile(self):

        return self.log


    def ErrorMessage(self):

        return self.log


class ScriptResultANO(ScriptResult, MRReflectionArrayHolder):
    """
    This class processes the Phaser logfile and returns results in a
    phaser.ResultANO-like object
    Corresponding to ResultANO in codebase/phaser/boost_python/Outputs_bpl.cpp
    """

    JOBNAME = "Phaser Module: ANISOTROPY CORRECTION"

    WILSON_B_EXTRACT = re.compile(
        r"""
        ^ \s+ Wilson \s+ B-factor:
        \s+
        ( -? \d+\.\d* )
        """,
        re.MULTILINE | re.VERBOSE
        )

    WILSON_K_EXTRACT = re.compile(
        r"""
        ^ \s+ Wilson \s+ Scale:
        \s+
        ( \d+\.\d* )
        """,
        re.MULTILINE | re.VERBOSE
        )

    PRINCIPAL_EXTRACT = re.compile(
        r"""
        ^ \s* Refined \s+ Anisotropy \s+ Parameters \n
        \s* -+ \n
        \s* Principal \s+ components \s+ of \s+ anisotropic \s+ part \s+ of
          \s+ B \s+ affecting \s+ observed \s+ amplitudes: \n
        \s* eigenB \s+ \( A \^ 2 \)
          \s+ direction \s+ cosines \s+ \( orthogonal \s+ coordinates \) \n
        \s* ( -? \d+ \. \d* ) \s* (?: \s* -? \d+ \. \d* ){3} \n
        \s* ( -? \d+ \. \d* ) \s* (?: \s* -? \d+ \. \d* ){3} \n
        \s* ( -? \d+ \. \d* ) \s* (?: \s* -? \d+ \. \d* ){3} \n
        """,
        re.MULTILINE | re.VERBOSE
        )


    # Internal methods
    def process(self):


        wilson_b = self.WILSON_B_EXTRACT.search( self.log )
        assert wilson_b
        self.wilson_b = float( wilson_b.group(1) )

        wilson_k = self.WILSON_K_EXTRACT.search( self.log )
        assert wilson_k
        self.wilson_k = float( wilson_k.group(1) )

        principal = self.PRINCIPAL_EXTRACT.search( self.log )
        assert principal

        self.principal_components = (
            float( principal.group( 1 ) ),
            float( principal.group( 2 ) ),
            float( principal.group( 3 ) ),
            )


    # Public methods
    def getEigenBs(self):

        return self.principal_components


    def getWilsonB(self):

        return self.wilson_b


    def getWilsonK(self):

        return self.wilson_k


    def getCorrectedF(self):

        return self.getMiller()


    def getCorrectedSIGF(self):

        return self.getMiller()


class ScriptResultMR_FRF(ScriptResultANO, StatisticsProducer):
    """
    This class processes the Phaser logfile and returns results in a
    phaser.ResultMR_FRF-like object
    Corresponding to ResultMR_RF in codebase/phaser/boost_python/Outputs_bpl.cpp
    """

    JOBNAME = "Phaser Module: MOLECULAR REPLACEMENT ROTATION FUNCTION"
    STATISTICS_EXTRACT = re.compile(
        r"""
        \$TABLE \s* : \s*
        Rotation \s Function \s [^:]* \s* : \s*
        \$GRAPHS \s* : \s*
        RF \s Number \s vs \s LL-gain \s* : \s*
        [^:]+ \s* : \s*
        [^:]+ \s* : \s*
        : \s*
        RF \s Number \s vs \s Z-Score \s* : \s*
        [^:]+ \s* : \s*
        [^:]+ \s* : \s*
        \$\$ \s+ Number \s+ LLG \s+ Z-Score \s+ \$\$ \s+ loggraph \s+ \$\$ \s+
        ( [^$]+ )
        \$\$
        """,
        re.MULTILINE | re.VERBOSE
        )

    # Internal methods
    def process(self):

        rlist_file = "%s.rlist" % self.root
        assert os.path.isfile( rlist_file )

        self.solutions = SolutionFileParser.parse_file( rlist_file )


    # Public methods
    def getRF(self):

        return [ float( result_line.split()[1] )
            for result_line in self.extract_statistics().splitlines()
            if result_line ]


    def getRFZ(self):

        return [ float( result_line.split()[2] )
            for result_line in self.extract_statistics().splitlines()
            if result_line ]


    def getDotRlist(self):

        return self.solutions.get_dot_sol()


class ScriptResultMR_FTF(ScriptResultANO, StatisticsProducer):
    """
    This class processes the Phaser logfile and returns results in a
    phaser.ResultMR_FTF-like object
    Corresponding to ResultMR_TF in codebase/phaser/boost_python/Outputs_bpl.cpp
    """

    JOBNAME = "Phaser Module: MOLECULAR REPLACEMENT TRANSLATION FUNCTION"
    STATISTICS_EXTRACT = re.compile(
        r"""
        \$TABLE \s* : \s*
        Translation \s Function \s [^:]* \s* : \s*
        \$GRAPHS \s* : \s*
        TF \s Number \s vs \s LL-gain \s* : \s*
        [^:]+ \s* : \s*
        [^:]+ \s* : \s*
        : \s*
        TF \s Number \s vs \s Z-Score \s* : \s*
        [^:]+ \s* : \s*
        [^:]+ \s* : \s*
        \$\$ \s+ Number \s+ LLG \s+ Z-Score \s+ \$\$ \s+ loggraph \s+ \$\$ \s+
        ( [^$]+ )
        \$\$
        """,
        re.MULTILINE | re.VERBOSE
        )

    # Internal methods
    def process(self):

        sol_file = "%s.sol" % self.root
        assert os.path.isfile( sol_file )

        self.solutions = SolutionFileParser.parse_file( sol_file )


    # Public methods
    def getTF(self):

        return [ float( result_line.split()[1] )
            for result_line in self.extract_statistics().splitlines()
            if result_line ]


    def getTFZ(self):

        return [ float( result_line.split()[2] )
            for result_line in self.extract_statistics().splitlines()
            if result_line ]


    def getDotSol(self):

        return self.solutions.get_dot_sol()


class ResultBase(object):
    """
    Phaser logfile processing base functionality
    """

    SUCCESS_EXTRACT = re.compile(
        r"""^
        \s* -------------------- \n
        \s* EXIT \s STATUS: \s SUCCESS \n
        \s* -------------------- \n
        \s* \n
        \s* <!--SUMMARY_BEGIN--> \n
        \s* CPU \s Time: [^\n]* \n
        \s* Finished: [^\n]* \n
        \s* \n
        \s* <!--SUMMARY_END--> \n
        </pre> \n
        </html> \s* \Z
        """,
        re.MULTILINE | re.VERBOSE
        )

    SPACE_GROUP_SYMBOLS_EXTRACT = re.compile(
        r"""
        ^ \s+ Space-Group \s+ Name \s+ \( Hall \s+ Symbol \):
        ( [^(]+ ) \(
        ( [^)]+ )
        \)
        """,
        re.MULTILINE | re.VERBOSE
        )

    SPACE_GROUP_NUMBER_EXTRACT = re.compile(
        r"""
        ^ \s+ Space-Group \s+ Number: \s+ ( \d+ ) \n
        """,
        re.MULTILINE | re.VERBOSE
        )

    UNIT_CELL_EXTRACT = re.compile(
        r"""
        ^ \s+ Unit \s+ Cell:
        ( [\d\. ]+ )
        \n
        """,
        re.MULTILINE | re.VERBOSE
        )

    REFLECTIONS_COUNT_EXTRACT = re.compile(
        r"""
        ^ \s+
        Number \s+ of \s+ Reflections \s+ in \s+ Selected \s+
        Resolution \s+ Range: \s+
        (\d+)
        $
        """,
        re.MULTILINE | re.VERBOSE
        )

    READ_DATA_HEADING = "Phaser Module: READ DATA FROM MTZ FILE"


    def __init__(self, log):

        self._logfile = log

        match = self.SUCCESS_EXTRACT.search( self._logfile )

        if not match:
            self._failed = True
            self._space_group_name = "P 1"
            self._space_group_hall = " P 1"
            self._space_group_num = 1
            self._unit_cell = ( 1., 1., 1., 90.0, 90.0, 90.0 )
            self._reflection_count = 0

        else:
            self._failed = False
            section = get_log_section(
                log = self._logfile,
                heading = "Phaser Module: READ DATA FROM MTZ FILE"
                )

            ( self._space_group_name, self._space_group_hall ) = extract_or_raise(
                regex = self.SPACE_GROUP_SYMBOLS_EXTRACT,
                text = section,
                transformation = lambda groups: ( groups[0].strip(), groups[1] ),
                message = "Cannot find space group labels in file"
                )

            self._space_group_num = extract_or_raise(
                regex = self.SPACE_GROUP_NUMBER_EXTRACT,
                text = section,
                transformation = lambda groups: int( groups[0] ),
                message = "Cannot find space group number in file"
                )

            self._unit_cell = extract_or_raise(
                regex = self.UNIT_CELL_EXTRACT,
                text = section,
                transformation = lambda groups:
                    tuple( [ float( number ) for number in groups[0].split() ] ),
                message = "Cannot find unit cell in file"
                )

            self._reflection_count = extract_or_raise(
                regex = self.REFLECTIONS_COUNT_EXTRACT,
                text = section,
                transformation = lambda groups: int( groups[0] ),
                message = "Cannot find reflection count in file"
                )


    # Public methods
    def getSpaceGroupHall(self):

        return self._space_group_hall


    def getSpaceGroupName(self):

        return self._space_group_name


    def getSpaceGroupNum(self):

        return self._space_group_num


    def getUnitCell(self):

        return self._unit_cell


    def Failed(self):

        return self._failed


    def getMiller(self):

        return [ None ] * self._reflection_count


    def logfile(self):

        return self._logfile


    def ErrorMessage(self):

        return self._logfile


class ResultMR(ResultBase):
    """
    This class processes the Phaser logfile and returns results in a
    phaser.ResultMR-like object
    Corresponding to ResultMR in codebase/phaser/boost_python/Outputs_bpl.cpp
    """

    REFINEMENT_TABLE = re.compile(
        r"""
        ^ \s* Refinement \s+ Table \s+ \( Sorted \)\n
        \s* -+ \n
        (?: \s* Refinement \s+ to \s+ full \s+ resolution \s* \n )?
        \s+ \#out \s+ =\#out \s+ \#in \s+ =T \s+ \( Start \s+ LLG \s+ Rval \s+ TFZ \) \s+ \( Refined \s+ LLG \s+ Rval \s+ TFZ==\) \s+ SpaceGroup \s+ Cntrst \n
        ( .*? ) \n
        \s+ Refinement \s+ Table \s+ \( Variance \s+ Ranges \)\n
        """,
        re.MULTILINE | re.DOTALL | re.VERBOSE
        )

    REFINEMENT_TABLE_LINE = get_regular_expression_for(
        format = ( INT_OR_NOTHING, INT_OR_NOTHING, INT, FLOAT, FLOAT, NA, FLOAT, FLOAT, ANYTHING )
        )

    PACKING_TABLE = re.compile(
        r"""
        ^ \s* Packing \s+ Table \n
        .*?
        \s* \#in \s+ \#out \s+ Clash-% \s+ Symm \s+ SpaceGroup \s+ Annotation \s+ \n
        ( .*? ) \n
        \s+ \d+ \s+ accepted \s+ of \s+ \d+ \s+ solutions \n+
        """,
        re.MULTILINE | re.DOTALL | re.VERBOSE
        )

    PACKING_TABLE_LINE = get_regular_expression_for(
        format = ( INT, INT_OR_NOTHING, INT, ANYTHING )
        )

    LLG_TABLE = re.compile(
        r"""
        ^ \s* Refinement \s+ Table \s+ \( Sorted \)\n
        \s* -+ \n
        (?: \s* Refinement \s+ to \s+ full \s+ resolution \s* \n )?
        \s+ \#out \s+ =\#out \s+ \#in \s+ =T \s+ \( Start \s+ LLG \s+ Rval \s+ TFZ \) \s+ \( Refined \s+ LLG \s+ Rval \s+ TFZ==\) \s+ SpaceGroup \s+ Cntrst \n
        ( .*? ) \n
        \s+ Refinement \s+ Table \s+ \( Variance \s+ Ranges \)\n
        -+ \n
        """,
        re.MULTILINE | re.DOTALL | re.VERBOSE
        )

    LLG_TABLE_LINE = get_regular_expression_for(
        format = ( INT_OR_NOTHING, INT_OR_NOTHING, INT, FLOAT, FLOAT, NA, FLOAT, FLOAT, ANYTHING )
        )

    PRUNING_TABLE = re.compile(
       r"""
       ^ \s* Occupancy \s+ Refinement \s+ Table \s+ \( Sorted \)\n
       \s* -+ \n
       \s+ \#in \s+ \#out \s+ \( Start \s+ LLG \s+ Rval \s+ TFZ\=\= \) \s+ \( Refined \s+ LLG \s+ Rval\) \s+ SpaceGroup \s+\n
       ( .*? ) \n
       """,
       re.MULTILINE | re.DOTALL | re.VERBOSE
       )

    PRUNING_TABLE_LINE = get_regular_expression_for(
       format = ( INT, INT, FLOAT, FLOAT, NA, FLOAT, ANYTHING )
       )

    def __init__(self, heading, table, table_line, value_format, log, sol):

        super( ResultMR, self ).__init__( log = log )

        if not self.Failed():
            log_section = get_last_log_section( heading = heading, log = log )

            self._values = value_format(
                table = extract_and_parse_table(
                    section = log_section,
                    table_format = table,
                    line_format = table_line
                    )
                )
            """
            table = extract_and_parse_table(
                section = log_section,
                table_format = table,
                line_format = table_line
                )
            """

            self._dot_sol = SolutionFileParser.parse_string( sol ).get_dot_sol()
            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )

        else:
            self._values = []
            self._dot_sol = []

    # Public methods
    def getValues(self):

        return self._values


    def getDotSol(self):
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        return self._dot_sol


    @classmethod
    def RNP(cls, log, sol):

        return cls(
            heading = "Phaser Module: MOLECULAR REPLACEMENT REFINEMENT AND PHASING",
            table = cls.REFINEMENT_TABLE,
            table_line = cls.REFINEMENT_TABLE_LINE,
            value_format = lambda table: [ float( l[6] ) for l in table if l[1] == "---" ],
            log = log,
            sol = sol,
            )

    @classmethod
    def RNP_from_root(cls, root):

        return cls.RNP(
            log = open( "%s.log" % root ).read(),
            sol = open( "%s.sol" % root ).read()
            )


    @classmethod
    def PAK(cls, log, sol):

        return cls(
            heading = "Phaser Module: MOLECULAR REPLACEMENT PACKING",
            table = cls.PACKING_TABLE,
            table_line = cls.PACKING_TABLE_LINE,
            value_format = lambda table: [ float( l[2] ) for l in table if l[1] != "---" ],
            log = log,
            sol = sol,
            )


    @classmethod
    def PAK_from_root(cls, root):

        return cls.PAK(
            log = open( "%s.log" % root ).read(),
            sol = open( "%s.sol" % root ).read()
            )

    @classmethod
    def LLG(cls, log, sol):

        return cls(
            heading = "Phaser Module: MOLECULAR REPLACEMENT REFINEMENT AND PHASING",
            table = cls.LLG_TABLE,
            table_line = cls.LLG_TABLE_LINE,
            value_format = lambda table: [ float( l[6] ) for l in table if l[1] == "---" ],
            log = log,
            sol = sol,
            )

    @classmethod
    def LLG_from_root(cls, root):

        return cls.LLG(
            log = open( "%s.log" % root ).read(),
            sol = open( "%s.sol" % root ).read()
            )

    @classmethod
    def OCC(cls, log, sol):

        return cls(
            heading = "Phaser Module: MOLECULAR REPLACEMENT MODEL PRUNING",
            table = cls.PRUNING_TABLE,
            table_line = cls.PRUNING_TABLE_LINE,
            value_format = lambda table: [ float( l[5] ) for l in table ],
            log = log,
            sol = sol,
            )

#root is fileroot
    @classmethod
    def OCC_from_root(cls, root):

        return cls.OCC(
            log = open( "%s.log" % root ).read(),
            sol = open( "%s.sol" % root ).read()
            )


class ResultGYRE(ResultBase):
    """
    This class processes the Phaser logfile and returns results in a
    phaser.ResultGYRE-like object
    Corresponding to ResultGYRE in codebase/phaser/boost_python/Outputs_bpl.cpp
    """

    LLG_TABLE = re.compile(
        r"""
        ^ \s* Refinement \s+ Table \s+ \( Sorted \)\n
        \s* -+ \n
        (?: \s* Refinement \s+ to \s+ full \s+ resolution \s* \n )?
        \s+ \#out \s+ =\#out \s+ \#in \s+ =T \s+ \( Start \s+ LLG \s+ Rval \s+ TFZ \) \s+ \( Refined \s+ LLG \s+ Rval \s+ TFZ==\) \s+ SpaceGroup \s+ Cntrst \n
        ( .*? ) \n
        \s+ Refinement \s+ Table \s+ \( Variance \s+ Ranges \)\n
        -+ \n
        """,
        re.MULTILINE | re.DOTALL | re.VERBOSE
        )

    LLG_TABLE_LINE = get_regular_expression_for(
        format = ( INT_OR_NOTHING, INT_OR_NOTHING, INT, FLOAT, FLOAT, NA, FLOAT, FLOAT, ANYTHING )
        )

    GYRE_TABLE = re.compile(
        r"""
        ^ \s* Number \s+ final-LLG \s+ initial-LLG \s+
        ^ \s* \$\$ \s+ loggraph \s+ \$\$ \s+
        ( [^$]+ )
        \$\$
        """,
        re.MULTILINE | re.VERBOSE
        )

    GYRE_TABLE_LINE = get_regular_expression_for(
        format = ( INT , FLOAT , FLOAT  )
        )


    def __init__(self, heading, table, table_line, value_format, log, sol):

        super( ResultGYRE, self ).__init__( log = log )

        if not self.Failed():
            log_section = get_last_log_section( heading = heading, log = log )
            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )

            self._values = value_format(
                table = extract_and_parse_table(
                    section = log_section,
                    table_format = table,
                    line_format = table_line
                    )
                )

            self._dot_sol = SolutionFileParser.parse_string( sol ).get_dot_sol()
            self.rawsolstr = sol

        else:
            self._values = []
            self._dot_sol = []
            self.rawsolstr = ""

    # Public methods
    def getValues(self):

        return self._values


    def getDotSol(self):
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        return self._dot_sol


    @classmethod
    def GYRE(cls, log, sol):
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        return cls(
            heading = "Phaser Module: MOLECULAR (?:REPLACEMENT ROTATION|REPLACEMENT) GYR(?:ATION|E) REFINEMENT",
            table = cls.GYRE_TABLE,
            table_line = cls.GYRE_TABLE_LINE,
            value_format = lambda table: [ float( l[1] ) for l in table ],
            log = log,
            sol = sol,
            )

    @classmethod
    def GYRE_from_root(cls, root):
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        return cls.GYRE(
            log = open( "%s.log" % root ).read(),
            sol = open( "%s.sol" % root ).read()
            )


    @classmethod
    def LLG(cls, log, sol):

        return cls(
            heading = "Phaser Module: MOLECULAR REPLACEMENT REFINEMENT AND PHASING",
            table = cls.LLG_TABLE,
            table_line = cls.LLG_TABLE_LINE,
            value_format = lambda table: [ float( l[6] ) for l in table if l[1] == "---" ],
            log = log,
            sol = sol,
            )

    @classmethod
    def LLG_from_root(cls, root):

        return cls.LLG(
            log = open( "%s.log" % root ).read(),
            sol = open( "%s.sol" % root ).read()
            )


class ResultNMA(object):
    """
    This class processes the Phaser logfile and returns results in a
    phaser.ResultNMA-like object
    Corresponding to ResultNMA in codebase/phaser/boost_python/Outputs_bpl.cpp
    """

    DOMAIN_ANALYSIS_EXTRACT = re.compile(
        r"""^
        \s* -+ \n
        \s* DOMAIN \s ANALYSIS \s* \n
        \s* -+ \n
        """,
        re.MULTILINE | re.VERBOSE
        )

    def __init__(self, log, sol):

        self.rawsolstr = sol
        self._logfile = log
        self._failed = True
        if self.DOMAIN_ANALYSIS_EXTRACT.search( self._logfile ):
            self._failed = False

    def Failed(self):

        return self._failed

    def ErrorMessage(self):

        return self._logfile

    def logfile(self):

        return self._logfile

    # Public methods
    def getValues(self):

        return None

    @classmethod
    def SCEDS_from_root(cls, root):

        return cls(
            log = open( "%s.log" % root ).read(),
            sol = open( "%s.sol" % root ).read()
            )


class ScriptHand(object):
    """
    Equivalent of Hand_EP
    Corresponding to HandEP in codebase/phaser/boost_python/Outputs_bpl.cpp
    """

    def __init__(self, space_group):

        self.space_group = space_group


    def getSpaceGroupName(self):

        return self.space_group


class ScriptResultEP(ScriptResult, OutputFileProducer, StatisticsProducer):
    """
    ResultEP-like object from .log and .sol-files
    Corresponding to ResultEP in codebase/phaser/boost_python/Outputs_bpl.cpp
    """

    JOBNAME = "Phaser Module: SINGLE-WAVELENGTH ANOMALOUS DISPERSION"

    STATISTICS_EXTRACT = re.compile(
        r"""
        ^ \s+ Final \s+ Log-Likelihood \s = \s+ ( -? \d+ \.? \d* ) \n
        ^ \s+ Final \s+ R-factor \s = \s+ ( -? \d+ \.? \d* ) \n
        \n
        """,
        re.MULTILINE | re.VERBOSE
        )
    HAND_EXTRACT = re.compile(
        r"""
        ^
        ------- \n
        RESULTS \n
        ------- \n
        \s+
        <!--SUMMARY_BEGIN--> \n
        \s+ \$TEXT : Result : \s+ \$\$ \s+ Baubles \s+ Markup \s+ \$\$ \n
        \s+
        \s+ SAD \s+ Refinement \s+ Table \s+ (Unsorted) \s+ \n
        [-]+\n
        """,
        re.MULTILINE | re.VERBOSE
        )

    # Internal methods
    def process(self):

        sol_file = "%s.1.sol" % self.root
        assert os.path.isfile( sol_file )

        self.atoms = AtomFileParser.parse_file( sol_file )
        #data = self.HAND_EXTRACT.search( self.get_log_section( self.JOBNAME ) )
        #assert data

        #self.direct_hand_sg = data.groups()[0]
        #self.inverted_hand_sg = data.groups()[1]


    # Public methods
    def getAtoms(self, inverted_hand, t):

        if not inverted_hand:
            return self.atoms.get_cctbx_atoms()

        else:
            return self.atoms.get_empty_atoms_list()


    def getTopAtoms(self):

        return self.atoms.get_cctbx_atoms()


    def getFp(self, element, inverted_hand, t):

        if not inverted_hand:
            return self.atoms.get_f_p( element )

        else:
            return 0.0


    def getFdp(self, element, inverted_hand, t):

        if not inverted_hand:
            return self.atoms.get_f_dp( element )

        else:
            return 0.0


    def getTopMtzFile(self):

        mtz_files = self.get_output_files_with_extension( ".mtz" )

        if mtz_files:
            return mtz_files[0]

        return ""


    def getMtzFile(self,  inverted_hand, t):

        if not inverted_hand:
            mtz_files = self.get_output_files_with_extension( ".mtz" )

            if mtz_files:
                return mtz_files[0]

        return ""


    def getPdbFile(self, inverted_hand, t):

        if not inverted_hand:
            pdb_files = self.get_output_files_with_extension( ".pdb" )

            if pdb_files:
                return pdb_files[0]

        return ""


    def getTopPdbFile(self):

        pdb_files = self.get_output_files_with_extension( ".pdb" )

        if pdb_files:
            return pdb_files[0]

        return ""


    def getSolFile(self, inverted_hand, t):

        if not inverted_hand:
            sol_files = self.get_output_files_with_extension( ".sol" )

            if sol_files:
                return sol_files[0]

        return ""


    def getLogLikelihood(self):

        return -float( self.extract_statistics() )


    """
    def getHand(self, inverted = False):

        if inverted:
            return ScriptHand( space_group = self.inverted_hand_sg )

        else:
            return ScriptHand( space_group = self.direct_hand_sg )
    """
