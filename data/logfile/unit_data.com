#!
phenix.phaser << eof > toxd_MR_AUTO.log
MODE MR_AUTO
HKLIN "../../tutorial/toxd.mtz"
LABIN F=FTOXD3 SIGF=SIGFTOXD3
ENSEMBLE toxd PDBFILE "../../tutorial/1D0D_B.pdb" IDENTITY 0.377
ROOT toxd_MR_AUTO
SEARCH ENSEMBLE toxd NUM 1
eof

phenix.phaser << eof > toxd_MR_PAK.log
MODE MR_PAK
HKLIN "../../tutorial/toxd.mtz"
LABIN F=FTOXD3 SIGF=SIGFTOXD3
ENSEMBLE toxd PDBFILE "../../tutorial/1D0D_B.pdb" IDENTITY 0.377
ROOT toxd_MR_PAK
@toxd_MR_AUTO.sol
eof

phenix.phaser << eof > toxd_MR_LLG.log
MODE MR_RNP
HKLIN "../../tutorial/toxd.mtz"
LABIN F=FTOXD3 SIGF=SIGFTOXD3
ENSEMBLE toxd PDBFILE "../../tutorial/1D0D_B.pdb" IDENTITY 0.377
ROOT toxd_MR_LLG
@toxd_MR_AUTO.sol
eof

phenix.phaser  << eof > beta_blip_MR_RNP.log
ROOT beta_blip_MR_RNP
MODE MR_RNP
HKLIN "../../tutorial/beta_blip_P3221.mtz"
LABIN F Fobs SIGF Sigma
ENSEmble beta PDBFile "../../tutorial/beta.pdb" RMS 1.0
ENSEmble blip PDBFile "../../tutorial/blip.pdb" RMS 1.0
COMPosition  PROTEIN SEQ "../../tutorial/beta.seq" NUM 1 # molecular weight of beta
COMPosition  PROTEIN SEQ "../../tutorial/blip.seq"  NUM 1 # molecular weight of blip
SOLU SET
SOLU SPAC P 32 2 1
SOLU 6DIM ENSE beta EULER 200.9 41.3 183.9 FRAC -0.50 -0.16 -0.28 BFAC -2.95
SOLU 6DIM ENSE blip EULER 43.9 81.0 117.2 FRAC -0.12 0.29 -0.09 BFAC 5.16
eof

rm *.1.pdb *.1.mtz *.sum
