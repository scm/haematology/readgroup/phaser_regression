Executing regression tests:

1. Individual
phenix.python path-to-regression-test-file <test|display|update>
    [ -e <script|python> ] [ -n ncpus ] [-s SUFFIX]

Executes regression test pointed by the path (in any current directory)
using an execution mode (default: python).

Modes:
  - test. Runs test and compares results with expected values
  - display. Displays expected results
  - update. Runs test and overwrites expected results with current ones


2. Grouped
phaser_regression.regression regression-group-name
    [ -n ncpus ] [-f OUTPUTFILE] [ -o <terse|full|html|silent|normal,terse_failed> ] [-s SUFFIX]

Executes a regression test group (in any current directory). All individual
tests are available as a respective group, and there are certain predefined
sets grouped by functionality/test data used. The job will be executed on all
existing execution modes. Available group names are listed with the -h option.

Creating new regression tests

Available test data is exported in the phaser_regression/test_data module, and
Phaser modes are available as methods of the InputData class in the
phaser_regression/test_phaser_keyword module (e.g. anisotropy_correction).
Creating a test is pairing the dataset with the required calculation. The
function call does not actually execute the test, it only creates an object
that can be executed by test_runner on any of the available ways (as a python
call or through a C++ executable/script).

Sometimes the calculation needs additional input besides the ones stored in the
test_data module (e.g. rotation list for translation function). This has to be
passed to the method that creates the test.

To save the expected results for the new test, run it as a single test using the
"update" option, and check in the resulting file. This will be created in the
phaser_regression/regression directory. The default filename is *.py2result if
executed with python2 interpreter. Otherwise it is *.py3result. These result files
corresponds to the test modules named *.py. The suffix can be changed with
the -s or --suffix option to a different suffix, e.g. -s .py3MacOS will create
files like tst_rnp_toxd.py3MacOS.

The -f or --outputfile option lets you specify the prefix of the output files, one
which is plain text and one which is html formatted. The default prefix is
PhaserRegressionOutPut.

Files in the phaser_regression/regression folder that are called tst*.py are
automatically picked up for the "all" regression group (this is the one exercised
nightly), but can also be associated with any of the existing groups by editing
phaser_regression_tests() function in phaser_regression/test_runner.py. A test
can belong to multiple regression groups.

Creating new evaluation protocols

The test_runner module executes the test and obtains a Result-object (or a
Result-object like Python object, if the results are parsed out from the logfile).
Comparisons are based on values that are stored in the Result-object and
accessible through methods/properties.

The test_reporter module defines what evaluation criteria are applied to each
mode. First, each result is associated with a type (defined in the test_result
module). The type also defines the pass/fail criteria and the tolerances, if any.

Adding a new value to an existing test protocol is equivalent to specifying how
the value should be extracted from the Result-object, and what its type is, and
assigning this to descriptive test key (which can be anything as long as it is
unique within the respective protocol).

Creating a new protocol involves creating a function that accepts two variables,
"input" and "result", respectively, and return a Python dictionary that contains
the test keys and the protocol for calculating them. To enable the new protocol,
it needs to be included in the "reporters" section of the InputData object (see
above).

When running the test in the "update" mode, the test_runner calculates and saves
the returned results. In "test" mode, the saved results are compared to the
calculated ones, and the results are rendered as text or html (there is no
additional code necessary for this). The "display" mode just reads the stored
values and prints them onto the screen.
