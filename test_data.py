from phaser_regression import test_phaser_keyword as keyword

import libtbx.load_env
import os.path

REGRESSION = libtbx.env.dist_path( "phaser_regression" )
ROOT = os.path.join( REGRESSION, "data", "regression" )


class Data(object):
    """
    A dummy object to hold data
    """

    def __init__(self, **kwargs):

        for ( name, value ) in kwargs.items():
            setattr( self, name, value )


APT = Data(
    data_ep = keyword.EPReflectionData(
        file_name = os.path.join( ROOT, "haptbr_withfc.mtz" ),
        labels_for = {
            ( "haptbr", "peak" ): ( "F_peak(+)", "SIGF_peak(+)", "F_peak(-)", "SIGF_peak(-)" ),
            },
        ),
    components = (
        keyword.Component.protein_seq_num(
            seq = os.path.join( ROOT, "apt.seq" ),
            num = 2,
            ),
        ),
    atoms_pdb = keyword.AtomPdb(
        xtal = "haptbr",
        filename = os.path.join( ROOT, "haptbr_hyss_ori.pdb" ),
        ),
    wavelength = keyword.Wavelength( value = 0.9202 ),
    scattering = keyword.Scattering(
        type = "Br",
        fp = -7,
        fdp = 5,
        fix = "EDGE"
        ),
    )


BARNASE = Data(
    data_mr = keyword.MRReflectionData(
        file_name = os.path.join( ROOT, "1b2s.mtz" ),
        fp = "Fo",
        sigfp = "Sigma_Fo"
        ),
    resolution = keyword.Resolution( high = 5.0 ),
    components = (
        keyword.Component.protein_mw_num( mw = 12172, num = 3 ),
        keyword.Component.protein_mw_num( mw = 9773, num = 3 ),
        ),
    ensembles = (
        keyword.EnsembleModel(
            name = "barnase",
            descriptions =  [
                keyword.EnsemblePDBFile.Id(
                    module_name = "phaser_regression",
                    file_name = os.path.join( "data", "regression", "1brs_barnase_A.pdb" ),
                    value = 1.000
                    ),
                ]
            ),
        keyword.EnsembleModel(
            name = "barstar",
            descriptions =  [
                keyword.EnsemblePDBFile.Id(
                    module_name = "phaser_regression",
                    file_name = os.path.join( "data", "regression", "1brs_barstar_D.pdb" ),
                    value = 1.000
                    ),
                ]
            ),
        ),
    solution = """
SOLU SET original
SOLU 6DIM ENSE barnase EULER  231.647   35.353   45.767 FRAC -0.22692  0.48806  0.08415
SOLU 6DIM ENSE barnase EULER   39.509  179.602   38.411 FRAC  0.00327 -0.59286  0.50032
SOLU 6DIM ENSE barnase EULER  175.890   47.606    4.502 FRAC -0.03394  1.29835  0.03471
SOLU 6DIM ENSE barstar EULER  105.393  179.442  105.579 FRAC -0.00046 -0.60907  0.49382
SOLU 6DIM ENSE barstar EULER  231.028   36.357   44.937 FRAC -0.22273  1.51380  0.09214
SOLU 6DIM ENSE barstar EULER  180.099   51.375    0.533 FRAC -0.03873  1.28763  0.07687
SOLU SET incomplete
SOLU 6DIM ENSE barnase EULER  231.647   35.353   45.767 FRAC -0.22692  0.48806  0.08415
SOLU 6DIM ENSE barnase EULER   39.509  179.602   38.411 FRAC  0.00327 -0.59286  0.50032
SOLU 6DIM ENSE barnase EULER  175.890   47.606    4.502 FRAC -0.03394  1.29835  0.03471
SOLU 6DIM ENSE barstar EULER  105.393  179.442  105.579 FRAC -0.00046 -0.60907  0.49382
SOLU 6DIM ENSE barstar EULER  231.028   36.357   44.937 FRAC -0.22273  1.51380  0.09214
SOLU SET scrambled, shift by 0.5 along c, round values
SOLU 6DIM ENSE barstar EULER  105.4  179.4  105.6 FRAC -0.001 -0.609  0.994
SOLU 6DIM ENSE barnase EULER   39.5  179.6   38.4 FRAC  0.003 -0.593  0.000
SOLU 6DIM ENSE barnase EULER  175.9   47.6    4.5 FRAC -0.034  1.298  0.535
SOLU 6DIM ENSE barstar EULER  231.0   36.4   44.9 FRAC -0.223  1.514  0.592
SOLU 6DIM ENSE barnase EULER  231.6   35.4   45.8 FRAC -0.227  0.488  0.584
SOLU 6DIM ENSE barstar EULER  180.1   51.4    0.5 FRAC -0.039  1.288  0.577
SOLU SET reverse order, shift by -0.3 along b, truncate values
SOLU 6DIM ENSE barstar EULER  180.0   51.0    0.0 FRAC -0.038  0.987  0.076
SOLU 6DIM ENSE barstar EULER  231.0   36.0   44.0 FRAC -0.222  0.213  0.092
SOLU 6DIM ENSE barstar EULER  105.0  179.0  105.0 FRAC -0.000 -0.909  0.493
SOLU 6DIM ENSE barnase EULER  175.0   47.0    4.0 FRAC -0.033  0.998  0.034
SOLU 6DIM ENSE barnase EULER   39.0  179.0   38.0 FRAC  0.003 -0.892  0.500
SOLU 6DIM ENSE barnase EULER  231.0   35.0   45.0 FRAC -0.226  0.188  0.084
SOLU SET wrong
SOLU 6DIM ENSE barnase EULER  231.647   35.353   45.767 FRAC -0.22692  0.18806  0.08415
SOLU 6DIM ENSE barnase EULER   39.509  179.602   38.411 FRAC  0.00327 -0.59286  0.50032
SOLU 6DIM ENSE barnase EULER  175.890   47.606    4.502 FRAC -0.03394  1.29835  0.03471
SOLU 6DIM ENSE barstar EULER  105.393  179.442  105.579 FRAC -0.00046 -0.60907  0.49382
SOLU 6DIM ENSE barstar EULER  231.028   36.357   44.937 FRAC -0.22273  1.51380  0.09214
SOLU 6DIM ENSE barstar EULER  180.099   51.375    0.533 FRAC -0.03873  1.28763  0.07687
""",
    )


BETABLIP = Data(
  data_mr = keyword.MRReflectionData(
    file_name = libtbx.env.under_dist( "phaser", os.path.join(  ROOT , "beta_blip_P3221.mtz" ) ),
      fp = "Fobs",
      sigfp = "Sigma"
      ),
  resolution = keyword.Resolution( high = 4.0 ),
  components = (
    keyword.Component.protein_seq_num(
      seq = os.path.join(  ROOT , "beta.seq" ),
      num = 1,
      ),
    keyword.Component.protein_seq_num(
      seq = os.path.join(  ROOT , "blip.seq" ),
      num = 1,
      ),
    ),
  ensembles = (
    keyword.EnsembleModel(
      name = "beta",
      descriptions =  [
        keyword.EnsemblePDBFile.Id(
          module_name = "phaser",
          file_name = os.path.join(  ROOT , "beta.pdb" ),
          value = 1.000
          ),
        ]
      ),
    keyword.EnsembleModel(
      name = "blip",
      descriptions =  [
        keyword.EnsemblePDBFile.Id(
          module_name = "phaser",
          file_name = os.path.join(  ROOT , "blip.pdb" ),
          value = 1.000
          ),
        ],
      ),
    keyword.EnsembleModel(
      name = "betablip_phaser",
      descriptions =  [
        keyword.EnsemblePDBFile.Id(
          module_name = "phaser",
          file_name = os.path.join(  ROOT , "beta-blip_phaser.1.pdb" ),
          value = 1.000
          ),
        ],
      ),
    ),
    rlist =  """
SOLU SET
SOLU TRIAL ENSEMBLE betablip_phaser EULER 0.833 1.199 0.815 RF 57.8 RFZ 6.09
SOLU TRIAL ENSEMBLE betablip_phaser EULER 87.630 40.481 265.655 RF 44.3 RFZ 4.30
SOLU TRIAL ENSEMBLE betablip_phaser EULER 104.861 13.171 262.074 RF 42.4 RFZ 4.16
SOLU ENSEMBLE betablip_phaser VRMS DELTA +0.0000 RMSD 1.00 #VRMS 1.00
  """,
  )


BETAPRUNE = Data(
  data_mr = keyword.MRReflectionData(
    file_name = libtbx.env.under_dist( "phaser", os.path.join(  ROOT , "beta_blip_P3221.mtz" ) ),
      fp = "Fobs",
      sigfp = "Sigma"
      ),
  resolution = keyword.Resolution( high = 3.0 ),
  components = (
    keyword.Component.protein_mw_num(
      mw = 28853,
      num = 1,
      ),
    keyword.Component.protein_mw_num(
      mw = 17522,
      num = 1,
      ),
    ),
  ensembles = (
    keyword.EnsembleModel(
      name = "beta1",
      descriptions =  [
        keyword.EnsemblePDBFile.Id(
          module_name = "phaser",
          file_name = os.path.join(  ROOT , "beta1.pdb" ),
          value = 1.000
          ),
        ]
      ),
    keyword.EnsembleModel(
      name = "beta2",
      descriptions =  [
        keyword.EnsemblePDBFile.Id(
          module_name = "phaser",
          file_name = os.path.join(  ROOT , "beta2.pdb" ),
          value = 1.000
          ),
        ],
      ),
    ),
  )


HIBCACB = Data(
  data_mr = keyword.MRReflectionData(
    file_name =  os.path.join(  ROOT , "2a8d_5A.mtz" ),
      fp = "FOBS_X",
      sigfp = "SIGFOBS_X"
      ),
  components = (
    keyword.Component.protein_seq_num(
      seq = os.path.join(  ROOT , "2a8d.seq" ),
      num = 6,
      ),
    ),
  ensembles = (
    keyword.EnsembleModel(
      name = "1i6p",
      descriptions =  [
        keyword.EnsemblePDBFile.Id(
          module_name = "phaser",
          file_name = os.path.join(  ROOT , "1i6p.pdb" ),
          value = 0.61
          ),
        ]
      ),
    ),
  )


OVERLAP = Data(
    data_mr = keyword.MRReflectionData(
        file_name = os.path.join( ROOT, "gpdtbrs_6.0.mtz" ),
        fp = "Fobs",
        sigfp = "SIGFobs"
        ),
    components = (
      keyword.Component.composition_by("AVERAGE")
      ,
    ),
    ensembles = (
      keyword.EnsembleModel(
        name = "overlap",
        descriptions =  [
          keyword.EnsemblePDBFile.Id(
              module_name = "phaser_regression",
              file_name = os.path.join( "data", "regression", "1gd1.pdb" ),
              value = 1.0,
            ),
          ]
        ),
      keyword.EnsembleModel(
        name = "overlapshift",
        descriptions =  [
          keyword.EnsemblePDBFile.Id(
              module_name = "phaser_regression",
              file_name = os.path.join( "data", "regression", "1gd1_shift.pdb" ),
              value = 1.0,
            ),
          ]
        ),
      ),
    )


FOURHELIXBUNDLE = Data(
    data_mr = keyword.MRReflectionData(
        file_name = os.path.join( ROOT, "A31P_3A.mtz" ),
        fp = "FP",
        sigfp = "SIGFP"
        ),
    components = (
        keyword.Component.protein_mw_num( mw = 12212, num = 1 ),
        ),
    ensembles = (
        keyword.EnsembleModel(
            name = "helix_pdb",
            descriptions =  [
                keyword.EnsemblePDBFile.Rms(
                    module_name = "phaser_regression",
                    file_name = os.path.join( "data", "regression", "helix.pdb" ),
                    value = 0.500,
                    ),
                ]
            ),
        keyword.EnsembleModel(
            name = "helix_ala",
            descriptions =  [
                #keyword.EnsembleHelix.Rms(
                keyword.EnsembleFragment.Helix(
                    length = 14,
                    rms = 1.0,
                    ),
                ]
            ),
        ),
    )


MRATOM = Data(
    data_mr = keyword.MRReflectionData(
        file_name = os.path.join( ROOT, "3a02.mtz" ),
        fp = "FOBS",
        sigfp = "SIGFOBS"
        ),
    resolution = keyword.Resolution( high = 1.0 ),
    components = (
        keyword.Component.protein_seq_num(
          seq = os.path.join(  ROOT , "3a02.fa" ),
          num = 1,
          ),
        ),
    ensembles = (
        keyword.EnsembleModel(
            name = "sulphur_atom",
            descriptions =  [
                keyword.EnsembleFragment.Atom(
                    type = "S",
                    rms = 0.1,
                    ),
                ]
            ),
        keyword.EnsembleModel(
            name = "sulphur_atom000",
            descriptions =  [
                keyword.EnsemblePDBFile.Rms(
                    module_name = "phaser_regression",
                    file_name = os.path.join( "data", "regression", "Satom000.pdb" ),
                    value = 0.1,
                    ),
                ]
            ),
        ),
    )


INS = Data(
    data_ep = keyword.EPReflectionData(
        file_name = os.path.join( ROOT, "insulin_withFc.mtz" ),
        labels_for = {
            ( "insulin", "peak" ): ( "F_CuKa(+)", "SIGF_CuKa(+)", "F_CuKa(-)", "SIGF_CuKa(-)" ),
            },
        ),
    components = (
        keyword.Component.protein_seq_num(
            seq = os.path.join( ROOT, "ins.seq" ),
            num = 1,
            ),
        ),
    atoms_pdb = keyword.AtomPdb(
        xtal = "insulin",
        filename = os.path.join( ROOT, "insulin_hyss_3site_ori.pdb" ),
        ),
    wavelength = keyword.Wavelength( value = 1.5418 ),
    )


IOD = Data(
    data_mr = keyword.MRReflectionData(
        file_name = os.path.join( ROOT, "iod_scala-unique.mtz" ),
        fp = "F_New",
        sigfp = "SIGF_New"
        ),
    data_ep = keyword.EPReflectionData(
        file_name = os.path.join( ROOT, "iod_scala-unique.mtz" ),
        labels_for = {
            ( "iod", "CuKa" ): ( "F_New(+)", "SIGF_New(+)", "F_New(-)", "SIGF_New(-)" ),
            }
        ),
    resolution = keyword.Resolution( high = 1.8 ),
    components = (
        keyword.Component.protein_seq_num(
            seq = os.path.join( ROOT, "hewl.seq" ),
            num = 1,
            ),
        ),
    ensembles = (
        keyword.EnsembleModel(
            name = "lactalbumin",
            descriptions =  [
                keyword.EnsemblePDBFile.Id(
                    module_name = "phaser_regression",
                    file_name = os.path.join( "data", "regression", "1fkq_prot.pdb" ),
                    value = 0.450
                    ),
                ],
            ),
        ),
    atoms = """
# TITLE   SAD model from HA sites
SPACegroup HALL  P 4nw 2abw #P 43 21 2
 ATOM CRYSTal iod TYPE I FRAC  0.1571  0.1571  0.5000 OCC 0.690 ISOB  24.77
 ATOM CRYSTal iod TYPE I FRAC  0.0066  0.3036  0.6503 OCC 0.729 ISOB  25.45
 ATOM CRYSTal iod TYPE I FRAC  0.0923  0.1312  0.3966 OCC 0.502 ISOB  22.22
 ATOM CRYSTal iod TYPE I FRAC  0.1019  0.6086  0.7799 OCC 0.304 ISOB  20.40
 ATOM CRYSTal iod TYPE I FRAC  0.0396  0.9213  0.4190 OCC 0.354 ISOB  28.95
 ATOM CRYSTal iod TYPE I FRAC  0.1258  0.6489  0.1826 OCC 0.292 ISOB  24.32
""",
    wavelength = keyword.Wavelength( value = 1.5418 ),
    partial = keyword.Partial(
        mode = "PDB",
        file_name = os.path.join( ROOT, "LYSO_IOD_PARTIAL.pdb" ),
        errordef = "IDENTITY",
        value = 45.0
        ),
    )


MBP = Data(
    data_ep = keyword.EPReflectionData(
        file_name = os.path.join( ROOT, "mbp_peak_withfc.mtz" ),
        labels_for = {
            ( "MBP", "peak" ): ( "F+", "SIGF+", "F-", "SIGF-" ),
            }
        ),
    components = (
        keyword.Component.protein_seq_num(
            seq = os.path.join( ROOT, "mbp.seq" ),
            num = 2,
            ),
        ),
    atoms_pdb = keyword.AtomPdb(
        xtal = "MBP",
        filename = os.path.join( ROOT, "mbp_hyss_ori.pdb" ),
        ),
    wavelength = keyword.Wavelength( value = 1.3863 ),
    scattering = keyword.Scattering(
        type = "YB",
        fp = -19.7,
        fdp = 22.8,
        fix = "EDGE"
        ),
    )


NAGT = Data(
  data_mr = keyword.MRReflectionData(
    file_name = libtbx.env.under_dist( "phaser", os.path.join( ROOT, "1xhb.mtz" ) ),
      fp = "FOBS_X",
      sigfp = "SIGFOBS_X"
      ),
  resolution = keyword.Resolution( high = 6.0 ),
  components = (
    keyword.Component.protein_seq_num(
      seq = os.path.join( ROOT, "1xhb.seq" ),
      num = 1,
      ),
    ),
  ensembles = (
    keyword.EnsembleModel(
      name = "MR_2D7I_A",
      descriptions =  [
        keyword.EnsemblePDBFile.Id(
          module_name = "phaser_regression",
          file_name = os.path.join( "data", "regression", "sculpt_2D7I_A.pdb" ),
          value = 0.449
          ),
        ]
      ),
    ),
    solution = """
# [No title given]
SOLU SET  RFZ=8.6 TFZ=13.9 PAK=13 LLG=177 TFZ==13.6 LLG=177 TFZ==13.5 OCC=237 PAK=6
SOLU SPAC P 43
SOLU 6DIM ENSE MR_2D7I_A EULER  307.0   76.1  307.5 FRAC -0.29 -0.14  0.25 BFAC -0.03 #TFZ==13.5
SOLU ENSEMBLE MR_2D7I_A VRMS DELTA +0.4547 #RMSD  #VRMS
""",
  )



P9 = Data(
    data_ep = keyword.EPReflectionData(
        file_name = os.path.join( ROOT, "p9_withfc.mtz" ),
        labels_for = {
            ( "p9", "peak" ): ( "F_peak(+)", "SIGF_peak(+)", "F_peak(-)", "SIGF_peak(-)" ),
            }
        ),
    components = (
        keyword.Component.protein_seq_num(
            seq = os.path.join( ROOT, "p9.seq" ),
            num = 1,
            ),
        ),
    atoms_pdb = keyword.AtomPdb(
        xtal = "p9",
        filename = os.path.join( ROOT, "p9_hyss_ori.pdb" ),
        ),
    wavelength = keyword.Wavelength( value = 0.9795 ),
    scattering = keyword.Scattering(
        type = "SE",
        fp = -6,
        fdp = 5,
        fix = "EDGE"
        ),
    )


RUSTI = Data(
    data_ep = keyword.EPReflectionData(
        file_name = os.path.join( ROOT, "rusticyanin_withfc.mtz" ),
        labels_for = {
            ( "rusti", "wave" ): ( "F+", "SIGF+", "F-", "SIGF-" ),
            }
        ),
    components = (
        keyword.Component.protein_seq_num(
            seq = os.path.join( ROOT, "rusti.seq" ),
            num = 1,
            ),
        ),
    atoms = """
ATOM CRYSTal rusti TYPE CU FRAC  0.1501  0.2513  0.4262 OCC 1.0 ISOB 15. FIXX
""",
    wavelength = keyword.Wavelength( value = 1.376 ),
    )


SAV = Data(
    data_ep = keyword.EPReflectionData(
        file_name = os.path.join( ROOT, "sav3_withfc.mtz" ),
        labels_for = {
            ( "sav3", "CuKa" ): ( "F_sav3(+)", "SIGF_sav3(+)", "F_sav3(-)", "SIGF_sav3(-)" ),
            }
        ),
    components = (
        keyword.Component.protein_seq_num(
            seq = os.path.join( ROOT, "sav.seq" ),
            num = 1,
            ),
        ),
    atoms_pdb = keyword.AtomPdb(
        xtal = "sav3",
        filename = os.path.join( ROOT, "sav3_hyss_ori.pdb" ),
        ),
    wavelength = keyword.Wavelength( value = 1.5418 ),
    )


TOXD = Data(
    data_mr = keyword.MRReflectionData(
        file_name = os.path.join(  ROOT , "toxd.mtz" ),
        fp = "FTOXD3",
        sigfp = "SIGFTOXD3"
        ),
    resolution = keyword.Resolution( high = 4.0 ),
    components = (
        keyword.Component.protein_mw_num( mw = 7139, num = 1 ),
        ),
    ensembles = (
        keyword.EnsembleModel(
            name = "toxd",
            descriptions =  [
                keyword.EnsemblePDBFile.Id(
                    module_name = "phaser",
                    file_name = os.path.join(  ROOT , "1D0D_B.pdb" ),
                    value = 0.364
                    ),
                keyword.EnsemblePDBFile.Id(
                    module_name = "phaser",
                    file_name = os.path.join(  ROOT , "1BIK.pdb" ),
                    value = 0.377
                    ),
                ],
            ),
        keyword.EnsembleMap(
            name = "toxd_ed",
            module_name = "phaser_regression",
            hklin = os.path.join( "data", "regression", "masked_density.mtz" ),
            f = "FC_mask",
            phi = "PHIC_mask",
            extent = ( 27.0, 35.0, 27.0 ),
            rms = 1.0,
            centre = ( 7.6, 7.0, 10.1 ),
            pmw = 7133,
            nmw = 0,
            scale = 1.0,
            )
        ),
    rlist = """
# [No title given]
SOLU SET
SOLU TRIAL ENSEMBLE toxd EULER  146.417   18.933  201.205 RF 80.2 RFZ 3.74
SOLU TRIAL ENSEMBLE toxd EULER   73.380   41.036  130.745 RF 76.5 RFZ 3.46
""",
    solution = """
# [No title given]
SPACegroup HALL  P 2ac 2ab #P 21 21 21
SOLU SET RFZ=3.7 TFZ=8.1
SOLU SPAC P 21 21 21
SOLU 6DIM ENSE toxd EULER  146.417   18.933  201.205 FRAC  0.87822  0.17486  0.99321
""",
    solution_ed = """
# [No title given]
SOLU SET at origo
SOLU 6DIM ENSE toxd_ed EULER  0.0       0.0     0.0     FRAC  0.0      0.0      0.0
""",
    )


TOXD_PHASED = Data(
    data_mr = keyword.MRReflectionData(
        file_name = os.path.join(  ROOT , "toxd_phased.mtz" ),
        fp = "FTOXD3",
        sigfp = "SIGFTOXD3",
        fmap = "FWT",
        phmap = "PHWT"
        ),
    components = (
        keyword.Component.protein_mw_num( mw = 7239, num = 1 ),
        ),
    ensembles = (
        keyword.EnsembleMap(
            name = "enstoxd_ed",
            module_name = "phaser_regression",
            hklin = os.path.join( "data", "regression", "masked_density.mtz" ),
            f = "FC_mask",
            phi = "PHIC_mask",
            extent = ( 27.0, 35.0, 27.0 ),
            rms = 1.0,
            centre = ( 7.6, 7.0, 10.1 ),
            pmw = 7133,
            nmw = 0,
            scale = 1.0,
            ),
        keyword.EnsembleModel(
            name = "ens1bik",
            descriptions =  [
                keyword.EnsemblePDBFile.Id(
                    module_name = "phaser",
                    file_name = os.path.join(  ROOT , "1BIK.pdb" ),
                    value = 0.61
                    ),
                ],
            ),
        ),
    )


TRYPSE = Data(
    data_ep = keyword.EPReflectionData(
        file_name = os.path.join( ROOT, "tryparedoxin_peak_withfc.mtz" ),
        labels_for = {
            ( "tryp", "peak" ): ( "F_peak(+)", "SIGF_peak(+)", "F_peak(-)", "SIGF_peak(-)" ),
            }
        ),
    components = (
        keyword.Component.protein_seq_num(
            seq = os.path.join( ROOT, "tryp.seq" ),
            num = 1,
            ),
        ),
    atoms_pdb = keyword.AtomPdb(
        xtal = "tryp",
        filename = os.path.join( ROOT, "tryparedoxin_hyss_ori.pdb" ),
        ),
    wavelength = keyword.Wavelength( value = 0.9795 ),
    scattering = keyword.Scattering(
        type = "SE",
        fp = -8,
        fdp = 4.5,
        fix = "EDGE"
        ),
    )


T0283 = Data(
  data_mr = keyword.MRReflectionData(
    file_name = libtbx.env.under_dist( "phaser", os.path.join( ROOT, "2hh6.mtz" ) ),
      fp = "FOBS",
      sigfp = "SIGFOBS"
      ),
  components = (
    keyword.Component.protein_seq_num(
      seq = os.path.join( ROOT, "2hh6.seq" ),
      num = 1,
      ),
    ),
  ensembles = (
    keyword.EnsembleModel(
      name = "camp",
      descriptions =  [
        keyword.EnsemblePDBFile.Rms(
          module_name = "phaser_regression",
          file_name = os.path.join( "data", "regression", "MR_model2.1.pdb" ),
          value = 1.7
          ),
        ]
      ),
    ),
    solution = """
# origin solution
SOLU SET at origo
SOLU 6DIM ENSE camp EULER  0.0       0.0     0.0     FRAC  0.0      0.0      0.0
""",
  )


TNCS_GIDEON = Data(
  data_mr = keyword.MRReflectionData(
    file_name = libtbx.env.under_dist( "phaser", os.path.join( ROOT, "2cc0_reindex_4A.mtz" ) ),
      ip = "IMEAN",
      sigip = "SIGIMEAN"
      ),
  components = (
    keyword.Component.protein_seq_num(
      seq = os.path.join( ROOT, "2cc0.seq" ),
      num = 2,
      ),
    ),
  ensembles = (
    keyword.EnsembleModel(
      name = "2c1g_trim",
      descriptions =  [
        keyword.EnsemblePDBFile.Id(
          module_name = "phaser_regression",
          file_name = os.path.join( "data", "regression", "2C1G.pdb" ),
          value = 0.33
          ),
        ]
      ),
    ),
  )
