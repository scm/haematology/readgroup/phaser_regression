from phaser import residue_topology

import unittest

class TestSidechainAtom(unittest.TestCase):

    def test_ala(self):

        sc = residue_topology.SidechainAtom(
            topology = residue_topology.ALA
            )

        self.assertEqual( sc.names, [ " CB ", ] )


    def test_phe(self):

        sc = residue_topology.SidechainAtom(
            topology = residue_topology.PHE
            )

        self.assertEqual(
            sorted( sc.names ),
            sorted( [ " CB ", " CG ", " CD1", " CD2", " CE1", " CE2", " CZ " ] )
            )


class TestMainchainDistance(unittest.TestCase):

    def test_ala(self):

        sca = residue_topology.SidechainAtom( topology = residue_topology.ALA )

        dist = residue_topology.MainchainDistance(
            topology = residue_topology.ALA,
            names = sca.names
            )

        self.assertEqual(
            dist.from_atom,
            {
                " CB ": 1,
                }
            )

        dist2 = residue_topology.MainchainDistance(
            topology = residue_topology.ALA,
            names = [ " CZ " ]
            )
        self.assertEqual(
            dist2.from_atom,
            {
                " CZ ": 0,
                }
            )


    def test_phe(self):

        dist = residue_topology.MainchainDistance.All(
            topology = residue_topology.PHE
            )

        self.assertEqual(
            dist.from_atom,
            {
                " CB ": 1,
                " CG ": 2,
                " CD1": 3,
                " CD2": 3,
                " CE1": 4,
                " CE2": 4,
                " CZ ": 5,
                }
            )


class TestSidechainCorrespondence(unittest.TestCase):

    def test_trp_to_lys(self):

        corresp = residue_topology.SidechainCorrespondence(
            source = residue_topology.TRP,
            target = residue_topology.LYS
            )

        self.assertEqual(
            corresp.mapping,
            dict( [ (" CB ", " CB " ), ( " CG ", " CG " ), ( " CD2", " CD " ),
                ( " CE2", " CE " ), ( " CZ2", " NZ " ) ] )
            )


    def test_glu_to_phe(self):

        corresp = residue_topology.SidechainCorrespondence(
            source = residue_topology.GLU,
            target = residue_topology.PHE
            )

        self.assertEqual(
            corresp.mapping,
            dict( [ ( " CB ", " CB " ), ( " CG ", " CG " ), ( " CD ", " CD1" ),
                ( " OE1", " CE1" ) ] )
            )


    def test_ile_to_cys(self):

        corresp = residue_topology.SidechainCorrespondence(
            source = residue_topology.ILE,
            target = residue_topology.CYS
            )

        self.assertEqual(
            corresp.mapping,
            dict( [ ( " CB ", " CB " ), ( " CG1", " SG " ) ] )
            )


suite_sidechain_atom = unittest.TestLoader().loadTestsFromTestCase(
    TestSidechainAtom
    )
suite_mainchain_distance = unittest.TestLoader().loadTestsFromTestCase(
    TestMainchainDistance
    )
suite_sidechain_correspondance = unittest.TestLoader().loadTestsFromTestCase(
    TestSidechainCorrespondence
    )

alltests = unittest.TestSuite(
    [
        suite_sidechain_atom,
        suite_mainchain_distance,
        suite_sidechain_correspondance
        ]
    )


def load_tests(loader, tests, pattern):

    return alltests


if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
