import unittest
import os.path
import random

import iotbx.pdb
import scitbx.matrix
from scitbx.array_family import flex
from cctbx import sgtbx
from cctbx import uctbx
from cctbx import crystal
import libtbx.load_env

from cctbx.development import random_structure

from phaser import matching
from phaser.pipeline.scaffolding import IterableCompareTestCase, Fake, Return

ROOT = libtbx.env.under_dist( "phaser_regression", "data" )

LYSOZYME = None

def lysozyme():

    global LYSOZYME

    if not LYSOZYME:
        input = iotbx.pdb.input(
            os.path.join( ROOT, "regression", "1fkq_prot.pdb" )
            )
        LYSOZYME = input.construct_hierarchy()

    return LYSOZYME


BARSTAR = None

def barstar():

    global BARSTAR

    if not BARSTAR:
        input = iotbx.pdb.input(
            os.path.join( ROOT, "regression", "1brs_barstar_D.pdb" )
            )
        BARSTAR = input.construct_hierarchy()

    return BARSTAR


BARNASE = None

def barnase():

    global BARNASE

    if not BARNASE:
        input = iotbx.pdb.input(
            os.path.join( ROOT, "regression", "1brs_barnase_A.pdb" )
            )
        BARNASE = input.construct_hierarchy()

    return BARNASE


CRYSTAL_1NOL = matching.CrystalInfo(
    space_group = sgtbx.space_group_info( "R 32:R" ).group(),
    cell = uctbx.unit_cell(
        parameters = [ 117.002, 117.002, 117.002, 60.02, 60.02, 60.02 ],
        )
    )


class TestPrincipalComponents(IterableCompareTestCase):

    def test_lysozyme(self):

        princ = matching.PrincipalComponents.from_hierarchy( root = lysozyme() )
        self.assertIterablesAlmostEqual(
            princ.centre,
            ( 20.388778895624363, 24.646063848502582, 29.016722161047426 ),
            8
            )
        self.assertIterablesAlmostEqual(
            princ.r,
            ( 0.15442967916116565, 0.3411550209758925, -0.9272349895561092,
                -0.2730469454070764, -0.8872055852450222, -0.37190269576046725,
                -0.949524533541943, 0.3106114955583196, -0.04385953749162846 ),
            8
            )
        self.assertIterablesAlmostEqual(
            princ.t,
            ( 15.348559057403044, 38.22461649255348, 12.976955032254963 ),
            8
            )
        self.assertIterablesAlmostEqual(
            princ.extent,
            ( 24.878713470242307, 15.839729654418065, 13.921960636233713 ),
            8
            )

    def test_barstar(self):

        princ = matching.PrincipalComponents.from_hierarchy( root = barstar() )
        self.assertIterablesAlmostEqual(
            princ.centre,
            ( 38.701735766861454, 34.50438634913516, 1.350359064227624 ),
            8
            )
        self.assertIterablesAlmostEqual(
            princ.r,
            ( 0.2812127447123806, -0.7047365062180295, -0.6513569290450011,
                -0.5792970411602588, -0.665791381732644, 0.47025181989388004,
                -0.7650714543846862, 0.24508833675665176, -0.5954807947126083 ),
            8
            )
        self.assertIterablesAlmostEqual(
            princ.t,
            ( 14.312645077937551, 44.75751527327344, 21.957083497959704 ),
            8
            )
        self.assertIterablesAlmostEqual(
            princ.extent,
            ( 16.9956927046462, 13.718511858660916, 13.276861660656628 ),
            8
            )


class TestEnsembleInfo(IterableCompareTestCase):

    def setUp(self):

        self.matching_principal = matching.PrincipalComponents


    def tearDown(self):

        matching.PrincipalComponents = self.matching_principal


    def test_from_hierarchy_lysozyme(self):

        princ = Fake()
        princ.centre = ( 20.388778895624363, 24.646063848502582, 29.016722161047426 )
        princ.r = ( 0.15442967916116565, 0.3411550209758925, -0.9272349895561092,
            -0.2730469454070764, -0.8872055852450222, -0.37190269576046725,
            -0.949524533541943, 0.3106114955583196, -0.04385953749162846 )
        princ.t = ( 15.348559057403044, 38.22461649255348, 12.976955032254963 )
        princ.extent = ( 24.878713470242307, 15.839729654418065, 13.921960636233713 )
        matching.PrincipalComponents = Fake()
        matching.PrincipalComponents.from_hierarchy = Return( values = [ princ ] )
        einfo = matching.EnsembleInfo.from_hierarchy( root = lysozyme() )
        self.assertIterablesAlmostEqual( einfo.centre, princ.centre, 8 )
        self.assertAlmostEqual( einfo.extent, 18.21346792029803, 8 )
        self.assertEqual( len( einfo.pgops ), 1 )
        self.assertIterablesAlmostEqual(
            einfo.pgops[0].r.elems,
            ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ),
            7
            )
        self.assertIterablesAlmostEqual(
            einfo.pgops[0].t.elems,
            ( 0, 0, 0 ),
            7
            )
        self.assertEqual(
            matching.PrincipalComponents.from_hierarchy._calls,
            [ ( (), { "root": lysozyme() } ) ]
            )


    def test_from_map(self):

        centre = object()
        extent = ( 1, 2, 3 )
        einfo = matching.EnsembleInfo.from_map( centre = centre, extent = extent )
        self.assertEqual( einfo.centre, centre )
        self.assertAlmostEqual( einfo.extent, 2, 8 )
        self.assertEqual( len( einfo.pgops ), 1 )
        self.assertIterablesAlmostEqual(
            einfo.pgops[0].r.elems,
            ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ),
            7
            )
        self.assertIterablesAlmostEqual(
            einfo.pgops[0].t.elems,
            ( 0, 0, 0 ),
            7
            )


def get_random_site(
    symbol,
    volume,
    tolerance,
    existing = [],
    general_only = True,
    caching = matching.simple_position_differences
    ):

    sgi = sgtbx.space_group_info( symbol = symbol )
    cctbx_cryst = sgi.any_compatible_crystal_symmetry( asu_volume = volume )
    crystinfo = matching.CrystalInfo(
        space_group = sgi.group(),
        cell = cctbx_cryst.unit_cell()
        )
    centre = random_structure.random_site(
        crystal.special_position_settings( cctbx_cryst ),
        existing_sites = existing,
        general_position_only = general_only
        )

    return matching.Position.new(
        centre = scitbx.matrix.col( centre ),
        crystal = crystinfo,
        tolerance = tolerance,
        detail = caching,
        )


class TestPosition(unittest.TestCase):

    ASU_VOLUME = 30**3
    tolerance = 1E-2


    def with_space_group(self, symbol, caching):

        point = get_random_site(
            symbol = symbol,
            volume = self.ASU_VOLUME,
            tolerance = self.tolerance,
            caching = caching,
            )

        for symop in point.crystal.symops:
            equiv = matching.Position.new(
                centre = scitbx.matrix.col( symop.sgop.inverse() * point.centre ),
                crystal = point.crystal,
                tolerance = self.tolerance,
                detail = matching.simple_position_differences,
                )
            self.assertEqual(
                list( point.operations_between( other = equiv ) ),
                [ symop ]
                )

            vector = scitbx.matrix.col(
                point.crystal.cell.fractionalize( flex.random_double_point_on_sphere() )
                )
            equiv2 = matching.Position.new(
                centre = scitbx.matrix.col( equiv.centre ) + 0.9 * self.tolerance * vector,
                crystal = point.crystal,
                tolerance = self.tolerance
                )
            self.assertEqual(
                list( point.operations_between( other = equiv2 ) ),
                [ symop ]
                )

            non_equiv = matching.Position.new(
                centre = scitbx.matrix.col( equiv.centre ) + 1.1 * self.tolerance * vector,
                crystal = point.crystal,
                tolerance = self.tolerance
                )
            self.assertEqual(
                list( point.operations_between( other = non_equiv ) ),
                []
                )


    def test_P422(self):

        self.with_space_group(
          symbol = "P 4 2 2",
          caching = matching.simple_position_differences,
          )
        self.with_space_group(
          symbol = "P 4 2 2",
          caching = matching.cached_position_differences,
          )


    def test_C2(self):

        self.with_space_group(
          symbol = "C 2",
          caching = matching.simple_position_differences,
          )
        self.with_space_group(
          symbol = "C 2",
          caching = matching.cached_position_differences,
          )


class TestOrientation(unittest.TestCase):

    def setUp(self):

        self.rotation = scitbx.matrix.sqr( flex.random_double_r3_rotation_matrix() )
        self.tolerance = 0.05


    def with_point_group(self, pgops):

        rot = matching.Orientation.new(
            rotation = self.rotation,
            pgops = pgops,
            tolerance = self.tolerance,
            detail = matching.cached_rotation_differences
            )

        for pgop in pgops:
            equiv = matching.Orientation.new(
                rotation = self.rotation * pgop.r.inverse(),
                pgops = pgops,
                tolerance = self.tolerance
                )
            self.assertEqual(
                list( rot.operations_between( other = equiv ) ),
                [ pgop ]
                )

            axis = flex.random_double_point_on_sphere()
            misset1 = scitbx.matrix.sqr(
                scitbx.math.r3_rotation_axis_and_angle_as_matrix( axis, 0.9 * self.tolerance )
                )
            equiv2 = equiv = matching.Orientation.new(
                rotation = self.rotation * misset1 * pgop.r.inverse(),
                pgops = pgops,
                tolerance = self.tolerance
                )
            self.assertEqual(
                list( rot.operations_between( other = equiv2 ) ),
                [ pgop ]
                )

            misset2 = scitbx.matrix.sqr(
                scitbx.math.r3_rotation_axis_and_angle_as_matrix( axis, 1.1 * self.tolerance )
                )
            non_equiv = matching.Orientation.new(
                rotation = self.rotation * misset2 * pgop.r.inverse(),
                pgops = pgops,
                tolerance = self.tolerance
                )
            self.assertEqual(
                list( rot.operations_between( other = non_equiv ) ),
                []
                )


    def test_D4(self):

        D4_R = (
            # EULER ( 0 0 0 )
            ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ),
            # Euler ( -89.9359 70.5488 150.003 ) A->C
            (
                0.49963169392265078, -0.86623729866989674,  0.0010549033427916883,
                0.28895767617244938,  0.16551783078558660, -0.9429248692621548000,
                0.81662208628532718,  0.47141997208976411,  0.3330038710073586800,
                ),
            # Euler ( -30.0387 109.375 -149.94 ) A->E
            (
                -0.002181699360786262, -0.57711292989520013,  0.81666143923640533,
                -0.577361355425907050, -0.66606871270912837, -0.47223546585444431,
                 0.816485826851059640, -0.47253903129559827, -0.33174954175335203,
                 ),
            # Euler ( 29.9504 70.48 -89.9739 ) A->G
            (
                 0.49938194009454856,    0.28928722844180843,    0.81665817657564765,
                -0.86638184051065781,    0.16721205699723679,    0.47055566559985007,
                -0.00042934923985563843,-0.94252481529942211,    0.33413588284412143
                ),
            # Euler ( 80.7154 179.857 140.445 ) A->B
            (
                -0.50408189872536291,   0.86365576315619130,    0.0004026717394422718,
                 0.86365371324903850,   0.50407955386723824,    0.0024631208158550875,
                 0.0019243098971902209, 0.0015893835606359647, -0.99999688544080811,
                 ),
            # Euler ( -149.833 109.542 -30.1141 ) A->D
            (
                -0.0019655643571881401,  0.57979154431591695,   -0.8147624817678597,
                 0.57917710004622081,   -0.66353639020105659,   -0.47357506866493848,
                -0.81519937643426088,   -0.47282261369219153,   -0.33449776178993618,
                ),
            # Euler ( 150.356 70.5751 29.9183 ) A->F
            (
                -0.49721771496481515,   -0.2845323464338177,    -0.81964375661505151,
                -0.29091192335276811,   -0.83533802688486702,    0.46645539303484357,
                -0.81740124588622998,    0.47037402635877246,    0.33257101279377288,
                ),
            # Euler ( 90.3118 109.379 90.0007 ) A->H
            (
                -0.99998521468579427,   -0.0017934923379470881, -0.0051335947482896403,
                -0.0054378559173105445,  0.33181055359099459,    0.94333036962066497,
                 1.1525125274350925e-05, 0.94334433793332928,   -0.33181540042062552,
                ),
            )
        D4_T = (
            ( 0, 0, 0 ),
            (  0.023650,  0.4938720,  0.328644 ),
            ( -0.399459,  0.2240120,  0.649999 ),
            ( -0.363914, -0.2147110,  0.310794 ),
            ( -0.708530,  0.3265810,  0.473547 ),
            ( -0.293822,  0.5968100,  0.145836 ),
            ( -0.313503,  0.0960579, -0.176907 ),
            ( -0.710493, -0.0951503,  0.161775 ),
            )
        pgops = [ scitbx.matrix.rt( ( r, t ) ) for ( r, t ) in zip( D4_R, D4_T ) ]
        self.with_point_group( pgops = pgops )


    def test_C6(self):

        C6_RANDOM_AXIS = (
            ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ),
            (
                 0.85066535712643709,   -0.2099692805307474,    -0.48195575670215995,
                 0.5081462995566004,     0.56338631181286503,    0.65144700621679963,
                 0.13474341702615766,   -0.79906743451056594,    0.5859483310606981,
                 ),
            (
                 0.55199607137931117,    0.088207738495105534,  -0.82916809637816236,
                 0.80632331858245354,   -0.30984106456140498,    0.50382657792303343,
                -0.21246892264984463,   -0.94668786280433237,   -0.24215500681790578,
                ),
            (
                 0.40266142850574815,    0.59635403805170595,   -0.69442467935200491,
                 0.59635403805170628,   -0.74645475274854023,   -0.29524085658753235,
                -0.69442467935200469,   -0.29524085658753291,   -0.65620667575720792,
                ),
            (
                 0.55199607137931106,    0.80632331858245354,   -0.21246892264984496,
                 0.088207738495105881,  -0.30984106456140548,   -0.94668786280433215,
                -0.82916809637816247,    0.50382657792303309,   -0.24215500681790625,
                ),
            (
                 0.85066535712643698,   0.50814629955660084,     0.13474341702615752,
                -0.2099692805307474,    0.56338631181286458,    -0.79906743451056628,
                -0.48195575670216034,   0.65144700621679985,    0.58594833106069777,
                ),
            )
        pgops = [ scitbx.matrix.rt( ( r, ( 0, 0, 0 ) ) ) for r in C6_RANDOM_AXIS ]
        self.with_point_group( pgops = pgops )


class TestMRPeakPO(unittest.TestCase):

    def test_1(self):

        crystal = matching.CrystalInfo(
            space_group = sgtbx.space_group_info( symbol = "C2" ).group(),
            cell = uctbx.unit_cell( ( 206.24, 43.51, 83.69, 90, 107.42, 90 ) )
            )
        ensemble = matching.EnsembleInfo(
            centre = ( 38.701735766861454, 34.50438634913516, 1.350359064227624 ),
            extent = sum( ( 24.878713470242307, 15.839729654418065, 13.921960636233713 ) ) / 3.0,
            pgops = [
                scitbx.matrix.rt( ( ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ), ( 0, 0, 0 ) ) ),
                ]
            )
        ref = matching.MRPeakPO.phaser_style(
            rotation = scitbx.math.euler_angles_zyz_matrix( 105.393, 179.442, 105.579 ),
            translation = ( -0.00046, -0.60907, 0.49382 ),
            crystal = crystal,
            ensemble = ensemble,
            dmin = 2.5
            )

        other = matching.MRPeakPO.phaser_style(
            rotation = scitbx.math.euler_angles_zyz_matrix( 105.4, 179.4, 105.6 ),
            translation = ( -0.001, -0.609, 0.494 ),
            crystal = crystal,
            ensemble = ensemble,
            dmin = 2.5
            )
        self.assertEqual(
            list( ref.operations_between( other = other ) ),
            [ ( crystal.symops[0], ensemble.pgops[0] ) ]
            )

        symop = crystal.symops[-1]
        other = matching.MRPeakPO.phaser_style(
            rotation = symop.orth_rotation.inverse() * scitbx.matrix.sqr( scitbx.math.euler_angles_zyz_matrix( 105.4, 179.4, 105.6 ) ),
            translation = symop.sgop.inverse() * ( -0.001, -0.609, 0.494 ),
            crystal = crystal,
            ensemble = ensemble,
            dmin = 2.5
            )
        self.assertEqual(
            list( ref.operations_between( other = other ) ),
            [ ( crystal.symops[-1], ensemble.pgops[0] ) ]
            )

        other = matching.MRPeakPO.phaser_style(
            rotation = scitbx.math.euler_angles_zyz_matrix( 180.099, 51.375, 0.533 ),
            translation = ( -0.03873, 1.28763, 0.07687 ),
            crystal = crystal,
            ensemble = ensemble,
            dmin = 2.5
            )
        self.assertEqual( list( ref.operations_between( other = other ) ), [] )


    def test_reference_setting_transformation(self):

        ensemble = matching.EnsembleInfo(
            extent = 33.480066363,
            centre = ( 72.52002254470484, 17.94423894707569, 15.478253345995112 ),
            pgops = [ scitbx.matrix.rt( ( ( 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 ), ( 0.0, 0.0, 0.0 ) ) ) ]
            )
        hexagonal = matching.MRPeakPO.phaser_style(
            rotation = scitbx.math.euler_angles_zyz_matrix( 188.518, 128.521, 358.448 ),
            translation = ( 0.24192, 0.09749, 0.32264 ),
            crystal = CRYSTAL_1NOL.reference_setting(),
            ensemble = ensemble,
            dmin = 2.5,
            )

        rhombohedral = matching.MRPeakPO.phaser_style(
            rotation = scitbx.math.euler_angles_zyz_matrix( 350.231, 26.635, 48.887 ),
            translation = ( 0.77486, -1.17825, -0.56454 ),
            crystal = CRYSTAL_1NOL,
            ensemble = ensemble,
            dmin = 2.5,
            )

        matches = hexagonal.operations_between( other = rhombohedral.reference_setting() )
        self.assertEqual( len( list( matches ) ), 1 )

        backwards = CRYSTAL_1NOL.change_of_basis_to_reference().inverse()
        rhombset = matching.MRPeakPO(
            orientation = hexagonal.orientation.with_changed_basis( cbop = backwards ),
            position = hexagonal.position.with_changed_basis( cbop = backwards ),
            )
        matches = rhombohedral.operations_between( other = rhombset )
        self.assertEqual( len( list( matches ) ), 1 )



class TestLabelled(unittest.TestCase):

    def test_phaser_mr_solution(self):

        crystal = matching.CrystalInfo(
            space_group = sgtbx.space_group_info( symbol = "C2" ).group(),
            cell = uctbx.unit_cell( ( 206.24, 43.51, 83.69, 90, 107.42, 90 ) )
            )
        ensemble1 = matching.EnsembleInfo(
            centre = ( 38.701735766861454, 34.50438634913516, 1.350359064227624 ),
            extent = sum( ( 24.878713470242307, 15.839729654418065, 13.921960636233713 ) ) / 3.0,
            pgops = [ scitbx.matrix.rt( ( ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ), ( 0, 0, 0 ) ) ) ]
            )
        ensemble2 = matching.EnsembleInfo(
            centre = ( 38.701735766861454, 34.50438634913516, 1.350359064227624 ),
            extent = sum( ( 24.878713470242307, 15.839729654418065, 13.921960636233713 ) ) / 3.0,
            pgops = [ scitbx.matrix.rt( ( ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ), ( 0, 0, 0 ) ) ) ]
            )

        ref = matching.Labelled.phaser_mr_solution(
            rotation = scitbx.math.euler_angles_zyz_matrix( 105.393, 179.442, 105.579 ),
            translation = ( -0.00046, -0.60907, 0.49382 ),
            crystal = crystal,
            ensemble = ensemble1,
            dmin = 2.5
            )

        other1 = matching.Labelled.phaser_mr_solution(
            rotation = scitbx.math.euler_angles_zyz_matrix( 105.4, 179.4, 105.6 ),
            translation = ( -0.001, -0.609, 0.494 ),
            crystal = crystal,
            ensemble = ensemble1,
            dmin = 2.5
            )
        self.assertEqual(
            list( ref.operations_between( other = other1 ) ),
            [ ( crystal.symops[0], ensemble1.pgops[0] ) ]
            )

        other2 = matching.Labelled.phaser_mr_solution(
            rotation = scitbx.math.euler_angles_zyz_matrix( 105.393, 179.442, 105.579 ),
            translation = ( -0.00046, -0.60907, 0.49382 ),
            crystal = crystal,
            ensemble = ensemble2,
            dmin = 2.5
            )
        self.assertEqual( list( ref.operations_between( other = other2 ) ), [] )


    def test_simple_heavy_atom(self):

        crystal = matching.CrystalInfo(
            space_group = sgtbx.space_group_info( symbol = "P 21 21 21" ).group(),
            cell = uctbx.unit_cell( ( 38.184, 50.387, 73.407, 90, 90, 90 ) )
            )
        ref = matching.Labelled.simple_heavy_atom(
            element = "SE",
            position = ( 0.57628, 0.04809, 0.48160 ),
            crystal = crystal,
            tolerance = 0.1
            )
        other1 = matching.Labelled.simple_heavy_atom(
            element = "SE",
            position = ( 0.576, 0.048, 0.482 ),
            crystal = crystal,
            tolerance = 0.1
            )
        self.assertEqual(
            list( ref.operations_between( other = other1 ) ),
            [ crystal.symops[0] ]
            )

        other2 = matching.Labelled.simple_heavy_atom(
            element = "S",
            position = ( 0.57628, 0.04809, 0.48160 ),
            crystal = crystal,
            tolerance = 0.1
            )
        self.assertEqual(
            list( ref.operations_between( other = other2 ) ),
            []
            )


class Handler(object):

    def __init__(self, shift_flags):

        self.flags = shift_flags


    def __call__(self, origdef):

        discrete = scitbx.matrix.col(
            [ 0 if flag else float( frac )
                for ( frac, flag ) in zip( origdef, self.flags ) ]
            )
        continuous = scitbx.matrix.col(
            [ matching.mod_short( flex.random_double() ) if flag else 0 for flag in self.flags ]
            )
        return ( discrete, continuous )


class TestOriginSearchEntity(IterableCompareTestCase):

    ASU_VOLUME = 30**3
    tolerance = 1E-2

    def atom_with_space_group(self, symbol, shifts):

        point = get_random_site(
            symbol = symbol,
            volume = self.ASU_VOLUME,
            tolerance = self.tolerance,
            caching = matching.simple_position_differences,
            )
        si = point.crystal.origin_search_info()
        om_point = matching.OriginSearchEntity(
            entity = point,
            detail_origin = matching.cached_origin_shifts,
            detail_position = matching.cached_position_differences,
            )
        handler = Handler( shift_flags = si.continuous_shift_flags )

        for origdef in shifts:
            ( discrete, continuous ) = handler( origdef = origdef )
            shift = discrete + continuous

            for symop in point.crystal.symops:
                equiv = matching.Position.new(
                    centre = scitbx.matrix.col( symop.sgop.inverse() * point.centre ) + shift,
                    crystal = point.crystal,
                    tolerance = self.tolerance
                    )
                om_equiv = matching.OriginSearchEntity( entity = equiv )
                found = list(
                    om_point.potential_origins_between( other = om_equiv )
                    )
                self.assertTrue( 0 < len( found ) )

                vector = point.crystal.cell.fractionalize( flex.random_double_point_on_sphere() )
                ( err_disc, err_cont ) = handler( origdef = vector )
                norm_err_disc_orth = scitbx.matrix.col( point.crystal.cell.orthogonalize( err_disc ) ).normalize()
                norm_err_disc_frac = scitbx.matrix.col( point.crystal.cell.fractionalize( norm_err_disc_orth ) )
                error = om_point.multiplier * self.tolerance * norm_err_disc_frac
                equiv2 = matching.Position.new(
                    centre = scitbx.matrix.col( equiv.centre ) + 0.9 * error + err_cont,
                    crystal = point.crystal,
                    tolerance = self.tolerance
                    )
                om_equiv2 = matching.OriginSearchEntity( entity = equiv2 )
                found = list(
                    om_point.potential_origins_between( other = om_equiv2 )
                    )
                self.assertTrue( 0 < len( found ) )

                non_equiv = matching.Position.new(
                    centre = scitbx.matrix.col( equiv.centre ) + 1.1 * error + err_cont,
                    crystal = point.crystal,
                    tolerance = self.tolerance
                    )
                om_non_equiv = matching.OriginSearchEntity( entity = non_equiv )
                self.assertEqual(
                    list( om_point.potential_origins_between( other = om_non_equiv ) ),
                    []
                    )


    def test_atoms(self):

        from fractions import Fraction
        tests = [
            (
                "P 21 21 21",
                [
                    ( 0, 0, 0 ),
                    ( Fraction( 1, 2 ), 0, 0 ),
                    ( 0, Fraction( 1, 2 ), 0 ),
                    ( 0, 0, Fraction( 1, 2 ) ),
                    ( 0, Fraction( 1, 2 ), Fraction( 1, 2 ) ),
                    ( Fraction( 1, 2 ), 0, Fraction( 1, 2 ) ),
                    ( Fraction( 1, 2 ), Fraction( 1, 2 ), 0 ),
                    ( Fraction( 1, 2 ), Fraction( 1, 2 ), Fraction( 1, 2 ) ),
                    ],
                ),
            (
                "R 3:H",
                [
                    ( 0, 0, 'z' ),
                    ( Fraction( 1, 3 ), Fraction( 2, 3 ), 'z'),
                    ( Fraction( 2, 3 ), Fraction( 1, 3 ), 'z'),
                    ],
                ),
            (
                "P 2",
                [
                    ( 0, 'y', 0 ),
                    ( 0, 'y', Fraction( 1, 2 ) ),
                    ( Fraction( 1, 2 ), 'y', 0 ),
                    ( Fraction( 1, 2 ), 'y', Fraction( 1, 2 ) ),
                    ],
                ),
            (
                "R 32:H",
                [
                    ( 0, 0, 0 ),
                    ( 0, 0, Fraction( 1, 2 ) ),
                    ( Fraction( 1, 3 ), Fraction( 2, 3 ), Fraction( 2, 3 ) ),
                    ( Fraction( 2, 3 ), Fraction( 1, 3 ), Fraction( 1, 3 ) ),
                    ( Fraction( 1, 3 ), Fraction( 2, 3 ), Fraction( 1, 6 ) ),
                    ( Fraction( 2, 3 ), Fraction( 1, 3 ), Fraction( 5, 6 ) ),
                    ],
                ),
            (
                "I 21 3",
                [
                    ( 0, 0, 0 ),
                    ( Fraction( 1, 2 ), Fraction( 1, 2 ), Fraction( 1, 2 ) ),
                    ],
                ),
            ]
        for ( sg, shifts ) in tests:
            self.atom_with_space_group( symbol = sg, shifts = shifts )


    def coordinate_transformation_test(
        self,
        crystal,
        ensemble,
        ref_rotation,
        ref_translation,
        tri_rotation,
        tri_translation,
        ):

        reference = matching.Labelled.phaser_mr_solution(
            rotation = scitbx.math.euler_angles_zyz_matrix( *ref_rotation ),
            translation = ref_translation,
            crystal = crystal,
            ensemble = ensemble,
            dmin = 2.5,
            )

        trial = matching.Labelled.phaser_mr_solution(
          rotation = scitbx.math.euler_angles_zyz_matrix( *tri_rotation ),
          translation = tri_translation,
          crystal = crystal,
          ensemble = ensemble,
          dmin = 2.5,
          )

        ori_ref = matching.OriginSearchEntity( entity = reference )
        ori_tri = matching.OriginSearchEntity( entity = trial )

        matches = list( ori_ref.potential_origins_between( other = ori_tri ) )
        self.assertEqual( len( matches ), 1 )


    def test_3ixv(self):

        self.coordinate_transformation_test(
            crystal = CRYSTAL_1NOL,
            ensemble = matching.EnsembleInfo(
                extent = 33.480066363,
                centre = ( 72.52002254470484, 17.94423894707569, 15.478253345995112 ),
                pgops = [ scitbx.matrix.rt( ( ( 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 ), ( 0.0, 0.0, 0.0 ) ) ) ]
                ),
            ref_rotation = ( 350.231, 26.635, 48.887 ),
            ref_translation = ( 0.27486, -0.67825, -0.06454 ),
            tri_rotation = ( 81.042, 84.352, 247.984 ),
            tri_translation = ( -0.67822, -0.06449, 0.27480 ),
            )


    def test_3gwj(self):

        self.coordinate_transformation_test(
            crystal = CRYSTAL_1NOL,
            ensemble = matching.EnsembleInfo(
                extent = 32.2814800982,
                centre = ( 4.944491303215677, -15.676272654893044, -34.01880090339728 ),
                pgops = [ scitbx.matrix.rt( ( ( 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 ), ( 0.0, 0.0, 0.0 ) ) ) ]
                ),
            ref_rotation = ( 256.330, 119.981, 121.095 ),
            ref_translation = ( 0.41613, -0.38051, -0.16397 ),
            tri_rotation = ( 165.242, 128.808, 317.719 ),
            tri_translation = ( 0.33583, -0.08381, 0.11956 ),
            )


    def test_1hc1(self):

        self.coordinate_transformation_test(
            crystal = CRYSTAL_1NOL,
            ensemble = matching.EnsembleInfo(
                extent = 33.3301481204,
                centre = ( -29.234244571498753, 57.20735209251235, 96.43854858794192 ),
                pgops = [ scitbx.matrix.rt( ( ( 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 ), ( 0.0, 0.0, 0.0 ) ) ) ]
                ),
            ref_rotation = ( 32.571, 166.712, 90.447 ),
            ref_translation = ( -0.49045, -0.53982, 0.76790 ),
            tri_rotation = ( 317.024, 115.163, 202.888 ),
            tri_translation = ( -0.76752, 0.53952, 0.49078 ),
            )


    def test_3ixv_R3(self):

        crystal = matching.CrystalInfo(
            space_group = sgtbx.space_group_info( "R 3:R" ).group(),
            cell = uctbx.unit_cell(
                parameters = [ 117.002, 117.002, 117.002, 60.02, 60.02, 60.02 ],
                )
            )
        self.coordinate_transformation_test(
            crystal = crystal,
            ensemble = matching.EnsembleInfo(
                extent = 33.480066363,
                centre = ( 72.52002254470484, 17.94423894707569, 15.478253345995112 ),
                pgops = [ scitbx.matrix.rt( ( ( 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 ), ( 0.0, 0.0, 0.0 ) ) ) ]
                ),
            ref_rotation = ( 350.231, 26.635, 48.887 ),
            ref_translation = ( 0.27486, -0.67825, -0.06454 ),
            tri_rotation = ( 81.042, 84.352, 247.984 ),
            tri_translation = ( -0.67822, -0.06449, 0.27480 ),
            )
        coord_shift = flex.random_double()
        self.coordinate_transformation_test(
            crystal = crystal,
            ensemble = matching.EnsembleInfo(
                extent = 33.480066363,
                centre = ( 72.52002254470484, 17.94423894707569, 15.478253345995112 ),
                pgops = [ scitbx.matrix.rt( ( ( 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 ), ( 0.0, 0.0, 0.0 ) ) ) ]
                ),
            ref_rotation = ( 350.231, 26.635, 48.887 ),
            ref_translation = ( 0.27486, -0.67825, -0.06454 ),
            tri_rotation = ( 81.042, 84.352, 247.984 ),
            tri_translation = ( -0.67822 + coord_shift, -0.06449 + coord_shift, 0.27480 + coord_shift ),
            )



class Entity(object):

    def __init__(self, label):

        self.label = label


    def operations_between(self, other):

        if self.label == other.label:
            seq = [ None ]

        else:
            seq = []

        return iter( seq )


    def __repr__(self):

        return "%s(%s, %s)" % ( self.__class__.__name__, self.label, id( self ) )


class TestMatchingFunctions(unittest.TestCase):

    def test_overlap(self):

        labels = [ 1, 2, 3, 4, 5, 6 ]
        left = [ Entity( label = l ) for l in labels + [ 7, 8, 9 ] ]
        right = [ Entity( label = l ) for l in labels + [ 10, 11, 12 ] ]
        ( pairing, singles ) = ( right[:6], right[6:] )
        random.shuffle( right )

        matches = list( matching.overlap( left = left, right = right ) )

        self.assertEqual( len( matches ), 12 )
        self.assertEqual( matches[:6], zip( left[:6], pairing ) )
        self.assertEqual( matches[6:9], zip( left[6:9], [ None ] * 3 ) )
        self.assertEqual(
            set( matches[9:12] ),
            set( zip( [ None ] * 3, singles ) )
            )


    def test_is_equivalent(self):

        labels = [ 1, 2, 3, 4, 5, 6 ]
        left = [ Entity( label = l ) for l in labels ]
        right = [ Entity( label = l ) for l in labels ]
        random.shuffle( right )
        non_match1 = Entity( label = 7 )
        non_match2 = Entity( label = 8 )
        self.assertTrue( matching.is_equivalent( left = left, right = right ) )
        self.assertFalse(
            matching.is_equivalent( left = left + [ non_match1 ], right = right )
            )
        self.assertFalse(
            matching.is_equivalent( left = left, right = right + [ non_match1 ] )
            )
        self.assertTrue(
            matching.is_equivalent( left = left + [ non_match1 ], right = right + [ non_match1 ] )
            )
        self.assertFalse(
            matching.is_equivalent( left = left + [ non_match1 ], right = right + [ non_match2 ] )
            )


    def test_phaser_style_equivalence_r3(self):

        crystal = matching.CrystalInfo(
            space_group = sgtbx.space_group_info( "R 32:R" ).group(),
            cell = uctbx.unit_cell(
                parameters = [ 117.002, 117.002, 117.002, 60.02, 60.02, 60.02 ],
                )
            )
        ensemble1 = matching.EnsembleInfo(
            extent = 33.480066363,
            centre = ( 72.52002254470484, 17.94423894707569, 15.478253345995112 ),
            pgops = [ scitbx.matrix.rt( ( ( 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 ), ( 0.0, 0.0, 0.0 ) ) ) ]
            )
        ensemble2 = matching.EnsembleInfo(
            extent = 33.3301481204,
            centre = ( -29.234244571498753, 57.20735209251235, 96.43854858794192 ),
            pgops = [ scitbx.matrix.rt( ( ( 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 ), ( 0.0, 0.0, 0.0 ) ) ) ]
            )
        ensemble3 = matching.EnsembleInfo(
            extent = 32.2814800982,
            centre = ( 4.944491303215677, -15.676272654893044, -34.01880090339728 ),
            pgops = [ scitbx.matrix.rt( ( ( 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 ), ( 0.0, 0.0, 0.0 ) ) ) ]
            )

        reference = [
            matching.Labelled.phaser_mr_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 350.231, 26.635, 48.887 ),
                translation = ( 0.27486, -0.67825, -0.06454 ),
                crystal = crystal,
                ensemble = ensemble1,
                dmin = 2.5,
                ),
            matching.Labelled.phaser_mr_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 32.571, 166.712, 90.447 ),
                translation = ( -0.49045, -0.53982, 0.76790 ),
                crystal = crystal,
                ensemble = ensemble2,
                dmin = 2.5,
                ),
            matching.Labelled.phaser_mr_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 256.330, 119.981, 121.095 ),
                translation = ( 0.91613, -0.88051, -0.66397 ),
                crystal = crystal,
                ensemble = ensemble3,
                dmin = 2.5,
                ),
                ]

        trial = [
            matching.Labelled.phaser_mr_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 81.042, 84.352, 247.984 ),
                translation = ( -0.67822, -0.06449, 0.27480 ),
                crystal = crystal,
                ensemble = ensemble1,
                dmin = 2.5,
                ),
            matching.Labelled.phaser_mr_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 317.024, 115.163, 202.888 ),
                translation = ( -0.76752, 0.53952, 0.49078 ),
                crystal = crystal,
                ensemble = ensemble2,
                dmin = 2.5,
                ),
            matching.Labelled.phaser_mr_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 165.242, 128.808, 317.719 ),
                translation = ( 0.33583, -0.08381, 0.11956 ),
                crystal = crystal,
                ensemble = ensemble3,
                dmin = 2.5,
                ),
            ]

        ori_ref = [ matching.OriginSearchEntity( entity = r ) for r in reference ]
        ori_tri = [ matching.OriginSearchEntity( entity = r ) for r in trial ]

        self.assertTrue(
            matching.phaser_style_equivalence_match( left = ori_ref, right = ori_tri )
            )

        crystal = matching.CrystalInfo(
            space_group = sgtbx.space_group_info( "R 3:R" ).group(),
            cell = uctbx.unit_cell(
                parameters = [ 117.002, 117.002, 117.002, 60.02, 60.02, 60.02 ],
                )
            )
        reference = [
            matching.Labelled.phaser_mr_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 350.231, 26.635, 48.887 ),
                translation = ( 0.27486, -0.67825, -0.06454 ),
                crystal = crystal,
                ensemble = ensemble1,
                dmin = 2.5,
                ),
            matching.Labelled.phaser_mr_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 256.330, 119.981, 121.095 ),
                translation = ( 0.91613, -0.88051, -0.66397 ),
                crystal = crystal,
                ensemble = ensemble3,
                dmin = 2.5,
                ),
                ]

        shift = 0.15
        trial = [
            matching.Labelled.phaser_mr_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 81.042, 84.352, 247.984 ),
                translation = ( -0.67822 + shift, -0.06449 + shift, 0.27480 + shift ),
                crystal = crystal,
                ensemble = ensemble1,
                dmin = 2.5,
                ),
            matching.Labelled.phaser_mr_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 165.242, 128.808, 317.719 ),
                translation = ( 0.33583 + shift, -0.08381 + shift, 0.11956 + shift ),
                crystal = crystal,
                ensemble = ensemble3,
                dmin = 2.5,
                ),
            ]

        ori_ref = [ matching.OriginSearchEntity( entity = r ) for r in reference ]
        ori_tri = [ matching.OriginSearchEntity( entity = r ) for r in trial ]

        self.assertTrue(
            matching.phaser_style_equivalence_match( left = ori_ref, right = ori_tri )
            )

    def test_phaser_style_equivalence_p212121(self):

        crystal = matching.CrystalInfo(
            space_group = sgtbx.space_group_info( "P 21 21 21" ).group(),
            cell = uctbx.unit_cell(
                parameters = [ 50, 60, 70, 90, 90, 90 ],
                )
            )
        ensemble = matching.EnsembleInfo(
            extent = 18.21346792029803,
            centre = ( 20.388778895624363, 24.646063848502582, 29.016722161047426 ),
            pgops = [ scitbx.matrix.rt( ( ( 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 ), ( 0.0, 0.0, 0.0 ) ) ) ]
            )

        reference = [
            matching.Labelled.phaser_mr_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 10, 20, 30 ),
                translation = ( 0.08, 0.22, 0.16 ),
                crystal = crystal,
                ensemble = ensemble,
                dmin = 2.5,
                ),
            matching.Labelled.phaser_mr_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 40, 50, 60 ),
                translation = ( 0.24, 0.09, 0.07 ),
                crystal = crystal,
                ensemble = ensemble,
                dmin = 2.5,
                ),
            matching.Labelled.phaser_mr_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 70, 80, 90 ),
                translation = ( 0.16, 0.02, 0.23 ),
                crystal = crystal,
                ensemble = ensemble,
                dmin = 2.5,
                ),
                ]
        ori_ref = [ matching.OriginSearchEntity( entity = r ) for r in reference ]
        shifts = [
            scitbx.matrix.col( ( 0, 0, 0 ) ),
            scitbx.matrix.col( ( 0.5, 0, 0 ) ),
            scitbx.matrix.col( ( 0, 0.5, 0 ) ),
            scitbx.matrix.col( ( 0, 0, 0.5 ) ),
            scitbx.matrix.col( ( 0, 0.5, 0.5 ) ),
            scitbx.matrix.col( ( 0.5, 0, 0.5 ) ),
            scitbx.matrix.col( ( 0.5, 0.5, 0 ) ),
            scitbx.matrix.col( ( 0.5, 0.5, 0.5 ) ),
            ]

        for shift in shifts:
            for symop in crystal.symops:
                trial = [
                    matching.Labelled.phaser_mr_solution(
                        rotation = symop.orth_rotation * scitbx.matrix.sqr( scitbx.math.euler_angles_zyz_matrix( 10, 20, 30 ) ),
                        translation = scitbx.matrix.col( symop.sgop * ( 0.08, 0.22, 0.16 ) ) + shift,
                        crystal = crystal,
                        ensemble = ensemble,
                        dmin = 2.5,
                        ),
                    matching.Labelled.phaser_mr_solution(
                        rotation = symop.orth_rotation * scitbx.matrix.sqr( scitbx.math.euler_angles_zyz_matrix( 40, 50, 60 ) ),
                        translation = scitbx.matrix.col( symop.sgop * ( 0.24, 0.09, 0.07 ) ) + shift,
                        crystal = crystal,
                        ensemble = ensemble,
                        dmin = 2.5,
                        ),
                    matching.Labelled.phaser_mr_solution(
                        rotation = symop.orth_rotation * scitbx.matrix.sqr( scitbx.math.euler_angles_zyz_matrix( 70, 80, 90 ) ),
                        translation = scitbx.matrix.col( symop.sgop * ( 0.16, 0.02, 0.23 ) ) + shift,
                        crystal = crystal,
                        ensemble = ensemble,
                        dmin = 2.5,
                        ),
                    ]
                ori_tri = [ matching.OriginSearchEntity( entity = r ) for r in trial ]
                self.assertTrue(
                    matching.phaser_style_equivalence_match( left = ori_ref, right = ori_tri )
                    )

    def test_phaser_style_equivalence_p21(self):

        crystal = matching.CrystalInfo(
            space_group = sgtbx.space_group_info( "P 21" ).group(),
            cell = uctbx.unit_cell(
                parameters = [ 50, 60, 70, 90, 100, 90 ],
                )
            )
        ensemble = matching.EnsembleInfo(
            extent = 18.21346792029803,
            centre = ( 20.388778895624363, 24.646063848502582, 29.016722161047426 ),
            pgops = [ scitbx.matrix.rt( ( ( 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 ), ( 0.0, 0.0, 0.0 ) ) ) ]
            )

        reference = [
            matching.Labelled.phaser_mr_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 10, 20, 30 ),
                translation = ( 0.06, 0.00, 0.16 ),
                crystal = crystal,
                ensemble = ensemble,
                dmin = 2.5,
                ),
            matching.Labelled.phaser_mr_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 40, 50, 60 ),
                translation = ( 0.24, 0.13, 0.09 ),
                crystal = crystal,
                ensemble = ensemble,
                dmin = 2.5,
                ),
                ]
        ori_ref = [ matching.OriginSearchEntity( entity = r ) for r in reference ]
        rshifts = [
            scitbx.matrix.col( ( 0, 0, 0 ) ),
            scitbx.matrix.col( ( 0, 0, 0.5 ) ),
            scitbx.matrix.col( ( 0.5, 0, 0 ) ),
            scitbx.matrix.col( ( 0.5, 0, 0.5 ) ),
            ]
        cshifts = [
            scitbx.matrix.col( ( 0, 0.11, 0 ) ),
            scitbx.matrix.col( ( 0, 0.46, 0 ) ),
            scitbx.matrix.col( ( 0, 0.56, 0 ) ),
            ]

        for rshift in rshifts:
            for cshift in cshifts:
                for symop in crystal.symops:
                    trial = [
                        matching.Labelled.phaser_mr_solution(
                            rotation = symop.orth_rotation * scitbx.matrix.sqr( scitbx.math.euler_angles_zyz_matrix( 10, 20, 30 ) ),
                            translation = scitbx.matrix.col( symop.sgop * ( 0.06, 0.00, 0.16 ) ) + rshift + cshift,
                            crystal = crystal,
                            ensemble = ensemble,
                            dmin = 2.5,
                            ),
                        matching.Labelled.phaser_mr_solution(
                            rotation = symop.orth_rotation * scitbx.matrix.sqr( scitbx.math.euler_angles_zyz_matrix( 40, 50, 60 ) ),
                            translation = scitbx.matrix.col( symop.sgop * ( 0.24, 0.13, 0.09 ) ) + rshift + cshift,
                            crystal = crystal,
                            ensemble = ensemble,
                            dmin = 2.5,
                            ),
                        ]
                    ori_tri = [ matching.OriginSearchEntity( entity = r ) for r in trial ]
                    self.assertTrue(
                        matching.phaser_style_equivalence_match( left = ori_ref, right = ori_tri )
                        )


    def test_phaser_style_equivalence_p1(self):

        crystal = matching.CrystalInfo(
            space_group = sgtbx.space_group_info( "P 1" ).group(),
            cell = uctbx.unit_cell(
                parameters = [ 50, 60, 70, 90, 100, 90 ],
                )
            )
        ensemble = matching.EnsembleInfo(
            extent = 18.21346792029803,
            centre = ( 20.388778895624363, 24.646063848502582, 29.016722161047426 ),
            pgops = [ scitbx.matrix.rt( ( ( 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 ), ( 0.0, 0.0, 0.0 ) ) ) ]
            )

        reference = [
            matching.Labelled.phaser_mr_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 10, 20, 30 ),
                translation = ( 0.06, 0.00, 0.16 ),
                crystal = crystal,
                ensemble = ensemble,
                dmin = 2.5,
                ),
            matching.Labelled.phaser_mr_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 40, 50, 60 ),
                translation = ( 0.24, 0.13, 0.09 ),
                crystal = crystal,
                ensemble = ensemble,
                dmin = 2.5,
                ),
                ]
        ori_ref = [ matching.OriginSearchEntity( entity = r ) for r in reference ]
        cshifts = [
            scitbx.matrix.col( ( 0.23, 0.11, 0.87 ) ),
            scitbx.matrix.col( ( -0.12, 0.46, 0.68 ) ),
            scitbx.matrix.col( ( -0.10, 0.56, -0.76 ) ),
            ]

        for cshift in cshifts:
            trial = [
                matching.Labelled.phaser_mr_solution(
                    rotation = scitbx.math.euler_angles_zyz_matrix( 10, 20, 30 ),
                    translation = scitbx.matrix.col( ( 0.06, 0.00, 0.16 ) ) + cshift,
                    crystal = crystal,
                    ensemble = ensemble,
                    dmin = 2.5,
                    ),
                matching.Labelled.phaser_mr_solution(
                    rotation = scitbx.math.euler_angles_zyz_matrix( 40, 50, 60 ),
                    translation = scitbx.matrix.col( ( 0.24, 0.13, 0.09 ) ) + cshift,
                    crystal = crystal,
                    ensemble = ensemble,
                    dmin = 2.5,
                    ),
                ]
            ori_tri = [ matching.OriginSearchEntity( entity = r ) for r in trial ]
            self.assertTrue(
                matching.phaser_style_equivalence_match( left = ori_ref, right = ori_tri )
                )

        trial = [
            matching.Labelled.phaser_mr_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 10, 20, 30 ),
                translation = scitbx.matrix.col( ( 0.06, 0.00, 0.16 ) ),
                crystal = crystal,
                ensemble = ensemble,
                dmin = 2.5,
                ),
            matching.Labelled.phaser_mr_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 40, 50, 60 ),
                translation = scitbx.matrix.col( ( 0.24, 0.13, 0.13 ) ),
                crystal = crystal,
                ensemble = ensemble,
                dmin = 2.5,
                ),
            ]
        ori_tri = [ matching.OriginSearchEntity( entity = r ) for r in trial ]
        self.assertFalse(
            matching.phaser_style_equivalence_match( left = ori_ref, right = ori_tri )
            )
        trial = [
            matching.Labelled.phaser_mr_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 10, 20, 30 ),
                translation = scitbx.matrix.col( ( 0.06, 0.00, 0.16 ) ) + scitbx.matrix.col( ( 0.1, 0.1, 0.1 ) ),
                crystal = crystal,
                ensemble = ensemble,
                dmin = 2.5,
                ),
            matching.Labelled.phaser_mr_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 40, 50, 60 ),
                translation = scitbx.matrix.col( ( 0.24, 0.13, 0.09 ) ),
                crystal = crystal,
                ensemble = ensemble,
                dmin = 2.5,
                ),
            ]
        ori_tri = [ matching.OriginSearchEntity( entity = r ) for r in trial ]
        self.assertFalse(
            matching.phaser_style_equivalence_match( left = ori_ref, right = ori_tri )
            )



suite_principal_components = unittest.TestLoader().loadTestsFromTestCase(
    TestPrincipalComponents
    )
suite_ensemble_info = unittest.TestLoader().loadTestsFromTestCase(
    TestEnsembleInfo
    )
suite_position = unittest.TestLoader().loadTestsFromTestCase(
    TestPosition
    )
suite_orientation = unittest.TestLoader().loadTestsFromTestCase(
    TestOrientation
    )
suite_mrpeak_po = unittest.TestLoader().loadTestsFromTestCase(
    TestMRPeakPO
    )
suite_labelled = unittest.TestLoader().loadTestsFromTestCase(
    TestLabelled
    )
suite_origin_search_entity = unittest.TestLoader().loadTestsFromTestCase(
    TestOriginSearchEntity
    )
suite_matching_functions = unittest.TestLoader().loadTestsFromTestCase(
    TestMatchingFunctions
    )

alltests = unittest.TestSuite(
    [
        suite_principal_components,
        suite_ensemble_info,
        suite_position,
        suite_orientation,
        suite_mrpeak_po,
        suite_labelled,
        suite_origin_search_entity,
        suite_matching_functions,
        ]
    )


def load_tests(loader, tests, pattern):

    return alltests


if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
