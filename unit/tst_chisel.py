from phaser import chisel
from phaser import mmt
from phaser import rsam
from phaser.pipeline.scaffolding import Append, Return, Raise, Fake, IterableCompareTestCase

import iotbx.pdb
import iotbx.bioinformatics
from scitbx.array_family import flex

import unittest

class TestEntity(unittest.TestCase):

    def test_root(self):

        r = iotbx.pdb.hierarchy.root()
        e = chisel.Entity.Create( obj = r )
        self.generic_tests( obj = r, entity = e )
        self.assertEqual( e._level, 0 )
        self.assertEqual( e.level_identifier(), "" )
        self.assertEqual( e.level_name(), "root ()" )
        self.assertEqual( str( e ), "root ()" )
        self.assertEqual( e.level_selector(), "all" )
        self.assertEqual( e.selector(), "all" )
        self.assertEqual( list( e.hierarchy() ), [ e ] )


    def test_model(self):

        m = iotbx.pdb.hierarchy.model()
        m.id = "1"
        e = chisel.Entity.Create( obj = m )
        self.generic_tests( obj = m, entity = e )
        self.assertEqual( e._level, 1 )
        self.assertEqual( e.level_identifier(), "id = '1'" )
        self.assertEqual( e.level_name(), "model (id = '1')" )
        self.assertEqual( str( e ), "model (id = '1')" )
        self.assertEqual( e.level_selector(), "model '1'" )
        self.assertEqual( e.selector(), "model '1'" )
        self.assertEqual( e.parent(), None )
        self.assertEqual( list( e.hierarchy() ), [ e ] )

        r = iotbx.pdb.hierarchy.root()
        r.append_model( m )
        self.assertEqual( str( e ), "model (id = '1')" )
        self.assertEqual( e.selector(), "all and model '1'" )
        self.assertEqual( e.parent() , chisel.Entity.Root( obj = r ) )
        self.assertEqual( list( e.hierarchy() ), [ chisel.Entity.Root( r ), e ] )


    def test_chain(self):

        c = iotbx.pdb.hierarchy.chain()
        c.id = "A"
        e = chisel.Entity.Create( obj = c )
        self.generic_tests( obj = c, entity = e )
        self.assertEqual( e._level, 2 )
        self.assertEqual( e.level_identifier(), "id = 'A'" )
        self.assertEqual( e.level_name(), "chain (id = 'A')" )
        self.assertEqual( str( e ), "chain (id = 'A')" )
        self.assertEqual( e.level_selector(), "chain 'A'" )
        self.assertEqual( e.selector(), "chain 'A'" )
        self.assertEqual( e.parent(), None )
        self.assertEqual( list( e.hierarchy() ), [ e ] )

        r = iotbx.pdb.hierarchy.root()
        m = iotbx.pdb.hierarchy.model()
        m.id = "1"
        r.append_model( m )
        m.append_chain( c )
        self.assertEqual( str( e ), "model (id = '1'), chain (id = 'A')" )
        self.assertEqual( e.selector(), "all and model '1' and chain 'A'" )
        self.assertEqual( e.parent() , chisel.Entity.Model( obj = m ) )
        self.assertEqual(
            list( e.hierarchy() ),
            [ chisel.Entity.Root( r ), chisel.Entity.Model( m ), e ]
            )


    def test_residue_group(self):

        rg = iotbx.pdb.hierarchy.residue_group()
        rg.resseq = 312
        rg.icode = ""
        e = chisel.Entity.Create( obj = rg )
        self.generic_tests( obj = rg, entity = e )
        self.assertEqual( e._level, 3 )
        self.assertEqual( e.level_identifier(), "resid = ' 312 '" )
        self.assertEqual( e.level_name(), "residue_group (resid = ' 312 ')" )
        self.assertEqual( str( e ), "residue_group (resid = ' 312 ')" )
        self.assertEqual( e.level_selector(), "resseq ' 312' and icode ''" )
        self.assertEqual( e.selector(), "resseq ' 312' and icode ''" )
        self.assertEqual( e.parent(), None )
        self.assertEqual( list( e.hierarchy() ), [ e ] )

        r = iotbx.pdb.hierarchy.root()
        m = iotbx.pdb.hierarchy.model()
        m.id = "1"
        r.append_model( m )
        c = iotbx.pdb.hierarchy.chain()
        c.id = "A"
        m.append_chain( c )
        c.append_residue_group( rg )
        self.assertEqual(
            str( e ),
            "model (id = '1'), chain (id = 'A'), residue_group (resid = ' 312 ')"
            )
        self.assertEqual(
            e.selector(),
            "all and model '1' and chain 'A' and resseq ' 312' and icode ''"
            )
        self.assertEqual( e.parent() , chisel.Entity.Chain( obj = c ) )
        self.assertEqual(
            list( e.hierarchy() ),
            [
                chisel.Entity.Root( r ),
                chisel.Entity.Model( m ),
                chisel.Entity.Chain( c ),
                e
                ]
            )


    def test_atom_group(self):

        ag = iotbx.pdb.hierarchy.atom_group()
        ag.resname = "GLY"
        ag.altloc = ""
        e = chisel.Entity.Create( obj = ag )
        self.generic_tests( obj = ag, entity = e )
        self.assertEqual( e._level, 4 )
        self.assertEqual( e.level_identifier(), "resname = 'GLY', altloc = ''" )
        self.assertEqual( e.level_name(), "atom_group (resname = 'GLY', altloc = '')" )
        self.assertEqual( str( e ), "atom_group (resname = 'GLY', altloc = '')" )
        self.assertEqual( e.level_selector(), "altid ''" )
        self.assertEqual( e.selector(), "altid ''" )
        self.assertEqual( e.parent(), None )
        self.assertEqual( list( e.hierarchy() ), [ e ] )

        r = iotbx.pdb.hierarchy.root()
        m = iotbx.pdb.hierarchy.model()
        m.id = "1"
        r.append_model( m )
        c = iotbx.pdb.hierarchy.chain()
        c.id = "A"
        m.append_chain( c )
        rg = iotbx.pdb.hierarchy.residue_group()
        rg.resseq = 312
        rg.icode = ""
        c.append_residue_group( rg )
        rg.append_atom_group( ag )
        self.assertEqual(
            str( e ),
            "model (id = '1'), chain (id = 'A'), residue_group (resid = ' 312 '), "
            + "atom_group (resname = 'GLY', altloc = '')"
            )
        self.assertEqual(
            e.selector(),
            "all and model '1' and chain 'A' and resseq ' 312' and icode '' and altid ''"
            )
        self.assertEqual( e.parent() , chisel.Entity.ResidueGroup( obj = rg ) )
        self.assertEqual(
            list( e.hierarchy() ),
            [
                chisel.Entity.Root( r ),
                chisel.Entity.Model( m ),
                chisel.Entity.Chain( c ),
                chisel.Entity.ResidueGroup( rg ),
                e
                ]
            )


    def test_atom(self):

        a = iotbx.pdb.hierarchy.atom()
        a.name = " CA "
        e = chisel.Entity.Create( obj = a )
        self.generic_tests( obj = a, entity = e )
        self.assertEqual( e._level, 5 )
        self.assertEqual( e.level_identifier(), "name = ' CA '" )
        self.assertEqual( e.level_name(), "atom (name = ' CA ')" )
        self.assertEqual( str( e ), "atom (name = ' CA ')" )
        self.assertEqual( e.level_selector(), "name ' CA '" )
        self.assertEqual( e.selector(), "name ' CA '" )
        self.assertEqual( e.parent(), None )
        self.assertEqual( list( e.hierarchy() ), [ e ] )

        r = iotbx.pdb.hierarchy.root()
        m = iotbx.pdb.hierarchy.model()
        m.id = "1"
        r.append_model( m )
        c = iotbx.pdb.hierarchy.chain()
        c.id = "A"
        m.append_chain( c )
        rg = iotbx.pdb.hierarchy.residue_group()
        rg.resseq = 312
        rg.icode = ""
        c.append_residue_group( rg )
        ag = iotbx.pdb.hierarchy.atom_group()
        ag.resname = "GLY"
        ag.altloc = ""
        rg.append_atom_group( ag )
        ag.append_atom( a )
        self.assertEqual(
            str( e ),
            "model (id = '1'), chain (id = 'A'), residue_group (resid = ' 312 '), "
            + "atom_group (resname = 'GLY', altloc = ''), atom (name = ' CA ')"
            )
        self.assertEqual(
            e.selector(),
            "all and model '1' and chain 'A' and resseq ' 312' and icode '' and "
            + "altid '' and name ' CA '"
            )
        self.assertEqual( e.parent() , chisel.Entity.AtomGroup( obj = ag ) )
        self.assertEqual(
            list( e.hierarchy() ),
            [
                chisel.Entity.Root( r ),
                chisel.Entity.Model( m ),
                chisel.Entity.Chain( c ),
                chisel.Entity.ResidueGroup( rg ),
                chisel.Entity.AtomGroup( ag ),
                e
                ]
            )


    def generic_tests(self, obj, entity):

        self.assertEqual( entity._object, obj )
        self.assertEqual( hash( entity ), hash( obj.memory_id() ) )
        e2 = chisel.Entity( obj = entity._object, level = entity._level )
        self.assertFalse( entity is e2 )
        self.assertTrue( entity == e2 )
        self.assertFalse( entity != e2 )


class TestIndexer(unittest.TestCase):

    def setUp(self):

        self.r = iotbx.pdb.hierarchy.root()
        self.m = iotbx.pdb.hierarchy.model()
        self.c = iotbx.pdb.hierarchy.chain()
        self.rg = iotbx.pdb.hierarchy.residue_group()
        self.ag = iotbx.pdb.hierarchy.atom_group()
        self.a0 = iotbx.pdb.hierarchy.atom()
        self.a1 = iotbx.pdb.hierarchy.atom()

        self.r.append_model( self.m )
        self.m.append_chain( self.c )
        self.c.append_residue_group( self.rg )
        self.rg.append_atom_group( self.ag )
        self.ag.append_atom( self.a0 )
        self.ag.append_atom( self.a1 )


    def test_forward_indexer(self):

        indexer = chisel.ForwardIndexer( root = self.r )
        self.assertEqual( indexer.arrays[0][ chisel.Entity.Root( self.r ) ], 0 )
        self.assertEqual( indexer.arrays[1][ chisel.Entity.Model( self.m ) ], 0 )
        self.assertEqual( indexer.arrays[2][ chisel.Entity.Chain( self.c ) ], 0 )
        self.assertEqual( indexer.arrays[3][ chisel.Entity.ResidueGroup( self.rg ) ], 0 )
        self.assertEqual( indexer.arrays[4][ chisel.Entity.AtomGroup( self.ag ) ], 0 )
        self.assertEqual( indexer.arrays[5][ chisel.Entity.Atom( self.a0 ) ], 0 )
        self.assertEqual( indexer.arrays[5][ chisel.Entity.Atom( self.a1 ) ], 1 )
        d = chisel.Entity.Root( self.r ).as_descriptor( indexer = indexer )
        self.assertEqual( ( d.level, d.index ), ( 0, 0 ) )
        d = chisel.Entity.Model( self.m ).as_descriptor( indexer = indexer )
        self.assertEqual( ( d.level, d.index ), ( 1, 0 ) )
        d = chisel.Entity.Chain( self.c ).as_descriptor( indexer = indexer )
        self.assertEqual( ( d.level, d.index ), ( 2, 0 ) )
        d = chisel.Entity.ResidueGroup( self.rg ).as_descriptor( indexer = indexer )
        self.assertEqual( ( d.level, d.index ), ( 3, 0 ) )
        d = chisel.Entity.AtomGroup( self.ag ).as_descriptor( indexer = indexer )
        self.assertEqual( ( d.level, d.index ), ( 4, 0 ) )
        d = chisel.Entity.Atom( self.a0 ).as_descriptor( indexer = indexer )
        self.assertEqual( ( d.level, d.index ), ( 5, 0 ) )
        d = chisel.Entity.Atom( self.a1 ).as_descriptor( indexer = indexer )
        self.assertEqual( ( d.level, d.index ), ( 5, 1 ) )


    def test_reverse_indexer(self):

        indexer = chisel.ReverseIndexer( root = self.r )
        self.assertEqual(
            chisel.Entity.Root( indexer.arrays[0][0] ),
            chisel.Entity.Root( self.r )
            )
        self.assertEqual(
            chisel.Entity.Model( indexer.arrays[1][0] ),
            chisel.Entity.Model( self.m )
            )
        self.assertEqual(
            chisel.Entity.Chain( indexer.arrays[2][0] ),
            chisel.Entity.Chain( self.c )
            )
        self.assertEqual(
            chisel.Entity.ResidueGroup( indexer.arrays[3][0] ),
            chisel.Entity.ResidueGroup( self.rg )
            )
        self.assertEqual(
            chisel.Entity.AtomGroup( indexer.arrays[4][0] ),
            chisel.Entity.AtomGroup( self.ag )
            )
        self.assertEqual(
            chisel.Entity.Atom( indexer.arrays[5][0] ),
            chisel.Entity.Atom( self.a0 )
            )
        self.assertEqual(
            chisel.Entity.Atom( indexer.arrays[5][1] ),
            chisel.Entity.Atom( self.a1 )
            )
        self.assertEqual(
            chisel.Descriptor( level = 0, index = 0 ).as_entity( indexer = indexer ),
            chisel.Entity.Root( self.r )
            )
        self.assertEqual(
            chisel.Descriptor( level = 1, index = 0 ).as_entity( indexer = indexer ),
            chisel.Entity.Model( self.m )
            )
        self.assertEqual(
            chisel.Descriptor( level = 2, index = 0 ).as_entity( indexer = indexer ),
            chisel.Entity.Chain( self.c )
            )
        self.assertEqual(
            chisel.Descriptor( level = 3, index = 0 ).as_entity( indexer = indexer ),
            chisel.Entity.ResidueGroup( self.rg )
            )
        self.assertEqual(
            chisel.Descriptor( level = 4, index = 0 ).as_entity( indexer = indexer ),
            chisel.Entity.AtomGroup( self.ag )
            )
        self.assertEqual(
            chisel.Descriptor( level = 5, index = 0 ).as_entity( indexer = indexer ),
            chisel.Entity.Atom( self.a0 )
            )
        self.assertEqual(
            chisel.Descriptor( level = 5, index = 1 ).as_entity( indexer = indexer ),
            chisel.Entity.Atom( self.a1 )
            )

    def test_blueprint(self):

        instr = [
            ( object(), chisel.Entity.Root( self.r ) ),
            ( object(), chisel.Entity.Chain( self.c ) ),
            ( object(), chisel.Entity.Atom( self.a0 ) ),
            ( object(), chisel.Entity.Atom( self.a0 ) ),
            ]
        bp = chisel.Blueprint.from_instructions(
            instructions = instr,
            indexer = chisel.ForwardIndexer( root = self.r )
            )
        self.assertEqual(
            bp.instructions( r_indexer = chisel.ReverseIndexer( root = self.r ) ),
            instr
            )


class TestDeleteInstruction(unittest.TestCase):

    def setUp(self):

        self.i = chisel.DeleteInstruction()


    def test_apply_to(self):

        op = Fake()
        op.remove_atom = Append()
        p = Fake()
        p.object = Return( values = [ op ] )
        p.iotbx_class = Return( values = [ iotbx.pdb.hierarchy.atom_group ] )
        entity = Fake()
        entity.parent = Return( values = [ p ] )
        oe = object()
        entity.object = Return( values = [ oe ] )
        self.i.apply_to( entity = entity )
        self.assertEqual( entity.parent._calls, [ ( (), {}) ] )
        self.assertEqual( p.iotbx_class._calls, [ ( (), {} ) ] )
        self.assertEqual( p.object._calls, [ ( (), {} ) ] )
        self.assertEqual( entity.object._calls, [ ( (), {} ) ] )
        self.assertEqual( op.remove_atom._calls, [ ( ( oe, ), {} ) ] )


class TestSetInstruction(unittest.TestCase):

    def setUp(self):

        self.e = Fake()
        self.o = Fake()
        self.e.object = Return( values = [ self.o ] )
        self.v = object()


    def test_resseq(self):

        i = chisel.SetInstruction.Resseq( value = self.v )
        i.apply_to( entity = self.e )
        self.assertEqual( self.e.object._calls, [ ( (), {} ) ] )
        self.assertEqual( self.o.resseq, self.v )


    def test_icode(self):

        i = chisel.SetInstruction.Icode( value = self.v )
        i.apply_to( entity = self.e )
        self.assertEqual( self.e.object._calls, [ ( (), {} ) ] )
        self.assertEqual( self.o.icode, self.v )


    def test_resname(self):

        i = chisel.SetInstruction.Resname( value = self.v )
        i.apply_to( entity = self.e )
        self.assertEqual( self.e.object._calls, [ ( (), {} ) ] )
        self.assertEqual( self.o.resname, self.v )


    def test_bfactor(self):

        i = chisel.SetInstruction.Bfactor( value = self.v )
        self.o.set_b = Append()
        i.apply_to( entity = self.e )
        self.assertEqual( self.e.object._calls, [ ( (), {} ) ] )
        self.assertEqual( self.o.set_b._calls, [ ( ( self.v, ), {} ) ]  )


    def test_element(self):

        i = chisel.SetInstruction.Element( value = self.v )
        self.o.set_element = Append()
        i.apply_to( entity = self.e )
        self.assertEqual( self.e.object._calls, [ ( (), {} ) ] )
        self.assertEqual( self.o.set_element._calls, [ ( ( self.v, ), {} ) ]  )


    def test_hetero(self):

        i = chisel.SetInstruction.Hetero( value = self.v )
        self.o.set_hetero = Append()
        i.apply_to( entity = self.e )
        self.assertEqual( self.e.object._calls, [ ( (), {} ) ] )
        self.assertEqual( self.o.set_hetero._calls, [ ( ( self.v, ), {} ) ]  )


    def test_name(self):

        i = chisel.SetInstruction.Name( value = self.v )
        self.o.set_name = Append()
        i.apply_to( entity = self.e )
        self.assertEqual( self.e.object._calls, [ ( (), {} ) ] )
        self.assertEqual( self.o.set_name._calls, [ ( ( self.v, ), {} ) ]  )


class TestAttachInstruction(unittest.TestCase):

    def setUp(self):

        self.d = Fake()
        self.i = chisel.AttachInstruction( data = self.d )


    def test_apply_to(self):

        oa = object()
        self.d.as_iotbx_object = Return( values = [ oa ] )
        oe = Fake()
        oe.append_atom = Append()
        e = Fake()
        e.object = Return( values = [ oe ] )
        e.iotbx_class = Return( values = [ iotbx.pdb.hierarchy.atom_group ] )
        self.i.apply_to( entity = e )

        self.assertEqual( e.iotbx_class._calls, [ ( (), {} ) ] )
        self.assertEqual( e.object._calls, [ ( (), {} ) ] )
        self.assertEqual( oe.append_atom._calls, [ ( ( oa, ), {} ) ] )


class TestResetIsotropicInstruction(unittest.TestCase):

    def setUp(self):

        self.i = chisel.ResetIsotropicInstruction()


    def test_apply_to(self):

        oe = Fake()
        oe.uij_erase = Append()
        e = Fake()
        e.object = Return( values = [ oe ] )
        self.i.apply_to( entity = e )

        self.assertEqual( e.object._calls, [ ( (), {} ) ] )
        self.assertEqual( oe.uij_erase._calls, [ ( (), {} ) ] )


class TestChainSample(unittest.TestCase):

    def setUp(self):

        self.sample = chisel.ChainSample( chain = object(), mmt = Fake() )
        self.rsam_VanDerWaalsRadius = chisel.rsam.VanDerWaalsRadius
        self.rsam_AccessibleSurfaceArea = chisel.rsam.AccessibleSurfaceArea


    def tearDown(self):

        chisel.rsam.VanDerWaalsRadius = self.rsam_VanDerWaalsRadius
        chisel.rsam.AccessibleSurfaceArea = self.rsam_AccessibleSurfaceArea


    def test_atom_radii(self):

        self.sample.mmt.mainchain_atom_names = [ object(), object() ]
        self.sample.mmt.mainchain_atom_radii = [ object(), object() ]
        self.sample.mmt.three_letter_codes = [ object(), object() ]
        self.sample.mmt.sidechain_atom_radii_dicts = [ object(), object() ]
        self.sample.mmt.unknown_atom_radius = object()
        vdw = object()
        chisel.rsam.VanDerWaalsRadius = Return( values = [ vdw ] )
        self.assertEqual( self.sample.atom_radii(), vdw )
        self.assertEqual(
            chisel.rsam.VanDerWaalsRadius._calls,
            [
                (
                    (),
                    {
                        "root": self.sample.chain,
                        "radius_for_mainchain_atom": dict(
                            zip(
                                self.sample.mmt.mainchain_atom_names,
                                self.sample.mmt.mainchain_atom_radii,
                                )
                            ),
                        "sidechain_data_for": dict(
                            zip(
                                self.sample.mmt.three_letter_codes,
                                self.sample.mmt.sidechain_atom_radii_dicts,
                                )
                            ),
                        "unknown_radius": self.sample.mmt.unknown_atom_radius,
                        }
                    ),
                ]
            )
        self.assertEqual( self.sample.atom_radii(), vdw )
        self.assertTrue( ( "radii", ) in self.sample.store )


    def test_accessible_surface_area(self):

        ( asa, vdw, precision, radius ) = ( object(), object(), object(), object() )
        self.sample.atom_radii = Return( values = [ vdw ] )
        chisel.rsam.AccessibleSurfaceArea = Return( values = [ asa ] )
        self.assertEqual(
            self.sample.accessible_surface_area( radius = radius, precision = precision ),
            asa
            )
        self.assertEqual( self.sample.atom_radii._calls, [ ( (), {} ) ] )
        self.assertEqual(
            chisel.rsam.AccessibleSurfaceArea._calls,
            [ ( (), { "root": self.sample.chain, "vdw_radii": vdw } ) ]
            )
        self.assertEqual(
            self.sample.accessible_surface_area( radius = radius, precision = precision ),
            asa
            )
        self.assertTrue( ( "asa", radius, precision ) in self.sample.store )



class TestAlignedChainSample(unittest.TestCase):

    def setUp(self):

        self.rsam_rg_difference = chisel.rsam.chain_residue_groups_difference
        #self.rsam_map_residues_to_sequence = chisel.rsam.map_residues_to_sequence
        self.rsam_match_chain_to_alignment = chisel.rsam.match_chain_to_alignment
        self.residue_substitution_similarity = chisel.residue_substitution.SequenceSimilarity
        self.iotbx_bioinformatics_alignment = iotbx.bioinformatics.alignment



    def tearDown(self):

        chisel.rsam.chain_residue_groups_difference = self.rsam_rg_difference
        #chisel.rsam.map_residues_to_sequence = self.rsam_map_residues_to_sequence
        chisel.rsam.match_chain_to_alignment = self.rsam_match_chain_to_alignment
        chisel.residue_substitution.SequenceSimilarity = self.residue_substitution_similarity
        iotbx.bioinformatics.alignment = self.iotbx_bioinformatics_alignment


    def create(self):

        aligned = [ object(), object(), None ]
        alignment = Fake()
        alignment.length = Return( values = [ len( aligned ) ] )
        alignment.alignments = [ object(), object() ]
        alignment.gap = object()
        chain = object()
        ( mtype, olf ) = ( Fake(), object() )
        mtype.one_letter_for = Return( values = [ olf ] )
        mtype.consecutivity = object()
        rsam.match_chain_to_alignment = Return( values = [ aligned ] )
        ( diff, minl, minf ) = ( object(), object(), object() )
        rsam.chain_residue_groups_difference = Return( values = [ diff ] )
        sample = chisel.AlignedChainSample(
            chain = chain,
            alignment = alignment,
            index = 1,
            mmt = mtype,
            min_hssp_length = minl,
            min_matching_fraction = minf,
            )
        self.assertEqual(
            rsam.match_chain_to_alignment._calls,
            [
                (
                    (),
                    {
                        "chain": chain,
                        "mtype": mtype,
                        "sequence": alignment.alignments[ 1 ],
                        "gap": alignment.gap,
                        "min_hssp_length": minl,
                        "min_matching_fraction": minf,
                        }
                    ),
                ]
            )
        self.assertEqual(
            rsam.chain_residue_groups_difference._calls,
            [ ( (), { "chain": chain, "residue_groups": aligned } ) ]
            )
        self.assertEqual( sample.aligned, aligned )
        self.assertEqual( sample.index, 1 )
        self.assertEqual( sample.alignment, alignment )
        self.assertEqual( sample.unaligned, diff )

        return sample


    def test_sequence_similarity(self):

        sample = self.create()
        ( matrix, unknown ) = ( object(), object() )
        sample.mmt.similarity_matrix_suite = Fake()
        sample.mmt.similarity_matrix_suite.names = Return(
            values = [ [ object(), matrix ] ]
            )
        mobj = Fake()
        mobj.normalization = Return( values = [ ( 2.0, 1.0 ) ] )
        sample.mmt.similarity_matrix_suite.matrix_for = Return( values = [ mobj ] )
        ss = Fake()
        ss.values = [ 1, 2, 3, 4 ]
        chisel.residue_substitution.SequenceSimilarity = Return( values = [ ss ] )
        res = [ ( v - 1.0 ) / 2.0 for v in ss.values ]
        self.assertEqual(
            sample.sequence_similarity( matrix = matrix, unknown = unknown ),
            res
            )
        self.assertEqual(
            sample.mmt.similarity_matrix_suite.names._calls,
            [ ( (), {} ) ]
            )
        self.assertEqual(
            sample.mmt.similarity_matrix_suite.matrix_for._calls,
            [ ( (), { "name": matrix } ) ]
            )
        self.assertEqual(
            chisel.residue_substitution.SequenceSimilarity._calls,
            [ ( (), { "alignment": sample.alignment, "matrix": mobj, "unknown": unknown } ) ]
            )
        self.assertEqual( mobj.normalization._calls, [ ( (), {} ) ] )
        self.assertTrue( ( "similarity", matrix, unknown ) in sample.store )
        self.assertEqual(
            sample.sequence_similarity( matrix = matrix, unknown = unknown ),
            res
            )


    def test_sequences(self):

        sample = self.create()
        self.assertEqual( sample.target_sequence(), sample.alignment.alignments[0] )
        self.assertEqual( sample.model_sequence(), sample.alignment.alignments[ sample.index ] )
        ali = object()
        iotbx.bioinformatics.alignment = Return( values = [ ali ] )
        self.assertEqual( sample.pairwise_alignment(), ali )
        self.assertEqual(
            iotbx.bioinformatics.alignment._calls,
            [
                (
                    (),
                    {
                        "names": [ "TARGET", "MODEL" ],
                        "alignments": [ sample.target_sequence(), sample.model_sequence() ],
                        }
                    )
                ]
            )


class TestLinearAveragedSequenceSimilarity(IterableCompareTestCase):

    def test_scores_for(self):

        calc = chisel.LinearAveragedSequenceSimilarity(
            matrix = "blosum62",
            unknown = 0,
            window = 1,
            weights = flex.double( [ 1, 2, 1 ] )
            )

        sample = Fake()
        ss = [ 0, 1, 2, 3, 4, 5, 6 ]
        sample.sequence_similarity = Return( values = [ ss ] )
        sample.mmt = Fake()
        matrix = Fake()
        matrix.gap_penalty = -1
        sample.mmt.similarity_matrix_suite = Fake()
        sample.mmt.similarity_matrix_suite.matrix_for = Return( values = [ matrix ] )

        self.assertEqual(
            calc.scores_for( sample = sample ),
            [ 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 4.0 ]
            )
        self.assertEqual(
            sample.sequence_similarity._calls,
            [ ( (), { "matrix": "blosum62", "unknown": 0 } ) ]
            )


class TestSpaceAveragedSequenceSimilarity(IterableCompareTestCase):

    def setUp(self):

        self.calc = chisel.SpaceAveragedSequenceSimilarity(
            matrix = "blosum62",
            unknown = 0,
            maxdist = 10.0
            )
        import libtbx
        import os.path
        pdb_file = os.path.join(
            libtbx.env.under_dist( "phaser", "tutorial" ),
            "1ahq_trunc.pdb"
            )
        root = iotbx.pdb.input( pdb_file ).construct_hierarchy()
        sel = root.atom_selection_cache().selection( "not name H" )
        self.root = root.select( sel, True )
        self.pos = [
            None,
            ( 3.113, 28.053, -17.33925 ),
            ( 4.50875, 26.59075, -14.82475 ),
            None,
            ( 3.776, 26.34925, -11.6415 ),
            None,
            ]


    def test_calculate_weights(self):

        self.assertIterablesAlmostEqual(
            self.calc.calculate_weights( distances = flex.double( [ 0, 1, 2, 3 ] ) ),
            [ 1.0, 0.81873075, 0.67032005, 0.54881164 ],
            8
            )


    def test_calculate_mean_positions(self):

        sample = Fake()
        rgs = self.root.models()[0].chains()[0].residue_groups()
        notacc = rgs[3]

        for a in notacc.atom_groups()[0].atoms()[:4]:
            a.parent().remove_atom( a )

        sample.aligned = [ None, rgs[0], rgs[1], None, rgs[2], rgs[3] ]
        sample.mmt = mmt.PROTEIN

        for ( o, e ) in zip( self.calc.calculate_mean_positions( sample = sample ), self.pos ):
            if not e:
                self.assert_( o is None )

            else:
                self.assertIterablesAlmostEqual( o, e, 8 )


    def test_calculate_ss_map(self):

        ( points, scores ) = self.calc.calculate_ss_map(
            positions = self.pos,
            scores = [ 1, 2, 3, 4, 5, 6 ]
            )
        self.assertCoordinatesAlmostEqual(
            points,
            [ self.pos[1], self.pos[2], self.pos[4] ],
            8
            )
        self.assertIterablesAlmostEqual(
            scores,
            [ 2, 3, 5 ],
            8
            )


    def test_calculate_average(self):

        points = flex.vec3_double( [ self.pos[1], self.pos[2], self.pos[4] ] )
        scores = flex.double( [ 2, 3, 5 ] )
        self.assertAlmostEqual(
            self.calc.calculate_average( position = self.pos[1], points = points, scores = scores ),
            2.78339828,
            8
            )


    def test_scores_for(self):

        sample = Fake()
        ss = [ 1, 2, 3, 4, 5, 6 ]
        sample.sequence_similarity = Return( values = [ ss ] )
        self.calc.calculate_mean_positions = Return( values = [ self.pos ] )
        points = [ self.pos[1], self.pos[2], self.pos[4] ]
        values = [ 2, 3, 5 ]
        self.calc.calculate_ss_map = Return( values = [ ( points, values ) ] )
        aver = [ object(), object(), object() ]
        self.calc.calculate_average = Return( values = aver )
        self.assertEqual(
            self.calc.scores_for( sample = sample ),
            [ 1, aver[0], aver[1], 4, aver[2], 6 ]
            )
        self.assertEqual(
            sample.sequence_similarity._calls,
            [ ( (), { "matrix": "blosum62", "unknown": 0 } ) ]
            )
        self.assertEqual(
            self.calc.calculate_mean_positions._calls,
            [ ( (), { "sample": sample } ) ]
            )
        self.assertEqual(
            self.calc.calculate_ss_map._calls,
            [ ( (), { "positions": self.pos, "scores": ss } ) ]
            )
        self.assertEqual(
            self.calc.calculate_average._calls,
            [ ( (), { "position": p, "points": points, "scores": values } )
                for p in points ]
            )


    def ntest_scores_for(self):

        calc = chisel.LinearAveragedSequenceSimilarity(
            matrix = "blosum62",
            unknown = 0,
            window = 1,
            weights = flex.double( [ 1, 2, 1 ] )
            )

        sample = Fake()
        ss = [ 0, 1, 2, 3, 4, 5, 6 ]
        sample.sequence_similarity = Return( values = [ ss ] )
        sample.mmt = Fake()
        matrix = Fake()
        matrix.gap_penalty = -1
        sample.mmt.similarity_matrix_suite = Fake()
        sample.mmt.similarity_matrix_suite.matrix_for = Return( values = [ matrix ] )

        self.assertEqual(
            calc.scores_for( sample = sample ),
            [ 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 4.0 ]
            )
        self.assertEqual(
            sample.sequence_similarity._calls,
            [ ( (), { "matrix": "blosum62", "unknown": 0 } ) ]
            )


class TestMDAGap(unittest.TestCase):

    def setUp(self):

        self.algorithm = chisel.MDAGap()


    def test_calculate(self):

        target = "A-CE"
        ( sample, ali ) = ( Fake(), Fake() )
        ali.gap = "-"
        sample.alignment = ali
        sample.target_sequence = Return( values = [ target ] )

        self.assertEqual(
            self.algorithm.calculate( sample = sample ),
            [ False, True, False, False ]
            )


class TestMDASimilarity(unittest.TestCase):

    def setUp(self):

        self.ssc = Fake()
        self.algorithm = chisel.MDASimilarity(
            threshold = 1.5,
            ss_calc = self.ssc
            )


    def test_calculate(self):

        self.ssc.scores_for = Return( values = [ [ 1.4, 1.5, 1.6, 1.7, 1.2 ] ] )
        cs = object()
        self.assertEqual(
            self.algorithm.calculate( sample = cs ),
            [ True, False, False, False, True ]
            )
        self.assertEqual(
            self.algorithm.ss_calc.scores_for._calls,
            [ ( (), { "sample": cs } ) ]
            )


class TestMDAThresholdAdjustSimilarity(unittest.TestCase):

    def setUp(self):

        self.ssc = Fake()
        self.algorithm = chisel.MDAThresholdAdjustSimilarity(
            ss_calc = self.ssc,
            offset = 0
            )


    def test_calculate(self):

        cs = Fake()
        cs.target_sequence = Return( values = [ "ABCD-FGH-IJ--" ] * 2 )
        cs.model_sequence = Return( values = [ "ABCDEFGHIJKLM" ] )
        cs.alignment = Fake()
        cs.alignment.gap = "-"
        scores = [ 0.9, 1.4, 1.5, 1.1, 1.2, 1.4, 2.1, 1.6, 0.5, 1.1, 1.7, 1.7, 1.2 ]
        self.ssc.scores_for = Return( values = [ scores ] )
        self.algorithm.get_threshold = Return( values = [ 1.2 ] )
        self.assertEqual(
            self.algorithm.calculate( sample = cs ),
            [ True, False, False, True, False, False, False, False, True,
                True, False, False, False ]
            )
        self.assertEqual(
            self.algorithm.ss_calc.scores_for._calls,
            [ ( (), { "sample": cs } ) ]
            )
        self.assertEqual(
            self.algorithm.get_threshold._calls,
            [ ( (), { "scores": scores, "target": 4 } ) ]
            )


    def test_get_threshold(self):

        self.assertAlmostEqual(
            self.algorithm.get_threshold( scores = [ 2, 2, 0, 0, 1, 1, 1 ], target = 3 ),
            1,
            3
            )
        self.assertAlmostEqual(
            self.algorithm.get_threshold( scores = [ 2, 2, 0, 1, 1, 1 ], target = 3 ),
            2,
            3
            )


class TestMDARemoveLong(unittest.TestCase):

    def setUp(self):

        self.algorithm = chisel.MDARemoveLong( min_length = 3 )


    def test_split_alignment(self):

        sample = Fake()
        sample.alignment = Fake()
        sample.alignment.gap = "-"
        sample.target_sequence = Return( values = [ "ABC--DE-FGHIJK---" ] )
        self.assertEqual(
            list( self.algorithm.split_alignment( sample = sample ) ),
            [ "ABC", "--", "DE", "-", "FGHIJK", "---" ]
            )


    def test_calculate(self):

        sample = Fake()
        sample.alignment = Fake()
        sample.alignment.gap = "-"
        self.algorithm.split_alignment = Return(
            values = [ [ "A", "AB", "ABC", "ABCD", "-", "--", "---", "----" ] ]
            )
        self.assertEqual(
            self.algorithm.calculate( sample = sample ),
            [ False, False, False, False, False, False, False, False, False,
                False, False, False, False, True, True, True, True, True,
                True, True ]
            )


#Real data for algorithm testing
"""
my_search = "VVCEVDPELKETLRKFRFR---KETNNAAIIMKVD--KDRQMVVLEDELQ-NISPEELKL"
my_model =  "-GIAVSDDCVQKFNELKLGHQH-----RYVTFKMNASN--TEVVVEHVGGPNATYEDFKS"

my_alignment = bioinformatics.alignment(
    alignments = [ my_search, my_model ],
    names = [ "Foo", "Bar" ]
    )

my_root = iotbx.pdb.input( PDB_1AHQ_TRUNC ).construct_hierarchy()
my_chain = my_root.models()[0].chains()[0]

for atom in my_chain.atoms():
    if atom.name.strip() == "H":
        atom.parent().remove_atom( atom )

my_chain_sample = chisel.ChainSample(
    chain = my_chain,
    alignment = my_alignment,
    sequence_index = 1,
    macromolecule_specifics = rsam.MacromoleculeSpecifics.Protein()
    )
"""

# Mainchain
class TestMainchainDelete(unittest.TestCase):

    def setUp(self):

        self.tool = chisel.MainchainDelete(
            deletion = [],
            polishing = [],
            applicable = None
            )
        self.chisel_rg = chisel.ResidueGroup


    def tearDown(self):

        chisel.ResidueGroup = self.chisel_rg


    def test_generate_empty(self):

        cs = Fake()
        rgs = [ object(), object(), object(), object(), None, None, None, None, ]
        cs.residue_groups = Return( values = [ rgs ] )
        vr = object()
        self.tool.design( chain_sample = cs, versioned_root = vr )


    def test_generate_deletion(self):

        cs = Fake()
        rgs = [ object(), object(), object(), object(), None, None, None, None, ]
        cs.residue_groups = Return( values = [ rgs ] )
        deletion = [ Fake(), Fake() ]
        deletion[0].calculate = Return(
            values = [ [ True, True, False, False, True, True, False, False, ] ]
            )
        deletion[1].calculate = Return(
            values = [ [ True, False, True, False, True, False, True, False, ] ]
            )
        vr = Fake()
        vr.delete = Append()
        self.tool._deletion = deletion
        ents = [ object(), object(), object() ]
        chisel.ResidueGroup = Return( values = ents )
        self.tool.design( chain_sample = cs, versioned_root = vr )

        for alg in deletion:
            self.assertEqual(
                alg.calculate._calls,
                [ ( (), { "chain_sample": cs } ) ]
                )
        self.assertEqual( cs.residue_groups._calls, [ ( (), {} ) ] )
        self.assertEqual(
            chisel.ResidueGroup._calls,
            [ ( (), { "obj": o } ) for o in rgs[:3] ]
            )
        self.assertEqual(
            sorted( vr.delete._calls ),
            sorted( [ ( (), { "entity": o } ) for o in ents ] )
            )

    def test_generate_polishing(self):

        cs = Fake()
        cs.residue_groups = Return( values = [ [ object() ] ] )
        polishing = [ Fake(), Fake() ]
        ( res_del, res_rest, res_rest_ne, res_del2 ) = (
            object(), object(), object(), object()
            )
        polishing[0].calculate = Return(
            values = [ ( [ res_del, res_rest ], [ res_rest_ne ] ) ]
            )
        polishing[1].calculate = Return(
            values = [ ( [ res_del2, res_rest_ne ], [ res_rest ] ) ]
            )
        vr = Fake()
        vr.delete = Append()
        ents = [ object(), object(), object() ]
        chisel.ResidueGroup = Return( values = ents )
        self.tool._polishing = polishing
        self.tool.design( chain_sample = cs, versioned_root = vr )

        mb_set = set( [ res_del, res_del2, res_rest_ne ] )
        self.assertEqual(
            polishing[0].calculate._calls,
            [ ( (), { "chain_sample": cs, "moribund": mb_set } ) ]
            )
        self.assertEqual(
            polishing[1].calculate._calls,
            [ ( (), { "chain_sample": cs, "moribund": mb_set } ) ]
            )
        self.assertEqual(
            chisel.ResidueGroup._calls,
            [ ( (), { "obj": o } ) for o in mb_set ]
            )
        self.assertEqual(
            sorted( vr.delete._calls ),
            sorted( [ ( (), { "entity": o } ) for o in ents ] )
            )


    def test_generate_del_and_polish(self):

        cs = Fake()
        rgs = [ object(), object(), object(), object(), object(), object(), object(), None ]
        cs.residue_groups = Return( values = [ rgs ] )
        deletion = [ Fake(), Fake() ]
        deletion[0].calculate = Return(
            values = [ [ False, False, False, False, True, False, False, False, ] ]
            )
        deletion[1].calculate = Return(
            values = [ [ False, False, False, False, False, True, False, False, ] ]
            )
        polishing = [ Fake(), Fake() ]
        polishing[0].calculate = Return(
            values = [ ( [ rgs[0], rgs[2] ], [ rgs[4] ] ) ]
            )
        polishing[1].calculate = Return(
            values = [ ( [ rgs[-2] ], [ rgs[2] ] ) ]
            )
        vr = Fake()
        vr.delete = Append()
        ents = [ object(), object(), object() ]
        chisel.ResidueGroup = Return( values = ents )
        self.tool._deletion = deletion
        self.tool._polishing = polishing
        self.tool.design( chain_sample = cs, versioned_root = vr )

        for alg in deletion:
            self.assertEqual(
                alg.calculate._calls,
                [ ( (), { "chain_sample": cs } ) ]
                )

        mb_set = set( [ rgs[0], rgs[-2], rgs[-3] ] )
        self.assertEqual(
            polishing[0].calculate._calls,
            [ ( (), { "chain_sample": cs, "moribund": mb_set } ) ]
            )
        self.assertEqual(
            polishing[1].calculate._calls,
            [ ( (), { "chain_sample": cs, "moribund": mb_set } ) ]
            )
        self.assertEqual( cs.residue_groups._calls, [ ( (), {} ) ] )
        self.assertEqual(
            sorted( chisel.ResidueGroup._calls ),
            sorted( [ ( (), { "obj": o } ) for o in mb_set ] )
            )
        self.assertEqual(
            sorted( vr.delete._calls ),
            sorted( [ ( (), { "entity": o } ) for o in ents ] )
            )


class TestMPARemoveShort(unittest.TestCase):

    def setUp(self):

        self.min_length = 3
        self.algorithm = chisel.MPARemoveShort( min_length = self.min_length )
        self.chisel_rsam_splitter = chisel.rsam.Splitter


    def tearDown(self):

        chisel.rsam.Splitter = self.chisel_rsam_splitter


    def test_calculate(self):

        ( cs, mmt, cons, spl ) = ( Fake(), Fake(), object(), Fake() )
        cs.mmt = Return( values = [ mmt ] )
        mmt.get_consecutivity_function = Return( values = [ cons ] )
        chisel.rsam.Splitter = Return( values = [ spl ] )
        rgs = [ object(), object(), object(), object() ]
        mb_rg = [ rgs[2] ]
        cs.residue_groups = Return( values = [ rgs ] )
        split_rgs = [
            ( object(), object(), object() ),
            ( object(), object() ),
            ( object(), object(), object(), object() ),
            ( object(), ),
            ]
        spl.split = Return( values = [ split_rgs ] )

        self.assertEqual(
            self.algorithm.calculate( chain_sample = cs, moribund = mb_rg ),
            ( list( split_rgs[1] + split_rgs[3] ), [] )
            )
        self.assertEqual( cs.residue_groups._calls, [ ( (), {} ) ] )
        self.assertEqual( cs.mmt._calls, [ ( (), {} ) ] )
        self.assertEqual( mmt.get_consecutivity_function._calls, [ ( (), {} ) ] )
        self.assertEqual(
            chisel.rsam.Splitter._calls,
            [ ( (), { "consecutivity": cons } ) ]
            )
        self.assertEqual(
            spl.split._calls,
            [ ( (), { "sequence": rgs[0:2] + [ rgs[3] ] } ) ]
            )


class TestMPAKeepRegular(unittest.TestCase):

    def setUp(self):

        self.max_length = 1
        self.algorithm = chisel.MPAKeepRegular( max_length = self.max_length )
        self.chisel_rsam_splitter = chisel.rsam.Splitter
        self.chisel_rsam_consec = chisel.rsam.FunctionDefinedConsecutivity


    def tearDown(self):

        chisel.rsam.Splitter = self.chisel_rsam_splitter
        chisel.rsam.FunctionDefinedConsecutivity = self.chisel_rsam_consec


    def test_get_secondary_structure(self):

        mmt = Fake()
        analyser = Return( values = [ object() ] )
        mmt.get_secondary_structure_analyser = Return( values = [ analyser ] )
        rgs = [ object() ]
        self.assertEqual(
            self.algorithm.get_secondary_structure( mmt = mmt, residue_groups = rgs ),
            analyser._orig[0]
            )
        self.assertEqual( mmt.get_secondary_structure_analyser._calls, [ ( (), {} ) ] )
        self.assertEqual( analyser._calls, [ ( (), { "residue_groups": rgs } ) ] )
        analyser = Raise( exception = ValueError, message = object() )
        mmt.get_secondary_structure_analyser = Return( values = [ analyser ] )
        self.assertRaises(
            chisel.Sorry,
            self.algorithm.get_secondary_structure,
            mmt = mmt,
            residue_groups = rgs
            )
        mmt.get_secondary_structure_analyser = Raise(
            exception = rsam.UndefinedException,
            message = None
            )
        self.assertEqual(
            self.algorithm.get_secondary_structure( mmt = mmt, residue_groups = rgs ),
            [ None ] * len( rgs )
            )


    def test_calculate(self):

        ( cs, mmt, cons, spl, fdc ) = ( Fake(), Fake(), object(), Fake(), object() )
        cs.mmt = Return( values = [ mmt ] )
        mmt.get_consecutivity_function = Return( values = [ cons ] )
        chisel.rsam.FunctionDefinedConsecutivity = Return( values = [ fdc ] )
        chisel.rsam.Splitter = Return( values = [ spl ] )
        rgs = [ object(), object(), None, None, object(), None, object(), object(), object(), object() ]
        helix = object()
        sec_str = [ helix, helix, helix, helix, helix, helix, helix ]
        mb_list = [ rgs[1], rgs[6], rgs[7], rgs[9] ]
        mb_rg = set( mb_list )
        cs.residue_groups = Return( values = [ rgs ] )
        self.algorithm.get_secondary_structure = Return( values = [ sec_str ] )
        split_rgs = [
            ( ( rgs[1], helix ), ),
            ( ( rgs[6], helix ), ( rgs[7], helix ) ),
            ( ( rgs[9], helix ), )
            ]
        spl.split = Return( values = [ split_rgs ] )

        self.assertEqual(
            self.algorithm.calculate( chain_sample = cs, moribund = mb_rg ),
            ( [], [ rgs[1] ] )
            )
        self.assertEqual( cs.residue_groups._calls, [ ( (), {} ) ] )
        self.assertEqual( cs.mmt._calls, [ ( (), {} ) ] )
        self.assertEqual( mmt.get_consecutivity_function._calls, [ ( (), {} ) ] )
        self.assertEqual( len( chisel.rsam.FunctionDefinedConsecutivity._calls ), 1 )
        self.assertEqual(
            chisel.rsam.Splitter._calls,
            [ ( (), { "consecutivity": fdc } ) ]
            )
        self.assertEqual(
            spl.split._calls,
            [ ( (), { "sequence": [ ( r, helix ) for r in mb_list] } ) ]
            )


# Sidechain
class TestSidechainPrune(unittest.TestCase):

    def setUp(self):

        self.tool = chisel.SidechainPrune( pruning = [], applicable = None )
        self.chisel_atom = chisel.Atom


    def tearDown(self):

        chisel.Atom = self.chisel_atom


    def test_calculate_pruning_levels(self):

        pruning = [ Fake(), Fake() ]
        pruning[0].calculate = Return( values = [ [ 1, 2, 3, 4, 5, 7 ] ] )
        pruning[1].calculate = Return( values = [ [ 7, 5, 2, 5, 2, 1 ] ] )
        self.tool._pruning = pruning
        cs = object()
        self.assertEqual(
            self.tool.calculate_pruning_levels( chain_sample = cs ),
            [ 1, 2, 2, 4, 2, 1 ]
            )

        for alg in self.tool._pruning:
            self.assertEqual(
                alg.calculate._calls,
                [ ( (), { "chain_sample": cs } ) ]
                )


    def test_generate(self):

        ( cs, mmt, top, tra ) = ( Fake(), Fake(), Fake(), Fake() )
        prune_levels = [ 2, 7, 2 ]
        self.tool.calculate_pruning_levels = Return( values = [ prune_levels ] )
        rgs = [ Fake(), Fake(), None, ]
        cs.residue_groups = Return( values = [ rgs ] )
        cs.mmt = Return( values = [ mmt ] )
        mmt.get_topology_provider = Return( values = [ top ] )
        mmt.get_translation = Return( values = [ tra ] )

        ags = [ [ Fake() ], [ Fake(), Fake() ] ]
        atoms = [ [ Fake(), Fake(), Fake() ], [ Fake(), Fake() ], [ Fake() ] ]
        names = [ [ object(), object(), object() ], [ object(), object() ], [ object() ] ]

        for ( ats, nas ) in zip( atoms, names ):
            for ( a, n ) in zip( ats, nas ):
                a.name = n

        rgs[0].atom_groups = Return( values = [ ags[0] ] )
        rgs[1].atom_groups = Return( values = [ ags[1] ] )
        resnames = [ object(), object(), object() ]
        ags[0][0].resname = resnames[0]
        ags[0][0].atoms = Return( values = [ atoms[0] ] )
        ags[1][0].resname = resnames[1]
        ags[1][0].atoms = Return( values = [ atoms[1] ] )
        ags[1][1].resname = resnames[2]
        ags[1][1].atoms = Return( values = [ atoms[2] ] )
        rescodes = [ object(), object(), object() ]
        tra.one_letter_code_for = Return( values = rescodes )
        topols = [ Fake(), Fake(), Fake() ]
        top.residue_info_for = Return( values = topols )
        mdfas = [ Fake(), Fake(), Fake() ]
        topols[0].mainchain_distance_for_atoms = Return( values = [ mdfas[0] ] )
        topols[1].mainchain_distance_for_atoms = Return( values = [ mdfas[1] ] )
        topols[2].mainchain_distance_for_atoms = Return( values = [ mdfas[2] ] )
        mdfas[0].get =  Return( values = [ 1, 2, 3 ] )
        mdfas[1].get =  Return( values = [ 8, 6 ] )
        mdfas[2].get =  Return( values = [ 3 ] )
        vr = Fake()
        vr.delete = Append()
        ents = [ object(), object() ]
        chisel.Atom = Return( values = ents )
        self.tool.design( chain_sample = cs, versioned_root = vr )
        self.assertEqual( cs.mmt._calls, [ ( (), {} ) ] )
        self.assertEqual( mmt.get_topology_provider._calls, [ ( (), {} ) ] )
        self.assertEqual( mmt.get_translation._calls, [ ( (), {} ) ] )
        self.assertEqual( cs.residue_groups._calls, [ ( (), {} ) ] )
        self.assertEqual(
            self.tool.calculate_pruning_levels._calls,
            [ ( (), { "chain_sample": cs } ) ]
            )
        self.assertEqual( rgs[0].atom_groups._calls, [ ( (), {} ) ] )
        self.assertEqual( rgs[1].atom_groups._calls, [ ( (), {} ) ] )
        self.assertEqual(
            tra.one_letter_code_for._calls,
            [ ( (), { "three_letter_code": r } ) for r in resnames ]
            )
        self.assertEqual(
            top.residue_info_for._calls,
            [ ( (), { "name": c } ) for c in rescodes ]
            )

        for t in topols:
            self.assertEqual( t.mainchain_distance_for_atoms._calls, [ ( (), {} ) ] )

        pl_ags = [ prune_levels[0], prune_levels[1], prune_levels[1] ]
        for ( bdf, ns, pl ) in zip( mdfas, names, pl_ags ):
            self.assertEqual( bdf.get._calls, [ ( (n, pl + 1 ), {} ) for n in ns ] )

        self.assertEqual(
            chisel.Atom._calls,
            [ ( (), { "obj": a } ) for a in [ atoms[0][2], atoms[1][0] ] ]
            )

        self.assertEqual(
            vr.delete._calls,
            [ ( (), { "entity": e } ) for e in ents ]
            )


class TestSPASchwarzenbacher(unittest.TestCase):

    def setUp(self):

        self.level = 2
        self.algorithm = chisel.SPASchwarzenbacher( level = self.level )


    def test_pruning_level_for(self):

        t = object()
        self.assertEqual(
            self.algorithm.pruning_level_for( target = t, is_identical = True ),
            self.algorithm.residue_max_bond_distance( rescode = t )
            )
        self.assertEqual(
            self.algorithm.pruning_level_for( target = t, is_identical = False ),
            self.level
            )


    def test_calculate(self):

        target = "ABCD"
        model = "AEFD"
        pls = [ object(), object(), object(), object() ]
        cs = Fake()
        cs.target_sequence = Return( values = [ target ] )
        cs.model_sequence = Return( values = [ model ] )
        self.algorithm.pruning_level_for = Return( values = pls )
        self.assertEqual( self.algorithm.calculate( chain_sample = cs ), pls )
        self.assertEqual( cs.target_sequence._calls, [ ( (), {} ) ] )
        self.assertEqual( cs.model_sequence._calls, [ ( (), {} ) ] )
        self.assertEqual(
            self.algorithm.pruning_level_for._calls,
            [ ( (), { "target": t, "is_identical": m == t } )
                for ( t, m ) in zip( target, model ) ]
            )


class TestSPASimilarity(unittest.TestCase):

    def setUp(self):

        ( self.upper, self.lower, self.level, self.ssc ) = ( 1.5, -1, 2, Fake() )
        self.algorithm = chisel.SPASimilarity(
            ssc = self.ssc,
            upper = self.upper,
            lower = self.lower,
            level = self.level
            )


    def test_pruning_level_for(self):

        t = object()
        self.assertEqual(
            self.algorithm.pruning_level_for( target = t, similarity = self.upper ),
            self.algorithm.residue_max_bond_distance( rescode = t )
            )
        self.assertEqual(
            self.algorithm.pruning_level_for( target = t, similarity = self.lower ),
            self.level
            )
        self.assertEqual(
            self.algorithm.pruning_level_for( target = t, similarity = self.lower - 0.01 ),
            self.algorithm.residue_min_bond_distance( rescode = t )
            )


    def test_calculate(self):

        target = "ABCD"
        cs = Fake()
        cs.target_sequence = Return( values = [ target ] )
        scores = [ object(), object(), object() ]
        pls = [ object(), object(), object() ]
        self.algorithm.ssc_scores_for = Return( values = [ scores ] )
        self.algorithm.pruning_level_for = Return( values = pls )

        self.assertEqual( self.algorithm.calculate( chain_sample = cs ), pls )
        self.assertEqual(
            self.algorithm.ssc_scores_for._calls,
            [ ( (), { "chain_sample": cs } ) ]
            )
        self.assertEqual( cs.target_sequence._calls, [ ( (), {} ) ] )
        self.assertEqual(
            self.algorithm.pruning_level_for._calls,
            [ ( (), { "target": t, "similarity": s } )
                for ( t, s ) in zip( target, scores ) ]
            )

# Rename
class TestResidueRename(unittest.TestCase):

    def setUp(self):

        self.tool = chisel.ResidueRename( completion = [], applicable = None )
        self.chisel_atom = chisel.Atom
        self.chisel_ag = chisel.AtomGroup


    def tearDown(self):

        chisel.Atom = self.chisel_atom
        chisel.AtomGroup = self.chisel_ag


    def test_get_atom_generation_data(self):

        needed = [ object(), object(), object(), object() ]
        coords_for = {
            object(): object(),
            object(): object(),
            }
        target = object()
        completion = [ Fake(), Fake() ]
        completion[0].provides_from = Return(
            values = [ [], [ needed[0], object() ], [] ]
            )
        completion[1].provides_from = Return(
            values = [ [ needed[1], needed[2], object() ], [] ]
            )
        gens = [ { "name": n, "xyz": None } for n in needed ]
        atoms = [ Fake(), Fake(), Fake() ]
        atoms[0].name = gens[0][ "name" ]
        atoms[1].name = gens[1][ "name" ]
        atoms[2].name = gens[2][ "name" ]
        completion[0].generate = Return( values = gens[:1] )
        completion[0].data_to_atom = Return( values = [ atoms[0] ] )
        completion[1].generate = Return( values = gens[1:3] )
        completion[1].data_to_atom = Return( values = [ atoms[1], atoms[2] ] )
        self.tool._completion = completion
        self.assertEqual(
            self.tool.get_atom_generation_data(
                target = target,
                needed = needed,
                coords_for = coords_for
                ),
            ( [ gens[1], gens[2], gens[0] ], dict( [ ( a.name, a ) for a in atoms ] ) )
            )
        available = coords_for.copy()
        available[ needed[0] ] = atoms[0]
        available[ needed[1] ] = atoms[1]
        available[ needed[2] ] = atoms[2]
        self.assertEqual(
            completion[0].provides_from._calls,
            [
                ( (), { "target": target, "available": set( coords_for.keys() ) } ),
                ( (), { "target": target, "available": set( coords_for.keys() + needed[1:3] ) } ),
                ( (), { "target": target, "available": set( coords_for.keys() + needed[0:3] ) } ),
                ]
            )
        self.assertEqual(
            completion[1].provides_from._calls,
            [
                ( (), { "target": target, "available": set( coords_for.keys() ) } ),
                ( (), { "target": target, "available": set( coords_for.keys() + needed[0:3] ) } ),
                ]
            )
        self.assertEqual(
            completion[0].generate._calls,
            [ ( (), { "name": needed[0], "from_atoms": available } ) ]
            )
        self.assertEqual(
            sorted( completion[1].generate._calls ),
            sorted( [
                ( (), { "name": needed[1], "from_atoms": available } ),
                ( (), { "name": needed[2], "from_atoms": available } ),
                ] )
            )
        self.assertEqual(
            completion[0].data_to_atom._calls,
            [ ( (), { "data": gens[0] } ) ]
            )
        self.assertEqual(
            completion[1].data_to_atom._calls,
            [ ( (), { "data": g } ) for g in [ gens[1], gens[2] ] ]
            )


    def test_generate(self):

        vr = object()

        ( cs, ali, mmt, pro, tra, gap ) = (
            Fake(), Fake(), Fake(), object(), object(), object()
            )
        ali.gap = gap
        cs.alignment = Return( values = [ ali ] )
        rgs = [ None, Fake() ]
        cs.residue_groups = Return( values = [ rgs ] )
        seq = [ object(), object() ]
        cs.target_sequence = Return( values = [ seq ] )
        cs.mmt = Return( values = [ mmt ] )
        mmt.get_topology_provider = Return( values = [ pro ] )
        mmt.get_translation = Return( values = [ tra ] )
        ags = [ Fake(), Fake(), Fake() ]
        altlocs = [ object(), object(), object() ]
        rgs[1].atom_groups = Return( values = [ ags ] )

        for ( ag, al ) in zip( ags, altlocs ):
            ag.altloc = al

        self.tool.set_resname = Append()
        ats_for = [ object(), object(), object() ]
        self.tool.rename_atoms = Return( values = ats_for )
        self.tool.complete_residue = Append()
        self.tool.design( chain_sample = cs, versioned_root = vr )
        self.assertEqual( cs.mmt._calls, [ ( (), {} ) ] )
        self.assertEqual( mmt.get_topology_provider._calls, [ ( (), {} ) ] )
        self.assertEqual( mmt.get_translation._calls, [ ( (), {} ) ] )
        self.assertEqual( cs.alignment._calls, [ ( (), {} ) ] )
        self.assertEqual( cs.target_sequence._calls, [ ( (), {} ) ] )
        self.assertEqual( cs.residue_groups._calls, [ ( (), {} ) ] )
        self.assertEqual( rgs[1].atom_groups._calls, [ ( (), {} ) ] )
        self.assertEqual(
            self.tool.set_resname._calls,
            [ (
                (),
                {
                    "atom_group": ag,
                    "target": seq[1],
                    "translation": tra,
                    "gap_char": gap,
                    "versioned_root": vr,
                    }
                )
                for ag in ags ]
            )
        self.assertEqual(
            self.tool.rename_atoms._calls,
            [ (
                (),
                {
                    "atom_group": ag,
                    "target": seq[1],
                    "provider": pro,
                    "translation": tra,
                    "versioned_root": vr,
                    }
                )
                for ag in ags ]
            )
        self.assertEqual(
            self.tool.complete_residue._calls,
            [ (
                (),
                {
                    "atom_group_for": dict( zip( altlocs, ags ) ),
                    "atom_for": dict( zip( altlocs, ats_for ) ),
                    "target": seq[1],
                    "provider": pro,
                    "versioned_root": vr,
                    }
                ) ]
            )


    def test_complete_residue(self):

        altlocs = [ object(), self.tool.BLANK_ALTLOC, object() ]
        ags = [ object(), object(), object() ]
        anames = [ object(), object(), object(), object() ]
        atm_altlocs = [
            { anames[2]: object(), anames[3]: object() },
            { anames[0]: object(), anames[1]: object() },
            { anames[2]: object() },
            ]

        ag_for = dict( zip( altlocs, ags ) )
        atm_for = dict( zip( altlocs, atm_altlocs ) )
        ( pro, top ) = ( Fake(), Fake() )
        ( target, all_names, vr ) = ( object(), object(), object() )
        pro.residue_info_for = Return( values = [ top ] )
        top.all_atoms = Return( values = [ all_names ] )
        self.tool.add_missing_atoms = Append()
        self.tool.complete_residue(
            atom_group_for = ag_for,
            atom_for = atm_for,
            target = target,
            provider = pro,
            versioned_root = vr
            )
        self.assertEqual(
            pro.residue_info_for._calls,
            [ ( (), { "name": target } ) ]
            )
        self.assertEqual( top.all_atoms._calls, [ ( (), {} ) ] )
        non_blank = [ a for a in atm_for if a != self.tool.BLANK_ALTLOC ]
        self.assertEqual(
            self.tool.add_missing_atoms._calls,
            [
                ( (), {
                    "target": target,
                    "atom_group": ag_for.get( self.tool.BLANK_ALTLOC, {} ),
                    "atom_named_as": atm_for.get( self.tool.BLANK_ALTLOC, {} ),
                    "names": all_names,
                    "present_names": set( anames ),
                    "versioned_root": vr,
                    } ),
                ( (), {
                    "target": target,
                    "atom_group": ag_for.get( non_blank[0], {} ),
                    "atom_named_as": atm_for.get( non_blank[0], {} ),
                    "names": all_names,
                    "present_names": set( anames[:4] ),
                    "versioned_root": vr,
                    } ),
                ( (), {
                    "target": target,
                    "atom_group": ag_for.get( non_blank[1], {} ),
                    "atom_named_as": atm_for.get( non_blank[1], {} ),
                    "names": all_names,
                    "present_names": set( anames[:3] ),
                    "versioned_root": vr,
                    } ),
                ]
            )


    def test_add_missing_atoms(self):

        ( target, ag ) = ( object(), object() )
        vr = Fake()
        vr.attach = Append()

        names = [ object(), object(), object(), object() ]
        present = [ names[0], names[1], object() ]
        atom_named_as = {
            present[0]: object(),
            present[2]: object(),
            }
        backup = atom_named_as.copy()
        data = [ object(), object() ]
        gen_for = { names[2]: object() }
        self.tool.get_atom_generation_data = Return( values = [ ( data, gen_for ) ] )
        ent = object()
        chisel.AtomGroup = Return( values = [ ent ] )
        self.tool.add_missing_atoms(
            target = target,
            atom_group = ag,
            atom_named_as = atom_named_as,
            names = names,
            present_names = set( present ),
            versioned_root = vr,
            )
        self.assertEqual(
            self.tool.get_atom_generation_data._calls,
            [ ( (), { "target": target, "needed":  names[2:], "coords_for": atom_named_as } ) ]
            )
        self.assertEqual( chisel.AtomGroup._calls, [ ( (), { "obj": ag } ) ] )
        self.assertEqual(
            vr.attach._calls,
             [ ( (), { "entity": ent, "data": d } ) for d in data ]
             )
        backup[ names[2] ] = gen_for[ names[2] ]
        self.assertEqual( atom_named_as, backup )
        self.assert_( atom_named_as is not backup )


    def test_set_resname(self):

        ( vr, trans, ag ) = ( Fake(), Fake(), Fake() )
        gap = object()
        target = gap
        ag.resname = self.tool.GAP_THREE_LETTER
        self.tool.set_resname(
            atom_group = ag,
            target = target,
            translation = trans,
            gap_char = gap,
            versioned_root = vr
            )

        name = object()
        trans.three_letter_code_for = Return( values = [ name ] )
        target = object()
        ag.resname = name
        self.tool.set_resname(
            atom_group = ag,
            target = target,
            translation = trans,
            gap_char = gap,
            versioned_root = vr
            )
        self.assertEqual(
            trans.three_letter_code_for._calls,
            [ ( (), { "one_letter_code": target } ) ]
            )

        vr.set = Append()
        ent = object()
        chisel.AtomGroup = Return( values = [ ent ] )
        ag.resname = object()
        trans.three_letter_code_for.reset()
        self.tool.set_resname(
            atom_group = ag,
            target = target,
            translation = trans,
            gap_char = gap,
            versioned_root = vr
            )
        self.assertEqual( chisel.AtomGroup._calls, [ ( (), { "obj": ag } ) ] )
        self.assertEqual(
            vr.set._calls,
            [ ( (), { "entity": ent, "property": "resname", "value": name } ) ]
            )


    def test_rename_atoms(self):

        ( ag, target, prov, trans, vr ) = ( Fake(), object(), Fake(), Fake(), Fake() )
        ag.resname = object()
        olc = object()
        trans.one_letter_code_for = Return( values = [ olc ] )
        old_names = [ Fake(), Fake(), Fake() ]
        new_names = [ Fake(), Fake(), old_names[2] ]
        old_names[0].strip = Return( values = [ [ object() ] ] )
        old_names[1].strip = Return( values = [ [ object() ] ] )
        old_names[2].strip = Return( values = [ [ object() ] ] )
        new_names[0].strip = Return( values = [ [ object() ] ] )
        #new_names[1].strip = Return( values = [ [ object() ] ] )
        prov.map_atoms = Return( values = [ zip( old_names, new_names ) ] )
        atoms = [ Fake(), Fake(), Fake() ]
        atoms[0].name = object()
        atoms[1].name = old_names[0]
        atoms[1].element = Fake()
        atoms[1].element.strip = Return( values = [ object() ] )
        atoms[2].name = old_names[2]
        atoms[2].element = Fake()
        atoms[2].element.strip = Return( values = [ old_names[2].strip._orig[0][0] ] )
        ag.atoms = Return( values = [ atoms ] )
        ents = [ object(), object(), object() ]
        chisel.Atom = Return( values = ents )
        vr.delete = Append()
        vr.set = Append()
        self.assertEqual(
            self.tool.rename_atoms(
                atom_group = ag,
                target = target,
                provider = prov,
                translation = trans,
                versioned_root = vr
                ),
            { new_names[0]: atoms[1], new_names[2]: atoms[2] }
            )
        self.assertEqual(
            trans.one_letter_code_for._calls,
            [ ( (), { "three_letter_code": ag.resname } ) ]
            )
        self.assertEqual(
            prov.map_atoms._calls,
            [ ( (), { "source": olc, "target": target } ) ]
            )
        self.assertEqual( ag.atoms._calls, [ ( (), {} ) ] )
        self.assertEqual(
            chisel.Atom._calls,
            [ ( (), { "obj": a } ) for a in [ atoms[0], atoms[1], atoms[1] ] ]
            )
        self.assertEqual( vr.delete._calls, [ ( (), { "entity": ents[0] } ) ] )
        self.assertEqual(
            vr.set._calls,
            [
                ( (), { "entity": ents[1], "property": "name", "value": new_names[0] } ),
                ( (), { "entity": ents[2], "property": "element", "value": new_names[0].strip._orig[0][0] } ),
                ]
            )


class TestAGACBetaUniform(IterableCompareTestCase):

    def setUp(self):

        self.algorithm = chisel.AGACBetaUniform()


    def test_provides_from(self):

        available = {
            self.algorithm.C: None,
            self.algorithm.CA: None,
            "Foo": None,
            }
        self.assertEqual(
            self.algorithm.provides_from( target = "A", available = available ),
            set()
            )
        available[ self.algorithm.N ] = None
        self.assertEqual(
            self.algorithm.provides_from( target = "A", available = available ),
            set( [ self.algorithm.CB ] )
            )
        self.assertEqual(
            self.algorithm.provides_from( target = "G", available = available ),
            set()
            )
        available[ self.algorithm.CB ] = None
        self.assertEqual(
            self.algorithm.provides_from( target = "A", available = available ),
            set( [ self.algorithm.CB ] )
            )


    def test_generate(self):

        ( c, n, ca ) = ( Fake(), Fake(), Fake() )
        c.xyz = ( 4.289,  26.576, -13.941 )
        n.xyz = ( 4.407,  26.635, -16.317 )
        ca.xyz = ( 4.143,  25.740, -15.210 )
        ( ca.b, ca.hetero, ca.occ, ca.segid ) = (
            object(), object(), object(), object()
            )
        coords = ( 5.129,  24.542, -15.254 ) # ( 5.095,  24.556, -15.212 )
        available = {
            self.algorithm.C: c,
            self.algorithm.CA: ca,
            self.algorithm.N: n,
            }
        d = self.algorithm.generate(
            name = self.algorithm.CB, from_atoms = available
            )
        self.assertEqual( d[ "b" ], ca.b )
        self.assertEqual( d[ "hetero" ], ca.hetero )
        self.assertEqual( d[ "occ" ], ca.occ )
        self.assertEqual( d[ "segid" ], ca.segid )
        self.assertEqual( d[ "name" ], self.algorithm.CB )
        self.assertEqual( d[ "element" ], " C" )
        self.assertIterablesAlmostEqual( d[ "xyz" ], coords, 1 )


    def test_atom_from_data(self):

        data = {
            "b": object(),
            "hetero": object(),
            "name": object(),
            "element": object(),
            "occ": object(),
            "segid": object(),
            "xyz": object(),
            }
        atom = Fake()
        atom.set_b = Append()
        atom.set_hetero = Append()
        atom.set_name = Append()
        atom.set_element = Append()
        atom.set_occ = Append()
        atom.set_segid = Append()
        atom.set_xyz = Append()
        chisel.atom = Return( values = [ atom ] )
        self.assertEqual( chisel.AGACBetaUniform.data_to_atom( data = data ), atom )
        self.assertEqual( atom.set_b._calls, [ ( ( data["b"], ), {} ) ] )
        self.assertEqual( atom.set_hetero._calls, [ ( ( data["hetero"], ), {} ) ] )
        self.assertEqual( atom.set_name._calls, [ ( ( data["name"], ), {} ) ] )
        self.assertEqual( atom.set_element._calls, [ ( ( data["element"], ), {} ) ] )
        self.assertEqual( atom.set_occ._calls, [ ( ( data["occ"], ), {} ) ] )
        self.assertEqual( atom.set_segid._calls, [ ( ( data["segid"], ), {} ) ] )
        self.assertEqual( atom.set_xyz._calls, [ ( ( data["xyz"], ), {} ) ] )


class TestResidueRenumber(unittest.TestCase):

    def setUp(self):

        self.tool = chisel.ResidueRenumber.Target( start = 1, applicable = None )
        self.chisel_rg = chisel.ResidueGroup


    def tearDown(self):

        chisel.ResidueGroup = self.chisel_rg


    def test_target(self):

        tool = chisel.ResidueRenumber.Target( start = 1, applicable = None )
        self.assertEqual( tool._enquire, chisel.ChainSample.target_sequence )


    def test_model(self):

        tool = chisel.ResidueRenumber.Model( start = 1, applicable = None )
        self.assertEqual( tool._enquire, chisel.ChainSample.model_sequence )


    def test_generate(self):

        vr = Fake()
        vr.set = Append()

        ( cs, ali ) = ( Fake(), Fake() )
        ali.gap = "-"
        cs.alignment = Return( values = [ ali ] )
        seq = "-AB--CDE-"
        rgs = [ Fake(), Fake(), None, None, Fake(), Fake(), Fake(), Fake(), Fake() ]
        ents = [ object(), object(), object(), object(), object(), object(), object() ]
        chisel.ResidueGroup = Return( values = ents )
        cs.residue_groups = Return( values = [ rgs ] )
        self.tool._enquire = Return( values = [ seq ] )

        resseqs = [ 1, 2, None, None, 3, 3, 5, 5, 7 ]
        icodes = [ " ", " ", None, None, " ", "A", " ", " ", " " ]

        for ( r, n, i ) in zip( rgs, resseqs, icodes ):
            if r:
                r.resseq_as_int = Return( values = [ n ] )
                r.icode = i

        self.tool.design( chain_sample = cs, versioned_root = vr )

        self.assertEqual(
            chisel.ResidueGroup._calls,
            [ ( (), { "obj": r } ) for r in rgs if r ]
            )
        self.assertEqual(
            vr.set._calls,
            [
                ( (), { "entity": ents[0], "property": "resseq", "value": 0 } ),
                ( (), { "entity": ents[0], "property": "icode", "value": "A" } ),
                ( (), { "entity": ents[1], "property": "resseq", "value": 1 } ),
                ( (), { "entity": ents[2], "property": "resseq", "value": 2 } ),
                ( (), { "entity": ents[2], "property": "icode", "value": "B" } ),
                ( (), { "entity": ents[3], "property": "icode", "value": " " } ),
                ( (), { "entity": ents[4], "property": "resseq", "value": 4 } ),
                ( (), { "entity": ents[6], "property": "resseq", "value": 5 } ),
                ( (), { "entity": ents[6], "property": "icode", "value": "A" } ),
                ]
            )


class TestBfactorPrediction(unittest.TestCase):

    def setUp(self):

        self.tool = chisel.BfactorPrediction( predictors = [], minimum = 3, applicable = None )
        self.chisel_atom = chisel.Atom


    def tearDown(self):

        chisel.Atom = self.chisel_atom


    def test_generate(self):

        vr = Fake()
        vr.set = Append()

        cs = Fake()
        atoms = [ Fake(), Fake(), Fake() ]
        cs.flattened_atoms = Return( values = [ atoms ] )
        bs = [ 1, 2, 5 ]

        for ( at, b ) in zip( atoms, bs ):
            at.b = b

        preds = [ Fake(), Fake() ]
        preds[0].calculate = Return( values = [ [ 1, 3, 5 ] ] )
        preds[1].calculate = Return( values = [ [ 4, -1, -1 ] ] )
        self.tool._predictors = preds
        ents = [ object(), object() ]
        chisel.Atom = Return( values = ents )

        self.tool.design( chain_sample = cs, versioned_root = vr )
        self.assertEqual( preds[0].calculate._calls, [ ( (), { "chain_sample": cs } ) ] )
        self.assertEqual( preds[1].calculate._calls, [ ( (), { "chain_sample": cs } ) ] )
        self.assertEqual( cs.flattened_atoms._calls, [ ( (), {} ) ] )
        self.assertEqual(
            chisel.Atom._calls,
            [ ( (), { "obj": a } ) for a in [ atoms[0], atoms[1] ] ]
            )
        self.assertEqual(
            vr.set._calls,
            [
                ( (), { "entity": ents[0], "property": "b", "value": 6 } ),
                ( (), { "entity": ents[1], "property": "b", "value": 3 } ),
                ]
            )


class TestScaled(unittest.TestCase):

    def test_calculate(self):

        values = [ 1, 2, 3, 4, 5 ]
        self.algorithm.calculate_primary = Return( values = [ values ] )
        cs = object()
        self.assertEqual(
            self.algorithm.calculate( chain_sample = cs ),
            [ self.algorithm._factor * v for v in values ]
            )

class TestBPAOriginalBfactor(TestScaled):

    def setUp(self):

        self.algorithm = chisel.BPAOriginalBfactor( factor = 2 )


    def test_calculate_primary(self):

        cs = Fake()
        atoms = [ Fake(), Fake() ]
        cs.flattened_atoms = Return( values = [ atoms ] )
        bs = [ object(), object() ]

        for ( at, b ) in zip( atoms, bs ):
            at.b = b

        self.assertEqual( self.algorithm.calculate_primary( chain_sample = cs ), bs )
        self.assertEqual( cs.flattened_atoms._calls, [ ( (), {} ) ] )


class TestBPAAccessibleSurfaceArea(TestScaled):

    def setUp(self):

        self.asa = Fake()
        self.algorithm = chisel.BPAAccessibleSurfaceArea(
            factor = 2,
            asa = self.asa
            )


    def test__init__(self):

        self.assertEqual( self.algorithm.asa_calculator, self.asa )


    def test_calculate_primary(self):

        ( cs, mmt, vdw, res ) = ( Fake(), Fake(), Fake(), object() )
        cs.mmt = Return( values = [ mmt ] )
        atoms = [ object(), object() ]
        cs.flattened_atoms = Return( values = [ atoms ] )
        rads = [ object(), object() ]
        vdw.radius_for = Return( values = rads )
        mmt.get_van_der_waals_radius_provider = Return( values = [ vdw ] )
        self.asa.accessible_surface_area_for = Return( values = [ res ] )

        self.assertEqual( self.algorithm.calculate_primary( chain_sample = cs ), res )
        self.assertEqual( cs.mmt._calls, [ ( (), {} ) ] )
        self.assertEqual( mmt.get_van_der_waals_radius_provider._calls, [ ( (), {} ) ] )
        self.assertEqual( cs.flattened_atoms._calls, [ ( (), {} ) ] )
        self.assertEqual(
            vdw.radius_for._calls,
            [ ( (), { "atom": a } ) for a in atoms ]
            )
        self.assertEqual(
            self.asa.accessible_surface_area_for._calls,
            [ ( (), { "atoms": atoms, "van_der_waals_radii": rads } ) ]
            )


class TestBPASequenceSimilarity(TestScaled, unittest.TestCase):

    def setUp(self):

        self.ssc = Fake()
        self.algorithm = chisel.BPASequenceSimilarity(
            factor = 2,
            ssc = self.ssc
            )


    def test__init__(self):

        self.assertEqual( self.algorithm._ssc, self.ssc )


    def test_calculate_primary(self):

        cs = Fake()
        atoms = [ ( object(), object() ), ( object(), ), (), ( object(), object(), object() ) ]
        cs.atoms = Return( values = [ atoms ] )
        scores = [ object(), object(), object(), object(), ]
        self.algorithm.ssc_scores_for = Return( values = [ scores ] )
        self.assertEqual(
            self.algorithm.calculate_primary( chain_sample = cs ),
            ( scores[0], scores[0], scores[1], scores[3], scores[3], scores[3] )
            )


suite_entity = unittest.TestLoader().loadTestsFromTestCase(
    TestEntity
    )
suite_indexer = unittest.TestLoader().loadTestsFromTestCase(
    TestIndexer
    )
suite_delete_instruction = unittest.TestLoader().loadTestsFromTestCase(
    TestDeleteInstruction
    )
suite_set_instruction = unittest.TestLoader().loadTestsFromTestCase(
    TestSetInstruction
    )
suite_attach_instruction = unittest.TestLoader().loadTestsFromTestCase(
    TestAttachInstruction
    )
suite_resetiso_instruction = unittest.TestLoader().loadTestsFromTestCase(
    TestResetIsotropicInstruction
    )
suite_chain_sample = unittest.TestLoader().loadTestsFromTestCase(
    TestChainSample
    )
suite_aligned_chain_sample = unittest.TestLoader().loadTestsFromTestCase(
    TestAlignedChainSample
    )


suite_linear_averaged_sequence_similarity = unittest.TestLoader().loadTestsFromTestCase(
    TestLinearAveragedSequenceSimilarity
    )
suite_space_averaged_sequence_similarity = unittest.TestLoader().loadTestsFromTestCase(
    TestSpaceAveragedSequenceSimilarity
    )
suite_mda_gap = unittest.TestLoader().loadTestsFromTestCase(
    TestMDAGap
    )
suite_mda_similarity = unittest.TestLoader().loadTestsFromTestCase(
    TestMDASimilarity
    )
suite_mda_threshold_adjust_similarity = unittest.TestLoader().loadTestsFromTestCase(
    TestMDAThresholdAdjustSimilarity
    )
suite_mda_remove_long = unittest.TestLoader().loadTestsFromTestCase(
    TestMDARemoveLong
    )

suite_mainchain_delete = unittest.TestLoader().loadTestsFromTestCase(
    TestMainchainDelete
    )
suite_mpa_remove_short = unittest.TestLoader().loadTestsFromTestCase(
    TestMPARemoveShort
    )
suite_mpa_keep_regular = unittest.TestLoader().loadTestsFromTestCase(
    TestMPAKeepRegular
    )
suite_sidechain_prune = unittest.TestLoader().loadTestsFromTestCase(
    TestSidechainPrune
    )
suite_spa_schwarzenbacher = unittest.TestLoader().loadTestsFromTestCase(
    TestSPASchwarzenbacher
    )
suite_spa_similarity = unittest.TestLoader().loadTestsFromTestCase(
    TestSPASimilarity
    )
suite_residue_rename = unittest.TestLoader().loadTestsFromTestCase(
    TestResidueRename
    )
suite_aga_cbeta = unittest.TestLoader().loadTestsFromTestCase(
    TestAGACBetaUniform
    )
suite_residue_renumber = unittest.TestLoader().loadTestsFromTestCase(
    TestResidueRenumber
    )
suite_bfactor_prediction = unittest.TestLoader().loadTestsFromTestCase(
    TestBfactorPrediction
    )
suite_bpa_original = unittest.TestLoader().loadTestsFromTestCase(
    TestBPAOriginalBfactor
    )
suite_bpa_asa = unittest.TestLoader().loadTestsFromTestCase(
    TestBPAAccessibleSurfaceArea
    )
suite_bpa_ssc = unittest.TestLoader().loadTestsFromTestCase(
    TestBPASequenceSimilarity
    )

alltests = unittest.TestSuite(
    [
        suite_entity,
        suite_indexer,
        suite_delete_instruction,
        suite_set_instruction,
        suite_attach_instruction,
        suite_resetiso_instruction,
        suite_chain_sample,
        suite_aligned_chain_sample,

        #suite_root,
        #suite_model,
        #suite_chain,
        #suite_residue_group,
        #suite_atom_group,
        #suite_atom,
        #suite_delete_instruction,
        #suite_set_instruction,
        #suite_attach_instruction,
        #suite_reset_isotropic_instruction,
        #suite_aligned_chain_sample,
        #suite_linear_averaged_sequence_similarity,
        #suite_space_averaged_sequence_similarity,
        #suite_mda_gap,
        #suite_mda_similarity,
        #suite_mda_threshold_adjust_similarity,
        #suite_mda_remove_long,

        #suite_mainchain_delete,

        #suite_mpa_remove_short,
        #suite_mpa_keep_regular,
        #suite_sidechain_prune,
        #suite_spa_schwarzenbacher,
        #suite_spa_similarity,
        #suite_residue_rename,
        #suite_aga_cbeta,
        #suite_residue_renumber,
        #suite_bfactor_prediction,
        #suite_bpa_original,
        #suite_bpa_asa,
        #suite_bpa_ssc,
        ]
    )


def load_tests(loader, tests, pattern):

    return alltests


if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
