import unittest

from phaser_regression import test_result

class TestAtomSet(unittest.TestCase):

    def equality_test(self, type):

        left = test_result.AtomSet(
            atoms = [
                test_result.Atom( element = "AX", coordinates = ( 0.15711, 0.15711, 0.50000 ), occupancy = 4.321, uiso = 0.2980 ),
                test_result.Atom( element = "AX", coordinates = ( 0.00653, 0.30365, 0.65019 ), occupancy = 4.787, uiso = 0.3249 ),
                test_result.Atom( element = "AX", coordinates = ( 0.09203, 0.13119, 0.39690 ), occupancy = 3.229, uiso = 0.2761 ),
                test_result.Atom( element = "AX", coordinates = ( -0.10224, 0.39069, 0.28014 ), occupancy = 1.816, uiso = 0.2260 ),
                test_result.Atom( element = "AX", coordinates = ( 0.04162, 0.26124, -0.17351 ), occupancy = 2.718, uiso = 0.3667 ),
                test_result.Atom( element = "AX", coordinates = ( -0.04017, 0.07876, -0.08161 ), occupancy = 2.152, uiso = 0.3433 ),
                test_result.Atom( element = "AX", coordinates = ( 0.14894, 0.37426, 0.43319 ), occupancy = 1.903, uiso = 0.3105 ),
                test_result.Atom( element = "AX", coordinates = ( 0.13355, 0.45944, 0.36534 ), occupancy = 2.439, uiso = 0.3395 ),
                test_result.Atom( element = "AX", coordinates = ( -0.14319, 0.20119, 0.65511 ), occupancy = 1.151, uiso = 0.2901 ),
                test_result.Atom( element = "AX", coordinates = ( -0.12047, 0.24524, 0.41357 ), occupancy = 0.822, uiso = 0.1988 ),
                ],
            cell = ( 78.11, 78.11, 37.01, 90, 90, 90 ),
            space_group = "P 43 21 2"
            )

        right = test_result.AtomSet(
            atoms = [
                test_result.Atom( element = "AX", coordinates = ( 0.15711, 0.15711, 0.50000 ), occupancy = 4.321, uiso = 0.2980 ),
                test_result.Atom( element = "AX", coordinates = ( 0.00653, 0.30365, 0.65019 ), occupancy = 4.787, uiso = 0.3249 ),
                test_result.Atom( element = "AX", coordinates = ( 0.09203, 0.13119, 0.39690 ), occupancy = 3.229, uiso = 0.2761 ),
                test_result.Atom( element = "AX", coordinates = ( -0.10224, 0.39069, 0.28014 ), occupancy = 1.816, uiso = 0.2260 ),
                test_result.Atom( element = "AX", coordinates = ( 0.04162, 0.26124, -0.17351 ), occupancy = 2.718, uiso = 0.3667 ),
                test_result.Atom( element = "AX", coordinates = ( -0.04017, 0.07876, -0.08161 ), occupancy = 2.152, uiso = 0.3433 ),
                test_result.Atom( element = "AX", coordinates = ( 0.14894, 0.37426, 0.43319 ), occupancy = 1.903, uiso = 0.3105 ),
                test_result.Atom( element = "AX", coordinates = ( 0.13355, 0.45944, 0.36534 ), occupancy = 2.439, uiso = 0.3395 ),
                test_result.Atom( element = "AX", coordinates = ( -0.14319, 0.20119, 0.65511 ), occupancy = 1.151, uiso = 0.2901 ),
                test_result.Atom( element = "AX", coordinates = ( -0.12047, 0.24524, 0.41357 ), occupancy = 0.822, uiso = 0.1988 ),
                ],
            cell = ( 78.11, 78.11, 37.01, 90, 90, 90 ),
            space_group = "P 43 21 2"
            )
        self.assertEqual(
            left.equality( other = right, type = type ),
            ( True, None )
            )

        right = test_result.AtomSet(
            atoms = [
                test_result.Atom( element = "AX", coordinates = ( 0.15711, 0.15711, 0.50000 ), occupancy = 4.321, uiso = 0.2980 ),
                test_result.Atom( element = "AX", coordinates = ( 0.00653, 0.30365, 0.65019 ), occupancy = 4.787, uiso = 0.3249 ),
                test_result.Atom( element = "AX", coordinates = ( 0.09203, 0.13119, 0.39690 ), occupancy = 3.229, uiso = 0.2761 ),
                test_result.Atom( element = "AX", coordinates = ( -0.10224, 0.39069, 0.28014 ), occupancy = 1.816, uiso = 0.2260 ),
                test_result.Atom( element = "AX", coordinates = ( -0.04017, 0.07876, -0.08161 ), occupancy = 2.152, uiso = 0.3433 ),
                test_result.Atom( element = "AX", coordinates = ( 0.04162, 0.26124, -0.17351 ), occupancy = 2.718, uiso = 0.3667 ),
                test_result.Atom( element = "AX", coordinates = ( 0.14894, 0.37426, 0.43319 ), occupancy = 1.903, uiso = 0.3105 ),
                test_result.Atom( element = "AX", coordinates = ( 0.13355, 0.45944, 0.36534 ), occupancy = 2.439, uiso = 0.3395 ),
                test_result.Atom( element = "AX", coordinates = ( -0.14319, 0.20119, 0.65511 ), occupancy = 1.151, uiso = 0.2901 ),
                test_result.Atom( element = "AX", coordinates = ( -0.12047, 0.24524, 0.41357 ), occupancy = 0.822, uiso = 0.1988 ),
                ],
            cell = ( 78.11, 78.11, 37.01, 90, 90, 90 ),
            space_group = "P 43 21 2"
            )
        self.assertEqual(
            left.equality( other = right, type = type ),
            ( True, None )
            )
        right = test_result.AtomSet(
            atoms = [
                test_result.Atom( element = "AX", coordinates = ( 0.15711, 0.15711, 0.50000 ), occupancy = 4.321, uiso = 0.2980 ),
                test_result.Atom( element = "AX", coordinates = ( 0.00653, 0.30365, 0.65019 ), occupancy = 4.787, uiso = 0.3249 ),
                test_result.Atom( element = "AX", coordinates = ( 0.09203, 0.13119, 0.39690 ), occupancy = 3.229, uiso = 0.2761 ),
                test_result.Atom( element = "AX", coordinates = ( -0.10224, 0.39069, 0.28014 ), occupancy = 1.816, uiso = 0.2260 ),
                test_result.Atom( element = "AX", coordinates = ( -0.04017, 0.07876, -0.08161 ), occupancy = 2.152, uiso = 0.3433 ),
                test_result.Atom( element = "IX", coordinates = ( 0.04162, 0.26124, -0.17351 ), occupancy = 2.718, uiso = 0.3667 ),
                test_result.Atom( element = "AX", coordinates = ( 0.14894, 0.37426, 0.43319 ), occupancy = 1.903, uiso = 0.3105 ),
                test_result.Atom( element = "AX", coordinates = ( 0.13355, 0.45944, 0.36534 ), occupancy = 2.439, uiso = 0.3395 ),
                test_result.Atom( element = "AX", coordinates = ( -0.14319, 0.20119, 0.65511 ), occupancy = 1.151, uiso = 0.2901 ),
                test_result.Atom( element = "AX", coordinates = ( -0.12047, 0.24524, 0.41357 ), occupancy = 0.822, uiso = 0.1988 ),
                ],
            cell = ( 78.11, 78.11, 37.01, 90, 90, 90 ),
            space_group = "P 43 21 2"
            )
        res = left.equality( other = right, type = type )
        self.assertFalse( res[0] )
        self.assertTrue(
            res[1].startswith( "different number of atoms for element" )
            )

        # Skip this test
        """
        right = test_result.AtomSet(
            atoms = [
                test_result.Atom( element = "AX", coordinates = ( 0.15711, 0.15711, 0.50000 ), occupancy = 4.321, uiso = 0.2980 ),
                test_result.Atom( element = "AX", coordinates = ( 0.23653, 0.30365, 0.65019 ), occupancy = 4.787, uiso = 0.3249 ),
                test_result.Atom( element = "AX", coordinates = ( 0.09203, 0.13119, 0.39690 ), occupancy = 3.229, uiso = 0.2761 ),
                test_result.Atom( element = "AX", coordinates = ( -0.10224, 0.39069, 0.28014 ), occupancy = 1.816, uiso = 0.2260 ),
                test_result.Atom( element = "AX", coordinates = ( -0.04017, 0.07876, -0.08161 ), occupancy = 2.152, uiso = 0.3433 ),
                test_result.Atom( element = "AX", coordinates = ( 0.04162, 0.26124, -0.17351 ), occupancy = 2.718, uiso = 0.3667 ),
                test_result.Atom( element = "AX", coordinates = ( 0.14894, 0.37426, 0.43319 ), occupancy = 1.903, uiso = 0.3105 ),
                test_result.Atom( element = "AX", coordinates = ( 0.13355, 0.45944, 0.36534 ), occupancy = 2.439, uiso = 0.3395 ),
                test_result.Atom( element = "AX", coordinates = ( -0.14319, 0.20119, 0.65511 ), occupancy = 1.151, uiso = 0.2901 ),
                test_result.Atom( element = "AX", coordinates = ( -0.12047, 0.24524, 0.41357 ), occupancy = 0.822, uiso = 0.1988 ),
                ],
            cell = ( 78.11, 78.11, 37.01, 90, 90, 90 ),
            space_group = "P 43 21 2"
            )

        res = left.equality( other = right, type = type )
        self.assertFalse( res[0] )
        self.assertTrue(
            res[1].startswith( "unpaired atom (site_mismatch):" )
            )
        """
        right = test_result.AtomSet(
            atoms = [
                test_result.Atom( element = "AX", coordinates = ( 0.15711, 0.15711, 0.50000 ), occupancy = 4.321, uiso = 0.2980 ),
                test_result.Atom( element = "AX", coordinates = ( 0.00653, 0.30365, 0.65019 ), occupancy = 4.787, uiso = 0.3249 ),
                test_result.Atom( element = "AX", coordinates = ( 0.09203, 0.13119, 0.39690 ), occupancy = 3.229, uiso = 0.2761 ),
                test_result.Atom( element = "AX", coordinates = ( -0.10224, 0.39069, 0.28014 ), occupancy = 1.816, uiso = 0.2260 ),
                test_result.Atom( element = "AX", coordinates = ( -0.04017, 0.07876, -0.08161 ), occupancy = 2.352, uiso = 0.3433 ),
                test_result.Atom( element = "AX", coordinates = ( 0.04162, 0.26124, -0.17351 ), occupancy = 2.718, uiso = 0.3667 ),
                test_result.Atom( element = "AX", coordinates = ( 0.14894, 0.37426, 0.43319 ), occupancy = 1.903, uiso = 0.3105 ),
                test_result.Atom( element = "AX", coordinates = ( 0.13355, 0.45944, 0.36534 ), occupancy = 2.439, uiso = 0.3395 ),
                test_result.Atom( element = "AX", coordinates = ( -0.14319, 0.20119, 0.65511 ), occupancy = 1.151, uiso = 0.2901 ),
                test_result.Atom( element = "AX", coordinates = ( -0.12047, 0.24524, 0.41357 ), occupancy = 0.822, uiso = 0.1988 ),
                ],
            cell = ( 78.11, 78.11, 37.01, 90, 90, 90 ),
            space_group = "P 43 21 2"
            )

        res = left.equality( other = right, type = type )
        self.assertFalse( res[0] )
        self.assertTrue(
            res[1].startswith( "atom pair (occupancy mismatch):" )
            )

        right = test_result.AtomSet(
            atoms = [
                test_result.Atom( element = "AX", coordinates = ( 0.15711, 0.15711, 0.50000 ), occupancy = 4.321, uiso = 0.2980 ),
                test_result.Atom( element = "AX", coordinates = ( 0.00653, 0.30365, 0.65019 ), occupancy = 4.787, uiso = 0.3249 ),
                test_result.Atom( element = "AX", coordinates = ( 0.09203, 0.13119, 0.39690 ), occupancy = 3.229, uiso = 0.2761 ),
                test_result.Atom( element = "AX", coordinates = ( -0.10224, 0.39069, 0.28014 ), occupancy = 1.816, uiso = 0.2260 ),
                test_result.Atom( element = "AX", coordinates = ( -0.04017, 0.07876, -0.08161 ), occupancy = 2.152, uiso = 0.3433 ),
                test_result.Atom( element = "AX", coordinates = ( 0.04162, 0.26124, -0.17351 ), occupancy = 2.718, uiso = 0.3667 ),
                test_result.Atom( element = "AX", coordinates = ( 0.14894, 0.37426, 0.43319 ), occupancy = 1.903, uiso = 0.3705 ),
                test_result.Atom( element = "AX", coordinates = ( 0.13355, 0.45944, 0.36534 ), occupancy = 2.439, uiso = 0.3395 ),
                test_result.Atom( element = "AX", coordinates = ( -0.14319, 0.20119, 0.65511 ), occupancy = 1.151, uiso = 0.2901 ),
                test_result.Atom( element = "AX", coordinates = ( -0.12047, 0.24524, 0.41357 ), occupancy = 0.822, uiso = 0.1988 ),
                ],
            cell = ( 78.11, 78.11, 37.01, 90, 90, 90 ),
            space_group = "P 43 21 2"
            )

        res = left.equality( other = right, type = type )
        self.assertFalse( res[0] )
        self.assertTrue(
            res[1].startswith( "atom pair (uiso mismatch):" )
            )


    def test_strong_equality(self):

        self.equality_test( type = "strong" )


    def test_weak_equality(self):

        self.equality_test( type = "weak" )


suite_atom_set = unittest.TestLoader().loadTestsFromTestCase(
    TestAtomSet
    )

alltests = unittest.TestSuite(
    [
        suite_atom_set,
        ]
    )


def load_tests(loader, tests, pattern):

    return alltests


if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
