from phaser import sculptor
from phaser import chisel

from phaser.pipeline.scaffolding import Fake, Return, Append, IterableCompareTestCase

import unittest

class TestSculptor(unittest.TestCase):

    def setUp(self):

        self.rsam_mms_determine = rsam.MacromoleculeSpecifics.Determine
        self.chisel_rg = chisel.ResidueGroup
        self.sculptor_rg_diff = sculptor.residue_group_difference
        self.sculptor_chisel_chain_sample = sculptor.chisel.ChainSample
        self.sculptor_chisel_chain_sample_noal = sculptor.chisel.ChainSampleNoAlignment


    def tearDown(self):

        rsam.MacromoleculeSpecifics.Determine = self.rsam_mms_determine
        chisel.ResidueGroup = self.chisel_rg
        sculptor.residue_group_difference = self.sculptor_rg_diff
        sculptor.chisel.ChainSample = self.sculptor_chisel_chain_sample
        sculptor.chisel.ChainSampleNoAlignment = self.sculptor_chisel_chain_sample_noal


    def test_residue_group_difference(self):

        chain = Fake()
        rgs = [ Fake(), Fake(), Fake() ]
        mapping = [ Fake(), None, None, Fake() ]
        chain.residue_groups = Return( values = [ rgs ] )
        ids = [ object(), object(), object() ]

        for ( r, i ) in zip( rgs, ids ):
            r.memory_id = Return( values = [ i ] )

        mapping[0].memory_id = Return( values = [ ids[1] ] )
        mapping[3].memory_id = Return( values = [ ids[0] ] )
        self.assertEqual(
            sculptor.residue_group_difference( chain = chain, residue_groups = mapping ),
            [ rgs[2] ]
            )
        self.assertEqual( chain.residue_groups._calls, [ ( (), {} ) ] )

        for rg in rgs + [ r for r in mapping if r ]:
            self.assertEqual( rg.memory_id._calls, [ ( (), {} ) ] )


    def test_create_chain_sample(self):

        ( chain, collection, logger, cs, rgs ) = ( Fake(), Fake(), Fake(), object(), object() )
        mmt = rsam.MacromoleculeSpecifics.UNKNOWN
        chain.residue_groups = Return( values = [ rgs ] )
        logger.info = Append()
        logger.debug = Append()
        rsam.MacromoleculeSpecifics.Determine = Return( values = [ mmt ] )
        sculptor.chisel.ChainSampleNoAlignment = Return( values = [ cs ] )
        self.assertEqual(
            sculptor.create_chain_sample(
                chain = chain,
                collection = collection,
                logger = logger
                ),
            cs
            )

        self.assertEqual(
            rsam.MacromoleculeSpecifics.Determine._calls,
            [ ( (), { "chain": chain } ) ]
            )
        self.assertEqual(
            sculptor.chisel.ChainSampleNoAlignment._calls,
            [ ( (), { "residue_groups": rgs, "mmt": mmt } ) ]
            )

        sculptor.chisel.ChainSampleNoAlignment.reset()
        mmt = rsam.MacromoleculeSpecifics.PROTEIN
        rsam.MacromoleculeSpecifics.Determine = Return( values = [ mmt ] )
        chain.residue_groups.reset()
        collection.find_alignment_sequence = Return( values = [ None ] )
        self.assertEqual(
            sculptor.create_chain_sample(
                chain = chain,
                collection = collection,
                logger = logger
                ),
            cs
            )

        self.assertEqual(
            rsam.MacromoleculeSpecifics.Determine._calls,
            [ ( (), { "chain": chain } ) ]
            )
        self.assertEqual(
            sculptor.chisel.ChainSampleNoAlignment._calls,
            [ ( (), { "residue_groups": rgs, "mmt": mmt } ) ]
            )
        self.assertEqual(
            collection.find_alignment_sequence._calls,
            [ ( (), { "chain": chain, "mmt": mmt } ) ]
            )

        rsam.MacromoleculeSpecifics.Determine.reset()
        ( ali, index, mapping, cs2 ) = ( object(), object(), object(), object() )
        sculptor.chisel.ChainSample = Return( values = [ cs2 ] )
        collection.find_alignment_sequence = Return( values = [ [ ( ali, index, mapping ), object() ] ] )
        self.assertEqual(
            sculptor.create_chain_sample(
                chain = chain,
                collection = collection,
                logger = logger
                ),
            cs2
            )

        self.assertEqual(
            rsam.MacromoleculeSpecifics.Determine._calls,
            [ ( (), { "chain": chain } ) ]
            )
        self.assertEqual(
            sculptor.chisel.ChainSample._calls,
            [ ( (), { "residue_groups": mapping, "mmt": mmt, "alignment": ali, "index": index } ) ]
            )
        self.assertEqual(
            collection.find_alignment_sequence._calls,
            [ ( (), { "chain": chain, "mmt": mmt } ) ]
            )


    def test_run_chain_design(self):

        ( sample, mmt, logger, vr ) = ( Fake(), object(), Fake(), object() )
        sample.mmt = Return( values = [ mmt ] )
        logger.info = Append()
        logger.debug = Append()
        workflow = [ Fake(), Fake(), Fake() ]
        workflow[0].applicable = Return( values = [ ( mmt, ) ] )
        workflow[0].design = Append()
        workflow[1].applicable = Return( values = [ ( object(), object() ) ] )
        workflow[2].applicable = Return( values = [ ( object(), mmt, ) ] )
        workflow[2].design = Append()
        sculptor.run_chain_design(
            sample = sample,
            workflow = workflow,
            versioned_root = vr,
            logger = logger
            )
        self.assertEqual( sample.mmt._calls, [ ( (), {} ) ] )

        for wf in workflow:
            self.assertEqual( wf.applicable._calls, [ ( (), {} ) ] )

        for wf in [ workflow[0], workflow[2] ]:
            self.assertEqual(
                wf.design._calls,
                [ ( (), { "chain_sample": sample, "versioned_root": vr } ) ]
                )


class TestFactory(IterableCompareTestCase):

    def setUp(self):

        #self.sculptor_rsam_triangular = sculptor.rsam.TriangularWeights
        #self.sculptor_rsam_unit = sculptor.rsam.UnitWeights
        #self.sculptor_rsam_asa = sculptor.rsam.AccessibleSurfaceArea
        #self.sculptor_rsam_ssc = sculptor.rsam.SequenceSimilarity
        #self.sculptor_get_weighting_scheme = sculptor.get_weighting_scheme
        #self.sculptor_get_ssc_from = sculptor.get_ssc_from
        #self.sculptor_chisel = sculptor.chisel
        #sculptor.chisel = Fake()
        pass


    def tearDown(self):

        #sculptor.rsam.TriangularWeights = self.sculptor_rsam_triangular
        #sculptor.rsam.UnitWeights = self.sculptor_rsam_unit
        #sculptor.rsam.AccessibleSurfaceArea = self.sculptor_rsam_asa
        #sculptor.rsam.SequenceSimilarity = self.sculptor_rsam_ssc
        #sculptor.get_weighting_scheme = self.sculptor_get_weighting_scheme
        #sculptor.get_ssc_from = self.sculptor_get_ssc_from
        #sculptor.chisel = self.sculptor_chisel
        pass


    def test_get_unit_weights(self):

        self.assertIterablesAlmostEqual(
            sculptor.get_unit_weights( window = 3 ),
            [ 1, 1, 1, 1, 1, 1, 1 ],
            8
            )


    def test_get_triangular_weights(self):

        self.assertIterablesAlmostEqual(
            sculptor.get_triangular_weights( window = 3 ),
            [ 1, 2, 3, 4, 3, 2, 1 ],
            8
            )


    def test_gap_mainchain_algorithm(self):

        alg = sculptor.gap_mainchain_algorithm( params = None )
        self.assert_( isinstance( alg, chisel.MDAGap ) )


    def test_similarity_mainchain_algorithm(self):

        params = Fake()
        params.calculation = Fake()
        params.calculation.weighting = "triangular"
        params.calculation.window = 1
        params.calculation.matrix = "blosum62"
        params.threshold = -1
        alg = sculptor.similarity_mainchain_algorithm( params = params )
        self.assert_( isinstance( alg, chisel.MDASimilarity ) )
        self.assertEqual( alg.ss_calc.matrix, "blosum62" )
        self.assertEqual( alg.ss_calc.unknown, 0 )
        self.assertIterablesAlmostEqual(
            alg.ss_calc.weights,
            [ 0.25, 0.5, 0.25 ],
            8
            )
        self.assertEqual( alg.ss_calc.window, 1 )
        self.assertAlmostEqual( alg.threshold, -1, 8 )


    def test_threshold_adjust_similarity_mainchain_algorithm(self):

        params = Fake()
        params.calculation = Fake()
        params.calculation.weighting = "triangular"
        params.calculation.window = 1
        params.calculation.matrix = "blosum62"
        params.offset = -0.05
        alg = sculptor.threshold_adjust_similarity_mainchain_algorithm( params = params )
        self.assert_( isinstance( alg, chisel.MDAThresholdAdjustSimilarity ) )
        self.assertEqual( alg.ss_calc.matrix, "blosum62" )
        self.assertEqual( alg.ss_calc.unknown, 0 )
        self.assertIterablesAlmostEqual(
            alg.ss_calc.weights,
            [ 0.25, 0.5, 0.25 ],
            8
            )
        self.assertEqual( alg.ss_calc.window, 1 )
        self.assertAlmostEqual( alg.offset, -0.05, 8 )


    def test_remove_long_mainchain_algorithm(self):

        params = Fake()
        params.min_length = 3
        alg = sculptor.remove_long_mainchain_algorithm( params = params )
        self.assert_( isinstance( alg, chisel.MDARemoveLong ) )
        self.assertEqual( alg.min_length, 3 )


    def test_remove_short_polishing_algorithm(self):

        params = Fake()
        params.minimum_length = 3
        alg = sculptor.remove_short_polishing_algorithm( params = params )
        self.assert_( isinstance( alg, chisel.MPARemoveShort ) )
        self.assertEqual( alg.min_length, 3 )


    def test_keep_regular_polishing_algorithm(self):

        params = Fake()
        params.maximum_length = 3
        alg = sculptor.keep_regular_polishing_algorithm( params = params )
        self.assert_( isinstance( alg, chisel.MPAKeepRegular ) )
        self.assertEqual( alg.max_length, 3 )


    def test_schwarzenbacher_sidechain_pruning_algorithm(self):

        params = Fake()
        params.pruning_level = 3
        alg = sculptor.schwarzenbacher_sidechain_pruning_algorithm( params = params )
        self.assert_( isinstance( alg, chisel.SPASchwarzenbacher ) )
        self.assertEqual( alg.level, 3 )


    def test_similarity_sidechain_pruning_algorithm(self):

        params = Fake()
        params.calculation = Fake()
        params.calculation.weighting = "triangular"
        params.calculation.window = 1
        params.calculation.matrix = "blosum62"
        params.full_truncation_limit = -1
        params.full_length_limit = 0
        params.pruning_level = 3
        alg = sculptor.similarity_sidechain_pruning_algorithm( params = params )
        self.assert_( isinstance( alg, chisel.SPASimilarity ) )
        self.assertEqual( alg.ss_calc.matrix, "blosum62" )
        self.assertEqual( alg.ss_calc.unknown, 0 )
        self.assertIterablesAlmostEqual(
            alg.ss_calc.weights,
            [ 0.25, 0.5, 0.25 ],
            8
            )
        self.assertEqual( alg.level, 3 )


    def test_original_bfactor_algorithm(self):

        params = Fake()
        params.factor = 3
        alg = sculptor.original_bfactor_algorithm( params = params )
        self.assert_( isinstance( alg, chisel.BPAOriginalBfactor ) )
        self.assertEqual( alg.factor, 3 )


    def test_similarity_bfactor_algorithm(self):

        params = Fake()
        params.calculation = Fake()
        params.calculation.weighting = "triangular"
        params.calculation.window = 1
        params.calculation.matrix = "blosum62"
        params.factor = 3
        alg = sculptor.similarity_bfactor_algorithm( params = params )
        self.assert_( isinstance( alg, chisel.BPASequenceSimilarity ) )
        self.assertEqual( alg.ss_calc.matrix, "blosum62" )
        self.assertEqual( alg.ss_calc.unknown, 0 )
        self.assertIterablesAlmostEqual(
            alg.ss_calc.weights,
            [ 0.25, 0.5, 0.25 ],
            8
            )
        self.assertEqual( alg.factor, 3 )


    def test_asa_bfactor_algorithm(self):

        params = Fake()
        params.probe_radius = 1.4
        params.precision = 960
        params.factor = 3
        alg = sculptor.asa_bfactor_algorithm( params = params )
        self.assert_( isinstance( alg, chisel.BPAAccessibleSurfaceArea ) )
        self.assertAlmostEqual( alg.probe, 1.4, 8 )
        self.assertEqual( alg.precision, 960 )
        self.assertEqual( alg.factor, 3 )


    def test_target_renumber_step(self):

        params = Fake()
        params.start = 3
        alg = sculptor.target_renumber_step( params = params )
        self.assert_( isinstance( alg, chisel.MRASequence ) )
        self.assertEqual( alg.start, 3 )
        self.assertEqual( alg.enquire, chisel.AlignedChainSample.target_sequence )


    def test_model_renumber_step(self):

        params = Fake()
        params.start = 3
        alg = sculptor.model_renumber_step( params = params )
        self.assert_( isinstance( alg, chisel.MRASequence ) )
        self.assertEqual( alg.start, 3 )
        self.assertEqual( alg.enquire, chisel.AlignedChainSample.model_sequence )


    def test_no_renumber_step(self):

        alg = sculptor.no_renumber_step( params = None )
        self.assert_( isinstance( alg, chisel.MRAOriginal ) )


    def test_get_protocol(self):

        for i in sculptor.PROTOCOLS.keys():
            self.assert_( sculptor.get_protocol_phil( key = i ) )


    def ntest_get_weights(self):

        self.assertEqual(
            sculptor.get_weighting_scheme( name = "triangular" ),
            sculptor.rsam.TriangularWeights
            )
        self.assertEqual(
            sculptor.get_weighting_scheme( name = "uniform" ),
            sculptor.rsam.UnitWeights
            )


    def ntest_get_ssc_from(self):

        ( mmt, params, mat, ws, ssc ) = ( Fake(), Fake(), object(), object(), object() )
        mmt.get_normalized_similarity_matrix = Return( values = [ mat ] )
        sculptor.rsam.SequenceSimilarity = Return( values = [ ssc ] )
        params.matrix = object()
        params.weighting = object()
        params.window = object()
        sculptor.get_weighting_scheme = Return( values = [ ws ] )
        self.assertEqual( sculptor.get_ssc_from( mmt = mmt, params = params ), ssc )
        self.assertEqual(
            mmt.get_normalized_similarity_matrix._calls,
            [ ( (), { "name": params.matrix } ) ]
            )
        self.assertEqual(
            sculptor.get_weighting_scheme._calls,
            [ ( (), { "name": params.weighting } ) ]
            )
        self.assertEqual(
            rsam.SequenceSimilarity._calls,
            [ ( (), { "matrix": mat, "window": params.window, "weighting": ws } ) ]
            )

    def ntest_get_mainchain_deletion_algorithm(self):

        ( gap, sim ) = ( object(), object() )
        sculptor.chisel.MDAGap = Return( values = [ gap ] )
        self.assertEqual(
            sculptor.get_mainchain_deletion_algorithm( name = "gap", params = None ),
            gap
            )
        self.assertEqual( sculptor.chisel.MDAGap._calls, [ ( (), {} ) ] )

        params = Fake()
        params.calculation = Fake()
        params.threshold = Fake()
        ssc = object()
        sculptor.get_ssc_from = Return( values = [ ssc ] )
        sculptor.chisel.MDASimilarity = Return( values = [ sim ] )
        self.assertEqual(
            sculptor.get_mainchain_deletion_algorithm( name = "similarity", params = params ),
            sim
            )
        self.assertEqual(
            sculptor.chisel.MDASimilarity._calls,
            [ ( (), { "threshold": params.threshold, "ssc": ssc } ) ]
            )
        self.assertEqual(
            sculptor.get_ssc_from._calls,
            [ ( (), { "mmt": rsam.MacromoleculeSpecifics.PROTEIN, "params": params.calculation } ) ]
            )

        self.assertRaises(
            sculptor.PhaserValueError,
            sculptor.get_mainchain_deletion_algorithm,
            name = "Foo",
            params = None
            )


    def ntest_get_mainchain_polishing_algorithm(self):

        params = Fake()
        params.minimum_length = object()
        rs = object()
        sculptor.chisel.MPARemoveShort = Return( values = [ rs ] )

        self.assertEqual(
            sculptor.get_mainchain_polishing_algorithm(
                name = "remove_short",
                params = params
                ),
            rs
            )
        self.assertEqual(
            sculptor.chisel.MPARemoveShort._calls,
            [ ( (), { "min_length": params.minimum_length } ) ]
            )

        self.assertRaises(
            sculptor.PhaserValueError,
            sculptor.get_mainchain_polishing_algorithm,
            name = "Foo",
            params = None
            )


    def ntest_get_sidechain_pruning_algorithm(self):

        params = Fake()
        params.pruning_level = object()
        res = object()
        sculptor.chisel.SPASchwarzenbacher = Return( values = [ res ] )
        self.assertEqual(
            sculptor.get_sidechain_pruning_algorithm( name = "schwarzenbacher", params = params ),
            res
            )
        self.assertEqual(
            sculptor.chisel.SPASchwarzenbacher._calls,
            [ ( (), { "level": params.pruning_level } ) ]
            )


        params.full_truncation_limit = object(),
        params.full_length_limit = object()
        params.calculation = object()
        res = object()
        sculptor.chisel.SPASimilarity = Return( values = [ res ] )
        ssc = object()
        sculptor.get_ssc_from = Return( values = [ ssc ] )
        self.assertEqual(
            sculptor.get_sidechain_pruning_algorithm( name = "similarity", params = params ),
            res
            )
        self.assertEqual(
            sculptor.chisel.SPASimilarity._calls,
            [ ( (), {
                "ssc": ssc,
                "level": params.pruning_level,
                "upper": params.full_length_limit,
                "lower": params.full_truncation_limit
                }
                ) ]
            )
        self.assertEqual(
            sculptor.get_ssc_from._calls,
            [ ( (), { "mmt": rsam.MacromoleculeSpecifics.PROTEIN, "params": params.calculation } ) ]
            )

        self.assertRaises(
            sculptor.PhaserValueError,
            sculptor.get_sidechain_pruning_algorithm,
            name = "Foo",
            params = None
            )


    def ntest_get_residue_completion_algorithm(self):

        res = object()
        sculptor.chisel.AGACBetaUniform = Return( values = [ res ] )
        self.assertEqual(
            sculptor.get_residue_completion_algorithm( name = "cbeta", params = None ),
            res
            )
        self.assertEqual( sculptor.chisel.AGACBetaUniform._calls, [ ( (), {} ) ] )

        self.assertRaises(
            sculptor.PhaserValueError,
            sculptor.get_residue_completion_algorithm,
            name = "Foo",
            params = None
            )


    def ntest_get_bfactor_prediction_algorithm(self):

        params = Fake()
        params.factor = object()
        params.constant = object()
        res = object()
        sculptor.chisel.BPAOriginalBfactor = Return( values = [ res ] )
        self.assertEqual(
            sculptor.get_bfactor_prediction_algorithm( name = "original", params = params ),
            res
            )
        self.assertEqual(
            sculptor.chisel.BPAOriginalBfactor._calls,
            [ ( (), { "factor": params.factor } ) ]
            )

        params.calculation = object()
        res = object()
        sculptor.chisel.BPASequenceSimilarity = Return( values = [ res ] )
        ssc = object()
        sculptor.get_ssc_from = Return( values = [ ssc ] )
        self.assertEqual(
            sculptor.get_bfactor_prediction_algorithm( name = "similarity", params = params ),
            res
            )
        self.assertEqual(
            sculptor.chisel.BPASequenceSimilarity._calls,
            [ ( (), {
                "ssc": ssc,
                "factor": params.factor,
                }
                ) ]
            )
        self.assertEqual(
            sculptor.get_ssc_from._calls,
            [ ( (), { "mmt": rsam.MacromoleculeSpecifics.PROTEIN, "params": params.calculation } ) ]
            )

        del params.calculation

        params.probe_radius = object()
        params.precision = object()
        res = object()
        sculptor.chisel.BPAAccessibleSurfaceArea = Return( values = [ res ] )
        asa = object()
        rsam.AccessibleSurfaceArea = Return( values = [ asa ] )
        self.assertEqual(
            sculptor.get_bfactor_prediction_algorithm( name = "asa", params = params ),
            res
            )
        self.assertEqual(
            sculptor.chisel.BPAAccessibleSurfaceArea._calls,
            [ ( (), {
                "asa": asa,
                "factor": params.factor,
                }
                ) ]
            )
        self.assertEqual(
            rsam.AccessibleSurfaceArea._calls,
            [ ( (), { "probe_radius": params.probe_radius, "precision": params.precision } ) ]
            )

        self.assertRaises(
            sculptor.PhaserValueError,
            sculptor.get_bfactor_prediction_algorithm,
            name = "Foo",
            params = None
            )


class TestWorkflowStepFactory(unittest.TestCase):

    def setUp(self):

        self.sculptor_get_algorithms = sculptor.get_algorithms
        self.sculptor_get_residue_completion_algorithm = sculptor.get_residue_completion_algorithm
        self.sculptor_get_mainchain_step = sculptor.get_mainchain_step
        self.sculptor_get_sidechain_prune_step = sculptor.get_sidechain_prune_step
        self.sculptor_get_bfactor_predict_step = sculptor.get_bfactor_predict_step
        self.sculptor_get_rename_step = sculptor.get_rename_step
        self.sculptor_get_renumber_step = sculptor.get_renumber_step
        self.sculptor_chisel_renumber_target = sculptor.chisel.ResidueRenumber.Target
        self.sculptor_chisel_renumber_model = sculptor.chisel.ResidueRenumber.Model
        self.sculptor_chisel_residue_rename = sculptor.chisel.ResidueRename
        self.sculptor_chisel_bfactor_predict = sculptor.chisel.BfactorPrediction
        self.sculptor_chisel_mainchain_delete = sculptor.chisel.MainchainDelete
        self.sculptor_chisel_sidechain_prune = sculptor.chisel.SidechainPrune
        self.sculptor_chisel_clean_unknown = sculptor.chisel.CleanUnknown


    def tearDown(self):

        sculptor.get_algorithms = self.sculptor_get_algorithms
        sculptor.get_residue_completion_algorithm = self.sculptor_get_residue_completion_algorithm
        sculptor.get_mainchain_step = self.sculptor_get_mainchain_step
        sculptor.get_sidechain_prune_step = self.sculptor_get_sidechain_prune_step
        sculptor.get_bfactor_predict_step = self.sculptor_get_bfactor_predict_step
        sculptor.get_rename_step = self.sculptor_get_rename_step
        sculptor.get_renumber_step = self.sculptor_get_renumber_step
        sculptor.chisel.ResidueRenumber.Target = self.sculptor_chisel_renumber_target
        sculptor.chisel.ResidueRenumber.Model = self.sculptor_chisel_renumber_model
        sculptor.chisel.ResidueRename = self.sculptor_chisel_residue_rename
        sculptor.chisel.BfactorPrediction = self.sculptor_chisel_bfactor_predict
        sculptor.chisel.MainchainDelete = self.sculptor_chisel_mainchain_delete
        sculptor.chisel.SidechainPrune = self.sculptor_chisel_sidechain_prune
        sculptor.chisel.CleanUnknown = self.sculptor_chisel_clean_unknown


    def test_get_algorithms(self):

        use = [ "foo", "bar" ]
        params = Fake()
        algs = [ object(), object() ]
        params.foo = object()
        factory = Return( values = algs )
        self.assertEqual(
            sculptor.get_algorithms( use = use, params = params, factory = factory ),
            algs
            )
        self.assertEqual(
            factory._calls,
            [
                ( (), { "name": "foo", "params": params.foo } ),
                ( (), { "name": "bar", "params": None } ),
                ]
            )


    def test_get_mainchain_step(self):

        deletion = Fake()
        deletion.use = object()
        polishing = Fake()
        polishing.use = object()
        ( alg_d, alg_p ) = ( object(), object() )
        sculptor.get_algorithms = Return( values = [ alg_d, alg_p ] )
        res = object()
        sculptor.chisel.MainchainDelete = Return( values = [ res ] )
        self.assertEqual(
            sculptor.get_mainchain_step( deletion = deletion, polishing = polishing ),
            res
            )
        self.assertEqual(
            sculptor.get_algorithms._calls,
            [
                ( (), {
                    "use": deletion.use,
                    "params": deletion,
                    "factory": sculptor.get_mainchain_deletion_algorithm,
                    } ),
                ( (), {
                    "use": polishing.use,
                    "params": polishing,
                    "factory": sculptor.get_mainchain_polishing_algorithm,
                    } ),
                ]
            )
        self.assertEqual(
            sculptor.chisel.MainchainDelete._calls,
            [ ( (), { "deletion": alg_d, "polishing": alg_p, "applicable": sculptor.MMTS } ) ]
            )


    def test_get_sidechain_prune_step(self):

        pruning = Fake()
        pruning.use = object()
        algs = object()
        sculptor.get_algorithms = Return( values = [ algs ] )
        res = object()
        sculptor.chisel.SidechainPrune = Return( values = [ res ] )
        self.assertEqual(
            sculptor.get_sidechain_prune_step( params = pruning ),
            res
            )
        self.assertEqual(
            sculptor.get_algorithms._calls,
            [
                ( (), {
                    "use": pruning.use,
                    "params": pruning,
                    "factory": sculptor.get_sidechain_pruning_algorithm,
                    } ),
                ]
            )
        self.assertEqual(
            sculptor.chisel.SidechainPrune._calls,
            [ ( (), { "pruning": algs, "applicable": sculptor.MMTS } ) ]
            )


    def test_get_bfactor_predict_step(self):

        params = Fake()
        params.use = object()
        params.minimum = object()
        algs = object()
        sculptor.get_algorithms = Return( values = [ algs ] )
        res = object()
        sculptor.chisel.BfactorPrediction = Return( values = [ res ] )
        self.assertEqual(
            sculptor.get_bfactor_predict_step( params = params ),
            res
            )
        self.assertEqual(
            sculptor.get_algorithms._calls,
            [
                ( (), {
                    "use": params.use,
                    "params": params,
                    "factory": sculptor.get_bfactor_prediction_algorithm,
                    } ),
                ]
            )
        self.assertEqual(
            sculptor.chisel.BfactorPrediction._calls,
            [ ( (), { "predictors": algs, "minimum": params.minimum, "applicable": sculptor.MMTS } ) ]
            )


    def test_get_rename_step(self):

        ( params, compl, res ) = ( Fake(), object(), object() )
        params.completion = [ "foo" ]
        params.foo = object()
        sculptor.get_residue_completion_algorithm = Return( values = [ compl ] )
        sculptor.chisel.ResidueRename = Return( values = [ res ] )
        self.assertEqual( sculptor.get_rename_step( params = params ), res )
        self.assertEqual(
            sculptor.get_residue_completion_algorithm._calls,
            [ ( (), { "name": "foo", "params": params.foo } ) ]
            )
        self.assertEqual(
            sculptor.chisel.ResidueRename._calls,
            [ ( (), { "completion": [ compl ], "applicable": sculptor.MMTS } ) ]
            )


    def test_get_renumber_step(self):

        ( params, res1, res2 ) = ( Fake(), object(), object() )
        sculptor.chisel.ResidueRenumber.Target = Return( values = [ res1 ] )
        params.use = "target"
        params.start = object()
        self.assertEqual( sculptor.get_renumber_step( params = params ), res1 )
        self.assertEqual(
            sculptor.chisel.ResidueRenumber.Target._calls,
            [ ( (), { "start": params.start, "applicable": sculptor.MMTS } ) ]
            )
        sculptor.chisel.ResidueRenumber.Model = Return( values = [ res2 ] )
        params.use = "model"
        self.assertEqual( sculptor.get_renumber_step( params = params ), res2 )
        self.assertEqual(
            sculptor.chisel.ResidueRenumber.Model._calls,
            [ ( (), { "start": params.start, "applicable": sculptor.MMTS } ) ]
            )


    def test_get_hetero_step(self):

        ( params, res ) = ( Fake(), object() )
        sculptor.chisel.CleanUnknown = Return( values = [ res ] )
        params.keep = [ "foo,bar" ]
        self.assertEqual( sculptor.get_hetero_step( params = params ), res )
        self.assertEqual(
            sculptor.chisel.CleanUnknown._calls,
            [ ( (), { "known": [ "foo", "bar" ], "applicable": sculptor.HETEROS } ) ]
            )


    def test_get_workflow(self):

        params = Fake()
        params.deletion = Fake()
        params.deletion.use = object()
        params.polishing = Fake()
        params.polishing.use = object
        params.pruning = Fake()
        params.pruning.use = object()
        params.bfactor = Fake()
        params.bfactor.use = object()
        params.rename = Fake()
        params.rename.use = object()
        params.renumber = Fake()
        params.renumber.use = object()
        params.hetero = Fake()
        params.hetero.use = object()

        ( mc, sc, bf, ra, ru, he ) = ( object(), object(), object(), object(), object(), object() )
        sculptor.get_mainchain_step = Return( values = [ mc ] )
        sculptor.get_sidechain_prune_step = Return( values = [ sc ] )
        sculptor.get_bfactor_predict_step = Return( values = [ bf ] )
        sculptor.get_rename_step = Return( values = [ ra ] )
        sculptor.get_renumber_step = Return( values = [ ru ] )
        sculptor.get_hetero_step = Return( values = [ he ] )

        self.assertEqual(
            sculptor.get_workflow( params = params ),
            [ mc, sc, bf, ru, ra, he ]
            )
        self.assertEqual(
            sculptor.get_mainchain_step._calls,
            [ ( (), { "deletion": params.deletion, "polishing": params.polishing } ) ]
            )
        self.assertEqual(
            sculptor.get_sidechain_prune_step._calls,
            [ ( (), { "params": params.pruning } ) ]
            )
        self.assertEqual(
            sculptor.get_bfactor_predict_step._calls,
            [ ( (), { "params": params.bfactor } ) ]
            )
        self.assertEqual(
            sculptor.get_rename_step._calls,
            [ ( (), { "params": params.rename } ) ]
            )
        self.assertEqual(
            sculptor.get_renumber_step._calls,
            [ ( (), { "params": params.renumber } ) ]
            )
        self.assertEqual(
            sculptor.get_hetero_step._calls,
            [ ( (), { "params": params.hetero } ) ]
            )


suite_sculptor = unittest.TestLoader().loadTestsFromTestCase(
    TestSculptor
    )
suite_factory = unittest.TestLoader().loadTestsFromTestCase(
    TestFactory
    )
suite_workflow_step_factory = unittest.TestLoader().loadTestsFromTestCase(
    TestWorkflowStepFactory
    )

alltests = unittest.TestSuite(
    [
        suite_factory,
        #suite_sculptor,
        #suite_workflow_step_factory,
        ]
    )


def load_tests(loader, tests, pattern):

    return alltests


if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
