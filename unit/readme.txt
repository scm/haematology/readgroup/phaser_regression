Running unit tests

1. Individual
phenix.python path-to-unit-test-file

Runs all unit tests in the file

2. All
phaser_regression.unit

Runs all unit tests located in $PHENIX/phaser_regression/unit
