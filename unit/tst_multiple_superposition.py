from phaser import multiple_superposition
from phaser.pipeline.scaffolding import Fake, Return, IterableCompareTestCase

from scitbx.array_family import flex
from scitbx import matrix

import iotbx.pdb

import libtbx.load_env

import unittest
import os.path

PDB_FILE = os.path.join(
    libtbx.env.under_dist( "phaser_regression", "data" ),
    "structure",
    "verotoxin.pdb",
    )
root = iotbx.pdb.input( PDB_FILE ).construct_hierarchy()
sel = "chain a and name ca"
sites_a = root.select( root.atom_selection_cache().selection( sel ) ).atoms().extract_xyz()
sel = "chain b and name ca"
sites_b = root.select( root.atom_selection_cache().selection( sel ) ).atoms().extract_xyz()
sel = "chain c and name ca"
sites_c = root.select( root.atom_selection_cache().selection( sel ) ).atoms().extract_xyz()
sel = "chain d and name ca"
sites_d = root.select( root.atom_selection_cache().selection( sel ) ).atoms().extract_xyz()
sel = "chain e and name ca and not ( resseq 34 and altid B )"
sites_e = root.select( root.atom_selection_cache().selection( sel ) ).atoms().extract_xyz()
count = len( sites_a )
assert len( sites_b ) == count
assert len( sites_c ) == count
assert len( sites_d ) == count
assert len( sites_e ) == count

class TestModule(IterableCompareTestCase):

    def setUp(self):

        self.ms_eigensystem = multiple_superposition.eigensystem


    def tearDown(self):

        multiple_superposition.eigensystem = self.ms_eigensystem


    def test_quaternion_to_quatmat(self):

        # Identity
        q = matrix.col( [ 0, 0, 0, 1 ] )
        self.assertEqual(
            multiple_superposition.quaternion_to_quatmat( q = q ),
            matrix.sqr( [ 1, 0, 0, 0,  0, 1, 0, 0,  0, 0, 1, 0,  0, 0, 0, 1 ] )
            )

        # 180-degree rotation
        q = matrix.col( [ 1, 1, 1, 0 ] )
        self.assertEqual(
            multiple_superposition.quaternion_to_quatmat( q = q ),
            matrix.sqr( [ 0, -1, 1, 1, 1, 0, -1, 1, -1, 1, 0, 1, -1, -1, -1, 0 ] )
            )


    def test_p_matrix_from(self):

        p = multiple_superposition.p_matrix_from(
            a_sites = sites_a,
            b_sites = sites_b,
            weights = flex.double( [ 1 ] * count )
            )
        expected = (
            -201005.181936, 36584.6089,     -31456.657083, 46386.54077,
            36584.6089,     -65584.392986,  -96731.324372, -19579.344111,
            -31456.657083,  -96731.324372,  -155712.26915, -17333.635044,
            46386.54077,    -19579.344111,  -17333.635044, 0,
            )

        self.assertIterablesAlmostEqual( p, expected, 4 )


    def test_top_quaternion(self):

        evs = ( object(), object(), object(), object(), object() )
        res = Fake()
        res.vectors = Return( values = [ evs ] )
        multiple_superposition.eigensystem = Fake()
        multiple_superposition.eigensystem.real_symmetric = Return( values = [ res ] )
        p = Fake()
        fdm = object()
        p.as_flex_double_matrix = Return( values = [ fdm ] )
        self.assertEqual(
            multiple_superposition.top_quaternion( p = p ),
            evs[0:4]
            )
        self.assertEqual( p.as_flex_double_matrix._calls, [ ( (), {} ) ] )
        self.assertEqual(
            multiple_superposition.eigensystem.real_symmetric._calls,
            [ ( ( fdm, ), {} ) ]
            )
        self.assertEqual( res.vectors._calls, [ ( (), {} ) ] )


    def test_component_arrays(self):

        v = flex.vec3_double(
            [ ( 1, 2, 3 ), ( 4, 5, 6 ), ( 7, 8, 9 ), ( 10, 11, 12 ) ]
            )
        ( a, b, c ) = multiple_superposition.component_arrays( vec3_array = v )
        self.assertEqual( list( a ), [ 1, 4, 7, 10 ] )
        self.assertEqual( list( b ), [ 2, 5, 8, 11 ] )
        self.assertEqual( list( c ), [ 3, 6, 9, 12 ] )


class TestSymmetricMatrix(unittest.TestCase):

    def create(self, dimension):

        normal = [ [ None ] * dimension for i in range( dimension ) ]
        symm = multiple_superposition.SymmetricMatrix( dimension = dimension )

        for i in range( dimension ):
            for j in range( i, dimension ):
                value = object()
                symm.set( left = j, right = i, value = value )
                normal[ j ][ i ] = value
                normal[ i ][ j ] = value

        return ( normal, symm )


    def check(self, normal, symmetric):

        dimension = symmetric.d

        for i in range( dimension ):
            for j in range( i, dimension ):
                self.assertEqual(
                    symmetric.get( left = j, right = i ),
                    symmetric.get( left = i, right = j )
                    )
                self.assertEqual(
                    symmetric.get( left = j, right = i ),
                    normal[ j ][ i ]
                    )
                self.assertEqual(
                    symmetric.get( left = j, right = i ),
                    normal[ i ][ j ]
                    )

            self.assertEqual(
                symmetric.column( index = i ), normal[ i ]
                )


    def test_dimensions(self):

        for d in [ 5, 50 ]:
            ( normal, symm ) = self.create( dimension = d )
            self.check( normal = normal, symmetric = symm )



class TestMultipleSuperposition(IterableCompareTestCase):

    def check_results(self, msup, residuals, rmsds, rotations, translations):

        for resi in residuals:
            msup.full_iteration()
            self.assertAlmostEqual( msup.residual(), resi, 8 )

        for ( i, rmsd ) in enumerate( rmsds ):
            self.assertAlmostEqual(
                msup.rmsd_between( left = 0, right = i ),
                rmsd,
                3
                )


        ( rot, tra ) = msup.transformations()
        self.assertEqual( len( rot ), msup.set_count )
        self.assertEqual( len( tra ), msup.set_count )

        for ( observed, expected ) in zip( rot, rotations ):
            self.assertIterablesAlmostEqual( observed, expected, 3 )

        for ( observed, expected ) in zip( tra, translations ):
            self.assertIterablesAlmostEqual( observed, expected, 3 )


class TestDiamondAlgorithm(TestMultipleSuperposition):


    def test_two_molecules(self):

        rotations = (
            ( 1.000,  0.000,  0.000,  0.000,  1.000,  0.000,  0.000,  0.000,  1.000 ),
            ( 0.667, -0.504, -0.548,  0.742,  0.391,  0.544, -0.060, -0.770,  0.635 ),
            )

        translations = (
            ( -3.838, -25.042, 24.313 ),
            (  2.436, -18.871, 42.497 ),
            )

        msup = multiple_superposition.DiamondAlgorithm(
            site_sets = [ sites_a, sites_b ],
            weights = flex.double( [ 1.0 ] * count )
            )

        self.check_results(
            msup = msup,
            residuals = [ 21.12417907, 21.12417907 ],
            rmsds = [ 0.0, 0.391 ],
            rotations = rotations,
            translations = translations
            )


    def test_five_molecules(self):

        rotations = (
            ( 1.000,  0.000,  0.000,  0.000,  1.000,  0.000,  0.000,  0.000,  1.000 ),
            ( 0.667, -0.504, -0.548,  0.742,  0.391,  0.544, -0.060, -0.770,  0.635 ),
            ( 0.097, -0.154, -0.983,  0.697, -0.694,  0.177, -0.710, -0.703,  0.040 ),
            ( 0.083,  0.633, -0.770, -0.074, -0.766, -0.638, -0.994,  0.110, -0.017 ),
            ( 0.625,  0.771, -0.122, -0.533,  0.308, -0.788, -0.570,  0.558,  0.604 ),
            )

        translations = (
            (  -3.838, -25.042, 24.313 ),
            (   2.438, -18.869, 42.498 ),
            (  -0.261,   2.238, 42.912 ),
            ( -14.205,   6.266, 27.633 ),
            ( -16.884, -11.285, 17.339 ),
            )

        msup = multiple_superposition.DiamondAlgorithm(
            site_sets = [ sites_a, sites_b, sites_c, sites_d, sites_e ],
            weights = flex.double( [ 1.0 ] * count )
            )

        self.check_results(
            msup = msup,
            residuals = [ 123.38642098, 123.38637532, 123.38637246, 123.38637228, 123.38637227 ],
            rmsds = [ 0.0, 0.391, 0.248, 0.243, 0.201 ],
            rotations = rotations,
            translations = translations
            )


    def test_five_molecules_with_weighting(self):

        rotations = (
            ( 1.000,  0.000,  0.000,  0.000,  1.000,  0.000,  0.000,  0.000,  1.000 ),
            ( 0.659, -0.503, -0.559,  0.749,  0.377,  0.545, -0.063, -0.778,  0.625 ),
            ( 0.083, -0.160, -0.984,  0.690, -0.703,  0.172, -0.719, -0.693,  0.052 ),
            ( 0.075,  0.637, -0.767, -0.079, -0.763, -0.642, -0.994,  0.109, -0.007 ),
            ( 0.624,  0.772, -0.121, -0.532,  0.306, -0.789, -0.572,  0.557,  0.602 ),
            )

        translations = (
            (  -3.733, -23.107,  26.337 ),
            (   2.314, -16.365,  44.856 ),
            (   0.479,   4.739,  44.891 ),
            ( -13.899,   8.282,  29.719 ),
            ( -16.710,  -9.392,  19.376 ),
            )

        msup = multiple_superposition.DiamondAlgorithm(
            site_sets = [ sites_a, sites_b, sites_c, sites_d, sites_e ],
            weights = flex.double( [ 1 ] * 34 + [ 0 ] * 35 )
            )

        self.check_results(
            msup = msup,
            residuals = [ 50.44387223, 50.44384160, 50.44383968 ],
            rmsds = [ 0.0, 0.394, 0.155, 0.206, 0.157 ],
            rotations = rotations,
            translations = translations
            )

        msup = multiple_superposition.DiamondAlgorithm(
            site_sets = [ sites_a[:34], sites_b[:34], sites_c[:34], sites_d[:34], sites_e[:34] ],
            weights = flex.double( [ 1 ] * 34 )
            )

        self.check_results(
            msup = msup,
            residuals = [ 50.44387223, 50.44384160, 50.44383968 ],
            rmsds = [ 0.0, 0.394, 0.155, 0.206, 0.157 ],
            rotations = rotations,
            translations = translations
            )


class TestWangSnoeyinkAlgorithm(TestMultipleSuperposition):

    def test_average_structure(self):

        sites = flex.vec3_double(
            [ ( 0, 0, 0 ), ( 1, 0, 0 ), ( 0, 1, 0 ), ( 0, 0, 1 ), ( 1, 1, 0 ),
                ( 1, 0, 1 ), ( 0, 1, 1 ), ( 1, 1, 1 ) ]
            )
        msup = multiple_superposition.WangSnoeyinkAlgorithm(
            site_sets = [ sites, sites ],
            selections = [
                flex.bool( [ True ] * 8 ),
                flex.bool( [ True ] * 8 ),
                ],
            weights = flex.double( [ 1 ] * 8 )
            )
        self.assertIterablesAlmostEqual( msup.norms, [ 2.0 ] * 8, 3 )
        self.assertIterablesAlmostEqual( msup.sums, [ 8.0 ] * 2, 3 )
        self.assertCoordinatesAlmostEqual(
            msup.centroids,
            [ ( 0.5, 0.5, 0.5 ) ] * 2,
            8
            )
        self.assertAlmostEqual(
            ( msup.average_structure() - ( sites - ( 0.5, 0.5, 0.5 ) ) ).sum_sq(),
            0,
            3
            )


    def test_pair(self):

        rotations = (
            ( 0.915,  0.401,  0.043, -0.343,  0.830, -0.440, -0.212,  0.388,  0.897 ),
            ( 0.906, -0.337, -0.256,  0.413,  0.837,  0.360,  0.093, -0.432,  0.897 ),
            )

        translations = (
            ( -12.521, -30.162, 12.900 ),
            (  -3.528, -35.202, 30.272 ),
            )

        msup = multiple_superposition.WangSnoeyinkAlgorithm(
            site_sets = [ sites_a, sites_b ],
            selections = [
                flex.bool( [ True ] * len( sites_a ) ),
                flex.bool( [ True ] * len( sites_b ) ),
                ],
            weights = flex.double( [ 1 ] * len( sites_a ) )
            )

        self.check_results(
            msup = msup,
            residuals = [ 0.07655623, 0.07653688, 0.07653688 ],
            rmsds = [ 0.0, 0.391 ],
            rotations = rotations,
            translations = translations
            )


    def test_five_molecules(self):

        rotations = (
            ( 0.410,  0.756, -0.511, -0.591, -0.207, -0.780, -0.695,  0.622,  0.362 ),
            ( 0.865,  0.482, -0.138, -0.501,  0.817, -0.284, -0.024,  0.315,  0.949 ),
            ( 0.929, -0.229, -0.289,  0.352,  0.782,  0.513,  0.109, -0.579,  0.808 ),
            ( 0.486, -0.376, -0.789,  0.741, -0.302,  0.600, -0.463, -0.876,  0.132 ),
            ( 0.144,  0.264, -0.954,  0.185, -0.954, -0.236, -0.972, -0.142, -0.186 ),
            )

        translations = (
            ( -32.916, -11.519, -4.102 ),
            ( -34.968, -30.683,  1.956 ),
            ( -20.338, -33.770, 17.102 ),
            ( -15.205, -14.447, 23.763 ),
            ( -24.307,  -1.212, 10.989 ),
            )


        msup = multiple_superposition.WangSnoeyinkAlgorithm(
            site_sets = [ sites_a, sites_b, sites_c, sites_d, sites_e ],
            selections = [
                flex.bool( [ True ] * len( sites_a ) ),
                flex.bool( [ True ] * len( sites_b ) ),
                flex.bool( [ True ] * len( sites_b ) ),
                flex.bool( [ True ] * len( sites_b ) ),
                flex.bool( [ True ] * len( sites_b ) ),
                ],
            weights = flex.double( [ 1 ] * len( sites_a ) )
            )

        self.check_results(
            msup = msup,
            residuals = [ 0.18927133, 0.17882083, 0.17882083 ],
            rmsds = [ 0.0, 0.391, 0.248, 0.243, 0.201 ],
            rotations = rotations,
            translations = translations
            )


    def test_five_molecules_with_weighting(self):

        rotations = (
            ( 0.614,  0.658, -0.436, -0.214, -0.392, -0.895, -0.760,  0.642, -0.100 ),
            ( 0.926,  0.278, -0.257, -0.378,  0.656, -0.653, -0.013,  0.702,  0.712 ),
            ( 0.818, -0.259, -0.513,  0.355,  0.930,  0.097,  0.452, -0.261,  0.853 ),
            ( 0.427, -0.159, -0.890,  0.904,  0.066,  0.422, -0.009, -0.985,  0.171 ),
            ( 0.282,  0.433, -0.856,  0.587, -0.784, -0.203, -0.759, -0.445, -0.475 ),
            )

        translations = (
            ( -28.975, -13.694, -14.643 ),
            ( -28.891, -34.200, -16.761 ),
            ( -16.139, -42.121,  -1.814 ),
            ( -16.026, -26.863,  12.906 ),
            ( -24.881, -10.074,   4.724 ),
            )

        msup = multiple_superposition.WangSnoeyinkAlgorithm(
            site_sets = [ sites_a, sites_b, sites_c, sites_d, sites_e ],
            selections = [
                flex.bool( [ True ] * len( sites_a ) ),
                flex.bool( [ True ] * len( sites_b ) ),
                flex.bool( [ True ] * len( sites_b ) ),
                flex.bool( [ True ] * len( sites_b ) ),
                flex.bool( [ True ] * len( sites_b ) ),
                ],
            weights = flex.double( [ 1 ] * 34 + [ 0 ] * 35 )
            )

        self.check_results(
            msup = msup,
            residuals = [ 0.16059005, 0.14836424, 0.14836423 ],
            rmsds = [ 0.0, 0.394, 0.155, 0.206, 0.157 ],
            rotations = rotations,
            translations = translations
            )

        msup = multiple_superposition.WangSnoeyinkAlgorithm(
            site_sets = [ sites_a[:34], sites_b[:34], sites_c[:34], sites_d[:34], sites_e[:34] ],
            selections = [
                flex.bool( [ True ] * 34 ),
                flex.bool( [ True ] * 34 ),
                flex.bool( [ True ] * 34 ),
                flex.bool( [ True ] * 34 ),
                flex.bool( [ True ] * 34 ),
                ],
            weights = flex.double( [ 1 ] * 34 )
            )

        self.check_results(
            msup = msup,
            residuals = [ 0.16059005, 0.14836424, 0.14836423 ],
            rmsds = [ 0.0, 0.394, 0.155, 0.206, 0.157 ],
            rotations = rotations,
            translations = translations
            )


    def test_not_identical_extent(self):

        rotations = (
            ( 1.000,  0.000,  0.000,  0.000,  1.000,  0.000,  0.000,  0.000,  1.000 ),
            ( 1.000,  0.000,  0.000,  0.000,  1.000,  0.000,  0.000,  0.000,  1.000 ),
            ( 1.000,  0.000,  0.000,  0.000,  1.000,  0.000,  0.000,  0.000,  1.000 ),
            )

        translations = (
            (  -3.838, -25.042, 24.313 ),
            (  -3.838, -25.042, 24.314 ),
            (  -3.838, -25.042, 24.313 ),
            )

        msup = multiple_superposition.WangSnoeyinkAlgorithm(
            site_sets = [ sites_a, sites_a, sites_a ],
            selections = [
                flex.bool( [ True ] * len( sites_a ) ),
                flex.bool( [ True ] * 34 + [ False ] * 35 ),
                flex.bool( [ False ] * 34 + [ True ] * 35 ),
                ],
            weights = flex.double( [ 1 ] * len( sites_a ) )
            )
        self.check_results(
            msup = msup,
            residuals = [
                0.47649628, 0.11912407, 0.02978102, 0.00744525, 0.00186131,
                0.00046533, 0.00011633, 0.00002908, 0.00000727, 0.00000182,
                0.00000045, 0.00000011, 0.00000003, 0.00000001, 0.0
                ],
            rmsds = [ 0.0, 0.0, 0.0 ],
            rotations = rotations,
            translations = translations
            )


class TestWeightingSchemes(TestMultipleSuperposition):

    def test_unit_weight(self):

        scheme = multiple_superposition.UnitWeightScheme()
        self.assertIterablesAlmostEqual(
            scheme.for_difference_squares( [ 1, 2, 3, 4 ] ),
            [ 1.0, 1.0, 1.0, 1.0 ],
            3
            )


    def testRobustResistantScheme(self):

        scheme = multiple_superposition.RobustResistantWeightScheme(
            critical_value_square = 9
            )

        self.assertIterablesAlmostEqual(
            scheme.for_difference_squares(
                flex.double( [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ] )
                ),
            [ 1.0, 0.790, 0.605, 0.444, 0.309, 0.198, 0.111, 0.049, 0.012, 0, 0 ],
            3
            )



suite_module = unittest.TestLoader().loadTestsFromTestCase(
    TestModule
    )
suite_symmetric_matrix = unittest.TestLoader().loadTestsFromTestCase(
    TestSymmetricMatrix
    )
suite_diamond = unittest.TestLoader().loadTestsFromTestCase(
    TestDiamondAlgorithm
    )
suite_wang_snoeyink = unittest.TestLoader().loadTestsFromTestCase(
    TestWangSnoeyinkAlgorithm
    )
suite_weighting = unittest.TestLoader().loadTestsFromTestCase(
    TestWeightingSchemes
    )
alltests = unittest.TestSuite(
    [
        suite_module,
        suite_symmetric_matrix,
        suite_diamond,
        suite_wang_snoeyink,
        suite_weighting,
        ]
    )


def load_tests(loader, tests, pattern):

    return alltests


if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
