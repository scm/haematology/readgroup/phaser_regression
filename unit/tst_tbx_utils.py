from phaser import tbx_utils

import libtbx.phil
import libtbx.load_env

import unittest
import os.path

class TestTbxUtils(unittest.TestCase):

    def test_process_command_line(self):

        phil_master = libtbx.phil.parse(
            """
            foo = None

            bar
            {
                foobar = 1
            }
            """
            )

        phaser_dir = libtbx.env.under_dist( "phaser", "phaser" )
        contents = [
            os.path.join( phaser_dir, f ) for f in os.listdir( phaser_dir )
            ]
        input_files = [ f for f in contents if os.path.isfile( f ) ][:5]
        input_phils = [ "foo=3", "foobar=5" ]
        input_unknowns = [ "bar.bar=5" ]

        ( phil_objects, files, unknowns ) = tbx_utils.process_command_line(
            phil_master = phil_master,
            args = input_files + input_phils + input_unknowns
            )

        self.assertEqual( files, input_files )
        self.assertEqual( unknowns, input_unknowns )
        self.assertEqual( len( phil_objects ), 2 )
        self.assertEqual( phil_objects[0].as_str(), "foo = 3\n" )
        self.assertEqual( phil_objects[1].as_str(), "bar.foobar = 5\n" )


    def test_group_files_on_extension(self):

        ( sorted, unknowns ) = tbx_utils.group_files_on_extension(
            files = [ "foo1.pdb", "bar.ent", "foobar.pdp", "foo.aln",
                "bar.pir", "foobar.ali" ],
            alignments = [ ".aln", ".pir", ".ali" ],
            models = [ ".pdb", ".ent" ]
            )

        self.assertEqual(
            sorted,
            {
                "alignments": [ "foo.aln", "bar.pir", "foobar.ali" ],
                "models": [ "foo1.pdb", "bar.ent" ],
                }
            )
        self.assertEqual( unknowns, [ "foobar.pdp" ] )


    def test_phil_assign(self):

        self.assertEqual( tbx_utils.phil_assign( "foo", 3 ).as_str(), "foo = 3\n" )


    def test_autoroot(self):

        root = "my_file_name"
        ext = "ext"
        label = "foo"
        self.assertEqual(
            tbx_utils.autoroot( file_name = "%s.%s" % ( root, ext ), label = label ),
            "%s_%s" % ( root, label )
            )


    def test_autoname(self):

        root = "my_file_name"
        ext1 = "ext1"
        label = "foo"
        ext2 = "ext2"
        self.assertEqual(
            tbx_utils.autoname(
                file_name = "%s.%s" % ( root, ext1 ),
                label = label,
                extension = ext2
                ),
            "%s_%s.%s" % ( root, label, ext2 )
            )



suite_tbx_utils = unittest.TestLoader().loadTestsFromTestCase(
    TestTbxUtils
    )

alltests = unittest.TestSuite(
    [
        suite_tbx_utils
        ]
    )


def load_tests(loader, tests, pattern):

    return alltests


if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
