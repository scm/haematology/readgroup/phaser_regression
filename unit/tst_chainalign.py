from phaser import chainalign
from phaser import mmtype

import libtbx.load_env

import unittest
import os.path

PDB_FILE = os.path.join(
  libtbx.env.under_dist( "phaser_regression", "data" ),
  "structure",
  "1ahq_trunc.pdb",
  )

import iotbx.pdb
ROOT = iotbx.pdb.input( PDB_FILE ).construct_hierarchy()
CHAIN = ROOT.models()[0].chains()[0]


class TestHSSFind(unittest.TestCase):

  def setUp(self):

    self.seq = "GIAVSDDCVQKFNELKLGHQHRYVTFKMNASNTEVVVEHVGGPNATYEDFKS"
    self.hssfind = chainalign.HSSFind( sequence = self.seq )


  def perform_test(self, trial, overlaps):

    self.assertEqual(
      list( p[0] for p in self.hssfind.matching_statistics( sequence = trial ) ),
      overlaps,
      )


  def test_identical(self):

    self.perform_test(
      trial = self.seq,
      overlaps = [
        52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35,
        34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17,
        16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1,
        ],
      )


  def test_single_register_error(self):

    seq = self.seq[ : 13 ] + self.seq[ 14 : ]
    self.perform_test(
      trial = seq,
      overlaps = [
        13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 38, 37, 36, 35,
        34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17,
        16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1,
        ],
      )


  def test_double_register_error(self):

    seq = self.seq[ : 13 ] + self.seq[ 14 : 35 ] + self.seq[ 36 : ]
    self.perform_test(
      trial = seq,
      overlaps = [
        13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 22, 21, 20, 19, 18, 17,
        16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 17, 16, 15, 14, 13,
        12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1,
        ],
      )


class TestEquivalent(unittest.TestCase):

  def setUp(self):

    self.compare = 1
    self.eq1 = chainalign.Equivalent( value = object(), compare = self.compare )
    self.eq2 = chainalign.Equivalent( value = object(), compare = self.compare )

  def test_equality(self):

    self.assertEqual( self.eq1, self.compare )
    self.assertEqual( self.eq1, self.eq2 )
    self.assertIsNot( self.eq1, self.eq2 )

    self.assertNotEqual( self.eq1, self.compare + 1 )
    self.assertNotEqual(
      self.eq1,
      chainalign.Equivalent( value = self.eq1.value, compare = self.compare + 1 ),
      )


  def test_hash(self):

    self.assertEqual( hash( self.eq1 ), hash( self.compare ) )
    self.assertEqual( hash( self.eq1 ), hash( self.eq2 ) )


class TestChainAligner(unittest.TestCase):

  def setUp(self):

    self.chain = CHAIN.detached_copy()


  def perform_test(self, aligner, trial, fragments, alignment):

    self.assertEqual(
      [ fragment.sequence for fragment in aligner.fragments ],
      [ list( f ) for f in fragments ],
      )

    ali = aligner.fragment_alignment( trial = trial )

    self.assertEqual( ali.identities, alignment[0] )
    self.assertEqual( ali.overlaps, alignment[1] )
    self.assertEqual( ali.residue_alignment(), alignment[2] )
    self.assertEqual( ali.iotbx_alignment().alignments[1], alignment[3] )


  def test_geometry(self):

    seq = "GIAVSDDCVQKFNELKLGHQHRYVTFKMNASNTEVVVEHVGGPNATYEDFKS"
    rgs = self.chain.residue_groups()
    self.perform_test(
      aligner = chainalign.ChainAligner.Geometry( rgs = rgs, mmt = mmtype.PROTEIN ),
      trial = chainalign.Trial( gapped = seq ),
      fragments = [ seq ],
      alignment = ( 52, 52, rgs, seq ),
      )

    for posi in [ 13, 35 ]:
      del rgs[posi]

    self.perform_test(
      aligner = chainalign.ChainAligner.Geometry( rgs = rgs, mmt = mmtype.PROTEIN ),
      trial = chainalign.Trial( gapped = seq ),
      fragments = [ "GIAVSDDCVQKFN", "LKLGHQHRYVTFKMNASNTEVV", "EHVGGPNATYEDFKS" ],
      alignment = (
        50,
        50,
        rgs[:13] + [ None ] + rgs[ 13 : 35 ] + [ None ] + rgs[ 35 : ],
        "GIAVSDDCVQKFN-LKLGHQHRYVTFKMNASNTEVV-EHVGGPNATYEDFKS",
        ),
      )

  def test_numbering(self):

    seq = "GIAVSDDCVQKFNELKLGHQHRYVTFKMNASNTEVVVEHVGGPNATYEDFKS"
    rgs = self.chain.residue_groups()
    self.perform_test(
      aligner = chainalign.ChainAligner.Numbering( rgs = rgs, mmt = mmtype.PROTEIN ),
      trial = chainalign.Trial( gapped = seq ),
      fragments = [ seq ],
      alignment = ( 52, 52, rgs, seq ),
      )

    for posi in [ 13, 36 ]:
      for rg in rgs[posi:]:
        rg.resseq = rg.resseq_as_int() + 10

    self.perform_test(
      aligner = chainalign.ChainAligner.Numbering( rgs = rgs, mmt = mmtype.PROTEIN ),
      trial = chainalign.Trial( gapped = seq ),
      fragments = [ "GIAVSDDCVQKFN", "ELKLGHQHRYVTFKMNASNTEVV", "VEHVGGPNATYEDFKS" ],
      alignment = ( 52, 52, rgs, seq ),
      )

    for posi in [ 13, 35 ]:
      del rgs[posi]

    self.perform_test(
      aligner = chainalign.ChainAligner.Numbering( rgs = rgs, mmt = mmtype.PROTEIN ),
      trial = chainalign.Trial( gapped = seq ),
      fragments = [ "GIAVSDDCVQKFN", "LKLGHQHRYVTFKMNASNTEVV", "EHVGGPNATYEDFKS" ],
      alignment = (
        50,
        50,
        rgs[:13] + [ None ] + rgs[ 13 : 35 ] + [ None ] + rgs[ 35 : ],
        "GIAVSDDCVQKFN-LKLGHQHRYVTFKMNASNTEVV-EHVGGPNATYEDFKS",
        ),
      )


class TestChainAlignmentAlgorithm(unittest.TestCase):

  def setUp(self):

    chain = CHAIN.detached_copy()
    self.rgs = chain.residue_groups()
    self.seq = mmtype.PROTEIN.one_letter_sequence_for_rgs( rgs = self.rgs )


  def get_base_aligner(self):

    return chainalign.ChainAlignmentAlgorithm.from_params(
      rgs = self.rgs,
      mmt = mmtype.PROTEIN,
      annotation = "Blah",
      params = chainalign.ChainAlignmentAlgorithm.parsed_master_phil().extract()
      )


  def identical_sequence_test(self, aligner):

    result = aligner( trial = chainalign.Trial( gapped = self.seq ) )
    self.assertEqual( result.identities, 52 )
    self.assertEqual( result.residue_alignment(), self.rgs )


  def short_sequence_test(self, aligner):

    seq = self.seq[ 10 : 19 ]
    result = aligner( trial = chainalign.Trial( gapped = seq ) )
    self.assertEqual( result.identities, len( seq ) )
    self.assertEqual( result.residue_alignment(), self.rgs[ 10 : 19 ] )


  def mismatching_sequence_test(self, aligner):

    matching = self.seq[ 5 : 25 ]
    seq = "X" * 10 + matching + "X" * 10
    result = aligner( trial = chainalign.Trial( gapped = seq ) )
    self.assertEqual( result.identities, len( matching ) )
    self.assertEqual(
      result.residue_alignment(),
      [ None ] * 5 + self.rgs[ : 35 ],
      )


  def test_base_aligner(self):

    aligner = self.get_base_aligner()
    self.identical_sequence_test( aligner = aligner )
    self.short_sequence_test( aligner = aligner )
    self.mismatching_sequence_test( aligner = aligner )


  def test_overlap_prefiltered_aligner(self):

    base = self.get_base_aligner()
    aligner = chainalign.OverlapPrefilter( aligner = base, threshold = 5 )
    self.identical_sequence_test( aligner = aligner )
    self.short_sequence_test( aligner = aligner )
    self.mismatching_sequence_test( aligner = aligner )

    aligner = chainalign.OverlapPrefilter.from_params(
      aligner = base,
      params = chainalign.OverlapPrefilter.parsed_master_phil().extract()
      )
    self.identical_sequence_test( aligner = aligner )
    self.assertRaises(
      chainalign.ChainAlignFailure,
      self.short_sequence_test,
      aligner = aligner,
      )
    self.mismatching_sequence_test( aligner = aligner )

    aligner = chainalign.OverlapPrefilter( aligner = base, threshold = 30 )
    self.identical_sequence_test( aligner = aligner )
    self.assertRaises(
      chainalign.ChainAlignFailure,
      self.short_sequence_test,
      aligner = aligner,
      )
    self.assertRaises(
      chainalign.ChainAlignFailure,
      self.mismatching_sequence_test,
      aligner = aligner,
      )


  def test_identity_postfiltered_aligner(self):

    base = self.get_base_aligner()
    aligner = chainalign.IdentityPostFilter.from_params(
      aligner = base,
      params = chainalign.IdentityPostFilter.parsed_master_phil().extract(),
      )
    self.identical_sequence_test( aligner = aligner )
    self.short_sequence_test( aligner = aligner )
    self.assertRaises(
      chainalign.ChainAlignFailure,
      self.mismatching_sequence_test,
      aligner = aligner,
      )

    aligner = chainalign.IdentityPostFilter( aligner = base, threshold = 0.5 )
    self.identical_sequence_test( aligner = aligner )
    self.short_sequence_test( aligner = aligner )
    self.mismatching_sequence_test( aligner = aligner )


  def test_overlap_prefiltered_identity_postfiltered_aligner(self):

    base = self.get_base_aligner()
    aligner = chainalign.IdentityPostFilter(
      aligner = chainalign.OverlapPrefilter( aligner = base, threshold = 5 ),
      threshold = 0.5,
      )
    self.identical_sequence_test( aligner = aligner )
    self.short_sequence_test( aligner = aligner )
    self.mismatching_sequence_test( aligner = aligner )

    aligner = chainalign.IdentityPostFilter(
      aligner = chainalign.OverlapPrefilter( aligner = base, threshold = 10 ),
      threshold = 0.5,
      )
    self.identical_sequence_test( aligner = aligner )
    self.assertRaises(
      chainalign.ChainAlignFailure,
      self.short_sequence_test,
      aligner = aligner,
      )
    self.mismatching_sequence_test( aligner = aligner )

    aligner = chainalign.IdentityPostFilter(
      aligner = chainalign.OverlapPrefilter( aligner = base, threshold = 5 ),
      threshold = 0.8,
      )
    self.identical_sequence_test( aligner = aligner )
    self.short_sequence_test( aligner = aligner )
    self.assertRaises(
      chainalign.ChainAlignFailure,
      self.mismatching_sequence_test,
      aligner = aligner,
      )

    aligner = chainalign.IdentityPostFilter(
      aligner = chainalign.OverlapPrefilter( aligner = base, threshold = 10 ),
      threshold = 0.8,
      )
    self.identical_sequence_test( aligner = aligner )
    self.assertRaises(
      chainalign.ChainAlignFailure,
      self.short_sequence_test,
      aligner = aligner,
      )
    self.assertRaises(
      chainalign.ChainAlignFailure,
      self.mismatching_sequence_test,
      aligner = aligner,
      )



suite_hssfind = unittest.TestLoader().loadTestsFromTestCase(
  TestHSSFind
  )
suite_equivalent = unittest.TestLoader().loadTestsFromTestCase(
  TestEquivalent
  )
suite_chain_aligner = unittest.TestLoader().loadTestsFromTestCase(
  TestChainAligner
  )
suite_chain_alignment_algorithm = unittest.TestLoader().loadTestsFromTestCase(
  TestChainAlignmentAlgorithm
  )

alltests = unittest.TestSuite(
  [
    suite_hssfind,
    suite_equivalent,
    suite_chain_aligner,
    suite_chain_alignment_algorithm,
    ]
  )


def load_tests(loader, tests, pattern):

    return alltests


if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
