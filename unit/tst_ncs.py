from phaser.ncs import *
from phaser.pipeline.scaffolding import IterableCompareTestCase

import scitbx.math
import iotbx.pdb.hierarchy
from scitbx.array_family import flex
import libtbx.load_env

import unittest
import math
import os.path

IDEAL_D4 = (
    ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ),      # A->A
    ( 0, 1, 0, -1, 0, 0, 0, 0, 1 ),     # A->B
    ( -1, 0, 0, 0, -1, 0, 0, 0, 1 ),    # A->C
    ( 0, -1, 0, 1, 0, 0, 0, 0, 1 ),     # A->D
    ( 0, 1, 0, 1, 0, 0, 0, 0, -1 ),     # A->E
    ( 1, 0, 0, 0, -1, 0, 0,0, -1 ),     # A->F
    ( 0, -1, 0, -1, 0, 0, 0, 0, -1 ),   # A->G
    ( -1, 0, 0, 0, 1, 0, 0, 0, -1 ),    # A->H
    )

D4_R = (
    # EULER ( 0 0 0 )
    ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ),
    # Euler ( -89.9359 70.5488 150.003 ) A->C
    (
        0.49963169392265078, -0.86623729866989674,  0.0010549033427916883,
        0.28895767617244938,  0.16551783078558660, -0.9429248692621548000,
        0.81662208628532718,  0.47141997208976411,  0.3330038710073586800,
        ),
    # Euler ( -30.0387 109.375 -149.94 ) A->E
    (
        -0.002181699360786262, -0.57711292989520013,  0.81666143923640533,
        -0.577361355425907050, -0.66606871270912837, -0.47223546585444431,
         0.816485826851059640, -0.47253903129559827, -0.33174954175335203,
         ),
    # Euler ( 29.9504 70.48 -89.9739 ) A->G
    (
         0.49938194009454856,    0.28928722844180843,    0.81665817657564765,
        -0.86638184051065781,    0.16721205699723679,    0.47055566559985007,
        -0.00042934923985563843,-0.94252481529942211,    0.33413588284412143
        ),
    # Euler ( 80.7154 179.857 140.445 ) A->B
    (
        -0.50408189872536291,   0.86365576315619130,    0.0004026717394422718,
         0.86365371324903850,   0.50407955386723824,    0.0024631208158550875,
         0.0019243098971902209, 0.0015893835606359647, -0.99999688544080811,
         ),
    # Euler ( -149.833 109.542 -30.1141 ) A->D
    (
        -0.0019655643571881401,  0.57979154431591695,   -0.8147624817678597,
         0.57917710004622081,   -0.66353639020105659,   -0.47357506866493848,
        -0.81519937643426088,   -0.47282261369219153,   -0.33449776178993618,
        ),
    # Euler ( 150.356 70.5751 29.9183 ) A->F
    (
        -0.49721771496481515,   -0.2845323464338177,    -0.81964375661505151,
        -0.29091192335276811,   -0.83533802688486702,    0.46645539303484357,
        -0.81740124588622998,    0.47037402635877246,    0.33257101279377288,
        ),
    # Euler ( 90.3118 109.379 90.0007 ) A->H
    (
        -0.99998521468579427,   -0.0017934923379470881, -0.0051335947482896403,
        -0.0054378559173105445,  0.33181055359099459,    0.94333036962066497,
         1.1525125274350925e-05, 0.94334433793332928,   -0.33181540042062552,
        ),
    )
D4_T = (
    ( 0, 0, 0 ),
    (  0.023650,  0.4938720,  0.328644 ),
    ( -0.399459,  0.2240120,  0.649999 ),
    ( -0.363914, -0.2147110,  0.310794 ),
    ( -0.708530,  0.3265810,  0.473547 ),
    ( -0.293822,  0.5968100,  0.145836 ),
    ( -0.313503,  0.0960579, -0.176907 ),
    ( -0.710493, -0.0951503,  0.161775 ),
    )

OP_R = (
    ( 0, -1, 0, 1, 0, 0, 0, 0, 1 ),
    )
OP_T = (
    ( 0, 0, 0 ),
    )

RANDOM1 = (
    ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ),
    (
         0.60543578044781787,    0.2962814140246251,     0.73869130186913023,
        -0.61687517448189899,   -0.41177240261820319,    0.67075219533741304,
         0.50290410105851047,   -0.8617777045999746,    -0.066533096973840536,
        ),
    (
         0.96801182185197365,   -0.24153293778404869,   -0.067933443312036476,
        -0.022756058003764599,   0.1851226139747004,    -0.98245192229406497,
         0.24987051562549772,    0.95257097255825562,    0.17370454127761589,
        ),

    )
RANDOM2 = (
    ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ),
    (
         0.33867162545458113,    0.79274041313277777,    0.50681768664681059,
        -0.92203481743803883,    0.3869541017548605,     0.010877433846797317,
        -0.18749220128835842,   -0.47098743138343074,    0.86198463671627223,
        ),
    (
         0.99876082220496631,   -0.026212551925286268,  -0.042305107847915011,
         0.010672502100423717,   0.94308502941353911,   -0.3323803920133987,
         0.04860985216369209,    0.33151701225974556,    0.94219613290174131,
        ),
    (
         0.37143083203343186,   -0.6990357248684953,     0.6110549831009725,
         0.60740572199436071,    0.68070828756138024,    0.40950520892140407,
        -0.70220896170830949,    0.21905543271699665,    0.67743434478457576,
        ),
    )
C6_RANDOM_AXIS = (
    ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ),
    (
         0.85066535712643709,   -0.2099692805307474,    -0.48195575670215995,
         0.5081462995566004,     0.56338631181286503,    0.65144700621679963,
         0.13474341702615766,   -0.79906743451056594,    0.5859483310606981,
         ),
    (
         0.55199607137931117,    0.088207738495105534,  -0.82916809637816236,
         0.80632331858245354,   -0.30984106456140498,    0.50382657792303343,
        -0.21246892264984463,   -0.94668786280433237,   -0.24215500681790578,
        ),
    (
         0.40266142850574815,    0.59635403805170595,   -0.69442467935200491,
         0.59635403805170628,   -0.74645475274854023,   -0.29524085658753235,
        -0.69442467935200469,   -0.29524085658753291,   -0.65620667575720792,
        ),
    (
         0.55199607137931106,    0.80632331858245354,   -0.21246892264984496,
         0.088207738495105881,  -0.30984106456140548,   -0.94668786280433215,
        -0.82916809637816247,    0.50382657792303309,   -0.24215500681790625,
        ),
    (
         0.85066535712643698,   0.50814629955660084,     0.13474341702615752,
        -0.2099692805307474,    0.56338631181286458,    -0.79906743451056628,
        -0.48195575670216034,   0.65144700621679985,    0.58594833106069777,
        ),
    )
TOL_ID_ANG = 1E-7
TOL_ID_SPA = 1E-10
TOL_AP_ANG = 0.02
TOL_AP_SPA = 0.20
ID_PREC = 10
PREC_EULER = 8
PREC_ORTH = 8

LYSO_PDB = os.path.join(
    libtbx.env.under_dist( "phaser_regression", "data" ),
    "regression",
    "1fkq_prot.pdb"
    )
LYSO_ROOT = iotbx.pdb.input( LYSO_PDB ).construct_hierarchy()
LYSO_CHAIN = LYSO_ROOT.models()[0].chains()[0]


def length(vector):

    return math.sqrt( sum( [ v*v for v in vector ] ) )


class TestOperator(IterableCompareTestCase):

    def setUp(self):

        self.op1 = Operator( rotation = D4_R[1], translation = D4_T[1] )
        self.op2 = Operator( translation = D4_T[2], rotation = D4_R[2] )
        self.op3 = Operator( OP_R[0], OP_T[0] )


    def test_get_rotation(self):

        self.assertIterablesAlmostEqual( self.op1.get_rotation(), D4_R[1], ID_PREC )
        self.assertIterablesAlmostEqual( self.op2.get_rotation(), D4_R[2], ID_PREC )
        self.assertEqual( self.op3.get_rotation(), OP_R[0] )


    def test_get_translation(self):

        self.assertIterablesAlmostEqual( self.op1.get_translation(), D4_T[1], ID_PREC )
        self.assertIterablesAlmostEqual( self.op2.get_translation(), D4_T[2], ID_PREC )
        self.assertEqual( self.op3.get_translation(), OP_T[0] )


    def test_unity(self):

        self.assertEqual( Operator.unity.get_rotation(), ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ) )
        self.assertEqual( Operator.unity.get_translation(), ( 0, 0, 0 ) )


    def test__str__(self):

        self.assertEqual(
            str( self.op1 ),
              "ROTA 1:   0.4996 -0.8662  0.0011\n"
            + "ROTA 2:   0.2890  0.1655 -0.9429\n"
            + "ROTA 3:   0.8166  0.4714  0.3330\n"
            + "TRANS:    0.0237  0.4939  0.3286"
            )


    def test_rotation_angle_radian(self):

        self.assertAlmostEqual(
            self.op1.rotation_angle_radian(),
            scitbx.math.r3_rotation_axis_and_angle_from_matrix( self.op1.get_rotation() ).angle(),
            ID_PREC
            )
        self.assertAlmostEqual(
            self.op2.rotation_angle_radian(),
            abs( scitbx.math.r3_rotation_axis_and_angle_from_matrix( self.op2.get_rotation() ).angle() ),
            ID_PREC
            )
        self.assertAlmostEqual(
            self.op3.rotation_angle_radian(),
            scitbx.math.r3_rotation_axis_and_angle_from_matrix( self.op3.get_rotation() ).angle(),
            ID_PREC
            )


    def test_displacement(self):

        self.assertAlmostEqual(
            self.op1.displacement(),
            length( self.op1.get_translation() ),
            ID_PREC
            )
        self.assertAlmostEqual(
            self.op2.displacement(),
            length( self.op2.get_translation() ),
            ID_PREC
            )
        self.assertEqual( self.op3.displacement(), length( self.op3.get_translation() ) )


    def test__mul__(self):

        product = self.op3 * self.op3
        self.assertEqual(
            product.get_rotation(), ( -1, 0, 0, 0, -1, 0, 0, 0, 1 )
            )
        self.assertEqual(
            product.get_translation(), ( 0, 0, 0 )
            )
        product *= self.op3
        self.assertEqual(
            product.get_rotation(), ( 0, 1, 0, -1, 0, 0, 0, 0, 1 )
            )
        self.assertEqual(
            product.get_translation(), ( 0, 0, 0 )
            )
        product *= self.op3
        self.assertEqual(
            product.get_rotation(), ( 1, 0, 0, 0, 1, 0, 0, 0, 1 )
            )
        self.assertEqual(
            product.get_translation(), ( 0, 0, 0 )
            )


    def test_inverse(self):

        inv = self.op3.inverse()
        self.assertEqual(
            inv.get_rotation(), ( 0, 1, 0, -1, 0, 0, 0, 0, 1 )
            )
        self.assertEqual(
            inv.get_translation(), ( 0, 0, 0 )
            )


    def test_transform(self):

        point = ( 1, 2, 3 )
        transformed = self.op3.transform( point )
        self.assertEqual( transformed, ( -2, 1, 3 ) )
        transformed = self.op3.transform( transformed )
        self.assertEqual( transformed, ( -1, -2, 3 ) )
        transformed = self.op3.transform( transformed )
        self.assertEqual( transformed, ( 2, -1, 3 ) )
        transformed = self.op3.transform( transformed )
        self.assertEqual( transformed, ( 1, 2, 3 ) )


    def test_approx_unity(self):

        product = self.op2 * self.op2
        self.assert_(
            product.approx_unity( angular = TOL_AP_ANG, spatial = TOL_AP_SPA )
            )

        product = self.op1 * self.op1 * self.op1 * self.op1
        self.assert_(
            product.approx_unity( angular = TOL_AP_ANG, spatial = TOL_AP_SPA )
            )


    def test_approx_inverse(self):

        self.assert_(
            self.op2.approx_inverse(
                other = self.op2,
                angular = TOL_AP_ANG,
                spatial = TOL_AP_SPA
                )
            )
        self.assert_(
            self.op1.approx_inverse(
                other = self.op1 * self.op1 * self.op1,
                angular = TOL_AP_ANG,
                spatial = TOL_AP_SPA
                )
            )


    def test_approx_equals(self):

        self.assert_(
            Operator.unity.approx_equals(
                other = self.op2 * self.op2,
                angular = TOL_AP_ANG,
                spatial = TOL_AP_SPA
                )
            )
        self.assert_(
            self.op2.approx_equals(
                other = self.op1 * self.op1,
                angular = TOL_AP_ANG,
                spatial = TOL_AP_SPA
                )
            )
        self.assert_( not self.op3.approx_equals( self.op1, 0.02, 0.01 ) )


class TestSymmetry(IterableCompareTestCase):

    def setUp(self):

        self.ops = ( [ Operator( r, t ) for ( r, t ) in zip( D4_R, D4_T ) ] )
        self.symm = Symmetry(
            operators = self.ops,
            angular = TOL_AP_ANG,
            spatial = TOL_AP_SPA
            )
        self.unit = Symmetry( [], TOL_AP_ANG, TOL_AP_SPA )
        self.incomplete = Symmetry(
            [ self.ops[0], self.ops[1], self.ops[3], self.ops[4], self.ops[5], self.ops[6] ],
            TOL_AP_ANG,
            TOL_AP_SPA
            )
        self.fac1 = Symmetry( [ self.ops[0], self.ops[5] ], TOL_AP_ANG, TOL_AP_SPA )
        self.fac2 = Symmetry(
            [ self.ops[0], self.ops[1], self.ops[3] ],
            TOL_AP_ANG,
            TOL_AP_SPA
            )


    def test__str__(self):

        self.assertEqual(
            str( self.symm ),
            "NCSSymmetry consisting of %s operators\n\n" % len( self.ops )
            + "\n".join( [ "Operator %s\n%s\n" % ( index + 1, op )
                for ( index, op ) in enumerate( self.ops ) ] )
            )


    def test_get_operators(self):

        self.assertEqual( len( self.symm.get_operators() ), len( self.ops ) )

        for ( comp, ref ) in zip( self.symm.get_operators(), self.ops ):
            self.assert_( comp.approx_equals( ref, TOL_ID_ANG, TOL_ID_SPA ) )


    def test_get_euler_list(self):

        for ( op, euler ) in zip( self.symm.get_operators(), self.symm.get_euler_list() ):
            exp = scitbx.math.euler_angles_zyz_angles( op.get_rotation() )
            self.assertIterablesAlmostEqual( exp, euler, ID_PREC )


    def test_get_orth_list(self):

        for ( op, orth ) in zip( self.symm.get_operators(), self.symm.get_orth_list() ):
            self.assertIterablesAlmostEqual(
                op.get_translation(),
                orth,
                ID_PREC
                )


    def test_contains(self):

        random_rot = Operator(
            scitbx.math.r3_rotation_axis_and_angle_as_matrix(
                flex.random_double_point_on_sphere(),
                2 * TOL_AP_ANG
                ),
            ( 0, 0, 0 )
            )
        random_disp = Operator(
            Operator.unity.get_rotation(),
            ( 2 * TOL_AP_SPA * flex.vec3_double(
                [ flex.random_double_point_on_sphere() ]
                ) )[0]
            )

        for op in self.ops:
            self.assert_( self.symm.contains( operator = op ) )
            self.assert_( not self.symm.contains( op * random_rot ) )
            self.assert_( not self.symm.contains( op * random_disp ) )


    def test_equals(self):

        self.assert_( self.symm.equals( self.ops ) )
        self.assert_( self.incomplete.equals( self.incomplete.get_operators() ) )
        self.assert_( not self.incomplete.equals(
            self.incomplete.get_operators() + ( self.ops[1], )
            ) )


    def test_left_products(self):

        random = Operator(
            flex.random_double_r3_rotation_matrix(),
            ( flex.random_double() * flex.vec3_double(
                [ flex.random_double_point_on_sphere() ]
                ) )[0]
            )
        for ( orig, trans ) in zip( self.ops, self.symm.left_products( operator = random ) ):
            self.assert_(
                trans.approx_equals( random * orig, TOL_ID_ANG, TOL_ID_SPA )
                )


    def test_right_products(self):

        random = Operator(
            flex.random_double_r3_rotation_matrix(),
            ( flex.random_double() * flex.vec3_double(
                [ flex.random_double_point_on_sphere() ]
                ) )[0]
            )
        for ( orig, trans ) in zip( self.ops, self.symm.right_products( operator = random ) ):
            self.assert_(
                trans.approx_equals( orig * random, TOL_ID_ANG, TOL_ID_SPA )
                )


    def test_intersect(self):

        rands = [
            Operator(
                flex.random_double_r3_rotation_matrix(),
                flex.random_double_point_on_sphere()
                )
            for i in range( 5 )
            ]
        ins = self.ops[:3]
        ops = self.symm.intersect( rands + ins )
        self.assertEquals( len( ops ), len( ins ) )

        for ( orig, test ) in zip( ins, ops ):
            self.assert_( orig.approx_equals( test, TOL_ID_ANG, TOL_ID_SPA ) )


    def test_quotient(self):

        quotient = self.symm.quotient( divisor = self.symm )
        self.assertEqual( len( quotient ), 1 )
        self.assert_( quotient[0].approx_unity( TOL_ID_ANG, TOL_ID_SPA ) )

        quotient = self.symm.quotient( divisor = self.fac1 )
        self.assertEqual( len( quotient ), 4 )

        for ( got, exp ) in zip( quotient, [ self.ops[0], self.ops[1], self.ops[2], self.ops[3] ] ):
            self.assert_( got.approx_equals( exp, TOL_ID_ANG, TOL_ID_SPA ) )

        quotient = Symmetry(
            self.incomplete.quotient( divisor = self.fac1 ),
            TOL_AP_ANG,
            TOL_AP_SPA
            )
        self.assert_( self.fac2.equals( quotient.get_operators() ) )


    def test_direct_product(self):

        dir_prod = self.fac1.direct_product( self.fac2 )
        self.assert_( self.incomplete.equals( dir_prod ) )


    def test_factorize(self):

        factorizations = self.symm.factorize()
        self.assertEqual( len( factorizations ), 1 )
        ( outer, inner ) = factorizations[0]
        self.assert_( self.symm.equals( outer.get_operators() ) )
        self.assert_( self.unit.equals( inner.get_operators() ) )

        factorizations = self.incomplete.factorize()
        self.assertEqual( len( factorizations ), 2 )
        ( outer, inner ) = factorizations[0]
        self.assert_( self.fac1.equals( outer.get_operators() ) )
        self.assert_( self.fac2.equals( inner.get_operators() ) )
        ( outer, inner ) = factorizations[1]
        self.assert_( self.unit.equals( outer.get_operators() ) )
        self.assert_( self.incomplete.equals( inner.get_operators() ) )

        random1_ops = [ Operator( rot, ( 0, 0, 0 ) ) for rot in RANDOM1 ]
        random2_ops = [ Operator( rot, ( 0, 0, 0 ) ) for rot in RANDOM2 ]
        c6_random_ops = [ Operator( rot, ( 0, 0, 0 ) ) for rot in  C6_RANDOM_AXIS ]
        d4_z_ops = [ Operator( rot, ( 0, 0, 0 ) ) for rot in IDEAL_D4 ]
        random1_sym = Symmetry( random1_ops, TOL_ID_ANG, TOL_ID_SPA )
        random2_sym = Symmetry( random2_ops, TOL_ID_ANG, TOL_ID_SPA )
        c6_random_sym = Symmetry( c6_random_ops, TOL_ID_ANG, TOL_ID_SPA )
        d4_z_sym = Symmetry( d4_z_ops, TOL_ID_ANG, TOL_ID_SPA )

        # Random * Random
        prod_sym = Symmetry(
            random1_sym.direct_product( random2_sym ),
            TOL_ID_ANG,
            TOL_ID_SPA
            )
        fact = prod_sym.factorize()
        self.assertEqual( len( fact ), 2 )
        self.assert_( self.unit.equals( fact[0][0].get_operators() ) )
        self.assert_( prod_sym.equals( fact[0][1].get_operators() ) )
        self.assert_( random1_sym.equals( fact[1][0].get_operators() ) )
        self.assert_( random2_sym.equals( fact[1][1].get_operators() ) )

        # Group * Random
        prod_sym = Symmetry(
            self.symm.direct_product( random1_sym ),
            TOL_AP_ANG,
            TOL_AP_SPA
            )
        fact = prod_sym.factorize()
        self.assertEqual( len( fact ), 2 )
        self.assert_( self.symm.equals( fact[0][0].get_operators() ) )
        self.assert_( random1_sym.equals( fact[0][1].get_operators() ) )
        self.assert_( self.unit.equals( fact[1][0].get_operators() ) )
        self.assert_( prod_sym.equals( fact[1][1].get_operators() ) )

        # Random * Group
        prod_sym = Symmetry(
            random1_sym.direct_product( self.symm ),
            TOL_AP_ANG,
            TOL_AP_SPA
            )
        fact = prod_sym.factorize()
        self.assertEqual( len( fact ), 2 )
        self.assert_( self.unit.equals( fact[0][0].get_operators() ) )
        self.assert_( prod_sym.equals( fact[0][1].get_operators() ) )
        self.assert_( random1_sym.equals( fact[1][0].get_operators() ) )
        self.assert_( self.symm.equals( fact[1][1].get_operators() ) )

        # Group * Group
        prod_sym = Symmetry(
            d4_z_sym.direct_product( c6_random_sym ),
            TOL_ID_ANG,
            TOL_ID_SPA
            )
        fact = prod_sym.factorize()
        self.assertEqual( len( fact ), 3 )
        self.assert_( d4_z_sym.equals( fact[0][0].get_operators() ) )
        self.assert_( c6_random_sym.equals( fact[0][1].get_operators() ) )
        self.assert_( self.unit.equals( fact[1][0].get_operators() ) )
        self.assert_( prod_sym.equals( fact[1][1].get_operators() ) )
        prod_sym = Symmetry(
            c6_random_sym.direct_product( d4_z_sym ),
            TOL_ID_ANG,
            TOL_ID_SPA
            )
        fact = prod_sym.factorize()
        self.assertEqual( len( fact ), 3 )
        self.assert_( c6_random_sym.equals( fact[0][0].get_operators() ) )
        self.assert_( d4_z_sym.equals( fact[0][1].get_operators() ) )
        self.assert_( self.unit.equals( fact[1][0].get_operators() ) )
        self.assert_( prod_sym.equals( fact[1][1].get_operators() ) )


class TestAssembly(IterableCompareTestCase):

    def setUp(self):

       self.chain = iotbx.pdb.hierarchy.chain()
       self.chain.id = "A"
       self.asse = Assembly(
            reference = self.chain,
            angular = 0.02,
            spatial = 0.10
            )


    def test__str__(self):

        self.assertEqual(
            str( self.asse ),
            "NCSAssembly consisting of 1 operators\n\n"
            + "Operator 1\n%s\n" % Operator.unity
            )


    def test_get_chains(self):

        chains = self.asse.get_chains()
        self.assertEqual( len( chains ), 1 )
        self.assertEqual( chains[0].id, "A" )


    def test_insert(self):

        chain_ids = [ "B", "C" ]
        ops = [ Operator.unity, Operator( D4_R[1], D4_T[1] ) ]

        for ( cid, op ) in zip( chain_ids, ops ):
            chain = iotbx.pdb.hierarchy.chain()
            chain.id = cid
            self.asse.insert( op, chain )

        chain_ids.insert( 0, "A" )
        ops.insert( 0, Operator.unity )

        self.assertEqual( len( self.asse.get_chains() ), len( chain_ids ) )

        for ( chain, chain_id ) in zip( self.asse.get_chains(), chain_ids ):
            self.assertEqual( chain.id, chain_id )

        self.assertEqual( len( self.asse.get_operators() ), len( ops ) )

        for ( comp, ref ) in zip( self.asse.get_operators(), ops ):
            self.assert_(
                ref.approx_equals( comp, TOL_ID_ANG, TOL_ID_SPA )
                )


class TestPointGroup(IterableCompareTestCase):

    def setUp(self):

        self.ops = ( [ Operator( r, t ) for ( r, t ) in zip( D4_R, D4_T ) ] )
        self.pg = PointGroup.from_operators( self.ops, TOL_AP_ANG, TOL_AP_SPA )


    def test__str__(self):

        self.assertEqual(
            str( self.pg ),
            "PointGroup '422' consisting of %s operators\n\n" % len( self.ops )
            + "\n".join( [ "Operator %s\n%s\n" % ( index + 1, op )
                for ( index, op ) in enumerate( self.ops ) ] )
            )


    def test_1(self):

        op_sets = [
            # All single-axis
            [ self.ops[0], self.ops[1] ],
            [ self.ops[0], self.ops[3] ],

            # 7-operator
            [ self.ops[0], self.ops[1], self.ops[2], self.ops[3], self.ops[4], self.ops[5], self.ops[6] ],
            [ self.ops[0], self.ops[1], self.ops[2], self.ops[3], self.ops[4], self.ops[5], self.ops[7] ],
            [ self.ops[0], self.ops[1], self.ops[2], self.ops[3], self.ops[4], self.ops[6], self.ops[7] ],
            [ self.ops[0], self.ops[1], self.ops[2], self.ops[3], self.ops[5], self.ops[6], self.ops[7] ],
            [ self.ops[0], self.ops[1], self.ops[2], self.ops[4], self.ops[5], self.ops[6], self.ops[7] ],
            [ self.ops[0], self.ops[1], self.ops[3], self.ops[4], self.ops[5], self.ops[6], self.ops[7] ],
            [ self.ops[0], self.ops[3], self.ops[3], self.ops[4], self.ops[5], self.ops[6], self.ops[7] ],

            # 6-operator
            [ self.ops[0], self.ops[3], self.ops[4], self.ops[5], self.ops[6], self.ops[7] ],
            [ self.ops[0], self.ops[1], self.ops[4], self.ops[5], self.ops[6], self.ops[7] ],
            [ self.ops[0], self.ops[1], self.ops[2], self.ops[3], self.ops[6], self.ops[7] ],
            [ self.ops[0], self.ops[1], self.ops[2], self.ops[3], self.ops[4], self.ops[7] ],
            [ self.ops[0], self.ops[1], self.ops[2], self.ops[3], self.ops[4], self.ops[5] ],

            # 5-operator
            [ self.ops[0], self.ops[1], self.ops[2], self.ops[3], self.ops[4] ],
            ]

        for gr in op_sets:
            pg = PointGroup.from_operators( gr, TOL_AP_ANG, TOL_AP_SPA )
            self.assertEqual( len( pg.get_operators() ), 1 )
            self.assertEqual( pg.hermann_mauguin_symbol(), "1" )


    def test_2(self):

        op_sets = [
            # All single-axis
            [ self.ops[0], self.ops[2] ],
            [ self.ops[0], self.ops[4] ],
            [ self.ops[0], self.ops[5] ],
            [ self.ops[0], self.ops[6] ],
            [ self.ops[0], self.ops[7] ],

            # C2 || Z
            [ self.ops[0], self.ops[2], self.ops[4], self.ops[5], self.ops[6], self.ops[7] ],
            [ self.ops[0], self.ops[1], self.ops[2], self.ops[3], self.ops[5], self.ops[7] ],
            [ self.ops[0], self.ops[1], self.ops[2], self.ops[3], self.ops[4], self.ops[6] ],

            # C2 || Y
            [ self.ops[0], self.ops[1], self.ops[3], self.ops[4], self.ops[5], self.ops[6] ],
            [ self.ops[0], self.ops[1], self.ops[2], self.ops[4], self.ops[5], self.ops[7] ],

            # C2 || X
            [ self.ops[0], self.ops[1], self.ops[3], self.ops[4], self.ops[6], self.ops[7] ],
            [ self.ops[0], self.ops[2], self.ops[3], self.ops[4], self.ops[5], self.ops[7] ],
            ]

        for gr in op_sets:
            pg = PointGroup.from_operators( gr, TOL_AP_ANG, TOL_AP_SPA )
            self.assertEqual( len( pg.get_operators() ), 2 )
            self.assertEqual( pg.hermann_mauguin_symbol(), "2" )


    def test_ideal_2(self):

        ops = [ Operator( rot, ( 0, 0, 0 ) ) for rot in IDEAL_D4 ]

        op_sets = [
            # All single-axis
            [ ops[0], ops[2] ],
            [ ops[0], ops[4] ],
            [ ops[0], ops[5] ],
            [ ops[0], ops[6] ],
            [ ops[0], ops[7] ],

            # C2 || Z
            [ ops[0], ops[2], ops[4], ops[5], ops[6], ops[7] ],
            [ ops[0], ops[1], ops[2], ops[3], ops[5], ops[7] ],
            [ ops[0], ops[1], ops[2], ops[3], ops[4], ops[6] ],

            # C2 || Y
            [ ops[0], ops[1], ops[3], ops[4], ops[5], ops[6] ],
            [ ops[0], ops[1], ops[2], ops[4], ops[5], ops[7] ],

            # C2 || X
            [ ops[0], ops[1], ops[3], ops[4], ops[6], ops[7] ],
            [ ops[0], ops[2], ops[3], ops[4], ops[5], ops[7] ],
            ]

        for gr in op_sets:
            pg = PointGroup.from_operators( gr, TOL_AP_ANG, TOL_AP_SPA )
            self.assertEqual( len( pg.get_operators() ), 2 )
            self.assertEqual( pg.hermann_mauguin_symbol(), "2" )


    def test_222(self):

        op_sets = [
            # All single-axis
            [ self.ops[0], self.ops[2], self.ops[5], self.ops[7] ],
            [ self.ops[0], self.ops[2], self.ops[4], self.ops[6] ],
            ]

        for gr in op_sets:
            pg = PointGroup.from_operators( gr, TOL_AP_ANG, TOL_AP_SPA )
            self.assertEqual( len( pg.get_operators() ), 4 )
            self.assertEqual( pg.hermann_mauguin_symbol(), "222" )


    def test_4(self):

        op_sets = [
            # All single-axis
            [ self.ops[0], self.ops[1], self.ops[2], self.ops[3] ],
            ]

        for gr in op_sets:
            pg = PointGroup.from_operators( gr, TOL_AP_ANG, TOL_AP_SPA )
            self.assertEqual( len( pg.get_operators() ), 4 )
            self.assertEqual( pg.hermann_mauguin_symbol(), "4" )


    def test_422(self):

        pg = PointGroup.from_operators( self.ops, TOL_AP_ANG, TOL_AP_SPA )
        self.assertEqual( len( pg.get_operators() ), 8 )
        self.assertEqual( pg.hermann_mauguin_symbol(), "422" )


    def test_6(self):

        pg = PointGroup.from_operators(
            [ Operator(rot, ( 0, 0, 0 ) ) for rot in C6_RANDOM_AXIS ],
            TOL_AP_ANG,
            TOL_AP_SPA
            )
        self.assertEqual( len( pg.get_operators() ), 6 )
        self.assertEqual( pg.hermann_mauguin_symbol(), "6" )


class TestMacromoleculeType(unittest.TestCase):

    def test_protein(self):

        mmt = MacromoleculeType.protein
        self.assertEqual(
            sorted( mmt.get_backbone_atom_names() ),
            sorted( [ " CA ", " N  ", " C  ", " O  " ] )
            )
        self.assertEqual( mmt.get_type_name(), "protein" )
        self.assertEqual( mmt.get_unknown_one_letter(), "X" )
        self.assertEqual(
            mmt.get_three_to_one().items(),
            [ ( "ALA", "A" ), ( "ARG", "R" ), ( "ASN", "N" ), ( "ASP", "D" ),
                ( "CYS", "C" ), ( "GLN", "Q" ), ( "GLU", "E" ), ( "GLY", "G" ),
                ( "HIS", "H" ), ( "ILE", "I" ), ( "LEU", "L" ), ( "LYS", "K" ),
                ( "MET", "M" ), ( "MSE", "M" ), ( "PHE", "F" ), ( "PRO", "P" ),
                ( "SER", "S" ), ( "THR", "T" ), ( "TRP", "W" ), ( "TYR", "Y" ),
                ( "VAL", "V" ) ]
            )
        self.assert_(
            mmt.recognize_chain( chain = LYSO_CHAIN, confidence = 0.95 )
            )
        self.assertEqual(
            mmt.count_recognized_residues( LYSO_CHAIN.residue_groups() ),
            124
            )
        new_mmt = MacromoleculeType.get_chain_type( chain = LYSO_CHAIN )
        self.assertEqual( new_mmt.get_type_name(), mmt.get_type_name() )
        self.assertEqual(
            mmt.residue_group_sequence( LYSO_CHAIN.residue_groups() ),
            "MEQLTKCEVFQKLKDLKDYGGVSLPEWVCVAFHTSGYDTQAIVQNNDSTEYGLFQINNKIWCKDD"
            + "QNPHSRNICNISCDKFLDDDLTDDIVCAKKILDKVGINYWLAHKALCSEKLDQWLCEKL"
            )


    def test_unknown(self):

        mmt = MacromoleculeType.unknown
        self.assertEqual( list( mmt.get_backbone_atom_names() ), [] )
        self.assertEqual( mmt.get_type_name(), "unknown" )
        self.assertEqual( mmt.get_unknown_one_letter(), "X" )
        self.assertEqual( mmt.get_three_to_one().items(), [] )
        self.assert_(
            not mmt.recognize_chain( chain = LYSO_CHAIN, confidence = 0.95 )
            )
        self.assertEqual(
            mmt.count_recognized_residues(
                residue_groups = LYSO_CHAIN.residue_groups()
                ),
            0
            )
        self.assertEqual(
            mmt.residue_group_sequence( LYSO_CHAIN.residue_groups() ),
            mmt.get_unknown_one_letter() * len( LYSO_CHAIN.residue_groups() )
            )


class TestReferenceChain(unittest.TestCase):

    def setUp(self):

        self.ref = ReferenceChain(
            chain = LYSO_CHAIN,
            macromolecule_type = MacromoleculeType.get_chain_type( LYSO_CHAIN )
            )


    def test_get_macromolecule_type(self):

        self.assertEqual(
            self.ref.get_macromolecule_type().get_type_name(),
            MacromoleculeType.protein.get_type_name()
            )


    def test_determine_numbering_shift(self):

        copy = LYSO_CHAIN.detached_copy()
        shift = 225

        for rg in copy.residue_groups():
            rg.resseq = rg.resseq_as_int() + shift

        moribund = ( copy.residue_groups()[:10] + copy.residue_groups()[85:90]
            + copy.residue_groups()[113:] )

        for rg in moribund:
            copy.remove_residue_group( rg )

        ( overlap, found ) = self.ref.determine_numbering_shift( chain = copy )
        self.assertEqual(
            len( overlap ),
            75
            )
        self.assertEqual( found, shift )


class TestChainAlignment(IterableCompareTestCase):

    def setUp(self):

        self.ref = LYSO_CHAIN
        self.ali = LYSO_CHAIN.detached_copy()
        self.shift = 225
        self.coord_shift = 10

        for rg in self.ali.residue_groups():
            rg.resseq = rg.resseq_as_int() + self.shift

        sc = self.coord_shift
        for atom in self.ali.atoms():
            ( x, y, z ) = atom.xyz
            atom.set_xyz( ( x + sc, y + sc, z + sc ) )

        moribund = ( self.ali.residue_groups()[:10]
            + self.ali.residue_groups()[85:90]
            + self.ali.residue_groups()[113:] )

        for rg in moribund:
            self.ali.remove_residue_group( rg )

        self.chain_ali = ChainAlignment(
            reference = self.ref,
            aligned = self.ali,
            offset = self.shift
            )


    def test_get_sequence_identity(self):

        self.assertAlmostEqual(
            self.chain_ali.get_sequence_identity(),
            1.0,
            7
            )


    def test_get_alignment_length(self):

        self.assertEqual(
            self.chain_ali.get_alignment_length(),
            len( self.ref.residue_groups() )
            )


    def test_get_sequence_coverage(self):

        self.assertAlmostEqual(
            self.chain_ali.get_sequence_coverage(),
            float( len( self.ali.residue_groups() ) ) / len( self.ref.residue_groups() ),
            7
            )


    def test_get_overlap_length(self):

        self.assertEqual(
            self.chain_ali.get_overlap_length(),
            len( self.ali.residue_groups() )
            )


    def test_get_residue_group_alignment(self):

        alignment = self.chain_ali.get_residue_group_alignment()
        self.assertEqual( len( alignment ), len( self.ali.residue_groups() ) )

        for ( ref, ali ) in alignment:
            self.assertEqual( ref.resseq_as_int(), ali.resseq_as_int() - self.shift )
            self.assertEqual( ref.icode, ali.icode )


    def test_get_atom_alignment(self):

        alignment = self.chain_ali.get_atom_alignment()
        self.assertEqual( len( alignment ), len( self.ali.atoms() ) )
        sc = self.coord_shift

        for ( ref, ali ) in alignment:
            self.assertEqual( ref.name, ali.name )
            ( x, y, z ) = ali.xyz
            self.assertIterablesAlmostEqual(
                ref.xyz,
                ( x - sc, y - sc, z - sc ),
                14
                )


class TestEnsembleSymmetry(IterableCompareTestCase):

    def setUp(self):

        self.esym = EnsembleSymmetry()
        self.value = 0.96


    def test_get_set_min_sequence_coverage(self):

        self.esym.set_min_sequence_coverage( value = self.value )
        self.assertAlmostEqual( self.esym.get_min_sequence_coverage(), self.value, 7 )


    def test_get_set_min_sequence_identity(self):

        self.esym.set_min_sequence_identity( value = self.value )
        self.assertAlmostEqual( self.esym.get_min_sequence_identity(), self.value, 7 )


    def test_get_set_max_rmsd(self):

        self.esym.set_max_rmsd( value = self.value )
        self.assertAlmostEqual( self.esym.get_max_rmsd(), self.value, 7 )


    def test_get_set_angular_tolerance(self):

        self.esym.set_angular_tolerance( value = self.value )
        self.assertAlmostEqual( self.esym.get_angular_tolerance(), self.value, 14 )


    def test_get_set_spatial_tolerance(self):

        self.esym.set_spatial_tolerance( value = self.value )
        self.assertAlmostEqual( self.esym.get_spatial_tolerance(), self.value, 14 )


    def test_get_set_root(self):

        root = self.esym.get_root()
        self.assertEqual( len( root.atoms() ), 0 )
        self.esym.set_root( root = LYSO_ROOT )
        root = self.esym.get_root()
        self.assertEqual( len( root.atoms() ), len( LYSO_ROOT.atoms() ) )


    def test_get_assemblies(self):

        self.assertEqual( len( self.esym.get_assemblies() ), 0 )
        self.esym.set_root( root = LYSO_ROOT )
        asse = self.esym.get_assemblies()
        self.assertEqual( len( asse ), 1 )
        self.assertEqual( len( asse[0].get_operators() ), 1 )
        self.assertEqual( asse[0].get_operators()[0].rotation_angle_radian(), 0 )
        self.assertEqual( asse[0].get_operators()[0].displacement(), 0 )
        self.assertEqual( len( asse[0].get_chains() ), 1 )
        self.assertEqual( asse[0].get_chains()[0].id, LYSO_CHAIN.id )
        self.assertEqual(
            len( asse[0].get_chains()[0].atoms() ),
            len( LYSO_CHAIN.atoms() )
            )


    def test_get_point_group(self):

        self.assertEqual( self.esym.get_point_group().hermann_mauguin_symbol(), "1" )
        self.esym.set_root( root = LYSO_ROOT )
        self.assertEqual( self.esym.get_point_group().hermann_mauguin_symbol(), "1" )


    def test_get_centre_of_assembly(self):

        self.assertIterablesAlmostEqual(
            self.esym.get_centre_of_assembly(),
            ( 0, 0, 0 ),
            14
            )
        self.esym.set_root( root = LYSO_ROOT )
        self.assertIterablesAlmostEqual(
            self.esym.get_centre_of_assembly(),
            ( 20.424169491525419, 24.682130608175505, 29.066522432701866 ),
            12
            )





suite_operator = unittest.TestLoader().loadTestsFromTestCase(
    TestOperator
    )
suite_symmetry = unittest.TestLoader().loadTestsFromTestCase(
    TestSymmetry
    )
suite_assembly = unittest.TestLoader().loadTestsFromTestCase(
    TestAssembly
    )
suite_point_group = unittest.TestLoader().loadTestsFromTestCase(
    TestPointGroup
    )
suite_macromolecule_type = unittest.TestLoader().loadTestsFromTestCase(
    TestMacromoleculeType
    )
suite_reference_chain = unittest.TestLoader().loadTestsFromTestCase(
    TestReferenceChain
    )
suite_chain_alignment = unittest.TestLoader().loadTestsFromTestCase(
    TestChainAlignment
    )
suite_ensemble_symmetry = unittest.TestLoader().loadTestsFromTestCase(
    TestEnsembleSymmetry
    )

alltests = unittest.TestSuite(
    [
        suite_operator,
        suite_symmetry,
        suite_assembly,
        suite_point_group,
        suite_macromolecule_type,
        suite_reference_chain,
        suite_chain_alignment,
        suite_ensemble_symmetry,
        ]
    )


def load_tests(loader, tests, pattern):

    return alltests


if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
