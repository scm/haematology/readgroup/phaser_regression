from phaser.rotation import *

from scitbx.array_family import flex
import scitbx.math
import scitbx.matrix

import unittest
import math

AXIS = ( 0.29490565430844229, -0.021795044471616204, -0.95527777692846483 )
ANGLE = 0.27459552652571673
MATRIX = scitbx.matrix.sqr(
    scitbx.math.r3_rotation_axis_and_angle_as_matrix( AXIS, ANGLE )
    )

class TestIsotropicObject(unittest.TestCase):

    def setUp(self):

        self.radius = 18.5
        self.object = IsotropicObject( radius = self.radius )
        self.rmsd = 2.2647218398724758


    def test_proportionality(self):

        self.assertAlmostEqual(
            IsotropicObject.PROPORTIONALITY,
            2.0 / math.sqrt( 5.0 ),
            14
            )


    def test_rotation_rmsd(self):

        single_rms = self.object.rotation_rmsd( rotation = MATRIX )
        self.assertAlmostEqual( single_rms, self.rmsd, 13 )
        bigobject = IsotropicObject( radius = 2.0 * self.radius )
        self.assertAlmostEqual(
            bigobject.rotation_rmsd( rotation = MATRIX ),
            2.0 * self.rmsd,
            13
            )
        double_rms = self.object.rotation_rmsd(
            rotation =scitbx.math.r3_rotation_axis_and_angle_as_matrix( AXIS, 2.0 * ANGLE )
            )
        self.assertAlmostEqual(
            double_rms / single_rms,
            abs( math.sin( ANGLE) / math.sin( ANGLE / 2.0 ) )
            )


    def test_max_rmsd_for_angle(self):

        self.assertAlmostEqual(
            self.object.max_rmsd_for_angle( degrees = 180.0 / math.pi * ANGLE ),
            self.object.rotation_rmsd( MATRIX ),
            13
            )


    def test_reference(self):

        reference = scitbx.matrix.sqr( flex.random_double_r3_rotation_matrix() )
        self.object.set_reference( reference )
        self.assertAlmostEqual(
            self.object.distance_from_reference( rotation = MATRIX * reference ),
            self.object.rotation_rmsd( rotation = MATRIX ),
            13
            )


class TestAnisotropicObject(unittest.TestCase):

    def setUp(self):

        self.a = 5.0
        self.b = 8.0
        self.c = 14.0
        self.orientation = scitbx.matrix.sqr(
            flex.random_double_r3_rotation_matrix()
            )
        self.object = AnisotropicObject(
            orientation = self.orientation,
            a = self.a,
            b = self.b,
            c = self.c
            )
        self.rmsd_a = 1.3957730737880532
        self.rmsd_b = 1.2868391906081302
        self.rmsd_c = 0.81662589152077414
        self.rmsd = 0.88248620735202199


    def test_proportionality(self):

        self.assertAlmostEqual(
            AnisotropicObject.PROPORTIONALITY,
            math.sqrt( 2.0 / 5.0 ),
            14
            )


    def test_axis_angle_rmsd(self):

        self.assertAlmostEqual(
            self.object.axis_angle_rmsd(
                axis = self.orientation.elems[:3],
                angle = ANGLE
                ),
            self.rmsd_a
            )
        self.assertAlmostEqual(
            self.object.axis_angle_rmsd(
                axis = self.orientation[3:6],
                angle = ANGLE
                ),
            self.rmsd_b
            )
        self.assertAlmostEqual(
            self.object.axis_angle_rmsd(
                axis = self.orientation[6:],
                angle = ANGLE
                ),
            self.rmsd_c
            )

        rot_axis = self.orientation.transpose() * scitbx.matrix.col( AXIS )
        rot_matrix = scitbx.math.r3_rotation_axis_and_angle_as_matrix(
            rot_axis,
            ANGLE
            )
        single_rms = self.object.axis_angle_rmsd( rot_axis, ANGLE )
        self.assertAlmostEqual( single_rms, self.rmsd, 14 )
        double_rms = self.object.axis_angle_rmsd( rot_axis, 2.0 * ANGLE )
        self.assertAlmostEqual(
            double_rms / single_rms,
            abs( math.sin( ANGLE) / math.sin( ANGLE / 2.0 ) )
            )


    def test_rotation_rmsd(self):

        self.assertAlmostEqual(
            self.object.axis_angle_rmsd( axis = AXIS, angle = ANGLE ),
            self.object.rotation_rmsd( MATRIX ),
            14
            )


    def test_max_rmsd_for_angle(self):

        permutations = [
            ( self.a, self.b, self.c ),
            ( self.a, self.c, self.b ),
            ( self.b, self.a, self.c ),
            ( self.b, self.c, self.a ),
            ( self.c, self.a, self.b ),
            ( self.c, self.b, self.a ),
            ]

        for ( a, b, c ) in permutations:
            ani = AnisotropicObject( self.orientation, a, b, c )
            self.assertAlmostEqual(
                self.object.max_rmsd_for_angle( degrees = 180.0 / math.pi * ANGLE ),
                self.rmsd_a,
                13
                )


    def test_reference(self):

        reference = scitbx.matrix.sqr( flex.random_double_r3_rotation_matrix() )
        self.object.set_reference( reference )
        self.assertAlmostEqual(
            self.object.distance_from_reference( rotation = MATRIX * reference ),
            self.object.rotation_rmsd( rotation = MATRIX ),
            12
            )



suite_isotropic_object = unittest.TestLoader().loadTestsFromTestCase(
    TestIsotropicObject
    )
suite_anisotropic_object = unittest.TestLoader().loadTestsFromTestCase(
    TestAnisotropicObject
    )

alltests = unittest.TestSuite(
    [
        suite_isotropic_object,
        suite_anisotropic_object,
        ]
    )


def load_tests(loader, tests, pattern):

    return alltests


if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
