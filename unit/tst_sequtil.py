from phaser import sequtil

import unittest

class TestLocalize(unittest.TestCase):

  def test_empty(self):

    self.assertEqual(
      list( sequtil.localize( sequence = [], width = 0, padding = None ) ),
      [],
      )
    self.assertEqual(
      list( sequtil.localize( sequence = [], width = 1, padding = None ) ),
      [],
      )
    self.assertEqual(
      list( sequtil.localize( sequence = [], width = 2, padding = None ) ),
      [],
      )

  def test_width_0(self):

    seq = [ 1, 2, 3, 4, 5, 6 ]
    self.assertEqual(
      [ list( d ) for d in sequtil.localize( sequence = seq, width = 0, padding = None ) ],
      [ [ 1 ], [ 2 ], [ 3 ], [ 4 ], [ 5 ], [ 6 ] ]
      )


  def test_width_1(self):

    seq = [ 1, 2, 3, 4, 5, 6 ]
    self.assertEqual(
      [ list( d ) for d in sequtil.localize( sequence = seq, width = 1, padding = None ) ],
      [ [ None, 1, 2 ], [ 1, 2, 3 ], [ 2, 3, 4 ], [ 3, 4, 5 ],
          [ 4, 5, 6 ], [ 5, 6, None ] ]
      )

  def test_width_2(self):

    seq = [ 1, 2, 3, 4, 5, 6 ]
    self.assertEqual(
      [ list( d ) for d in sequtil.localize( sequence = seq, width = 2, padding = None ) ],
      [ [ None, None, 1, 2, 3 ], [ None, 1, 2, 3 , 4], [ 1, 2, 3, 4, 5 ],
          [ 2, 3, 4, 5, 6 ], [ 3, 4, 5, 6, None ], [ 4, 5, 6, None, None ] ]
      )

  def test_width_3(self):

    seq = [ 1, 2, 3 ]
    self.assertEqual(
      [ list( d ) for d in sequtil.localize( sequence = seq, width = 3, padding = None ) ],
      [
        [ None, None, None, 1, 2, 3, None ],
        [ None, None, 1, 2, 3, None, None ],
        [ None, 1, 2, 3, None, None, None ],
        ]
      )

  def test_width_4(self):

    seq = [ 1, 2, 3 ]
    self.assertEqual(
      [ list( d ) for d in sequtil.localize( sequence = seq, width = 4, padding = None ) ],
      [
        [ None, None, None, None, 1, 2, 3, None, None ],
        [ None, None, None, 1, 2, 3, None, None, None ],
        [ None, None, 1, 2, 3, None, None, None, None ],
        ]
      )

class TestSplit(unittest.TestCase):

  @staticmethod
  def consecutivity(left, right):

    return left + 1 == right


  def test_empty(self):

    self.assertEqual(
      list( sequtil.split( [], consecutivity =  self.consecutivity ) ),
      [],
      )
    self.assertEqual( list( sequtil.split( [], consecutivity = None ) ), [] )


  def test_1(self):

    seq = [ 0, 1, 3, -5, -4, -3, 0 ]
    self.assertEqual(
      list( sequtil.split( seq, consecutivity =  self.consecutivity ) ),
      [ [ 0, 1 ], [ 3 ], [ -5, -4, -3 ], [ 0 ] ],
      )


  def test_2(self):

    seq = [ 0, 9, 1, 2, 3 ]
    self.assertEqual(
      list( sequtil.split( seq, consecutivity =  self.consecutivity ) ),
      [ [ 0 ], [ 9 ], [ 1, 2, 3 ] ],
      )


  def test_3(self):

    seq = [ 0 ]
    self.assertEqual(
      list( sequtil.split( seq, consecutivity =  self.consecutivity ) ),
      [ [ 0 ] ],
      )


  def test_4(self):

    seq = [ 0, 1, 2 ]
    self.assertEqual(
      list( sequtil.split( seq, consecutivity =  self.consecutivity ) ),
      [ [ 0, 1, 2 ] ],
      )


class TestConvolution(unittest.TestCase):

  def test_empty(self):

    self.assertEqual(
      list( sequtil.convolution( sequence = [], pattern =  [ 1, 2, 1 ], padding = 0 ) ),
      [],
      )


  def test_1(self):

    seq = [ 1, 1, 1, 1, 1 ]
    self.assertEqual(
      list( sequtil.convolution( sequence = seq, pattern =  [ 1, 2, 1 ], padding = 0 ) ),
      [ 3, 4, 4, 4, 3 ],
      )


  def test_2(self):

    seq = [ 1, 1, 1, 1, 1 ]
    self.assertEqual(
      list( sequtil.convolution( sequence = seq, pattern =  [ 1, 2, 1 ], padding = 1 ) ),
      [ 4, 4, 4, 4, 4 ],
      )


  def test_3(self):

    seq = [ 0, 0, 0, 1, 0 ]
    self.assertEqual(
      list( sequtil.convolution( sequence = seq, pattern =  [ 1, 2, 1 ], padding = 0 ) ),
      [ 0, 0, 1, 2, 1 ],
      )



suite_localize = unittest.TestLoader().loadTestsFromTestCase(
  TestLocalize
  )
suite_split = unittest.TestLoader().loadTestsFromTestCase(
  TestSplit
  )
suite_convolution = unittest.TestLoader().loadTestsFromTestCase(
  TestConvolution
  )

alltests = unittest.TestSuite(
  [
    suite_localize,
    suite_split,
    suite_convolution,
    ]
  )


def load_tests(loader, tests, pattern):

  return alltests


if __name__ == "__main__":
  unittest.TextTestRunner( verbosity = 2 ).run( alltests )
