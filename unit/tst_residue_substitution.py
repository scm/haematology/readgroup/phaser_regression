import unittest

from phaser import residue_substitution
from phaser.pipeline.scaffolding import Fake, Return

class TestScoringMatrix(unittest.TestCase):

    def setUp(self):

        self.matrix = residue_substitution.ScoringMatrix(
            name = "foo",
            indexer = [ "A", "B" ],
            scores = [ [ 3, 1 ], [ 1, 2 ] ],
            gap_penalty = -1
            )


    def test_score_for(self):

        self.assertEqual(
            self.matrix.score_for( left = "A", right = "A" ),
            3
            )
        self.assertEqual(
            self.matrix.score_for( left = "A", right = "B" ),
            1
            )
        self.assertEqual(
            self.matrix.score_for( left = "B", right = "B" ),
            2
            )
        self.assertRaises( ValueError, self.matrix.score_for, "A", "C" )


    def test_normalization(self):

        self.assertEqual( self.matrix.normalization(), ( 0.75, 1.75 ) )


    def test_identity(self):

        self.check_matrix(
            matrix = residue_substitution.identity,
            scores = residue_substitution.identity_similarity_scores,
            gap_penalty = -1,
            normalization = ( 0.950, 0.050 )
            )


    def test_dayhoff(self):

        self.check_matrix(
            matrix = residue_substitution.dayhoff,
            scores = residue_substitution.dayhoff_mdm78_similarity_scores,
            gap_penalty = -80,
            normalization = ( 70.170, -11.570 )
            )


    def test_blosum50(self):

        self.check_matrix(
            matrix = residue_substitution.blosum50,
            scores = residue_substitution.blosum50_similarity_scores,
            gap_penalty = -10,
            normalization = ( 8.019, -1.106 )
            )


    def test_blosum62(self):

        self.check_matrix(
            matrix = residue_substitution.blosum62,
            scores = residue_substitution.blosum62_similarity_scores,
            gap_penalty = -8,
            normalization = ( 6.865, -1.065 )
            )


    def check_matrix(self, matrix, scores, gap_penalty, normalization):

        for left, line in zip( matrix.indexer, scores ):
            self.assertEqual(
                [ matrix.score_for( left, r ) for r in matrix.indexer ],
                line
                )

        self.assertEqual( matrix.gap_penalty, gap_penalty )
        ( scale, base ) = matrix.normalization()
        self.assertAlmostEqual( scale, normalization[0], 3 )
        self.assertAlmostEqual( base, normalization[1], 3 )


class TestSubstitutionSuite(unittest.TestCase):

    def setUp(self):

        self.matrices = [ Fake(), Fake() ]
        self.matrices[0].name = object()
        self.matrices[1].name = object()
        self.suite = residue_substitution.SubstitutionSuite(
            matrices = self.matrices
            )


    def test_names(self):

        self.assertEqual(
            sorted( self.suite.names() ),
            sorted( [ m.name for m in self.matrices ] )
            )


    def test_matrix_for(self):

        self.assertEqual(
            self.suite.matrix_for( name = self.matrices[0].name ),
            self.matrices[0]
            )
        self.assertEqual(
            self.suite.matrix_for( name = self.matrices[1].name ),
            self.matrices[1]
            )


    def test_protein(self):

        self.assertEqual(
            sorted( residue_substitution.protein.names() ),
            [ "blosum50", "blosum62", "dayhoff", "identity" ]
            )
        self.assertEqual(
            residue_substitution.protein.matrix_for( name = "identity" ),
            residue_substitution.identity
            )
        self.assertEqual(
            residue_substitution.protein.matrix_for( name = "dayhoff" ),
            residue_substitution.dayhoff
            )
        self.assertEqual(
            residue_substitution.protein.matrix_for( name = "blosum50" ),
            residue_substitution.blosum50
            )
        self.assertEqual(
            residue_substitution.protein.matrix_for( name = "blosum62" ),
            residue_substitution.blosum62
            )


    def test_unknown(self):

        self.assertEqual(
            sorted( residue_substitution.unknown.names() ),
            []
            )


class TestSequenceSimilarity(unittest.TestCase):

    def test_calculate1(self):

        ali1 = "TSWYLLLQQLIDGESLSRSQAAELMQGWLSEAVPPELSGAILTALNFKGVSADELTGMAEVLQSQSKMTNSP----F"
        ali2 = "-THQPILEKLFKSQSMTQEESHQLFAAIVRGELEDSQLAAALISMKMRGERPEEIAGAASALLADAQPFPRPDYDFA"

        alignment = Fake()
        alignment.alignments = [ ali1, ali2 ]
        alignment.gap = "-"
        alignment.multiplicity = Return( values = [ 2 ] )

        ssim = residue_substitution.SequenceSimilarity(
            alignment = alignment,
            matrix = residue_substitution.blosum62,
            unknown = 0
            )

        self.assertEqual(
            ssim.values,
            [
                -8,  1, -2, -1, -3,  2,  4,  2,  1,  4,  0, -1,  0,  2,  4,  2,
                 1,  1,  0,  2,  1, -2,  2,  4,  0, -1,  0, -3,  1, -1, -2, -1,
                 1, -1, -1,  0, -2, -2,  0,  4, -1,  4, -1,  1,  2,  0,  0,  2,
                 6, -2, -1, -1,  2,  5,  2,  0,  6, -1,  4,  0,  0,  4, -2,  1,
                 0,  1,  1, -2, -2, -2, -1,  7, -8, -8, -8, -8, -2
                ]
            )


    def test_calculate2(self):

        alignment = Fake()
        alignment.alignments = [ "AAAA", "AAAA", "AGAA", "AA-A", "AAAB" ]
        alignment.gap = "-"
        alignment.multiplicity = Return( values = [ 5 ] )

        unknown = 0
        ssim = residue_substitution.SequenceSimilarity(
            alignment = alignment,
            matrix = residue_substitution.blosum62,
            unknown = unknown
            )

        self.assertEqual(
            ssim.values,
            [
                residue_substitution.blosum62.score_for( left = "A", right = "A" ),
                residue_substitution.blosum62.score_for( left = "A", right = "G" ),
                residue_substitution.blosum62.gap_penalty,
                unknown
                ]
            )


    def test_calculate3(self):

        alignment = Fake()
        alignment.alignments = [ "ACDEFGHIKLMNPQRSTVWY-" ]
        alignment.gap = "-"
        alignment.multiplicity = Return( values = [ 1 ] )

        unknown = 0
        ssim = residue_substitution.SequenceSimilarity(
            alignment = alignment,
            matrix = residue_substitution.blosum62,
            unknown = unknown
            )

        self.assertEqual(
            ssim.values,
            [
                residue_substitution.blosum62.score_for( left = c, right = c )
                if c != alignment.gap else residue_substitution.blosum62.gap_penalty
                for c in alignment.alignments[0]
                ]
            )


suite_scoring_matrix = unittest.TestLoader().loadTestsFromTestCase(
    TestScoringMatrix
    )
suite_substitution_suite = unittest.TestLoader().loadTestsFromTestCase(
    TestSubstitutionSuite
    )
suite_sequence_similarity = unittest.TestLoader().loadTestsFromTestCase(
    TestSequenceSimilarity
    )


alltests = unittest.TestSuite(
    [
        suite_scoring_matrix,
        suite_substitution_suite,
        suite_sequence_similarity,
        ]
    )


def load_tests(loader, tests, pattern):

    return alltests


if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
