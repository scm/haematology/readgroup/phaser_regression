import phaser
import phaser.pickle_support

import pickle, cPickle
import unittest

class TestPickle(unittest.TestCase):

    enabled = False

    CELL = phaser.map_str_float()
    VRMS = phaser.map_str_float1D()
    VRMS[ "ensemble1" ] = ( 1.2, 1.3 )
    RLIST = phaser.mr_rlist( "Foo", ( 0.0, 0.0, 0.0 ), 0.0, 99.0 )
    NDIM = phaser.mr_ndim(
        "Foo",
        ( 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, -1.0 ),
        True,
        True,
        ( 0.25, 0.50, 0.75 ),
        80.0,
        False,
        False,
        True,
        )
    MAPCOEFS = phaser.MapCoefs(
        [ 1 ],
        [ 2, 2 ],
        [ 3, 3, 3 ],
        [ 4, 4, 4, 4 ],
        [ 5, 5, 5, 5, 5 ],
        [ 6, 6, 6, 6, 6, 6 ],
        [ 7, 7, 7, 7, 7, 7, 7 ],
        [ 8, 8, 8, 8, 8, 8, 8, 8 ],
        [ 9, 9, 9, 9, 9, 9, 9, 9, 9 ],
        [ 10, 10, 10, 10, 10, 10, 10, 10, 10 ],
        [ 11, 11, 11, 11, 11, 11, 11, 11, 11, 11 ],
        )
    SET = phaser.mr_set(
        " RFZ=13.0 TFZ=28.0",
        [ NDIM, NDIM ],
        [ RLIST ],
        185.0,
        18.0,
        1675.6,
        18.0,
        0.0,
        1,
        4,
        VRMS,
        CELL,
        MAPCOEFS
        )
    SOLUTION = phaser.mr_solution( [ SET, SET, SET ] )
    SPACE_GROUP = phaser.SpaceGroup( "P 2ac 2ab" )
    UNIT_CELL = phaser.UnitCell( [ 11, 12, 13, 80, 90, 100 ] )
    OUTPUT = phaser.Output()
    OUTPUT.setLogfile(logfile = "logfile")
    DATA_REFL = phaser.data_refl(
        [ ( 0, 1, 2 ), ( 3, 4, 5 ), ( 6, 7, 8 ) ],
        [ 30, 60, 90 ],
        [ 11, 12, 13 ]
        )
    DATA_A = phaser.DataA(
        [ 10, 20, 30, 80, 90, 100 ],
        DATA_REFL,
        phaser.data_resharp(),
        )
    RF_HALL = "P 21 21 21"
    TITLE = ""
    MODE = "LLG"
    NUM_TOP = 3
    MAP_STRING_DATA_PDB = phaser.map_string_data_pdb()
    RESULT_MR = phaser.ResultMR( OUTPUT )
    RESULT_MR.init( MODE, TITLE, NUM_TOP, MAP_STRING_DATA_PDB )
    RESULT_MR.setDataA( DATA_A )
    RESULT_MR.setUnitCell( UNIT_CELL.getUnitCell() )
    RESULT_MR.setDotSol( SOLUTION )

    RESULT_MR_RF = phaser.ResultMR_RF( OUTPUT )
    RESULT_MR_RF.init( RF_HALL, TITLE, NUM_TOP, MAP_STRING_DATA_PDB, MAP_STRING_DATA_MAP )
    RESULT_MR_RF.setUnitCell( UNIT_CELL.getUnitCell() )
    RESULT_MR_RF.setDotRlist( SOLUTION )

    RESULT_MR_TF = phaser.ResultMR_TF( OUTPUT )
    RESULT_MR_TF.init( TITLE, NUM_TOP, MAP_STRING_DATA_PDB, MAP_STRING_DATA_MAP )
    RESULT_MR_TF.setUnitCell( UNIT_CELL.getUnitCell() )
    RESULT_MR_TF.setDotSol( SOLUTION )

    BINSTATS = phaser.BinStats()
    BINSTATS.NUM_bin = [ 1, 2 ]
    BINSTATS.FOM_bin = [ 0.1, 0.2 ]
    BINSTATS.LoRes_bin = [ 100, 50 ]
    BINSTATS.HiRes_bin = [ 51, 5 ]

    HANDEP = phaser.HandEP()
    LOG_LIKELIHOOD = 93237.126
    HANDEP.selected = [True, False, True]
    HANDEP.log_likelihood = LOG_LIKELIHOOD
    HANDEP.acentStats = BINSTATS
    HANDEP.centStats = BINSTATS
    HANDEP.allStats = BINSTATS
    HANDEP.FOM = [ 0.78, 0.93 ]

    RESULT_EP= phaser.ResultEP()
    RESULT_EP.FOM = HANDEP.FOM

    RESULT_EP = phaser.ResultEP()
    RESULT_EP.setLogfile(
        logfile = OUTPUT.logfile(),
        )
    RESULT_EP.setUnitCell( UNIT_CELL.getUnitCell() )
    RESULT_EP.setHand( HANDEP, False )
    RESULT_EP.setHand( HANDEP, True )

    def setUp(self):

        if not self.enabled:
            phaser.pickle_support.enable()
            self.enabled = True


    def equals_mr_rlist(self, original, restored):

        self.assertEqual( restored.MODLID, original.MODLID )
        self.assertEqual( restored.EULER, original.EULER )
        self.assertEqual( restored.RF, original.RF )
        self.assertEqual( restored.RFZ, original.RFZ )


    def equals_mr_ndim(self, original, restored):

        self.assertEqual( restored.getModlid(), original.getModlid() )
        self.assertEqual( restored.getR(), original.getR() )
        self.assertEqual( restored.getFrac(), original.getFrac() )
        self.assertEqual( restored.getInFrac(), original.getInFrac() )
        self.assertEqual( restored.getFracT(), original.getFracT() )
        self.assertEqual( restored.getBfac(), original.getBfac() )
        self.assertEqual( restored.getFixR(), original.getFixR() )
        self.assertEqual( restored.getFixT(), original.getFixT() )
        self.assertEqual( restored.getFixB(), original.getFixB() )


    def equals_MapCoefs(self, original, restored):

        self.assertEqual( list( restored.FC ), list( original.FC ) )
        self.assertEqual( list( restored.PHIC ), list( original.PHIC ) )
        self.assertEqual( list( restored.FWT ), list( original.FWT ) )
        self.assertEqual( list( restored.PHWT ), list( original.PHWT ) )
        self.assertEqual( list( restored.DELFWT ), list( original.DELFWT ) )
        self.assertEqual( list( restored.PHDELWT ), list( original.PHDELWT ) )
        self.assertEqual( list( restored.FOM ), list( original.FOM ) )
        self.assertEqual( list( restored.HLA ), list( original.HLA ) )
        self.assertEqual( list( restored.HLB ), list( original.HLB ) )
        self.assertEqual( list( restored.HLC ), list( original.HLC ) )
        self.assertEqual( list( restored.HLD ), list( original.HLD ) )


    def equals_mr_set(self, original, restored):

        self.assertEqual( restored.ANNOTATION, original.ANNOTATION )
        self.assertEqual( restored.TF, original.TF )
        self.assertEqual( restored.TFZ, original.TFZ )
        self.assertEqual( restored.LLG, original.LLG )
        self.assertEqual( restored.ORIG_LLG, original.ORIG_LLG )
        self.assertEqual( restored.R, original.R )
        self.assertEqual( restored.ORIG_R, original.ORIG_R )
        self.assertEqual( restored.PAK, original.PAK )
        self.assertEqual( restored.ORIG_NUM, original.ORIG_NUM )
        self.assertEqual( restored.KEEP, original.KEEP )
        self.assertEqual( restored.EQUIV, original.EQUIV )
        self.assertEqual( restored.VRMS.keys(), original.VRMS.keys() )
        self.assertEqual( [ list( sa ) for sa in restored.VRMS.values() ], [ list( sa ) for sa in original.VRMS.values() ] )
        self.equals_MapCoefs( restored.MAPCOEFS, original.MAPCOEFS )

        for ( rest_rl, orig_rl ) in zip( restored.RLIST, original.RLIST ):
            self.equals_mr_rlist( rest_rl, orig_rl )

        for ( rest_nd, orig_nd ) in zip( restored.KNOWN, original.KNOWN ):
            self.equals_mr_ndim( rest_nd, orig_nd )


    def equals_mr_solution(self, original, restored):

        for (rest_set, orig_set ) in zip( restored, original ):
            self.equals_mr_set( rest_set, orig_set )


    def equals_SpaceGroup(self, original, restored):

        self.assertEqual( original.getSpaceGroupHall(), restored.getSpaceGroupHall() )


    def equals_UnitCell(self, original, restored):

        self.assertEqual( original.getUnitCell(), restored.getUnitCell() )


    def equals_Output(self, original, restored):

        self.assertEqual( original.summary(), restored.summary() )
        self.assertEqual( original.logfile(), restored.logfile() )
        self.assertEqual( original.verbose(), restored.verbose() )


    def equals_data_refl(self, original, restored):

        self.assertEqual( original.MILLER, restored.MILLER )
        self.assertEqual( original.FMEAN, restored.FMEAN )
        self.assertEqual( original.SIGFMEAN, restored.SIGFMEAN )


    def equals_DataA(self, original, restored):

        self.equals_UnitCell( original, restored )
        self.assertEqual( original.getMiller(), restored.getMiller() )
        self.assertEqual( original.getF(), restored.getF() )
        self.assertEqual( original.getSIGF(), restored.getSIGF() )


    def equals_ResultMR(self, original, restored):

        self.equals_mr_solution( original.getDotSol(), restored.getDotSol() )
        self.equals_Output( original, restored )
        self.equals_DataA( original, restored )
        self.assertEqual( original.getTitle(), restored.getTitle() )
        self.assertEqual( original.getResultType(), restored.getResultType() )


    def equals_ResultMR_RF(self, original, restored):

        self.equals_mr_solution( original.getDotRlist(), restored.getDotRlist() )
        self.equals_Output( original, restored )
        self.equals_UnitCell( original, restored )
        self.assertEqual( original.getTitle(), restored.getTitle() )


    def equals_ResultMR_TF(self, original, restored):

        self.equals_mr_solution( original.getDotSol(), restored.getDotSol() )
        self.equals_Output( original, restored )
        self.equals_UnitCell( original, restored )
        self.assertEqual( original.getTitle(), restored.getTitle() )


    def equals_BinStats(self, original, restored):

        self.assertEqual( original.numbins(), restored.numbins() )

        for bin in range( original.numbins() ):
            self.assertEqual( original.getBinNum( bin ), restored.getBinNum( bin ) )
            self.assertEqual( original.getBinNum( bin ), restored.getBinNum( bin ) )


    def equals_DataS(self, original, restored):

        self.assertEqual( original.log_likelihood, restored.log_likelihood )
        self.assertEqual( len( original.FOM ), len( restored.FOM ) )

        for ( orig, rest ) in zip( original.FOM, restored.FOM ):
            self.assertAlmostEqual( orig, rest, 8 )

        self.equals_BinStats( original.allStats, restored.allStats )


    def equals_ResultEP_SAD(self, original, restored):

        self.equals_SpaceGroup( original, restored )
        self.equals_DataS( original, restored )


    def equals_ResultEP(self, original, restored):

        self.equals_Output( original, restored )
        self.equals_UnitCell( original, restored )
        self.equals_ResultEP_SAD( original.getHand( True ), restored.getHand( True ) )
        self.equals_ResultEP_SAD( original.getHand( False ), restored.getHand( False ) )


    def pickle_test(self, original, equals_method):

        dumps = pickle.dumps( original )
        restored1 = pickle.loads( dumps )

        self.assertNotEqual( id( original ), id( restored1 ) )
        equals_method( original, restored1 )

        dumps = cPickle.dumps( original )
        restored2 = cPickle.loads( dumps )

        self.assertNotEqual( id( original ), id( restored2 ), )
        equals_method( original, restored2 )


    def testMR_Rlist(self):

        self.pickle_test( self.RLIST, self.equals_mr_rlist )


    def testMR_Ndim(self):


        self.pickle_test( self.NDIM, self.equals_mr_ndim )


    def testMapCoefs(self):


        self.pickle_test( self.MAPCOEFS, self.equals_MapCoefs )


    def testMRSet(self):

        self.pickle_test( self.SET, self.equals_mr_set )


    def testMRSolution(self):

        self.pickle_test( self.SOLUTION, self.equals_mr_solution )


    def testSpaceGroup(self):

        self.pickle_test( self.SPACE_GROUP, self.equals_SpaceGroup )


    def testUnitCell(self):

        self.pickle_test( self.UNIT_CELL, self.equals_UnitCell )


    def testOutput(self):

        self.pickle_test( self.OUTPUT, self.equals_Output )


    def testdata_refl(self):

        self.pickle_test( self.DATA_REFL, self.equals_data_refl )


    def testDataA(self):

        self.pickle_test( self.DATA_A, self.equals_DataA )


    def testResultMR(self):

        self.pickle_test( self.RESULT_MR, self.equals_ResultMR )


    def testResultMR_RF(self):

        self.pickle_test( self.RESULT_MR_RF, self.equals_ResultMR_RF )


    def testResultMR_TF(self):

        self.pickle_test( self.RESULT_MR_TF, self.equals_ResultMR_TF )


    def testBinStats(self):

        self.pickle_test( self.BINSTATS, self.equals_BinStats )


    def testResultEP(self):

        self.pickle_test( self.RESULT_EP, self.equals_ResultEP )


suite_pickle = unittest.TestLoader().loadTestsFromTestCase(
    TestPickle
    )

alltests = unittest.TestSuite(
    [
        suite_pickle,
        ]
    )


def load_tests(loader, tests, pattern):

    return alltests


if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
