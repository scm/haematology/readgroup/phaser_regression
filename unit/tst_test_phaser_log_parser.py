import unittest
import os.path

import libtbx.load_env

from phaser_regression import test_phaser_log_parser
from phaser.pipeline.scaffolding import IterableCompareTestCase

DATA = os.path.join(
    libtbx.env.under_dist( "phaser_regression", "data" ),
    "logfile",
    )


class TestScriptResult(unittest.TestCase):

    result = None
    LOG = None

    def setUp(self):

        if self.__class__.result is None:
              with open( os.path.join( DATA, "toxd_MR_DAT.log") ) as f:
                  self.__class__.LOG = f.read()

              self.__class__.result = test_phaser_log_parser.ResultBase(
                  log = self.__class__.LOG,
                  )

    def test_getSpaceGroupHall(self):

        self.assertEqual( self.result.getSpaceGroupHall(), " P 2ac 2ab" )


    def test_getSpaceGroupName(self):

        self.assertEqual( self.result.getSpaceGroupName(), "P 21 21 21" )


    def test_getSpaceGroupNum(self):

        self.assertEqual( self.result.getSpaceGroupNum(), 19 )


    def test_getUnitCell(self):

        ( a, b, c, alpha, beta, gamma ) = self.result.getUnitCell()

        self.assertAlmostEqual( a, 73.58, 2 )
        self.assertAlmostEqual( b, 38.73, 2 )
        self.assertAlmostEqual( c, 23.19, 2 )
        self.assertAlmostEqual( alpha, 90.00, 2 )
        self.assertAlmostEqual( beta, 90.00, 2 )
        self.assertAlmostEqual( gamma, 90.00, 2 )


    def test_Failed(self):

        self.assertFalse( self.result.Failed() )


    def test_getMiller(self):

        self.assertEqual( len( self.result.getMiller() ), 3161 )


    def test_logfile(self):

        self.assertEqual( self.result.logfile(), self.LOG )


    def test_ErrorMessage(self):

        self.assertEqual( self.result.ErrorMessage(), self.LOG )


class TestScriptResultMR_RNP(TestScriptResult, IterableCompareTestCase):

    result = None
    LOG = None
    SOL = None

    result2 = None
    LOG2 = None
    SOL2 = None

    def setUp(self):

        if self.__class__.result is None:
            with open(os.path.join( DATA, "toxd_MR_RNP.log") ) as log:
                self.__class__.LOG = log.read()

            with open(os.path.join( DATA, "toxd_MR_RNP.sol") ) as sol:
                self.__class__.SOL = sol.read()

            self.__class__.result = test_phaser_log_parser.ResultMR.RNP(
                log = self.__class__.LOG,
                sol = self.__class__.SOL,
                )

        if self.__class__.result2 is None:
            with open(os.path.join( DATA, "beta_blip_MR_RNP.log") ) as log:
                self.__class__.LOG2 = log.read()

            with open(os.path.join( DATA, "beta_blip_MR_RNP.sol") ) as sol:
                self.__class__.SOL2 = sol.read()

            self.__class__.result2 = test_phaser_log_parser.ResultMR.RNP(
                log = self.__class__.LOG2,
                sol = self.__class__.SOL2,
                )

    def test_getValues(self):

       self.assertIterablesAlmostEqual(
           self.result.getValues(),
           [ 19.2 ],
           2
           )


    def test_getDotSol(self):

        dot_sol = self.result.getDotSol()

        self.assertEqual( len( dot_sol ), 1 )

        mset = dot_sol[0]
        self.assertEqual( len( mset.KNOWN ), 1 )
        self.assertIterablesAlmostEqual(
            mset.KNOWN[0].getEuler(),
            ( 144.998, 23.888, 195.821 ),
            2
            )
        self.assertIterablesAlmostEqual(
            mset.KNOWN[0].getFracT(),
            ( 0.88802, 0.27667, 0.8598 ),
            5
            )
        self.assertEqual( mset.KNOWN[0].getModlid(), "toxd" )


    def test_getValues2(self):

       self.assertIterablesAlmostEqual(
           self.result2.getValues(),
           [ 1017.9 ],
           2
           )


    def test_getDotSol2(self):

        dot_sol = self.result2.getDotSol()

        self.assertEqual( len( dot_sol ), 1 )

        mset = dot_sol[0]
        self.assertEqual( len( mset.KNOWN ), 2 )
        self.assertIterablesAlmostEqual(
            mset.KNOWN[0].getEuler(),
            ( 200.899, 41.273, 183.874 ),
            2
            )
        self.assertIterablesAlmostEqual(
            mset.KNOWN[1].getEuler(),
            ( 43.881, 80.993, 117.236 ),
            2
            )
        self.assertIterablesAlmostEqual(
            mset.KNOWN[0].getFracT(),
            ( -0.49572, -0.15796, -0.28097 ),
            2
            )
        self.assertIterablesAlmostEqual(
            mset.KNOWN[1].getFracT(),
            ( -0.12353, 0.2932, -0.09218 ),
            2
            )
        self.assertEqual( mset.KNOWN[0].getModlid(), "beta" )
        self.assertEqual( mset.KNOWN[1].getModlid(), "blip" )


class TestScriptResultMR_PAK(TestScriptResult, IterableCompareTestCase):

    result = None
    LOG = None
    SOL = None

    def setUp(self):

        if self.__class__.result is None:
            with open(os.path.join( DATA, "toxd_MR_PAK.log") ) as log:
                self.__class__.LOG = log.read()

            with open(os.path.join( DATA, "toxd_MR_RNP.sol") ) as sol:
                self.__class__.SOL = sol.read()

            self.__class__.result = test_phaser_log_parser.ResultMR.PAK(
                log = self.__class__.LOG,
                sol = self.__class__.SOL,
                )

    def test_getValues(self):

       self.assertEqual( self.result.getValues(), [ 0 ] )


class TestScriptResultMR_LLG(TestScriptResult, IterableCompareTestCase):

    result = None
    LOG = None
    SOL = None

    def setUp(self):

        if self.__class__.result is None:
            with open(os.path.join( DATA, "toxd_MR_LLG.log") )as log:
                self.__class__.LOG = log.read()

            with open(os.path.join( DATA, "toxd_MR_RNP.sol") ) as sol:
                self.__class__.SOL = sol.read()

            self.__class__.result = test_phaser_log_parser.ResultMR.LLG(
                log = self.__class__.LOG,
                sol = self.__class__.SOL
                )

    def test_getValues(self):

       expecteds = [ 93.6 ]
       gots = self.result.getValues()

       self.assertEqual( len( expecteds ), len( gots ) )

       for ( e, g ) in zip( expecteds, gots ):
          self.assertAlmostEqual( e, g, 1  )


class TestScriptResultMR_AUTO(TestScriptResult, IterableCompareTestCase):

    result = None
    LOG = None
    SOL = None

    def setUp(self):

        if self.__class__.result is None:
            with open(os.path.join( DATA, "toxd_MR_AUTO.log") ) as log:
                self.__class__.LOG = log.read()

            with open(os.path.join( DATA, "toxd_MR_RNP.sol") ) as sol:
                self.__class__.SOL = sol.read()

            self.__class__.result = test_phaser_log_parser.ResultMR_AUTO(
                log = self.__class__.LOG,
                sol = self.__class__.SOL
                )

    def test_getValues(self):

       self.assertIterablesAlmostEqual(
           self.result.getValues(),
           [ 103.2 ],
           2
           )


suite_script_result = unittest.TestLoader().loadTestsFromTestCase(
    TestScriptResult
    )
suite_script_result_mr_rnp = unittest.TestLoader().loadTestsFromTestCase(
    TestScriptResultMR_RNP
    )
suite_script_result_mr_pak = unittest.TestLoader().loadTestsFromTestCase(
    TestScriptResultMR_PAK
    )
suite_script_result_mr_llg = unittest.TestLoader().loadTestsFromTestCase(
    TestScriptResultMR_LLG
    )
suite_script_result_mr_auto = unittest.TestLoader().loadTestsFromTestCase(
    TestScriptResultMR_AUTO
    )

alltests = unittest.TestSuite(
    [
        suite_script_result,
        suite_script_result_mr_rnp,
        suite_script_result_mr_pak,
        suite_script_result_mr_llg,
        suite_script_result_mr_auto,
        ]
    )


def load_tests(loader, tests, pattern):

    return alltests


if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
