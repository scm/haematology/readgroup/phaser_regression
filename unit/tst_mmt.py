from phaser.pipeline.scaffolding import Fake, Return

from phaser import mmt

import unittest

class TestMacromoleculeType(unittest.TestCase):

    def test_recognize_chain_type(self):

        known = ( object(), object(), object() )
        dummy = mmt.MacromoleculeType(
            name = "protein",
            one_letter_codes = (),
            three_letter_codes = known ,
            molecular_weights = (),
            hydrogen_counts = (),
            consecutivity = None,
            mainchain_atom_names = (),
            mainchain_atom_radii = (),
            sidechain_topologies = (),
            sidechain_atom_radii_dicts = (),
            secondary_structure_analyser = None,
            similarity_matrix_suite = None,
            min_length = 2,
            )

        ags = [ Fake(), Fake(), Fake(), Fake() ]
        ags[0].resname = known[2]
        ags[1].resname = known[0]
        ags[2].resname = object()
        ags[3].resname = known[0]

        rgs = [ Fake(), Fake(), Fake(), Fake() ]
        rgs[0].atom_groups = Return( values = [ [ ags[0] ] ] )
        rgs[1].atom_groups = Return( values = [ [ ags[1] ] ] )
        rgs[2].atom_groups = Return( values = [ [ ags[2] ] ] )
        rgs[3].atom_groups = Return( values = [ [ ags[3] ] ] )

        chain = Fake()
        chain.residue_groups = Return( values = [ rgs ] )

        self.assertEqual(
            dummy.recognize_chain_type( chain = chain, confidence = 0.9 ),
            False
            )

        rgs[0].atom_groups.reset()
        rgs[1].atom_groups.reset()
        rgs[2].atom_groups.reset()
        rgs[3].atom_groups.reset()
        chain.residue_groups.reset()

        self.assertEqual(
            dummy.recognize_chain_type( chain = chain, confidence = 0.75 ),
            True
            )


class TestModule(unittest.TestCase):

    def setUp(self):

        self._mmt_known = mmt.KNOWN


    def tearDown(self):

        mmt.KNOWN = self._mmt_known


    def test_determine(self):

        dummy = Fake()
        ( chain, confidence ) = ( object(), object() )
        mmt.KNOWN = [ dummy ]

        dummy.recognize_chain_type = Return( values = [ True, False ] )
        self.assertEqual(
            mmt.determine( chain = chain, confidence = confidence ),
            dummy
            )
        self.assertEqual(
            mmt.determine( chain = chain, confidence = confidence ),
            mmt.UNKNOWN
            )
        self.assertEqual(
            dummy.recognize_chain_type._calls,
            [ ( (), { "chain": chain, "confidence": confidence } ) ] * 2
            )





suite_macromolecule_type = unittest.TestLoader().loadTestsFromTestCase(
    TestMacromoleculeType
    )
suite_module = unittest.TestLoader().loadTestsFromTestCase(
    TestModule
    )


alltests = unittest.TestSuite(
    [
        suite_macromolecule_type,
        suite_module,
        ]
    )


def load_tests(loader, tests, pattern):

    return alltests


if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
