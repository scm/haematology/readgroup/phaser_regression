from phaser import matrix2eulerDEG, matrix2eulerDEG_old
from phaser.pipeline.scaffolding import IterableCompareTestCase
from scitbx.array_family import flex
import scitbx.math

import unittest

class TestMatrix2EulerDEG(IterableCompareTestCase):

    def test_random(self):

        for i in range( 10000 ):
            m = flex.random_double_r3_rotation_matrix()
            self.assertIterablesAlmostEqual(
                matrix2eulerDEG_old( m ),
                matrix2eulerDEG( m ),
                4,
                )


    def test_1(self):

        m = scitbx.math.euler_angles_zyz_matrix( 10, 1E-6, 30 )
        self.assertIterablesAlmostEqual(
            matrix2eulerDEG_old( m ),
            matrix2eulerDEG( m ),
            6,
            )

suite_matrix2eulerDEG = unittest.TestLoader().loadTestsFromTestCase(
    TestMatrix2EulerDEG
    )

alltests = unittest.TestSuite(
    [
        suite_matrix2eulerDEG,
        ]
    )


def load_tests(loader, tests, pattern):

    return alltests


if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
