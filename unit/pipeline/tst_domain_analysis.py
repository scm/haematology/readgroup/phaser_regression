import unittest
import os.path
import libtbx.load_env

from phaser import proxy
from phaser.pipeline.scaffolding import Fake, Return
from phaser.pipeline import domain_analysis
from phaser.pipeline import homology
from phaser.pipeline.proxy import cath, dbfetch

DIR = os.path.join(
    libtbx.env.under_dist( "phaser_regression", "data" ),
    "bioinformatics",
    )

class TestModule(unittest.TestCase):

    def setUp(self):

        self.cath_fetch = cath.fetch
        self.cath_check = cath.check
        self.dbfetch_fetch_single = dbfetch.fetch_single
        self.dbfetch_check = dbfetch.check
        self.proxy_get_pdb_redirections = proxy.get_pdb_redirections
        self.da_average_domain_defs = domain_analysis.average_domain_definitions

        from iotbx import bioinformatics
        ( seqs, junk ) = bioinformatics.fasta_sequence_parse(
            text = open( os.path.join( DIR, "2CHL.fasta.txt" ) ).read()
            )
        self.sequence = seqs[0]
        self.blast = homology.get_ncbi_blast_parser()(
            data = open( os.path.join( DIR, "ncbi_blast_2chl.xml" ) ).read()
            )


    def tearDown(self):

        cath.fetch = self.cath_fetch
        cath.check = self.cath_check
        dbfetch.fetch_single = self.dbfetch_fetch_single
        dbfetch.check = self.dbfetch_check
        proxy.get_pdb_redirections = self.proxy_get_pdb_redirections
        domain_analysis.average_domain_definitions = self.da_average_domain_defs


    def get_domain_definitions(self, index, pdb_file, cath_file):

        alignment = list( self.blast.hits() )[ index - 1 ].alignment.copy()

        from phaser import rsam
        rsam.alignment_stich( sequence = self.sequence, alignment = alignment, index = 0 )

        import iotbx.pdb
        root = iotbx.pdb.input( os.path.join( DIR, pdb_file ) ).construct_hierarchy()

        ann = homology.parse_cath_xml(
            data = open( os.path.join( DIR, cath_file ) ).read(),
            )

        return domain_analysis.map_domains_on_target(
            annotation = ann,
            alignment = alignment,
            chain = root.models()[0].chains()[0],
            min_domain_length = 10
            )


    def test_map_domains_2chl_a(self):

        res = self.get_domain_definitions(
            index = 2,
            pdb_file = "2chl_a.pdb",
            cath_file = "cath_2chl.xml"
            )
        self.assertEqual( len( res ), 2 )
        self.assertEqual( res[0].iselection, set( range( 24, 108 ) ) )
        self.assertEqual( res[1].iselection, set( range( 108, 322 ) ) )


    def test_map_domains_2r7b_a(self):

        res = self.get_domain_definitions(
            index = 24,
            pdb_file = "2r7b_a.pdb",
            cath_file = "cath_2r7b.xml"
            )
        self.assertEqual( len( res ), 2 )
        self.assertEqual( res[0].iselection, set( range( 30, 102 ) ) )
        self.assertEqual(
            res[1].iselection,
            set( range( 102, 240 ) ).difference(
                [ 109, 179], range( 124, 132 ), range( 164, 169 ), range( 188, 192 )
                )
            )


    def test_map_domains_1stc_e(self):

        res = self.get_domain_definitions(
            index = 99,
            pdb_file = "1stc_e.pdb",
            cath_file = "cath_1stc.xml"
            )
        self.assertEqual( len( res ), 2 )
        self.assertEqual(
            res[0].iselection,
            set( range( 112, 326 ) ).difference(
                [ 113, 180 ], range( 163, 168 ), range( 186, 195 ),
                range( 237, 240 ), range( 266, 270 ), range( 287, 289 ),
                range( 307, 310 )
                )
            )
        self.assertEqual(
            res[1].iselection,
            set( range( 35, 108 ) ).union(
                range( 109, 112 ), range( 326, 343 )
                )
            )


    def test_map_domains_2uzt_a(self):

        res = self.get_domain_definitions(
            index = 94,
            pdb_file = "2uzt_a.pdb",
            cath_file = "cath_2uzt.xml"
            )
        self.assertEqual( len( res ), 2 )

        self.assertEqual(
            res[0].iselection,
            set( range( 112, 261 ) ).difference(
                [ 113, 180 ], range( 163, 168 ), range( 186, 195 ), range( 237, 240 )
                )
            )
        self.assertEqual(
            res[1].iselection,
            set( range( 35, 112 ) ).difference( [ 108 ] )
            )


    def test_map_domains_2f7e_e(self):

        res = self.get_domain_definitions(
            index = 92,
            pdb_file = "2f7e_e.pdb",
            cath_file = "cath_2f7e.xml"
            )
        self.assertEqual( len( res ), 2 )
        self.assertEqual(
            res[0].iselection,
            set( range( 112, 326 ) ).difference(
                [ 113, 180, 287, 288 ], range( 163, 168 ), range( 186, 195 ),
                range( 237, 240 ), range( 266, 270 ), range( 307, 310 ),
                )
            )
        self.assertEqual(
            res[1].iselection,
            set( range( 35, 112 ) ).union( range( 326, 343 ) ).difference( [ 108 ] )
            )


    def test_average_domain_definitions(self):

        small_2chl = domain_analysis.ChainSequenceSelection.from_iselection(
            count = len( self.sequence ),
            iselection = set( range( 24, 108 ) )
            )
        large_2chl = domain_analysis.ChainSequenceSelection.from_iselection(
            count = len( self.sequence ),
            iselection = set( range( 108, 322 ) )
            )

        small_2r7b = domain_analysis.ChainSequenceSelection.from_iselection(
            count = len( self.sequence ),
            iselection = set( range( 30, 102 ) )
            )
        large_2r7b = domain_analysis.ChainSequenceSelection.from_iselection(
            count = len( self.sequence ),
            iselection = set( range( 102, 240 ) ).difference(
                [ 109, 179], range( 124, 132 ), range( 164, 169 ), range( 188, 192 )
                )
            )

        small_1stc = domain_analysis.ChainSequenceSelection.from_iselection(
            count = len( self.sequence ),
            iselection = set( range( 35, 108 ) ).union(
                range( 109, 112 ), range( 326, 343 )
                )
            )
        large_1stc = domain_analysis.ChainSequenceSelection.from_iselection(
            count = len( self.sequence ),
            iselection = set( range( 112, 326 ) ).difference(
                [ 113, 180 ], range( 163, 168 ), range( 186, 195 ),
                range( 237, 240 ), range( 266, 270 ), range( 287, 289 ),
                range( 307, 310 )
                )
            )

        small_2f7e = domain_analysis.ChainSequenceSelection.from_iselection(
            count = len( self.sequence ),
            iselection = set( range( 35, 112 ) ).union( range( 326, 343 ) ).difference( [ 108 ] )
            )
        large_2f7e = domain_analysis.ChainSequenceSelection.from_iselection(
            count = len( self.sequence ),
            iselection = set( range( 112, 326 ) ).difference(
                [ 113, 180, 287, 288 ], range( 163, 168 ), range( 186, 195 ),
                range( 237, 240 ), range( 266, 270 ), range( 307, 310 ),
                )
            )

        small_2uzt = domain_analysis.ChainSequenceSelection.from_iselection(
            count = len( self.sequence ),
            iselection = set( range( 35, 112 ) ).difference( [ 108 ] )
            )
        large_2uzt = domain_analysis.ChainSequenceSelection.from_iselection(
            count = len( self.sequence ),
            iselection = set( range( 112, 261 ) ).difference(
                [ 113, 180 ], range( 163, 168 ), range( 186, 195 ), range( 237, 240 )
                )
            )

        # using all definitions
        res = domain_analysis.average_domain_definitions(
            assignments = [
                [ small_2chl, large_2chl ],
                [ small_2r7b, large_2r7b ],
                [ large_1stc, small_1stc ],
                [ large_2uzt, small_2uzt ],
                [ large_2f7e, small_2f7e ],
                ],
            cluster_radius = 0.2,
            polish_function = domain_analysis.polish
            )

        self.assertEqual( len( res ), 1 )
        self.assertEqual( len( res[0] ), 2 )

        dom1 = frozenset( range( 24, 112 ) + range( 326, 343 ) )
        dom2 = frozenset( range( 112, 326 ) )
        self.assertEqual(
            set( [ frozenset( d.iselection ) for d in res[0][0] ] ),
            set( [ dom1, dom2 ] )
            )
        self.assertEqual(
            res[0][1].iselection,
            set( range( 24 ) + range( 343, 351 ) )
            )

        # 3 definitions
        res = domain_analysis.average_domain_definitions(
            assignments = [
                [ large_1stc, small_1stc ],
                [ large_2uzt, small_2uzt ],
                [ large_2f7e, small_2f7e ],
                ],
            cluster_radius = 0.2,
            polish_function = domain_analysis.polish
            )

        self.assertEqual( len( res ), 1 )
        self.assertEqual( len( res[0] ), 2 )

        dom1 = frozenset(
            set( range( 114, 326 ) ).difference( range( 163, 168 ), range( 186, 195 ) )
            )
        dom2 = frozenset( range( 35, 112 ) + range( 326, 343 ) )
        self.assertEqual(
            set( [ frozenset( d.iselection ) for d in res[0][0] ] ),
            set( [ dom1, dom2 ] )
            )
        self.assertEqual(
            res[0][1].iselection,
            set( range( 35 ) + [ 112, 113 ] + range( 163, 168 )
                + range( 186, 195 ) + range( 343, 351 ) )
            )


    def test_process(self):

        cath_2f7e = Fake()
        cath_2f7e.data = open( os.path.join( DIR, "cath_2f7e.xml" ) ).read()
        cath_2uzt = Fake()
        cath_2uzt.data = open( os.path.join( DIR, "cath_2uzt.xml" ) ).read()
        cath_1stc = Fake()
        cath_1stc.data = open( os.path.join( DIR, "cath_1stc.xml" ) ).read()
        cath.fetch = Return( values = [ cath_2f7e, cath_2uzt, cath_1stc ] )
        cath.check = Return( values = [ None ] )

        pdb_2f7e = Fake()
        pdb_2f7e.data = open( os.path.join( DIR, "2f7e_e.pdb" ) ).read()
        pdb_2uzt = Fake()
        pdb_2uzt.data = open( os.path.join( DIR, "2uzt_a.pdb" ) ).read()
        pdb_1stc = Fake()
        pdb_1stc.data = open( os.path.join( DIR, "1stc_e.pdb" ) ).read()
        dbfetch.fetch_single = Return( values = [ pdb_2f7e, pdb_2uzt, pdb_1stc ] )
        dbfetch.check = Return( values = [ None ] )

        pdb_redir = Fake()
        pdb_redir.retracted = Return( values = [ False, False, False ] )
        pdb_redir.obsoleted = Return( values = [ False, False, False ] )
        proxy.get_pdb_redirections = Return( values = [ pdb_redir ] )

        dom1 = domain_analysis.ChainSequenceSelection.from_iselection(
            count = 351,
            iselection = set( range( 114, 326 ) ).difference(
                range( 163, 168 ), range( 186, 195 ),
                )
            )
        dom2 = domain_analysis.ChainSequenceSelection.from_iselection(
            count = 351,
            iselection = set( range( 35, 112 ) ).union( range( 326, 343 ) )
            )
        missing = domain_analysis.ChainSequenceSelection.from_iselection(
            count = 351,
            iselection = set( range( 35 ) + [ 112, 113 ] + range( 163, 168 )
                + range( 186, 195 ) + range( 343, 351 ) )
            )
        da_res = [ ( frozenset( [ dom1, dom2 ] ), missing ) ]
        domain_analysis.average_domain_definitions = Return( values = [ da_res ] )

        from phaser import output

        hits = list( self.blast.hits() )
        logger = output.SingleStream()
        params = Fake()
        params.min_domain_length = 10
        params.cluster_radius = 0.2
        params.polish_long = 15
        params.polish_short = 5
        res = domain_analysis.process(
            sequence = self.sequence,
            hits = [ hits[91], hits[93], hits[98] ],
            params = params,
            logger = logger
            )
        self.assertEqual( res, da_res )
        self.assertEqual(
            cath.fetch._calls,
            [
                ( (), { "identifier": "2F7E" } ),
                ( (), { "identifier": "2UZT" } ),
                ( (), { "identifier": "1STC" } ),
                ]
            )
        self.assertEqual(
            [ r[1][ "identifier" ] for r in dbfetch.fetch_single._calls ],
            [ "2F7E", "2UZT", "1STC" ]
            )
        self.assertEqual( cath.check._calls, [ ( (), {} ) ] )
        self.assertEqual( dbfetch.check._calls, [ ( (), {} ) ] )


suite_module = unittest.TestLoader().loadTestsFromTestCase(
    TestModule
    )

alltests = unittest.TestSuite(
    [
        suite_module,
        ]
    )

if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
