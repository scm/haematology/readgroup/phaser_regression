from phaser.pipeline import expert
from phaser.pipeline import mr_object
from phaser.pipeline.scaffolding import Fake, IterableCompareTestCase, Return

from phaser import matching
from phaser import output
from phaser import mmt
from phaser.pipeline import superposition

import libtbx.load_env
import scitbx.matrix
import scitbx.math
from cctbx import uctbx
from cctbx import sgtbx

import unittest
import os.path

SLT = [
    ( (   82.626,  39.519,  101.333 ), (  0.164, 0.458,  0.025 ) ), # A 1
    ( (   93.392, 140.473,   -5.484 ), (  0.170, 0.962, -0.037 ) ), # B 2
    ( (   89.651, 139.778,   64.629 ), (  0.169, 0.959, -0.037 ) ), # C 2
    ( (   90.202, 141.745,  135.455 ), (  0.169, 0.966, -0.036 ) ), # D 2
    ( (   93.512, 143.650, -148.836 ), (  0.169, 0.967, -0.034 ) ), # E 2
    ( ( -138.863, 113.954,   67.815 ), ( -0.025, 0.842, -0.272 ) ), # F 3
    ( ( -138.375, 116.004,  138.890 ), ( -0.028, 0.839, -0.270 ) ), # G 3
    ( (  -89.713, 138.276,   59.058 ), ( -0.333, 1.049, -0.028 ) ), # H 1 SE
    ( ( -134.801,  59.586,   21.515 ), ( -0.037, 1.356,  0.263 ) ), # I 4
    ( (   86.870,  38.471,   25.271 ), (  0.166, 0.459,  0.027 ) ), # J 1
    ( (  -44.335, 116.589,  -14.744 ), (  0.035, 0.856,  0.234 ) ), # K 4 SE
    ( (   85.113,  41.352,  169.910 ), (  0.166, 1.454,  0.028 ) ), # L 1 SE
    ( ( -134.601, 115.982,  -75.176 ), ( -0.029, 0.841, -0.271 ) ), # M 3
    ( (  -42.817, 118.984,  -85.606 ), (  0.034, 0.854,  0.236 ) ), # N 4 SE
    ( ( -135.760, 118.011, -147.150 ), ( -0.027, 0.839, -0.268 ) ), # O 3
    ( (  -90.020, 140.774,  130.261 ), ( -0.332, 1.045, -0.027 ) ), # P 1 SE
    ( ( -135.393, 113.721,   -2.688 ), ( -0.028, 0.843, -0.272 ) ), # Q 3
    ( (   96.211, 142.084,  -75.310 ), (  0.171, 0.964, -0.035 ) ), # R 2
    ( ( -134.088,  63.522, -122.543 ), ( -0.036, 1.358,  0.267 ) ), # S 4
    ( ( -133.031,  60.605,  -51.483 ), ( -0.036, 1.358,  0.264 ) ), # T 4
    ]

ROOT = os.path.join(
    libtbx.env.under_dist( "phaser_regression", "data" ),
    "structure",
    )

SLT_EXPECTEDS = [
        mr_object.Generator(
            fold = 5,
            axis = scitbx.matrix.col( ( 0.033, -0.013, 0.999 ) ),
            centre = scitbx.matrix.col( ( -0.235, 0.160, 0.010 ) ),
            ),
        mr_object.Generator(
            fold = 2,
            axis = scitbx.matrix.col( ( 0.815, -0.426, -0.392 ) ),
            centre = scitbx.matrix.col( ( 9.748, -3.214, 23.765 ) ),
            ),
        mr_object.Generator(
            fold = 2,
            axis = scitbx.matrix.col( ( 0.115, 0.925, 0.363 ) ),
            centre = scitbx.matrix.col( ( 32.107, -0.935, -7.756 ) ),
            ),
        mr_object.Generator(
            fold = 2,
            axis = scitbx.matrix.col( ( 0.307, 0.255, 0.917 ) ),
            centre = scitbx.matrix.col( ( 28.528, 10.229, -12.413 ) ),
            ),
        mr_object.Generator(
            fold = 2,
            axis = scitbx.matrix.col( ( -0.426, 0.838, -0.340 ) ),
            centre = scitbx.matrix.col( ( 31.964, 13.547, -6.645 ) ),
            ),
        mr_object.Generator(
            fold = 2,
            axis = scitbx.matrix.col( ( -0.426, -0.459, 0.780 ) ),
            centre = scitbx.matrix.col( ( 25.000, -20.183, 1.774 ) ),
            ),
        ]
SLT_COMPONENT = mr_object.Component( mtype = mmt.PROTEIN, mw = 10000 )
SLT_ENSEMBLE = mr_object.MultifileEnsemble(
    pdbs = [ os.path.join( ROOT, "monomer.pdb" ) ],
    rmsds = [ 0.80 ],
    composition = mr_object.Composition( components = [ SLT_COMPONENT ] )
    )
SLT_STRUCTURE = mr_object.Structure(
    peaks = [
        mr_object.Peak(
            ensemble = SLT_ENSEMBLE,
            rotation = mr_object.Rotation.Euler( angles = p[0] ),
            translation = p[1],
            bfactor = 0
            )
        for p in SLT
        ],
    significant = True,
    )
SLT_CELL = uctbx.unit_cell(
    parameters = [ 127.5, 97.7, 164.2, 90.0, 90.0, 90.0 ]
    )
SLT_SPAC = sgtbx.space_group_info( number = 19 ).group()
SLT_CRYSTAL = matching.CrystalInfo(
    space_group = SLT_SPAC,
    cell = SLT_CELL,
    )
SLT_DMIN = 2.75
CONTEXT = Fake()

#import sys
CONTEXT.output = output.SingleStream(
    stream = None, #sys.stdout,
    level = output.SingleStream.VERBOSE
    )
CONTEXT.superposition = superposition.Proxy(
    method = superposition.simple_ssm_superposition
    )

class TestObjectCompletion(unittest.TestCase):

    def setUp(self):

        self.expert = expert.ObjectCompletion()
        self.context = Fake()

        from phaser import output
        self.context.output = output.SingleStream(
            stream = None,
            level = output.SingleStream.VERBOSE
            )

        from phaser.pipeline import superposition
        self.context.superposition = superposition.Proxy(
            method = superposition.simple_ssm_superposition
            )
        self.slt_component = mr_object.Component( mtype = mmt.PROTEIN, mw = 10000 )
        self.slt_ensemble = mr_object.MultifileEnsemble(
            pdbs = [ os.path.join( ROOT, "monomer.pdb" ) ],
            rmsds = [ 0.80 ],
            composition = mr_object.Composition( components = [ self.slt_component ] )
            )
        self.slt_generator = mr_object.Generator.from_rotation_translation(
            power = 1,
            fold = 5,
            axis = scitbx.matrix.col(
                ( 0.034837143908216656, -0.0012095271938242775, 0.9993922705566046 )
                ),
            translation = scitbx.matrix.col(
                (-0.18640396875067672, 0.3921807979045849, 0.006972372540474437)
                )
            )
        self.slt_assembly = mr_object.Assembly.Simple(
            ensemble = self.slt_ensemble,
            symmetry = mr_object.RotationAxis( axis = self.slt_generator ),
            strategy = mr_object.RefineStrategy(),
            )
        from cctbx import uctbx
        self.slt_cell = uctbx.unit_cell(
            parameters = [ 127.5, 97.7, 164.2, 90.0, 90.0, 90.0 ]
            )
        from cctbx import sgtbx
        self.slt_spac = sgtbx.space_group_info( number = 19 ).group()
        self.slt_crystal = matching.CrystalInfo(
            space_group = self.slt_spac,
            cell = self.slt_cell,
            )
        self.slt_dmin = 2.75
        self.slt_problem = Fake()
        self.slt_problem.data = mr_object.Blackboard()
        self.slt_problem.root = object()
        self.slt_problem.xray_data = Fake()
        self.slt_problem.xray_data.resolution = self.slt_dmin
        self.slt_problem.xray_data.cell = self.slt_cell.parameters()
        self.slt_case = mr_object.Case(
            problem = self.slt_problem,
            composition = None,
            space_group_hall = self.slt_spac.type().hall_symbol()
            )
        self.slt_structure = mr_object.Structure(
            peaks = [
                mr_object.Peak(
                    ensemble = self.slt_ensemble,
                    rotation = mr_object.Rotation.Euler( angles = p[0] ),
                    translation = p[1],
                    bfactor = 0
                    )
                for p in SLT
                ]
            )
        self.slt_multiplier = 3


    def process_assembly(self, initial, generateds):

        peak = mr_object.Peak(
            ensemble = self.slt_ensemble,
            rotation = mr_object.Rotation.Euler( angles = initial[0] ),
            translation = initial[1],
            bfactor = 0
            )
        peaks = self.expert.process_assembly(
            peak = peak,
            assembly = self.slt_assembly,
            cell = self.slt_cell,
            context = self.context
            )

        for ( got, expected ) in zip( peaks, generateds ):
            got_tmf = got[0].matching_mrpeak(
                crystal = self.slt_crystal,
                dmin = self.slt_dmin,
                multiplier = self.slt_multiplier,
                )
            exp_peak = mr_object.Peak(
                ensemble = self.slt_ensemble,
                rotation = mr_object.Rotation.Euler( angles = expected[0] ),
                translation = expected[1],
                bfactor = 0
                )
            exp_tmf = exp_peak.matching_mrpeak(
                crystal = self.slt_crystal,
                dmin = self.slt_dmin,
                multiplier = self.slt_multiplier,
                )
            self.assertTrue( any( got_tmf.operations_between( other = exp_tmf ) ) )


    def test_process_assembly_slt(self):

        self.process_assembly(
            initial = SLT[1],
            generateds = [ SLT[2], SLT[3], SLT[4], SLT[17] ]
            )
        self.process_assembly(
            initial = SLT[0],
            generateds = [ SLT[11], SLT[7], SLT[15], SLT[9] ]
            )


    def test_complete_assemblies_in(self):

        averageds = self.expert.complete_assemblies_in(
            structure = self.slt_structure,
            assemblies = [ self.slt_assembly ],
            case = self.slt_case,
            context = self.context,
            )
        self.assertEqual( averageds, [] )

        # Incomplete
        structure = mr_object.Structure( peaks = self.slt_structure.peaks[1:] )
        averageds = self.expert.complete_assemblies_in(
            structure = structure,
            assemblies = [ self.slt_assembly ],
            case = self.slt_case,
            context = self.context,
            )
        self.assertEqual( len( averageds ), 1 )
        ref = self.slt_structure.peaks[0].matching_mrpeak(
            crystal = self.slt_crystal,
            dmin = self.slt_dmin,
            multiplier = self.slt_multiplier
            )
        other = averageds[0][0].matching_mrpeak(
            crystal = self.slt_crystal,
            dmin = self.slt_dmin,
            multiplier = self.slt_multiplier
            )
        self.assertTrue( any( ref.operations_between( other = other ) ) )

        # Mixture
        ense = mr_object.MultifileEnsemble(
            pdbs = [ os.path.join( ROOT, "monomer.pdb" ) ],
            rmsds = [ 0.80 ],
            composition = mr_object.Composition( components = [ self.slt_component ] )
            )
        template = self.slt_structure.peaks[0]
        peak = mr_object.Peak(
            ensemble = ense,
            rotation = template.rotation,
            translation = template.translation,
            bfactor = 0
            )
        structure = mr_object.Structure(
            peaks = [ peak ] + self.slt_structure.peaks[1:]
            )
        averageds = self.expert.complete_assemblies_in(
            structure = structure,
            assemblies = [ self.slt_assembly ],
            case = self.slt_case,
            context = self.context,
            )
        self.assertEqual( averageds, [] )

        # Mixture and incomplete
        structure = mr_object.Structure(
            peaks = [ peak ] + self.slt_structure.peaks[2:]
            )
        averageds = self.expert.complete_assemblies_in(
            structure = structure,
            assemblies = [ self.slt_assembly ],
            case = self.slt_case,
            context = self.context,
            )
        self.assertEqual( len( averageds ), 1 )
        ref = self.slt_structure.peaks[1].matching_mrpeak(
            crystal = self.slt_crystal,
            dmin = self.slt_dmin,
            multiplier = self.slt_multiplier
            )
        other = averageds[0][0].matching_mrpeak(
            crystal = self.slt_crystal,
            dmin = self.slt_dmin,
            multiplier = self.slt_multiplier
            )
        self.assertTrue( any( ref.operations_between( other = other ) ) )

         # Mixture and incomplete and two assemblies
        assembly = mr_object.Assembly.Simple(
            ensemble = ense,
            symmetry = mr_object.RotationAxis( axis = self.slt_generator ),
            strategy = mr_object.RefineStrategy(),
            )
        averageds = self.expert.complete_assemblies_in(
            structure = structure,
            assemblies = [ self.slt_assembly, assembly ],
            case = self.slt_case,
            context = self.context,
            )
        self.assertEqual( len( averageds ), 2 )
        ref1 = self.slt_structure.peaks[1].matching_mrpeak(
            crystal = self.slt_crystal,
            dmin = self.slt_dmin,
            multiplier = self.slt_multiplier
            )
        template = self.slt_structure.peaks[1]
        repl = mr_object.Peak(
            ensemble = ense,
            rotation = template.rotation,
            translation = template.translation,
            bfactor = 0
            )
        ref2 = repl.matching_mrpeak(
            crystal = self.slt_crystal,
            dmin = self.slt_dmin,
            multiplier = self.slt_multiplier
            )
        other = averageds[0][0].matching_mrpeak(
            crystal = self.slt_crystal,
            dmin = self.slt_dmin,
            multiplier = self.slt_multiplier
            )
        res1_1 = any( ref1.operations_between( other = other ) )
        res2_1 = any( ref2.operations_between( other = other ) )

        other = averageds[1][0].matching_mrpeak(
            crystal = self.slt_crystal,
            dmin = self.slt_dmin,
            multiplier = self.slt_multiplier
            )
        res1_2 = any( ref1.operations_between( other = other ) )
        res2_2 = any( ref2.operations_between( other = other ) )
        self.assertTrue( ( res1_1 or res2_1 ) and ( res1_2 or res2_2 ) )
        self.assertTrue( ( res1_1 is not res2_1 ) and ( res1_2 is not res2_2 ) )


    def test_is_known_failure(self):

        structure = mr_object.Structure(
            peaks = self.slt_structure.peaks[1:]
            )
        peak = self.slt_structure.peaks[0]
        res = self.expert.is_known_failure(
            structure = self.slt_case.create_matching_mr_structure( structure = structure ),
            peak = peak,
            failures = [],
            case = self.slt_case,
            context = self.context,
            )
        self.assertFalse( res )

        fail = (
            self.slt_ensemble,
            self.slt_case.create_matching_mr_peak(
                peak = peak,
                multiplier = self.slt_multiplier,
                ),
            self.slt_case.create_matching_mr_structure( structure = self.slt_structure ),
            )
        res = self.expert.is_known_failure(
            structure = self.slt_case.create_matching_mr_structure( structure = structure ),
            peak = peak,
            failures = [ fail ],
            case = self.slt_case,
            context = self.context,
            )
        self.assertFalse( res )

        fail = (
            self.slt_ensemble,
            self.slt_case.create_matching_mr_peak(
                peak = self.slt_structure.peaks[1],
                multiplier = self.slt_multiplier,
                ),
            self.slt_case.create_matching_mr_structure( structure = structure ),
            )
        res = self.expert.is_known_failure(
            structure = self.slt_case.create_matching_mr_structure( structure = structure ),
            peak = peak,
            failures = [ fail ],
            case = self.slt_case,
            context = self.context,
            )
        self.assertFalse( res )


        fail = (
            self.slt_ensemble,
            self.slt_case.create_matching_mr_peak(
                peak = peak,
                multiplier = self.slt_multiplier,
                ),
            self.slt_case.create_matching_mr_structure(
                structure = mr_object.Structure(
                    peaks = self.slt_structure.peaks[3:]
                    )
                ),
            )
        res = self.expert.is_known_failure(
            structure = self.slt_case.create_matching_mr_structure( structure = structure ),
            peak = peak,
            failures = [ fail ],
            case = self.slt_case,
            context = self.context,
            )
        self.assertTrue( res )

        ense = mr_object.MultifileEnsemble(
            pdbs = [ os.path.join( ROOT, "monomer.pdb" ) ],
            rmsds = [ 0.80 ],
            composition = mr_object.Composition( components = [ self.slt_component ] )
            )
        eq_peak = mr_object.Peak(
            ensemble = ense,
            rotation = peak.rotation,
            translation = peak.translation,
            bfactor = 0
            )
        fail = (
            ense,
            self.slt_case.create_matching_mr_peak(
                peak = eq_peak,
                multiplier = self.slt_multiplier,
                ),
            self.slt_case.create_matching_mr_structure(
                structure = mr_object.Structure(
                    peaks = self.slt_structure.peaks[3:]
                    )
                ),
            )
        res = self.expert.is_known_failure(
            structure = self.slt_case.create_matching_mr_structure( structure = structure ),
            peak = peak,
            failures = [ fail ],
            case = self.slt_case,
            context = self.context,
            )
        self.assertTrue( res )


    def test_call(self):

        res = self.expert(
            board = None,
            case = self.slt_case,
            context = self.context,
            solutions = [],
            postprocessings = []
            )
        self.assertEqual( res, [] )

        sol = Fake()
        sol.structure = mr_object.Structure(
            peaks = self.slt_structure.peaks[1:],
            significant = True
            )
        res = self.expert(
            board = None,
            case = self.slt_case,
            context = self.context,
            solutions = [ sol ],
            postprocessings = []
            )
        self.assertEqual( res, [] )

        reftmf = self.slt_structure.peaks[0].matching_mrpeak(
            crystal = self.slt_crystal,
            dmin = self.slt_dmin,
            multiplier = self.slt_multiplier
            )
        self.slt_case.data.write( obj = self.slt_assembly )
        res = self.expert(
            board = None,
            case = self.slt_case,
            context = self.context,
            solutions = [ sol ],
            postprocessings = []
            )
        self.assertEqual( len( res ), 1 )
        self.assertEqual( res[0].structure, sol.structure )
        tmf = res[0].peak.matching_mrpeak(
            crystal = self.slt_crystal,
            dmin = self.slt_dmin,
            multiplier = self.slt_multiplier
            )
        self.assertTrue( any( reftmf.operations_between( other = tmf ) ) )

        fail = mr_object.FillFailureRecord(
            structure = self.slt_structure,
            peak = self.slt_structure.peaks[0]
            )
        self.slt_case.data.write( obj = fail )
        res = self.expert(
            board = None,
            case = self.slt_case,
            context = self.context,
            solutions = [ sol ],
            postprocessings = []
            )
        self.assertEqual( len( res ), 1 )
        self.assertEqual( res[0].structure, sol.structure )
        tmf = res[0].peak.matching_mrpeak(
            crystal = self.slt_crystal,
            dmin = self.slt_dmin,
            multiplier = self.slt_multiplier
            )
        self.assertTrue( any( reftmf.operations_between( other = tmf ) ) )

        ense = mr_object.MultifileEnsemble(
            pdbs = [ os.path.join( ROOT, "monomer.pdb" ) ],
            rmsds = [ 0.80 ],
            composition = mr_object.Composition( components = [ self.slt_component ] )
            )
        eq_peak = mr_object.Peak(
            ensemble = ense,
            rotation = self.slt_structure.peaks[0].rotation,
            translation = self.slt_structure.peaks[0].translation,
            bfactor = 0
            )
        fail = mr_object.FillFailureRecord(
            structure = sol.structure,
            peak = eq_peak
            )
        self.slt_case.data.write( obj = fail )
        res = self.expert(
            board = None,
            case = self.slt_case,
            context = self.context,
            solutions = [ sol ],
            postprocessings = []
            )
        self.assertEqual( res, [] )


class TestGeneratorSearch(IterableCompareTestCase):

    def setUp(self):

        self.expert = expert.GeneratorSearch(
            tolerances = expert.SymmetryTolerances()
            )

        self.slt_problem = Fake()
        self.slt_problem.data = mr_object.Blackboard()
        self.slt_problem.root = object()
        self.slt_problem.xray_data = Fake()
        self.slt_problem.xray_data.resolution = SLT_DMIN
        self.slt_problem.xray_data.cell = SLT_CELL.parameters()
        self.slt_case = mr_object.Case(
            problem = self.slt_problem,
            composition = None,
            space_group_hall = SLT_SPAC.type().hall_symbol()
            )
        self.board = mr_object.Blackboard()


    def test_potential_assembly_members_for(self):

        # No candidates
        res = self.expert.potential_assembly_members_for(
            reference = SLT_STRUCTURE.peaks[0],
            other = SLT_STRUCTURE.peaks[1],
            case = self.slt_case
            )
        self.assertEqual( res, [] )

        # Same candidate
        res = self.expert.potential_assembly_members_for(
            reference = SLT_STRUCTURE.peaks[0],
            other = SLT_STRUCTURE.peaks[9],
            case = self.slt_case
            )
        self.assertEqual( len(res ), 1 )
        self.assertIterablesAlmostEqual(
            res[0].rotation.matrix(),
            SLT_STRUCTURE.peaks[9].rotation.matrix(),
            3,
            )
        self.assertIterablesAlmostEqual(
            res[0].translation,
            SLT_STRUCTURE.peaks[9].translation,
            3,
            )

        # Symmetry equivalent
        res = self.expert.potential_assembly_members_for(
            reference = SLT_STRUCTURE.peaks[0],
            other = SLT_STRUCTURE.peaks[7],
            case = self.slt_case
            )
        self.assertEqual( len(res ), 1 )
        self.assertIterablesAlmostEqual(
            res[0].rotation.euler(),
            ( 89.713, 41.724, -120.942 ),
            3,
            )
        self.assertIterablesAlmostEqual(
            res[0].translation,
            ( 0.167, 0.451, 0.028 ),
            3,
            )


    def check_generator(self, trial, expected):

        self.assertEqual( trial.fold, expected.fold )
        self.assertAlmostEqual( ( trial.axis - expected.axis ).length(), 0, 1 )
        self.assertAlmostEqual( ( trial.centre - expected.centre ).length() / 2, 0, 0 )


    def check_results(self, relation, ensemble, generator, peaks):

        self.assertEqual( relation.setting.ensemble, ensemble )
        self.check_generator( trial = relation.setting.generator, expected = generator )
        self.assertTrue( relation.relating, peaks )


    def test_find_assemblies_in(self):

        generator = SLT_EXPECTEDS[0]

        # Simple
        results = self.expert.find_assemblies_in(
            structure = mr_object.Structure( peaks = SLT_STRUCTURE.peaks[1:3] ),
            case = self.slt_case,
            context = CONTEXT
            )
        self.assertEqual( len( results ), 1 )
        self.check_results(
            relation = results[0],
            ensemble = SLT_ENSEMBLE,
            generator = generator,
            peaks = SLT_STRUCTURE.peaks[1:3],
            )

        # Different cell
        results = self.expert.find_assemblies_in(
            structure = mr_object.Structure(
                peaks = [ SLT_STRUCTURE.peaks[0], SLT_STRUCTURE.peaks[11] ]
                ),
            case = self.slt_case,
            context = CONTEXT
            )
        self.assertEqual( len( results ), 1 )
        self.check_results(
            relation = results[0],
            ensemble = SLT_ENSEMBLE,
            generator = generator,
            peaks = ( SLT_STRUCTURE.peaks[0], SLT_STRUCTURE.peaks[11] ),
            )

        # Different asu
        results = self.expert.find_assemblies_in(
            structure = mr_object.Structure(
                peaks = [ SLT_STRUCTURE.peaks[0], SLT_STRUCTURE.peaks[7] ]
                ),
            case = self.slt_case,
            context = CONTEXT
            )
        self.assertEqual( len( results ), 1 )
        self.check_results(
            relation = results[0],
            ensemble = SLT_ENSEMBLE,
            generator = generator,
            peaks = ( SLT_STRUCTURE.peaks[0], SLT_STRUCTURE.peaks[7] ),
            )

        # Different ensemble
        ensemble = mr_object.MultifileEnsemble(
            pdbs = [ os.path.join( ROOT, "monomer.pdb" ) ],
            rmsds = [ 0.80 ],
            composition = mr_object.Composition( components = [ SLT_COMPONENT ] )
            )
        peak = mr_object.Peak(
            ensemble = ensemble,
            rotation = SLT_STRUCTURE.peaks[7].rotation,
            translation = SLT_STRUCTURE.peaks[7].translation,
            bfactor = 0
            )
        results = self.expert.find_assemblies_in(
            structure = mr_object.Structure(
                peaks = [ SLT_STRUCTURE.peaks[0], peak ]
                ),
            case = self.slt_case,
            context = CONTEXT
            )
        self.assertEqual( len( results ), 1 )
        self.check_results(
            relation = results[0],
            ensemble = SLT_ENSEMBLE,
            generator = generator,
            peaks = ( SLT_STRUCTURE.peaks[0], peak ),
            )

        results = self.expert.find_assemblies_in(
            structure = mr_object.Structure(
                peaks = [ peak, SLT_STRUCTURE.peaks[0] ]
                ),
            case = self.slt_case,
            context = CONTEXT
            )
        self.assertEqual( len( results ), 1 )
        self.check_results(
            relation = results[0],
            ensemble = ensemble,
            generator = generator,
            peaks = ( peak, SLT_STRUCTURE.peaks[0] ),
            )

        # Unrelated
        results = self.expert.find_assemblies_in(
            structure = mr_object.Structure( peaks = SLT_STRUCTURE.peaks[:2] ),
            case = self.slt_case,
            context = CONTEXT
            )
        self.assertEqual( len( results ), 0 )


    def prepare_board_with(self, structures):

        for ( i, s ) in enumerate( structures, start = 1 ):
            label =  mr_object.ForSymmetryAnalysis( structure = s, index = i )
            self.board.write( obj = label )


    def do_calculation(self):

        self.expert(
            board = self.board,
            case = self.slt_case,
            context = CONTEXT,
            )


    @staticmethod
    def get_test_structures():

        return [ SLT_STRUCTURE, mr_object.Structure( peaks = SLT_STRUCTURE.peaks[1:17] ) ]


    def test_call(self):

        self.do_calculation()
        self.assertEqual( self.board.read( type = mr_object.StructureRelations ), [] )

        structures = self.get_test_structures()
        self.prepare_board_with( structures = structures )
        self.do_calculation()
        analyses = self.board.read( type = mr_object.StructureRelations )
        self.assertEqual( len( analyses ), 2 )
        analysis_for = dict( [ ( a.structure, a ) for a in analyses ] )
        self.assertTrue( structures[0] in analysis_for )
        analysis = analysis_for[ structures[0] ]
        self.assertEqual( len( analysis.relations ), 45 )
        self.assertTrue(
            all( rel.setting.ensemble == SLT_ENSEMBLE for rel in analysis.relations )
            )

        igens = [ r.setting.generator for r in analysis.relations ]
        fivefolds = ( igens[:7] + igens[8:13] + igens[15:16] + igens[17:22]
            + igens[23:] )

        for gen in fivefolds:
            self.check_generator( trial = gen, expected = SLT_EXPECTEDS[0] )

        self.check_generator( trial = igens[7], expected = SLT_EXPECTEDS[4] )
        self.check_generator( trial = igens[13], expected = SLT_EXPECTEDS[1] )
        self.check_generator( trial = igens[14], expected = SLT_EXPECTEDS[5] )
        self.check_generator( trial = igens[16], expected = SLT_EXPECTEDS[3] )
        self.check_generator( trial = igens[22], expected = SLT_EXPECTEDS[2] )

        self.assertTrue( structures[1] in analysis_for )
        analysis = analysis_for[ structures[1] ]
        self.assertEqual( len( analysis.relations ), 30 )
        self.assertTrue(
            all( rel.setting.ensemble == SLT_ENSEMBLE for rel in analysis.relations )
            )

        igens = [
            r.setting.generator for r in analysis.relations
            ]
        fivefolds = ( igens[:3] + igens[4:7] + igens[10:14] + igens[15:] )

        for gen in fivefolds:
            self.check_generator( trial = gen, expected = SLT_EXPECTEDS[0] )

        self.check_generator( trial = igens[3], expected = SLT_EXPECTEDS[4] )
        self.check_generator( trial = igens[7], expected = SLT_EXPECTEDS[1] )
        self.check_generator( trial = igens[8], expected = SLT_EXPECTEDS[5] )
        self.check_generator( trial = igens[9], expected = SLT_EXPECTEDS[3] )
        self.check_generator( trial = igens[14], expected = SLT_EXPECTEDS[2] )


class TestGeneratorClassifyAverage(IterableCompareTestCase):

    def setUp(self):

        self.expert = expert.GeneratorClassifyAverage(
            tolerances = expert.SymmetryTolerances()
            )

        self.slt_problem = Fake()
        self.slt_problem.data = mr_object.Blackboard()
        self.slt_problem.root = object()
        self.slt_problem.xray_data = Fake()
        self.slt_problem.xray_data.resolution = SLT_DMIN
        self.slt_problem.xray_data.cell = SLT_CELL.parameters()
        self.slt_case = mr_object.Case(
            problem = self.slt_problem,
            composition = None,
            space_group_hall = SLT_SPAC.type().hall_symbol()
            )


    def check_averaged_assemblies(self, expected, got):

        self.assertEqual( len( expected ), len( got ) )

        for ( average, members ) in expected.items():
            equivs = list(
                cluster for ( genset, cluster ) in got.items()
                if ( average.ensemble == genset.ensemble
                    and average.generator.fold == genset.generator.fold
                    and average.generator.approx_equal_spatially(
                        other = genset.generator,
                        max_misalignment = 0.001,
                        max_spatial_error = 0.01,
                        )
                    )
                )
            self.assertEqual( len( equivs ), 1 )
            self.assertEqual( members, equivs[0] )


    def test_merge_equivalent_assemblies(self):

        gen_search = expert.GeneratorSearch(
            tolerances = expert.SymmetryTolerances()
            )

        srel1 = mr_object.StructureRelations(
            structure = SLT_STRUCTURE,
            relations = gen_search.find_assemblies_in(
                structure = SLT_STRUCTURE,
                case = self.slt_case,
                context = CONTEXT
                )
            )
        rels = srel1.relations
        cluster_of = self.expert.merge_equivalent_assemblies(
            relations = rels,
            context = CONTEXT
            )

        members_for = {
            mr_object.GeneratorSetting( ensemble = SLT_ENSEMBLE, generator = SLT_EXPECTEDS[0] ):
                set( rels[:7] + rels[8:13] + rels[15:16] + rels[17:22] + rels[23:] ),
            mr_object.GeneratorSetting( ensemble = SLT_ENSEMBLE, generator = SLT_EXPECTEDS[1] ):
                set( [ rels[13] ] ),
            mr_object.GeneratorSetting( ensemble = SLT_ENSEMBLE, generator = SLT_EXPECTEDS[2] ):
                set( [ rels[22] ] ),
            mr_object.GeneratorSetting( ensemble = SLT_ENSEMBLE, generator = SLT_EXPECTEDS[3] ):
                set( [ rels[16] ] ),
            mr_object.GeneratorSetting( ensemble = SLT_ENSEMBLE, generator = SLT_EXPECTEDS[4] ):
                set( [ rels[7] ] ),
            mr_object.GeneratorSetting( ensemble = SLT_ENSEMBLE, generator = SLT_EXPECTEDS[5] ):
                set( [ rels[14] ] ),
            }
        self.check_averaged_assemblies(
            expected = members_for,
            got = dict( ( key, set( value ) ) for ( key, value ) in cluster_of.items() ),
            )


    def test_call(self):

        board = mr_object.Blackboard()
        self.expert(
            board = board,
            case = self.slt_case,
            context = CONTEXT,
            )
        self.assertEqual( board.read( type = mr_object.GeneratorCluster ), [] )

        gst = TestGeneratorSearch( methodName = "do_calculation" )
        gst.setUp()
        structures = gst.get_test_structures()
        gst.prepare_board_with( structures = structures )
        gst.do_calculation()
        rels = gst.board.read( type = mr_object.StructureRelations )
        self.assertEqual( len( rels ), 2 )
        self.expert(
            board = gst.board,
            case = self.slt_case,
            context = CONTEXT,
            )
        clusters = gst.board.read( type = mr_object.GeneratorCluster )
        self.assertEqual( len( clusters ), 6 )
        analyses = gst.board.read( type = mr_object.StructureRelations )
        self.assertEqual( len( analyses ), 2 )
        relations_for = dict( [ ( an.structure, an.relations ) for an in analyses ] )
        self.assertTrue( structures[0] in relations_for )
        self.assertTrue( structures[1] in relations_for )
        rels1 = relations_for[ structures[0] ]
        rels2 = relations_for[ structures[1] ]

        expected = {
            mr_object.GeneratorSetting( ensemble = SLT_ENSEMBLE, generator = SLT_EXPECTEDS[0] ):
                {
                    structures[0]: set( rels1[:7] + rels1[8:13] + rels1[15:16] + rels1[17:22] + rels1[23:] ),
                    structures[1]: set( rels2[:3] + rels2[4:7] + rels2[10:14] + rels2[15:] ),
                    },
            mr_object.GeneratorSetting( ensemble = SLT_ENSEMBLE, generator = SLT_EXPECTEDS[1] ):
                {
                    structures[0]: set( [ rels1[13] ] ),
                    structures[1]: set( [ rels2[7] ] ),
                    },
            mr_object.GeneratorSetting( ensemble = SLT_ENSEMBLE, generator = SLT_EXPECTEDS[2] ):
                {
                    structures[0]: set( [ rels1[22] ] ),
                    structures[1]: set( [ rels2[14] ] ),
                    },
            mr_object.GeneratorSetting( ensemble = SLT_ENSEMBLE, generator = SLT_EXPECTEDS[3] ):
                {
                    structures[0]: set( [ rels1[16] ] ),
                    structures[1]: set( [ rels2[9] ] ),
                    },
            mr_object.GeneratorSetting( ensemble = SLT_ENSEMBLE, generator = SLT_EXPECTEDS[4] ):
                {
                    structures[0]: set( [ rels1[7] ] ),
                    structures[1]: set( [ rels2[3] ] ),
                    },
            mr_object.GeneratorSetting( ensemble = SLT_ENSEMBLE, generator = SLT_EXPECTEDS[5] ):
                {
                    structures[0]: set( [ rels1[14] ] ),
                    structures[1]: set( [ rels2[8] ] ),
                    },
            }
        self.assertTrue( all( len( clus.relations_from ) == 2 for clus in clusters ) )
        self.assertTrue( all( structures[0] in clus.relations_from for clus in clusters ) )
        self.assertTrue( all( structures[1] in clus.relations_from for clus in clusters ) )
        self.check_averaged_assemblies(
            expected = expected,
            got = dict(
                (
                    clus.average,
                    {
                        structures[0]: set( clus.relations_from[ structures[0] ] ),
                        structures[1]: set( clus.relations_from[ structures[1] ] ),
                        },
                    )
                for clus in clusters
                ),
            )


class TestPointGroupDetermine(IterableCompareTestCase):

    def setUp(self):

        self.expert = expert.PointGroupDetermine(
            tolerances = expert.SymmetryTolerances()
            )

        self.structure1 = object()
        self.structure2 = object()
        self.relations1 = [
            [ object(), object() ],
            [ object(), object() ],
            [ object(), object() ],
            [ object(), object() ],
            [ object(), object() ],
            [ object(), object() ],
            ]
        self.relations2 = [
            [ object(), object() ],
            [ object(), object() ],
            [ object(), object() ],
            [ object(), object() ],
            [ object(), object() ],
            [ object(), object() ],
            ]

        self.gclusters = [
            self.create_generator_cluster( generator = g, relations1 = r1, relations2 = r2 )
            for ( g, r1, r2 ) in zip( SLT_EXPECTEDS, self.relations1, self.relations2 )
            ]


    def create_generator_cluster(self, generator, relations1, relations2):

        gcluster = mr_object.GeneratorCluster(
            average = mr_object.GeneratorSetting(
                ensemble = SLT_ENSEMBLE,
                generator = generator,
                )
            )

        for r in relations1:
            gcluster.add( structure = self.structure1, relation = r )

        for r in relations2:
            gcluster.add( structure = self.structure2, relation = r )

        return gcluster


    def test_find_rotation_axes(self):

        results = self.expert.find_rotation_axes( clusters = [ self.gclusters[0] ] )

        self.assertEqual( len( results ), 1 )
        self.assertTrue( isinstance( results[0], mr_object.SymmetryAnalysis ) )
        self.assertEqual( results[0].setting.ensemble, self.gclusters[0].average.ensemble )
        self.assertTrue( isinstance( results[0].setting.point_group, mr_object.RotationAxis ) )
        self.assertEqual( results[0].setting.point_group.axis, self.gclusters[0].average.generator )
        self.assertEqual( len( results[0].relations_from ), 2 )
        self.assertTrue( self.structure1 in results[0].relations_from )
        self.assertTrue( self.structure2 in results[0].relations_from )
        self.assertEqual(
            results[0].relations_from[ self.structure1 ],
            self.relations1[0],
            )
        self.assertEqual(
            results[0].relations_from[ self.structure2 ],
            self.relations2[0],
            )
        self.assertEqual(
            results[0].formed_by,
            set( [ self.gclusters[0] ] )
            )


    def test_call(self):

        board = mr_object.Blackboard()
        self.expert(
            board = board,
            case = None,
            context = CONTEXT,
            )
        self.assertEqual( board.read( type = mr_object.SymmetryAnalysis ), [] )

        for gcl in self.gclusters:
            board.write( obj = gcl )

        self.expert(
            board = board,
            case = None,
            context = CONTEXT,
            )
        symms = board.read( type = mr_object.SymmetryAnalysis )
        self.assertEqual( len( symms ), 6 )
        self.assertTrue(
            all( isinstance( sa.setting.point_group, mr_object.RotationAxis ) for sa in symms )
            )
        symm_analysis_for = dict( [ ( sa.setting.point_group.axis, sa ) for sa in symms ] )

        for ( op, mols1, mols2, gcl ) in zip( SLT_EXPECTEDS, self.relations1, self.relations2, self.gclusters ):
            self.assertTrue( op in symm_analysis_for )
            trial = symm_analysis_for[ op ]
            self.assertEqual( trial.setting.ensemble, SLT_ENSEMBLE )
            self.assertEqual( len( trial.relations_from ), 2 )
            self.assertTrue( self.structure1 in trial.relations_from )
            self.assertTrue( self.structure2 in trial.relations_from )
            self.assertEqual(
                trial.formed_by,
                set( [ gcl ] ),
                )
            self.assertEqual( trial.relations_from[ self.structure1 ], mols1 )
            self.assertEqual( trial.relations_from[ self.structure2 ], mols2 )


class TestFindSymmetryRelatedAssemblies(unittest.TestCase):

    def fake_model_relations(self, relations):

        model_relations = []

        for r in relations:
            rel = Fake()
            rel.relating = r
            model_relations.append( rel )

        return model_relations


    def calculate_and_test(self, relations, assemblies):

        saa = expert.find_symmetry_related_assemblies(
            relations = self.fake_model_relations( relations = relations )
            )
        self.assertEqual( saa, set( frozenset( a ) for a in assemblies ) )


    def test_1(self):

        relateds = [
            [
                [ 0, 7 ], [ 7, 9 ], [ 9, 11 ], [ 11, 15 ],
                [ 1, 2 ], [ 1, 3 ], [ 1, 4 ], [ 1, 17 ],
                [ 5, 6 ], [ 12, 14 ], [ 16, 12 ], [ 6, 14 ],
                [ 8, 10 ], [ 18, 13 ], [ 13, 19 ], [ 18, 10 ],
                ],
            [ [ 3, 5 ] ],
            [ [ 6, 9 ] ],
            [ [ 4, 16 ] ],
            [ [ 1, 12 ] ],
            [ [ 3, 15 ] ],
            ]

        assemblies = [
            [ [ 0, 7, 9, 11, 15 ], [ 1, 2, 3, 4, 17 ], [ 5, 6, 12, 14, 16 ], [ 8, 10, 13, 18, 19 ] ],
            [ [ 3, 5 ] ],
            [ [ 6, 9 ] ],
            [ [ 4, 16 ] ],
            [ [ 1, 12 ] ],
            [ [ 3, 15 ] ],
            ]

        for ( relations, assems ) in zip( relateds, assemblies ):
            self.calculate_and_test( relations = relations, assemblies = assems )


    def test_2(self):

        relateds = [
            [
                [ 7, 9 ], [ 9, 11 ], [ 11, 15 ],
                [ 1, 2 ], [ 1, 3 ], [ 1, 4 ],
                [ 5, 6 ], [ 12, 14 ], [ 16, 12 ], [ 6, 14 ],
                [ 8, 10 ], [ 13, 10 ],
                ],
            [ [ 3, 5 ] ],
            [ [ 6, 9 ] ],
            [ [ 4, 16 ] ],
            [ [ 1, 12 ] ],
            [ [ 3, 15 ] ],
            ]
        assemblies = [
            [ [ 7, 9, 11, 15 ], [ 1, 2, 3, 4 ], [ 5, 6, 12, 14, 16 ], [ 8, 10, 13 ] ],
            [ [ 3, 5 ] ],
            [ [ 6, 9 ] ],
            [ [ 4, 16 ] ],
            [ [ 1, 12 ] ],
            [ [ 3, 15 ] ],
            ]

        for ( relations, assems ) in zip( relateds, assemblies ):
            self.calculate_and_test( relations = relations, assemblies = assems )


class TestSymmetryCompare(unittest.TestCase):

    def setUp(self):

        self.expert = expert.SymmetryCompare(
            tolerances = expert.SymmetryTolerances()
            )
        self.ref1 = mr_object.SymmetrySetting(
            ensemble = SLT_ENSEMBLE,
            point_group = mr_object.RotationAxis( axis = SLT_EXPECTEDS[0] )
            )
        self.ref2 = mr_object.SymmetrySetting(
            ensemble = SLT_ENSEMBLE,
            point_group = mr_object.RotationAxis( axis = SLT_EXPECTEDS[1] )
            )

        self.case = Fake()
        self.case.data = mr_object.Blackboard()
        self.case.problem = Fake()
        self.case.problem.data = mr_object.Blackboard()


    def test_determine_containment_relationship(self):

        ensemble = mr_object.MultifileEnsemble(
            pdbs = [ os.path.join( ROOT, "monomer.pdb" ) ],
            rmsds = [ 0.80 ],
            composition = mr_object.Composition( components = [ SLT_COMPONENT ] )
            )

        trial = mr_object.SymmetrySetting(
            ensemble = ensemble,
            point_group = mr_object.RotationAxis( axis =  SLT_EXPECTEDS[0] )
            )

        self.assertEqual(
            self.expert.determine_containment_relationship(
                existing = self.ref1,
                trial = trial,
                context = CONTEXT,
                ),
            ( True, True )
            )
        self.assertEqual(
            self.expert.determine_containment_relationship(
                existing = self.ref2,
                trial = trial,
                context = CONTEXT,
                ),
            ( False, False )
            )

        tenfold = mr_object.Generator(
            fold = 10,
            axis = SLT_EXPECTEDS[0].axis,
            centre = SLT_EXPECTEDS[0].centre
            )
        trial = mr_object.SymmetrySetting(
            ensemble = ensemble,
            point_group = mr_object.RotationAxis( axis =  tenfold )
            )
        self.assertEqual(
            self.expert.determine_containment_relationship(
                existing = self.ref1,
                trial = trial,
                context = CONTEXT,
                ),
            ( False, True )
            )
        self.assertEqual(
            self.expert.determine_containment_relationship(
                existing = trial,
                trial = self.ref1,
                context = CONTEXT,
                ),
            ( True, False )
            )

        ensemble = mr_object.MultifileEnsemble(
            pdbs = [ os.path.join( ROOT, "monomer.pdb" ) ],
            rmsds = [ 0.80 ],
            composition = mr_object.Composition(
                components = [ mr_object.Component( mtype = mmt.PROTEIN, mw = 10000 ) ]
                )
            )

        trial = mr_object.SymmetrySetting(
            ensemble = ensemble,
            point_group = mr_object.RotationAxis( axis =  SLT_EXPECTEDS[0] )
            )
        self.assertRaises(
            RuntimeError,
            self.expert.determine_containment_relationship,
            existing = self.ref1,
            trial = trial,
            context = CONTEXT,
            )


    def test_call(self):

        board = mr_object.Blackboard()
        self.expert(
            board = board,
            case = self.case,
            context = CONTEXT,
            )
        self.assertEqual( board.read( type = mr_object.SymmetrySetRelationAnalysis ), [] )

        self.case.data.write( obj = self.ref1 )
        self.case.data.write( obj = self.ref2 )

        trials = [
            mr_object.SymmetryAnalysis(
                setting = mr_object.SymmetrySetting(
                    ensemble = SLT_ENSEMBLE,
                    point_group = mr_object.RotationAxis( axis = SLT_EXPECTEDS[0] )
                    ),
                relations_from = {},
                formed_by = [],
                ),
            mr_object.SymmetryAnalysis(
                setting = mr_object.SymmetrySetting(
                    ensemble = SLT_ENSEMBLE,
                    point_group = mr_object.RotationAxis( axis = SLT_EXPECTEDS[1] )
                    ),
                relations_from = {},
                formed_by = [],
                ),
            mr_object.SymmetryAnalysis(
                setting = mr_object.SymmetrySetting(
                    ensemble = SLT_ENSEMBLE,
                    point_group = mr_object.RotationAxis( axis = SLT_EXPECTEDS[2] )
                    ),
                relations_from = {},
                formed_by = [],
                ),
            ]

        for t in trials:
            board.write( obj = t )

        self.expert(
            board = board,
            case = self.case,
            context = CONTEXT,
            )

        relations = board.read( type = mr_object.SymmetrySetRelationAnalysis )
        self.assertEqual( len( relations ), 3 )
        equiv_for = dict( [ ( r.analysis, r ) for r in relations ] )
        self.assertTrue( trials[0] in equiv_for )
        self.assertEqual( equiv_for[ trials[0] ].contained_by, [ self.ref1 ] )
        self.assertEqual( equiv_for[ trials[0] ].contains, [ self.ref1 ] )
        self.assertTrue( trials[1] in equiv_for )
        self.assertEqual( equiv_for[ trials[1] ].contained_by, [ self.ref2 ] )
        self.assertEqual( equiv_for[ trials[1] ].contains, [ self.ref2 ] )
        self.assertTrue( trials[2] in equiv_for )
        self.assertEqual( equiv_for[ trials[2] ].contained_by, [] )
        self.assertEqual( equiv_for[ trials[2] ].contains, [] )


class TestSymmetryCreationPolicy(unittest.TestCase):

    def setUp(self):

        self.expert = expert.SymmetryCreationPolicy()
        self.case = Fake()
        self.case.data = mr_object.Blackboard()
        self.expert_fsra = expert.find_symmetry_related_assemblies


    def tearDown(self):

         expert.find_symmetry_related_assemblies = self.expert_fsra


    def test_call(self):

        structure1 = object()
        structure2 = object()
        board = mr_object.Blackboard()
        setting1 = mr_object.SymmetrySetting( ensemble = object(), point_group = object() )
        sra1 = mr_object.SymmetrySetRelationAnalysis(
            analysis = mr_object.SymmetryAnalysis(
                setting = setting1,
                relations_from = {
                    structure1: object(),
                    structure2: object(),
                    },
                formed_by = []
                ),
            contains = [ object() ],
            contained_by = []
            )
        board.write( obj = sra1 )

        setting2 = mr_object.SymmetrySetting( ensemble = object(), point_group = object() )
        sra2 = mr_object.SymmetrySetRelationAnalysis(
            analysis = mr_object.SymmetryAnalysis(
                setting = setting2,
                relations_from = {
                    structure1: object()
                    },
                formed_by = []
                ),
            contains = [],
            contained_by = [ object() ]
            )
        board.write( obj = sra2 )
        assemblies_for = {
            sra1.analysis.relations_from[ structure1 ]: object(),
            sra1.analysis.relations_from[ structure2 ]: object(),
            }
        expert.find_symmetry_related_assemblies = lambda relations: assemblies_for[ relations ]

        self.expert(
            board = board,
            case = self.case,
            context = CONTEXT,
            )
        nsyms = board.read( type = mr_object.NewSymmetry )
        self.assertEqual( len( nsyms ), 1 )
        self.assertEqual( nsyms[0].analysis, sra1 )
        self.assertEqual( nsyms[0].assemblies_in,
            {
                structure1: assemblies_for[ sra1.analysis.relations_from[ structure1 ] ],
                structure2: assemblies_for[ sra1.analysis.relations_from[ structure2 ] ],
                }
            )
        syms = self.case.data.read( type = mr_object.SymmetrySetting )
        self.assertEqual( syms, [ setting1 ] )


class TestSymmetrySubgroupPolicy(unittest.TestCase):

    def setUp(self):

        self.expert = expert.SymmetrySubgroupPolicy()
        self.case = Fake()
        self.case.data = mr_object.Blackboard()


    def test_call(self):

        settings = [
            mr_object.SymmetrySetting( ensemble = object(), point_group = object() ),
            mr_object.SymmetrySetting( ensemble = object(), point_group = object() ),
            mr_object.SymmetrySetting( ensemble = object(), point_group = object() ),
            mr_object.SymmetrySetting( ensemble = object(), point_group = object() ),
            mr_object.SymmetrySetting( ensemble = object(), point_group = object() ),
            ]

        for s in settings:
            self.case.data.write( obj = s )

        analysis = Fake()
        analysis.contains = [ settings[1] ]
        analysis.analysis = Fake()
        analysis.analysis.setting = object()

        an2 = Fake()
        an2.contains = [ settings[3], settings[4] ]
        an2.analysis = Fake()
        an2.analysis.setting = object()

        an3 = Fake()
        an3.contains = [ settings[1] ]
        an3.analysis = Fake()
        an3.analysis.setting = object()

        board = mr_object.Blackboard()
        board.write( obj = mr_object.NewSymmetry( analysis = analysis, assemblies_in = None ) )
        board.write( obj = mr_object.NewSymmetry( analysis = an2, assemblies_in = None ) )
        board.write( obj = mr_object.NewSymmetry( analysis = an3, assemblies_in = None ) )

        self.expert(
            board = board,
            case = self.case,
            context = CONTEXT,
            )
        symsettings = self.case.data.read( type = mr_object.SymmetrySetting )
        self.assertEqual( set( symsettings ), set( [ settings[0], settings[2] ] ) )
        notices = board.read( type = mr_object.ObsoletedBySuperSymmetry )
        self.assertEqual( len( notices ), 4 )
        ssym_for = {}

        for n in notices:
            ssym_for.setdefault( n.setting, [] ).append( n.supersymmetry )

        self.assertTrue( settings[1] in ssym_for )
        self.assertEqual(
            set( ssym_for[ settings[1] ] ),
            set( [ analysis.analysis.setting, an3.analysis.setting ] ),
            )
        self.assertTrue( settings[3] in ssym_for )
        self.assertEqual( ssym_for[ settings[3] ], [ an2.analysis.setting ] )
        self.assertTrue( settings[4] in ssym_for )
        self.assertEqual( ssym_for[ settings[4] ], [ an2.analysis.setting ] )


class TestSymmetryUnsupportedPolicy(unittest.TestCase):

    def setUp(self):

        self.expert = expert.SymmetryUnsupportedPolicy()
        self.case = Fake()
        self.case.data = mr_object.Blackboard()


    def test_call(self):

        settings = [
            mr_object.SymmetrySetting( ensemble = object(), point_group = object() ),
            mr_object.SymmetrySetting( ensemble = object(), point_group = object() ),
            mr_object.SymmetrySetting( ensemble = object(), point_group = object() ),
            ]

        for s in settings:
            self.case.data.write( obj = s )

        pg1 = Fake()
        pg1.series = Return( values = [ ( 1, 2 ) ] )
        pg2 = Fake()
        pg2.series = Return( values = [ ( 1, 2, 3 ) ] )
        an1 = Fake()
        an1.setting = mr_object.SymmetrySetting(
            ensemble = object(),
            point_group = pg1,
            )
        an2 = Fake()
        an2.setting = mr_object.SymmetrySetting(
            ensemble = object(),
            point_group = pg2,
            )

        ssras = [
            mr_object.SymmetrySetRelationAnalysis(
                analysis = object(),
                contains = [ settings[0] ],
                contained_by = [],
                ),
            mr_object.SymmetrySetRelationAnalysis(
                analysis = an1,
                contains = [],
                contained_by = [ settings[1] ],
                ),
            mr_object.SymmetrySetRelationAnalysis(
                analysis = an2,
                contains = [],
                contained_by = [ settings[1] ],
                ),
            ]
        board = mr_object.Blackboard()

        for s in ssras:
            board.write( obj = s )

        self.expert(
            board = board,
            case = self.case,
            context = CONTEXT,
            )
        symsettings = self.case.data.read( type = mr_object.SymmetrySetting )
        self.assertEqual( set( symsettings ), set( [ settings[0], an2.setting ] ) )
        dgs = board.read( type = mr_object.SymmetryDowngrade )
        self.assertEqual( len( dgs ), 2 )
        dg_for = dict( [ ( dg.setting, dg.highest ) for dg in dgs ] )
        self.assertTrue( settings[1] in dg_for )
        self.assertEqual( dg_for[ settings[1] ], an2.setting )
        self.assertTrue( settings[2] in dg_for )
        self.assertEqual( dg_for[ settings[2] ], None )


class TestSymmetryUpdate(unittest.TestCase):

    def setUp(self):

        self.expert = expert.SymmetryUpdate(
            tolerances = expert.SymmetryTolerances()
            )
        self.ref1 = mr_object.SymmetrySetting(
            ensemble = SLT_ENSEMBLE,
            point_group = mr_object.RotationAxis( axis = SLT_EXPECTEDS[0] )
            )
        self.ref2 = mr_object.SymmetrySetting(
            ensemble = SLT_ENSEMBLE,
            point_group = mr_object.RotationAxis( axis = SLT_EXPECTEDS[1] )
            )

        self.case = Fake()
        self.case.data = mr_object.Blackboard()
        self.expert_fsra = expert.find_symmetry_related_assemblies


    def tearDown(self):

         expert.find_symmetry_related_assemblies = self.expert_fsra


    def test_updated_point_group(self):

        result = self.expert.updated_point_group(
            symmetry = self.ref1,
            relateds = set(
                [
                    mr_object.GeneratorCluster(
                        average = mr_object.GeneratorSetting(
                            ensemble = SLT_ENSEMBLE,
                            generator = SLT_EXPECTEDS[0],
                            ),
                        ),
                    mr_object.GeneratorCluster(
                        average = mr_object.GeneratorSetting(
                            ensemble = SLT_ENSEMBLE,
                            generator = SLT_EXPECTEDS[1],
                            ),
                        ),
                    mr_object.GeneratorCluster(
                        average = mr_object.GeneratorSetting(
                            ensemble = SLT_ENSEMBLE,
                            generator = mr_object.Generator(
                                fold = 2 * SLT_EXPECTEDS[0].fold,
                                axis = SLT_EXPECTEDS[0].axis,
                                centre = SLT_EXPECTEDS[0].centre,
                                ),
                            ),
                        ),
                    ]
                ),
            context = CONTEXT,
            )
        self.assertTrue( isinstance( result, mr_object.RotationAxis ) )
        self.assertTrue( result.axis is not self.ref1.point_group.axis )
        self.assertTrue(
            result.axis.approx_equal_spatially(
                other = SLT_EXPECTEDS[0],
                max_misalignment = 0.99,
                max_spatial_error = 1.0,
                )
            )
        self.assertEqual( result.axis.fold, self.ref1.point_group.axis.fold )

        result = self.expert.updated_point_group(
            symmetry = self.ref1,
            relateds = set(),
            context = CONTEXT,
            )
        self.assertTrue( isinstance( result, mr_object.RotationAxis ) )
        self.assertTrue( result.axis is self.ref1.point_group.axis )


    def test_call(self):

        self.case.data.write( obj = self.ref1 )
        self.case.data.write( obj = self.ref2 )
        sa = Fake()
        sa.formed_by = set( [ object() ] )
        structure = object()
        sa.relations_from = {
            structure: [ object() ],
            }
        assemblies = object()
        expert.find_symmetry_related_assemblies = Return( values = [ assemblies ] )

        srel = mr_object.SymmetrySetRelationAnalysis(
            analysis = sa,
            contained_by = [ self.ref1 ],
            contains = [ self.ref1 ]
            )
        board = mr_object.Blackboard()
        board.write( obj = srel )
        updated = object()
        original = self.ref1.point_group
        self.expert.updated_point_group = Return( values = [ updated ] )
        self.expert(
            board = board,
            case = self.case,
            context = CONTEXT,
            )
        settings = self.case.data.read( type = mr_object.SymmetrySetting )
        self.assertEqual( len( settings ), 2 )
        self.assertTrue( self.ref2 in settings )
        self.assertTrue( self.ref1 in settings )
        self.assertEqual( self.ref1.ensemble, SLT_ENSEMBLE )
        self.assertEqual( self.ref1.point_group, updated )

        records = board.read( type = mr_object.SymmetryUpdateRecord )
        self.assertEqual( len( records ), 1 )
        self.assertEqual( records[0].setting, self.ref1 )
        self.assertEqual( records[0].previous, original )
        self.assertEqual( records[0].assemblies_in, { structure: assemblies } )
        self.assertEqual(
            expert.find_symmetry_related_assemblies._calls,
            [ ( (), { "relations": sa.relations_from[ structure ] } ) ],
            )

        self.assertEqual(
            self.expert.updated_point_group._calls,
            [ ( (), { "symmetry": self.ref1, "relateds": sa.formed_by, "context": CONTEXT } ) ]
            )


class TestLocalSymmetryExpert(unittest.TestCase):

    def setUp(self):

        self.slt_problem = Fake()
        self.slt_problem.data = mr_object.Blackboard()
        self.slt_problem.root = object()
        self.slt_problem.xray_data = Fake()
        self.slt_problem.xray_data.resolution = SLT_DMIN
        self.slt_problem.xray_data.cell = SLT_CELL.parameters()
        self.case = mr_object.Case(
            problem = self.slt_problem,
            composition = None,
            space_group_hall = SLT_SPAC.type().hall_symbol()
            )


    def select(self, array, type):

        return [ obj for obj in array if isinstance( obj, type ) ]


    def test_setup(self):

        e = expert.LocalSymmetryExpert()
        self.assertEqual(
            len( self.select( array = e.actions, type = expert.SymmetryCreationPolicy ) ),
            1
            )
        self.assertEqual(
            len( self.select( array = e.actions, type = expert.SymmetrySubgroupPolicy ) ),
            1
            )
        self.assertEqual(
            len( self.select( array = e.actions, type = expert.SymmetryUnsupportedPolicy ) ),
            1
            )
        self.assertEqual(
            len( self.select( array = e.actions, type = expert.SymmetryUpdate ) ),
            1
            )
        count = len( e.actions )

        e = expert.LocalSymmetryExpert( add_new_symmetry = False )
        self.assertEqual( len( e.actions ), count - 1 )
        self.assertEqual(
            len( self.select( array = e.actions, type = expert.SymmetryCreationPolicy ) ),
            0
            )

        e = expert.LocalSymmetryExpert( remove_subgroup_symmetry = False )
        self.assertEqual( len( e.actions ), count - 1 )
        self.assertEqual(
            len( self.select( array = e.actions, type = expert.SymmetrySubgroupPolicy ) ),
            0
            )

        e = expert.LocalSymmetryExpert( remove_unsupported_symmetry = False )
        self.assertEqual( len( e.actions ), count - 1 )
        self.assertEqual(
            len( self.select( array = e.actions, type = expert.SymmetryUnsupportedPolicy ) ),
            0
            )

        e = expert.LocalSymmetryExpert( update_known_symmetry = False )
        self.assertEqual( len( e.actions ), count - 1 )
        self.assertEqual(
            len( self.select( array = e.actions, type = expert.SymmetryUpdate ) ),
            0
            )


    def test_call(self):

        board = mr_object.Blackboard()
        exp = expert.LocalSymmetryExpert()
        results = exp(
            board = board,
            case = self.case,
            context = CONTEXT,
            solutions = [],
            postprocessings = [],
            )
        self.assertEqual( results, [] )
        self.assertEqual( board.read( type = object ), [] )

        sol = Fake()
        sol.structure = SLT_STRUCTURE
        results = exp(
            board = board,
            case = self.case,
            context = CONTEXT,
            solutions = [ sol ],
            postprocessings = [],
            )
        self.assertEqual( results, [] )
        self.assertEqual(
            len( self.case.data.read( type = mr_object.SymmetrySetting ) ),
            6,
            )


class TestPartitionStructureAccordingToMembershipIn(unittest.TestCase):

    def setUp(self):

        self.slt_problem = Fake()
        self.slt_problem.data = mr_object.Blackboard()
        self.slt_problem.root = object()
        self.slt_problem.xray_data = Fake()
        self.slt_problem.xray_data.resolution = SLT_DMIN
        self.slt_problem.xray_data.cell = SLT_CELL.parameters()
        self.slt_case = mr_object.Case(
            problem = self.slt_problem,
            composition = None,
            space_group_hall = SLT_SPAC.type().hall_symbol()
            )
        self.slt_assembly = mr_object.Assembly.Simple(
            ensemble = SLT_ENSEMBLE,
            symmetry = mr_object.RotationAxis( axis = SLT_EXPECTEDS[0] ),
            strategy = object(),
            )


    def test_complete_normal(self):

        results = expert.partition_structure_according_to_membership_in(
            structure = SLT_STRUCTURE,
            assembly = self.slt_assembly,
            case = self.slt_case,
            context = CONTEXT,
            multiplier = 3.0,
            )
        member_index_collection = [
            [ 0, 7, 9, 11, 15 ],
            [ 1, 2, 3, 4, 17 ],
            [ 5, 6, 12, 14, 16 ],
            [ 8, 10, 13, 18, 19 ],
            ]
        assemblies = set(
            frozenset( [ SLT_STRUCTURE.peaks[ i ] for i in indices ] )
            for indices in member_index_collection
            )

        self.assertEqual( len( results ), 4 )
        self.assertEqual(
            set( frozenset( a ) for a in results ),
            assemblies
            )


    def test_incomplete(self):

        structure = mr_object.Structure( peaks = SLT_STRUCTURE.peaks[1:17] )
        results = expert.partition_structure_according_to_membership_in(
            structure = structure,
            assembly = self.slt_assembly,
            case = self.slt_case,
            context = CONTEXT,
            multiplier = 3.0,
            )
        member_index_collection = [
            [ 7, 9, 11, 15 ],
            [ 1, 2, 3, 4 ],
            [ 5, 6, 12, 14, 16 ],
            [ 8, 10, 13 ],
            ]
        assemblies = set(
            frozenset( [ SLT_STRUCTURE.peaks[ i ] for i in indices ] )
            for indices in member_index_collection
            )

        self.assertEqual( len( results ), 4 )
        self.assertEqual(
            set( frozenset( a ) for a in results ),
            assemblies
            )


    def test_other_ensemble(self):

        ensemble = mr_object.MultifileEnsemble(
            pdbs = [ os.path.join( ROOT, "monomer.pdb" ) ],
            rmsds = [ 0.80 ],
            composition = mr_object.Composition( components = [ SLT_COMPONENT ] )
            )
        reset = [
            mr_object.Peak(
                ensemble = ensemble,
                rotation = mr_object.Rotation.Euler( angles = p[0] ),
                translation = p[1],
                bfactor = 0
                )
            for p in SLT[1:5]
            ]
        peaks = reset + SLT_STRUCTURE.peaks[5:17]
        structure = mr_object.Structure( peaks = peaks )
        results = expert.partition_structure_according_to_membership_in(
            structure = structure,
            assembly = self.slt_assembly,
            case = self.slt_case,
            context = CONTEXT,
            multiplier = 3.0,
            )
        member_index_collection = [
            [ 7, 9, 11, 15 ],
            [ 1, 2, 3, 4 ],
            [ 5, 6, 12, 14, 16 ],
            [ 8, 10, 13 ],
            ]
        assemblies = set(
            frozenset( [ peaks[ i - 1 ] for i in indices ] )
            for indices in member_index_collection
            )

        self.assertEqual( len( results ), 4 )
        self.assertEqual(
            set( frozenset( a ) for a in results ),
            assemblies
            )


class TestAcceptObservedAssemblyStrategy(unittest.TestCase):

    def setUp(self):

        self.expert_psatm = expert.partition_structure_according_to_membership_in
        self.structures = [ object(), object() ]
        self.assembly = Fake()
        self.assembly.description = [ object(), object() ]

        self.case = object()
        self.strategy = expert.AcceptObservedAssemblyStrategy()


    def tearDown(self):

        expert.partition_structure_according_to_membership_in = self.expert_psatm


    def test_1(self):

        expert.partition_structure_according_to_membership_in = Return(
            values = [
                [ [ object() ], [ object(), object() ] ],
                ]
            )
        result = self.strategy(
            assembly = self.assembly,
            structures = self.structures,
            case = self.case,
            context = CONTEXT,
            )
        self.assertTrue( result )
        self.assertEqual(
            expert.partition_structure_according_to_membership_in._calls,
            [
                (
                    (),
                    {
                        "assembly": self.assembly,
                        "structure": self.structures[0],
                        "case": self.case,
                        "context": CONTEXT,
                        "multiplier": self.strategy.multiplier,
                        }
                    ),
                ]
            )


    def test_2(self):

        expert.partition_structure_according_to_membership_in = Return(
            values = [
                [ [ object() ], [ object() ] ],
                [ [ object(), object() ] ],
                ]
            )
        result = self.strategy(
            assembly = self.assembly,
            structures = self.structures,
            case = self.case,
            context = CONTEXT,
            )
        self.assertTrue( result )
        self.assertEqual(
            expert.partition_structure_according_to_membership_in._calls,
            [
                (
                    (),
                    {
                        "assembly": self.assembly,
                        "structure": self.structures[0],
                        "case": self.case,
                        "context": CONTEXT,
                        "multiplier": self.strategy.multiplier,
                        }
                    ),
                (
                    (),
                    {
                        "assembly": self.assembly,
                        "structure": self.structures[1],
                        "case": self.case,
                        "context": CONTEXT,
                        "multiplier": self.strategy.multiplier,
                        }
                    ),
                ]
            )


    def test_3(self):

        expert.partition_structure_according_to_membership_in = Return(
            values = [
                [ [ object() ], [ object() ] ],
                [ [ object() ], [ object() ] ],
                ]
            )
        result = self.strategy(
            assembly = self.assembly,
            structures = self.structures,
            case = self.case,
            context = CONTEXT,
            )
        self.assertFalse( result )
        self.assertEqual(
            expert.partition_structure_according_to_membership_in._calls,
            [
                (
                    (),
                    {
                        "assembly": self.assembly,
                        "structure": self.structures[0],
                        "case": self.case,
                        "context": CONTEXT,
                        "multiplier": self.strategy.multiplier,
                        }
                    ),
                (
                    (),
                    {
                        "assembly": self.assembly,
                        "structure": self.structures[1],
                        "case": self.case,
                        "context": CONTEXT,
                        "multiplier": self.strategy.multiplier,
                        }
                    ),
                ]
            )


class TestAssemblyCreate(unittest.TestCase):

    def setUp(self):

        self.case = Fake()
        self.case.data = mr_object.Blackboard()


    def test_create_always(self):

        exp = expert.AssemblyCreate.Always()
        self.assertTrue( exp.is_valid is expert.AcceptAssemblyStrategy )


    def test_create_observed(self):

        exp = expert.AssemblyCreate.Observed()
        self.assertTrue(
            isinstance( exp.is_valid, expert.AcceptObservedAssemblyStrategy )
            )


    def test_create_never(self):

        exp = expert.AssemblyCreate.Never()
        self.assertTrue( exp.is_valid is expert.RejectAssemblyStrategy )


    def create_fake_structures(self, significants):

        structures = []

        for label in significants:
            s = Fake()
            s.structure = Fake()
            s.structure.significant = label
            structures.append( s )

        return structures


    def test_call(self):

        board = mr_object.Blackboard()
        pgs = [ object(), object(), object() ]
        settings = []

        for pg in pgs:
            setting = mr_object.SymmetrySetting(
                ensemble = object(),
                point_group = pg,
                )
            settings.append( setting )

        for s in settings:
            self.case.data.write( obj = s )

        known = mr_object.Assembly.Simple(
                ensemble = object(),
                symmetry = pgs[2],
                strategy = object(),
                )
        self.case.data.write( obj = known )

        exp = expert.AssemblyCreate( is_valid = Return( values = [ True, False ] ) )
        solutions = self.create_fake_structures( significants = [ True, False ] )
        postprocessings = self.create_fake_structures( significants = [ False, True ] )
        results = exp(
            board = board,
            case = self.case,
            context = CONTEXT,
            solutions = solutions,
            postprocessings = postprocessings,
            )
        self.assertEqual( results, [] )
        used_structures = [ solutions[0].structure, postprocessings[1].structure ]
        used_pgs = set( pgs[:2] )
        ensemble_for = dict( [ ( s.point_group, s.ensemble ) for s in settings ] )

        self.assertEqual( len( exp.is_valid._calls ), 2 )

        for ( posi, keyw ) in exp.is_valid._calls:
            self.assertEqual( posi, () )
            self.assertEqual( keyw[ "case" ], self.case )
            self.assertEqual( keyw[ "context" ], CONTEXT )
            self.assertEqual( keyw[ "structures" ], used_structures )
            self.assertTrue( keyw[ "assembly" ].symmetry in used_pgs )
            self.assertEqual(
                keyw[ "assembly" ].definitions,
                [ ( ensemble_for[ keyw[ "assembly" ].symmetry ], mr_object.PointGroup.IDENTITY ) ]
                )
            self.assertTrue( isinstance(
                keyw[ "assembly" ].strategy, mr_object.RefineStrategy )
            )

        novel = exp.is_valid._calls[0][1][ "assembly" ]
        self.assertEqual(
            set( self.case.data.read( type = mr_object.Assembly ) ),
            set( [ known, novel ] ),
            )

        recs = board.read( type = mr_object.NewAssemblyRecord )
        self.assertEqual( len( recs ), 1 )
        self.assertEqual( recs[0].assembly, novel )


class TestAssemblyRemove(unittest.TestCase):

    def setUp(self):

        self.expert = expert.AssemblyRemove()
        self.case = Fake()
        self.case.data = mr_object.Blackboard()


    def test_remove_subgroup_symmetries(self):

        pgs = [ object(), object() ]
        assemblies = [
            mr_object.Assembly.Simple( ensemble = object(), symmetry = p, strategy = object() )
            for p in pgs
            ]

        for a in assemblies:
            self.case.data.write( obj = a )

        board = mr_object.Blackboard()

        for pg in [ pgs[1], object() ]:
            setting = Fake()
            setting.point_group = pg
            board.write(
                obj = mr_object.ObsoletedBySuperSymmetry(
                    setting = setting,
                    supersymmetry = object(),
                    )
                )

        self.expert.remove_subgroup_symmetries(
            board = board,
            case = self.case,
            context = CONTEXT,
            )
        surviving = self.case.data.read( type = mr_object.Assembly )
        self.assertEqual( surviving, [ assemblies[0] ] )
        recs = board.read( type = mr_object.AssemblyRemovalRecord )
        self.assertEqual( len( recs ), 1 )
        self.assertEqual( recs[0].assembly, assemblies[1] )


    def test_remove_supergroup_symmetries(self):

        pgs = [ object(), object(), object() ]
        assemblies = [
            mr_object.Assembly.Simple( ensemble = object(), symmetry = p, strategy = object() )
            for p in pgs
            ]

        for a in assemblies:
            self.case.data.write( obj = a )

        board = mr_object.Blackboard()
        ssym = Fake()
        ssym.point_group = object()
        highests = [ ssym, None, object() ]

        for ( pg, repl ) in zip( [ pgs[1], pgs[2], object() ], highests ):
            setting = Fake()
            setting.point_group = pg
            board.write(
                obj = mr_object.SymmetryDowngrade(
                    setting = setting,
                    highest = repl,
                    )
                )

        self.expert.remove_supergroup_symmetries(
            board = board,
            case = self.case,
            context = CONTEXT,
            )
        reviseds = self.case.data.read( type = mr_object.Assembly )
        self.assertEqual( len( reviseds ), 2 )
        assembly_with = dict( [ ( a.symmetry, a ) for a in reviseds ] )
        self.assertTrue( pgs[0] in assembly_with )
        self.assertEqual( assembly_with[ pgs[0] ], assemblies[0] )
        self.assertFalse( pgs[1] in assembly_with )
        pg = ssym.point_group
        self.assertTrue( ssym.point_group in assembly_with )
        self.assertTrue( assembly_with[ pg ] is not assemblies[1] )
        self.assertEqual( assembly_with[ pg ].symmetry, ssym.point_group )
        self.assertEqual( assembly_with[ pg ].definitions, assemblies[1].definitions )
        self.assertEqual( assembly_with[ pg ].strategy, assemblies[1].strategy )
        self.assertTrue( pgs[2] not in assembly_with )
        recs = board.read( type = mr_object.AssemblyRemovalRecord )
        self.assertEqual( len( recs ), 2 )
        self.assertEqual(
            set( r.assembly for r in recs ),
            set( [ assemblies[1], assemblies[2] ] )
            )
        recs = board.read( type = mr_object.NewAssemblyRecord )
        self.assertEqual( len( recs ), 1 )
        self.assertEqual( recs[0].assembly, assembly_with[ pg ] )


class TestAssemblyUpdate(unittest.TestCase):

    def setUp(self):

        self.expert = expert.AssemblyUpdate()
        self.case = Fake()
        self.case.data = mr_object.Blackboard()


    def test_call(self):

        pgs = [ object(), object() ]
        assemblies = [
            mr_object.Assembly.Simple( ensemble = object(), symmetry = p, strategy = object() )
            for p in pgs
            ]

        for assem in assemblies:
            self.case.data.write( obj = assem )

        setting = Fake()
        setting.point_group = object()
        rec = mr_object.SymmetryUpdateRecord(
            setting = setting,
            previous = pgs[0],
            assemblies_in = {},
            )
        board = mr_object.Blackboard()
        board.write( obj = rec )
        results = self.expert(
            board = board,
            case = self.case,
            context = CONTEXT,
            solutions = [],
            postprocessings = [],
            )
        self.assertEqual( results, [] )
        self.assertEqual(
            set( assemblies ),
            set( self.case.data.read( type = mr_object.Assembly ) )
            )
        self.assertEqual( assemblies[0].symmetry, setting.point_group )
        self.assertEqual( assemblies[1].symmetry, pgs[1] )


class TestSearchModelMaintenance(unittest.TestCase):

    def setUp(self):

        self.expert = expert.SearchModelMaintenance( is_valid = object() )
        self.case = Fake()
        self.case.data = mr_object.Blackboard()


    def test_create_always(self):

        exp = expert.SearchModelMaintenance.Always()
        self.assertTrue( exp.is_valid is expert.AcceptAssemblyStrategy )


    def test_create_if_observed(self):

        exp = expert.SearchModelMaintenance.Observed()
        self.assertTrue(
            isinstance( exp.is_valid, expert.AcceptObservedAssemblyStrategy )
            )


    def test_create_never(self):

        exp = expert.SearchModelMaintenance.Never()
        self.assertTrue( exp.is_valid is expert.RejectAssemblyStrategy )


    def test_add_new_assemblies(self):

        assemblies = []

        for s in [ 5, 6, 7 ]:
            ensemble = Fake()
            ensemble.composition = [ object() ]
            symmetry = Fake()
            symmetry.series = lambda: range( 5 )
            assembly = mr_object.Assembly.Simple(
                ensemble = ensemble,
                symmetry = symmetry,
                strategy = object()
                )
            assemblies.append( assembly )
            self.case.data.write( obj = assembly )

        aensemble = mr_object.AssemblyEnsemble( assembly = assemblies[0] )
        self.case.data.write( obj = aensemble )
        self.expert.is_valid = Return( values = [ True, False ] )
        structures = object()

        self.expert.add_new_assemblies(
            structures = structures,
            board = object(),
            case = self.case,
            context = CONTEXT,
            )

        self.assertEqual( len( self.expert.is_valid._calls ), 2 )
        useds = set()

        for ( posi, keyw ) in self.expert.is_valid._calls:
            self.assertEqual( posi, () )
            self.assertEqual( keyw[ "case" ], self.case )
            self.assertEqual( keyw[ "context" ], CONTEXT )
            self.assertEqual( keyw[ "structures" ], structures )
            self.assertTrue( keyw[ "assembly" ] in assemblies )
            useds.add( keyw[ "assembly" ] )

        self.assertEqual( set( assemblies[1:] ), useds )

        novel = self.expert.is_valid._calls[0][1][ "assembly" ]

        ensembles = self.case.data.read( type = mr_object.AssemblyEnsemble )
        self.assertEqual( len( ensembles ), 2 )
        self.assertEqual(
            set( [ e.assembly for e in ensembles ] ),
            set( [ aensemble.assembly, novel ] ),
            )


    def test_remove_deleted_assemblies(self):

        assemblies = [ Fake(), Fake() ]
        models = []

        for a in assemblies:
            a.composition = object()
            a.info = lambda: object()
            models.append( mr_object.AssemblyEnsemble( assembly = a ) )

        for m in models:
            self.case.data.write( obj = m )

        board = mr_object.Blackboard()
        board.write(
            obj = mr_object.AssemblyRemovalRecord( assembly = assemblies[1] )
            )
        self.expert.remove_deleted_assemblies(
            board = board,
            case = self.case,
            context = CONTEXT,
            )
        self.assertEqual(
            self.case.data.read( type = mr_object.AssemblyEnsemble ),
            [ models[0] ]
            )


"""
class TestAssemblyModelCreate(unittest.TestCase):

    def setUp(self):

        self.expert = expert.AssemblyModelCreate( observe_first = True )
        self.context = Fake()
        from phaser import output
        self.context.output = output.SingleStream(
            stream = None,
            level = output.SingleStream.VERBOSE
            )

        from phaser.pipeline import superposition
        self.context.superposition = superposition.Proxy(
            method = superposition.simple_ssm_superposition
            )

        self.slt_case = Fake()
        self.slt_case.data = mr_object.Blackboard()


    def test_approx_contains(self):

        import iotbx.pdb
        slt_root = iotbx.pdb.input( os.path.join( ROOT, "monomer.pdb" ) ).construct_hierarchy()

        slt_ense1 = Fake()
        slt_ense1.substituteable_with = lambda other: other == slt_ense1
        slt_ense1.superpose_template = lambda: slt_ense1
        slt_ense1.root = lambda: slt_root

        slt_generator = mr_object.Generator.from_rotation_translation(
            power = 1,
            fold = 5,
            axis = scitbx.matrix.col(
                ( 0.034837143908216656, -0.0012095271938242775, 0.9993922705566046 )
                ),
            translation = scitbx.matrix.col(
                (-0.18640396875067672, 0.3921807979045849, 0.006972372540474437)
                )
            )
        reference = mr_object.Assembly.Simple(
            ensemble = slt_ense1,
            symmetry = mr_object.RotationAxis( axis = slt_generator ),
            )

        generator = mr_object.Generator.from_rotation_translation(
            power = 1,
            fold = 10,
            axis = scitbx.matrix.col(
                ( 0.034837143908216656, -0.0012095271938242775, 0.9993922705566046 )
                ),
            translation = scitbx.matrix.col(
                (-0.18640396875067672, 0.3921807979045849, 0.006972372540474437)
                )
            )
        other = mr_object.Assembly.Simple(
            ensemble = slt_ense1,
            symmetry = mr_object.RotationAxis( axis = generator ),
            )
        self.assertTrue(
            self.expert.approx_contains(
                left = other,
                right = reference,
                context = self.context,
                )
            )
        self.assertFalse(
            self.expert.approx_contains(
                left = reference,
                right = other,
                context = self.context,
                )
            )

        other = mr_object.Assembly(
            definitions = [
                ( slt_ense1, mr_object.PointGroup.IDENTITY ), # 1
                ( slt_ense1, scitbx.matrix.rt( ( (  0.310, -0.951,  0.023,  0.950,  0.309, -0.034,  0.025,  0.032,  0.999 ), ( -0.19,    0.39,    0.01 ) ) ) ), # 2
                ( slt_ense1, scitbx.matrix.rt( ( ( -0.807, -0.588,  0.062,  0.587, -0.809, -0.023,  0.064,  0.018,  0.998 ), ( -0.62,    0.34,    0.02 ) ) ) ), # 3
                ( slt_ense1, scitbx.matrix.rt( ( ( -0.807,  0.587,  0.064, -0.588, -0.809,  0.018,  0.062, -0.023,  0.998 ), ( -0.70,   -0.09,    0.02 ) ) ) ), # 4
                ( slt_ense1, scitbx.matrix.rt( ( (  0.310,  0.950,  0.025, -0.951,  0.309,  0.032,  0.023, -0.034,  0.999 ), ( -0.32,   -0.30,    0.01 ) ) ) ), # 5
                ],
            symmetry = mr_object.PointGroup(),
            strategy = mr_object.RefineStrategy(),
            )
        self.assertTrue(
            self.expert.approx_contains(
                left = reference,
                right = other,
                context = self.context,
                )
            )

        other = mr_object.Assembly(
            definitions = [
                ( slt_ense1, scitbx.matrix.rt( ( ( -0.807, -0.588,  0.062,  0.587, -0.809, -0.023,  0.064,  0.018,  0.998 ), ( -0.62,    0.34,    0.02 ) ) ) ), # 3
                ( slt_ense1, mr_object.PointGroup.IDENTITY ), # 1
                ( slt_ense1, scitbx.matrix.rt( ( ( -0.807,  0.587,  0.064, -0.588, -0.809,  0.018,  0.062, -0.023,  0.998 ), ( -0.70,   -0.09,    0.02 ) ) ) ), # 4
                ( slt_ense1, scitbx.matrix.rt( ( (  0.310,  0.950,  0.025, -0.951,  0.309,  0.032,  0.023, -0.034,  0.999 ), ( -0.32,   -0.30,    0.01 ) ) ) ), # 5
                ( slt_ense1, scitbx.matrix.rt( ( (  0.310, -0.951,  0.023,  0.950,  0.309, -0.034,  0.025,  0.032,  0.999 ), ( -0.19,    0.39,    0.01 ) ) ) ), # 2
                ],
            symmetry = mr_object.PointGroup(),
            strategy = mr_object.RefineStrategy(),
            )
        self.assertTrue(
            self.expert.approx_contains(
                left = reference,
                right = other,
                context = self.context,
                )
            )

        import random
        from scitbx.array_family import flex
        rotation = scitbx.math.r3_rotation_axis_and_angle_as_matrix(
            flex.random_double_point_on_sphere(),
            random.uniform( 1, 2 )
            )
        translation = random.uniform( 10, 20 ) * scitbx.matrix.col(
            flex.random_double_point_on_sphere()
            )
        rt = scitbx.matrix.rt( ( rotation, translation ) )
        slt_trans_root = slt_root.deep_copy()
        atoms = slt_trans_root.atoms()
        atoms.set_xyz( rt * atoms.extract_xyz() )
        slt_trans_ense = Fake()
        slt_trans_ense.substituteable_with = lambda other: other == slt_ense1 or other == slt_trans_ense
        slt_ense1.substituteable_with = lambda other: other == slt_ense1 or other == slt_trans_ense
        slt_trans_ense.superpose_template = lambda: slt_trans_ense
        slt_trans_ense.root = lambda: slt_trans_root

        mixed_other = mr_object.Assembly(
            definitions = [
                ( other.ensembles[0], other.operations[0] ),
                ( slt_trans_ense, other.operations[1] * rt ),
                ( slt_trans_ense, other.operations[2] * rt ),
                ( other.ensembles[3], other.operations[3] ),
                ( other.ensembles[4], other.operations[4] ),
                ],
            symmetry = mr_object.PointGroup(),
            strategy = mr_object.RefineStrategy(),
            )
        self.assertTrue(
            self.expert.approx_contains(
                left = reference,
                right = mixed_other,
                context = self.context,
                )
            )

        nslt_ense = Fake()
        nslt_ense.substituteable_with = lambda other: other == nslt_ense
        nslt_ense.superpose_template = lambda: nslt_ense
        nslt_ense.root = lambda: slt_root

        other = mr_object.Assembly(
            definitions = [
                ( slt_ense1, scitbx.matrix.rt( ( ( -0.807, -0.588,  0.062,  0.587, -0.809, -0.023,  0.064,  0.018,  0.998 ), ( -0.62,    0.34,    0.02 ) ) ) ), # 3
                ( slt_ense1, mr_object.PointGroup.IDENTITY ), # 1
                ( slt_ense1, scitbx.matrix.rt( ( ( -0.807,  0.587,  0.064, -0.588, -0.809,  0.018,  0.062, -0.023,  0.998 ), ( -0.70,   -0.09,    0.02 ) ) ) ), # 4
                ( nslt_ense, scitbx.matrix.rt( ( (  0.310,  0.950,  0.025, -0.951,  0.309,  0.032,  0.023, -0.034,  0.999 ), ( -0.32,   -0.30,    0.01 ) ) ) ), # 5
                ( slt_ense1, scitbx.matrix.rt( ( (  0.310, -0.951,  0.023,  0.950,  0.309, -0.034,  0.025,  0.032,  0.999 ), ( -0.19,    0.39,    0.01 ) ) ) ), # 2
                ],
            symmetry = mr_object.PointGroup(),
            strategy = mr_object.RefineStrategy(),
            )
        self.assertFalse(
            self.expert.approx_contains(
                left = reference,
                right = other,
                context = self.context,
                )
            )

        other = mr_object.Assembly(
            definitions = [
                ( slt_ense1, mr_object.PointGroup.IDENTITY ), # 1
                ( slt_ense1, scitbx.matrix.rt( ( (  0.310, -0.951,  0.023,  0.950,  0.309, -0.034,  0.025,  0.032,  0.999 ), ( -0.19,    0.39,    0.01 ) ) ) ), # 2
                ( slt_ense1, scitbx.matrix.rt( ( ( -0.807, -0.588,  0.062,  0.587, -0.809, -0.023,  0.064,  0.018,  0.998 ), ( -0.62,    0.34,    0.02 ) ) ) ), # 3
                ( slt_ense1, scitbx.matrix.rt( ( ( -0.807,  0.587,  0.064, -0.588, -0.809,  0.018,  0.062, -0.023,  0.998 ), ( -0.70,   -0.09,    0.02 ) ) ) ), # 4
                ( slt_ense1, scitbx.matrix.rt( ( (  0.187,  0.629, -0.755, -0.276, -0.704, -0.655, -0.943,  0.331,  0.042 ), ( -0.32,   -0.30,    0.01 ) ) ) ), # 5
                ],
            symmetry = mr_object.PointGroup(),
            strategy = mr_object.RefineStrategy(),
            )
        self.assertFalse(
            self.expert.approx_contains(
                left = reference,
                right = other,
                context = self.context,
                )
            )

        other = mr_object.Assembly(
            definitions = [
                ( slt_ense1, mr_object.PointGroup.IDENTITY ), # 1
                ( slt_ense1, scitbx.matrix.rt( ( (  0.310, -0.951,  0.023,  0.950,  0.309, -0.034,  0.025,  0.032,  0.999 ), ( -0.19,    0.39,    0.01 ) ) ) ), # 2
                ( slt_ense1, scitbx.matrix.rt( ( ( -0.807, -0.588,  0.062,  0.587, -0.809, -0.023,  0.064,  0.018,  0.998 ), ( -0.62,    0.34,    0.02 ) ) ) ), # 3
                ( slt_ense1, scitbx.matrix.rt( ( ( -0.807,  0.587,  0.064, -0.588, -0.809,  0.018,  0.062, -0.023,  0.998 ), ( -0.70,   -0.09,    0.02 ) ) ) ), # 4
                ( slt_ense1, scitbx.matrix.rt( ( (  0.310,  0.950,  0.025, -0.951,  0.309,  0.032,  0.023, -0.034,  0.999 ), ( -3.32,    2.30,    5.01 ) ) ) ), # 5
                ],
            symmetry = mr_object.PointGroup(),
            strategy = mr_object.RefineStrategy(),
            )
        self.assertFalse(
            self.expert.approx_contains(
                left = reference,
                right = other,
                context = self.context,
                )
            )

        reference2 = mr_object.Assembly(
            definitions = [
                ( slt_ense1, mr_object.PointGroup.IDENTITY ), # 1
                ( slt_ense1, scitbx.matrix.rt( ( (  0.310, -0.951,  0.023,  0.950,  0.309, -0.034,  0.025,  0.032,  0.999 ), ( -0.19,    0.39,    0.01 ) ) ) ), # 2
                ( slt_ense1, scitbx.matrix.rt( ( ( -0.807, -0.588,  0.062,  0.587, -0.809, -0.023,  0.064,  0.018,  0.998 ), ( -0.62,    0.34,    0.02 ) ) ) ), # 3
                ( nslt_ense, scitbx.matrix.rt( ( ( -0.807,  0.587,  0.064, -0.588, -0.809,  0.018,  0.062, -0.023,  0.998 ), ( -0.70,   -0.09,    0.02 ) ) ) ), # 4
                ( nslt_ense, scitbx.matrix.rt( ( (  0.310,  0.950,  0.025, -0.951,  0.309,  0.032,  0.023, -0.034,  0.999 ), ( -0.32,   -0.30,    0.01 ) ) ) ), # 5
                ],
            symmetry = mr_object.PointGroup(),
            strategy = mr_object.RefineStrategy(),
            )
        other = mr_object.Assembly(
            definitions = [
                ( slt_ense1, scitbx.matrix.rt( ( ( -0.807, -0.588,  0.062,  0.587, -0.809, -0.023,  0.064,  0.018,  0.998 ), ( -0.62,    0.34,    0.02 ) ) ) ), # 3
                ( nslt_ense, scitbx.matrix.rt( ( (  0.310, -0.951,  0.023,  0.950,  0.309, -0.034,  0.025,  0.032,  0.999 ), ( -0.19,    0.39,    0.01 ) ) ) ), # 2
                ( slt_ense1, scitbx.matrix.rt( ( ( -0.807,  0.587,  0.064, -0.588, -0.809,  0.018,  0.062, -0.023,  0.998 ), ( -0.70,   -0.09,    0.02 ) ) ) ), # 4
                ( slt_ense1, scitbx.matrix.rt( ( (  0.310,  0.950,  0.025, -0.951,  0.309,  0.032,  0.023, -0.034,  0.999 ), ( -0.32,   -0.30,    0.01 ) ) ) ), # 5
                ( nslt_ense, mr_object.PointGroup.IDENTITY ), # 1
                ],
            symmetry = mr_object.PointGroup(),
            strategy = mr_object.RefineStrategy(),
            )
        self.assertTrue(
            self.expert.approx_contains(
                left = reference2,
                right = other,
                context = self.context,
                )
            )
        other = mr_object.Assembly(
            definitions = [
                ( slt_ense1, scitbx.matrix.rt( ( ( -0.807, -0.588,  0.062,  0.587, -0.809, -0.023,  0.064,  0.018,  0.998 ), ( -0.62,    0.34,    0.02 ) ) ) ), # 3
                ( slt_ense1, mr_object.PointGroup.IDENTITY ), # 1
                ( nslt_ense, scitbx.matrix.rt( ( ( -0.807,  0.587,  0.064, -0.588, -0.809,  0.018,  0.062, -0.023,  0.998 ), ( -0.70,   -0.09,    0.02 ) ) ) ), # 4
                ( slt_ense1, scitbx.matrix.rt( ( (  0.310,  0.950,  0.025, -0.951,  0.309,  0.032,  0.023, -0.034,  0.999 ), ( -0.32,   -0.30,    0.01 ) ) ) ), # 5
                ( nslt_ense, scitbx.matrix.rt( ( (  0.310, -0.951,  0.023,  0.950,  0.309, -0.034,  0.025,  0.032,  0.999 ), ( -0.19,    0.39,    0.01 ) ) ) ), # 2
                ],
            symmetry = mr_object.PointGroup(),
            strategy = mr_object.RefineStrategy(),
            )
        self.assertFalse(
            self.expert.approx_contains(
                left = reference2,
                right = other,
                context = self.context,
                )
            )


    def run_tests_with_call(self, amc_expert):

        amc_expert(
            board = None,
            case = self.slt_case,
            context = self.context,
            solutions = [],
            postprocessings = []
            )
        self.assertEqual(
            self.slt_case.data.read( type = mr_object.RegularEnsemble ),
            []
            )
        ensemble = Fake()
        ensemble.composition = 1
        reg_assembly1 = mr_object.Assembly(
            definitions = [ ( ensemble, None ) ],
            symmetry = mr_object.PointGroup(),
            strategy = mr_object.RefineStrategy(),
            )
        regense1 = mr_object.RegularEnsemble( assembly = reg_assembly1 )
        self.slt_case.data.write( obj = regense1 )
        trial_assembly1 = mr_object.Assembly(
            definitions = [ ( ensemble, None ) ],
            symmetry = mr_object.PointGroup(),
            strategy = mr_object.RefineStrategy(),
            )
        self.slt_case.data.write( obj = trial_assembly1 )
        amc_expert.approx_contains = Return( values = [ True ] )
        amc_expert(
            board = None,
            case = self.slt_case,
            context = self.context,
            solutions = [],
            postprocessings = []
            )
        self.assertEqual(
            amc_expert.approx_contains._calls,
            [ ( (), { "left": trial_assembly1, "right": reg_assembly1, "context": self.context } ) ]
            )
        self.assertEqual(
            self.slt_case.data.read( type = mr_object.RegularEnsemble ),
            [ regense1 ]
            )
        self.assertEqual( regense1.assembly, trial_assembly1 )

        trial_assembly2 = mr_object.Assembly(
            definitions = [ ( ensemble, None ) ],
            symmetry = mr_object.PointGroup(),
            strategy = mr_object.RefineStrategy(),
            present = True
            )
        self.slt_case.data.write( obj = trial_assembly2 )
        self.slt_case.data.delete( obj = trial_assembly1 )
        amc_expert.approx_contains = Return( values = [ False ] )
        amc_expert(
            board = None,
            case = self.slt_case,
            context = self.context,
            solutions = [],
            postprocessings = []
            )
        self.assertEqual(
            amc_expert.approx_contains._calls,
            [ ( (), { "left": trial_assembly2, "right": trial_assembly1, "context": self.context } ) ]
            )
        regenses = self.slt_case.data.read( type = mr_object.RegularEnsemble )
        self.assertTrue( regense1 in regenses )
        self.assertEqual( regense1.assembly, trial_assembly1 )
        others = [ o for o in regenses if o != regense1 ]
        self.assertEqual( len( others ), 1 )
        self.assertEqual( others[0].assembly, trial_assembly2 )

        self.slt_case.data.delete( obj = trial_assembly2 )
        self.slt_case.data.delete( obj = others[0] )

        trial_assembly3 = mr_object.Assembly(
            definitions = [ ( ensemble, None ) ],
            symmetry = mr_object.PointGroup(),
            strategy = mr_object.RefineStrategy(),
            present = False
            )
        self.slt_case.data.write( obj = trial_assembly3 )
        amc_expert.approx_contains = Return( values = [ False ] )
        amc_expert(
            board = None,
            case = self.slt_case,
            context = self.context,
            solutions = [],
            postprocessings = []
            )
        self.assertEqual(
            amc_expert.approx_contains._calls,
            [ ( (), { "left": trial_assembly3, "right": trial_assembly1, "context": self.context } ) ]
            )
        regenses = self.slt_case.data.read( type = mr_object.RegularEnsemble )
        self.assertTrue( regense1 in regenses )
        self.assertEqual( regense1.assembly, trial_assembly1 )
        others = [ o for o in regenses if o != regense1 ]
        return ( others, trial_assembly3 )


    def test_call_observe(self):

        ( others, assembly ) = self.run_tests_with_call(
            amc_expert = expert.AssemblyModelCreate( observe_first = True )
            )
        self.assertEqual( len( others ), 0 )


    def test_call_anyway(self):

        ( others, assembly ) = self.run_tests_with_call(
            amc_expert = expert.AssemblyModelCreate( observe_first = False )
            )
        self.assertEqual( len( others ), 1 )
        self.assertEqual( others[0].assembly, assembly )
 """

suite_object_completion = unittest.TestLoader().loadTestsFromTestCase(
    TestObjectCompletion
    )
suite_generator_search = unittest.TestLoader().loadTestsFromTestCase(
    TestGeneratorSearch
    )
suite_generator_classify_average = unittest.TestLoader().loadTestsFromTestCase(
    TestGeneratorClassifyAverage
    )
suite_point_group_determine = unittest.TestLoader().loadTestsFromTestCase(
    TestPointGroupDetermine
    )
suite_find_symmetry_related_assemblies = unittest.TestLoader().loadTestsFromTestCase(
    TestFindSymmetryRelatedAssemblies
    )
suite_symmetry_compare = unittest.TestLoader().loadTestsFromTestCase(
    TestSymmetryCompare
    )
suite_symmetry_creation_policy = unittest.TestLoader().loadTestsFromTestCase(
    TestSymmetryCreationPolicy
    )
suite_symmetry_subgroup_policy = unittest.TestLoader().loadTestsFromTestCase(
    TestSymmetrySubgroupPolicy
    )
suite_symmetry_unsupported_policy = unittest.TestLoader().loadTestsFromTestCase(
    TestSymmetryUnsupportedPolicy
    )
suite_symmetry_update = unittest.TestLoader().loadTestsFromTestCase(
    TestSymmetryUpdate
    )
suite_local_symmetry_expert = unittest.TestLoader().loadTestsFromTestCase(
    TestLocalSymmetryExpert
    )
suite_partition_structure_according_to_membership_in = unittest.TestLoader().loadTestsFromTestCase(
    TestPartitionStructureAccordingToMembershipIn
    )
suite_accept_observed_assembly_strategy = unittest.TestLoader().loadTestsFromTestCase(
    TestAcceptObservedAssemblyStrategy
    )
suite_assembly_create = unittest.TestLoader().loadTestsFromTestCase(
    TestAssemblyCreate
    )
suite_assembly_remove = unittest.TestLoader().loadTestsFromTestCase(
    TestAssemblyRemove
    )
suite_assembly_update = unittest.TestLoader().loadTestsFromTestCase(
    TestAssemblyUpdate
    )
suite_search_model_maintenance = unittest.TestLoader().loadTestsFromTestCase(
    TestSearchModelMaintenance
    )
#suite_model_assembly = unittest.TestLoader().loadTestsFromTestCase(
#    TestAssemblyModelCreate
#    )

alltests = unittest.TestSuite(
    [
        suite_object_completion,
        suite_generator_search,
        suite_generator_classify_average,
        suite_point_group_determine,
        suite_find_symmetry_related_assemblies,
        suite_symmetry_compare,
        suite_symmetry_creation_policy,
        suite_symmetry_subgroup_policy,
        suite_symmetry_unsupported_policy,
        suite_symmetry_update,
        suite_local_symmetry_expert,
        suite_partition_structure_according_to_membership_in,
        suite_accept_observed_assembly_strategy,
        suite_assembly_create,
        suite_assembly_remove,
        suite_assembly_update,
        suite_search_model_maintenance,
        #suite_model_assembly,
        ]
    )

if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
