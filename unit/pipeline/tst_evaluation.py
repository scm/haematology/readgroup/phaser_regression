import unittest

from phaser.pipeline import evaluation
from phaser.pipeline.scaffolding import Fake, Return, IterableCompareTestCase

class TestTFZvsProbabilityCurve(unittest.TestCase):

    def check_probability_for(self, curve, tfzs, probabilities, tolerance):

        for ( tfz, prob ) in zip( tfzs, probabilities ):
            self.assertTrue(
                abs( curve.probability_for( tfz = tfz ) - prob ) <= tolerance
                )


    def test_polar(self):

        self.check_probability_for(
            curve = evaluation.tfz_polar_single,
            tfzs = evaluation.TFZ_BINS,
            probabilities = evaluation.TFZ_POLAR_SOLVED_FRACS,
            tolerance = 0.05,
            )
        self.check_probability_for(
            curve = evaluation.tfz_polar_single,
            tfzs = [ 4.58, 5.5, 6.32 ],
            probabilities = [ 0.14, 0.47, 0.81 ],
            tolerance = 0.01,
            )


    def test_nonpolar(self):

        self.check_probability_for(
            curve = evaluation.tfz_nonpolar,
            tfzs = evaluation.TFZ_BINS,
            probabilities = evaluation.TFZ_NONPOLAR_SOLVED_FRACS,
            tolerance = 0.05,
            )
        self.check_probability_for(
            curve = evaluation.tfz_nonpolar,
            tfzs = [ 4.6, 6.6, 7.1 ],
            probabilities = [ 0.03, 0.36, 0.65 ],
            tolerance = 0.01,
            )


class TestTFZvsProbabilityCurveCollection(unittest.TestCase):

    def test_polar_nonpolar(self):

        from cctbx import sgtbx

        for symbol in [ "P 1", "C 2", "P 32", "R3:R", "R3:H", "I 41" ]:
            description = evaluation.TFZBasedProbabilityStructureDescription(
                peaks = [ object() ],
                space_group_info = sgtbx.space_group_info( symbol )
                )
            curve = evaluation.TFZ_POLAR_NONPOLAR.probability_curve_for(
                description = description
                )
            self.assertEqual( curve, evaluation.tfz_polar_single )

        for symbol in [ "P 1", "C 2", "P 32", "R3:R", "R3:H", "I 41" ]:
            description = evaluation.TFZBasedProbabilityStructureDescription(
                peaks = [ object(), object() ],
                space_group_info = sgtbx.space_group_info( symbol )
                )
            curve = evaluation.TFZ_POLAR_NONPOLAR.probability_curve_for(
                description = description
                )
            self.assertEqual( curve, evaluation.tfz_nonpolar )

        for symbol in [ "P 1", "C 2", "P 32", "R3:R", "R3:H", "I 41" ]:
            description = evaluation.TFZBasedProbabilityStructureDescription(
                peaks = [],
                space_group_info = sgtbx.space_group_info( symbol )
                )
            curve = evaluation.TFZ_POLAR_NONPOLAR.probability_curve_for(
                description = description
                )
            self.assertEqual( curve, evaluation.tfz_polar_single )

        for symbol in [ "P 21 2 21", "R32:R", "R32:H", "I 41 2 2", "I 21 3" ]:
            description = evaluation.TFZBasedProbabilityStructureDescription(
                peaks = [ object() ],
                space_group_info = sgtbx.space_group_info( symbol )
                )
            curve = evaluation.TFZ_POLAR_NONPOLAR.probability_curve_for(
                description = description
                )
            self.assertEqual( curve, evaluation.tfz_nonpolar )

        for symbol in [ "P 21 2 21", "R32:R", "R32:H", "I 41 2 2", "I 21 3" ]:
            description = evaluation.TFZBasedProbabilityStructureDescription(
                peaks = [],
                space_group_info = sgtbx.space_group_info( symbol )
                )
            curve = evaluation.TFZ_POLAR_NONPOLAR.probability_curve_for(
                description = description
                )
            self.assertEqual( curve, evaluation.tfz_nonpolar )


class TestTFZBasedProbability(IterableCompareTestCase):

    def check(self, symbol, tfzs, probabilities):

        from cctbx import sgtbx
        import operator
        desc = evaluation.TFZBasedProbabilityStructureDescription(
            space_group_info = sgtbx.space_group_info( symbol ),
            peaks = [
                evaluation.TFZBasedProbabilityPeakDescription( tfz = t ) for t in tfzs
                ],
            )
        prob = evaluation.TFZBasedProbability(
            collection = evaluation.TFZ_POLAR_NONPOLAR,
            description = desc,
            )
        self.assertEqual(
            prob.curve,
            evaluation.TFZ_POLAR_NONPOLAR.probability_curve_for( description = desc ),
            )
        self.assertIterablesAlmostEqual( prob.individuals, probabilities, 1 )
        self.assertAlmostEqual( prob.total, reduce( operator.mul, probabilities, 1 ), 1 )


    def test_polar(self):

        tfz_scores = [ [ 4.58 ], [ 5.5 ], [ 6.32 ] ]
        probabilities = [ [ 0.14 ], [ 0.47 ], [ 0.81 ] ]

        for symbol in [ "P 1", "C 2", "P 32", "R3:R", "R3:H", "I 41" ]:
            for ( tfzs, probs ) in zip( tfz_scores, probabilities ):
                self.check( symbol = symbol, tfzs = tfzs, probabilities = probs )

        tfz_scores = [ [ 8.1, 9.1 ] ]
        probabilities = [ [ 0.97, 0.98 ] ]

        for symbol in [ "P 1", "C 2", "P 32", "R3:R", "R3:H", "I 41" ]:
            for ( tfzs, probs ) in zip( tfz_scores, probabilities ):
                self.check( symbol = symbol, tfzs = tfzs, probabilities = probs )


    def test_nonpolar(self):

        tfz_scores = [ [ 4.6 ], [ 6.6 ], [ 7.1 ] ]
        probabilities = [ [ 0.03 ], [ 0.36 ], [ 0.65 ] ]

        for symbol in [ "P 21 2 21", "R32:R", "R32:H", "I 41 2 2", "I 21 3" ]:
            for ( tfzs, probs ) in zip( tfz_scores, probabilities ):
                self.check( symbol = symbol, tfzs = tfzs, probabilities = probs )

        tfz_scores = [ [ 8.1, 9.1 ] ]
        probabilities = [ [ 0.97, 0.98 ] ]

        for symbol in [ "P 21 2 21", "R32:R", "R32:H", "I 41 2 2", "I 21 3" ]:
            for ( tfzs, probs ) in zip( tfz_scores, probabilities ):
                self.check( symbol = symbol, tfzs = tfzs, probabilities = probs )


suite_tfz_vs_probability_curve = unittest.TestLoader().loadTestsFromTestCase(
    TestTFZvsProbabilityCurve
    )
suite_tfz_vs_probability_curve_collection = unittest.TestLoader().loadTestsFromTestCase(
    TestTFZvsProbabilityCurveCollection
    )
suite_tfz_based_probability = unittest.TestLoader().loadTestsFromTestCase(
    TestTFZBasedProbability
    )

alltests = unittest.TestSuite(
    [
        suite_tfz_vs_probability_curve,
        suite_tfz_vs_probability_curve_collection,
        suite_tfz_based_probability,
        ]
    )

if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
