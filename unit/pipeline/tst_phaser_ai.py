import unittest

import scitbx.matrix
import scitbx.math

from phaser.pipeline import phaser_ai
from phaser.pipeline.scaffolding import Fake

def matching_mrpeak(peaks, crystal, dmin, cached):

    from phaser import matching

    if cached:
        detail_orientation = matching.cached_rotation_differences
        detail_position = matching.cached_position_differences

    else:
        detail_orientation = matching.simple_rotation_differences
        detail_position = matching.simple_position_differences

    structure = []

    for ( euler, frac, ense ) in peaks:
        peak = matching.Labelled.phaser_mr_solution(
            rotation = scitbx.math.euler_angles_zyz_matrix( *euler ),
            translation = frac,
            crystal = crystal,
            ensemble = ense,
            dmin = dmin,
            detail_position = detail_position,
            detail_orientation = detail_orientation,
            )
        structure.append( matching.OriginSearchEntity( entity = peak ) )

    return structure


class TestSolutionSelection(unittest.TestCase):

    def test_slt(self):

        case = Fake()
        case.problem = Fake()
        case.problem.root = object()
        case.partials = [ case.problem.root ]
        case.score_for = {}
        case.score_for[ case.problem.root ] = 0.0
        ensemble = Fake()
        ensemble.centre = scitbx.matrix.col(
            ( 17.751147332483825, -0.007434299385070427, 0.00925503581549817 )
            )
        ensemble.extent = 14.072787449410832
        ensemble.pgops = [
            scitbx.matrix.rt(
                ( ( 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 ), ( 0.0, 0.0, 0.0 ) )
                ),
            ]

        from phaser import matching
        from cctbx import sgtbx
        from cctbx import uctbx
        crystal = matching.CrystalInfo(
            cell = uctbx.unit_cell(
                parameters = ( 127.5, 97.7,164.2, 90.0, 90.0, 90.0 )
                ),
            space_group = sgtbx.space_group_info( number = 19 ).group()
            )

        data = [
             ( ( -86.531, 140.495,   -5.496 ), ( -0.169, 0.038, -0.037 ),   40.0 ),
             ( ( -97.401,  39.497,  101.308 ), ( -0.164, 0.542,  0.025 ),  125.9 ),
             ( ( -83.758, 142.078,  -75.291 ), ( -0.171, 0.036, -0.035 ),  248.9 ),
             ( ( -86.405, 143.626, -148.781 ), ( -0.169, 0.033, -0.034 ),  375.9 ),
             ( ( -93.107,  38.505,   25.234 ), ( -0.166, 0.541,  0.027 ),  539.7 ),
             ( (  41.618, 116.006,  138.859 ), (  0.028, 0.161, -0.270 ),  722.6 ),
             ( ( -94.970,  41.396,  169.930 ), ( -0.166, 0.546,  0.028 ),  979.3 ),
             ( (  41.159, 113.975,   67.805 ), (  0.025, 0.158, -0.272 ), 1239.2 ),
             ( (  45.414, 115.985,  -75.225 ), (  0.029, 0.159, -0.271 ), 1550.8 ),
             ( (  44.600, 113.705,   -2.757 ), (  0.028, 0.157, -0.272 ), 1896.9 ),
             ( ( -89.792, 141.769,  135.448 ), ( -0.169, 0.034, -0.036 ), 2269.7 ),
             ( ( -90.238,  41.728, -121.027 ), ( -0.167, 0.549,  0.028 ), 2664.7 ),
             ( ( -89.868,  39.216,  -49.859 ), ( -0.168, 0.545,  0.027 ), 3075.2 ),
             ( ( -90.322, 139.786,   64.664 ), ( -0.169, 0.041, -0.037 ), 3612.3 ),
             ( (  44.219, 118.005, -147.191 ), (  0.027, 0.161, -0.268 ), 4186.8 ),
             ( ( 134.082, 116.485,   57.476 ), ( -0.036, 0.142,  0.233 ), 4718.6 ),
             ( ( 134.836, 120.409, -158.525 ), ( -0.037, 0.144,  0.237 ), 5332.0 ),
             ( ( 137.146, 118.987,  -85.622 ), ( -0.034, 0.146,  0.236 ), 6043.6 ),
             ( ( 135.708, 116.582,  -14.714 ), ( -0.035, 0.144,  0.234 ), 6715.6 ),
             ( ( 133.062, 119.368,  128.479 ), ( -0.036, 0.142,  0.236 ), 7470.7 ),
            ]

        for ( index, coords ) in enumerate( data, start = 1 ):
            st = Fake()
            st.molecules = [ ( euler, frac, ensemble ) for ( euler, frac, llg ) in data[ : index ] ]
            case.score_for[ st ] = coords[ -1 ]
            case.partials.append( st )

        case.create_matching_origin_search_structure = lambda structure, cached = False, template_equivalence = False: (
            matching_mrpeak(
                peaks = structure.molecules,
                crystal = crystal,
                dmin = 2.751,
                cached = cached
                )
            )

        result = phaser_ai.SolutionSelection( case = case, threshold = 0.75, template_equivalence = False )
        self.assertEqual( result.case, case )
        self.assertAlmostEqual( result.best_score, 7470.7, 1 )
        self.assertAlmostEqual( result.threshold_score, 5603.0, 1 )
        self.assertEqual( result.thresholdeds, case.partials[ 20 : 17 : -1 ] )
        self.assertEqual( result.solutions, [ case.partials[20] ] )

        # Add some bad molecules
        extra = [
            ( (   0.000,   0.000,    0.000 ), (  0.000, 0.000,  0.000 ), 7450.7 ),
            ( (  50.000,  50.000,   50.000 ), (  0.100, 0.200,  0.300 ), 6850.7 ),
            ]

        for ( euler, frac, llg ) in extra:
            st = Fake()
            st.molecules = [ ( e, f, ensemble ) for ( e, f, s ) in data ] + [ ( euler, frac, ensemble ) ]
            case.score_for[ st ] = llg
            case.partials.append( st )

        result = phaser_ai.SolutionSelection( case = case, threshold = 0.75, template_equivalence = False )
        self.assertEqual( result.case, case )
        self.assertAlmostEqual( result.best_score, 7470.7, 1 )
        self.assertAlmostEqual( result.threshold_score, 5603.0, 1 )
        self.assertEqual(
            result.thresholdeds,
            [ case.partials[20], case.partials[21], case.partials[22], case.partials[19], case.partials[18] ]
            )
        self.assertEqual(
            result.solutions,
            [ case.partials[20], case.partials[21], case.partials[22] ]
            )


suite_solution_selection = unittest.TestLoader().loadTestsFromTestCase(
    TestSolutionSelection
    )

alltests = unittest.TestSuite(
    [
        suite_solution_selection,
        ]
    )

if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
