from phaser.pipeline import mr_object

import scitbx.matrix
from scitbx.array_family import flex

import unittest
import math

class TestEquivalenceRelation(unittest.TestCase):

    def setUp(self):

        self.obj = mr_object.EquivalenceRelation(
            relation = lambda l, r: abs( l - r ) < 0.01
            )

        for n in [ 0.87, 0.875, 0.878, 0.881, 0.976, 0.988, 0.982, 0.999, 1.006, 1.001 ]:
            self.obj.add( value = n )


    def test_equivalents_of(self):

        self.assertEqual(
            sorted( self.obj.equivalents_of( value = 0.87 ) ),
            [ 0.875, 0.878 ]
            )
        self.assertEqual(
            sorted( self.obj.equivalents_of( value = 0.875 ) ),
            [ 0.87, 0.878, 0.881 ]
            )
        self.assertEqual(
            sorted( self.obj.equivalents_of( value = 0.878 ) ),
            [ 0.87, 0.875, 0.881 ]
            )
        self.assertEqual(
            sorted( self.obj.equivalents_of( value = 0.881 ) ),
            [ 0.875, 0.878 ]
            )

        self.assertEqual(
            sorted( self.obj.equivalents_of( value = 0.988 ) ),
            [ 0.982 ]
            )
        self.assertEqual(
            sorted( self.obj.equivalents_of( value = 1.001 ) ),
            sorted( [ 0.999, 1.006 ] )
            )


    def test_equivalence_group_of(self):

        self.assertEqual(
            self.obj.equivalence_group_of( value = 0.87 ),
            set( [ 0.87, 0.875, 0.878, 0.881 ] )
            )
        self.assertEqual(
            self.obj.equivalence_group_of( value = 0.988 ),
            set( [ 0.976, 0.982, 0.988 ] )
            )
        self.assertEqual(
            self.obj.equivalence_group_of( value = 1.001 ),
            set( [ 0.999, 1.001, 1.006 ] )
            )


    def test_classes(self):

        self.assertEqual(
            set( [ frozenset( o ) for o in self.obj.classes() ] ),
            set(
                [
                    frozenset( [ 0.87, 0.875, 0.878, 0.881 ] ),
                    frozenset( [ 0.976, 0.982, 0.988 ] ),
                    frozenset( [ 0.999, 1.001, 1.006 ] ),
                    ]
                )
            )
        self.obj.add( 0.995 )
        self.assertEqual(
            set( [ frozenset( o ) for o in self.obj.classes() ] ),
            set(
                [
                    frozenset( [ 0.87, 0.875, 0.878, 0.881 ] ),
                    frozenset( [ 0.976, 0.982, 0.988, 0.995, 0.999, 1.001, 1.006 ] ),
                    ]
                )
            )


class TestEquivalenceRelationChain(unittest.TestCase):

    def test_floats(self):

        relations = [
            [ 0.87, 0.875, 0.878, 0.881 ],
            [ 0.976, 0.982, 0.988, 0.995, 0.999, 1.001, 1.006 ],
            ]

        ec = mr_object.EquivalenceRelationChain()

        for r in relations:
            ec.add( relation = r )

        self.assertEqual( ec.classes, set( frozenset( r ) for r in relations ) )

        minimal = [
            [ 0.87, 0.875 ], [ 0.87, 878 ], [ 0.878, 0.881 ],
            [ 0.976, 0.982 ], [ 0.982, 0.988 ], [ 0.988, 0.995 ],
            [ 0.995, 0.999 ], [ 0.995, 1.001 ], [ 0.999, 1.006 ],
            ]

        ec = mr_object.EquivalenceRelationChain()

        for r in relations:
            ec.add( relation = r )

        self.assertEqual( ec.classes, set( frozenset( r ) for r in relations ) )



class TestGenerator(unittest.TestCase):

    def assertGeneratorsAlmostEqual(self, left, right, angular, spatial):

        self.assertEqual( left.fold, right.fold )

        if left.axis.dot( right.axis ) < 0:
            right = right.inverted_axis_setting()

        self.assertAlmostEqual( left.axis.dot( right.axis ), 1, angular )
        self.assertAlmostEqual(
            ( left.displacement - right.displacement ).length(),
            0,
            spatial
            )


    def get_randomized_operator(self, fold):

        axis = scitbx.matrix.col( flex.random_double_point_on_sphere() )
        import random
        translation = random.randint( 10, 20 ) * scitbx.matrix.col(
            flex.random_double_point_on_sphere()
            )
        t_par = axis.dot( translation ) * axis
        return mr_object.Generator.from_rotation_translation(
            power = 1,
            fold = fold,
            axis = axis,
            translation = translation - t_par
            )


    def recognition_tests(self, generator):

        import fractions

        for ( power, rt ) in enumerate( generator.nontrivial_series(), start = 1 ):
            gcd = fractions.gcd( power, generator.fold )
            expected = mr_object.Generator(
                fold = generator.fold // gcd,
                axis = generator.axis,
                centre = generator.centre
                )
            mismatch = scitbx.matrix.rt(
                (
                    scitbx.math.r3_rotation_axis_and_angle_as_matrix(
                        flex.random_double_point_on_sphere(),
                        0.01 / generator.fold
                        ),
                    0.01 * scitbx.matrix.col( flex.random_double_point_on_sphere() ),
                    )
                )
            recovered = mr_object.Generator.from_scitbx_rt_operator(
                op = rt * mismatch,
                max_fold = 20,
                angular = 0.01,
                spatial = 0.1
                )
            self.assertGeneratorsAlmostEqual( expected, recovered, 2, 1 )


    def transformation_test(self, generator):

        transformation = scitbx.matrix.rt(
            (
                scitbx.math.r3_rotation_axis_and_angle_as_matrix(
                    flex.random_double_point_on_sphere(),
                    1.0
                    ),
                scitbx.matrix.col( flex.random_double_point_on_sphere() ),
                )
            )

        transformed = generator.transformed( operation = transformation )
        expected = transformation * generator.rt * transformation.inverse()
        result =  expected * transformed.rt.inverse()

        self.assertAlmostEqual(
            math.acos( scitbx.math.r3_rotation_cos_rotation_angle_from_matrix( result.r ) ),
            0,
            7
            )
        self.assertAlmostEqual( result.t.length(), 0, 7 )


    def centre_test(self, generator):

        self.assertAlmostEqual(
            ( generator.centre - generator.rt * generator.centre ).length(),
            0,
            7
            )


    def equality_comparisons(self, left, right, misalignment, spatial):

        self.assertTrue(
            left.approx_equal_spatially(
                other = right,
                max_misalignment = 1.1 * misalignment,
                max_spatial_error = 1.1 * spatial
                )
            )
        self.assertTrue(
            right.approx_equal_spatially(
                other = left,
                max_misalignment = 1.1 * misalignment,
                max_spatial_error = 1.1 * spatial
                )
            )
        self.assertFalse(
            left.approx_equal_spatially(
                other = right,
                max_misalignment = 0.9 * misalignment,
                max_spatial_error = 0.9 * spatial
                )
            )
        self.assertFalse(
            right.approx_equal_spatially(
                other = left,
                max_misalignment = 0.9 * misalignment,
                max_spatial_error = 0.9 * spatial
                )
            )


    def single_equality_test(self, expected, fold, axis, centre, misalignment, spatial):

        transformed = mr_object.Generator(
            fold = fold,
            axis = axis,
            centre = centre,
            )

        # Normal
        self.equality_comparisons(
            left = expected,
            right = transformed,
            misalignment = misalignment,
            spatial = spatial
            )

        # Inverted
        self.equality_comparisons(
            left = expected,
            right = transformed.inverted_axis_setting(),
            misalignment = misalignment,
            spatial = spatial
            )


    def equality_tests(self, generator):

        misalignment = 0.01
        spatial = 0.15
        mismatch_rot = scitbx.matrix.sqr(
            scitbx.math.r3_rotation_axis_and_angle_as_matrix(
                generator.displacement,
                math.acos( 1 - misalignment )
                )
            )
        mismatch_disp = spatial * generator.displacement.normalize()

        # Misalignment
        self.single_equality_test(
            expected = generator,
            fold = generator.fold,
            axis = scitbx.matrix.col( mismatch_rot * generator.axis ),
            centre = generator.centre,
            misalignment = misalignment,
            spatial = spatial / 10.0
            )
        self.single_equality_test(
            expected = generator,
            fold = generator.fold * 3,
            axis = scitbx.matrix.col( mismatch_rot * generator.axis ),
            centre = generator.centre,
            misalignment = misalignment,
            spatial = spatial / 10.0
            )

        # Mismatch
        self.single_equality_test(
            expected = generator,
            fold = generator.fold,
            axis = generator.axis,
            centre = generator.centre + mismatch_disp,
            misalignment = misalignment / 10.0,
            spatial = spatial
            )
        self.single_equality_test(
            expected = generator,
            fold = generator.fold * 3,
            axis = generator.axis,
            centre = generator.centre + mismatch_disp,
            misalignment = misalignment / 10.0,
            spatial = spatial
            )


    def subgroup_supergroup_test(self, generator):

        generator.is_rotational_supergroup_of( other = generator )
        higher = mr_object.Generator(
            fold = 4 * generator.fold,
            axis = generator.axis,

            )


    def test_folds(self):

        for fold in range( 2, 13 ):
            gen = self.get_randomized_operator( fold = fold )
            self.centre_test( generator = gen )
            self.recognition_tests( generator = gen )
            self.transformation_test( generator = gen )
            self.equality_tests( generator = gen )


    def test_1(self):

        # Transf pentamer
        op1 = scitbx.matrix.rt(
            (
                ( 0.3137, -0.2287, 0.9216, 0.1176, 0.9724, 0.2013, -0.9422, 0.04528, 0.3319 ),
                ( 32.04, 3.313, -3.397 ),
                )
            )
        gen1 = mr_object.Generator.from_scitbx_rt_operator( op = op1 )

        # Orig pentamer
        op2 = scitbx.matrix.rt(
            (
                ( 0.3101, -0.9505, 0.01812, 0.9499, 0.3091, -0.04553, 0.03768, 0.03133, 0.9988 ),
                ( -0.09189, 0.329, 0.007173 ),
                )
            )
        gen2 = mr_object.Generator.from_scitbx_rt_operator( op = op2 )

        # Sup transf to orig
        sup = scitbx.matrix.rt(
            (
                ( -0.9847, -0.003724, -0.1741, -0.1696, -0.2064, 0.9637, -0.03952, 0.9785, 0.2027 ),
                ( 9.888, 26.03, 25.56 ),
                )
            )
        self.assertGeneratorsAlmostEqual( gen1.transformed( operation = sup ), gen2, 2, 0 )


suite_equivalence_relation = unittest.TestLoader().loadTestsFromTestCase(
    TestEquivalenceRelation
    )
suite_equivalence_relation_chain = unittest.TestLoader().loadTestsFromTestCase(
    TestEquivalenceRelationChain
    )
suite_generator = unittest.TestLoader().loadTestsFromTestCase(
    TestGenerator
    )

alltests = unittest.TestSuite(
    [
        suite_equivalence_relation,
        suite_equivalence_relation_chain,
        suite_generator,
        ]
    )

if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
