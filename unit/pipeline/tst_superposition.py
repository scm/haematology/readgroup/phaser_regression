import unittest
import os.path
import libtbx.load_env
import scitbx.matrix

FOLDER = os.path.join(
  libtbx.env.under_dist( "phaser_regression", "data" ),
  "regression",
  )

from phaser.pipeline import superposition
from phaser.pipeline import mr_object
from phaser.pipeline.scaffolding import Fake, Return, Raise, IterableCompareTestCase

class TestSimpleSSMSuperposition(IterableCompareTestCase):

    def test_method(self):

        import iotbx.pdb

        inp1 = iotbx.pdb.input( os.path.join( FOLDER, "1fkq_prot.pdb" ) )
        ensemble1 = Fake()
        ensemble1.root = Return( values = [ inp1.construct_hierarchy() ] )

        inp2 = iotbx.pdb.input( os.path.join( FOLDER, "LYSO_IOD_PARTIAL.pdb" ) )
        ensemble2 = Fake()
        ensemble2.root = Return( values = [ inp2.construct_hierarchy() ] )


        sup = superposition.simple_ssm_superposition(
            reference = ensemble1,
            moving = ensemble2,
            )
        self.assertTrue( isinstance( sup, superposition.Superposition ) )
        self.assertAlmostEqual( sup.rmsd, 0, 3 )
        self.assertIterablesAlmostEqual(
            sup.transformation.r.elems,
            ( 0.7197, -0.2229, -0.6575, -0.3346, 0.7185, -0.6098, 0.6083, 0.6589, 0.4425 ),
            4,
            )
        self.assertIterablesAlmostEqual(
            sup.transformation.t.elems,
            ( 30.34, 16.35, 11.71 ),
            2,
            )


class TestProxy(unittest.TestCase):

    def setUp(self):

        self.result = superposition.Superposition(
            transformation = scitbx.matrix.rt(
                ( ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ), ( 0, 0, 0 ) )
                ),
            rmsd = 0,
            )
        self.proxy = superposition.Proxy(
            method = Return( values = [ self.result ] )
            )
        self.proxy.method.NAME = "Fake superposition"
        from libtbx.utils import null_out
        self.stream = null_out()
        import sys
        #self.stream = sys.stdout
        self.ensemble1 = Fake()
        self.ensemble2 = Fake()
        self.templ1 = object()
        self.templ2 = object()
        self.ensemble1.superpose_template = Return( values = [ self.templ1 ] )
        self.ensemble2.superpose_template = Return( values = [ self.templ2 ] )


    def test_superpose_identical(self):

        sup_templ = object()
        self.ensemble1.superpose_template = Return( values = [ sup_templ ] )
        self.ensemble2.superpose_template = Return( values = [ sup_templ ] )
        sup = self.proxy.superpose(
            reference = self.ensemble1,
            moving = self.ensemble2,
            stream = self.stream,
            )
        self.assertEqual( sup, superposition.Superposition.unit() )


    def test_superpose_cache_direct(self):

        val = superposition.Superposition(
            transformation = scitbx.matrix.rt(
                ( ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ), ( 0, 0, 0 ) )
                ),
            rmsd = 0,
            )
        self.proxy.cache[ ( self.templ1, self.templ2 ) ] = val
        sup = self.proxy.superpose(
            reference = self.ensemble1,
            moving = self.ensemble2,
            stream = self.stream,
            )
        self.assertEqual( sup, val )


    def test_superpose_cache_inverted(self):

        val = superposition.Superposition(
            transformation = scitbx.matrix.rt(
                ( ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ), ( 0, 0, 0 ) )
                ),
            rmsd = 0,
            )
        ival = Fake()
        ival.inverse = Return( values = [ val ] )

        self.proxy.cache[ ( self.templ2, self.templ1 ) ] = ival
        sup = self.proxy.superpose(
            reference = self.ensemble1,
            moving = self.ensemble2,
            stream = self.stream,
            )
        self.assertEqual( sup, val )


    def test_superpose_cached_failure(self):

        self.proxy.failures.add( ( self.templ1, self.templ2 ) )
        self.proxy.failures.add( ( self.templ2, self.templ1 ) )
        self.assertRaises(
            RuntimeError,
            self.proxy.superpose,
            reference = self.ensemble1,
            moving = self.ensemble2,
            stream = self.stream,
            )


    def test_superpose_method_success(self):

        sup = self.proxy.superpose(
            reference = self.ensemble1,
            moving = self.ensemble2,
            stream = self.stream,
            )
        self.assertEqual( sup, self.result )


    def test_superpose_method_failure(self):

        self.proxy.method = Raise( exception = RuntimeError, message = "Simulated error" )
        self.proxy.method.NAME = "Fake superposition"
        self.assertFalse( ( self.templ1, self.templ2 ) in self.proxy.failures )
        self.assertFalse( ( self.templ2, self.templ1 ) in self.proxy.failures )
        self.assertRaises(
            RuntimeError,
            self.proxy.superpose,
            reference = self.ensemble1,
            moving = self.ensemble2,
            stream = self.stream,
            )
        self.assertTrue( ( self.templ1, self.templ2 ) in self.proxy.failures )
        self.assertTrue( ( self.templ2, self.templ1 ) in self.proxy.failures )


suite_simple_ssm_superpose = unittest.TestLoader().loadTestsFromTestCase(
    TestSimpleSSMSuperposition
    )
suite_proxy = unittest.TestLoader().loadTestsFromTestCase(
    TestProxy
    )

alltests = unittest.TestSuite(
    [
        suite_simple_ssm_superpose,
        suite_proxy,
        ]
    )


def load_tests(loader, tests, pattern):

    return alltests


if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
