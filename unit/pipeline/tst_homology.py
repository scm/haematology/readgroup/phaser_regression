from phaser.pipeline import homology

import unittest


class TestNCBIBlastXMLParse(unittest.TestCase):

    def setUp(self):

        self.hs = homology.parse(
            data = XML_NCBI_BLAST,
            builder = homology.BLAST_BUILDER,
            data_type = homology.Blast
            )


    def testHsps(self):

        hsp = self.hs.root.iterations[0].hits[4].hsps[0]
        self.assertEqual( hsp.num, 1 )
        self.assertAlmostEqual( hsp.bit_score, 159.073, 3 )
        self.assertEqual( hsp.score, 401 )
        self.assertAlmostEqual( hsp.evalue, 6.95488e-40, 8 )
        self.assertEqual( hsp.query.start, 8 )
        self.assertEqual( hsp.query.end, 192 )
        self.assertEqual( hsp.query.frame, 0 )
        self.assertEqual( hsp.hit.start, 2 )
        self.assertEqual( hsp.hit.end, 193 )
        self.assertEqual( hsp.hit.frame, 0 )
        self.assertEqual( hsp.identity, 78 )
        self.assertEqual( hsp.positive, 127 )
        self.assertEqual( hsp.gaps, 7 )
        self.assertEqual( hsp.length, 192 )
        self.assertEqual(
            hsp.query.seq,
            "KSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSS-GSARGKMLSEIMEKGQLVPLET"
            + "VLDMLR---DAMVAKVDTSKGFLIDGYPREVKQGEEFERKI---GQPTLLLYVDAGPETMTKRL"
            + "LKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLD"
            )
        self.assertEqual(
            hsp.hit.seq,
            "KPLVVFVLGGPGAGKGTQCARIVEKYGYTHLSAGELLRDERKNPDSQYGELIEKYIKEGKIVPVEI"
            + "TISLLKREMDQTMAANAQKNKFLIDGFPRNQDNLQGWNKTMDGKADVSFVLFFDCNNEICIERC"
            + "LERGKSSGRSDDNRESLEKRIQTYLQSTKPIIDLYEEMGKVKKIDASKSVDEVFDEVVQIFD"
            )
        self.assertEqual(
            hsp.midline,
            "K  ++FV+GGPG+GKGTQC +IV+KYGYTHLS G+LLR E  +  S  G+++ + +++G++VP+E "
            + " + +L+   D  +A       FLIDG+PR     + + + +      + +L+ D   E   +R "
            + "L+RG++SGR DDN E+++KR++TY ++T+P+I  YE+ G V+K++A  SVD+VF +V    D"
            )


    def testHits(self):

        hit = self.hs.root.iterations[0].hits[0]

        self.assertEqual( hit.num, 1 )
        self.assertEqual( hit.identifier, "gi|157836759|pdb|3ADK|A" )
        self.assertEqual(
            hit.annotation,
            "Chain A, Refined Structure Of Porcine Cytosolic Adenylate Kinase "
            + "At 2.1 Angstroms Resolution"
            )
        self.assertEqual( hit.accession, "3ADK-A" )
        self.assertEqual( hit.length, 194 )
        self.assertEqual( len( hit.hsps ), 1 )


    def testIterations(self):

        iteration = self.hs.root.iterations[0]

        self.assertEqual( iteration.num, 1 )
        self.assertEqual( iteration.query_id, "77636" )
        self.assertEqual( iteration.query_def, "3ADK:A|PDBID|CHAIN|SEQUENCE" )
        self.assertEqual( iteration.query_len, 195 )
        self.assertEqual( len( iteration.hits ), 68 )
        self.assertEqual( iteration.statistics.db_num, 40680 )
        self.assertEqual( iteration.statistics.db_len, 9289891 )
        self.assertEqual( iteration.statistics.hsp_len, 0 )
        self.assertEqual( iteration.statistics.eff_space, 0 )
        self.assertEqual( iteration.statistics.kappa, 0.041 )
        self.assertEqual( iteration.statistics.lambdav, 0.267 )
        self.assertEqual( iteration.statistics.entropy, 0.14 )


    def testRoot(self):

        output = self.hs.root
        self.assertEqual( output.program, "blastp" )
        self.assertEqual( output.version, "BLASTP 2.2.20+" )
        self.assertEqual(
            output.reference,
            """Alejandro A. Sch&auml;ffer, L. Aravind, Thomas L. Madden, Sergei Shavirin, John L. Spouge, Yuri I. Wolf, Eugene V. Koonin, and Stephen F. Altschul (2001), "Improving the accuracy of PSI-BLAST protein database searches with composition-based statistics and other refinements", Nucleic Acids Res. 29:2994-3005."""
            )
        self.assertEqual( output.db, "pdb" )
        self.assertEqual( output.query_id, "77636" )
        self.assertEqual( output.query_def, "3ADK:A|PDBID|CHAIN|SEQUENCE" )
        self.assertEqual( output.query_len, 195 )
        self.assertEqual( len( output.iterations ), 1 )
        self.assertEqual( output.parameters.matrix, "BLOSUM62" )
        self.assertEqual( output.parameters.expect, 10 )
        self.assertEqual( output.parameters.gap_open, 11 )
        self.assertEqual( output.parameters.gap_extend, 1 )
        self.assertEqual( output.parameters.filter, "F" )


class TestGetNCBIParser(TestNCBIBlastXMLParse):

    def setUp(self):

        self.hs = homology.get_ncbi_blast_parser()( data = XML_NCBI_BLAST )


class TestWUBlastXMLParse(unittest.TestCase):

    def setUp(self):

        self.hs = homology.parse(
            data = XML_EBI_WU_BLAST,
            builder = homology.EBI_WU_BLAST_BUILDER,
            data_type = homology.EBIWUBlast
            )


    def testAlignment(self):

        ali = self.hs.root.hits[6].alignments[0]
        self.assertEqual( ali.number, 1 )
        self.assertEqual( ali.score, 605 )
        self.assertAlmostEqual( ali.bits, 218.0, 1 )
        self.assertAlmostEqual( ali.expectation, 2.0e-59, 10 )
        self.assertAlmostEqual( ali.probability, 2.0e-59, 10 )
        self.assertEqual( ali.identity, 58 )
        self.assertEqual( ali.positives, 79 )
        self.assertEqual( ali.query.start, 4 )
        self.assertEqual( ali.query.end, 194 )
        self.assertEqual(
            ali.query.seq,
            "EKLKKSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVP"
            + "LETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKR"
            + "GETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTL"
            )
        self.assertEqual( ali.match.start, 7 )
        self.assertEqual( ali.match.end, 197 )
        self.assertEqual(
            ali.match.seq,
            "EDLRKCKIIFIIGGPGSGKGTQCEKLVEKYGFTHLSTGELLREELASESERSKLIRDIMERGDLVP"
            + "SGIVLELLKEAMVASLGDTRGFLIDGYPREVKQGEEFGRRIGDPQLVICMDCSADTMTNRLLQM"
            + "SRSSLPVDDTTKTIAKRLEAYYRASIPVIAYYETKTQLHKINAEGTPEDVFLQLCTAIDSI"
            )
        self.assertEqual(
            ali.pattern,
            "E L+K KIIF++GGPGSGKGTQCEK+V+KYG+THLSTG+LLR E++S S R K++ +IME+G LVP"
            + "   VL++L++AMVA +  ++GFLIDGYPREVKQGEEF R+IG P L++ +D   +TMT RLL+ "
            + "  +S  VDD  +TI KRLE YY+A+ PVIA+YE +  + K+NAEG+ +DVF Q+CT +D++"
            )


    def testHit(self):

        hit = self.hs.root.hits[0]

        self.assertEqual( hit.number, 1 )
        self.assertEqual( hit.database, "pdb" )
        self.assertEqual( hit.identifier, "3ADK_A" )
        self.assertEqual( hit.length, 195 )
        self.assertEqual( hit.description, "mol:protein length:195  ADENYLATE KINASE" )
        self.assertEqual( len( hit.alignments ), 1 )


    def testRoot(self):

        result = self.hs.root

        self.assertEqual( result.program.name, "WU-blastp" )
        self.assertEqual( result.program.version, "2.0MP-WashU [04-May-2006]" )
        self.assertEqual( result.program.citation, "PMID:12824421" )
        self.assertEqual(
            result.command,
            "/ebi/extserv/bin/wu-blast/blastp \"pdb\" /ebi/extserv/blast-work/interactive/blast-20090331-16013"
            + "83158.input E=10 B=50 V=100 -mformat=1 -matrix BLOSUM62 -sump  -filter seg -cpus 8 -sort_by_pvalue -putenv='"
            + "WUBLASTMAT=/ebi/extserv/bin/wu-blast/matrix' -putenv=\"WUBLASTDB=$IDATA_CURRENT/blastdb\" -putenv="
            + "'WUBLASTFILTER=/ebi/extserv/bin/wu-blast/filter' "
            )
        self.assertEqual( result.time.start, "2009-03-31T16:01:44+01:00" )
        self.assertEqual( result.time.end, "2009-03-31T16:01:45+01:00" )
        self.assertEqual( result.time.search, "PT01S" )
        self.assertEqual( len( result.parameters.sequences ), 1 )

        seq = result.parameters.sequences[0]
        self.assertEqual( seq.number, 1 )
        self.assertEqual( seq.name, "Sequence" )
        self.assertEqual( seq.type, "p" )
        self.assertEqual( seq.length, 195 )
        self.assertEqual( len( result.parameters.databases ), 1 )

        db = result.parameters.databases[0]
        self.assertEqual( db.number, 1 )
        self.assertEqual( db.name, "pdb" )
        self.assertEqual( db.type, "p" )
        self.assertEqual( db.created, "2009-03-27T00:00:07+01:00" )

        self.assertEqual( result.parameters.scores, 100.0 )
        self.assertEqual( result.parameters.alignments, 50 )
        self.assertEqual( result.parameters.matrix, "BLOSUM62" )
        self.assertEqual( result.parameters.expectation_upper, 10.0 )
        self.assertEqual( result.parameters.statistics, "sump" )
        self.assertEqual( result.parameters.filter, "seg" )
        self.assertEqual( len( result.hits ), 50 )


class TestCathXMLParse(unittest.TestCase):

    def setUp(self):

        self.cath = homology.parse(
            data = XML_CATH_2CHL,
            builder = homology.CATH_ANNOTATION_BUILDER,
            data_type = homology.CathAnnotation
            )


    def testRoot(self):

        result = self.cath.root.query

        self.assertEqual( result.pdb_code, "2chl" )
        self.assertEqual( result.chain_code, "" )
        self.assertEqual( result.domain_code, "" )
        self.assertEqual( result.domain_count, 2 )


    def testDomain(self):

        res = [
            ( "2chlA01", "2chl","A", "01", 84,
                "VLMVGPNFRVGKKIGCGNFGELRLGKNLYTNEYVAIKLEPMKSRAPQLHLEYRFYKQLGSGDGIPQVYYFGPCGKYNAMVLELL",
                "VLMVGPNFRVGKKIGCGNFGELRLGKNLYTNEYVAIKLEPMKSRAPQLHLEYRFYKQLGSGDGIPQVYYFGPCGKYNAMVLELL",
                "3", "30", "200", "20", "5", "1", "1", "1", "3", "3.30.200.20.5.1.1.1.3" ),
            ( "2chlA02", "2chl", "A", "02", 214,
                "GPSLEDLFDLCDRTFSLKTVLMIAIQLISRMEYVHSKNLIYRDVKPENFLIGRPGNKTQQVIHIIDFALAKEYIDPETKKHIPYREHKSLTGTARYMSINTHLGKEQSRRDDLEALGHMFMYFLRGSLPWQGLKADTLKERYQKIGDTKRATPIEVLCENFPEMATYLRYVRRLDFFEKPDYDYLRKLFTDLFDRKGYMFDYEYDWIGKQLPTP",
                "GPSLEDLFDLCDRTFSLKTVLMIAIQLISRMEYVHSKNLIYRDVKPENFLIGRPGNKTQQVIHIIDFALAKEYIDPETKKHIPYREHKSLTGTARYMSINTHLGKEQSRRDDLEALGHMFMYFLRGSLPWQGLKADTLKERYQKIGDTKRATPIEVLCENFPEMATYLRYVRRLDFFEKPDYDYLRKLFTDLFDRKGYMFDYEYDWIGKQLPTP",
                "1", "10", "510", "10", "8", "1", "1", "1", "3", "1.10.510.10.8.1.1.1.3" )
            ]

        self.assertEqual( len( self.cath.root.query.domains ), len( res ) )

        for ( data, node ) in zip( res, self.cath.root.query.domains ):
            self.assertEqual( node.domain_id, data[0] )
            self.assertEqual( node.pdb_id, data[1] )
            self.assertEqual( node.chain_code, data[2] )
            self.assertEqual( node.domain_code, data[3] )
            self.assertEqual( node.dom_length, data[4] )
            self.assertEqual( node.dom_atom_sequence, data[5] )
            self.assertEqual( node.dom_combs_sequence, data[6] )

            ann = node.cath_code

            self.assertEqual( ann.class_code, data[7] )
            self.assertEqual( ann.arch_code, data[8] )
            self.assertEqual( ann.top_code, data[9] )
            self.assertEqual( ann.homol_code, data[10] )
            self.assertEqual( ann.s35_code, data[11] )
            self.assertEqual( ann.s60_code, data[12] )
            self.assertEqual( ann.s95_code, data[13] )
            self.assertEqual( ann.s100_code, data[14] )
            self.assertEqual( ann.s100_count, data[15] )
            self.assertEqual( ann.uni_code, data[16] )


    def testSegments(self):

        data = [
            [
                ( 1, "25", "108", 84, "VLMVGPNFRVGKKIGCGNFGELRLGKNLYTNEYVAIKLEPMKSRAPQLHLEYRFYKQLGSGDGIPQVYYFGPCGKYNAMVLELL" ),
                ],
            [
                ( 1, "109", "322", 214, "GPSLEDLFDLCDRTFSLKTVLMIAIQLISRMEYVHSKNLIYRDVKPENFLIGRPGNKTQQVIHIIDFALAKEYIDPETKKHIPYREHKSLTGTARYMSINTHLGKEQSRRDDLEALGHMFMYFLRGSLPWQGLKADTLKERYQKIGDTKRATPIEVLCENFPEMATYLRYVRRLDFFEKPDYDYLRKLFTDLFDRKGYMFDYEYDWIGKQLPTP" ),
                ]
            ]

        self.assertEqual( len( data ), len( self.cath.root.query.domains ) )

        for ( domdata, domnode ) in zip( data, self.cath.root.query.domains ):
            self.assertEqual( len( domdata ), len( domnode.segments ) )

            for ( segdata, segnode ) in zip( domdata, domnode.segments ):
                self.assertEqual( segnode.seg_num, segdata[0] )
                self.assertEqual( segnode.pdb_start, segdata[1] )
                self.assertEqual( segnode.pdb_stop, segdata[2] )
                self.assertEqual( segnode.slength, segdata[3] )
                self.assertEqual( segnode.sseqs, segdata[4] )


XML_NCBI_BLAST = """<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE BlastOutput PUBLIC "-//NCBI//NCBI BlastOutput/EN" "NCBI_BlastOutput.dtd">
<BlastOutput>
  <BlastOutput_program>blastp</BlastOutput_program>
  <BlastOutput_version>BLASTP 2.2.20+</BlastOutput_version>
  <BlastOutput_reference>Alejandro A. Sch&amp;auml;ffer, L. Aravind, Thomas L. Madden, Sergei Shavirin, John L. Spouge, Yuri I. Wolf, Eugene V. Koonin, and Stephen F. Altschul (2001), "Improving the accuracy of PSI-BLAST protein database searches with composition-based statistics and other refinements", Nucleic Acids Res. 29:2994-3005.</BlastOutput_reference>
  <BlastOutput_db>pdb</BlastOutput_db>
  <BlastOutput_query-ID>77636</BlastOutput_query-ID>
  <BlastOutput_query-def>3ADK:A|PDBID|CHAIN|SEQUENCE</BlastOutput_query-def>
  <BlastOutput_query-len>195</BlastOutput_query-len>
  <BlastOutput_param>
    <Parameters>
      <Parameters_matrix>BLOSUM62</Parameters_matrix>
      <Parameters_expect>10</Parameters_expect>
      <Parameters_gap-open>11</Parameters_gap-open>
      <Parameters_gap-extend>1</Parameters_gap-extend>
      <Parameters_filter>F</Parameters_filter>
    </Parameters>
  </BlastOutput_param>
  <BlastOutput_iterations>
    <Iteration>
      <Iteration_iter-num>1</Iteration_iter-num>
      <Iteration_query-ID>77636</Iteration_query-ID>
      <Iteration_query-def>3ADK:A|PDBID|CHAIN|SEQUENCE</Iteration_query-def>
      <Iteration_query-len>195</Iteration_query-len>
      <Iteration_hits>
        <Hit>
          <Hit_num>1</Hit_num>
          <Hit_id>gi|157836759|pdb|3ADK|A</Hit_id>
          <Hit_def>Chain A, Refined Structure Of Porcine Cytosolic Adenylate Kinase At 2.1 Angstroms Resolution</Hit_def>
          <Hit_accession>3ADK-A</Hit_accession>
          <Hit_len>194</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>391.734</Hsp_bit-score>
              <Hsp_score>1005</Hsp_score>
              <Hsp_evalue>7.7238e-110</Hsp_evalue>
              <Hsp_query-from>2</Hsp_query-from>
              <Hsp_query-to>195</Hsp_query-to>
              <Hsp_hit-from>1</Hsp_hit-from>
              <Hsp_hit-to>194</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>194</Hsp_identity>
              <Hsp_positive>194</Hsp_positive>
              <Hsp_gaps>0</Hsp_gaps>
              <Hsp_align-len>194</Hsp_align-len>
              <Hsp_qseq>MEEKLKKSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</Hsp_qseq>
              <Hsp_hseq>MEEKLKKSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</Hsp_hseq>
              <Hsp_midline>MEEKLKKSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>2</Hit_num>
          <Hit_id>gi|66361358|pdb|1Z83|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of Human Ak1a In Complex With Ap5a &gt;gi|66361359|pdb|1Z83|B Chain B, Crystal Structure Of Human Ak1a In Complex With Ap5a &gt;gi|66361360|pdb|1Z83|C Chain C, Crystal Structure Of Human Ak1a In Complex With Ap5a &gt;gi|134104061|pdb|2C95|A Chain A, Structure Of Adenylate Kinase 1 In Complex With P1,P4-Di (Adenosine)tetraphosphate &gt;gi|134104062|pdb|2C95|B Chain B, Structure Of Adenylate Kinase 1 In Complex With P1,P4-Di (Adenosine)tetraphosphate</Hit_def>
          <Hit_accession>1Z83-A</Hit_accession>
          <Hit_len>196</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>375.17</Hsp_bit-score>
              <Hsp_score>962</Hsp_score>
              <Hsp_evalue>6.12348e-105</Hsp_evalue>
              <Hsp_query-from>2</Hsp_query-from>
              <Hsp_query-to>194</Hsp_query-to>
              <Hsp_hit-from>2</Hsp_hit-from>
              <Hsp_hit-to>194</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>183</Hsp_identity>
              <Hsp_positive>189</Hsp_positive>
              <Hsp_gaps>0</Hsp_gaps>
              <Hsp_align-len>193</Hsp_align-len>
              <Hsp_qseq>MEEKLKKSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTL</Hsp_qseq>
              <Hsp_hseq>MEEKLKKTNIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRSEVSSGSARGKKLSEIMEKGQLVPLETVLDMLRDAMVAKVNTSKGFLIDGYPREVQQGEEFERRIGQPTLLLYVDAGPETMTQRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDSVFSQVCTHLDAL</Hsp_hseq>
              <Hsp_midline>MEEKLKK+ IIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLR+EVSSGSARGK LSEIMEKGQLVPLETVLDMLRDAMVAKV+TSKGFLIDGYPREV+QGEEFER+IGQPTLLLYVDAGPETMT+RLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVD VFSQVCTHLD L</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>3</Hit_num>
          <Hit_id>gi|73536318|pdb|2BWJ|A</Hit_id>
          <Hit_def>Chain A, Structure Of Adenylate Kinase 5 &gt;gi|73536319|pdb|2BWJ|B Chain B, Structure Of Adenylate Kinase 5 &gt;gi|73536320|pdb|2BWJ|C Chain C, Structure Of Adenylate Kinase 5 &gt;gi|73536321|pdb|2BWJ|D Chain D, Structure Of Adenylate Kinase 5 &gt;gi|73536322|pdb|2BWJ|E Chain E, Structure Of Adenylate Kinase 5 &gt;gi|73536323|pdb|2BWJ|F Chain F, Structure Of Adenylate Kinase 5</Hit_def>
          <Hit_accession>2BWJ-A</Hit_accession>
          <Hit_len>199</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>241.506</Hsp_bit-score>
              <Hsp_score>615</Hsp_score>
              <Hsp_evalue>1.06554e-64</Hsp_evalue>
              <Hsp_query-from>4</Hsp_query-from>
              <Hsp_query-to>194</Hsp_query-to>
              <Hsp_hit-from>7</Hsp_hit-from>
              <Hsp_hit-to>197</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>112</Hsp_identity>
              <Hsp_positive>152</Hsp_positive>
              <Hsp_gaps>0</Hsp_gaps>
              <Hsp_align-len>191</Hsp_align-len>
              <Hsp_qseq>EKLKKSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTL</Hsp_qseq>
              <Hsp_hseq>EDLRKCKIIFIIGGPGSGKGTQCEKLVEKYGFTHLSTGELLREELASESERSKLIRDIMERGDLVPSGIVLELLKEAMVASLGDTRGFLIDGYPREVKQGEEFGRRIGDPQLVICMDCSADTMTNRLLQMSRSSLPVDDTTKTIAKRLEAYYRASIPVIAYYETKTQLHKINAEGTPEDVFLQLCTAIDSI</Hsp_hseq>
              <Hsp_midline>E L+K KIIF++GGPGSGKGTQCEK+V+KYG+THLSTG+LLR E++S S R K++ +IME+G LVP   VL++L++AMVA +  ++GFLIDGYPREVKQGEEF R+IG P L++ +D   +TMT RLL+   +S  VDD  +TI KRLE YY+A+ PVIA+YE +  + K+NAEG+ +DVF Q+CT +D++</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>4</Hit_num>
          <Hit_id>gi|157834086|pdb|1UKY|A</Hit_id>
          <Hit_def>Chain A, Substrate Specificity And Assembly Of Catalytic Center Derived From Two Structures Of Ligated Uridylate Kinase &gt;gi|157834087|pdb|1UKZ|A Chain A, Substrate Specificity And Assembly Of Catalytic Center Derived From Two Structures Of Ligated Uridylate Kinase</Hit_def>
          <Hit_accession>1UKY-A</Hit_accession>
          <Hit_len>203</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>174.096</Hsp_bit-score>
              <Hsp_score>440</Hsp_score>
              <Hsp_evalue>2.57378e-44</Hsp_evalue>
              <Hsp_query-from>11</Hsp_query-from>
              <Hsp_query-to>187</Hsp_query-to>
              <Hsp_hit-from>17</Hsp_hit-from>
              <Hsp_hit-to>195</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>85</Hsp_identity>
              <Hsp_positive>124</Hsp_positive>
              <Hsp_gaps>2</Hsp_gaps>
              <Hsp_align-len>179</Hsp_align-len>
              <Hsp_qseq>IIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVS-SGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKG-FLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQV</Hsp_qseq>
              <Hsp_hseq>VIFVLGGPGAGKGTQCEKLVKDYSFVHLSAGDLLRAEQGRAGSQYGELIKNCIKEGQIVPQEITLALLRNAISDNVKANKHKFLIDGFPRKMDQAISFERDIVESKFILFFDCPEDIMLERLLERGKTSGRSDDNIESIKKRFNTFKETSMPVIEYFETKSKVVRVRCDRSVEDVYKDV</Hsp_hseq>
              <Hsp_midline>+IFV+GGPG+GKGTQCEK+V+ Y + HLS GDLLRAE   +GS  G+++   +++GQ+VP E  L +LR+A+   V  +K  FLIDG+PR++ Q   FER I +   +L+ D   + M +RLL+RG+TSGR DDN E+IKKR  T+ + + PVI ++E +  V +V  + SV+DV+  V</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>5</Hit_num>
          <Hit_id>gi|50513735|pdb|1TEV|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of The Human UmpCMP KINASE IN OPEN Conformation</Hit_def>
          <Hit_accession>1TEV-A</Hit_accession>
          <Hit_len>196</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>159.073</Hsp_bit-score>
              <Hsp_score>401</Hsp_score>
              <Hsp_evalue>6.95488e-40</Hsp_evalue>
              <Hsp_query-from>8</Hsp_query-from>
              <Hsp_query-to>192</Hsp_query-to>
              <Hsp_hit-from>2</Hsp_hit-from>
              <Hsp_hit-to>193</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>78</Hsp_identity>
              <Hsp_positive>127</Hsp_positive>
              <Hsp_gaps>7</Hsp_gaps>
              <Hsp_align-len>192</Hsp_align-len>
              <Hsp_qseq>KSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSS-GSARGKMLSEIMEKGQLVPLETVLDMLR---DAMVAKVDTSKGFLIDGYPREVKQGEEFERKI---GQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLD</Hsp_qseq>
              <Hsp_hseq>KPLVVFVLGGPGAGKGTQCARIVEKYGYTHLSAGELLRDERKNPDSQYGELIEKYIKEGKIVPVEITISLLKREMDQTMAANAQKNKFLIDGFPRNQDNLQGWNKTMDGKADVSFVLFFDCNNEICIERCLERGKSSGRSDDNRESLEKRIQTYLQSTKPIIDLYEEMGKVKKIDASKSVDEVFDEVVQIFD</Hsp_hseq>
              <Hsp_midline>K  ++FV+GGPG+GKGTQC +IV+KYGYTHLS G+LLR E  +  S  G+++ + +++G++VP+E  + +L+   D  +A       FLIDG+PR     + + + +      + +L+ D   E   +R L+RG++SGR DDN E+++KR++TY ++T+P+I  YE+ G V+K++A  SVD+VF +V    D</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>6</Hit_num>
          <Hit_id>gi|5822263|pdb|1QF9|A</Hit_id>
          <Hit_def>Chain A, Ph Influences Fluoride Coordination Number Of The Alfx Phosphoryl Transfer Transition State Analog In UmpCMP Kinase &gt;gi|5822578|pdb|5UKD|A Chain A, Ph Influences Fluoride Coordination Number Of The Alfx Phosphoryl Transfer Transition State Analog &gt;gi|157834085|pdb|1UKE|A Chain A, UmpCMP KINASE FROM SLIME MOLD &gt;gi|157836400|pdb|2UKD|A Chain A, UmpCMP KINASE FROM SLIME MOLD COMPLEXED WITH ADP, CMP &gt;gi|157836902|pdb|3UKD|A Chain A, UmpCMP KINASE FROM SLIME MOLD COMPLEXED WITH ADP, CMP, AND Alf3 &gt;gi|157837007|pdb|4UKD|A Chain A, UmpCMP KINASE FROM SLIME MOLD COMPLEXED WITH ADP, UDP, Beryllium Fluoride</Hit_def>
          <Hit_accession>1QF9-A</Hit_accession>
          <Hit_len>194</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>152.525</Hsp_bit-score>
              <Hsp_score>384</Hsp_score>
              <Hsp_evalue>7.31616e-38</Hsp_evalue>
              <Hsp_query-from>8</Hsp_query-from>
              <Hsp_query-to>187</Hsp_query-to>
              <Hsp_hit-from>5</Hsp_hit-from>
              <Hsp_hit-to>185</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>79</Hsp_identity>
              <Hsp_positive>117</Hsp_positive>
              <Hsp_gaps>5</Hsp_gaps>
              <Hsp_align-len>183</Hsp_align-len>
              <Hsp_qseq>KSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFE---RKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQV</Hsp_qseq>
              <Hsp_hseq>KPNVVFVLGGPGSGKGTQCANIVRDFGWVHLSAGDLLRQEQQSGSKDGEMIATMIKNGEIVPSIVTVKLLKNAIDA--NQGKNFLVDGFPRNEENNNSWEENMKDFVDTKFVLFFDCPEEVMTQRLLKRGESSGRSDDNIESIKKRFNTFNVQTKLVIDHYNKFDKVKIIPANRDVNEVYNDV</Hsp_hseq>
              <Hsp_midline>K  ++FV+GGPGSGKGTQC  IV+ +G+ HLS GDLLR E  SGS  G+M++ +++ G++VP    + +L++A+ A  +  K FL+DG+PR  +    +E   +       +L+ D   E MT+RLLKRGE+SGR DDN E+IKKR  T+   T+ VI  Y K   V+ + A   V++V++ V</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>7</Hit_num>
          <Hit_id>gi|206581907|pdb|3CM0|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of Adenylate Kinase From Thermus Thermophilus Hb8</Hit_def>
          <Hit_accession>3CM0-A</Hit_accession>
          <Hit_len>186</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>139.043</Hsp_bit-score>
              <Hsp_score>349</Hsp_score>
              <Hsp_evalue>8.72737e-34</Hsp_evalue>
              <Hsp_query-from>10</Hsp_query-from>
              <Hsp_query-to>191</Hsp_query-to>
              <Hsp_hit-from>5</Hsp_hit-from>
              <Hsp_hit-to>184</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>70</Hsp_identity>
              <Hsp_positive>121</Hsp_positive>
              <Hsp_gaps>10</Hsp_gaps>
              <Hsp_align-len>186</Hsp_align-len>
              <Hsp_qseq>KIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQP-TLLL---YVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHL</Hsp_qseq>
              <Hsp_hseq>QAVIFLGPPGAGKGTQASRLAQELGFKKLSTGDILRDHVARGTPLGERVRPIMERGDLVPDDLILELIREELAERV------IFDGFPRTLAQAEALDRLLSETGTRLLGVVLVEVPEEELVRRILRRAELEGRSDDNEETVRRRLEVYREKTEPLVGYYEARGVLKRVDGLGTPDEVYARIRAAL</Hsp_hseq>
              <Hsp_midline>+ +  +G PG+GKGTQ  ++ Q+ G+  LSTGD+LR  V+ G+  G+ +  IME+G LVP + +L+++R+ +  +V      + DG+PR + Q E  +R + +  T LL    V+   E + +R+L+R E  GR DDNEET+++RLE Y + TEP++ +YE RG++++V+  G+ D+V++++   L</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>8</Hit_num>
          <Hit_id>gi|3114421|pdb|1ZAK|A</Hit_id>
          <Hit_def>Chain A, Adenylate Kinase From Maize In Complex With The Inhibitor P1,P5-Bis(Adenosine-5'-)pentaphosphate (Ap5a) &gt;gi|3114422|pdb|1ZAK|B Chain B, Adenylate Kinase From Maize In Complex With The Inhibitor P1,P5-Bis(Adenosine-5'-)pentaphosphate (Ap5a)</Hit_def>
          <Hit_accession>1ZAK-A</Hit_accession>
          <Hit_len>222</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>135.576</Hsp_bit-score>
              <Hsp_score>340</Hsp_score>
              <Hsp_evalue>9.64924e-33</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>187</Hsp_query-to>
              <Hsp_hit-from>8</Hsp_hit-from>
              <Hsp_hit-to>204</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>74</Hsp_identity>
              <Hsp_positive>107</Hsp_positive>
              <Hsp_gaps>25</Hsp_gaps>
              <Hsp_align-len>199</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKR-----------------------GETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQV</Hsp_qseq>
              <Hsp_hseq>VMISGAPASGKGTQCELIKTKYQLAHISAGDLLRAEIAAGSENGKRAKEFMEKGQLVPDEIVVNMVKERLRQPDAQENGWLLDGYPRSYSQAMALETLEIRPDTFILLDVPDELLVERVVGRRLDPVTGKIYHLKYSPPENEEIASRLTQRFDDTEEKVKLRLETYYQNIESLLSTYE--NIIVKVQGDATVDAVFAKI</Hsp_hseq>
              <Hsp_midline>+ + G P SGKGTQCE I  KY   H+S GDLLRAE+++GS  GK   E MEKGQLVP E V++M+++ +        G+L+DGYPR   Q    E    +P   + +D   E + +R++ R                          + R DD EE +K RLETYY+  E +++ YE   I+ KV  + +VD VF+++</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>9</Hit_num>
          <Hit_id>gi|157834399|pdb|1ZIN|A</Hit_id>
          <Hit_def>Chain A, Adenylate Kinase With Bound Ap5a &gt;gi|157834400|pdb|1ZIO|A Chain A, Phosphotransferase &gt;gi|157834401|pdb|1ZIP|A Chain A, Bacillus Stearothermophilus Adenylate Kinase</Hit_def>
          <Hit_accession>1ZIN-A</Hit_accession>
          <Hit_len>217</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>126.331</Hsp_bit-score>
              <Hsp_score>316</Hsp_score>
              <Hsp_evalue>6.10338e-30</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>187</Hsp_query-to>
              <Hsp_hit-from>3</Hsp_hit-from>
              <Hsp_hit-to>208</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>69</Hsp_identity>
              <Hsp_positive>109</Hsp_positive>
              <Hsp_gaps>32</Hsp_gaps>
              <Hsp_align-len>207</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIG----QPTLLLYVDAGPETMTKRLLKR---------------------------GETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQV</Hsp_qseq>
              <Hsp_hseq>LVLMGLPGAGKGTQAEKIVAAYGIPHISTGDMFRAAMKEGTPLGLQAKQYMDRGDLVPDEVTIGIVRERL-SKDDCQNGFLLDGFPRTVAQAEALETMLADIGRKLDYVIHIDVRQDVLMERLTGRRICRNCGATYHLIFHPPAKPGVCDKCGGELYQRADDNEATVANRLEVNMKQMKPLVDFYEQKGYLRNINGEQDMEKVFADI</Hsp_hseq>
              <Hsp_midline>+ ++G PG+GKGTQ EKIV  YG  H+STGD+ RA +  G+  G    + M++G LVP E  + ++R+ + +K D   GFL+DG+PR V Q E  E  +     +   ++++D   + + +RL  R                           GE   R DDNE T+  RLE   K  +P++ FYE++G +R +N E  ++ VF+ +</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>10</Hit_num>
          <Hit_id>gi|48425544|pdb|1S3G|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of Adenylate Kinase From Bacillus Globisporus</Hit_def>
          <Hit_accession>1S3G-A</Hit_accession>
          <Hit_len>217</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>118.242</Hsp_bit-score>
              <Hsp_score>295</Hsp_score>
              <Hsp_evalue>1.33796e-27</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>184</Hsp_query-to>
              <Hsp_hit-from>3</Hsp_hit-from>
              <Hsp_hit-to>205</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>69</Hsp_identity>
              <Hsp_positive>104</Hsp_positive>
              <Hsp_gaps>38</Hsp_gaps>
              <Hsp_align-len>207</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFE-------RKIGQPTLLLYVDAGPETMTKRLLKR---------------------------GETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVF</Hsp_qseq>
              <Hsp_hseq>IVLMGLPGAGKGTQADRIVEKYGTPHISTGDMFRAAIQEGTELGVKAKSFMDQGALVPDEVTIGIVRERL-SKSDCDNGFLLDGFPRTVPQAEALDQLLADMGRKIEH---VLNIQVEKEELIARLTGRRICKVCGTSYHLLFNPPQVEGKCDKDGGELYQRADDNPDTVTNRLEVNMNQTAPLLAFYDSKEVLVNINGQKDIKDVF</Hsp_hseq>
              <Hsp_midline>I ++G PG+GKGTQ ++IV+KYG  H+STGD+ RA +  G+  G      M++G LVP E  + ++R+ + +K D   GFL+DG+PR V Q E  +       RKI     +L +    E +  RL  R                           GE   R DDN +T+  RLE     T P++AFY+ + ++  +N +  + DVF</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>11</Hit_num>
          <Hit_id>gi|186972751|pdb|2QAJ|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of A Thermostable Mutant Of Bacillus Subtilis Adenylate Kinase (Q199rG213E) &gt;gi|186972752|pdb|2QAJ|B Chain B, Crystal Structure Of A Thermostable Mutant Of Bacillus Subtilis Adenylate Kinase (Q199rG213E)</Hit_def>
          <Hit_accession>2QAJ-A</Hit_accession>
          <Hit_len>217</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>114.775</Hsp_bit-score>
              <Hsp_score>286</Hsp_score>
              <Hsp_evalue>1.76259e-26</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>195</Hsp_query-to>
              <Hsp_hit-from>3</Hsp_hit-from>
              <Hsp_hit-to>216</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>72</Hsp_identity>
              <Hsp_positive>115</Hsp_positive>
              <Hsp_gaps>32</Hsp_gaps>
              <Hsp_align-len>215</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQ---GEEFERKIGQPT-LLLYVDAGPETMTKRLLKR---------------------------GETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</Hsp_qseq>
              <Hsp_hseq>LVLMGLPGAGKGTQGERIVEDYGIPHISTGDMFRAAMKEETPLGLEAKSYIDKGELVPDEVTIGIVKERL-GKDDCERGFLLDGFPRTVAQAEALEEILEEYGKPIDYVINIEVDKDVLMERLTGRRICSVCGTTYHLVFNPPKTPGICDKDGGELYQRADDNEETVSKRLEVNMKQTQPLLDFYSEKGYLANVNGQRDIQDVYADVKDLLEGLK</Hsp_hseq>
              <Hsp_midline>+ ++G PG+GKGTQ E+IV+ YG  H+STGD+ RA +   +  G      ++KG+LVP E  + ++++ +  K D  +GFL+DG+PR V Q    EE   + G+P   ++ ++   + + +RL  R                           GE   R DDNEET+ KRLE   K T+P++ FY ++G +  VN +  + DV++ V   L+ LK</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>12</Hit_num>
          <Hit_id>gi|163310973|pdb|2RGX|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of Adenylate Kinase From Aquifex Aeolicus In Complex With Ap5a &gt;gi|163310974|pdb|2RH5|A Chain A, Structure Of Apo Adenylate Kinase From Aquifex Aeolicus &gt;gi|163310975|pdb|2RH5|C Chain C, Structure Of Apo Adenylate Kinase From Aquifex Aeolicus &gt;gi|163310976|pdb|2RH5|B Chain B, Structure Of Apo Adenylate Kinase From Aquifex Aeolicus</Hit_def>
          <Hit_accession>2RGX-A</Hit_accession>
          <Hit_len>206</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>114.39</Hsp_bit-score>
              <Hsp_score>285</Hsp_score>
              <Hsp_evalue>2.34075e-26</Hsp_evalue>
              <Hsp_query-from>11</Hsp_query-from>
              <Hsp_query-to>188</Hsp_query-to>
              <Hsp_hit-from>2</Hsp_hit-from>
              <Hsp_hit-to>199</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>74</Hsp_identity>
              <Hsp_positive>111</Hsp_positive>
              <Hsp_gaps>30</Hsp_gaps>
              <Hsp_align-len>203</Hsp_align-len>
              <Hsp_qseq>IIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLI-DGYPREVKQGEEFERKIGQPTL----LLYVDAGPETMTKRLLKR------GETSG--------------RVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVC</Hsp_qseq>
              <Hsp_hseq>ILVFLGPPGAGKGTQAKRLAKEKGFVHISTGDILREAVQKGTPLGKKAKEYMERGELVP-----DDLIIALIEEVFPKHGNVIFDGFPRTVKQAEALDEMLEKKGLKVDHVLLFEVPDEVVIERLSGRRINPETGEVYHVKYNPPPPGVKVIQREDDKPEVIKKRLEVYREQTAPLIEYYKKKGILRIIDASKPVEEVYRQVL</Hsp_hseq>
              <Hsp_midline>I+  +G PG+GKGTQ +++ ++ G+ H+STGD+LR  V  G+  GK   E ME+G+LVP     D L  A++ +V    G +I DG+PR VKQ E  +  + +  L    +L  +   E + +RL  R      GE                 R DD  E IKKRLE Y + T P+I +Y+K+GI+R ++A   V++V+ QV </Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>13</Hit_num>
          <Hit_id>gi|48425192|pdb|1P3J|A</Hit_id>
          <Hit_def>Chain A, Adenylate Kinase From Bacillus Subtilis</Hit_def>
          <Hit_accession>1P3J-A</Hit_accession>
          <Hit_len>217</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>113.62</Hsp_bit-score>
              <Hsp_score>283</Hsp_score>
              <Hsp_evalue>3.64257e-26</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>195</Hsp_query-to>
              <Hsp_hit-from>3</Hsp_hit-from>
              <Hsp_hit-to>216</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>72</Hsp_identity>
              <Hsp_positive>114</Hsp_positive>
              <Hsp_gaps>32</Hsp_gaps>
              <Hsp_align-len>215</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQ---GEEFERKIGQPT-LLLYVDAGPETMTKRLLKR---------------------------GETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</Hsp_qseq>
              <Hsp_hseq>LVLMGLPGAGKGTQGERIVEDYGIPHISTGDMFRAAMKEETPLGLEAKSYIDKGELVPDEVTIGIVKERL-GKDDCERGFLLDGFPRTVAQAEALEEILEEYGKPIDYVINIEVDKDVLMERLTGRRICSVCGTTYHLVFNPPKTPGICDKDGGELYQRADDNEETVSKRLEVNMKQTQPLLDFYSEKGYLANVNGQQDIQDVYADVKDLLGGLK</Hsp_hseq>
              <Hsp_midline>+ ++G PG+GKGTQ E+IV+ YG  H+STGD+ RA +   +  G      ++KG+LVP E  + ++++ +  K D  +GFL+DG+PR V Q    EE   + G+P   ++ ++   + + +RL  R                           GE   R DDNEET+ KRLE   K T+P++ FY ++G +  VN +  + DV++ V   L  LK</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>14</Hit_num>
          <Hit_id>gi|166007058|pdb|2P3S|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of A Thermostable Mutant Of Bacillus Subtilis Adenylate Kinase (G214rQ199R)</Hit_def>
          <Hit_accession>2P3S-A</Hit_accession>
          <Hit_len>217</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>113.62</Hsp_bit-score>
              <Hsp_score>283</Hsp_score>
              <Hsp_evalue>3.67309e-26</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>195</Hsp_query-to>
              <Hsp_hit-from>3</Hsp_hit-from>
              <Hsp_hit-to>216</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>72</Hsp_identity>
              <Hsp_positive>114</Hsp_positive>
              <Hsp_gaps>32</Hsp_gaps>
              <Hsp_align-len>215</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQ---GEEFERKIGQPT-LLLYVDAGPETMTKRLLKR---------------------------GETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</Hsp_qseq>
              <Hsp_hseq>LVLMGLPGAGKGTQGERIVEDYGIPHISTGDMFRAAMKEETPLGLEAKSYIDKGELVPDEVTIGIVKERL-GKDDCERGFLLDGFPRTVAQAEALEEILEEYGKPIDYVINIEVDKDVLMERLTGRRICSVCGTTYHLVFNPPKTPGICDKDGGELYQRADDNEETVSKRLEVNMKQTQPLLDFYSEKGYLANVNGQRDIQDVYADVKDLLGRLK</Hsp_hseq>
              <Hsp_midline>+ ++G PG+GKGTQ E+IV+ YG  H+STGD+ RA +   +  G      ++KG+LVP E  + ++++ +  K D  +GFL+DG+PR V Q    EE   + G+P   ++ ++   + + +RL  R                           GE   R DDNEET+ KRLE   K T+P++ FY ++G +  VN +  + DV++ V   L  LK</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>15</Hit_num>
          <Hit_id>gi|110591304|pdb|2EU8|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of A Thermostable Mutant Of Bacillus Subtilis Adenylate Kinase (Q199r) &gt;gi|110591305|pdb|2EU8|B Chain B, Crystal Structure Of A Thermostable Mutant Of Bacillus Subtilis Adenylate Kinase (Q199r)</Hit_def>
          <Hit_accession>2EU8-A</Hit_accession>
          <Hit_len>216</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>113.62</Hsp_bit-score>
              <Hsp_score>283</Hsp_score>
              <Hsp_evalue>4.02617e-26</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>195</Hsp_query-to>
              <Hsp_hit-from>3</Hsp_hit-from>
              <Hsp_hit-to>216</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>72</Hsp_identity>
              <Hsp_positive>114</Hsp_positive>
              <Hsp_gaps>32</Hsp_gaps>
              <Hsp_align-len>215</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQ---GEEFERKIGQPT-LLLYVDAGPETMTKRLLKR---------------------------GETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</Hsp_qseq>
              <Hsp_hseq>LVLMGLPGAGKGTQGERIVEDYGIPHISTGDMFRAAMKEETPLGLEAKSYIDKGELVPDEVTIGIVKERL-GKDDCERGFLLDGFPRTVAQAEALEEILEEYGKPIDYVINIEVDKDVLMERLTGRRICSVCGTTYHLVFNPPKTPGICDKDGGELYQRADDNEETVSKRLEVNMKQTQPLLDFYSEKGYLANVNGQRDIQDVYADVKDLLGGLK</Hsp_hseq>
              <Hsp_midline>+ ++G PG+GKGTQ E+IV+ YG  H+STGD+ RA +   +  G      ++KG+LVP E  + ++++ +  K D  +GFL+DG+PR V Q    EE   + G+P   ++ ++   + + +RL  R                           GE   R DDNEET+ KRLE   K T+P++ FY ++G +  VN +  + DV++ V   L  LK</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>16</Hit_num>
          <Hit_id>gi|165760925|pdb|2ORI|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of A Thermostable Mutant Of Bacillus Subtilis Adenylate Kinase (A193vQ199R) &gt;gi|165760926|pdb|2ORI|B Chain B, Crystal Structure Of A Thermostable Mutant Of Bacillus Subtilis Adenylate Kinase (A193vQ199R)</Hit_def>
          <Hit_accession>2ORI-A</Hit_accession>
          <Hit_len>216</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>112.849</Hsp_bit-score>
              <Hsp_score>281</Hsp_score>
              <Hsp_evalue>6.37077e-26</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>195</Hsp_query-to>
              <Hsp_hit-from>3</Hsp_hit-from>
              <Hsp_hit-to>216</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>72</Hsp_identity>
              <Hsp_positive>114</Hsp_positive>
              <Hsp_gaps>32</Hsp_gaps>
              <Hsp_align-len>215</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQ---GEEFERKIGQPT-LLLYVDAGPETMTKRLLKR---------------------------GETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</Hsp_qseq>
              <Hsp_hseq>LVLMGLPGAGKGTQGERIVEDYGIPHISTGDMFRAAMKEETPLGLEAKSYIDKGELVPDEVTIGIVKERL-GKDDCERGFLLDGFPRTVAQAEALEEILEEYGKPIDYVINIEVDKDVLMERLTGRRICSVCGTTYHLVFNPPKTPGICDKDGGELYQRADDNEETVSKRLEVNMKQTQPLLDFYSEKGYLVNVNGQRDIQDVYADVKDLLGGLK</Hsp_hseq>
              <Hsp_midline>+ ++G PG+GKGTQ E+IV+ YG  H+STGD+ RA +   +  G      ++KG+LVP E  + ++++ +  K D  +GFL+DG+PR V Q    EE   + G+P   ++ ++   + + +RL  R                           GE   R DDNEET+ KRLE   K T+P++ FY ++G +  VN +  + DV++ V   L  LK</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>17</Hit_num>
          <Hit_id>gi|163311052|pdb|3BE4|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of Cryptosporidium Parvum Adenylate Kinase Cgd5_3360</Hit_def>
          <Hit_accession>3BE4-A</Hit_accession>
          <Hit_len>217</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>111.694</Hsp_bit-score>
              <Hsp_score>278</Hsp_score>
              <Hsp_evalue>1.24189e-25</Hsp_evalue>
              <Hsp_query-from>7</Hsp_query-from>
              <Hsp_query-to>187</Hsp_query-to>
              <Hsp_hit-from>4</Hsp_hit-from>
              <Hsp_hit-to>213</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>72</Hsp_identity>
              <Hsp_positive>111</Hsp_positive>
              <Hsp_gaps>33</Hsp_gaps>
              <Hsp_align-len>212</Hsp_align-len>
              <Hsp_qseq>KKSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFER---KIGQP-TLLLYVDAGPETMTKRLLKR--GETSGRV-------------------------DDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQV</Hsp_qseq>
              <Hsp_hseq>KKHNLI-LIGAPGSGKGTQCEFIKKEYGLAHLSTGDMLREAIKNGTKIGLEAKSIIESGNFVGDEIVLGLVKEKFDLGV-CVNGFVLDGFPRTIPQAEGLAKILSEIGDSLTSVIYFEIDDSEIIERISGRCTHPASGRIYHVKYNPPKQPGIDDVTGEPLVWRDDDNAEAVKVRLDVFHKQTAPLVKFYEDLGILKRVNAKLPPKEVTEQI</Hsp_hseq>
              <Hsp_midline>KK  +I ++G PGSGKGTQCE I ++YG  HLSTGD+LR  + +G+  G     I+E G  V  E VL ++++     V    GF++DG+PR + Q E   +   +IG   T ++Y +     + +R+  R     SGR+                         DDN E +K RL+ ++K T P++ FYE  GI+++VNA+    +V  Q+</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>18</Hit_num>
          <Hit_id>gi|493809|pdb|1AKE|A</Hit_id>
          <Hit_def>Chain A, Structure Of The Complex Between Adenylate Kinase From Escherichia Coli And The Inhibitor Ap5a Refined At 1.9 Angstroms Resolution: A Model For A Catalytic Transition State &gt;gi|493810|pdb|1AKE|B Chain B, Structure Of The Complex Between Adenylate Kinase From Escherichia Coli And The Inhibitor Ap5a Refined At 1.9 Angstroms Resolution: A Model For A Catalytic Transition State &gt;gi|576003|pdb|1ANK|A Chain A, The Closed Conformation Of A Highly Flexible Protein: The Structure Of E. Cloi Adenylate Kinase With Bound Amp And Amppnp &gt;gi|576004|pdb|1ANK|B Chain B, The Closed Conformation Of A Highly Flexible Protein: The Structure Of E. Cloi Adenylate Kinase With Bound Amp And Amppnp &gt;gi|1633462|pdb|4AKE|A Chain A, Adenylate Kinase &gt;gi|1633463|pdb|4AKE|B Chain B, Adenylate Kinase &gt;gi|1942106|pdb|2ECK|A Chain A, Structure Of Phosphotransferase &gt;gi|1942107|pdb|2ECK|B Chain B, Structure Of Phosphotransferase</Hit_def>
          <Hit_accession>1AKE-A</Hit_accession>
          <Hit_len>214</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>111.694</Hsp_bit-score>
              <Hsp_score>278</Hsp_score>
              <Hsp_evalue>1.24189e-25</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>167</Hsp_query-to>
              <Hsp_hit-from>3</Hsp_hit-from>
              <Hsp_hit-to>184</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>66</Hsp_identity>
              <Hsp_positive>99</Hsp_positive>
              <Hsp_gaps>28</Hsp_gaps>
              <Hsp_align-len>183</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKR--GETSGRV-------------------------DDNEETIKKRLETYYKATEPVIAFYEK</Hsp_qseq>
              <Hsp_hseq>IILLGAPGAGKGTQAQFIMEKYGIPQISTGDMLRAAVKSGSELGKQAKDIMDAGKLVTDELVIALVKE-RIAQEDCRNGFLLDGFPRTIPQADAMKEAGINVDYVLEFDVPDELIVDRIVGRRVHAPSGRVYHVKFNPPKVEGKDDVTGEELTTRKDDQEETVRKRLVEYHQMTAPLIGYYSK</Hsp_hseq>
              <Hsp_midline>I ++G PG+GKGTQ + I++KYG   +STGD+LRA V SGS  GK   +IM+ G+LV  E V+ ++++  +A+ D   GFL+DG+PR + Q +  +        +L  D   E +  R++ R     SGRV                         DD EET++KRL  Y++ T P+I +Y K</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>19</Hit_num>
          <Hit_id>gi|167013243|pdb|2OO7|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of A Thermostable Mutant Of Bacillus Subtilis Adenylate Kinase (T179iQ199R) &gt;gi|167013244|pdb|2OO7|B Chain B, Crystal Structure Of A Thermostable Mutant Of Bacillus Subtilis Adenylate Kinase (T179iQ199R)</Hit_def>
          <Hit_accession>2OO7-A</Hit_accession>
          <Hit_len>217</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>111.694</Hsp_bit-score>
              <Hsp_score>278</Hsp_score>
              <Hsp_evalue>1.58186e-25</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>195</Hsp_query-to>
              <Hsp_hit-from>3</Hsp_hit-from>
              <Hsp_hit-to>216</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>71</Hsp_identity>
              <Hsp_positive>113</Hsp_positive>
              <Hsp_gaps>32</Hsp_gaps>
              <Hsp_align-len>215</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQ---GEEFERKIGQPT-LLLYVDAGPETMTKRLLKR---------------------------GETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</Hsp_qseq>
              <Hsp_hseq>LVLMGLPGAGKGTQGERIVEDYGIPHISTGDMFRAAMKEETPLGLEAKSYIDKGELVPDEVTIGIVKERL-GKDDCERGFLLDGFPRTVAQAEALEEILEEYGKPIDYVINIEVDKDVLMERLTGRRICSVCGTTYHLVFNPPKTPGICDKDGGELYQRADDNEETVSKRLEVNMKQIQPLLDFYSEKGYLANVNGQRDIQDVYADVKDLLGGLK</Hsp_hseq>
              <Hsp_midline>+ ++G PG+GKGTQ E+IV+ YG  H+STGD+ RA +   +  G      ++KG+LVP E  + ++++ +  K D  +GFL+DG+PR V Q    EE   + G+P   ++ ++   + + +RL  R                           GE   R DDNEET+ KRLE   K  +P++ FY ++G +  VN +  + DV++ V   L  LK</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>20</Hit_num>
          <Hit_id>gi|165760928|pdb|2OSB|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of A Thermostable Mutant Of Bacillus Subtilis Adenylate Kinase (Q16lQ199R) &gt;gi|165760929|pdb|2OSB|B Chain B, Crystal Structure Of A Thermostable Mutant Of Bacillus Subtilis Adenylate Kinase (Q16lQ199R)</Hit_def>
          <Hit_accession>2OSB-A</Hit_accession>
          <Hit_len>216</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>110.538</Hsp_bit-score>
              <Hsp_score>275</Hsp_score>
              <Hsp_evalue>3.58333e-25</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>195</Hsp_query-to>
              <Hsp_hit-from>3</Hsp_hit-from>
              <Hsp_hit-to>216</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>71</Hsp_identity>
              <Hsp_positive>113</Hsp_positive>
              <Hsp_gaps>32</Hsp_gaps>
              <Hsp_align-len>215</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQ---GEEFERKIGQPT-LLLYVDAGPETMTKRLLKR---------------------------GETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</Hsp_qseq>
              <Hsp_hseq>LVLMGLPGAGKGTLGERIVEDYGIPHISTGDMFRAAMKEETPLGLEAKSYIDKGELVPDEVTIGIVKERL-GKDDCERGFLLDGFPRTVAQAEALEEILEEYGKPIDYVINIEVDKDVLMERLTGRRICSVCGTTYHLVFNPPKTPGICDKDGGELYQRADDNEETVSKRLEVNMKQTQPLLDFYSEKGYLANVNGQRDIQDVYADVKDLLGGLK</Hsp_hseq>
              <Hsp_midline>+ ++G PG+GKGT  E+IV+ YG  H+STGD+ RA +   +  G      ++KG+LVP E  + ++++ +  K D  +GFL+DG+PR V Q    EE   + G+P   ++ ++   + + +RL  R                           GE   R DDNEET+ KRLE   K T+P++ FY ++G +  VN +  + DV++ V   L  LK</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>21</Hit_num>
          <Hit_id>gi|157829936|pdb|1AKY|A</Hit_id>
          <Hit_def>Chain A, High-Resolution Structures Of Adenylate Kinase From Yeast Ligated With Inhibitor Ap5a, Showing The Pathway Of Phosphoryl Transfer &gt;gi|157834546|pdb|2AKY|A Chain A, High-Resolution Structures Of Adenylate Kinase From Yeast Ligated With Inhibitor Ap5a, Showing The Pathway Of Phosphoryl Transfer</Hit_def>
          <Hit_accession>1AKY-A</Hit_accession>
          <Hit_len>220</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>109.383</Hsp_bit-score>
              <Hsp_score>272</Hsp_score>
              <Hsp_evalue>7.85071e-25</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>191</Hsp_query-to>
              <Hsp_hit-from>7</Hsp_hit-from>
              <Hsp_hit-to>217</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>63</Hsp_identity>
              <Hsp_positive>109</Hsp_positive>
              <Hsp_gaps>35</Hsp_gaps>
              <Hsp_align-len>213</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKI---GQP---TLLLYVDAGPETMTKRLLKR--GETSG-------------------------RVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHL</Hsp_qseq>
              <Hsp_hseq>MVLIGPPGAGKGTQAPNLQERFHAAHLATGDMLRSQIAKGTQLGLEAKKIMDQGGLVSDDIMVNMIKDELTNNPACKNGFILDGFPRTIPQAEKLDQMLKEQGTPLEKAIELKVDD--ELLVARITGRLIHPASGRSYHKIFNPPKEDMKDDVTGEALVQRSDDNADALKKRLAAYHAQTEPIVDFYKKTGIWAGVDASQPPATVWADILNKL</Hsp_hseq>
              <Hsp_midline>+ ++G PG+GKGTQ   + +++   HL+TGD+LR++++ G+  G    +IM++G LV  + +++M++D +        GF++DG+PR + Q E+ ++ +   G P    + L VD   E +  R+  R     SG                         R DDN + +KKRL  Y+  TEP++ FY+K GI   V+A      V++ +   L</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>22</Hit_num>
          <Hit_id>gi|10121071|pdb|1E4V|A</Hit_id>
          <Hit_def>Chain A, Mutant G10v Of Adenylate Kinase From E. Coli, Modified In The Gly-Loop &gt;gi|10121072|pdb|1E4V|B Chain B, Mutant G10v Of Adenylate Kinase From E. Coli, Modified In The Gly-Loop</Hit_def>
          <Hit_accession>1E4V-A</Hit_accession>
          <Hit_len>214</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>108.227</Hsp_bit-score>
              <Hsp_score>269</Hsp_score>
              <Hsp_evalue>1.39618e-24</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>167</Hsp_query-to>
              <Hsp_hit-from>3</Hsp_hit-from>
              <Hsp_hit-to>184</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>65</Hsp_identity>
              <Hsp_positive>98</Hsp_positive>
              <Hsp_gaps>28</Hsp_gaps>
              <Hsp_align-len>183</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKR--GETSGRV-------------------------DDNEETIKKRLETYYKATEPVIAFYEK</Hsp_qseq>
              <Hsp_hseq>IILLGAPVAGKGTQAQFIMEKYGIPQISTGDMLRAAVKSGSELGKQAKDIMDAGKLVTDELVIALVKE-RIAQEDCRNGFLLDGFPRTIPQADAMKEAGINVDYVLEFDVPDELIVDRIVGRRVHAPSGRVYHVKFNPPKVEGKDDVTGEELTTRKDDQEETVRKRLVEYHQMTAPLIGYYSK</Hsp_hseq>
              <Hsp_midline>I ++G P +GKGTQ + I++KYG   +STGD+LRA V SGS  GK   +IM+ G+LV  E V+ ++++  +A+ D   GFL+DG+PR + Q +  +        +L  D   E +  R++ R     SGRV                         DD EET++KRL  Y++ T P+I +Y K</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>23</Hit_num>
          <Hit_id>gi|157836762|pdb|3AKY|A</Hit_id>
          <Hit_def>Chain A, Stability, Activity And Structure Of Adenylate Kinase Mutants</Hit_def>
          <Hit_accession>3AKY-A</Hit_accession>
          <Hit_len>220</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>107.457</Hsp_bit-score>
              <Hsp_score>267</Hsp_score>
              <Hsp_evalue>2.52476e-24</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>191</Hsp_query-to>
              <Hsp_hit-from>7</Hsp_hit-from>
              <Hsp_hit-to>217</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>63</Hsp_identity>
              <Hsp_positive>108</Hsp_positive>
              <Hsp_gaps>35</Hsp_gaps>
              <Hsp_align-len>213</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKI---GQP---TLLLYVDAGPETMTKRLLKR--GETSG-------------------------RVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHL</Hsp_qseq>
              <Hsp_hseq>MVLIGPPGAGKGTQAPNLQERFHAAHLATGDMLRSQIAKGTQLGLEAKKIMDQGGLVSDDIMVNMIKDELTNNPACKNGFILDGFPRTIPQAEKLDQMLKEQGTPLEKAIELKVDD--ELLVARITGRLIHPASGRSYHKIFNPPKEDMKDDVTGEALVQRSDDNADALKKRLAAYHAQTEPIVDFYKKTGIWAGVDASQPPATVWADFLNKL</Hsp_hseq>
              <Hsp_midline>+ ++G PG+GKGTQ   + +++   HL+TGD+LR++++ G+  G    +IM++G LV  + +++M++D +        GF++DG+PR + Q E+ ++ +   G P    + L VD   E +  R+  R     SG                         R DDN + +KKRL  Y+  TEP++ FY+K GI   V+A      V++     L</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>24</Hit_num>
          <Hit_id>gi|88192646|pdb|2C9Y|A</Hit_id>
          <Hit_def>Chain A, Structure Of Human Adenylate Kinase 2</Hit_def>
          <Hit_accession>2C9Y-A</Hit_accession>
          <Hit_len>242</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>107.071</Hsp_bit-score>
              <Hsp_score>266</Hsp_score>
              <Hsp_evalue>3.43791e-24</Hsp_evalue>
              <Hsp_query-from>14</Hsp_query-from>
              <Hsp_query-to>187</Hsp_query-to>
              <Hsp_hit-from>21</Hsp_hit-from>
              <Hsp_hit-to>224</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>67</Hsp_identity>
              <Hsp_positive>112</Hsp_positive>
              <Hsp_gaps>36</Hsp_gaps>
              <Hsp_align-len>207</Hsp_align-len>
              <Hsp_qseq>VVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFE-----RKIGQPTLLLYVDAGPETM-----TKRLLK------------------RGETSG-----RVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQV</Hsp_qseq>
              <Hsp_hseq>LLGPPGAGKGTQAPRLAENFCVCHLATGDMLRAMVASGSELGKKLKATMDAGKLVSDEMVVELIEKNLETPL-CKNGFLLDGFPRTVRQAEMLDDLMEKRKEKLDSVIEF--SIPDSLLIRRITGRLIHPKSGRSYHEEFNPPKEPMKDDITGEPLIRRSDDNEKALKIRLQAYHTQTTPLIEYYRKRGIHSAIDASQTPDVVFASI</Hsp_hseq>
              <Hsp_midline>++G PG+GKGTQ  ++ + +   HL+TGD+LRA V+SGS  GK L   M+ G+LV  E V++++   +   +    GFL+DG+PR V+Q E  +     RK    +++ +  + P+++     T RL+                   + + +G     R DDNE+ +K RL+ Y+  T P+I +Y KRGI   ++A  + D VF+ +</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>25</Hit_num>
          <Hit_id>gi|157829916|pdb|1AK2|A</Hit_id>
          <Hit_def>Chain A, Adenylate Kinase Isoenzyme-2 &gt;gi|157834545|pdb|2AK2|A Chain A, Adenylate Kinase Isoenzyme-2</Hit_def>
          <Hit_accession>1AK2-A</Hit_accession>
          <Hit_len>233</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>107.071</Hsp_bit-score>
              <Hsp_score>266</Hsp_score>
              <Hsp_evalue>3.96183e-24</Hsp_evalue>
              <Hsp_query-from>14</Hsp_query-from>
              <Hsp_query-to>187</Hsp_query-to>
              <Hsp_hit-from>21</Hsp_hit-from>
              <Hsp_hit-to>224</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>68</Hsp_identity>
              <Hsp_positive>111</Hsp_positive>
              <Hsp_gaps>36</Hsp_gaps>
              <Hsp_align-len>207</Hsp_align-len>
              <Hsp_qseq>VVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFE-----RKIGQPTLLLYVDAGPETM-----TKRLLK------------------RGETSG-----RVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQV</Hsp_qseq>
              <Hsp_hseq>LLGPPGAGKGTQAPKLAKNFCVCHLATGDMLRAMVASGSELGKKLKATMDAGKLVSDEMVLELIEKNLETP-PCKNGFLLDGFPRTVRQAEMLDDLMEKRKEKLDSVIEF--SIPDSLLIRRITGRLIHPQSGRSYHEEFNPPKEPMKDDITGEPLIRRSDDNKKALKIRLEAYHTQTTPLVEYYSKRGIHSAIDASQTPDVVFASI</Hsp_hseq>
              <Hsp_midline>++G PG+GKGTQ  K+ + +   HL+TGD+LRA V+SGS  GK L   M+ G+LV  E VL+++   +        GFL+DG+PR V+Q E  +     RK    +++ +  + P+++     T RL+                   + + +G     R DDN++ +K RLE Y+  T P++ +Y KRGI   ++A  + D VF+ +</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>26</Hit_num>
          <Hit_id>gi|1421658|pdb|1DVR|A</Hit_id>
          <Hit_def>Chain A, Structure Of A Mutant Adenylate Kinase Ligated With An Atp- Analogue Showing Domain Closure Over Atp &gt;gi|1421659|pdb|1DVR|B Chain B, Structure Of A Mutant Adenylate Kinase Ligated With An Atp- Analogue Showing Domain Closure Over Atp</Hit_def>
          <Hit_accession>1DVR-A</Hit_accession>
          <Hit_len>220</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>105.145</Hsp_bit-score>
              <Hsp_score>261</Hsp_score>
              <Hsp_evalue>1.21189e-23</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>191</Hsp_query-to>
              <Hsp_hit-from>7</Hsp_hit-from>
              <Hsp_hit-to>217</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>62</Hsp_identity>
              <Hsp_positive>108</Hsp_positive>
              <Hsp_gaps>35</Hsp_gaps>
              <Hsp_align-len>213</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKI---GQP---TLLLYVDAGPETMTKRLLKR--GETSGR-------------------------VDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHL</Hsp_qseq>
              <Hsp_hseq>MVLIGPPGAGKGTQAPNLQERFHAAHLATGDMLRSQIAKGTQLGLEAKKIMDQGGLVSDDIMVNMIKDELTNNPACKNGFILVGFPRTIPQAEKLDQMLKEQGTPLEKAIELKVDD--ELLVARITGRLIHPASGRSYHKIFNPPKEDMKDDVTGEALVQISDDNADALKKRLAAYHAQTEPIVDFYKKTGIWAGVDASQPPATVWADILNKL</Hsp_hseq>
              <Hsp_midline>+ ++G PG+GKGTQ   + +++   HL+TGD+LR++++ G+  G    +IM++G LV  + +++M++D +        GF++ G+PR + Q E+ ++ +   G P    + L VD   E +  R+  R     SGR                          DDN + +KKRL  Y+  TEP++ FY+K GI   V+A      V++ +   L</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>27</Hit_num>
          <Hit_id>gi|10121073|pdb|1E4Y|A</Hit_id>
          <Hit_def>Chain A, Mutant P9l Of Adenylate Kinase From E. Coli, Modified In The Gly-Loop &gt;gi|10121074|pdb|1E4Y|B Chain B, Mutant P9l Of Adenylate Kinase From E. Coli, Modified In The Gly-Loop</Hit_def>
          <Hit_accession>1E4Y-A</Hit_accession>
          <Hit_len>214</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>103.99</Hsp_bit-score>
              <Hsp_score>258</Hsp_score>
              <Hsp_evalue>2.67738e-23</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>167</Hsp_query-to>
              <Hsp_hit-from>3</Hsp_hit-from>
              <Hsp_hit-to>184</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>64</Hsp_identity>
              <Hsp_positive>97</Hsp_positive>
              <Hsp_gaps>28</Hsp_gaps>
              <Hsp_align-len>183</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKR--GETSGRV-------------------------DDNEETIKKRLETYYKATEPVIAFYEK</Hsp_qseq>
              <Hsp_hseq>IILLGALVAGKGTQAQFIMEKYGIPQISTGDMLRAAVKSGSELGKQAKDIMDAGKLVTDELVIALVKE-RIAQEDCRNGFLLDGFPRTIPQADAMKEAGINVDYVLEFDVPDELIVDRIVGRRVHAPSGRVYHVKFNPPKVEGKDDVTGEELTTRKDDQEETVRKRLVEYHQMTAPLIGYYSK</Hsp_hseq>
              <Hsp_midline>I ++G   +GKGTQ + I++KYG   +STGD+LRA V SGS  GK   +IM+ G+LV  E V+ ++++  +A+ D   GFL+DG+PR + Q +  +        +L  D   E +  R++ R     SGRV                         DD EET++KRL  Y++ T P+I +Y K</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>28</Hit_num>
          <Hit_id>gi|42543246|pdb|1P4S|A</Hit_id>
          <Hit_def>Chain A, Solution Structure Of Mycobacterium Tuberculosis Adenylate Kinase</Hit_def>
          <Hit_accession>1P4S-A</Hit_accession>
          <Hit_len>181</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>100.908</Hsp_bit-score>
              <Hsp_score>250</Hsp_score>
              <Hsp_evalue>2.30468e-22</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>191</Hsp_query-to>
              <Hsp_hit-from>3</Hsp_hit-from>
              <Hsp_hit-to>179</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>59</Hsp_identity>
              <Hsp_positive>99</Hsp_positive>
              <Hsp_gaps>11</Hsp_gaps>
              <Hsp_align-len>184</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQG----EEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHL</Hsp_qseq>
              <Hsp_hseq>VLLLGPPGAGKGTQAVKLAEKLGIPQISTGELFRRNIEEGTKLGVEAKRYLDAGDLVPSDLTNELVDDRL-NNPDAANGFILDGYPRSVEQAKALHEMLERRGTDIDAVLEFRVSEEVLLERLKGR----GRADDTDDVILNRMKVYRDETAPLLEYY--RDQLKTVDAVGTMDEVFARALRAL</Hsp_hseq>
              <Hsp_midline>+ ++G PG+GKGTQ  K+ +K G   +STG+L R  +  G+  G      ++ G LVP +   +++ D +    D + GF++DGYPR V+Q     E  ER+      +L      E + +RL  R    GR DD ++ I  R++ Y   T P++ +Y  R  ++ V+A G++D+VF++    L</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>29</Hit_num>
          <Hit_id>gi|109157291|pdb|2CDN|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of Mycobacterium Tuberculosis Adenylate Kinase Complexed With Two Molecules Of Adp And Mg</Hit_def>
          <Hit_accession>2CDN-A</Hit_accession>
          <Hit_len>201</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>100.523</Hsp_bit-score>
              <Hsp_score>249</Hsp_score>
              <Hsp_evalue>3.32699e-22</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>191</Hsp_query-to>
              <Hsp_hit-from>23</Hsp_hit-from>
              <Hsp_hit-to>199</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>59</Hsp_identity>
              <Hsp_positive>99</Hsp_positive>
              <Hsp_gaps>11</Hsp_gaps>
              <Hsp_align-len>184</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQG----EEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHL</Hsp_qseq>
              <Hsp_hseq>VLLLGPPGAGKGTQAVKLAEKLGIPQISTGELFRRNIEEGTKLGVEAKRYLDAGDLVPSDLTNELVDDRL-NNPDAANGFILDGYPRSVEQAKALHEMLERRGTDIDAVLEFRVSEEVLLERLKGR----GRADDTDDVILNRMKVYRDETAPLLEYY--RDQLKTVDAVGTMDEVFARALRAL</Hsp_hseq>
              <Hsp_midline>+ ++G PG+GKGTQ  K+ +K G   +STG+L R  +  G+  G      ++ G LVP +   +++ D +    D + GF++DGYPR V+Q     E  ER+      +L      E + +RL  R    GR DD ++ I  R++ Y   T P++ +Y  R  ++ V+A G++D+VF++    L</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>30</Hit_num>
          <Hit_id>gi|83754030|pdb|2AR7|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of Human Adenylate Kinase 4, Ak4 &gt;gi|83754031|pdb|2AR7|B Chain B, Crystal Structure Of Human Adenylate Kinase 4, Ak4 &gt;gi|83754359|pdb|2BBW|A Chain A, Crystal Structure Of Human Adenylate Kinase 4 (Ak4) In Complex With Diguanosine Pentaphosphate &gt;gi|83754360|pdb|2BBW|B Chain B, Crystal Structure Of Human Adenylate Kinase 4 (Ak4) In Complex With Diguanosine Pentaphosphate</Hit_def>
          <Hit_accession>2AR7-A</Hit_accession>
          <Hit_len>246</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>95.9005</Hsp_bit-score>
              <Hsp_score>237</Hsp_score>
              <Hsp_evalue>7.79452e-21</Hsp_evalue>
              <Hsp_query-from>2</Hsp_query-from>
              <Hsp_query-to>175</Hsp_query-to>
              <Hsp_hit-from>23</Hsp_hit-from>
              <Hsp_hit-to>217</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>62</Hsp_identity>
              <Hsp_positive>102</Hsp_positive>
              <Hsp_gaps>37</Hsp_gaps>
              <Hsp_align-len>203</Hsp_align-len>
              <Hsp_qseq>MEEKLKKSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKG--FLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRG--ETSGRV-------------------------DDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVN</Hsp_qseq>
              <Hsp_hseq>MASKLLRAVIL---GPPGSGKGTVCQRIAQNFGLQHLSSGHFLRENIKASTEVGEMAKQYIEKSLLVPDHVITRL----MMSELENRRGQHWLLDGFPRTLGQAEALD-KICEVDLVISLNIPFETLKDRLSRRWIHPPSGRVYNLDFNPPHVHGIDDVTGEPLVQQEDDKPEAVAARLRQYKDVAKPVIELYKSRGVLHQFS</Hsp_hseq>
              <Hsp_midline>M  KL ++ I+   G PGSGKGT C++I Q +G  HLS+G  LR  + + +  G+M  + +EK  LVP   +  +    M+++++  +G  +L+DG+PR + Q E  + KI +  L++ ++   ET+  RL +R     SGRV                         DD  E +  RL  Y    +PVI  Y+ RG++ + +</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>31</Hit_num>
          <Hit_id>gi|1065285|pdb|2AK3|A</Hit_id>
          <Hit_def>Chain A, The Three-Dimensional Structure Of The Complex Between Mitochondrial Matrix Adenylate Kinase And Its Substrate Amp At 1.85 Angstroms Resolution &gt;gi|1065286|pdb|2AK3|B Chain B, The Three-Dimensional Structure Of The Complex Between Mitochondrial Matrix Adenylate Kinase And Its Substrate Amp At 1.85 Angstroms Resolution</Hit_def>
          <Hit_accession>2AK3-A</Hit_accession>
          <Hit_len>226</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>88.1965</Hsp_bit-score>
              <Hsp_score>217</Hsp_score>
              <Hsp_evalue>1.62526e-18</Hsp_evalue>
              <Hsp_query-from>14</Hsp_query-from>
              <Hsp_query-to>193</Hsp_query-to>
              <Hsp_hit-from>11</Hsp_hit-from>
              <Hsp_hit-to>213</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>59</Hsp_identity>
              <Hsp_positive>100</Hsp_positive>
              <Hsp_gaps>31</Hsp_gaps>
              <Hsp_align-len>207</Hsp_align-len>
              <Hsp_qseq>VVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRG--ETSGRV-------------------------DDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDT</Hsp_qseq>
              <Hsp_hseq>IMGAPGSGKGTVSSRITKHFELKHLSSGDLLRDNMLRGTEIGVLAKTFIDQGKLIPDDVMTRLVLHEL--KNLTQYNWLLDGFPRTLPQAEALDRAY-QIDTVINLNVPFEVIKQRLTARWIHPGSGRVYNIEFNPPKTMGIDDLTGEPLVQREDDRPETVVKRLKAYEAQTEPVLEYYRKKGVLETFSGT-ETNKIWPHVYAFLQT</Hsp_hseq>
              <Hsp_midline>++G PGSGKGT   +I + +   HLS+GDLLR  +  G+  G +    +++G+L+P + +  ++   +  K  T   +L+DG+PR + Q E  +R   Q   ++ ++   E + +RL  R     SGRV                         DD  ET+ KRL+ Y   TEPV+ +Y K+G++   +     + ++  V   L T</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>32</Hit_num>
          <Hit_id>gi|67464380|pdb|1ZD8|A</Hit_id>
          <Hit_def>Chain A, Structure Of Human Adenylate Kinase 3 Like 1</Hit_def>
          <Hit_accession>1ZD8-A</Hit_accession>
          <Hit_len>227</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>85.8853</Hsp_bit-score>
              <Hsp_score>211</Hsp_score>
              <Hsp_evalue>7.29756e-18</Hsp_evalue>
              <Hsp_query-from>14</Hsp_query-from>
              <Hsp_query-to>193</Hsp_query-to>
              <Hsp_hit-from>12</Hsp_hit-from>
              <Hsp_hit-to>214</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>61</Hsp_identity>
              <Hsp_positive>100</Hsp_positive>
              <Hsp_gaps>33</Hsp_gaps>
              <Hsp_align-len>208</Hsp_align-len>
              <Hsp_qseq>VVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMV-AKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRG--ETSGRV-------------------------DDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDT</Hsp_qseq>
              <Hsp_hseq>IMGAPGSGKGTVSSRITTHFELKHLSSGDLLRDNMLRGTEIGVLAKAFIDQGKLIPDDV---MTRLALHELKNLTQYSWLLDGFPRTLPQAEALDRAY-QIDTVINLNVPFEVIKQRLTARWIHPASGRVYNIEFNPPKTVGIDDLTGEPLIQREDDKPETVIKRLKAYEDQTKPVLEYYQKKGVLETFSGT-ETNKIWPYVYAFLQT</Hsp_hseq>
              <Hsp_midline>++G PGSGKGT   +I   +   HLS+GDLLR  +  G+  G +    +++G+L+P +    M R A+   K  T   +L+DG+PR + Q E  +R   Q   ++ ++   E + +RL  R     SGRV                         DD  ET+ KRL+ Y   T+PV+ +Y+K+G++   +     + ++  V   L T</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>33</Hit_num>
          <Hit_id>gi|51247251|pdb|1Q3T|A</Hit_id>
          <Hit_def>Chain A, Solution Structure And Function Of An Essential Cmp Kinase Of Streptococcus Pneumoniae</Hit_def>
          <Hit_accession>1Q3T-A</Hit_accession>
          <Hit_len>236</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>31.9574</Hsp_bit-score>
              <Hsp_score>71</Hsp_score>
              <Hsp_evalue>0.128226</Hsp_evalue>
              <Hsp_query-from>7</Hsp_query-from>
              <Hsp_query-to>46</Hsp_query-to>
              <Hsp_hit-from>13</Hsp_hit-from>
              <Hsp_hit-to>53</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>16</Hsp_identity>
              <Hsp_positive>23</Hsp_positive>
              <Hsp_gaps>1</Hsp_gaps>
              <Hsp_align-len>41</Hsp_align-len>
              <Hsp_qseq>KKSKIIFVVGGPGS-GKGTQCEKIVQKYGYTHLSTGDLLRA</Hsp_qseq>
              <Hsp_hseq>KMKTIQIAIDGPASSGKSTVAKIIAKDFGFTYLDTGAMYRA</Hsp_hseq>
              <Hsp_midline>K   I   + GP S GK T  + I + +G+T+L TG + RA</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>34</Hit_num>
          <Hit_id>gi|61679459|pdb|1Y63|A</Hit_id>
          <Hit_def>Chain A, Initial Crystal Structural Analysis Of A Probable Kinase From Leishmania Major Friedlin</Hit_def>
          <Hit_accession>1Y63-A</Hit_accession>
          <Hit_len>184</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>31.5722</Hsp_bit-score>
              <Hsp_score>70</Hsp_score>
              <Hsp_evalue>0.209784</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>145</Hsp_query-to>
              <Hsp_hit-from>13</Hsp_hit-from>
              <Hsp_hit-to>133</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>30</Hsp_identity>
              <Hsp_positive>60</Hsp_positive>
              <Hsp_gaps>15</Hsp_gaps>
              <Hsp_align-len>135</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQKY-GYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEE</Hsp_qseq>
              <Hsp_hseq>ILITGTPGTGKTSMAEMIAAELDGFQHLEVGKLVKENHFYTEYDTELDTHIIEEKD---EDRLLDFMEPIMVSR----GNHVVDYHSSEL-----FPERWFHMVVVLH--TSTEVLFERLTKRQYSEAKRAENME</Hsp_hseq>
              <Hsp_midline>I + G PG+GK +  E I  +  G+ HL  G L++          ++ + I+E+      + +LD +   MV++       ++D +  E+     F  +     ++L+     E + +RL KR  +  +  +N E</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>35</Hit_num>
          <Hit_id>gi|88192972|pdb|2FEM|A</Hit_id>
          <Hit_def>Chain A, Mutant R188m Of The Cytidine Monophosphate Kinase From E. Coli &gt;gi|88192973|pdb|2FEO|A Chain A, Mutant R188m Of The Cytidine Monophosphate Kinase From E. Coli Complexed With Dcmp</Hit_def>
          <Hit_accession>2FEM-A</Hit_accession>
          <Hit_len>227</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>31.187</Hsp_bit-score>
              <Hsp_score>69</Hsp_score>
              <Hsp_evalue>0.271709</Hsp_evalue>
              <Hsp_query-from>9</Hsp_query-from>
              <Hsp_query-to>76</Hsp_query-to>
              <Hsp_hit-from>5</Hsp_hit-from>
              <Hsp_hit-to>69</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>19</Hsp_identity>
              <Hsp_positive>37</Hsp_positive>
              <Hsp_gaps>3</Hsp_gaps>
              <Hsp_align-len>68</Hsp_align-len>
              <Hsp_qseq>SKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDM</Hsp_qseq>
              <Hsp_hseq>APVITIDGPSGAGKGTLCKAMAEALQWHLLDSGAIYR--VLALAALHHHV-DVASEDALVPLASHLDV</Hsp_hseq>
              <Hsp_midline>+ +I + G  G+GKGT C+ + +   +  L +G + R  V + +A    + ++  +  LVPL + LD+</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>36</Hit_num>
          <Hit_id>gi|6137471|pdb|2CMK|A</Hit_id>
          <Hit_def>Chain A, Cytidine Monophosphate Kinase In Complex With Cytidine-Di- Phosphate &gt;gi|18655777|pdb|1KDO|A Chain A, Cytidine Monophosphate Kinase From E. Coli In Complex With Cytidine Monophosphate &gt;gi|18655778|pdb|1KDO|B Chain B, Cytidine Monophosphate Kinase From E. Coli In Complex With Cytidine Monophosphate &gt;gi|18655779|pdb|1KDP|A Chain A, Cytidine Monophosphate Kinase From E. Coli In Complex With 2'-Deoxy-Cytidine Monophosphate &gt;gi|18655780|pdb|1KDP|B Chain B, Cytidine Monophosphate Kinase From E. Coli In Complex With 2'-Deoxy-Cytidine Monophosphate &gt;gi|18655781|pdb|1KDR|A Chain A, Cytidine Monophosphate Kinase From E.Coli In Complex With Ara-Cytidine Monophosphate &gt;gi|18655782|pdb|1KDR|B Chain B, Cytidine Monophosphate Kinase From E.Coli In Complex With Ara-Cytidine Monophosphate &gt;gi|18655783|pdb|1KDT|A Chain A, Cytidine Monophosphate Kinase From E.Coli In Complex With 2',3'-Dideoxy-Cytidine Monophosphate &gt;gi|18655784|pdb|1KDT|B Chain B, Cytidine Monophosphate Kinase From E.Coli In Complex With 2',3'-Dideoxy-Cytidine Monophosphate</Hit_def>
          <Hit_accession>2CMK-A</Hit_accession>
          <Hit_len>227</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>31.187</Hsp_bit-score>
              <Hsp_score>69</Hsp_score>
              <Hsp_evalue>0.271709</Hsp_evalue>
              <Hsp_query-from>9</Hsp_query-from>
              <Hsp_query-to>76</Hsp_query-to>
              <Hsp_hit-from>5</Hsp_hit-from>
              <Hsp_hit-to>69</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>19</Hsp_identity>
              <Hsp_positive>37</Hsp_positive>
              <Hsp_gaps>3</Hsp_gaps>
              <Hsp_align-len>68</Hsp_align-len>
              <Hsp_qseq>SKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDM</Hsp_qseq>
              <Hsp_hseq>APVITIDGPSGAGKGTLCKAMAEALQWHLLDSGAIYR--VLALAALHHHV-DVASEDALVPLASHLDV</Hsp_hseq>
              <Hsp_midline>+ +I + G  G+GKGT C+ + +   +  L +G + R  V + +A    + ++  +  LVPL + LD+</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>37</Hit_num>
          <Hit_id>gi|6137462|pdb|1CKE|A</Hit_id>
          <Hit_def>Chain A, Cmp Kinase From Escherichia Coli Free Enzyme Structure</Hit_def>
          <Hit_accession>1CKE-A</Hit_accession>
          <Hit_len>227</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>30.8018</Hsp_bit-score>
              <Hsp_score>68</Hsp_score>
              <Hsp_evalue>0.280931</Hsp_evalue>
              <Hsp_query-from>9</Hsp_query-from>
              <Hsp_query-to>76</Hsp_query-to>
              <Hsp_hit-from>5</Hsp_hit-from>
              <Hsp_hit-to>69</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>19</Hsp_identity>
              <Hsp_positive>37</Hsp_positive>
              <Hsp_gaps>3</Hsp_gaps>
              <Hsp_align-len>68</Hsp_align-len>
              <Hsp_qseq>SKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDM</Hsp_qseq>
              <Hsp_hseq>APVITIDGPSGAGKGTLCKAMAEALQWHLLDSGAIYR--VLALAALHHHV-DVASEDALVPLASHLDV</Hsp_hseq>
              <Hsp_midline>+ +I + G  G+GKGT C+ + +   +  L +G + R  V + +A    + ++  +  LVPL + LD+</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>38</Hit_num>
          <Hit_id>gi|149242542|pdb|2O01|3</Hit_id>
          <Hit_def>Chain 3, The Structure Of A Plant Photosystem I Supercomplex At 3.4 Angstrom Resolution</Hit_def>
          <Hit_accession>2O01-3</Hit_accession>
          <Hit_len>165</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>30.4166</Hsp_bit-score>
              <Hsp_score>67</Hsp_score>
              <Hsp_evalue>0.452009</Hsp_evalue>
              <Hsp_query-from>38</Hsp_query-from>
              <Hsp_query-to>87</Hsp_query-to>
              <Hsp_hit-from>2</Hsp_hit-from>
              <Hsp_hit-to>51</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>13</Hsp_identity>
              <Hsp_positive>24</Hsp_positive>
              <Hsp_gaps>0</Hsp_gaps>
              <Hsp_align-len>50</Hsp_align-len>
              <Hsp_qseq>LSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDT</Hsp_qseq>
              <Hsp_hseq>LAYGEIINGRFAMLGAAGAIAPEILGKAGLIPAETALPWFQTGVIPPAGT</Hsp_hseq>
              <Hsp_midline>L+ G+++    +   A G +  EI+ K  L+P ET L   +  ++    T</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>39</Hit_num>
          <Hit_id>gi|60593443|pdb|1RKB|A</Hit_id>
          <Hit_def>Chain A, The Structure Of Adrenal Gland Protein Ad-004</Hit_def>
          <Hit_accession>1RKB-A</Hit_accession>
          <Hit_len>173</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>30.0314</Hsp_bit-score>
              <Hsp_score>66</Hsp_score>
              <Hsp_evalue>0.547634</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>47</Hsp_query-to>
              <Hsp_hit-from>7</Hsp_hit-from>
              <Hsp_hit-to>42</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>14</Hsp_identity>
              <Hsp_positive>21</Hsp_positive>
              <Hsp_gaps>0</Hsp_gaps>
              <Hsp_align-len>36</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAE</Hsp_qseq>
              <Hsp_hseq>ILLTGTPGVGKTTLGKELASKSGLKYINVGDLAREE</Hsp_hseq>
              <Hsp_midline>I + G PG GK T  +++  K G  +++ GDL R E</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>40</Hit_num>
          <Hit_id>gi|149242130|pdb|2IA5|A</Hit_id>
          <Hit_def>Chain A, T4 Polynucleotide KinasePHOSPHATASE WITH BOUND SULFATE AND Magnesium. &gt;gi|149242131|pdb|2IA5|B Chain B, T4 Polynucleotide KinasePHOSPHATASE WITH BOUND SULFATE AND Magnesium. &gt;gi|149242132|pdb|2IA5|C Chain C, T4 Polynucleotide KinasePHOSPHATASE WITH BOUND SULFATE AND Magnesium. &gt;gi|149242133|pdb|2IA5|D Chain D, T4 Polynucleotide KinasePHOSPHATASE WITH BOUND SULFATE AND Magnesium. &gt;gi|149242134|pdb|2IA5|E Chain E, T4 Polynucleotide KinasePHOSPHATASE WITH BOUND SULFATE AND Magnesium. &gt;gi|149242135|pdb|2IA5|F Chain F, T4 Polynucleotide KinasePHOSPHATASE WITH BOUND SULFATE AND Magnesium. &gt;gi|149242136|pdb|2IA5|G Chain G, T4 Polynucleotide KinasePHOSPHATASE WITH BOUND SULFATE AND Magnesium. &gt;gi|149242137|pdb|2IA5|H Chain H, T4 Polynucleotide KinasePHOSPHATASE WITH BOUND SULFATE AND Magnesium. &gt;gi|149242138|pdb|2IA5|I Chain I, T4 Polynucleotide KinasePHOSPHATASE WITH BOUND SULFATE AND Magnesium. &gt;gi|149242139|pdb|2IA5|J Chain J, T4 Polynucleotide KinasePHOSPHATASE WITH BOUND SULFATE AND Magnesium. &gt;gi|149242140|pdb|2IA5|K Chain K, T4 Polynucleotide KinasePHOSPHATASE WITH BOUND SULFATE AND Magnesium. &gt;gi|149242141|pdb|2IA5|L Chain L, T4 Polynucleotide KinasePHOSPHATASE WITH BOUND SULFATE AND Magnesium.</Hit_def>
          <Hit_accession>2IA5-A</Hit_accession>
          <Hit_len>301</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>30.0314</Hsp_bit-score>
              <Hsp_score>66</Hsp_score>
              <Hsp_evalue>0.575748</Hsp_evalue>
              <Hsp_query-from>10</Hsp_query-from>
              <Hsp_query-to>181</Hsp_query-to>
              <Hsp_hit-from>3</Hsp_hit-from>
              <Hsp_hit-to>180</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>42</Hsp_identity>
              <Hsp_positive>71</Hsp_positive>
              <Hsp_gaps>14</Hsp_gaps>
              <Hsp_align-len>182</Hsp_align-len>
              <Hsp_qseq>KIIFVVGGPGSGKGTQCEKIVQKY-GYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDG---YPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLE----TYYKAT--EPVIAFYEKRGIVRKVNAEGSVD</Hsp_qseq>
              <Hsp_hseq>KIILTIGCPGSGKSTWAREFIAKNPGFYNINRDD-YRQSIMAHEERDEYKYTKKKEGIVTGMQ--FDTAKSILYGG-DSVKGVIISDTNLNPERRLAWETFAKEYGWKVEHKVFDVPWTELVKRNSKRGTKAVPIDVLRSMYKSMREYLGLPVYNGTPGKPKAVIFDVDGTLAKMNGRGPYD</Hsp_hseq>
              <Hsp_midline>KII  +G PGSGK T   + + K  G+ +++  D  R  + +   R +      ++G +  ++   D  +  +    D+ KG +I      P      E F ++ G        D     + KR  KRG  +  +D      K   E      Y  T  +P    ++  G + K+N  G  D</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>41</Hit_num>
          <Hit_id>gi|5822329|pdb|1QRJ|B</Hit_id>
          <Hit_def>Chain B, Solution Structure Of Htlv-I Capsid Protein</Hit_def>
          <Hit_accession>1QRJ-B</Hit_accession>
          <Hit_len>199</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>29.6462</Hsp_bit-score>
              <Hsp_score>65</Hsp_score>
              <Hsp_evalue>0.625848</Hsp_evalue>
              <Hsp_query-from>20</Hsp_query-from>
              <Hsp_query-to>46</Hsp_query-to>
              <Hsp_hit-from>160</Hsp_hit-from>
              <Hsp_hit-to>186</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>11</Hsp_identity>
              <Hsp_positive>18</Hsp_positive>
              <Hsp_gaps>0</Hsp_gaps>
              <Hsp_align-len>27</Hsp_align-len>
              <Hsp_qseq>SGKGTQCEKIVQKYGYTHLSTGDLLRA</Hsp_qseq>
              <Hsp_hseq>SNANKECQKLLQARGHTNSPLGDMLRA</Hsp_hseq>
              <Hsp_midline>S    +C+K++Q  G+T+   GD+LRA</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>42</Hit_num>
          <Hit_id>gi|22219316|pdb|1LY1|A</Hit_id>
          <Hit_def>Chain A, Structure And Mechanism Of T4 Polynucleotide Kinase</Hit_def>
          <Hit_accession>1LY1-A</Hit_accession>
          <Hit_len>181</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>29.261</Hsp_bit-score>
              <Hsp_score>64</Hsp_score>
              <Hsp_evalue>0.873805</Hsp_evalue>
              <Hsp_query-from>10</Hsp_query-from>
              <Hsp_query-to>181</Hsp_query-to>
              <Hsp_hit-from>3</Hsp_hit-from>
              <Hsp_hit-to>180</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>42</Hsp_identity>
              <Hsp_positive>71</Hsp_positive>
              <Hsp_gaps>14</Hsp_gaps>
              <Hsp_align-len>182</Hsp_align-len>
              <Hsp_qseq>KIIFVVGGPGSGKGTQCEKIVQKY-GYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDG---YPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLE----TYYKAT--EPVIAFYEKRGIVRKVNAEGSVD</Hsp_qseq>
              <Hsp_hseq>KIILTIGCPGSGKSTWAREFIAKNPGFYNINRDD-YRQSIMAHEERDEYKYTKKKEGIVTGMQ--FDTAKSILYGG-DSVKGVIISDTNLNPERRLAWETFAKEYGWKVEHKVFDVPWTELVKRNSKRGTKAVPIDVLRSMYKSMREYLGLPVYNGTPGKPKAVIFDVDGTLAKMNGRGPYD</Hsp_hseq>
              <Hsp_midline>KII  +G PGSGK T   + + K  G+ +++  D  R  + +   R +      ++G +  ++   D  +  +    D+ KG +I      P      E F ++ G        D     + KR  KRG  +  +D      K   E      Y  T  +P    ++  G + K+N  G  D</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>43</Hit_num>
          <Hit_id>gi|119390387|pdb|2J37|W</Hit_id>
          <Hit_def>Chain W, Model Of Mammalian Srp Bound To 80s Rncs</Hit_def>
          <Hit_accession>2J37-W</Hit_accession>
          <Hit_len>504</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>29.261</Hsp_bit-score>
              <Hsp_score>64</Hsp_score>
              <Hsp_evalue>0.90346</Hsp_evalue>
              <Hsp_query-from>8</Hsp_query-from>
              <Hsp_query-to>46</Hsp_query-to>
              <Hsp_hit-from>100</Hsp_hit-from>
              <Hsp_hit-to>142</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>18</Hsp_identity>
              <Hsp_positive>22</Hsp_positive>
              <Hsp_gaps>4</Hsp_gaps>
              <Hsp_align-len>43</Hsp_align-len>
              <Hsp_qseq>KSKIIFVVGGPGSGKGTQCEKIV---QKYGY-THLSTGDLLRA</Hsp_qseq>
              <Hsp_hseq>KQNVIMFVGLQGSGKTTTCSKLAYYYQRKGWKTCLICADTFRA</Hsp_hseq>
              <Hsp_midline>K  +I  VG  GSGK T C K+    Q+ G+ T L   D  RA</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>44</Hit_num>
          <Hit_id>gi|126031079|pdb|2JAS|A</Hit_id>
          <Hit_def>Chain A, Structure Of Deoxyadenosine Kinase From M.Mycoides With Bound Datp &gt;gi|126031080|pdb|2JAS|B Chain B, Structure Of Deoxyadenosine Kinase From M.Mycoides With Bound Datp &gt;gi|126031081|pdb|2JAS|C Chain C, Structure Of Deoxyadenosine Kinase From M.Mycoides With Bound Datp &gt;gi|126031082|pdb|2JAS|D Chain D, Structure Of Deoxyadenosine Kinase From M.Mycoides With Bound Datp &gt;gi|126031083|pdb|2JAS|E Chain E, Structure Of Deoxyadenosine Kinase From M.Mycoides With Bound Datp &gt;gi|126031084|pdb|2JAS|F Chain F, Structure Of Deoxyadenosine Kinase From M.Mycoides With Bound Datp</Hit_def>
          <Hit_accession>2JAS-A</Hit_accession>
          <Hit_len>206</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>28.8758</Hsp_bit-score>
              <Hsp_score>63</Hsp_score>
              <Hsp_evalue>1.15079</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>195</Hsp_query-to>
              <Hsp_hit-from>4</Hsp_hit-from>
              <Hsp_hit-to>203</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>47</Hsp_identity>
              <Hsp_positive>80</Hsp_positive>
              <Hsp_gaps>42</Hsp_gaps>
              <Hsp_align-len>213</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQKYGYTHLS------------TGDL----LRAEVSSGSARGKMLSEIMEKGQLVPLETVL---DMLRDAMVAKVD--------TSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRV--DDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</Hsp_qseq>
              <Hsp_hseq>IAIFGTVGAGKSTISAEISKKLGYEIFKEPVEENPYFEQYYKDLKKTVFKMQIYMLTARSKQLK------QAKNLENIIFDRTLLEDPIFMKVNYDLNNVDQTDYNTYIDFYNNVVLENLKIPENKLSFDIVIYLRVSTKTAISRIKKRGRSEELLIGEEYWETLNKNYEEFYKQNVYDFPFF-------VVDAELDVKTQIELIMNKLNSIK</Hsp_hseq>
              <Hsp_midline>I + G  G+GK T   +I +K GY                  DL     + ++   +AR K L       Q   LE ++    +L D +  KV+        T     ID Y   V +  +         +++Y+    +T   R+ KRG +   +  ++  ET+ K  E +YK       F+        V+AE  V      +   L+++K</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>45</Hit_num>
          <Hit_id>gi|126031077|pdb|2JAQ|A</Hit_id>
          <Hit_def>Chain A, Structure Of Deoxyadenosine Kinase From M. Mycoides With Bound Dctp &gt;gi|126031078|pdb|2JAQ|B Chain B, Structure Of Deoxyadenosine Kinase From M. Mycoides With Bound Dctp &gt;gi|126031085|pdb|2JAT|A Chain A, Structure Of Deoxyadenosine Kinase From M.Mycoides With Products Dcmp And A Flexible Dcdp Bound &gt;gi|126031086|pdb|2JAT|B Chain B, Structure Of Deoxyadenosine Kinase From M.Mycoides With Products Dcmp And A Flexible Dcdp Bound</Hit_def>
          <Hit_accession>2JAQ-A</Hit_accession>
          <Hit_len>205</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>28.8758</Hsp_bit-score>
              <Hsp_score>63</Hsp_score>
              <Hsp_evalue>1.27198</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>195</Hsp_query-to>
              <Hsp_hit-from>3</Hsp_hit-from>
              <Hsp_hit-to>202</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>47</Hsp_identity>
              <Hsp_positive>80</Hsp_positive>
              <Hsp_gaps>42</Hsp_gaps>
              <Hsp_align-len>213</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQKYGYTHLS------------TGDL----LRAEVSSGSARGKMLSEIMEKGQLVPLETVL---DMLRDAMVAKVD--------TSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRV--DDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</Hsp_qseq>
              <Hsp_hseq>IAIFGTVGAGKSTISAEISKKLGYEIFKEPVEENPYFEQYYKDLKKTVFKMQIYMLTARSKQLK------QAKNLENIIFDRTLLEDPIFMKVNYDLNNVDQTDYNTYIDFYNNVVLENLKIPENKLSFDIVIYLRVSTKTAISRIKKRGRSEELLIGEEYWETLNKNYEEFYKQNVYDFPFF-------VVDAELDVKTQIELIMNKLNSIK</Hsp_hseq>
              <Hsp_midline>I + G  G+GK T   +I +K GY                  DL     + ++   +AR K L       Q   LE ++    +L D +  KV+        T     ID Y   V +  +         +++Y+    +T   R+ KRG +   +  ++  ET+ K  E +YK       F+        V+AE  V      +   L+++K</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>46</Hit_num>
          <Hit_id>gi|24987639|pdb|1LTQ|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of T4 Polynucleotide Kinase &gt;gi|46015454|pdb|1RC8|A Chain A, T4 Polynucleotide Kinase Bound To 5'-Gtcac-3' Ssdna &gt;gi|46015523|pdb|1RPZ|A Chain A, T4 Polynucleotide Kinase Bound To 5'-Tgcac-3' Ssdna &gt;gi|46015542|pdb|1RRC|A Chain A, T4 Polynucleotide Kinase Bound To 5'-Gtc-3' Ssdna</Hit_def>
          <Hit_accession>1LTQ-A</Hit_accession>
          <Hit_len>301</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>28.4906</Hsp_bit-score>
              <Hsp_score>62</Hsp_score>
              <Hsp_evalue>1.41771</Hsp_evalue>
              <Hsp_query-from>10</Hsp_query-from>
              <Hsp_query-to>181</Hsp_query-to>
              <Hsp_hit-from>3</Hsp_hit-from>
              <Hsp_hit-to>180</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>42</Hsp_identity>
              <Hsp_positive>69</Hsp_positive>
              <Hsp_gaps>14</Hsp_gaps>
              <Hsp_align-len>182</Hsp_align-len>
              <Hsp_qseq>KIIFVVGGPGSGKGTQCEKIVQKY-GYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDG---YPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLE----TYYKAT--EPVIAFYEKRGIVRKVNAEGSVD</Hsp_qseq>
              <Hsp_hseq>KIILTIGCPGSGKSTWAREFIAKNPGFYNINRDD-YRQSIXAHEERDEYKYTKKKEGIVTGXQ--FDTAKSILYGG-DSVKGVIISDTNLNPERRLAWETFAKEYGWKVEHKVFDVPWTELVKRNSKRGTKAVPIDVLRSXYKSXREYLGLPVYNGTPGKPKAVIFDVDGTLAKXNGRGPYD</Hsp_hseq>
              <Hsp_midline>KII  +G PGSGK T   + + K  G+ +++  D  R  + +   R +      ++G +   +   D  +  +    D+ KG +I      P      E F ++ G        D     + KR  KRG  +  +D      K   E      Y  T  +P    ++  G + K N  G  D</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>47</Hit_num>
          <Hit_id>gi|11514612|pdb|1QNH|A</Hit_id>
          <Hit_def>Chain A, Plasmodium Falciparum Cyclophilin (Double Mutant) Complexed With Cyclosporin A &gt;gi|11514613|pdb|1QNH|B Chain B, Plasmodium Falciparum Cyclophilin (Double Mutant) Complexed With Cyclosporin A</Hit_def>
          <Hit_accession>1QNH-A</Hit_accession>
          <Hit_len>170</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>28.1054</Hsp_bit-score>
              <Hsp_score>61</Hsp_score>
              <Hsp_evalue>2.0127</Hsp_evalue>
              <Hsp_query-from>104</Hsp_query-from>
              <Hsp_query-to>131</Hsp_query-to>
              <Hsp_hit-from>92</Hsp_hit-from>
              <Hsp_hit-to>119</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>12</Hsp_identity>
              <Hsp_positive>15</Hsp_positive>
              <Hsp_gaps>0</Hsp_gaps>
              <Hsp_align-len>28</Hsp_align-len>
              <Hsp_qseq>EEFERKIGQPTLLLYVDAGPETMTKRLL</Hsp_qseq>
              <Hsp_hseq>ENFNMKHDQPGLLSMANAGPNTNSSQFL</Hsp_hseq>
              <Hsp_midline>E F  K  QP LL   +AGP T + + L</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>48</Hit_num>
          <Hit_id>gi|109157994|pdb|2GRJ|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of Dephospho-Coa Kinase (Ec 2.7.1.24) (Dephosphocoenzyme A Kinase) (Tm1387) From Thermotoga Maritima At 2.60 A Resolution &gt;gi|109157995|pdb|2GRJ|B Chain B, Crystal Structure Of Dephospho-Coa Kinase (Ec 2.7.1.24) (Dephosphocoenzyme A Kinase) (Tm1387) From Thermotoga Maritima At 2.60 A Resolution &gt;gi|109157996|pdb|2GRJ|C Chain C, Crystal Structure Of Dephospho-Coa Kinase (Ec 2.7.1.24) (Dephosphocoenzyme A Kinase) (Tm1387) From Thermotoga Maritima At 2.60 A Resolution &gt;gi|109157997|pdb|2GRJ|D Chain D, Crystal Structure Of Dephospho-Coa Kinase (Ec 2.7.1.24) (Dephosphocoenzyme A Kinase) (Tm1387) From Thermotoga Maritima At 2.60 A Resolution &gt;gi|109157998|pdb|2GRJ|E Chain E, Crystal Structure Of Dephospho-Coa Kinase (Ec 2.7.1.24) (Dephosphocoenzyme A Kinase) (Tm1387) From Thermotoga Maritima At 2.60 A Resolution &gt;gi|109157999|pdb|2GRJ|F Chain F, Crystal Structure Of Dephospho-Coa Kinase (Ec 2.7.1.24) (Dephosphocoenzyme A Kinase) (Tm1387) From Thermotoga Maritima At 2.60 A Resolution &gt;gi|109158000|pdb|2GRJ|G Chain G, Crystal Structure Of Dephospho-Coa Kinase (Ec 2.7.1.24) (Dephosphocoenzyme A Kinase) (Tm1387) From Thermotoga Maritima At 2.60 A Resolution &gt;gi|109158001|pdb|2GRJ|H Chain H, Crystal Structure Of Dephospho-Coa Kinase (Ec 2.7.1.24) (Dephosphocoenzyme A Kinase) (Tm1387) From Thermotoga Maritima At 2.60 A Resolution</Hit_def>
          <Hit_accession>2GRJ-A</Hit_accession>
          <Hit_len>192</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>28.1054</Hsp_bit-score>
              <Hsp_score>61</Hsp_score>
              <Hsp_evalue>2.09844</Hsp_evalue>
              <Hsp_query-from>11</Hsp_query-from>
              <Hsp_query-to>48</Hsp_query-to>
              <Hsp_hit-from>14</Hsp_hit-from>
              <Hsp_hit-to>50</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>16</Hsp_identity>
              <Hsp_positive>21</Hsp_positive>
              <Hsp_gaps>1</Hsp_gaps>
              <Hsp_align-len>38</Hsp_align-len>
              <Hsp_qseq>IIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEV</Hsp_qseq>
              <Hsp_hseq>VIGVTGKIGTGKSTVCEILKNKYG-AHVVNVDRIGHEV</Hsp_hseq>
              <Hsp_midline>+I V G  G+GK T CE +  KYG  H+   D +  EV</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>49</Hit_num>
          <Hit_id>gi|126031143|pdb|2JEO|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of Human Uridine-Cytidine Kinase 1 &gt;gi|145580570|pdb|2UVQ|A Chain A, Crystal Structure Of Human Uridine-Cytidine Kinase 1 In Complex With Adp</Hit_def>
          <Hit_accession>2JEO-A</Hit_accession>
          <Hit_len>245</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>27.7202</Hsp_bit-score>
              <Hsp_score>60</Hsp_score>
              <Hsp_evalue>2.41824</Hsp_evalue>
              <Hsp_query-from>11</Hsp_query-from>
              <Hsp_query-to>34</Hsp_query-to>
              <Hsp_hit-from>27</Hsp_hit-from>
              <Hsp_hit-to>50</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>13</Hsp_identity>
              <Hsp_positive>16</Hsp_positive>
              <Hsp_gaps>0</Hsp_gaps>
              <Hsp_align-len>24</Hsp_align-len>
              <Hsp_qseq>IIFVVGGPGSGKGTQCEKIVQKYG</Hsp_qseq>
              <Hsp_hseq>LIGVSGGTASGKSTVCEKIMELLG</Hsp_hseq>
              <Hsp_midline>+I V GG  SGK T CEKI++  G</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>50</Hit_num>
          <Hit_id>gi|15988207|pdb|1JZT|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of Yeast Ynu0, Ynl200c &gt;gi|15988208|pdb|1JZT|B Chain B, Crystal Structure Of Yeast Ynu0, Ynl200c</Hit_def>
          <Hit_accession>1JZT-A</Hit_accession>
          <Hit_len>246</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>27.7202</Hsp_bit-score>
              <Hsp_score>60</Hsp_score>
              <Hsp_evalue>2.74065</Hsp_evalue>
              <Hsp_query-from>7</Hsp_query-from>
              <Hsp_query-to>36</Hsp_query-to>
              <Hsp_hit-from>56</Hsp_hit-from>
              <Hsp_hit-to>87</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>12</Hsp_identity>
              <Hsp_positive>20</Hsp_positive>
              <Hsp_gaps>2</Hsp_gaps>
              <Hsp_align-len>32</Hsp_align-len>
              <Hsp_qseq>KKSKIIFVVGGPGS--GKGTQCEKIVQKYGYT</Hsp_qseq>
              <Hsp_hseq>EKGKHVFVIAGPGNNGGDGLVCARHLKLFGYN</Hsp_hseq>
              <Hsp_midline>+K K +FV+ GPG+  G G  C + ++ +GY </Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>51</Hit_num>
          <Hit_id>gi|82408092|pdb|2BDT|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of The Putative Gluconate Kinase From Bacillus Halodurans, Northeast Structural Genomics Target Bhr61</Hit_def>
          <Hit_accession>2BDT-A</Hit_accession>
          <Hit_len>189</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>27.335</Hsp_bit-score>
              <Hsp_score>59</Hsp_score>
              <Hsp_evalue>3.40462</Hsp_evalue>
              <Hsp_query-from>10</Hsp_query-from>
              <Hsp_query-to>101</Hsp_query-to>
              <Hsp_hit-from>2</Hsp_hit-from>
              <Hsp_hit-to>88</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>18</Hsp_identity>
              <Hsp_positive>43</Hsp_positive>
              <Hsp_gaps>7</Hsp_gaps>
              <Hsp_align-len>93</Hsp_align-len>
              <Hsp_qseq>KIIFVVGGP-GSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVK</Hsp_qseq>
              <Hsp_hseq>KKLYIITGPAGVGKSTTCKRLAAQLDNSAYIEGDIINHXVVGGYRPPWESDELLA----LTWKNITDLTVNFLLAQNDVVLDYI--AFPDEAE</Hsp_hseq>
              <Hsp_midline>K ++++ GP G GK T C+++  +   +    GD++   V  G        E++     +  + + D+  + ++A+ D    ++   +P E +</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>52</Hit_num>
          <Hit_id>gi|62738505|pdb|1XRJ|A</Hit_id>
          <Hit_def>Chain A, Rapid Structure Determination Of Human Uridine-Cytidine Kinase 2 Using A Conventional Laboratory X-Ray Source And A Single Samarium Derivative &gt;gi|62738506|pdb|1XRJ|B Chain B, Rapid Structure Determination Of Human Uridine-Cytidine Kinase 2 Using A Conventional Laboratory X-Ray Source And A Single Samarium Derivative</Hit_def>
          <Hit_accession>1XRJ-A</Hit_accession>
          <Hit_len>261</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>26.9498</Hsp_bit-score>
              <Hsp_score>58</Hsp_score>
              <Hsp_evalue>4.09062</Hsp_evalue>
              <Hsp_query-from>11</Hsp_query-from>
              <Hsp_query-to>34</Hsp_query-to>
              <Hsp_hit-from>22</Hsp_hit-from>
              <Hsp_hit-to>45</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>13</Hsp_identity>
              <Hsp_positive>15</Hsp_positive>
              <Hsp_gaps>0</Hsp_gaps>
              <Hsp_align-len>24</Hsp_align-len>
              <Hsp_qseq>IIFVVGGPGSGKGTQCEKIVQKYG</Hsp_qseq>
              <Hsp_hseq>LIGVSGGTASGKSSVCAKIVQLLG</Hsp_hseq>
              <Hsp_midline>+I V GG  SGK + C KIVQ  G</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>53</Hit_num>
          <Hit_id>gi|93279032|pdb|2CCG|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of His-Tagged S. Aureus Thymidylate Kinase Complexed With Thymidine Monophosphate (Tmp) &gt;gi|93279033|pdb|2CCG|B Chain B, Crystal Structure Of His-Tagged S. Aureus Thymidylate Kinase Complexed With Thymidine Monophosphate (Tmp)</Hit_def>
          <Hit_accession>2CCG-A</Hit_accession>
          <Hit_len>225</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>26.9498</Hsp_bit-score>
              <Hsp_score>58</Hsp_score>
              <Hsp_evalue>4.30062</Hsp_evalue>
              <Hsp_query-from>85</Hsp_query-from>
              <Hsp_query-to>194</Hsp_query-to>
              <Hsp_hit-from>114</Hsp_hit-from>
              <Hsp_hit-to>225</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>29</Hsp_identity>
              <Hsp_positive>53</Hsp_positive>
              <Hsp_gaps>16</Hsp_gaps>
              <Hsp_align-len>119</Hsp_align-len>
              <Hsp_qseq>VDTSKGFLIDGYPR-----EVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNA----EGSVDDVFSQVCTHLDTL</Hsp_qseq>
              <Hsp_hseq>IDSSLAY--QGYARGIGVEEVRALNEFAINGLYPDLTIYLNVSAEVGRERIIKNSRDQNRLD--QEDLKFH-EKVIEGYQEIIHNESQR--FKSVNADQPLENVVEDTYQTIIKYLEKI</Hsp_hseq>
              <Hsp_midline>+D+S  +   GY R     EV+   EF      P L +Y++   E   +R++K      R+D  +E +K   E   +  + +I    +R   + VNA    E  V+D +  +  +L+ +</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>54</Hit_num>
          <Hit_id>gi|93279034|pdb|2CCJ|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of S. Aureus Thymidylate Kinase Complexed With Thymidine Monophosphate &gt;gi|93279035|pdb|2CCJ|B Chain B, Crystal Structure Of S. Aureus Thymidylate Kinase Complexed With Thymidine Monophosphate &gt;gi|93279036|pdb|2CCK|A Chain A, Crystal Structure Of Unliganded S. Aureus Thymidylate Kinase &gt;gi|93279037|pdb|2CCK|B Chain B, Crystal Structure Of Unliganded S. Aureus Thymidylate Kinase</Hit_def>
          <Hit_accession>2CCJ-A</Hit_accession>
          <Hit_len>205</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>26.9498</Hsp_bit-score>
              <Hsp_score>58</Hsp_score>
              <Hsp_evalue>4.33665</Hsp_evalue>
              <Hsp_query-from>85</Hsp_query-from>
              <Hsp_query-to>194</Hsp_query-to>
              <Hsp_hit-from>94</Hsp_hit-from>
              <Hsp_hit-to>205</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>29</Hsp_identity>
              <Hsp_positive>53</Hsp_positive>
              <Hsp_gaps>16</Hsp_gaps>
              <Hsp_align-len>119</Hsp_align-len>
              <Hsp_qseq>VDTSKGFLIDGYPR-----EVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNA----EGSVDDVFSQVCTHLDTL</Hsp_qseq>
              <Hsp_hseq>IDSSLAY--QGYARGIGVEEVRALNEFAINGLYPDLTIYLNVSAEVGRERIIKNSRDQNRLD--QEDLKFH-EKVIEGYQEIIHNESQR--FKSVNADQPLENVVEDTYQTIIKYLEKI</Hsp_hseq>
              <Hsp_midline>+D+S  +   GY R     EV+   EF      P L +Y++   E   +R++K      R+D  +E +K   E   +  + +I    +R   + VNA    E  V+D +  +  +L+ +</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>55</Hit_num>
          <Hit_id>gi|48425784|pdb|1UDW|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of Human Uridine-Cytidine Kinase 2 Complexed With A Feedback-Inhibitor, Ctp &gt;gi|48425785|pdb|1UDW|B Chain B, Crystal Structure Of Human Uridine-Cytidine Kinase 2 Complexed With A Feedback-Inhibitor, Ctp &gt;gi|48425786|pdb|1UEI|A Chain A, Crystal Structure Of Human Uridine-Cytidine Kinase 2 Complexed With A Feedback-Inhibitor, Utp &gt;gi|48425787|pdb|1UEI|B Chain B, Crystal Structure Of Human Uridine-Cytidine Kinase 2 Complexed With A Feedback-Inhibitor, Utp &gt;gi|48425788|pdb|1UEJ|A Chain A, Crystal Structure Of Human Uridine-Cytidine Kinase 2 Complexed With A Substrate, Cytidine &gt;gi|48425789|pdb|1UEJ|B Chain B, Crystal Structure Of Human Uridine-Cytidine Kinase 2 Complexed With A Substrate, Cytidine &gt;gi|48425790|pdb|1UFQ|A Chain A, Crystal Structure Of Ligand-Free Human Uridine-Cytidine Kinase 2 &gt;gi|48425791|pdb|1UFQ|B Chain B, Crystal Structure Of Ligand-Free Human Uridine-Cytidine Kinase 2 &gt;gi|48425792|pdb|1UFQ|C Chain C, Crystal Structure Of Ligand-Free Human Uridine-Cytidine Kinase 2 &gt;gi|48425793|pdb|1UFQ|D Chain D, Crystal Structure Of Ligand-Free Human Uridine-Cytidine Kinase 2 &gt;gi|48425799|pdb|1UJ2|A Chain A, Crystal Structure Of Human Uridine-Cytidine Kinase 2 Complexed With Products, Cmp And Adp &gt;gi|48425800|pdb|1UJ2|B Chain B, Crystal Structure Of Human Uridine-Cytidine Kinase 2 Complexed With Products, Cmp And Adp</Hit_def>
          <Hit_accession>1UDW-A</Hit_accession>
          <Hit_len>252</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>26.9498</Hsp_bit-score>
              <Hsp_score>58</Hsp_score>
              <Hsp_evalue>4.8335</Hsp_evalue>
              <Hsp_query-from>11</Hsp_query-from>
              <Hsp_query-to>34</Hsp_query-to>
              <Hsp_hit-from>24</Hsp_hit-from>
              <Hsp_hit-to>47</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>13</Hsp_identity>
              <Hsp_positive>15</Hsp_positive>
              <Hsp_gaps>0</Hsp_gaps>
              <Hsp_align-len>24</Hsp_align-len>
              <Hsp_qseq>IIFVVGGPGSGKGTQCEKIVQKYG</Hsp_qseq>
              <Hsp_hseq>LIGVSGGTASGKSSVCAKIVQLLG</Hsp_hseq>
              <Hsp_midline>+I V GG  SGK + C KIVQ  G</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>56</Hit_num>
          <Hit_id>gi|11514610|pdb|1QNG|A</Hit_id>
          <Hit_def>Chain A, Plasmodium Falciparum Cyclophilin Complexed With Cyclosporin A</Hit_def>
          <Hit_accession>1QNG-A</Hit_accession>
          <Hit_len>170</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>26.9498</Hsp_bit-score>
              <Hsp_score>58</Hsp_score>
              <Hsp_evalue>4.8335</Hsp_evalue>
              <Hsp_query-from>104</Hsp_query-from>
              <Hsp_query-to>131</Hsp_query-to>
              <Hsp_hit-from>92</Hsp_hit-from>
              <Hsp_hit-to>119</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>11</Hsp_identity>
              <Hsp_positive>14</Hsp_positive>
              <Hsp_gaps>0</Hsp_gaps>
              <Hsp_align-len>28</Hsp_align-len>
              <Hsp_qseq>EEFERKIGQPTLLLYVDAGPETMTKRLL</Hsp_qseq>
              <Hsp_hseq>ENFNMKHDQPGLLSMANAGPNTNSSQFF</Hsp_hseq>
              <Hsp_midline>E F  K  QP LL   +AGP T + +  </Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>57</Hit_num>
          <Hit_id>gi|145579338|pdb|2GA8|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of Yfh7 From Saccharomyces Cerevisiae: A Putative P-Loop Containing Kinase With A Circular Permutation. &gt;gi|145579339|pdb|2GAA|A Chain A, Crystal Structure Of Yfh7 From Saccharomyces Cerevisiae: A Putative P-Loop Containing Kinase With A Circular Permutation.</Hit_def>
          <Hit_accession>2GA8-A</Hit_accession>
          <Hit_len>359</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>26.5646</Hsp_bit-score>
              <Hsp_score>57</Hsp_score>
              <Hsp_evalue>5.34251</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>31</Hsp_query-to>
              <Hsp_hit-from>27</Hsp_hit-from>
              <Hsp_hit-to>46</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>10</Hsp_identity>
              <Hsp_positive>14</Hsp_positive>
              <Hsp_gaps>0</Hsp_gaps>
              <Hsp_align-len>20</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQ</Hsp_qseq>
              <Hsp_hseq>VILVGSPGSGKSTIAEELXQ</Hsp_hseq>
              <Hsp_midline>+ +VG PGSGK T  E++ Q</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>58</Hit_num>
          <Hit_id>gi|62738645|pdb|1YJ5|A</Hit_id>
          <Hit_def>Chain A, Molecular Architecture Of Mammalian Polynucleotide Kinase, A Dna Repair Enzyme &gt;gi|62738646|pdb|1YJ5|B Chain B, Molecular Architecture Of Mammalian Polynucleotide Kinase, A Dna Repair Enzyme</Hit_def>
          <Hit_accession>1YJ5-A</Hit_accession>
          <Hit_len>383</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>26.5646</Hsp_bit-score>
              <Hsp_score>57</Hsp_score>
              <Hsp_evalue>5.43241</Hsp_evalue>
              <Hsp_query-from>8</Hsp_query-from>
              <Hsp_query-to>43</Hsp_query-to>
              <Hsp_hit-from>224</Hsp_hit-from>
              <Hsp_hit-to>259</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>11</Hsp_identity>
              <Hsp_positive>20</Hsp_positive>
              <Hsp_gaps>0</Hsp_gaps>
              <Hsp_align-len>36</Hsp_align-len>
              <Hsp_qseq>KSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDL</Hsp_qseq>
              <Hsp_hseq>NPEVVVAVGFPGAGKSTFIQEHLVSAGYVHVNRDTL</Hsp_hseq>
              <Hsp_midline>  +++  VG PG+GK T  ++ +   GY H++   L</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>59</Hit_num>
          <Hit_id>gi|157880426|pdb|1US5|A</Hit_id>
          <Hit_def>Chain A, Putative Glur0 Ligand Binding Core With L-Glutamate</Hit_def>
          <Hit_accession>1US5-A</Hit_accession>
          <Hit_len>299</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>26.5646</Hsp_bit-score>
              <Hsp_score>57</Hsp_score>
              <Hsp_evalue>6.26029</Hsp_evalue>
              <Hsp_query-from>14</Hsp_query-from>
              <Hsp_query-to>51</Hsp_query-to>
              <Hsp_hit-from>119</Hsp_hit-from>
              <Hsp_hit-to>156</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>14</Hsp_identity>
              <Hsp_positive>18</Hsp_positive>
              <Hsp_gaps>0</Hsp_gaps>
              <Hsp_align-len>38</Hsp_align-len>
              <Hsp_qseq>VVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSG</Hsp_qseq>
              <Hsp_hseq>VVGDVGSGTEQNARQILEAYGLTFDDLGQAIRVSASQG</Hsp_hseq>
              <Hsp_midline>VVG  GSG      +I++ YG T    G  +R   S G</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>60</Hit_num>
          <Hit_id>gi|157880425|pdb|1US4|A</Hit_id>
          <Hit_def>Chain A, Putative Glur0 Ligand Binding Core With L-Glutamate</Hit_def>
          <Hit_accession>1US4-A</Hit_accession>
          <Hit_len>299</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>26.5646</Hsp_bit-score>
              <Hsp_score>57</Hsp_score>
              <Hsp_evalue>6.63682</Hsp_evalue>
              <Hsp_query-from>14</Hsp_query-from>
              <Hsp_query-to>51</Hsp_query-to>
              <Hsp_hit-from>119</Hsp_hit-from>
              <Hsp_hit-to>156</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>14</Hsp_identity>
              <Hsp_positive>18</Hsp_positive>
              <Hsp_gaps>0</Hsp_gaps>
              <Hsp_align-len>38</Hsp_align-len>
              <Hsp_qseq>VVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSG</Hsp_qseq>
              <Hsp_hseq>VVGDVGSGTEQNARQILEAYGLTFDDLGQAIRVSASQG</Hsp_hseq>
              <Hsp_midline>VVG  GSG      +I++ YG T    G  +R   S G</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>61</Hit_num>
          <Hit_id>gi|62738634|pdb|1YE8|A</Hit_id>
          <Hit_def>Chain A, Crystal Structure Of Thep1 From The Hyperthermophile Aquifex Aeolicus</Hit_def>
          <Hit_accession>1YE8-A</Hit_accession>
          <Hit_len>178</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>26.5646</Hsp_bit-score>
              <Hsp_score>57</Hsp_score>
              <Hsp_evalue>6.63682</Hsp_evalue>
              <Hsp_query-from>12</Hsp_query-from>
              <Hsp_query-to>34</Hsp_query-to>
              <Hsp_hit-from>3</Hsp_hit-from>
              <Hsp_hit-to>25</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>11</Hsp_identity>
              <Hsp_positive>15</Hsp_positive>
              <Hsp_gaps>0</Hsp_gaps>
              <Hsp_align-len>23</Hsp_align-len>
              <Hsp_qseq>IFVVGGPGSGKGTQCEKIVQKYG</Hsp_qseq>
              <Hsp_hseq>IIITGEPGVGKTTLVKKIVERLG</Hsp_hseq>
              <Hsp_midline>I + G PG GK T  +KIV++ G</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>62</Hit_num>
          <Hit_id>gi|6729847|pdb|4TMK|A</Hit_id>
          <Hit_def>Chain A, Complex Of E. Coli Thymidylate Kinase With The Bisubstrate Inhibitor Tp5a &gt;gi|6729850|pdb|5TMP|A Chain A, Complex Of E. Coli Thymidylate Kinase With The Bisubstrate Inhibitor Aztp5a</Hit_def>
          <Hit_accession>4TMK-A</Hit_accession>
          <Hit_len>213</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>26.1794</Hsp_bit-score>
              <Hsp_score>56</Hsp_score>
              <Hsp_evalue>7.27479</Hsp_evalue>
              <Hsp_query-from>112</Hsp_query-from>
              <Hsp_query-to>135</Hsp_query-to>
              <Hsp_hit-from>132</Hsp_hit-from>
              <Hsp_hit-to>155</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>12</Hsp_identity>
              <Hsp_positive>14</Hsp_positive>
              <Hsp_gaps>0</Hsp_gaps>
              <Hsp_align-len>24</Hsp_align-len>
              <Hsp_qseq>QPTLLLYVDAGPETMTKRLLKRGE</Hsp_qseq>
              <Hsp_hseq>RPDLTLYLDVTPEVGLKRARARGE</Hsp_hseq>
              <Hsp_midline>+P L LY+D  PE   KR   RGE</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>63</Hit_num>
          <Hit_id>gi|222143060|pdb|3CKF|A</Hit_id>
          <Hit_def>Chain A, The Crystal Structure Of Ospa Deletion Mutant</Hit_def>
          <Hit_accession>3CKF-A</Hit_accession>
          <Hit_len>228</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>25.7942</Hsp_bit-score>
              <Hsp_score>55</Hsp_score>
              <Hsp_evalue>8.96213</Hsp_evalue>
              <Hsp_query-from>95</Hsp_query-from>
              <Hsp_query-to>189</Hsp_query-to>
              <Hsp_hit-from>111</Hsp_hit-from>
              <Hsp_hit-to>197</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>21</Hsp_identity>
              <Hsp_positive>39</Hsp_positive>
              <Hsp_gaps>8</Hsp_gaps>
              <Hsp_align-len>95</Hsp_align-len>
              <Hsp_qseq>GYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCT</Hsp_qseq>
              <Hsp_hseq>GKAKEVLKGYVLEGTLTAEKTTLVVKEGTVTLSKNISKSGEVSVELNDTDSSAATKKTAAWNSGTSTLTIT--------VNSKKTKDLVFTSSNT</Hsp_hseq>
              <Hsp_midline>G  +EV +G   E  +      L V  G  T++K + K GE S  ++D + +   +    + +    +           VN++ + D VF+   T</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>64</Hit_num>
          <Hit_id>gi|197107503|pdb|3E1S|A</Hit_id>
          <Hit_def>Chain A, Structure Of An N-Terminal Truncation Of Deinococcus Radiodurans Recd2</Hit_def>
          <Hit_accession>3E1S-A</Hit_accession>
          <Hit_len>574</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>25.7942</Hsp_bit-score>
              <Hsp_score>55</Hsp_score>
              <Hsp_evalue>9.1893</Hsp_evalue>
              <Hsp_query-from>4</Hsp_query-from>
              <Hsp_query-to>30</Hsp_query-to>
              <Hsp_hit-from>199</Hsp_hit-from>
              <Hsp_hit-to>225</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>8</Hsp_identity>
              <Hsp_positive>17</Hsp_positive>
              <Hsp_gaps>0</Hsp_gaps>
              <Hsp_align-len>27</Hsp_align-len>
              <Hsp_qseq>EKLKKSKIIFVVGGPGSGKGTQCEKIV</Hsp_qseq>
              <Hsp_hseq>DQLAGHRLVVLTGGPGTGKSTTTKAVA</Hsp_hseq>
              <Hsp_midline>++L   +++ + GGPG+GK T  + + </Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>65</Hit_num>
          <Hit_id>gi|162329948|pdb|2OY1|O</Hit_id>
          <Hit_def>Chain O, The Crystal Structure Of Ospa Mutant</Hit_def>
          <Hit_accession>2OY1-O</Hit_accession>
          <Hit_len>250</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>25.7942</Hsp_bit-score>
              <Hsp_score>55</Hsp_score>
              <Hsp_evalue>9.42222</Hsp_evalue>
              <Hsp_query-from>95</Hsp_query-from>
              <Hsp_query-to>189</Hsp_query-to>
              <Hsp_hit-from>133</Hsp_hit-from>
              <Hsp_hit-to>219</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>21</Hsp_identity>
              <Hsp_positive>39</Hsp_positive>
              <Hsp_gaps>8</Hsp_gaps>
              <Hsp_align-len>95</Hsp_align-len>
              <Hsp_qseq>GYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCT</Hsp_qseq>
              <Hsp_hseq>GKAKEVLKGYVLEGTLTAEKTTLVVKEGTVTLSKNISKSGEVSVELNDTDSSAATKKTAAWNSGTSTLTIT--------VNSKKTKDLVFTSSNT</Hsp_hseq>
              <Hsp_midline>G  +EV +G   E  +      L V  G  T++K + K GE S  ++D + +   +    + +    +           VN++ + D VF+   T</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>66</Hit_num>
          <Hit_id>gi|162329945|pdb|2OL8|O</Hit_id>
          <Hit_def>Chain O, The Crystal Structure Of Ospa Mutant</Hit_def>
          <Hit_accession>2OL8-O</Hit_accession>
          <Hit_len>249</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>25.7942</Hsp_bit-score>
              <Hsp_score>55</Hsp_score>
              <Hsp_evalue>9.42222</Hsp_evalue>
              <Hsp_query-from>95</Hsp_query-from>
              <Hsp_query-to>189</Hsp_query-to>
              <Hsp_hit-from>132</Hsp_hit-from>
              <Hsp_hit-to>218</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>21</Hsp_identity>
              <Hsp_positive>39</Hsp_positive>
              <Hsp_gaps>8</Hsp_gaps>
              <Hsp_align-len>95</Hsp_align-len>
              <Hsp_qseq>GYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCT</Hsp_qseq>
              <Hsp_hseq>GKAKEVLKGYVLEGTLTAEKTTLVVKEGTVTLSKNISKSGEVSVELNDTDSSAATKKTAAWNSGTSTLTIT--------VNSKKTKDLVFTSSNT</Hsp_hseq>
              <Hsp_midline>G  +EV +G   E  +      L V  G  T++K + K GE S  ++D + +   +    + +    +           VN++ + D VF+   T</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>67</Hit_num>
          <Hit_id>gi|162329943|pdb|2OL7|A</Hit_id>
          <Hit_def>Chain A, The Crystal Structure Of Ospa Mutant &gt;gi|162329944|pdb|2OL7|B Chain B, The Crystal Structure Of Ospa Mutant</Hit_def>
          <Hit_accession>2OL7-A</Hit_accession>
          <Hit_len>251</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>25.7942</Hsp_bit-score>
              <Hsp_score>55</Hsp_score>
              <Hsp_evalue>9.42222</Hsp_evalue>
              <Hsp_query-from>95</Hsp_query-from>
              <Hsp_query-to>189</Hsp_query-to>
              <Hsp_hit-from>134</Hsp_hit-from>
              <Hsp_hit-to>220</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>21</Hsp_identity>
              <Hsp_positive>39</Hsp_positive>
              <Hsp_gaps>8</Hsp_gaps>
              <Hsp_align-len>95</Hsp_align-len>
              <Hsp_qseq>GYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCT</Hsp_qseq>
              <Hsp_hseq>GKAKEVLKGYVLEGTLTAEKTTLVVKEGTVTLSKNISKSGEVSVELNDTDSSAATKKTAAWNSGTSTLTIT--------VNSKKTKDLVFTSSNT</Hsp_hseq>
              <Hsp_midline>G  +EV +G   E  +      L V  G  T++K + K GE S  ++D + +   +    + +    +           VN++ + D VF+   T</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
        <Hit>
          <Hit_num>68</Hit_num>
          <Hit_id>gi|222143061|pdb|3CKG|A</Hit_id>
          <Hit_def>Chain A, The Crystal Structure Of Ospa Deletion Mutant</Hit_def>
          <Hit_accession>3CKG-A</Hit_accession>
          <Hit_len>236</Hit_len>
          <Hit_hsps>
            <Hsp>
              <Hsp_num>1</Hsp_num>
              <Hsp_bit-score>25.7942</Hsp_bit-score>
              <Hsp_score>55</Hsp_score>
              <Hsp_evalue>9.98893</Hsp_evalue>
              <Hsp_query-from>95</Hsp_query-from>
              <Hsp_query-to>189</Hsp_query-to>
              <Hsp_hit-from>119</Hsp_hit-from>
              <Hsp_hit-to>205</Hsp_hit-to>
              <Hsp_query-frame>0</Hsp_query-frame>
              <Hsp_hit-frame>0</Hsp_hit-frame>
              <Hsp_identity>21</Hsp_identity>
              <Hsp_positive>39</Hsp_positive>
              <Hsp_gaps>8</Hsp_gaps>
              <Hsp_align-len>95</Hsp_align-len>
              <Hsp_qseq>GYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCT</Hsp_qseq>
              <Hsp_hseq>GKAKEVLKGYVLEGTLTAEKTTLVVKEGTVTLSKNISKSGEVSVELNDTDSSAATKKTAAWNSGTSTLTIT--------VNSKKTKDLVFTSSNT</Hsp_hseq>
              <Hsp_midline>G  +EV +G   E  +      L V  G  T++K + K GE S  ++D + +   +    + +    +           VN++ + D VF+   T</Hsp_midline>
            </Hsp>
          </Hit_hsps>
        </Hit>
      </Iteration_hits>
      <Iteration_stat>
        <Statistics>
          <Statistics_db-num>40680</Statistics_db-num>
          <Statistics_db-len>9289891</Statistics_db-len>
          <Statistics_hsp-len>0</Statistics_hsp-len>
          <Statistics_eff-space>0</Statistics_eff-space>
          <Statistics_kappa>0.041</Statistics_kappa>
          <Statistics_lambda>0.267</Statistics_lambda>
          <Statistics_entropy>0.14</Statistics_entropy>
        </Statistics>
      </Iteration_stat>
    </Iteration>
  </BlastOutput_iterations>
</BlastOutput>
"""

XML_EBI_WU_BLAST = """<?xml version="1.0"?>
<EBIApplicationResult xmlns="http://www.ebi.ac.uk/schema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://www.ebi.ac.uk/schema/ApplicationResult.xsd">
<Header>
    <program name="WU-blastp" version="2.0MP-WashU [04-May-2006]" citation="PMID:12824421"/>
    <commandLine command="/ebi/extserv/bin/wu-blast/blastp &quot;pdb&quot; /ebi/extserv/blast-work/interactive/blast-20090331-1601383158.input E=10 B=50 V=100 -mformat=1 -matrix BLOSUM62 -sump  -filter seg -cpus 8 -sort_by_pvalue -putenv=&apos;WUBLASTMAT=/ebi/extserv/bin/wu-blast/matrix&apos; -putenv=&quot;WUBLASTDB=$IDATA_CURRENT/blastdb&quot; -putenv=&apos;WUBLASTFILTER=/ebi/extserv/bin/wu-blast/filter&apos; "/>
    <parameters>
        <sequences total="1">
            <sequence number="1" name="Sequence" type="p" length="195"/>
        </sequences>
        <databases total="1" sequences="126242" letters="31803858">
            <database number="1" name="pdb" type="p" created="2009-03-27T00:00:07+01:00"/>
        </databases>
        <scores>100</scores>
        <alignments>50</alignments>
        <matrix>BLOSUM62</matrix>
        <expectationUpper>10</expectationUpper>
        <statistics>sump</statistics>
        <filter>seg</filter>
    </parameters>
    <timeInfo start="2009-03-31T16:01:44+01:00" end="2009-03-31T16:01:45+01:00" search="PT01S"/>
</Header>
<SequenceSimilaritySearchResult>
    <hits total="50">
        <hit number="1" database="pdb" id="3ADK_A" length="195" description="mol:protein length:195  ADENYLATE KINASE">
            <alignments total="1">
                <alignment number="1">
                    <score>984</score>
                    <bits>351.4</bits>
                    <expectation>1.3e-99</expectation>
                    <probability>1.3e-99</probability>
                    <identity>100</identity>
                    <positives>100</positives>
                    <querySeq start="2" end="195">MEEKLKKSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</querySeq>
                    <pattern>MEEKLKKSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</pattern>
                    <matchSeq start="2" end="195">MEEKLKKSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="2" database="pdb" id="1Z83_A" length="196" description="mol:protein length:196  Adenylate kinase 1">
            <alignments total="1">
                <alignment number="1">
                    <score>935</score>
                    <bits>334.2</bits>
                    <expectation>2.1e-94</expectation>
                    <probability>2.1e-94</probability>
                    <identity>94</identity>
                    <positives>97</positives>
                    <querySeq start="2" end="194">MEEKLKKSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTL</querySeq>
                    <pattern>MEEKLKK+ IIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLR+EVSSGSARGK LSEIMEKGQLVPLETVLDMLRDAMVAKV+TSKGFLIDGYPREV+QGEEFER+IGQPTLLLYVDAGPETMT+RLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVD VFSQVCTHLD L</pattern>
                    <matchSeq start="2" end="194">MEEKLKKTNIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRSEVSSGSARGKKLSEIMEKGQLVPLETVLDMLRDAMVAKVNTSKGFLIDGYPREVQQGEEFERRIGQPTLLLYVDAGPETMTQRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDSVFSQVCTHLDAL</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="3" database="pdb" id="1Z83_B" length="196" description="mol:protein length:196  Adenylate kinase 1">
            <alignments total="1">
                <alignment number="1">
                    <score>935</score>
                    <bits>334.2</bits>
                    <expectation>2.1e-94</expectation>
                    <probability>2.1e-94</probability>
                    <identity>94</identity>
                    <positives>97</positives>
                    <querySeq start="2" end="194">MEEKLKKSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTL</querySeq>
                    <pattern>MEEKLKK+ IIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLR+EVSSGSARGK LSEIMEKGQLVPLETVLDMLRDAMVAKV+TSKGFLIDGYPREV+QGEEFER+IGQPTLLLYVDAGPETMT+RLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVD VFSQVCTHLD L</pattern>
                    <matchSeq start="2" end="194">MEEKLKKTNIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRSEVSSGSARGKKLSEIMEKGQLVPLETVLDMLRDAMVAKVNTSKGFLIDGYPREVQQGEEFERRIGQPTLLLYVDAGPETMTQRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDSVFSQVCTHLDAL</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="4" database="pdb" id="1Z83_C" length="196" description="mol:protein length:196  Adenylate kinase 1">
            <alignments total="1">
                <alignment number="1">
                    <score>935</score>
                    <bits>334.2</bits>
                    <expectation>2.1e-94</expectation>
                    <probability>2.1e-94</probability>
                    <identity>94</identity>
                    <positives>97</positives>
                    <querySeq start="2" end="194">MEEKLKKSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTL</querySeq>
                    <pattern>MEEKLKK+ IIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLR+EVSSGSARGK LSEIMEKGQLVPLETVLDMLRDAMVAKV+TSKGFLIDGYPREV+QGEEFER+IGQPTLLLYVDAGPETMT+RLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVD VFSQVCTHLD L</pattern>
                    <matchSeq start="2" end="194">MEEKLKKTNIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRSEVSSGSARGKKLSEIMEKGQLVPLETVLDMLRDAMVAKVNTSKGFLIDGYPREVQQGEEFERRIGQPTLLLYVDAGPETMTQRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDSVFSQVCTHLDAL</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="5" database="pdb" id="2C95_A" length="196" description="mol:protein length:196  ADENYLATE KINASE 1">
            <alignments total="1">
                <alignment number="1">
                    <score>935</score>
                    <bits>334.2</bits>
                    <expectation>2.1e-94</expectation>
                    <probability>2.1e-94</probability>
                    <identity>94</identity>
                    <positives>97</positives>
                    <querySeq start="2" end="194">MEEKLKKSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTL</querySeq>
                    <pattern>MEEKLKK+ IIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLR+EVSSGSARGK LSEIMEKGQLVPLETVLDMLRDAMVAKV+TSKGFLIDGYPREV+QGEEFER+IGQPTLLLYVDAGPETMT+RLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVD VFSQVCTHLD L</pattern>
                    <matchSeq start="2" end="194">MEEKLKKTNIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRSEVSSGSARGKKLSEIMEKGQLVPLETVLDMLRDAMVAKVNTSKGFLIDGYPREVQQGEEFERRIGQPTLLLYVDAGPETMTQRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDSVFSQVCTHLDAL</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="6" database="pdb" id="2C95_B" length="196" description="mol:protein length:196  ADENYLATE KINASE 1">
            <alignments total="1">
                <alignment number="1">
                    <score>935</score>
                    <bits>334.2</bits>
                    <expectation>2.1e-94</expectation>
                    <probability>2.1e-94</probability>
                    <identity>94</identity>
                    <positives>97</positives>
                    <querySeq start="2" end="194">MEEKLKKSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTL</querySeq>
                    <pattern>MEEKLKK+ IIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLR+EVSSGSARGK LSEIMEKGQLVPLETVLDMLRDAMVAKV+TSKGFLIDGYPREV+QGEEFER+IGQPTLLLYVDAGPETMT+RLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVD VFSQVCTHLD L</pattern>
                    <matchSeq start="2" end="194">MEEKLKKTNIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRSEVSSGSARGKKLSEIMEKGQLVPLETVLDMLRDAMVAKVNTSKGFLIDGYPREVQQGEEFERRIGQPTLLLYVDAGPETMTQRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDSVFSQVCTHLDAL</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="7" database="pdb" id="2BWJ_A" length="199" description="mol:protein length:199  ADENYLATE KINASE 5">
            <alignments total="1">
                <alignment number="1">
                    <score>605</score>
                    <bits>218.0</bits>
                    <expectation>2.0e-59</expectation>
                    <probability>2.0e-59</probability>
                    <identity>58</identity>
                    <positives>79</positives>
                    <querySeq start="4" end="194">EKLKKSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTL</querySeq>
                    <pattern>E L+K KIIF++GGPGSGKGTQCEK+V+KYG+THLSTG+LLR E++S S R K++ +IME+G LVP   VL++L++AMVA +  ++GFLIDGYPREVKQGEEF R+IG P L++ +D   +TMT RLL+   +S  VDD  +TI KRLE YY+A+ PVIA+YE +  + K+NAEG+ +DVF Q+CT +D++</pattern>
                    <matchSeq start="7" end="197">EDLRKCKIIFIIGGPGSGKGTQCEKLVEKYGFTHLSTGELLREELASESERSKLIRDIMERGDLVPSGIVLELLKEAMVASLGDTRGFLIDGYPREVKQGEEFGRRIGDPQLVICMDCSADTMTNRLLQMSRSSLPVDDTTKTIAKRLEAYYRASIPVIAYYETKTQLHKINAEGTPEDVFLQLCTAIDSI</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="8" database="pdb" id="2BWJ_B" length="199" description="mol:protein length:199  ADENYLATE KINASE 5">
            <alignments total="1">
                <alignment number="1">
                    <score>605</score>
                    <bits>218.0</bits>
                    <expectation>2.0e-59</expectation>
                    <probability>2.0e-59</probability>
                    <identity>58</identity>
                    <positives>79</positives>
                    <querySeq start="4" end="194">EKLKKSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTL</querySeq>
                    <pattern>E L+K KIIF++GGPGSGKGTQCEK+V+KYG+THLSTG+LLR E++S S R K++ +IME+G LVP   VL++L++AMVA +  ++GFLIDGYPREVKQGEEF R+IG P L++ +D   +TMT RLL+   +S  VDD  +TI KRLE YY+A+ PVIA+YE +  + K+NAEG+ +DVF Q+CT +D++</pattern>
                    <matchSeq start="7" end="197">EDLRKCKIIFIIGGPGSGKGTQCEKLVEKYGFTHLSTGELLREELASESERSKLIRDIMERGDLVPSGIVLELLKEAMVASLGDTRGFLIDGYPREVKQGEEFGRRIGDPQLVICMDCSADTMTNRLLQMSRSSLPVDDTTKTIAKRLEAYYRASIPVIAYYETKTQLHKINAEGTPEDVFLQLCTAIDSI</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="9" database="pdb" id="2BWJ_C" length="199" description="mol:protein length:199  ADENYLATE KINASE 5">
            <alignments total="1">
                <alignment number="1">
                    <score>605</score>
                    <bits>218.0</bits>
                    <expectation>2.0e-59</expectation>
                    <probability>2.0e-59</probability>
                    <identity>58</identity>
                    <positives>79</positives>
                    <querySeq start="4" end="194">EKLKKSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTL</querySeq>
                    <pattern>E L+K KIIF++GGPGSGKGTQCEK+V+KYG+THLSTG+LLR E++S S R K++ +IME+G LVP   VL++L++AMVA +  ++GFLIDGYPREVKQGEEF R+IG P L++ +D   +TMT RLL+   +S  VDD  +TI KRLE YY+A+ PVIA+YE +  + K+NAEG+ +DVF Q+CT +D++</pattern>
                    <matchSeq start="7" end="197">EDLRKCKIIFIIGGPGSGKGTQCEKLVEKYGFTHLSTGELLREELASESERSKLIRDIMERGDLVPSGIVLELLKEAMVASLGDTRGFLIDGYPREVKQGEEFGRRIGDPQLVICMDCSADTMTNRLLQMSRSSLPVDDTTKTIAKRLEAYYRASIPVIAYYETKTQLHKINAEGTPEDVFLQLCTAIDSI</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="10" database="pdb" id="2BWJ_D" length="199" description="mol:protein length:199  ADENYLATE KINASE 5">
            <alignments total="1">
                <alignment number="1">
                    <score>605</score>
                    <bits>218.0</bits>
                    <expectation>2.0e-59</expectation>
                    <probability>2.0e-59</probability>
                    <identity>58</identity>
                    <positives>79</positives>
                    <querySeq start="4" end="194">EKLKKSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTL</querySeq>
                    <pattern>E L+K KIIF++GGPGSGKGTQCEK+V+KYG+THLSTG+LLR E++S S R K++ +IME+G LVP   VL++L++AMVA +  ++GFLIDGYPREVKQGEEF R+IG P L++ +D   +TMT RLL+   +S  VDD  +TI KRLE YY+A+ PVIA+YE +  + K+NAEG+ +DVF Q+CT +D++</pattern>
                    <matchSeq start="7" end="197">EDLRKCKIIFIIGGPGSGKGTQCEKLVEKYGFTHLSTGELLREELASESERSKLIRDIMERGDLVPSGIVLELLKEAMVASLGDTRGFLIDGYPREVKQGEEFGRRIGDPQLVICMDCSADTMTNRLLQMSRSSLPVDDTTKTIAKRLEAYYRASIPVIAYYETKTQLHKINAEGTPEDVFLQLCTAIDSI</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="11" database="pdb" id="2BWJ_E" length="199" description="mol:protein length:199  ADENYLATE KINASE 5">
            <alignments total="1">
                <alignment number="1">
                    <score>605</score>
                    <bits>218.0</bits>
                    <expectation>2.0e-59</expectation>
                    <probability>2.0e-59</probability>
                    <identity>58</identity>
                    <positives>79</positives>
                    <querySeq start="4" end="194">EKLKKSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTL</querySeq>
                    <pattern>E L+K KIIF++GGPGSGKGTQCEK+V+KYG+THLSTG+LLR E++S S R K++ +IME+G LVP   VL++L++AMVA +  ++GFLIDGYPREVKQGEEF R+IG P L++ +D   +TMT RLL+   +S  VDD  +TI KRLE YY+A+ PVIA+YE +  + K+NAEG+ +DVF Q+CT +D++</pattern>
                    <matchSeq start="7" end="197">EDLRKCKIIFIIGGPGSGKGTQCEKLVEKYGFTHLSTGELLREELASESERSKLIRDIMERGDLVPSGIVLELLKEAMVASLGDTRGFLIDGYPREVKQGEEFGRRIGDPQLVICMDCSADTMTNRLLQMSRSSLPVDDTTKTIAKRLEAYYRASIPVIAYYETKTQLHKINAEGTPEDVFLQLCTAIDSI</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="12" database="pdb" id="2BWJ_F" length="199" description="mol:protein length:199  ADENYLATE KINASE 5">
            <alignments total="1">
                <alignment number="1">
                    <score>605</score>
                    <bits>218.0</bits>
                    <expectation>2.0e-59</expectation>
                    <probability>2.0e-59</probability>
                    <identity>58</identity>
                    <positives>79</positives>
                    <querySeq start="4" end="194">EKLKKSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTL</querySeq>
                    <pattern>E L+K KIIF++GGPGSGKGTQCEK+V+KYG+THLSTG+LLR E++S S R K++ +IME+G LVP   VL++L++AMVA +  ++GFLIDGYPREVKQGEEF R+IG P L++ +D   +TMT RLL+   +S  VDD  +TI KRLE YY+A+ PVIA+YE +  + K+NAEG+ +DVF Q+CT +D++</pattern>
                    <matchSeq start="7" end="197">EDLRKCKIIFIIGGPGSGKGTQCEKLVEKYGFTHLSTGELLREELASESERSKLIRDIMERGDLVPSGIVLELLKEAMVASLGDTRGFLIDGYPREVKQGEEFGRRIGDPQLVICMDCSADTMTNRLLQMSRSSLPVDDTTKTIAKRLEAYYRASIPVIAYYETKTQLHKINAEGTPEDVFLQLCTAIDSI</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="13" database="pdb" id="1UKY_A" length="203" description="mol:protein length:203  URIDYLATE KINASE">
            <alignments total="1">
                <alignment number="1">
                    <score>429</score>
                    <bits>156.1</bits>
                    <expectation>8.8e-41</expectation>
                    <probability>8.8e-41</probability>
                    <identity>47</identity>
                    <positives>69</positives>
                    <querySeq start="11" end="187">IIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVS-SGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKG-FLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQV</querySeq>
                    <pattern>+IFV+GGPG+GKGTQCEK+V+ Y + HLS GDLLRAE   +GS  G+++   +++GQ+VP E  L +LR+A+   V  +K  FLIDG+PR++ Q   FER I +   +L+ D   + M +RLL+RG+TSGR DDN E+IKKR  T+ + + PVI ++E +  V +V  + SV+DV+  V</pattern>
                    <matchSeq start="17" end="195">VIFVLGGPGAGKGTQCEKLVKDYSFVHLSAGDLLRAEQGRAGSQYGELIKNCIKEGQIVPQEITLALLRNAISDNVKANKHKFLIDGFPRKMDQAISFERDIVESKFILFFDCPEDIMLERLLERGKTSGRSDDNIESIKKRFNTFKETSMPVIEYFETKSKVVRVRCDRSVEDVYKDV</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="14" database="pdb" id="1UKZ_A" length="203" description="mol:protein length:203  URIDYLATE KINASE">
            <alignments total="1">
                <alignment number="1">
                    <score>429</score>
                    <bits>156.1</bits>
                    <expectation>8.8e-41</expectation>
                    <probability>8.8e-41</probability>
                    <identity>47</identity>
                    <positives>69</positives>
                    <querySeq start="11" end="187">IIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVS-SGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKG-FLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQV</querySeq>
                    <pattern>+IFV+GGPG+GKGTQCEK+V+ Y + HLS GDLLRAE   +GS  G+++   +++GQ+VP E  L +LR+A+   V  +K  FLIDG+PR++ Q   FER I +   +L+ D   + M +RLL+RG+TSGR DDN E+IKKR  T+ + + PVI ++E +  V +V  + SV+DV+  V</pattern>
                    <matchSeq start="17" end="195">VIFVLGGPGAGKGTQCEKLVKDYSFVHLSAGDLLRAEQGRAGSQYGELIKNCIKEGQIVPQEITLALLRNAISDNVKANKHKFLIDGFPRKMDQAISFERDIVESKFILFFDCPEDIMLERLLERGKTSGRSDDNIESIKKRFNTFKETSMPVIEYFETKSKVVRVRCDRSVEDVYKDV</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="15" database="pdb" id="1TEV_A" length="196" description="mol:protein length:196  UMP-CMP kinase">
            <alignments total="1">
                <alignment number="1">
                    <score>400</score>
                    <bits>145.9</bits>
                    <expectation>1.0e-37</expectation>
                    <probability>1.0e-37</probability>
                    <identity>40</identity>
                    <positives>67</positives>
                    <querySeq start="8" end="192">KSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSAR-GKMLSEIMEKGQLVPLETVLDMLR----DAMVAKVDTSKGFLIDGYPREVKQGEEFERKI-GQP--TLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLD</querySeq>
                    <pattern>K  ++FV+GGPG+GKGTQC +IV+KYGYTHLS G+LLR E  +  ++ G+++ + +++G++VP+E  + +L+      M A    +K FLIDG+PR     + + + + G+   + +L+ D   E   +R L+RG++SGR DDN E+++KR++TY ++T+P+I  YE+ G V+K++A  SVD+VF +V    D</pattern>
                    <matchSeq start="2" end="193">KPLVVFVLGGPGAGKGTQCARIVEKYGYTHLSAGELLRDERKNPDSQYGELIEKYIKEGKIVPVEITISLLKREMDQTMAANAQKNK-FLIDGFPRNQDNLQGWNKTMDGKADVSFVLFFDCNNEICIERCLERGKSSGRSDDNRESLEKRIQTYLQSTKPIIDLYEEMGKVKKIDASKSVDEVFDEVVQIFD</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="16" database="pdb" id="1QF9_A" length="194" description="mol:protein length:194  PROTEIN (URIDYLMONOPHOSPHATE/CYTIDYLMONOPHOSP">
            <alignments total="1">
                <alignment number="1">
                    <score>386</score>
                    <bits>140.9</bits>
                    <expectation>3.2e-36</expectation>
                    <probability>3.2e-36</probability>
                    <identity>43</identity>
                    <positives>64</positives>
                    <querySeq start="6" end="187">LKKSK--IIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQ---PTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQV</querySeq>
                    <pattern>++KSK  ++FV+GGPGSGKGTQC  IV+ +G+ HLS GDLLR E  SGS  G+M++ +++ G++VP    + +L++A+ A  +  K FL+DG+PR  +    +E  +        +L+ D   E MT+RLLKRGE+SGR DDN E+IKKR  T+   T+ VI  Y K   V+ + A   V++V++ V</pattern>
                    <matchSeq start="1" end="185">MEKSKPNVVFVLGGPGSGKGTQCANIVRDFGWVHLSAGDLLRQEQQSGSKDGEMIATMIKNGEIVPSIVTVKLLKNAIDA--NQGKNFLVDGFPRNEENNNSWEENMKDFVDTKFVLFFDCPEEVMTQRLLKRGESSGRSDDNIESIKKRFNTFNVQTKLVIDHYNKFDKVKIIPANRDVNEVYNDV</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="17" database="pdb" id="1UKE_A" length="194" description="mol:protein length:194  URIDYLMONOPHOSPHATE/CYTIDYLMONOPHOSPHATE KINA">
            <alignments total="1">
                <alignment number="1">
                    <score>386</score>
                    <bits>140.9</bits>
                    <expectation>3.2e-36</expectation>
                    <probability>3.2e-36</probability>
                    <identity>43</identity>
                    <positives>64</positives>
                    <querySeq start="6" end="187">LKKSK--IIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQ---PTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQV</querySeq>
                    <pattern>++KSK  ++FV+GGPGSGKGTQC  IV+ +G+ HLS GDLLR E  SGS  G+M++ +++ G++VP    + +L++A+ A  +  K FL+DG+PR  +    +E  +        +L+ D   E MT+RLLKRGE+SGR DDN E+IKKR  T+   T+ VI  Y K   V+ + A   V++V++ V</pattern>
                    <matchSeq start="1" end="185">MEKSKPNVVFVLGGPGSGKGTQCANIVRDFGWVHLSAGDLLRQEQQSGSKDGEMIATMIKNGEIVPSIVTVKLLKNAIDA--NQGKNFLVDGFPRNEENNNSWEENMKDFVDTKFVLFFDCPEEVMTQRLLKRGESSGRSDDNIESIKKRFNTFNVQTKLVIDHYNKFDKVKIIPANRDVNEVYNDV</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="18" database="pdb" id="2UKD_A" length="194" description="mol:protein length:194  URIDYLMONOPHOSPHATE/CYTIDYLMONOPHOSPHATE KINA">
            <alignments total="1">
                <alignment number="1">
                    <score>386</score>
                    <bits>140.9</bits>
                    <expectation>3.2e-36</expectation>
                    <probability>3.2e-36</probability>
                    <identity>43</identity>
                    <positives>64</positives>
                    <querySeq start="6" end="187">LKKSK--IIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQ---PTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQV</querySeq>
                    <pattern>++KSK  ++FV+GGPGSGKGTQC  IV+ +G+ HLS GDLLR E  SGS  G+M++ +++ G++VP    + +L++A+ A  +  K FL+DG+PR  +    +E  +        +L+ D   E MT+RLLKRGE+SGR DDN E+IKKR  T+   T+ VI  Y K   V+ + A   V++V++ V</pattern>
                    <matchSeq start="1" end="185">MEKSKPNVVFVLGGPGSGKGTQCANIVRDFGWVHLSAGDLLRQEQQSGSKDGEMIATMIKNGEIVPSIVTVKLLKNAIDA--NQGKNFLVDGFPRNEENNNSWEENMKDFVDTKFVLFFDCPEEVMTQRLLKRGESSGRSDDNIESIKKRFNTFNVQTKLVIDHYNKFDKVKIIPANRDVNEVYNDV</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="19" database="pdb" id="3UKD_A" length="194" description="mol:protein length:194  URIDYLMONOPHOSPHATE/CYTIDYLMONOPHOSPHATE KINA">
            <alignments total="1">
                <alignment number="1">
                    <score>386</score>
                    <bits>140.9</bits>
                    <expectation>3.2e-36</expectation>
                    <probability>3.2e-36</probability>
                    <identity>43</identity>
                    <positives>64</positives>
                    <querySeq start="6" end="187">LKKSK--IIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQ---PTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQV</querySeq>
                    <pattern>++KSK  ++FV+GGPGSGKGTQC  IV+ +G+ HLS GDLLR E  SGS  G+M++ +++ G++VP    + +L++A+ A  +  K FL+DG+PR  +    +E  +        +L+ D   E MT+RLLKRGE+SGR DDN E+IKKR  T+   T+ VI  Y K   V+ + A   V++V++ V</pattern>
                    <matchSeq start="1" end="185">MEKSKPNVVFVLGGPGSGKGTQCANIVRDFGWVHLSAGDLLRQEQQSGSKDGEMIATMIKNGEIVPSIVTVKLLKNAIDA--NQGKNFLVDGFPRNEENNNSWEENMKDFVDTKFVLFFDCPEEVMTQRLLKRGESSGRSDDNIESIKKRFNTFNVQTKLVIDHYNKFDKVKIIPANRDVNEVYNDV</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="20" database="pdb" id="4UKD_A" length="194" description="mol:protein length:194  URIDYLMONOPHOSPHATE/CYTIDYLMONOPHOSPHATE KINA">
            <alignments total="1">
                <alignment number="1">
                    <score>386</score>
                    <bits>140.9</bits>
                    <expectation>3.2e-36</expectation>
                    <probability>3.2e-36</probability>
                    <identity>43</identity>
                    <positives>64</positives>
                    <querySeq start="6" end="187">LKKSK--IIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQ---PTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQV</querySeq>
                    <pattern>++KSK  ++FV+GGPGSGKGTQC  IV+ +G+ HLS GDLLR E  SGS  G+M++ +++ G++VP    + +L++A+ A  +  K FL+DG+PR  +    +E  +        +L+ D   E MT+RLLKRGE+SGR DDN E+IKKR  T+   T+ VI  Y K   V+ + A   V++V++ V</pattern>
                    <matchSeq start="1" end="185">MEKSKPNVVFVLGGPGSGKGTQCANIVRDFGWVHLSAGDLLRQEQQSGSKDGEMIATMIKNGEIVPSIVTVKLLKNAIDA--NQGKNFLVDGFPRNEENNNSWEENMKDFVDTKFVLFFDCPEEVMTQRLLKRGESSGRSDDNIESIKKRFNTFNVQTKLVIDHYNKFDKVKIIPANRDVNEVYNDV</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="21" database="pdb" id="5UKD_A" length="194" description="mol:protein length:194  URIDYLMONOPHOSPHATE/CYTIDYLMONOPHOSPHATE KINA">
            <alignments total="1">
                <alignment number="1">
                    <score>386</score>
                    <bits>140.9</bits>
                    <expectation>3.2e-36</expectation>
                    <probability>3.2e-36</probability>
                    <identity>43</identity>
                    <positives>64</positives>
                    <querySeq start="6" end="187">LKKSK--IIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQ---PTLLLYVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQV</querySeq>
                    <pattern>++KSK  ++FV+GGPGSGKGTQC  IV+ +G+ HLS GDLLR E  SGS  G+M++ +++ G++VP    + +L++A+ A  +  K FL+DG+PR  +    +E  +        +L+ D   E MT+RLLKRGE+SGR DDN E+IKKR  T+   T+ VI  Y K   V+ + A   V++V++ V</pattern>
                    <matchSeq start="1" end="185">MEKSKPNVVFVLGGPGSGKGTQCANIVRDFGWVHLSAGDLLRQEQQSGSKDGEMIATMIKNGEIVPSIVTVKLLKNAIDA--NQGKNFLVDGFPRNEENNNSWEENMKDFVDTKFVLFFDCPEEVMTQRLLKRGESSGRSDDNIESIKKRFNTFNVQTKLVIDHYNKFDKVKIIPANRDVNEVYNDV</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="22" database="pdb" id="3CM0_A" length="186" description="mol:protein length:186  Adenylate kinase">
            <alignments total="1">
                <alignment number="1">
                    <score>369</score>
                    <bits>135.0</bits>
                    <expectation>2.0e-34</expectation>
                    <probability>2.0e-34</probability>
                    <identity>39</identity>
                    <positives>66</positives>
                    <querySeq start="11" end="187">IIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQP-TLLL---YVDAGPETMTKRLLKRGETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQV</querySeq>
                    <pattern>+IF+ G PG+GKGTQ  ++ Q+ G+  LSTGD+LR  V+ G+  G+ +  IME+G LVP + +L+++R+ +  +V      + DG+PR + Q E  +R + +  T LL    V+   E + +R+L+R E  GR DDNEET+++RLE Y + TEP++ +YE RG++++V+  G+ D+V++++</pattern>
                    <matchSeq start="7" end="180">VIFL-GPPGAGKGTQASRLAQELGFKKLSTGDILRDHVARGTPLGERVRPIMERGDLVPDDLILELIREELAERV------IFDGFPRTLAQAEALDRLLSETGTRLLGVVLVEVPEEELVRRILRRAELEGRSDDNEETVRRRLEVYREKTEPLVGYYEARGVLKRVDGLGTPDEVYARI</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="23" database="pdb" id="2QAJ_A" length="217" description="mol:protein length:217  Adenylate kinase">
            <alignments total="2">
                <alignment number="1">
                    <score>218</score>
                    <bits>81.8</bits>
                    <expectation>1.3e-31</expectation>
                    <probability>1.3e-31</probability>
                    <identity>35</identity>
                    <positives>60</positives>
                    <querySeq start="12" end="133">IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFER---KIGQPT-LLLYVDAGPETMTKRLLKR</querySeq>
                    <pattern>+ ++G PG+GKGTQ E+IV+ YG  H+STGD+ RA +   +  G      ++KG+LVP E  + ++++ +  K D  +GFL+DG+PR V Q E  E    + G+P   ++ ++   + + +RL  R</pattern>
                    <matchSeq start="3" end="127">LVLMGLPGAGKGTQGERIVEDYGIPHISTGDMFRAAMKEETPLGLEAKSYIDKGELVPDEVTIGIVKERL-GKDDCERGFLLDGFPRTVAQAEALEEILEEYGKPIDYVINIEVDKDVLMERLTGR</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>140</score>
                    <bits>54.3</bits>
                    <expectation>1.3e-31</expectation>
                    <probability>1.3e-31</probability>
                    <identity>43</identity>
                    <positives>62</positives>
                    <querySeq start="134" end="195">GETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</querySeq>
                    <pattern>GE   R DDNEET+ KRLE   K T+P++ FY ++G +  VN +  + DV++ V   L+ LK</pattern>
                    <matchSeq start="155" end="216">GELYQRADDNEETVSKRLEVNMKQTQPLLDFYSEKGYLANVNGQRDIQDVYADVKDLLEGLK</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="24" database="pdb" id="2QAJ_B" length="217" description="mol:protein length:217  Adenylate kinase">
            <alignments total="2">
                <alignment number="1">
                    <score>218</score>
                    <bits>81.8</bits>
                    <expectation>1.3e-31</expectation>
                    <probability>1.3e-31</probability>
                    <identity>35</identity>
                    <positives>60</positives>
                    <querySeq start="12" end="133">IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFER---KIGQPT-LLLYVDAGPETMTKRLLKR</querySeq>
                    <pattern>+ ++G PG+GKGTQ E+IV+ YG  H+STGD+ RA +   +  G      ++KG+LVP E  + ++++ +  K D  +GFL+DG+PR V Q E  E    + G+P   ++ ++   + + +RL  R</pattern>
                    <matchSeq start="3" end="127">LVLMGLPGAGKGTQGERIVEDYGIPHISTGDMFRAAMKEETPLGLEAKSYIDKGELVPDEVTIGIVKERL-GKDDCERGFLLDGFPRTVAQAEALEEILEEYGKPIDYVINIEVDKDVLMERLTGR</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>140</score>
                    <bits>54.3</bits>
                    <expectation>1.3e-31</expectation>
                    <probability>1.3e-31</probability>
                    <identity>43</identity>
                    <positives>62</positives>
                    <querySeq start="134" end="195">GETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</querySeq>
                    <pattern>GE   R DDNEET+ KRLE   K T+P++ FY ++G +  VN +  + DV++ V   L+ LK</pattern>
                    <matchSeq start="155" end="216">GELYQRADDNEETVSKRLEVNMKQTQPLLDFYSEKGYLANVNGQRDIQDVYADVKDLLEGLK</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="25" database="pdb" id="2P3S_A" length="217" description="mol:protein length:217  Adenylate kinase">
            <alignments total="2">
                <alignment number="1">
                    <score>218</score>
                    <bits>81.8</bits>
                    <expectation>2.1e-31</expectation>
                    <probability>2.1e-31</probability>
                    <identity>35</identity>
                    <positives>60</positives>
                    <querySeq start="12" end="133">IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFER---KIGQPT-LLLYVDAGPETMTKRLLKR</querySeq>
                    <pattern>+ ++G PG+GKGTQ E+IV+ YG  H+STGD+ RA +   +  G      ++KG+LVP E  + ++++ +  K D  +GFL+DG+PR V Q E  E    + G+P   ++ ++   + + +RL  R</pattern>
                    <matchSeq start="3" end="127">LVLMGLPGAGKGTQGERIVEDYGIPHISTGDMFRAAMKEETPLGLEAKSYIDKGELVPDEVTIGIVKERL-GKDDCERGFLLDGFPRTVAQAEALEEILEEYGKPIDYVINIEVDKDVLMERLTGR</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>138</score>
                    <bits>53.6</bits>
                    <expectation>2.1e-31</expectation>
                    <probability>2.1e-31</probability>
                    <identity>43</identity>
                    <positives>61</positives>
                    <querySeq start="134" end="195">GETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</querySeq>
                    <pattern>GE   R DDNEET+ KRLE   K T+P++ FY ++G +  VN +  + DV++ V   L  LK</pattern>
                    <matchSeq start="155" end="216">GELYQRADDNEETVSKRLEVNMKQTQPLLDFYSEKGYLANVNGQRDIQDVYADVKDLLGRLK</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="26" database="pdb" id="1P3J_A" length="217" description="mol:protein length:217  Adenylate kinase">
            <alignments total="2">
                <alignment number="1">
                    <score>218</score>
                    <bits>81.8</bits>
                    <expectation>2.7e-31</expectation>
                    <probability>2.7e-31</probability>
                    <identity>35</identity>
                    <positives>60</positives>
                    <querySeq start="12" end="133">IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFER---KIGQPT-LLLYVDAGPETMTKRLLKR</querySeq>
                    <pattern>+ ++G PG+GKGTQ E+IV+ YG  H+STGD+ RA +   +  G      ++KG+LVP E  + ++++ +  K D  +GFL+DG+PR V Q E  E    + G+P   ++ ++   + + +RL  R</pattern>
                    <matchSeq start="3" end="127">LVLMGLPGAGKGTQGERIVEDYGIPHISTGDMFRAAMKEETPLGLEAKSYIDKGELVPDEVTIGIVKERL-GKDDCERGFLLDGFPRTVAQAEALEEILEEYGKPIDYVINIEVDKDVLMERLTGR</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>137</score>
                    <bits>53.3</bits>
                    <expectation>2.7e-31</expectation>
                    <probability>2.7e-31</probability>
                    <identity>43</identity>
                    <positives>61</positives>
                    <querySeq start="134" end="195">GETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</querySeq>
                    <pattern>GE   R DDNEET+ KRLE   K T+P++ FY ++G +  VN +  + DV++ V   L  LK</pattern>
                    <matchSeq start="155" end="216">GELYQRADDNEETVSKRLEVNMKQTQPLLDFYSEKGYLANVNGQQDIQDVYADVKDLLGGLK</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="27" database="pdb" id="2EU8_A" length="216" description="mol:protein length:216  Adenylate kinase">
            <alignments total="2">
                <alignment number="1">
                    <score>218</score>
                    <bits>81.8</bits>
                    <expectation>2.7e-31</expectation>
                    <probability>2.7e-31</probability>
                    <identity>35</identity>
                    <positives>60</positives>
                    <querySeq start="12" end="133">IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFER---KIGQPT-LLLYVDAGPETMTKRLLKR</querySeq>
                    <pattern>+ ++G PG+GKGTQ E+IV+ YG  H+STGD+ RA +   +  G      ++KG+LVP E  + ++++ +  K D  +GFL+DG+PR V Q E  E    + G+P   ++ ++   + + +RL  R</pattern>
                    <matchSeq start="3" end="127">LVLMGLPGAGKGTQGERIVEDYGIPHISTGDMFRAAMKEETPLGLEAKSYIDKGELVPDEVTIGIVKERL-GKDDCERGFLLDGFPRTVAQAEALEEILEEYGKPIDYVINIEVDKDVLMERLTGR</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>137</score>
                    <bits>53.3</bits>
                    <expectation>2.7e-31</expectation>
                    <probability>2.7e-31</probability>
                    <identity>43</identity>
                    <positives>61</positives>
                    <querySeq start="134" end="195">GETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</querySeq>
                    <pattern>GE   R DDNEET+ KRLE   K T+P++ FY ++G +  VN +  + DV++ V   L  LK</pattern>
                    <matchSeq start="155" end="216">GELYQRADDNEETVSKRLEVNMKQTQPLLDFYSEKGYLANVNGQRDIQDVYADVKDLLGGLK</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="28" database="pdb" id="2EU8_B" length="216" description="mol:protein length:216  Adenylate kinase">
            <alignments total="2">
                <alignment number="1">
                    <score>218</score>
                    <bits>81.8</bits>
                    <expectation>2.7e-31</expectation>
                    <probability>2.7e-31</probability>
                    <identity>35</identity>
                    <positives>60</positives>
                    <querySeq start="12" end="133">IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFER---KIGQPT-LLLYVDAGPETMTKRLLKR</querySeq>
                    <pattern>+ ++G PG+GKGTQ E+IV+ YG  H+STGD+ RA +   +  G      ++KG+LVP E  + ++++ +  K D  +GFL+DG+PR V Q E  E    + G+P   ++ ++   + + +RL  R</pattern>
                    <matchSeq start="3" end="127">LVLMGLPGAGKGTQGERIVEDYGIPHISTGDMFRAAMKEETPLGLEAKSYIDKGELVPDEVTIGIVKERL-GKDDCERGFLLDGFPRTVAQAEALEEILEEYGKPIDYVINIEVDKDVLMERLTGR</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>137</score>
                    <bits>53.3</bits>
                    <expectation>2.7e-31</expectation>
                    <probability>2.7e-31</probability>
                    <identity>43</identity>
                    <positives>61</positives>
                    <querySeq start="134" end="195">GETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</querySeq>
                    <pattern>GE   R DDNEET+ KRLE   K T+P++ FY ++G +  VN +  + DV++ V   L  LK</pattern>
                    <matchSeq start="155" end="216">GELYQRADDNEETVSKRLEVNMKQTQPLLDFYSEKGYLANVNGQRDIQDVYADVKDLLGGLK</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="29" database="pdb" id="2ORI_A" length="216" description="mol:protein length:216  Adenylate kinase">
            <alignments total="2">
                <alignment number="1">
                    <score>218</score>
                    <bits>81.8</bits>
                    <expectation>4.3e-31</expectation>
                    <probability>4.3e-31</probability>
                    <identity>35</identity>
                    <positives>60</positives>
                    <querySeq start="12" end="133">IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFER---KIGQPT-LLLYVDAGPETMTKRLLKR</querySeq>
                    <pattern>+ ++G PG+GKGTQ E+IV+ YG  H+STGD+ RA +   +  G      ++KG+LVP E  + ++++ +  K D  +GFL+DG+PR V Q E  E    + G+P   ++ ++   + + +RL  R</pattern>
                    <matchSeq start="3" end="127">LVLMGLPGAGKGTQGERIVEDYGIPHISTGDMFRAAMKEETPLGLEAKSYIDKGELVPDEVTIGIVKERL-GKDDCERGFLLDGFPRTVAQAEALEEILEEYGKPIDYVINIEVDKDVLMERLTGR</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>135</score>
                    <bits>52.6</bits>
                    <expectation>4.3e-31</expectation>
                    <probability>4.3e-31</probability>
                    <identity>43</identity>
                    <positives>61</positives>
                    <querySeq start="134" end="195">GETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</querySeq>
                    <pattern>GE   R DDNEET+ KRLE   K T+P++ FY ++G +  VN +  + DV++ V   L  LK</pattern>
                    <matchSeq start="155" end="216">GELYQRADDNEETVSKRLEVNMKQTQPLLDFYSEKGYLVNVNGQRDIQDVYADVKDLLGGLK</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="30" database="pdb" id="2ORI_B" length="216" description="mol:protein length:216  Adenylate kinase">
            <alignments total="2">
                <alignment number="1">
                    <score>218</score>
                    <bits>81.8</bits>
                    <expectation>4.3e-31</expectation>
                    <probability>4.3e-31</probability>
                    <identity>35</identity>
                    <positives>60</positives>
                    <querySeq start="12" end="133">IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFER---KIGQPT-LLLYVDAGPETMTKRLLKR</querySeq>
                    <pattern>+ ++G PG+GKGTQ E+IV+ YG  H+STGD+ RA +   +  G      ++KG+LVP E  + ++++ +  K D  +GFL+DG+PR V Q E  E    + G+P   ++ ++   + + +RL  R</pattern>
                    <matchSeq start="3" end="127">LVLMGLPGAGKGTQGERIVEDYGIPHISTGDMFRAAMKEETPLGLEAKSYIDKGELVPDEVTIGIVKERL-GKDDCERGFLLDGFPRTVAQAEALEEILEEYGKPIDYVINIEVDKDVLMERLTGR</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>135</score>
                    <bits>52.6</bits>
                    <expectation>4.3e-31</expectation>
                    <probability>4.3e-31</probability>
                    <identity>43</identity>
                    <positives>61</positives>
                    <querySeq start="134" end="195">GETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</querySeq>
                    <pattern>GE   R DDNEET+ KRLE   K T+P++ FY ++G +  VN +  + DV++ V   L  LK</pattern>
                    <matchSeq start="155" end="216">GELYQRADDNEETVSKRLEVNMKQTQPLLDFYSEKGYLVNVNGQRDIQDVYADVKDLLGGLK</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="31" database="pdb" id="2OO7_A" length="217" description="mol:protein length:217  Adenylate kinase">
            <alignments total="2">
                <alignment number="1">
                    <score>218</score>
                    <bits>81.8</bits>
                    <expectation>1.1e-30</expectation>
                    <probability>1.1e-30</probability>
                    <identity>35</identity>
                    <positives>60</positives>
                    <querySeq start="12" end="133">IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFER---KIGQPT-LLLYVDAGPETMTKRLLKR</querySeq>
                    <pattern>+ ++G PG+GKGTQ E+IV+ YG  H+STGD+ RA +   +  G      ++KG+LVP E  + ++++ +  K D  +GFL+DG+PR V Q E  E    + G+P   ++ ++   + + +RL  R</pattern>
                    <matchSeq start="3" end="127">LVLMGLPGAGKGTQGERIVEDYGIPHISTGDMFRAAMKEETPLGLEAKSYIDKGELVPDEVTIGIVKERL-GKDDCERGFLLDGFPRTVAQAEALEEILEEYGKPIDYVINIEVDKDVLMERLTGR</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>131</score>
                    <bits>51.2</bits>
                    <expectation>1.1e-30</expectation>
                    <probability>1.1e-30</probability>
                    <identity>41</identity>
                    <positives>59</positives>
                    <querySeq start="134" end="195">GETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</querySeq>
                    <pattern>GE   R DDNEET+ KRLE   K  +P++ FY ++G +  VN +  + DV++ V   L  LK</pattern>
                    <matchSeq start="155" end="216">GELYQRADDNEETVSKRLEVNMKQIQPLLDFYSEKGYLANVNGQRDIQDVYADVKDLLGGLK</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="32" database="pdb" id="2OO7_B" length="217" description="mol:protein length:217  Adenylate kinase">
            <alignments total="2">
                <alignment number="1">
                    <score>218</score>
                    <bits>81.8</bits>
                    <expectation>1.1e-30</expectation>
                    <probability>1.1e-30</probability>
                    <identity>35</identity>
                    <positives>60</positives>
                    <querySeq start="12" end="133">IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFER---KIGQPT-LLLYVDAGPETMTKRLLKR</querySeq>
                    <pattern>+ ++G PG+GKGTQ E+IV+ YG  H+STGD+ RA +   +  G      ++KG+LVP E  + ++++ +  K D  +GFL+DG+PR V Q E  E    + G+P   ++ ++   + + +RL  R</pattern>
                    <matchSeq start="3" end="127">LVLMGLPGAGKGTQGERIVEDYGIPHISTGDMFRAAMKEETPLGLEAKSYIDKGELVPDEVTIGIVKERL-GKDDCERGFLLDGFPRTVAQAEALEEILEEYGKPIDYVINIEVDKDVLMERLTGR</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>131</score>
                    <bits>51.2</bits>
                    <expectation>1.1e-30</expectation>
                    <probability>1.1e-30</probability>
                    <identity>41</identity>
                    <positives>59</positives>
                    <querySeq start="134" end="195">GETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</querySeq>
                    <pattern>GE   R DDNEET+ KRLE   K  +P++ FY ++G +  VN +  + DV++ V   L  LK</pattern>
                    <matchSeq start="155" end="216">GELYQRADDNEETVSKRLEVNMKQIQPLLDFYSEKGYLANVNGQRDIQDVYADVKDLLGGLK</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="33" database="pdb" id="2OSB_A" length="216" description="mol:protein length:216  Adenylate kinase">
            <alignments total="2">
                <alignment number="1">
                    <score>211</score>
                    <bits>79.3</bits>
                    <expectation>1.4e-30</expectation>
                    <probability>1.4e-30</probability>
                    <identity>34</identity>
                    <positives>59</positives>
                    <querySeq start="12" end="133">IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFER---KIGQPT-LLLYVDAGPETMTKRLLKR</querySeq>
                    <pattern>+ ++G PG+GKGT  E+IV+ YG  H+STGD+ RA +   +  G      ++KG+LVP E  + ++++ +  K D  +GFL+DG+PR V Q E  E    + G+P   ++ ++   + + +RL  R</pattern>
                    <matchSeq start="3" end="127">LVLMGLPGAGKGTLGERIVEDYGIPHISTGDMFRAAMKEETPLGLEAKSYIDKGELVPDEVTIGIVKERL-GKDDCERGFLLDGFPRTVAQAEALEEILEEYGKPIDYVINIEVDKDVLMERLTGR</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>137</score>
                    <bits>53.3</bits>
                    <expectation>1.4e-30</expectation>
                    <probability>1.4e-30</probability>
                    <identity>43</identity>
                    <positives>61</positives>
                    <querySeq start="134" end="195">GETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</querySeq>
                    <pattern>GE   R DDNEET+ KRLE   K T+P++ FY ++G +  VN +  + DV++ V   L  LK</pattern>
                    <matchSeq start="155" end="216">GELYQRADDNEETVSKRLEVNMKQTQPLLDFYSEKGYLANVNGQRDIQDVYADVKDLLGGLK</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="34" database="pdb" id="2OSB_B" length="216" description="mol:protein length:216  Adenylate kinase">
            <alignments total="2">
                <alignment number="1">
                    <score>211</score>
                    <bits>79.3</bits>
                    <expectation>1.4e-30</expectation>
                    <probability>1.4e-30</probability>
                    <identity>34</identity>
                    <positives>59</positives>
                    <querySeq start="12" end="133">IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFER---KIGQPT-LLLYVDAGPETMTKRLLKR</querySeq>
                    <pattern>+ ++G PG+GKGT  E+IV+ YG  H+STGD+ RA +   +  G      ++KG+LVP E  + ++++ +  K D  +GFL+DG+PR V Q E  E    + G+P   ++ ++   + + +RL  R</pattern>
                    <matchSeq start="3" end="127">LVLMGLPGAGKGTLGERIVEDYGIPHISTGDMFRAAMKEETPLGLEAKSYIDKGELVPDEVTIGIVKERL-GKDDCERGFLLDGFPRTVAQAEALEEILEEYGKPIDYVINIEVDKDVLMERLTGR</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>137</score>
                    <bits>53.3</bits>
                    <expectation>1.4e-30</expectation>
                    <probability>1.4e-30</probability>
                    <identity>43</identity>
                    <positives>61</positives>
                    <querySeq start="134" end="195">GETSGRVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQVCTHLDTLK</querySeq>
                    <pattern>GE   R DDNEET+ KRLE   K T+P++ FY ++G +  VN +  + DV++ V   L  LK</pattern>
                    <matchSeq start="155" end="216">GELYQRADDNEETVSKRLEVNMKQTQPLLDFYSEKGYLANVNGQRDIQDVYADVKDLLGGLK</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="35" database="pdb" id="3BE4_A" length="217" description="mol:protein length:217  Adenylate kinase">
            <alignments total="2">
                <alignment number="1">
                    <score>218</score>
                    <bits>81.8</bits>
                    <expectation>8.6e-29</expectation>
                    <probability>8.6e-29</probability>
                    <identity>37</identity>
                    <positives>57</positives>
                    <querySeq start="7" end="140">KKSKIIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFER---KIGQP-TLLLYVDAGPETMTKRLLKR--GETSGRV</querySeq>
                    <pattern>KK  +I ++G PGSGKGTQCE I ++YG  HLSTGD+LR  + +G+  G     I+E G  V  E VL ++++     V  + GF++DG+PR + Q E   +   +IG   T ++Y +     + +R+  R     SGR+</pattern>
                    <matchSeq start="4" end="141">KKHNLI-LIGAPGSGKGTQCEFIKKEYGLAHLSTGDMLREAIKNGTKIGLEAKSIIESGNFVGDEIVLGLVKEKFDLGVCVN-GFVLDGFPRTIPQAEGLAKILSEIGDSLTSVIYFEIDDSEIIERISGRCTHPASGRI</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>113</score>
                    <bits>44.8</bits>
                    <expectation>8.6e-29</expectation>
                    <probability>8.6e-29</probability>
                    <identity>42</identity>
                    <positives>67</positives>
                    <querySeq start="139" end="187">RVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQV</querySeq>
                    <pattern>R DDN E +K RL+ ++K T P++ FYE  GI+++VNA+    +V  Q+</pattern>
                    <matchSeq start="165" end="213">RDDDNAEAVKVRLDVFHKQTAPLVKFYEDLGILKRVNAKLPPKEVTEQI</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="36" database="pdb" id="2RGX_A" length="206" description="mol:protein length:206  Adenylate kinase">
            <alignments total="2">
                <alignment number="1">
                    <score>202</score>
                    <bits>76.2</bits>
                    <expectation>1.8e-28</expectation>
                    <probability>1.8e-28</probability>
                    <identity>35</identity>
                    <positives>58</positives>
                    <querySeq start="11" end="133">IIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLI-DGYPREVKQGEEFERKIGQPTL----LLYVDAGPETMTKRLLKR</querySeq>
                    <pattern>I+  +G PG+GKGTQ +++ ++ G+ H+STGD+LR  V  G+  GK   E ME+G+LVP + ++     A++ +V    G +I DG+PR VKQ E  +  + +  L    +L  +   E + +RL  R</pattern>
                    <matchSeq start="2" end="124">ILVFLGPPGAGKGTQAKRLAKEKGFVHISTGDILREAVQKGTPLGKKAKEYMERGELVPDDLII-----ALIEEVFPKHGNVIFDGFPRTVKQAEALDEMLEKKGLKVDHVLLFEVPDEVVIERLSGR</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>126</score>
                    <bits>49.4</bits>
                    <expectation>1.8e-28</expectation>
                    <probability>1.8e-28</probability>
                    <identity>48</identity>
                    <positives>71</positives>
                    <querySeq start="139" end="187">RVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQV</querySeq>
                    <pattern>R DD  E IKKRLE Y + T P+I +Y+K+GI+R ++A   V++V+ QV</pattern>
                    <matchSeq start="150" end="198">REDDKPEVIKKRLEVYREQTAPLIEYYKKKGILRIIDASKPVEEVYRQV</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="37" database="pdb" id="2RH5_A" length="206" description="mol:protein length:206  Adenylate kinase">
            <alignments total="2">
                <alignment number="1">
                    <score>202</score>
                    <bits>76.2</bits>
                    <expectation>1.8e-28</expectation>
                    <probability>1.8e-28</probability>
                    <identity>35</identity>
                    <positives>58</positives>
                    <querySeq start="11" end="133">IIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLI-DGYPREVKQGEEFERKIGQPTL----LLYVDAGPETMTKRLLKR</querySeq>
                    <pattern>I+  +G PG+GKGTQ +++ ++ G+ H+STGD+LR  V  G+  GK   E ME+G+LVP + ++     A++ +V    G +I DG+PR VKQ E  +  + +  L    +L  +   E + +RL  R</pattern>
                    <matchSeq start="2" end="124">ILVFLGPPGAGKGTQAKRLAKEKGFVHISTGDILREAVQKGTPLGKKAKEYMERGELVPDDLII-----ALIEEVFPKHGNVIFDGFPRTVKQAEALDEMLEKKGLKVDHVLLFEVPDEVVIERLSGR</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>126</score>
                    <bits>49.4</bits>
                    <expectation>1.8e-28</expectation>
                    <probability>1.8e-28</probability>
                    <identity>48</identity>
                    <positives>71</positives>
                    <querySeq start="139" end="187">RVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQV</querySeq>
                    <pattern>R DD  E IKKRLE Y + T P+I +Y+K+GI+R ++A   V++V+ QV</pattern>
                    <matchSeq start="150" end="198">REDDKPEVIKKRLEVYREQTAPLIEYYKKKGILRIIDASKPVEEVYRQV</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="38" database="pdb" id="2RH5_C" length="206" description="mol:protein length:206  Adenylate kinase">
            <alignments total="2">
                <alignment number="1">
                    <score>202</score>
                    <bits>76.2</bits>
                    <expectation>1.8e-28</expectation>
                    <probability>1.8e-28</probability>
                    <identity>35</identity>
                    <positives>58</positives>
                    <querySeq start="11" end="133">IIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLI-DGYPREVKQGEEFERKIGQPTL----LLYVDAGPETMTKRLLKR</querySeq>
                    <pattern>I+  +G PG+GKGTQ +++ ++ G+ H+STGD+LR  V  G+  GK   E ME+G+LVP + ++     A++ +V    G +I DG+PR VKQ E  +  + +  L    +L  +   E + +RL  R</pattern>
                    <matchSeq start="2" end="124">ILVFLGPPGAGKGTQAKRLAKEKGFVHISTGDILREAVQKGTPLGKKAKEYMERGELVPDDLII-----ALIEEVFPKHGNVIFDGFPRTVKQAEALDEMLEKKGLKVDHVLLFEVPDEVVIERLSGR</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>126</score>
                    <bits>49.4</bits>
                    <expectation>1.8e-28</expectation>
                    <probability>1.8e-28</probability>
                    <identity>48</identity>
                    <positives>71</positives>
                    <querySeq start="139" end="187">RVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQV</querySeq>
                    <pattern>R DD  E IKKRLE Y + T P+I +Y+K+GI+R ++A   V++V+ QV</pattern>
                    <matchSeq start="150" end="198">REDDKPEVIKKRLEVYREQTAPLIEYYKKKGILRIIDASKPVEEVYRQV</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="39" database="pdb" id="2RH5_B" length="206" description="mol:protein length:206  Adenylate kinase">
            <alignments total="2">
                <alignment number="1">
                    <score>202</score>
                    <bits>76.2</bits>
                    <expectation>1.8e-28</expectation>
                    <probability>1.8e-28</probability>
                    <identity>35</identity>
                    <positives>58</positives>
                    <querySeq start="11" end="133">IIFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLI-DGYPREVKQGEEFERKIGQPTL----LLYVDAGPETMTKRLLKR</querySeq>
                    <pattern>I+  +G PG+GKGTQ +++ ++ G+ H+STGD+LR  V  G+  GK   E ME+G+LVP + ++     A++ +V    G +I DG+PR VKQ E  +  + +  L    +L  +   E + +RL  R</pattern>
                    <matchSeq start="2" end="124">ILVFLGPPGAGKGTQAKRLAKEKGFVHISTGDILREAVQKGTPLGKKAKEYMERGELVPDDLII-----ALIEEVFPKHGNVIFDGFPRTVKQAEALDEMLEKKGLKVDHVLLFEVPDEVVIERLSGR</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>126</score>
                    <bits>49.4</bits>
                    <expectation>1.8e-28</expectation>
                    <probability>1.8e-28</probability>
                    <identity>48</identity>
                    <positives>71</positives>
                    <querySeq start="139" end="187">RVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQV</querySeq>
                    <pattern>R DD  E IKKRLE Y + T P+I +Y+K+GI+R ++A   V++V+ QV</pattern>
                    <matchSeq start="150" end="198">REDDKPEVIKKRLEVYREQTAPLIEYYKKKGILRIIDASKPVEEVYRQV</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="40" database="pdb" id="1AKE_A" length="214" description="mol:protein length:214  ADENYLATE KINASE">
            <alignments total="2">
                <alignment number="1">
                    <score>238</score>
                    <bits>88.8</bits>
                    <expectation>2.3e-28</expectation>
                    <probability>2.3e-28</probability>
                    <identity>39</identity>
                    <positives>60</positives>
                    <querySeq start="12" end="140">IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRG--ETSGRV</querySeq>
                    <pattern>I ++G PG+GKGTQ + I++KYG   +STGD+LRA V SGS  GK   +IM+ G+LV  E V+ ++++  +A+ D   GFL+DG+PR + Q +  +        +L  D   E +  R++ R     SGRV</pattern>
                    <matchSeq start="3" end="132">IILLGAPGAGKGTQAQFIMEKYGIPQISTGDMLRAAVKSGSELGKQAKDIMDAGKLVTDELVIALVKER-IAQEDCRNGFLLDGFPRTIPQADAMKEAGINVDYVLEFDVPDELIVDRIVGRRVHAPSGRV</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>89</score>
                    <bits>36.4</bits>
                    <expectation>2.3e-28</expectation>
                    <probability>2.3e-28</probability>
                    <identity>48</identity>
                    <positives>69</positives>
                    <querySeq start="135" end="167">ETSGRVDDNEETIKKRLETYYKATEPVIAFYEK</querySeq>
                    <pattern>E + R DD EET++KRL  Y++ T P+I +Y K</pattern>
                    <matchSeq start="152" end="184">ELTTRKDDQEETVRKRLVEYHQMTAPLIGYYSK</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="41" database="pdb" id="1AKE_B" length="214" description="mol:protein length:214  ADENYLATE KINASE">
            <alignments total="2">
                <alignment number="1">
                    <score>238</score>
                    <bits>88.8</bits>
                    <expectation>2.3e-28</expectation>
                    <probability>2.3e-28</probability>
                    <identity>39</identity>
                    <positives>60</positives>
                    <querySeq start="12" end="140">IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRG--ETSGRV</querySeq>
                    <pattern>I ++G PG+GKGTQ + I++KYG   +STGD+LRA V SGS  GK   +IM+ G+LV  E V+ ++++  +A+ D   GFL+DG+PR + Q +  +        +L  D   E +  R++ R     SGRV</pattern>
                    <matchSeq start="3" end="132">IILLGAPGAGKGTQAQFIMEKYGIPQISTGDMLRAAVKSGSELGKQAKDIMDAGKLVTDELVIALVKER-IAQEDCRNGFLLDGFPRTIPQADAMKEAGINVDYVLEFDVPDELIVDRIVGRRVHAPSGRV</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>89</score>
                    <bits>36.4</bits>
                    <expectation>2.3e-28</expectation>
                    <probability>2.3e-28</probability>
                    <identity>48</identity>
                    <positives>69</positives>
                    <querySeq start="135" end="167">ETSGRVDDNEETIKKRLETYYKATEPVIAFYEK</querySeq>
                    <pattern>E + R DD EET++KRL  Y++ T P+I +Y K</pattern>
                    <matchSeq start="152" end="184">ELTTRKDDQEETVRKRLVEYHQMTAPLIGYYSK</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="42" database="pdb" id="1ANK_A" length="214" description="mol:protein length:214  ADENYLATE KINASE">
            <alignments total="2">
                <alignment number="1">
                    <score>238</score>
                    <bits>88.8</bits>
                    <expectation>2.3e-28</expectation>
                    <probability>2.3e-28</probability>
                    <identity>39</identity>
                    <positives>60</positives>
                    <querySeq start="12" end="140">IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRG--ETSGRV</querySeq>
                    <pattern>I ++G PG+GKGTQ + I++KYG   +STGD+LRA V SGS  GK   +IM+ G+LV  E V+ ++++  +A+ D   GFL+DG+PR + Q +  +        +L  D   E +  R++ R     SGRV</pattern>
                    <matchSeq start="3" end="132">IILLGAPGAGKGTQAQFIMEKYGIPQISTGDMLRAAVKSGSELGKQAKDIMDAGKLVTDELVIALVKER-IAQEDCRNGFLLDGFPRTIPQADAMKEAGINVDYVLEFDVPDELIVDRIVGRRVHAPSGRV</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>89</score>
                    <bits>36.4</bits>
                    <expectation>2.3e-28</expectation>
                    <probability>2.3e-28</probability>
                    <identity>48</identity>
                    <positives>69</positives>
                    <querySeq start="135" end="167">ETSGRVDDNEETIKKRLETYYKATEPVIAFYEK</querySeq>
                    <pattern>E + R DD EET++KRL  Y++ T P+I +Y K</pattern>
                    <matchSeq start="152" end="184">ELTTRKDDQEETVRKRLVEYHQMTAPLIGYYSK</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="43" database="pdb" id="1ANK_B" length="214" description="mol:protein length:214  ADENYLATE KINASE">
            <alignments total="2">
                <alignment number="1">
                    <score>238</score>
                    <bits>88.8</bits>
                    <expectation>2.3e-28</expectation>
                    <probability>2.3e-28</probability>
                    <identity>39</identity>
                    <positives>60</positives>
                    <querySeq start="12" end="140">IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRG--ETSGRV</querySeq>
                    <pattern>I ++G PG+GKGTQ + I++KYG   +STGD+LRA V SGS  GK   +IM+ G+LV  E V+ ++++  +A+ D   GFL+DG+PR + Q +  +        +L  D   E +  R++ R     SGRV</pattern>
                    <matchSeq start="3" end="132">IILLGAPGAGKGTQAQFIMEKYGIPQISTGDMLRAAVKSGSELGKQAKDIMDAGKLVTDELVIALVKER-IAQEDCRNGFLLDGFPRTIPQADAMKEAGINVDYVLEFDVPDELIVDRIVGRRVHAPSGRV</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>89</score>
                    <bits>36.4</bits>
                    <expectation>2.3e-28</expectation>
                    <probability>2.3e-28</probability>
                    <identity>48</identity>
                    <positives>69</positives>
                    <querySeq start="135" end="167">ETSGRVDDNEETIKKRLETYYKATEPVIAFYEK</querySeq>
                    <pattern>E + R DD EET++KRL  Y++ T P+I +Y K</pattern>
                    <matchSeq start="152" end="184">ELTTRKDDQEETVRKRLVEYHQMTAPLIGYYSK</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="44" database="pdb" id="2ECK_A" length="214" description="mol:protein length:214  ADENYLATE KINASE">
            <alignments total="2">
                <alignment number="1">
                    <score>238</score>
                    <bits>88.8</bits>
                    <expectation>2.3e-28</expectation>
                    <probability>2.3e-28</probability>
                    <identity>39</identity>
                    <positives>60</positives>
                    <querySeq start="12" end="140">IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRG--ETSGRV</querySeq>
                    <pattern>I ++G PG+GKGTQ + I++KYG   +STGD+LRA V SGS  GK   +IM+ G+LV  E V+ ++++  +A+ D   GFL+DG+PR + Q +  +        +L  D   E +  R++ R     SGRV</pattern>
                    <matchSeq start="3" end="132">IILLGAPGAGKGTQAQFIMEKYGIPQISTGDMLRAAVKSGSELGKQAKDIMDAGKLVTDELVIALVKER-IAQEDCRNGFLLDGFPRTIPQADAMKEAGINVDYVLEFDVPDELIVDRIVGRRVHAPSGRV</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>89</score>
                    <bits>36.4</bits>
                    <expectation>2.3e-28</expectation>
                    <probability>2.3e-28</probability>
                    <identity>48</identity>
                    <positives>69</positives>
                    <querySeq start="135" end="167">ETSGRVDDNEETIKKRLETYYKATEPVIAFYEK</querySeq>
                    <pattern>E + R DD EET++KRL  Y++ T P+I +Y K</pattern>
                    <matchSeq start="152" end="184">ELTTRKDDQEETVRKRLVEYHQMTAPLIGYYSK</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="45" database="pdb" id="2ECK_B" length="214" description="mol:protein length:214  ADENYLATE KINASE">
            <alignments total="2">
                <alignment number="1">
                    <score>238</score>
                    <bits>88.8</bits>
                    <expectation>2.3e-28</expectation>
                    <probability>2.3e-28</probability>
                    <identity>39</identity>
                    <positives>60</positives>
                    <querySeq start="12" end="140">IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRG--ETSGRV</querySeq>
                    <pattern>I ++G PG+GKGTQ + I++KYG   +STGD+LRA V SGS  GK   +IM+ G+LV  E V+ ++++  +A+ D   GFL+DG+PR + Q +  +        +L  D   E +  R++ R     SGRV</pattern>
                    <matchSeq start="3" end="132">IILLGAPGAGKGTQAQFIMEKYGIPQISTGDMLRAAVKSGSELGKQAKDIMDAGKLVTDELVIALVKER-IAQEDCRNGFLLDGFPRTIPQADAMKEAGINVDYVLEFDVPDELIVDRIVGRRVHAPSGRV</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>89</score>
                    <bits>36.4</bits>
                    <expectation>2.3e-28</expectation>
                    <probability>2.3e-28</probability>
                    <identity>48</identity>
                    <positives>69</positives>
                    <querySeq start="135" end="167">ETSGRVDDNEETIKKRLETYYKATEPVIAFYEK</querySeq>
                    <pattern>E + R DD EET++KRL  Y++ T P+I +Y K</pattern>
                    <matchSeq start="152" end="184">ELTTRKDDQEETVRKRLVEYHQMTAPLIGYYSK</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="46" database="pdb" id="4AKE_A" length="214" description="mol:protein length:214  ADENYLATE KINASE">
            <alignments total="2">
                <alignment number="1">
                    <score>238</score>
                    <bits>88.8</bits>
                    <expectation>2.3e-28</expectation>
                    <probability>2.3e-28</probability>
                    <identity>39</identity>
                    <positives>60</positives>
                    <querySeq start="12" end="140">IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRG--ETSGRV</querySeq>
                    <pattern>I ++G PG+GKGTQ + I++KYG   +STGD+LRA V SGS  GK   +IM+ G+LV  E V+ ++++  +A+ D   GFL+DG+PR + Q +  +        +L  D   E +  R++ R     SGRV</pattern>
                    <matchSeq start="3" end="132">IILLGAPGAGKGTQAQFIMEKYGIPQISTGDMLRAAVKSGSELGKQAKDIMDAGKLVTDELVIALVKER-IAQEDCRNGFLLDGFPRTIPQADAMKEAGINVDYVLEFDVPDELIVDRIVGRRVHAPSGRV</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>89</score>
                    <bits>36.4</bits>
                    <expectation>2.3e-28</expectation>
                    <probability>2.3e-28</probability>
                    <identity>48</identity>
                    <positives>69</positives>
                    <querySeq start="135" end="167">ETSGRVDDNEETIKKRLETYYKATEPVIAFYEK</querySeq>
                    <pattern>E + R DD EET++KRL  Y++ T P+I +Y K</pattern>
                    <matchSeq start="152" end="184">ELTTRKDDQEETVRKRLVEYHQMTAPLIGYYSK</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="47" database="pdb" id="4AKE_B" length="214" description="mol:protein length:214  ADENYLATE KINASE">
            <alignments total="2">
                <alignment number="1">
                    <score>238</score>
                    <bits>88.8</bits>
                    <expectation>2.3e-28</expectation>
                    <probability>2.3e-28</probability>
                    <identity>39</identity>
                    <positives>60</positives>
                    <querySeq start="12" end="140">IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRG--ETSGRV</querySeq>
                    <pattern>I ++G PG+GKGTQ + I++KYG   +STGD+LRA V SGS  GK   +IM+ G+LV  E V+ ++++  +A+ D   GFL+DG+PR + Q +  +        +L  D   E +  R++ R     SGRV</pattern>
                    <matchSeq start="3" end="132">IILLGAPGAGKGTQAQFIMEKYGIPQISTGDMLRAAVKSGSELGKQAKDIMDAGKLVTDELVIALVKER-IAQEDCRNGFLLDGFPRTIPQADAMKEAGINVDYVLEFDVPDELIVDRIVGRRVHAPSGRV</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>89</score>
                    <bits>36.4</bits>
                    <expectation>2.3e-28</expectation>
                    <probability>2.3e-28</probability>
                    <identity>48</identity>
                    <positives>69</positives>
                    <querySeq start="135" end="167">ETSGRVDDNEETIKKRLETYYKATEPVIAFYEK</querySeq>
                    <pattern>E + R DD EET++KRL  Y++ T P+I +Y K</pattern>
                    <matchSeq start="152" end="184">ELTTRKDDQEETVRKRLVEYHQMTAPLIGYYSK</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="48" database="pdb" id="1E4V_A" length="214" description="mol:protein length:214  ADENYLATE KINASE">
            <alignments total="2">
                <alignment number="1">
                    <score>229</score>
                    <bits>85.7</bits>
                    <expectation>2.0e-27</expectation>
                    <probability>2.0e-27</probability>
                    <identity>38</identity>
                    <positives>59</positives>
                    <querySeq start="12" end="140">IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRG--ETSGRV</querySeq>
                    <pattern>I ++G P +GKGTQ + I++KYG   +STGD+LRA V SGS  GK   +IM+ G+LV  E V+ ++++  +A+ D   GFL+DG+PR + Q +  +        +L  D   E +  R++ R     SGRV</pattern>
                    <matchSeq start="3" end="132">IILLGAPVAGKGTQAQFIMEKYGIPQISTGDMLRAAVKSGSELGKQAKDIMDAGKLVTDELVIALVKER-IAQEDCRNGFLLDGFPRTIPQADAMKEAGINVDYVLEFDVPDELIVDRIVGRRVHAPSGRV</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>89</score>
                    <bits>36.4</bits>
                    <expectation>2.0e-27</expectation>
                    <probability>2.0e-27</probability>
                    <identity>48</identity>
                    <positives>69</positives>
                    <querySeq start="135" end="167">ETSGRVDDNEETIKKRLETYYKATEPVIAFYEK</querySeq>
                    <pattern>E + R DD EET++KRL  Y++ T P+I +Y K</pattern>
                    <matchSeq start="152" end="184">ELTTRKDDQEETVRKRLVEYHQMTAPLIGYYSK</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="49" database="pdb" id="1E4V_B" length="214" description="mol:protein length:214  ADENYLATE KINASE">
            <alignments total="2">
                <alignment number="1">
                    <score>229</score>
                    <bits>85.7</bits>
                    <expectation>2.0e-27</expectation>
                    <probability>2.0e-27</probability>
                    <identity>38</identity>
                    <positives>59</positives>
                    <querySeq start="12" end="140">IFVVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGEEFERKIGQPTLLLYVDAGPETMTKRLLKRG--ETSGRV</querySeq>
                    <pattern>I ++G P +GKGTQ + I++KYG   +STGD+LRA V SGS  GK   +IM+ G+LV  E V+ ++++  +A+ D   GFL+DG+PR + Q +  +        +L  D   E +  R++ R     SGRV</pattern>
                    <matchSeq start="3" end="132">IILLGAPVAGKGTQAQFIMEKYGIPQISTGDMLRAAVKSGSELGKQAKDIMDAGKLVTDELVIALVKER-IAQEDCRNGFLLDGFPRTIPQADAMKEAGINVDYVLEFDVPDELIVDRIVGRRVHAPSGRV</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>89</score>
                    <bits>36.4</bits>
                    <expectation>2.0e-27</expectation>
                    <probability>2.0e-27</probability>
                    <identity>48</identity>
                    <positives>69</positives>
                    <querySeq start="135" end="167">ETSGRVDDNEETIKKRLETYYKATEPVIAFYEK</querySeq>
                    <pattern>E + R DD EET++KRL  Y++ T P+I +Y K</pattern>
                    <matchSeq start="152" end="184">ELTTRKDDQEETVRKRLVEYHQMTAPLIGYYSK</matchSeq>
                </alignment>
            </alignments>
        </hit>
        <hit number="50" database="pdb" id="1AK2_A" length="233" description="mol:protein length:233  ADENYLATE KINASE ISOENZYME-2">
            <alignments total="2">
                <alignment number="1">
                    <score>202</score>
                    <bits>76.2</bits>
                    <expectation>1.1e-26</expectation>
                    <probability>1.1e-26</probability>
                    <identity>45</identity>
                    <positives>64</positives>
                    <querySeq start="14" end="104">VVGGPGSGKGTQCEKIVQKYGYTHLSTGDLLRAEVSSGSARGKMLSEIMEKGQLVPLETVLDMLRDAMVAKVDTSKGFLIDGYPREVKQGE</querySeq>
                    <pattern>++G PG+GKGTQ  K+ + +   HL+TGD+LRA V+SGS  GK L   M+ G+LV  E VL+++   +        GFL+DG+PR V+Q E</pattern>
                    <matchSeq start="21" end="110">LLGPPGAGKGTQAPKLAKNFCVCHLATGDMLRAMVASGSELGKKLKATMDAGKLVSDEMVLELIEKNLETP-PCKNGFLLDGFPRTVRQAE</matchSeq>
                </alignment>
                <alignment number="2">
                    <score>109</score>
                    <bits>43.4</bits>
                    <expectation>1.1e-26</expectation>
                    <probability>1.1e-26</probability>
                    <identity>40</identity>
                    <positives>65</positives>
                    <querySeq start="139" end="187">RVDDNEETIKKRLETYYKATEPVIAFYEKRGIVRKVNAEGSVDDVFSQV</querySeq>
                    <pattern>R DDN++ +K RLE Y+  T P++ +Y KRGI   ++A  + D VF+ +</pattern>
                    <matchSeq start="176" end="224">RSDDNKKALKIRLEAYHTQTTPLVEYYSKRGIHSAIDASQTPDVVFASI</matchSeq>
                </alignment>
            </alignments>
        </hit>
    </hits>
</SequenceSimilaritySearchResult>
</EBIApplicationResult>
"""

XML_CATH_2CHL = """<?xml version="1.0" encoding="ISO-8859-1"?>
<document>
  <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns="http://www.cathdb.info">
   <rdf:Description rdf:about="self">
    <dc:title>CATH PDB Query '2chl'</dc:title>
    <dc:description>CATH PDB Query</dc:description>
    <dc:identifier>http://www.cathdb.info/pdb/2chl</dc:identifier>
    <dc:date>15:33:16 20-Dec-2011</dc:date>
    <dc:type>Dataset</dc:type>
    <dc:format>application/xml</dc:format>
   </rdf:Description>
  </rdf:RDF>
  <cath_pdb_query pdb_code="2chl" chain_code="" domain_code="" cath_domain_count="2">    <cath_domain domain_id="2chlA01" pdb_id="2chl" chain_code="A" domain_code="01" dom_length="84" dom_atom_sequence="VLMVGPNFRVGKKIGCGNFGELRLGKNLYTNEYVAIKLEPMKSRAPQLHLEYRFYKQLGSGDGIPQVYYFGPCGKYNAMVLELL" dom_combs_sequence="VLMVGPNFRVGKKIGCGNFGELRLGKNLYTNEYVAIKLEPMKSRAPQLHLEYRFYKQLGSGDGIPQVYYFGPCGKYNAMVLELL">
        <cath_code class_code="3" arch_code="30" top_code="200" homol_code="20" s35_code="5" s60_code="1" s95_code="1" s100_code="1" s100_count="3">3.30.200.20.5.1.1.1.3</cath_code>

      <segments seg_count="1">            <segment seg_num="1" pdb_start="25" pdb_stop="108" slength="84" sseqs="VLMVGPNFRVGKKIGCGNFGELRLGKNLYTNEYVAIKLEPMKSRAPQLHLEYRFYKQLGSGDGIPQVYYFGPCGKYNAMVLELL"/>    </segments>
  </cath_domain>    <cath_domain domain_id="2chlA02" pdb_id="2chl" chain_code="A" domain_code="02" dom_length="214" dom_atom_sequence="GPSLEDLFDLCDRTFSLKTVLMIAIQLISRMEYVHSKNLIYRDVKPENFLIGRPGNKTQQVIHIIDFALAKEYIDPETKKHIPYREHKSLTGTARYMSINTHLGKEQSRRDDLEALGHMFMYFLRGSLPWQGLKADTLKERYQKIGDTKRATPIEVLCENFPEMATYLRYVRRLDFFEKPDYDYLRKLFTDLFDRKGYMFDYEYDWIGKQLPTP" dom_combs_sequence="GPSLEDLFDLCDRTFSLKTVLMIAIQLISRMEYVHSKNLIYRDVKPENFLIGRPGNKTQQVIHIIDFALAKEYIDPETKKHIPYREHKSLTGTARYMSINTHLGKEQSRRDDLEALGHMFMYFLRGSLPWQGLKADTLKERYQKIGDTKRATPIEVLCENFPEMATYLRYVRRLDFFEKPDYDYLRKLFTDLFDRKGYMFDYEYDWIGKQLPTP">
        <cath_code class_code="1" arch_code="10" top_code="510" homol_code="10" s35_code="8" s60_code="1" s95_code="1" s100_code="1" s100_count="3">1.10.510.10.8.1.1.1.3</cath_code>

      <segments seg_count="1">            <segment seg_num="1" pdb_start="109" pdb_stop="322" slength="214" sseqs="GPSLEDLFDLCDRTFSLKTVLMIAIQLISRMEYVHSKNLIYRDVKPENFLIGRPGNKTQQVIHIIDFALAKEYIDPETKKHIPYREHKSLTGTARYMSINTHLGKEQSRRDDLEALGHMFMYFLRGSLPWQGLKADTLKERYQKIGDTKRATPIEVLCENFPEMATYLRYVRRLDFFEKPDYDYLRKLFTDLFDRKGYMFDYEYDWIGKQLPTP"/>    </segments>
  </cath_domain>  </cath_pdb_query>
</document>
"""

suite_ncbi_blast_xml_parse = unittest.TestLoader().loadTestsFromTestCase(
    TestNCBIBlastXMLParse
    )
suite_get_ncbi_blast_parser = unittest.TestLoader().loadTestsFromTestCase(
    TestGetNCBIParser
    )
suite_wu_blast_xml_parse = unittest.TestLoader().loadTestsFromTestCase(
    TestWUBlastXMLParse
    )
suite_cath_annotation = unittest.TestLoader().loadTestsFromTestCase(
    TestCathXMLParse
    )

alltests = unittest.TestSuite(
    [
        suite_ncbi_blast_xml_parse,
        suite_get_ncbi_blast_parser,
        suite_wu_blast_xml_parse,
        suite_cath_annotation,
        ]
    )

if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
