from phaser import template_new as template
from phaser.pipeline.scaffolding import IterableCompareTestCase, Fake

from cctbx import sgtbx, uctbx
from scitbx.array_family import flex
import scitbx.matrix
import scitbx.math
import iotbx.pdb
import libtbx.load_env

import os.path
import unittest
import math

ROOT = libtbx.env.under_dist( "phaser_regression", "data" )

LYSO = iotbx.pdb.input(
    os.path.join( ROOT, "regression", "1fkq_prot.pdb" )
    ) \
    .construct_hierarchy()

BARNASE = iotbx.pdb.input(
    os.path.join( ROOT, "regression", "1brs_barnase_A.pdb" )
    ) \
    .construct_hierarchy()

BARSTAR = iotbx.pdb.input(
    os.path.join( ROOT, "regression", "1brs_barstar_D.pdb" )
    ) \
    .construct_hierarchy()

class TestEnsembleInfo(IterableCompareTestCase):

    def test_lysozyme(self):

        einfo = template.EnsembleInfo( root = LYSO )
        self.assertCoordinatesAlmostEqual(
            [ einfo.centre ],
            [ (20.388778895624363, 24.646063848502582, 29.016722161047426) ],
            7
            )
        self.assertAlmostEqual( einfo.extent, 18.21346792029803 )
        self.assertEqual( len( einfo.pgops ), 1 )
        self.assertIterablesAlmostEqual(
            einfo.pgops[0].r.elems,
            ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ),
            7
            )
        self.assertIterablesAlmostEqual(
            einfo.pgops[0].t.elems,
            ( 0, 0, 0 ),
            7
            )


class TestOrthogonalDistancesBetween(IterableCompareTestCase):

    def test_lysozyme(self):

        ensemble = Fake()
        ensemble.centre = scitbx.matrix.col(
            ( 20.388778895624394, 24.6460638485026, 29.01672216104745 )
            )
        space_group = sgtbx.space_group_info( symbol = "P43212" ).group()
        cell = uctbx.unit_cell( ( 78.11, 78.11, 37.01, 90, 90, 90 ) )
        cinfo = template.CrystalInfo( space_group = space_group, cell = cell, dmin = 2.5 )
        left = template.PeakInfo(
            transformation = scitbx.matrix.rt(
                ( ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ), ( 0, 0, 0 ) )
                ),
            ensemble = ensemble,
            cell = cell
            )
        distances = [ 0.0, 36.49997599988679, 36.49997599988679, 42.05528431383889,
            39.66872491292646, 50.66178076565818, 17.082708005004257, 46.84308504795964 ]

        # Identical
        self.assertIterablesAlmostEqual(
            [ scitbx.matrix.col( cinfo.cell.orthogonalize( p[2] ) ).length()
                for p in template.orthogonal_distances_between(
                    left = left.centre_frac,
                    right = left.centre_frac,
                    crystal = cinfo
                    )
            ],
            distances,
            7
            )

        # Integer unit cell shifts
        right = template.PeakInfo(
            transformation = scitbx.matrix.rt(
                ( ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ), ( 3 * 78.11, 2 * 78.11, 5 * 37.01 ) )
                ),
            ensemble = ensemble,
            cell = cell
            )
        self.assertIterablesAlmostEqual(
            [ scitbx.matrix.col( cinfo.cell.orthogonalize( p[2] ) ).length()
                for p in template.orthogonal_distances_between(
                    left = left.centre_frac,
                    right = right.centre_frac,
                    crystal = cinfo
                    )
            ],
            distances,
            7
            )

        # Approximately integer unit cell shifts
        right = template.PeakInfo(
            transformation = scitbx.matrix.rt(
                ( ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ), ( 3 * 78.1105, 2 * 78.1105, 5 * 37.0105 ) )
                ),
            ensemble = ensemble,
            cell = cell
            )
        self.assertIterablesAlmostEqual(
            [ scitbx.matrix.col( cinfo.cell.orthogonalize( p[2] ) ).length()
                for p in template.orthogonal_distances_between(
                    left = left.centre_frac,
                    right = right.centre_frac,
                    crystal = cinfo
                    )
            ],
            distances,
            2
            )

        # Approximately integer unit cell shifts + crystal symmetry
        right = template.PeakInfo(
            transformation = scitbx.matrix.rt(
                ( ( 0, -1, 0, 1, 0, 0, 0, 0, 1 ), ( 3.5 * 78.1105, 2.5 * 78.1105, 5.75 * 37.0105 ) )
                ),
            ensemble = ensemble,
            cell = cell
            )

        tdistances = [
            distances[1],
            distances[5],
            distances[0],
            distances[7],
            distances[6],
            distances[2],
            distances[3],
            distances[4],
            ]
        self.assertIterablesAlmostEqual(
            [ scitbx.matrix.col( cinfo.cell.orthogonalize( p[2] ) ).length()
                for p in template.orthogonal_distances_between(
                    left = left.centre_frac,
                    right = right.centre_frac,
                    crystal = cinfo
                    )
            ],
            tdistances,
            2
            )


class TestRotationDifferences(IterableCompareTestCase):

    def test_lysozyme(self):

        einfo = Fake()
        einfo.centre = scitbx.matrix.col(
            ( 20.388778895624394, 24.6460638485026, 29.01672216104745 )
            )
        einfo.pgops = [
            scitbx.matrix.rt( ( ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ), ( 0, 0, 0 ) ) ),
            scitbx.matrix.rt( ( ( 0, -1, 0, 1, 0, 0, 0, 0, 1 ), ( 0, 0, 0 ) ) ),
            ]
        space_group = sgtbx.space_group_info( symbol = "P43212" ).group()
        cell = uctbx.unit_cell( ( 78.11, 78.11, 37.01, 90, 90, 90 ) )
        cinfo = template.CrystalInfo( space_group = space_group, cell = cell, dmin = 2.5 )
        left = template.PeakInfo(
            transformation = scitbx.matrix.rt(
                ( ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ), ( 0, 0, 0 ) )
                ),
            ensemble = einfo,
            cell = cell
            )

        # Identical
        self.assertIterablesAlmostEqual(
            [ p[1] for p in template.rotation_differences_between(
                left = left.transformation.r,
                right = left.transformation.r,
                ensemble = einfo
                )
            ],
            [ 0, math.pi / 2.0 ],
            7
            )

        # Rotated
        right = template.PeakInfo(
            transformation = scitbx.matrix.rt(
                ( ( -1, 0, 0, 0, -1, 0, 0, 0, 1 ), ( 0, 0, 0 ) )
                ),
            ensemble = einfo,
            cell = cell
            )
        self.assertIterablesAlmostEqual(
            [ p[1] for p in template.rotation_differences_between(
                left = left.transformation.r,
                right = right.transformation.r,
                ensemble = einfo
                )
            ],
            [ math.pi, math.pi / 2.0 ],
            7
            )


class TestMatchTolerance(unittest.TestCase):

    def test_lysozyme(self):

        einfo = Fake()
        einfo.extent = 18.21346792029803
        cinfo = Fake()
        cinfo.dmin = 2.5
        mt = template.MatchTolerance.from_ensemble( ensemble = einfo, crystal = cinfo )
        self.assertAlmostEqual( mt.rotation, 0.06860361, 7 )
        self.assertAlmostEqual( mt.translation, 0.5, 7 )


class TestSymmetryOperatorsBetween(IterableCompareTestCase):

    def test_lysozyme(self):

        einfo = template.EnsembleInfo( root = LYSO )
        space_group = sgtbx.space_group_info( symbol = "P43212" ).group()
        cell = uctbx.unit_cell( ( 78.11, 78.11, 37.01, 90, 90, 90 ) )
        cinfo = template.CrystalInfo( space_group = space_group, cell = cell, dmin = 2.5 )
        mt = template.MatchTolerance.from_ensemble( ensemble = einfo, crystal = cinfo )

        tra = scitbx.matrix.col(
            ( flex.random_double(), flex.random_double(), flex.random_double() )
            )
        rot = scitbx.matrix.sqr( flex.random_double_r3_rotation_matrix() )
        test = template.PeakInfo(
            transformation = scitbx.matrix.rt( ( rot, tra ) ),
            ensemble = einfo,
            cell = cell
            )

        for ( index, op ) in enumerate( cinfo.sgops ):
            misset_axis = flex.random_double_point_on_sphere()
            misset_angle = mt.rotation * flex.random_double()
            misset_rot = scitbx.matrix.sqr(
                scitbx.math.r3_rotation_axis_and_angle_as_matrix(
                    misset_axis,
                    misset_angle
                    ),
                )
            misset_shift = mt.translation * flex.random_double() * scitbx.matrix.col(
                flex.random_double_point_on_sphere()
                )
            misset = scitbx.matrix.rt(
                ( misset_rot, einfo.centre - misset_rot * einfo.centre + misset_shift )
                )
            trans = test.transformation * misset
            sym = scitbx.matrix.rt(
                (
                    cinfo.orth_rotations[ index ] * trans.r,
                    cell.orthogonalize( op * cell.fractionalize( trans.t ) )
                    )
                )
            reference = template.PeakInfo(
                transformation = sym,
                ensemble = einfo,
                cell = cell
                )

            ( sgop, pgop, shift ) = template.symmetry_operators_between(
                left = reference,
                right = test,
                shift_flags = ( False, False, False ),
                crystal = cinfo,
                multiplier = 1.0
                )
            self.assertEqual( sgop, op )
            self.assertEqual( pgop, einfo.pgops[0] )
            self.assertIterablesAlmostEqual( shift, ( 0, 0, 0 ), 7 )


    def test_lactalbumin(self):

        einfo = Fake()
        einfo.centre = scitbx.matrix.col(
            ( 20.78146399228467, 24.87727987312208, 28.810993952167948 )
            )
        einfo.extent = 17.150608023113424
        einfo.pgops = [
            scitbx.matrix.rt( ( ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ), ( 0, 0, 0 ) ) ),
            ]
        space_group = sgtbx.space_group_info( symbol = "P43212" ).group()
        cell = uctbx.unit_cell( ( 78.11, 78.11, 37.01, 90, 90, 90 ) )
        cinfo = template.CrystalInfo( space_group = space_group, cell = cell, dmin = 2.5 )
        mt = template.MatchTolerance.from_ensemble( ensemble = einfo, crystal = cinfo )

        reference = template.PeakInfo.from_phaser_solution(
            rotation = scitbx.math.euler_angles_zyz_matrix( 46.854, 64.091, -42.217 ),
            translation = ( -0.298, -0.161, 0.673 ),
            ensemble = einfo,
            cell = cell
            )
        equiv = template.PeakInfo.from_phaser_solution(
            rotation = scitbx.math.euler_angles_zyz_matrix( 43.493, 115.778, 137.549 ),
            translation = ( -0.157, -0.301, -0.676 ),
            ensemble = einfo,
            cell = cell
            )
        ( sgop, pgop, shift ) = template.symmetry_operators_between(
            left = reference,
            right = equiv,
            shift_flags = ( False, False, False ),
            crystal = cinfo,
            multiplier = 1.0
            )
        self.assertEqual( sgop, cinfo.sgops[6] )
        self.assertEqual( pgop, einfo.pgops[0] )
        self.assertIterablesAlmostEqual( shift, ( 0, 0, 0 ), 7 )


class TestStructureMatching(unittest.TestCase):

    def setUp(self):

        self.einfo_barnase = template.EnsembleInfo( root = BARNASE )
        self.einfo_barstar = template.EnsembleInfo( root = BARSTAR )
        cell = uctbx.unit_cell( ( 206.24, 43.51, 83.69, 90, 107.42, 90 ) )
        space_group = sgtbx.space_group_info( symbol = "C2" ).group()
        self.cinfo = template.CrystalInfo( space_group = space_group, cell = cell, dmin = 2.5 )
        self.reference = [
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 231.647, 35.353, 45.767 ),
                translation = ( -0.22692, 0.48806, 0.08415 ),
                ensemble = self.einfo_barnase,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 39.509, 179.602, 38.411 ),
                translation = ( 0.00327, -0.59286, 0.50032 ),
                ensemble = self.einfo_barnase,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 175.890, 47.606, 4.502 ),
                translation = ( -0.03394, 1.29835, 0.03471 ),
                ensemble = self.einfo_barnase,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 105.393, 179.442, 105.579 ),
                translation = ( -0.00046, -0.60907, 0.49382 ),
                ensemble = self.einfo_barstar,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 231.028, 36.357, 44.937 ),
                translation = ( -0.22273, 1.51380, 0.09214 ),
                ensemble = self.einfo_barstar,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 180.099, 51.375, 0.533 ),
                translation = ( -0.03873, 1.28763, 0.07687 ),
                ensemble = self.einfo_barstar,
                cell = cell
                ),
            ]

        self.incomplete = [
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 231.647, 35.353, 45.767 ),
                translation = ( -0.22692, 0.48806, 0.08415 ),
                ensemble = self.einfo_barnase,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 39.509, 179.602, 38.411 ),
                translation = ( 0.00327, -0.59286, 0.50032 ),
                ensemble = self.einfo_barnase,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 175.890, 47.606, 4.502 ),
                translation = ( -0.03394, 1.29835, 0.03471 ),
                ensemble = self.einfo_barnase,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 105.393, 179.442, 105.579 ),
                translation = ( -0.00046, -0.60907, 0.49382 ),
                ensemble = self.einfo_barstar,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 231.028, 36.357, 44.937 ),
                translation = ( -0.22273, 1.51380, 0.09214 ),
                ensemble = self.einfo_barstar,
                cell = cell
                ),
            ]

        self.scrambled = [
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 105.4, 179.4, 105.6 ),
                translation = ( -0.001, -0.609, 0.494 ),
                ensemble = self.einfo_barstar,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 39.5, 179.6, 38.4 ),
                translation = ( 0.003, -0.593, 0.500 ),
                ensemble = self.einfo_barnase,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 175.9, 47.6, 4.5 ),
                translation = ( -0.034, 1.298, 0.035 ),
                ensemble = self.einfo_barnase,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 231.0, 36.4, 44.9 ),
                translation = ( -0.223, 1.514, 0.092 ),
                ensemble = self.einfo_barstar,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 231.6, 35.4, 45.8 ),
                translation = ( -0.227, 0.488, 0.084 ),
                ensemble = self.einfo_barnase,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 180.1, 51.4, 0.5 ),
                translation = ( -0.039, 1.288, 0.077 ),
                ensemble = self.einfo_barstar,
                cell = cell
                ),
            ]

        self.shifted_along_c = [
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 105.4, 179.4, 105.6 ),
                translation = ( -0.001, -0.609, 0.994 ),
                ensemble = self.einfo_barstar,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 39.5, 179.6, 38.4 ),
                translation = ( 0.003, -0.593, 0.000 ),
                ensemble = self.einfo_barnase,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 175.9, 47.6, 4.5 ),
                translation = ( -0.034, 1.298, 0.535 ),
                ensemble = self.einfo_barnase,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 231.0, 36.4, 44.9 ),
                translation = ( -0.223, 1.514, 0.592 ),
                ensemble = self.einfo_barstar,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 231.6, 35.4, 45.8 ),
                translation = ( -0.227, 0.488, 0.584 ),
                ensemble = self.einfo_barnase,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 180.1, 51.4, 0.5 ),
                translation = ( -0.039, 1.288, 0.577 ),
                ensemble = self.einfo_barstar,
                cell = cell
                ),
            ]

        self.reversed_shifted_c = [
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 180.0, 51.0, 0.0 ),
                translation = ( -0.038, 0.987, 0.076 ),
                ensemble = self.einfo_barstar,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 231.0, 36.0, 45.0 ),
                translation = ( -0.223, 0.214, 0.092 ),
                ensemble = self.einfo_barstar,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 105.0, 179.0, 105.5 ),
                translation = ( 0.000, -0.909, 0.494 ),
                ensemble = self.einfo_barstar,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 176.0, 48.0, 4.5 ),
                translation = ( -0.034, 0.998, 0.035 ),
                ensemble = self.einfo_barnase,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 39.5, 180.0, 38.0 ),
                translation = ( 0.003, -0.893, 0.500 ),
                ensemble = self.einfo_barnase,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 231.5, 35.0, 46.0 ),
                translation = ( -0.227, 0.188, 0.084 ),
                ensemble = self.einfo_barnase,
                cell = cell
                ),
            ]

        self.reversed_shifted_bc = [
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 180.0, 51.0, 0.0 ),
                translation = ( -0.038, 0.987, 0.576 ),
                ensemble = self.einfo_barstar,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 231.0, 36.0, 45.0 ),
                translation = ( -0.223, 0.214, 0.592 ),
                ensemble = self.einfo_barstar,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 105.0, 179.0, 105.5 ),
                translation = ( 0.000, -0.909, 0.994 ),
                ensemble = self.einfo_barstar,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 176.0, 48.0, 4.5 ),
                translation = ( -0.034, 0.998, 0.535 ),
                ensemble = self.einfo_barnase,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 39.5, 180.0, 38.0 ),
                translation = ( 0.003, -0.893, 0.000 ),
                ensemble = self.einfo_barnase,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 231.5, 35.0, 46.0 ),
                translation = ( -0.227, 0.188, 0.584 ),
                ensemble = self.einfo_barnase,
                cell = cell
                ),
            ]

        self.wrong = [
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 231.647, 35.353, 45.767 ),
                translation = ( -0.22692, 0.18806, 0.08415 ),
                ensemble = self.einfo_barnase,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 39.509, 179.602, 38.411 ),
                translation = ( 0.00327, -0.59286, 0.50032 ),
                ensemble = self.einfo_barnase,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 175.890, 47.606, 4.502 ),
                translation = ( -0.03394, 1.29835, 0.03471 ),
                ensemble = self.einfo_barnase,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 105.393, 179.442, 105.579 ),
                translation = ( -0.00046, -0.60907, 0.49382 ),
                ensemble = self.einfo_barstar,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 231.028, 36.357, 44.937 ),
                translation = ( -0.22273, 1.51380, 0.09214 ),
                ensemble = self.einfo_barstar,
                cell = cell
                ),
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 180.099, 51.375, 0.533 ),
                translation = ( -0.03873, 1.28763, 0.07687 ),
                ensemble = self.einfo_barstar,
                cell = cell
                ),
            ]

        self.ref2 = [
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 231.647, 35.353, 45.767 ),
                translation = ( -0.22692, 0.18806, 0.08415 ),
                ensemble = self.einfo_barnase,
                cell = cell
                ),
            ]
        self.wrong2 = [
            template.PeakInfo.from_phaser_solution(
                rotation = scitbx.math.euler_angles_zyz_matrix( 231.647, 35.353, 45.767 ),
                translation = ( -1.25692, 2.18806, 3.08415 ),
                ensemble = self.einfo_barnase,
                cell = cell
                ),
            ]


    def test_is_symmetry_equivalent(self):

        self.assertFalse(
            template.is_symmetry_equivalent(
                left = self.reference,
                right = self.incomplete,
                crystal = self.cinfo
                )
            )
        self.assertTrue(
            template.is_symmetry_equivalent(
                left = self.reference,
                right = self.scrambled,
                crystal = self.cinfo
                )
            )
        self.assertFalse(
            template.is_symmetry_equivalent(
                left = self.reference,
                right = self.shifted_along_c,
                crystal = self.cinfo
                )
            )
        self.assertTrue(
            template.is_symmetry_equivalent(
                left = self.reference,
                right = self.shifted_along_c,
                crystal = self.cinfo,
                origins = template.plausible_origin_shifts(
                    left = self.reference,
                    right = self.shifted_along_c,
                    crystal = self.cinfo,
                    multiplier = 1.0
                    )
                )
            )
        self.assertTrue(
            template.is_symmetry_equivalent(
                left = self.reference,
                right = self.reversed_shifted_c,
                crystal = self.cinfo,
                origins = template.plausible_origin_shifts(
                    left = self.reference,
                    right = self.reversed_shifted_c,
                    crystal = self.cinfo,
                    multiplier = 1.0
                    ),
                multiplier = 2.0
                )
            )
        self.assertTrue(
            template.is_symmetry_equivalent(
                left = self.reference,
                right = self.reversed_shifted_bc,
                crystal = self.cinfo,
                origins = template.plausible_origin_shifts(
                    left = self.reference,
                    right = self.reversed_shifted_bc,
                    crystal = self.cinfo,
                    multiplier = 1.0
                    ),
                multiplier = 2.0
                )
            )
        self.assertFalse(
            template.is_symmetry_equivalent(
                left = self.reference,
                right = self.wrong,
                crystal = self.cinfo
                )
            )
        self.assertFalse(
            template.is_symmetry_equivalent(
                left = self.ref2,
                right = self.wrong2,
                crystal = self.cinfo,
                origins = template.plausible_origin_shifts(
                    left = self.ref2,
                    right = self.wrong2,
                    crystal = self.cinfo,
                    multiplier = 1.0
                    )
                )
            )


    def test_overlap(self):

        self.assertEqual(
            template.overlap(
                left = self.reference,
                right = self.incomplete,
                crystal = self.cinfo
                ),
            [ ( r, i ) for ( r, i ) in zip( self.reference, self.incomplete ) ]
            )
        self.assertEqual(
            template.overlap(
                left = self.reference,
                right = self.scrambled,
                crystal = self.cinfo
                ),
            [
                ( self.reference[0], self.scrambled[4] ),
                ( self.reference[1], self.scrambled[1] ),
                ( self.reference[2], self.scrambled[2] ),
                ( self.reference[3], self.scrambled[0] ),
                ( self.reference[4], self.scrambled[3] ),
                ( self.reference[5], self.scrambled[5] ),
                ]
            )
        self.assertEqual(
            template.overlap(
                left = self.reference,
                right = self.shifted_along_c,
                crystal = self.cinfo
                ),
            []
            )
        self.assertEqual(
            template.overlap(
                left = self.reference,
                right = self.wrong,
                crystal = self.cinfo
                ),
            [ ( r, i ) for ( r, i ) in zip( self.reference[1:], self.wrong[1:] ) ]
            )

suite_ensemble_info = unittest.TestLoader().loadTestsFromTestCase(
    TestEnsembleInfo
    )
suite_orthogonal_distances = unittest.TestLoader().loadTestsFromTestCase(
    TestOrthogonalDistancesBetween
    )
suite_rotation_differences = unittest.TestLoader().loadTestsFromTestCase(
    TestRotationDifferences
    )
suite_match_tolerance = unittest.TestLoader().loadTestsFromTestCase(
    TestMatchTolerance
    )
suite_symmetry_operators_between = unittest.TestLoader().loadTestsFromTestCase(
    TestSymmetryOperatorsBetween
    )
suite_structure_matching = unittest.TestLoader().loadTestsFromTestCase(
    TestStructureMatching
    )

alltests = unittest.TestSuite(
    [
        suite_ensemble_info,
        suite_orthogonal_distances,
        suite_rotation_differences,
        suite_match_tolerance,
        suite_symmetry_operators_between,
        suite_structure_matching,
        ]
    )


def load_tests(loader, tests, pattern):

    return alltests


if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
