from phaser import ensembler
from phaser import mmt
from phaser import rsam

import ccp4io_adaptbx

from phaser.pipeline.scaffolding import Fake, Append, Return, IterableCompareTestCase

import unittest

class TestModule(unittest.TestCase):

    def setUp(self):

        self.logger = Fake()
        self.logger.debug = Append()
        self.logger.info = Append()
        self.logger.warning = Append()
        self.logger.indent = Append()
        self.logger.dedent = Append()

        self.ensembler_mmt_protein = mmt.PROTEIN
        self.ensembler_rsam_map_to_alignment = rsam.map_to_alignment
        self.ccp4io_adaptbx_ssm = ccp4io_adaptbx.SecondaryStructureMatching


    def tearDown(self):

        mmt.PROTEIN = self.ensembler_mmt_protein
        rsam.map_to_alignment = self.ensembler_rsam_map_to_alignment
        ccp4io_adaptbx.SecondaryStructureMatching = self.ccp4io_adaptbx_ssm


    def get_fake_pdb_objects(self, chain_counts):

        pdbs = []

        for c in chain_counts:
            pdb = Fake()
            pdb.name = Fake()
            pdb.infos = [
                self.get_fake_chain_info_object( pdb ) for i in range( c )
                ]
            pdb.get_chain_infos = Return( values = [ pdb.infos ] )
            pdbs.append( pdb )

        return pdbs


    def get_fake_chain_info_object(
        self,
        pdbobj = None,
        m_index = 0,
        c_index = 0,
        model = object(),
        chain = object(),
        identity = 0.1
        ):

        info = Fake()
        info.c_index = m_index
        info.m_index = c_index
        info.pdb = pdbobj
        info.model = model
        info.chain = chain
        info.identifier = object()
        info.identity = identity
        return info


    def test_select_chains_1(self):

        pdbs = self.get_fake_pdb_objects( chain_counts = [ 2 ] )

        mmt.PROTEIN = Fake()
        mmt.PROTEIN.recognize_chain_type = Return( values = [ False, True ] )

        result = ensembler.find_chains( pdbs = pdbs, logger = self.logger )
        self.assertEqual( result, [ pdbs[0].infos[1] ] )


    def test_map_by_resid_1(self):

        chains = [ Fake(), Fake() ]
        rgs = [
            [ Fake(), Fake(), Fake(), Fake() ],
            [ Fake(), Fake(), Fake(), Fake() ],
            ]
        rgs[0][0].resseq_as_int = Return( values = [ 103 ] )
        rgs[0][0].icode = "B"
        rgs[0][1].resseq_as_int = Return( values = [ 103 ] )
        rgs[0][1].icode = "A"
        rgs[0][2].resseq_as_int = Return( values = [ 104 ] )
        rgs[0][2].icode = ""
        rgs[0][3].resseq_as_int = Return( values = [ 105 ] )
        rgs[0][3].icode = ""

        rgs[1][0].resseq_as_int = Return( values = [ 104 ] )
        rgs[1][0].icode = ""
        rgs[1][1].resseq_as_int = Return( values = [ 103 ] )
        rgs[1][1].icode = "V"
        rgs[1][2].resseq_as_int = Return( values = [ 106 ] )
        rgs[1][2].icode = ""
        rgs[1][3].resseq_as_int = Return( values = [ 103 ] )
        rgs[1][3].icode = "A"

        for ( c, r ) in zip( chains, rgs ):
            c.residue_groups = Return( values = [ r ] )

        infos = [ self.get_fake_chain_info_object( chain = c ) for c in chains ]

        self.assertEqual(
            ensembler.map_by_resid(
                infos = infos,
                alignments = [],
                logger = self.logger
                ),
            [
                [ rgs[0][1], rgs[1][3] ], [ rgs[0][0], None ], [ None, rgs[1][1] ],
                [ rgs[0][2], rgs[1][0] ], [ rgs[0][3], None ], [ None, rgs[1][2] ],
                ]
            )

    def test_map_by_alignments_1(self):

        self.assertRaises(
            RuntimeError,
            ensembler.map_by_alignments,
            infos = object(),
            alignments = [],
            logger = self.logger
            )


    def test_map_by_alignments_2(self):

        alfs = [ Fake(), Fake() ]
        alis = [ Fake(), Fake() ]
        alis[0].alignments = [ "ABCD" ]
        alis[1].alignments = [ "ABCE" ]
        gap = object()

        for ( f, a ) in zip( alfs, alis ):
            f.object = a
            a.gap = gap

        self.assertRaises(
            RuntimeError,
            ensembler.map_by_alignments,
            infos = object(),
            alignments = alfs,
            logger = self.logger
            )


    def test_map_by_alignments_3(self):

        alfs = [ Fake() ]
        ali = Fake()
        ali.alignments = [ "ABCD" ]
        ali.gap = object()
        alfs[0].object = ali
        rsam.map_to_alignment = Return( values = [ ( -1, [] ) ] )
        rgs = object()
        chain = Fake()
        chain.residue_groups = Return( values = [ rgs ] )
        infos = [ self.get_fake_chain_info_object( chain = chain ) ]

        self.assertRaises(
            RuntimeError,
            ensembler.map_by_alignments,
            infos = infos,
            alignments = alfs,
            logger = self.logger
            )


    def test_map_by_alignments_4(self):

        ali = Fake()
        ali.alignments = [ "-ABCD", "ABCEF" ]
        ali.gap = "-"
        alfs = [ Fake() ]
        alfs[0].object = ali
        alfs[0].name = object()
        rgs1 = [ object(), object(), object(), object(), object() ]
        rgs2 = [ object(), object(), object(), object() ]
        chains = [ Fake(), Fake() ]
        chains[0].residue_groups = Return( values = [ rgs1 ] )
        chains[1].residue_groups = Return( values = [ rgs2 ] )
        infos = [ self.get_fake_chain_info_object( chain = c ) for c in chains ]
        rmap1 = [ None, rgs1[1], rgs1[2], rgs1[3], rgs1[4] ]
        rmap2 = [ None, rgs2[0], rgs2[1], rgs2[2], None ]
        rsam.map_to_alignment = Return(
            values = [ ( 1, rmap1 ), ( 0, rmap2 ) ]
            )

        self.assertEqual(
            ensembler.map_by_alignments(
                infos = infos,
                alignments = alfs,
                logger = self.logger
                ),
            [
                ( rgs1[1], rgs2[0] ), ( rgs1[2], rgs2[1] ),
                ( rgs1[3], rgs2[2] ), ( rgs1[4], None ),
                ]
            )


class TestEnsembler(IterableCompareTestCase):

    def test_process(self):

        results = ensembler.process(
            pdbs = self.pdbs,
            alignments = self.alignments,
            params = self.params,
            logger = self.logger
            )
        self.assertEqual( len(results ), self.result_count )
        self.assertEqual(
            [ r.ensembler_input_index for r in results ],
            self.result_input_indices
            )
        self.assertEqual(
            [ r.pdb for r in results ],
            self.result_pdbs
            )
        self.assertEqual(
            [ r.model for r in results ],
            self.result_models
            )
        self.assertEqual(
            [ r.chain for r in results ],
            self.result_chains
            )
        self.assertIterablesAlmostEqual(
            [ r.ensembler_identity for r in results ],
            self.result_identities,
            3
            )
        self.assertIterablesAlmostEqual(
            [ r.ensembler_overlap for r in results ],
            self.result_overlaps,
            3
            )

        for ( r, rot, tra ) in zip( results, self.rotations, self.translations ):
            self.assertIterablesAlmostEqual( r.ensembler_rotation, rot, 3 )
            self.assertIterablesAlmostEqual( r.ensembler_translation, tra, 3 )

        self.assertIterablesAlmostEqual(
            [ r.ensembler_wrmsd for r in results ],
            self.result_wrmsds,
            3
            )
        self.assertIterablesAlmostEqual(
            [ r.ensembler_unwrmsd for r in results ],
            self.result_unwrmsds,
            3
            )
        self.assertEqual(
            [ r.ensembler_cluster for r in results ],
            self.result_clusters
            )
        if hasattr( self, "result_trimming_masks" ):
            self.assertEqual(
                [ "".join( "%d" % i for i in r.ensembler_trimming_mask.as_int() ) for r in results ],
                self.result_trimming_masks
                )


class TestVerotoxin(TestEnsembler):

    def setUp(self):

        import os.path
        import libtbx.load_env
        from phaser import tbx_utils
        from phaser import output

        vtxn = os.path.join(
            libtbx.env.under_dist( "phaser_regression", "data" ),
            "structure",
            "verotoxin.pdb",
            )
        pdb = tbx_utils.PDBObject.from_file( file_name = vtxn )
        self.pdbs = [ pdb ]
        self.alignments = []
        self.params = ensembler.PHIL_MASTER.extract()
        self.logger = output.SingleStream()

        self.result_count = 5
        self.result_input_indices = [ 1, 2, 3, 4, 5 ]
        self.result_pdbs = self.pdbs * 5
        model = pdb.object.models()[0]
        self.result_models = [ model ] * 5
        self.result_chains = model.chains()[:5]
        self.result_identities = [ 1.0 ] * 5
        self.result_overlaps = [ 1.0 ] * 5
        self.rotations = [
            [ 1.000, -0.000, -0.000,  0.000,  1.000, -0.000,  0.000,  0.000,  1.000 ],
            [ 0.668, -0.504, -0.548,  0.742,  0.391,  0.544, -0.060, -0.770,  0.635 ],
            [ 0.097, -0.154, -0.983,  0.697, -0.694,  0.177, -0.710, -0.703,  0.040 ],
            [ 0.083,  0.633, -0.770, -0.074, -0.766, -0.638, -0.994,  0.110, -0.017 ],
            [ 0.625,  0.771, -0.122, -0.534,  0.308, -0.788, -0.570,  0.558,  0.604 ],
            ]
        self.translations = [
            [  -3.8147, -25.0350,  24.3015 ],
            [   2.4605, -18.8599,  42.4862 ],
            [  -0.2391,   2.2440,  42.9023 ],
            [ -14.1830,   6.2743,  27.6215 ],
            [ -16.8636, -11.2747, 17.32989 ],
            ]
        self.result_wrmsds = [ 0.280, 0.379, 0.288, 0.265, 0.260 ]
        self.result_unwrmsds = [ 0.282, 0.382, 0.290, 0.266, 0.262 ]
        self.result_clusters = [ 1 ] * 5


class TestSulphatase(TestEnsembler):

    def setUp(self):

        import os.path
        import libtbx.load_env
        from phaser import tbx_utils
        from phaser import output

        path = os.path.join(
            libtbx.env.under_dist( "phaser_regression", "data" ),
            "structure",
            )
        pdb1 = tbx_utils.PDBObject.from_gzipped_file(
            file_name = os.path.join( path, "1FSU.pdb.gz" )
            )
        pdb2 = tbx_utils.PDBObject.from_gzipped_file(
            file_name = os.path.join( path, "2QZU.pdb.gz" )
            )
        pdb3 = tbx_utils.PDBObject.from_gzipped_file(
            file_name = os.path.join( path, "3B5Q.pdb.gz" )
            )
        pdb4 = tbx_utils.PDBObject.from_gzipped_file(
            file_name = os.path.join( path, "3ED4.pdb.gz" )
            )
        self.pdbs = [ pdb1, pdb2, pdb3, pdb4 ]
        self.alignments = []
        self.params = ensembler.PHIL_MASTER.extract()
        self.params.configuration.trim = True
        self.logger = output.SingleStream()

        self.result_count = 8
        self.result_input_indices = [ 1, 2, 3, 4, 5, 6, 7, 8 ]
        self.result_pdbs = [ pdb1, pdb2, pdb3, pdb3, pdb4, pdb4, pdb4, pdb4 ]
        model1 = pdb1.object.models()[0]
        model2 = pdb2.object.models()[0]
        model3 = pdb3.object.models()[0]
        model4 = pdb4.object.models()[0]
        self.result_models = [ model1, model2, model3, model3 ] + [ model4 ] * 4
        self.result_chains = ( [ model1.chains()[0], model2.chains()[0] ]
            + list( model3.chains() )[:2] + list( model4.chains() )[:4] )
        self.result_identities = [ 0.254, 0.240, 0.339, 0.338, 0.573, 0.569, 0.570, 0.571 ]
        self.result_overlaps = [ 0.911, 0.825, 0.831, 0.812, 0.854, 0.859, 0.850, 0.857 ]
        self.rotations = [
            [  1.000, -0.011, -0.004,  0.011,  1.000,  0.004,  0.004, -0.004,  1.000 ],
            [  0.220, -0.570, -0.792, -0.971, -0.206, -0.122, -0.094,  0.795, -0.599 ],
            [ -0.262, -0.111,  0.959,  0.416, -0.909,  0.009,  0.871,  0.401,  0.285 ],
            [  0.095,  0.210,  0.973, -0.868,  0.496, -0.022, -0.487, -0.843,  0.229 ],
            [  0.335,  0.656, -0.676,  0.494,  0.488,  0.719,  0.802, -0.575, -0.160 ],
            [ -0.965,  0.167, -0.204, -0.125, -0.971, -0.206, -0.232, -0.173,  0.957 ],
            [ -0.269,  0.159,  0.950, -0.152, -0.981,  0.121,  0.951, -0.111,  0.288 ],
            [ -0.690,  0.612, -0.386,  0.719,  0.521, -0.460, -0.081, -0.595, -0.800 ],
            ]
        self.translations = [
            [ -19.0686,  -9.2601, -28.4714 ],
            [  58.0918,  40.0638,  -3.1147 ],
            [ -41.8253, -27.5252, -94.4753 ],
            [ -66.4058,  37.5060,  77.6817 ],
            [  -0.1546, -75.1371,  -2.8617 ],
            [  70.9192,  39.1706,   1.8250 ],
            [  -3.3775,  70.8142, -10.4394 ],
            [  -1.4902, -61.9472,  78.40770 ],
            ]
        self.result_wrmsds = [ 1.542, 1.635, 1.548, 1.549, 1.187, 1.215, 1.238, 1.185 ]
        self.result_unwrmsds = [ 3.448, 3.981, 4.398, 4.237, 3.034, 3.064, 3.056, 3.037 ]
        self.result_clusters = [ 1, 3, 4, 4, 2, 2, 2, 2 ]
        self.result_trimming_masks = [
            ( "0011111111111111111000000011111111111111111000111011111111111111"
                + "111100111000010000101111111111111111111111110000000000111011"
                + "110000000000000000000000000000000000000000000111111111111100"
                + "000001111111111111000000000000110000000011111111111111111111"
                + "111111111111111111010000000000001111111111101111101100100111"
                + "111111111111111111111000111110111100001000000110101000000000"
                + "000000011111011111111000000000000000000000000000011111100001"
                + "110100001111111111111011000000000000000000000000000" ),
            ( "0000000001111111111111111100000000111111111111111110000011111111"
                + "111111111101111110000100000010011111111111111111111111100000"
                + "000000000000000000000011101111000000000000000000000000000011"
                + "111111111110000000011111111111110000000000000110000000000000"
                + "000000000000000001111111111111111111111111111111111111101000"
                + "000011111111111011111001101001111111111111111111110011101111"
                + "011111000010000000111010000000011111101111111000000001111110"
                + "00011100100011111111111110110000000000000" ),
            ( "0111111111111111110000000011111111111111111000001111111111111111"
                + "111101111010000001001111111111111111111111110001111111000000"
                + "000000000000111111111111100000011111111111110000000000000000"
                + "011000000000000000000000000000000000000000000000000111111111"
                + "111111111111111111111111111110100000001101111111111011110110"
                + "100011101111101111111111111111011111011110000100000001101010"
                + "000000001111101111111100000111111000011101000000111111111111"
                + "1011000000000000000000000000000000000000" ),
            ( "0111111111111111110000000011111111111111111000001111111111111111"
                + "111101111010000001001111111111111111111111110001111111000000"
                + "000000000000111111111111100000011111111111100100000000000000"
                + "011000000000000000000000000000000000000000000000000111111111"
                + "111111111111111111111111111110100000001101111111110111110110"
                + "100011101111101111111111111111011111011110000100000001101010"
                + "000000001111101111111100000111111000011100100000111111111111"
                + "1011000000000000000000000000000000000000000" ),
            ( "1111111111111111100000000111111111111111110000011111111111111111"
                + "111101110000100001011111111111111111111111100000000000001110"
                + "111100000000000000000000000000000000000000000111111111111100"
                + "000001111111111111000000000000110000000000000000000000000001"
                + "111111111111111111111111111111111111101000000000000000000000"
                + "111111111110111110011010011111111111111111111111100011111011"
                + "110001000000011010100000000011111011111111000001111110000111"
                + "01000011111111111110110000000000000000000000" ),
            ( "0000001111111111111111100000000111111111111111110000011111111111"
                + "111111111101110000100001011111111111111111111111100000000000"
                + "001110111100000000000000000000000000000000000000000111111111"
                + "111100000001111111111111000000000000110000000000000000000000"
                + "000001111111111111111111111111111111111111101000000000000000"
                + "000000111111111110111110011010011111111111111111111111100011"
                + "111011110001000000011010100000000011111011111111000001111110"
                + "00011101000011111111111110110000000000000000000000" ),
            ( "1111111111111111100000000111111111111111110000011111111111111111"
                + "111101110000100001011111111111111111111111100000000000001110"
                + "111100000000000000000000000000000000000000000111111111111100"
                + "000011111111111110000000000001100000000000000000000000000011"
                + "111111111111111111111111111111111111010000000000000000000001"
                + "111111111101111100111000111111111111111111111111000111110111"
                + "100010000000110101000000000111110111111110000011111100001110"
                + "1000011111111111110110000000000000000000000" ),
            ( "0000001111111111111111100000000111111111111111110000011111111111"
                + "111111111101110000100001011111111111111111111111100000000000"
                + "001110111100000000000000000000000000000000000000000111111111"
                + "111100000001111111111111000000000000110000000000000000000000"
                + "000001111111111111111111111111111111111111101000000000000000"
                + "000000111111111110111110011010011111111111111111111111100011"
                + "111011110001000000011010100000000011111011111111000001111110"
                + "00011101000011111111111110110000000000000000000000" ),
            ]



suite_module = unittest.TestLoader().loadTestsFromTestCase(
    TestModule
    )
suite_verotoxin = unittest.TestLoader().loadTestsFromTestCase(
    TestVerotoxin
    )
suite_sulphatase = unittest.TestLoader().loadTestsFromTestCase(
    TestSulphatase
    )

alltests = unittest.TestSuite(
    [
        suite_module,
        suite_verotoxin,
        suite_sulphatase,
        ]
    )


def load_tests(loader, tests, pattern):

    return alltests


if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
