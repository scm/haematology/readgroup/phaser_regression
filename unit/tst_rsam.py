from phaser.pipeline.scaffolding import Fake, ListFake
from phaser.pipeline.scaffolding import Raise, Return, Append
from phaser.pipeline.scaffolding import IterableCompareTestCase

from phaser import rsam
from phaser import mmt
from phaser import residue_substitution
from iotbx import bioinformatics
from scitbx.array_family import flex

import unittest


class TestModule(unittest.TestCase):

    def test_localize(self):

        self.assertEqual(
            list( rsam.localize( sequence = [], width = 0, padding = None ) ),
            []
            )
        self.assertEqual(
            list( rsam.localize( sequence = [], width = 1, padding = None ) ),
            []
            )
        self.assertEqual(
            list( rsam.localize( sequence = [], width = 2, padding = None ) ),
            []
            )

        seq = [ 1, 2, 3, 4, 5, 6 ]
        self.assertEqual(
            [ list( d ) for d in rsam.localize( sequence = seq, width = 0, padding = None ) ],
            [ [ 1 ], [ 2 ], [ 3 ], [ 4 ], [ 5 ], [ 6 ] ]
            )
        self.assertEqual(
            [ list( d ) for d in rsam.localize( sequence = seq, width = 1, padding = None ) ],
            [ [ None, 1, 2 ], [ 1, 2, 3 ], [ 2, 3, 4 ], [ 3, 4, 5 ],
                [ 4, 5, 6 ], [ 5, 6, None ] ]
            )
        self.assertEqual(
            [ list( d ) for d in rsam.localize( sequence = seq, width = 2, padding = None ) ],
            [ [ None, None, 1, 2, 3 ], [ None, 1, 2, 3 , 4], [ 1, 2, 3, 4, 5 ],
                [ 2, 3, 4, 5, 6 ], [ 3, 4, 5, 6, None ], [ 4, 5, 6, None, None ] ]
            )
        self.assertEqual(
            [ list( d ) for d in rsam.localize( sequence = [ 1, 2, 3 ], width = 2, padding = None ) ],
            [ [ None, None, 1, 2, 3 ], [ None, 1, 2, 3 , None],
                [ 1, 2, 3, None, None ] ]
            )

    def test_split(self):

        splitter = rsam.split(
            sequence = [ 0, 1, 3, -5, -4, -3, 0 ],
            consecutivity = lambda left, right: ( left + 1 ) == right
            )
        self.assertEqual(
            list( splitter ),
            [ [ 0, 1 ], [ 3 ], [ -5, -4, -3 ], [ 0 ] ]
            )


    def test_sequence_stich(self):

        result = rsam.sequence_stich(
            seq_extend = "ABCBA",
            seq_final = "DEABDBAED"
            )
        self.assertEqual( result, "DEABCBAED" )
        result = rsam.sequence_stich(
            seq_extend = "DEABDBAED",
            seq_final = "ABCBA"
            )
        self.assertEqual( result, "DEABDBAED" )


class TestStructuralConsecutivity(unittest.TestCase):

    def setUp(self):

        self.pre = object()
        self.post = object()
        self.distance_cutoff = 1.7
        self.consec = rsam.StructuralConsecutivity(
            pre_bond_atom = self.pre,
            post_bond_atom = self.post,
            distance_cutoff = self.distance_cutoff
            )
        ( self.left, self.right ) = ( object(), object() )


    def test_candidates(self):

        name = object()
        atoms = [ Fake(), Fake(), Fake() ]
        atoms[0].name = name
        atoms[2].name = name
        atoms[1].name = object()

        parents = [ Fake(), Fake() ]
        parents[0].altloc = object()
        parents[1].altloc = object()
        atoms[0].parent = Return( values = [ parents[0] ] )
        atoms[2].parent = Return( values = [ parents[1] ] )

        rg = Fake()
        rg.atoms = Return( values = [ atoms ] )

        self.assertEqual(
            self.consec.candidates( rg = rg, name = name ),
            { parents[0].altloc: atoms[0], parents[1].altloc: atoms[2] }
            )


    def test_call(self):

        self.one_empty( first = object(), second = None )
        self.one_empty( first = None, second = object() )

        self.first_blank( distance = self.distance_cutoff )
        self.first_blank( distance = self.distance_cutoff + 0.01 )

        self.second_blank( distance = self.distance_cutoff )
        self.second_blank( distance = self.distance_cutoff + 0.01 )

        self.no_blank( distance = self.distance_cutoff )
        self.no_blank( distance = self.distance_cutoff + 0.01 )


    def one_empty(self, first, second):

        d = { object(): object() }
        e = {}
        self.consec.candidates = Return( values = [ first, second ] )
        self.assertEqual( self.consec( left = self.left, right = self.right ), False )
        self.assertEqual(
            self.consec.candidates._calls,
            [
                ( (), { "rg": self.left, "name": self.pre } ),
                ( (), { "rg": self.right, "name": self.post } )
                ]
            )


    def first_blank(self, distance):

        atom = Fake()
        atom.distance = Return( values = [ distance ] )
        d1 = { "": atom }
        d2 = { "A": object(), "B": object() }
        self.consec.candidates = Return( values = [ d1, d2 ] )
        self.assertEqual(
            self.consec( left = self.left, right = self.right ),
            distance <= self.distance_cutoff
            )
        self.assertEqual(
            atom.distance._calls,
            [ ( ( d2[ "A" ], ), {} ) ]
            )


    def second_blank(self, distance):

        atom = Fake()
        atom.distance = Return( values = [ distance ] )
        d1 = { "A": atom, "B": object() }
        d2 = { "": object() }
        self.consec.candidates = Return( values = [ d1, d2 ] )
        self.assertEqual(
            self.consec( left = self.left, right = self.right ),
            distance <= self.distance_cutoff
            )
        self.assertEqual(
            atom.distance._calls,
            [ ( ( d2[ "" ], ), {} ) ]
            )


    def no_blank(self, distance):

        atom = Fake()
        atom.distance = Return( values = [ distance ] )
        d1 = { "B": atom, "A": object() }
        d2 = { "C": object(), "B": object() }
        self.consec.candidates = Return( values = [ d1, d2 ] )
        self.assertEqual(
            self.consec( left = self.left, right = self.right ),
            distance <= self.distance_cutoff
            )
        self.assertEqual(
            atom.distance._calls,
            [ ( ( d2[ "B" ], ), {} ) ]
            )


    def no_match(self):

        d1 = { "B": object(), "A": object() }
        d2 = { "C": object() }
        self.consec.candidates = Return( values = [ d1, d2 ] )
        self.assertEqual(
            self.consec( left = self.left, right = self.right ),
            False
            )


class TestMapResiduesToSequence(unittest.TestCase):

    def setUp(self):

        self.sequence = "ABCD--BCDA"
        one_letter = ( "A", "B", "C", "D" )
        three_letter = ( "ALA", "BLA", "CLA", "DLA" )
        three_letter_for = dict( zip( one_letter, three_letter ) )
        self.ags = [ Fake(), Fake(), Fake(), Fake() ]
        self.ags[0].resname = three_letter_for[ "B" ]
        self.ags[1].resname = three_letter_for[ "C" ]
        self.ags[2].resname = three_letter_for[ "D" ]
        self.ags[3].resname = three_letter_for[ "A" ]

        self.rgs = [ Fake(), Fake(), Fake(), Fake() ]
        self.rgs[0].atom_groups = Return( values = [ [ self.ags[0] ] ] * 2 )
        self.rgs[1].atom_groups = Return( values = [ [ self.ags[1] ] ] * 2 )
        self.rgs[2].atom_groups = Return( values = [ [ self.ags[2] ] ] * 2 )
        self.rgs[3].atom_groups = Return( values = [ [ self.ags[3] ] ] * 2 )

        self.one_letter_for = dict( zip( three_letter, one_letter ) )
        self.map_residues_to_sequence = rsam.map_residues_to_sequence


    def tearDown(self):

        rsam.map_residues_to_sequence = self.map_residues_to_sequence


    def test_map_to_1(self):

        mapping = rsam.map_residues_to_sequence(
            sequence = self.sequence,
            residues = self.rgs,
            consecutivity = lambda left, right: True,
            one_letter_for = self.one_letter_for,
            min_length = 2,
            gap_char = "-"
            )
        self.assertEqual(
            mapping,
            [ None, None, None, None, None, None ] + self.rgs
            )


    def test_map_to_2(self):

        mapping = rsam.map_residues_to_sequence(
            sequence = self.sequence,
            residues = self.rgs,
            consecutivity = lambda left, right: False if right == self.rgs[3] else True,
            one_letter_for = self.one_letter_for,
            min_length = 2,
            gap_char = "-"
            )
        self.assertEqual(
            mapping,
            [ None ] + self.rgs[:3] + [ None, None, None, None, None, None ]
            )


    def test_map_to_3(self):

        mapping = rsam.map_residues_to_sequence(
            sequence = self.sequence,
            residues = self.rgs,
            consecutivity = lambda left, right: False if right == self.rgs[3] else True,
            one_letter_for = self.one_letter_for,
            min_length = 0,
            gap_char = "-"
            )
        self.assertEqual(
            mapping,
            [ None ] + self.rgs[:3] + [ None, None, None, None, None, self.rgs[3] ]
            )


    def test_map_to_sequences_1(self):

        mappings = [ object(), object() ]
        rsam.map_residues_to_sequence = Return( values = mappings )
        sequences = [ object(), object() ]
        residues = object()
        consecutivity = object()
        one_letter_for = object()
        min_length = object()
        gap_char = object()
        self.assertEqual(
            rsam.map_to_sequences(
                sequences = sequences,
                residues = residues,
                consecutivity = consecutivity,
                one_letter_for = one_letter_for,
                min_length = min_length,
                gap_char = gap_char
                ),
            mappings
            )

        self.assertEqual(
            rsam.map_residues_to_sequence._calls,
            [
                (
                    (),
                    {
                        "sequence": sequences[0],
                        "residues": residues,
                        "consecutivity": consecutivity,
                        "one_letter_for": one_letter_for,
                        "min_length": min_length,
                        "gap_char": gap_char,
                        },
                    ),
                (
                    (),
                    {
                        "sequence": sequences[1],
                        "residues": residues,
                        "consecutivity": consecutivity,
                        "one_letter_for": one_letter_for,
                        "min_length": min_length,
                        "gap_char": gap_char,
                        },
                    )
                ]
            )


    def test_map_to_sequences_2(self):

        rsam.map_residues_to_sequence = Raise( exception = RuntimeError, message = "" )
        sequences = [ object() ]
        residues = object()
        consecutivity = object()
        one_letter_for = object()
        min_length = object()
        gap_char = object()
        self.assertEqual(
            rsam.map_to_sequences(
                sequences = sequences,
                residues = residues,
                consecutivity = consecutivity,
                one_letter_for = one_letter_for,
                min_length = min_length,
                gap_char = gap_char
                ),
            [ None ]
            )


class TestAlignResiduesToSequence(unittest.TestCase):

    def setUp(self):

        self.rsam_split = rsam.split


    def tearDown(self):

        rsam.split = self.rsam_split


    def test_longer_sequence(self):

        sequence = "ABCDBCDADABCCBADDD"
        one_letter = ( "A", "B", "C", "D" )
        three_letter = ( "ALA", "BLA", "CLA", "DLA" )
        three_letter_for = dict( zip( one_letter, three_letter ) )
        ags1 = [ Fake(), Fake(), Fake(), Fake() ]
        ags1[0].resname = three_letter_for[ "B" ]
        ags1[1].resname = three_letter_for[ "C" ]
        ags1[2].resname = three_letter_for[ "D" ]
        ags1[3].resname = three_letter_for[ "A" ]

        rgs1 = [ Fake(), Fake(), Fake(), Fake() ]
        rgs1[0].atom_groups = lambda: [ ags1[0] ]
        rgs1[1].atom_groups = lambda: [ ags1[1] ]
        rgs1[2].atom_groups = lambda: [ ags1[2] ]
        rgs1[3].atom_groups = lambda: [ ags1[3] ]

        ags2 = [ Fake(), Fake(), Fake(), Fake() ]
        ags2[0].resname = "UNK"
        ags2[1].resname = three_letter_for[ "C" ]
        ags2[2].resname = three_letter_for[ "B" ]
        ags2[3].resname = "UNK"

        rgs2 = [ Fake(), Fake(), Fake(), Fake() ]
        rgs2[0].atom_groups = lambda: [ ags2[0] ]
        rgs2[1].atom_groups = lambda: [ ags2[1] ]
        rgs2[2].atom_groups = lambda: [ ags2[2] ]
        rgs2[3].atom_groups = lambda: [ ags2[3] ]

        rsam.split = Return( values = [ [ rgs1, [ object() ], rgs2 ] ] )
        consecutivity = object()

        result = rsam.align_residues_to_sequence(
            residues = object(),
            sequence = sequence,
            consecutivity = consecutivity,
            one_letter_for = dict( zip( three_letter, one_letter ) ),
            unknown = "X",
            min_hssp_length = 2,
            min_fragment_length = 2,
            min_fraction = 0.5,
            )
        self.assertEqual(
            result,
            [ None ] * 4 + rgs1 + [ None ] * 3 + rgs2 + [ None ] * 3
            )


    def test_shorter_sequence(self):

        sequence = "BCDA"
        one_letter = ( "A", "B", "C", "D" )
        three_letter = ( "ALA", "BLA", "CLA", "DLA" )
        three_letter_for = dict( zip( one_letter, three_letter ) )
        ags1 = [ Fake(), Fake(), Fake(), Fake(), Fake(), Fake(), Fake() ]
        ags1[0].resname = three_letter_for[ "A" ]
        ags1[1].resname = three_letter_for[ "B" ]
        ags1[2].resname = three_letter_for[ "C" ]
        ags1[3].resname = three_letter_for[ "D" ]
        ags1[4].resname = three_letter_for[ "A" ]
        ags1[5].resname = three_letter_for[ "B" ]
        ags1[6].resname = three_letter_for[ "A" ]

        rgs1 = [ Fake(), Fake(), Fake(), Fake(), Fake(), Fake(), Fake() ]
        rgs1[0].atom_groups = lambda: [ ags1[0] ]
        rgs1[1].atom_groups = lambda: [ ags1[1] ]
        rgs1[2].atom_groups = lambda: [ ags1[2] ]
        rgs1[3].atom_groups = lambda: [ ags1[3] ]
        rgs1[4].atom_groups = lambda: [ ags1[4] ]
        rgs1[5].atom_groups = lambda: [ ags1[5] ]
        rgs1[6].atom_groups = lambda: [ ags1[6] ]

        rsam.split = Return( values = [ [ rgs1 ] ] )
        consecutivity = object()

        result = rsam.align_residues_to_sequence(
            residues = object(),
            sequence = sequence,
            consecutivity = consecutivity,
            one_letter_for = dict( zip( three_letter, one_letter ) ),
            unknown = "X",
            min_hssp_length = 2,
            min_fragment_length = 2,
            min_fraction = 0.5,
            )
        self.assertEqual( result, rgs1[1:5] )


class TestNumberingFrame(unittest.TestCase):

    def setUp(self):

        self._rsam_difflib_seq_match = rsam.difflib.SequenceMatcher
        self._rsam_split = rsam.split
        self._rsam_numbframe_to_sequence = rsam.NumberingFrame.to_sequence


    def tearDown(self):

        rsam.difflib.SequenceMatcher = self._rsam_difflib_seq_match
        rsam.split = self._rsam_split
        #rsam.NumberingFrame.to_sequence = self._rsam_numbframe_to_sequence


    def test_empty(self):

        frame = rsam.NumberingFrame(
            residues1 = [],
            residues2 = [ object() ],
            consecutivity = None
            )
        self.assertEqual( frame.shift, 0 )
        self.assertEqual( frame.overlap, 0 )


    def test_shift(self):

        ags1 = [ Fake(), Fake(), Fake(), Fake(), Fake(), Fake(), Fake(), Fake(), Fake() ]
        rgs1 = [ Fake(), Fake(), Fake(), Fake(), Fake(), Fake(), Fake(), Fake(), Fake() ]

        for ( ags, name ) in zip( ags1, "ABCDEFGHI" ):
            ags.resname = name

        for ( rg, ag ) in zip( rgs1, ags1 ):
            rg.atom_groups = Return( values = [ [ ag ] ] )

        rgs1[5].resseq_as_int = Return( values = [ 105 ] )

        ags2 = [ Fake(), Fake(), Fake(), Fake(), Fake(), Fake() ]
        rgs2 = [ Fake(), Fake(), Fake(), Fake(), Fake(), Fake() ]

        for ( ags, name ) in zip( ags2, "FGHIJK" ):
            ags.resname = name

        for ( rg, ag ) in zip( rgs2, ags2 ):
            rg.atom_groups = Return( values = [ [ ag ] ] )

        rgs2[0].resseq_as_int = Return( values = [ 205 ] )

        frame = rsam.NumberingFrame(
            residues1 = rgs1,
            residues2 = rgs2,
            consecutivity = lambda left, right: True
            )

        self.assertEqual( frame.shift, 100 )
        self.assertEqual( frame.overlap, 4 )


class TestVanDerWaalsRadius(unittest.TestCase):

    def setUp(self):

        self.atom_names = [ object(), object() ]
        self.values = [ 1.0, 2.0 ]
        self.mainchain_radius_for = {
            object(): 0.0,
            self.atom_names[0]: self.values[0]
            }
        self.resname = object()
        self.sidechain_data_for = {
            self.resname: {
                self.atom_names[1]: self.values[1],
                object(): 0.0,
                },
            object(): 0.0,
            }
        self.unknown = 3.0


    def test_vdw_radius_mainchain(self):

        atoms = [ Fake() ]
        atoms[0].name = self.atom_names[0]
        root = Fake()
        root.atoms = Return( values = [ atoms ] )
        vdw = rsam.VanDerWaalsRadius(
            root = root,
            radius_for_mainchain_atom = self.mainchain_radius_for,
            sidechain_data_for = self.sidechain_data_for,
            unknown_radius = self.unknown
            )
        self.assertEqual( list( vdw.values ), [ self.values[0] ] )


    def test_vdw_radius_sidechain(self):

        atoms = [ Fake() ]
        atoms[0].name = self.atom_names[1]
        parent = Fake()
        parent.resname = self.resname
        atoms[0].parent = Return( values = [ parent ] )
        root = Fake()
        root.atoms = Return( values = [ atoms ] )
        vdw = rsam.VanDerWaalsRadius(
            root = root,
            radius_for_mainchain_atom = self.mainchain_radius_for,
            sidechain_data_for = self.sidechain_data_for,
            unknown_radius = self.unknown
            )
        self.assertEqual( list( vdw.values ), [ self.values[1] ] )


    def test_vdw_radius_unknown(self):

        atoms = [ Fake() ]
        atoms[0].name = object()
        parent = Fake()
        parent.resname = object()
        atoms[0].parent = Return( values = [ parent ] )
        root = Fake()
        root.atoms = Return( values = [ atoms ] )
        vdw = rsam.VanDerWaalsRadius(
            root = root,
            radius_for_mainchain_atom = self.mainchain_radius_for,
            sidechain_data_for = self.sidechain_data_for,
            unknown_radius = self.unknown
            )
        self.assertEqual( list( vdw.values ), [ self.unknown ] )


class TestAccessibleSurfaceArea(IterableCompareTestCase):

    def create_root(self, atoms, names, coords, parents, altlocs):

        for ( a, n, c, p, aloc ) in zip( atoms, names, coords, parents, altlocs ):
            a.name = n
            a.xyz = c
            p.altloc = aloc
            a.parent = Return( values = [ p ] * 2 )

        atoms.extract_xyz = Return( values = [ coords ] )
        root = Fake()
        root.atoms = Return( values = [ atoms ] * 2 )

        return root


    def test_single_atom_1(self):

        atoms = ListFake( [ Fake() ] )
        root = self.create_root(
            atoms = atoms,
            names = [ " CA " ],
            coords = flex.vec3_double( [ ( 1, 2, 45 ) ] ),
            parents = [ Fake() ],
            altlocs = [ None ]
            )

        vdw_radii = Fake()
        vdw_radii.values = flex.double( [ 1.50 ] )
        asa = rsam.AccessibleSurfaceArea(
            root = root,
            vdw_radii = vdw_radii
            )
        self.assertIterablesAlmostEqual( asa.values, [ 105.683 ], 3 )


    def test_single_atom_2(self):

        atoms = ListFake( [ Fake() ] )
        root = self.create_root(
            atoms = atoms,
            names = [ " CA " ],
            coords = flex.vec3_double( [ ( 1, 2, 45 ) ] ),
            parents = [ Fake() ],
            altlocs = [ "A" ]
            )

        vdw_radii = Fake()
        vdw_radii.values = flex.double( [ 1.50 ] )
        asa = rsam.AccessibleSurfaceArea(
            root = root,
            vdw_radii = vdw_radii
            )
        self.assertIterablesAlmostEqual( asa.values, [ 105.683 ], 3 )


    def test_two_atoms_1(self):

        atoms = ListFake( [ Fake(), Fake() ] )
        root = self.create_root(
            atoms = atoms,
            names = [ " CA ", " CA " ],
            coords = flex.vec3_double( [ ( 1, 2, 45 ), ( 1, 0, 45 ) ] ),
            parents = [ Fake(), Fake() ],
            altlocs = [ None, None ]
            )

        vdw_radii = Fake()
        vdw_radii.values = flex.double( [ 1.50, 1.50 ] )
        asa = rsam.AccessibleSurfaceArea(
            root = root,
            vdw_radii = vdw_radii
            )
        self.assertIterablesAlmostEqual( asa.values, [ 71.116, 71.116 ], 3 )


    def test_two_atoms_2(self):

        atoms = ListFake( [ Fake(), Fake() ] )
        root = self.create_root(
            atoms = atoms,
            names = [ " CA ", " CA " ],
            coords = flex.vec3_double( [ ( 1, 2, 45 ), ( 1, 2, 45 ) ] ),
            parents = [ Fake(), Fake() ],
            altlocs = [ None, None ]
            )

        vdw_radii = Fake()
        vdw_radii.values = flex.double( [ 1.60, 1.50 ] )
        asa = rsam.AccessibleSurfaceArea(
            root = root,
            vdw_radii = vdw_radii
            )
        self.assertIterablesAlmostEqual( asa.values, [ 113.097, 0.0 ], 3 )


    def test_two_atoms_3(self):

        atoms = ListFake( [ Fake(), Fake() ] )
        root = self.create_root(
            atoms = atoms,
            names = [ " CA ", " CA " ],
            coords = flex.vec3_double( [ ( 1, 2, 45 ), ( 1, 2, 45 ) ] ),
            parents = [ Fake(), Fake() ],
            altlocs = [ "A", "B" ]
            )

        vdw_radii = Fake()
        vdw_radii.values = flex.double( [ 1.50, 1.50 ] )
        asa = rsam.AccessibleSurfaceArea(
            root = root,
            vdw_radii = vdw_radii
            )
        self.assertIterablesAlmostEqual( asa.values, [ 105.683, 105.683 ], 3 )


    def test_structure(self):

        import iotbx.pdb
        import libtbx
        import os.path
        pdb_file = os.path.join(
            libtbx.env.under_dist( "phaser_regression", "data" ),
            "structure",
            "1ahq_trunc.pdb",
            )
        root = iotbx.pdb.input( pdb_file ).construct_hierarchy()
        asc = root.atom_selection_cache().selection( "not name h" )
        selected = root.select( asc, True )

        import phaser.mmt
        vdw = rsam.VanDerWaalsRadius.from_mmt(
            root = selected.models()[0].chains()[0],
            mmt = phaser.mmt.PROTEIN
            )
        asa = rsam.AccessibleSurfaceArea(
            root = selected.models()[0].chains()[0],
            vdw_radii = vdw
            )

        self.assertIterablesAlmostEqual(
            asa.values,
            [
                45.834,  36.659,   3.743,  26.927,   5.940,   0.330,   0.110,
                24.569,   7.422,  11.322,   1.635,  39.877,   0.000,   4.954,
                 0.000,   0.000,  39.374,   4.257,   0.000,   0.000,  17.493,
                10.693,  18.869,  44.280,   0.000,   0.991,   0.110,   0.197,
                 8.051,   0.000,   7.925,   6.138,   0.110,   1.321,   4.128,
                27.549,  11.573,  28.807,  41.775,   3.267,   1.761,   0.000,
                 0.000,  16.479,   7.170,  13.166,   8.513,   0.000,   0.000,
                 0.440,   2.752,   6.793,   0.377,   0.000,   1.321,   0.110,
                 0.000,   3.774,  36.606,  23.901,   1.188,   2.092,   0.000,
                 0.197,   9.183,   4.025,   2.312,  29.995,   0.000,  50.821,
                22.014,   0.000,   0.000,   0.000,   0.000,   0.000,   0.000,
                 0.000,   8.428,   0.000,   8.428,   5.661,  45.915,   0.000,
                 1.321,   0.000,   0.000,   6.919,   1.982,  14.642,   7.486,
                22.898,  10.018,  21.027,   0.000,   0.000,   0.881,   0.983,
                 9.686,   0.503,  22.867,   0.000,  10.567,   0.000,   0.000,
                 2.092,   0.000,   0.000,   0.000,   6.667,   0.000,   2.277,
                 5.148,   0.000,   0.000,   0.000,   3.440,   0.000,   3.522,
                14.089,  32.455,   0.000,   3.743,   0.000,  15.036,   1.635,
                21.762,  14.341,   0.000,   0.000,  20.630,  13.083,  38.745,
                 0.000,   1.101,   0.771,  36.067,  15.850,  14.215,  32.078,
                24.027,   1.287,  14.862,   1.431,  22.800,   0.000,   1.541,
                 0.000,  10.417,   9.183,   6.541,   0.000,   5.394,  22.348,
                 0.000,  51.702,  59.249,   0.000,   5.284,   0.000,   4.914,
                 1.006,  13.334,   1.871,  21.382,   0.000,  33.210,  27.046,
                 0.000,   0.000,   0.000,   3.440,   0.000,   0.000,   0.000,
                 0.000,   6.715,   0.000,  12.076,   3.774,   0.000,   0.000,
                 0.000,  13.365,   0.000,   5.787,  14.215,   0.000,   2.312,
                 0.000,   0.000,  27.549,   5.283,  17.234,  38.745,  24.907,
                 0.000,   1.541,   0.000,   0.000,  22.014,   0.000,   0.110,
                 1.101,   0.000,   0.000,   0.000,   6.039,  14.466,   2.673,
                 0.000,   0.000,  18.574,   3.145,   0.000,   0.377,   0.000,
                 1.321,   0.000,   0.000,   0.000,   0.000,  25.788,   3.145,
                 1.584,   0.000,   0.330,  24.962,   8.177,   1.651,   0.881,
                17.504,   0.771,  20.696,  10.458,   0.000,   5.835,   0.661,
                 0.000,  19.121,   0.000,   7.296,   2.642,   0.000,  31.826,
                34.719,  44.405,   4.455,   0.000,   0.000,   3.931,   6.541,
                18.114,   0.268,  50.318,   0.000,   1.871,   0.661,   7.076,
                 7.422,   0.000,   0.000,   0.000,   2.516,  35.726,   4.950,
                 5.945,   1.211,  24.765,  65.916,   0.396,   6.275,   0.000,
                14.348,  31.323,   0.198,  14.844,   0.000,   0.000,   0.000,
                11.989,   9.938,   5.032,  10.592,   0.000,  49.940,  37.613,
                 0.000,   2.312,   0.000,   6.486,   8.051,   0.000,  39.751,
                21.385,   0.000,   0.000,   0.000,   0.000,   0.252,   0.000,
                 0.881,  21.976,  33.856,   0.000,   0.000,   0.000,   0.000,
                 1.635,  14.970,  12.831,   0.000,   0.000,   0.000,   0.000,
                 6.793,  15.976,   7.296,   0.891,   0.000,   0.000,  18.377,
                 4.277,   6.164,   0.000,   0.000,   1.871,   0.000,  26.141,
                 0.000,   4.277,   0.220,   3.861,  31.183,   0.000,   1.321,
                 0.000,   0.000,   4.025,   3.648,   0.000,  10.238,  16.403,
                 0.000,  17.611,  58.746,   5.544,   0.000,   0.881,  26.731,
                 9.183,   9.938,  19.750,   0.000,   4.293,   0.000,   0.000,
                 8.117,  18.384,   1.321,   0.000,   0.099,   0.000,   0.000,
                 1.278,  13.460,  34.845,   9.037,   2.574,   0.000,   1.982,
                16.805,  25.033,   5.661,  30.292,   0.000,  50.821,  34.216,
                 2.475,   2.422,   0.000,   4.619,  13.460,   0.000,   3.523,
                 0.110,   0.786,   9.812,   0.000,  45.034,  16.856,   8.216,
                 3.413,   0.000,   0.000,  31.826,   1.871,  20.586,  14.972,
                20.036,   8.917,   2.972,   9.701,  43.148,   2.574,   0.661,
                 0.000,   0.000,  18.492,  25.285,   3.523,  23.758,  27.619,
                 0.000,   0.550,   0.000,  13.169,   7.548,   8.302,  25.342,
                11.879,   0.000,   0.000,   0.000,  13.562,   3.019,   0.440,
                 0.110,   3.082,  12.440,  16.843,  31.815,   0.000,   2.532,
                 0.000,  25.944,   0.126,   7.925,   7.422,  16.228,   0.000,
                33.587,  41.764,  45.538,   0.198,   6.165,  13.981,  44.617,
                19.876,   1.485,  33.461,
                ],
            3
            )


class TestRGIndexer(unittest.TestCase):

    def setUp(self):

        self.rgs = []

        for i in range( 5 ):
            rgs = Fake()
            rgs.resseq = object()
            rgs.icode = object()
            self.rgs.append( rgs )
            rgs.resseq_as_int = Return( values = [ rgs.resseq ] )

        self.indexer = rsam.RGIndexer( rgs = self.rgs )


    def test_contains(self):

        for rg in self.rgs:
            self.assert_(
                ( rg.resseq, rg.icode ) in self.indexer
                )

        self.assert_(
            ( object(), object() ) not in self.indexer
            )


    def test_getitem(self):

        for ( index, rg ) in enumerate( self.rgs ):
            self.assertEqual(
                self.indexer[ ( rg.resseq, rg.icode ) ],
                ( index, rg )
                )


suite_module = unittest.TestLoader().loadTestsFromTestCase(
    TestModule
    )
suite_structural_consecutivity = unittest.TestLoader().loadTestsFromTestCase(
    TestStructuralConsecutivity
    )
suite_map_residues_to_sequence = unittest.TestLoader().loadTestsFromTestCase(
    TestMapResiduesToSequence
    )
suite_align_residues_to_sequence = unittest.TestLoader().loadTestsFromTestCase(
    TestAlignResiduesToSequence
    )
suite_numbering_frame = unittest.TestLoader().loadTestsFromTestCase(
    TestNumberingFrame
    )
suite_vdw_radius = unittest.TestLoader().loadTestsFromTestCase(
    TestVanDerWaalsRadius
    )
suite_accessible_surface_area = unittest.TestLoader().loadTestsFromTestCase(
    TestAccessibleSurfaceArea
    )
suite_rgi_indexer = unittest.TestLoader().loadTestsFromTestCase(
    TestRGIndexer
    )

alltests = unittest.TestSuite(
    [
        suite_module,
        suite_structural_consecutivity,
        suite_map_residues_to_sequence,
        suite_align_residues_to_sequence,
        suite_numbering_frame,
        suite_vdw_radius,
        suite_accessible_surface_area,
        suite_rgi_indexer,
        ]
    )


def load_tests(loader, tests, pattern):

    return alltests


if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
