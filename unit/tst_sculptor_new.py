from phaser import sculptor_new as sculptor
from phaser import logoutput_new as logoutput
from phaser import mmtype
from phaser import chisellib
from phaser import tbx_utils

from iotbx import bioinformatics
import iotbx.pdb
import libtbx.load_env
from libtbx.utils import null_out

import unittest
import os.path
import sys

PDB_FILE = os.path.join(
    libtbx.env.under_dist( "phaser_regression", "data" ),
    "structure",
    "1ahq_trunc.pdb",
    )
ROOT = iotbx.pdb.input( PDB_FILE ).construct_hierarchy()
SEQUENCE = "GIAVSDDCVQKFNELKLGHQHRYVTFKMNASNTEVVVEHVGGPNATYEDFKS"

NULLOUT = libtbx.utils.null_out()


class TestChainAlignment(unittest.TestCase):

  def setUp(self):

    #from phaser.logoutput_new import console
    #self.handler = console.Handler.FromDefaults( stream = sys.stdout, width = 80 )
    self.logger = logoutput.Writer(
      handlers = [],
      level = tbx_utils.LOGGING_LEVEL_INFO,
      **tbx_utils.LOGGING_LEVELS
      )

    self.alignment = bioinformatics.alignment(
      names = [ "CHAIN", "As", "Xs" ],
      alignments = [ SEQUENCE, "A" * len( SEQUENCE ), "X" * len( SEQUENCE ) ],
      )
    self.trials = [
      sculptor.AlignmentSequence( name = "myalignment", alignment = self.alignment, index = i )
      for i in range( self.alignment.multiplicity() )
      ]
    self.chain = ROOT.models()[0].chains()[0]
    self.params = sculptor.parsed_master_phil().extract().chain_to_alignment_matching


  def test_full(self):

    with self.logger:
      ( strial, resiali, rescodeali ) = sculptor.chain_alignment(
        chain = self.chain,
        mmt = mmtype.PROTEIN,
        trials = self.trials,
        params = self.params,
        logger = self.logger,
        )

    self.assertEqual( strial, self.trials[0] )
    self.assertEqual( resiali, self.chain.residue_groups() )
    self.assertEqual( rescodeali, SEQUENCE )

    copy = self.chain.detached_copy()
    rgs = copy.residue_groups()

    for rg in [ rgs[10], rgs[35] ]:
      copy.remove_residue_group( rg )

    with self.logger:
      ( strial, resiali, rescodeali ) = sculptor.chain_alignment(
        chain = copy,
        mmt = mmtype.PROTEIN,
        trials = self.trials,
        params = self.params,
        logger = self.logger,
        )

    self.assertEqual( strial, self.trials[0] )
    self.assertEqual(
      resiali,
      rgs[ : 10 ] + [ None ] + rgs[ 11 : 35 ] + [ None ] + rgs[ 36 : ],
      )
    self.assertEqual(
      rescodeali,
      SEQUENCE[ : 10 ] + "-" + SEQUENCE[ 11 : 35 ] + "-" + SEQUENCE[ 36 : ],
      )


  def test_mismatch(self):

    copy = self.chain.detached_copy()
    rgs = copy.residue_groups()
    rgs[ 10 ].atom_groups()[0].resname = "ALA"
    rgs[ 35 ].atom_groups()[0].resname = "ALA"
    modseq = SEQUENCE[ : 10 ] + "A" + SEQUENCE[ 11 : 35 ] + "A" + SEQUENCE[ 36 : ]

    with self.logger:
      ( strial, resiali, rescodeali ) = sculptor.chain_alignment(
        chain = copy,
        mmt = mmtype.PROTEIN,
        trials = self.trials,
        params = self.params,
        logger = self.logger,
        )

    self.assertEqual( strial, self.trials[0] )
    self.assertEqual( resiali, rgs )
    self.assertEqual( rescodeali, modseq )

    myali = bioinformatics.alignment( names = [ "MODCHAIN" ], alignments = [ modseq ] )
    mytrial = sculptor.AlignmentSequence( name = "myali", alignment = myali, index = 0 )

    with self.logger:
      ( strial, resiali, rescodeali ) = sculptor.chain_alignment(
        chain = copy,
        mmt = mmtype.PROTEIN,
        trials = self.trials + [ mytrial ],
        params = self.params,
        logger = self.logger,
        )

    self.assertEqual( strial, mytrial )
    self.assertEqual( resiali, rgs )
    self.assertEqual( rescodeali, modseq )

    for rg in [ rgs[25] ]:
      copy.remove_residue_group( rg )

    with self.logger:
      ( strial, resiali, rescodeali ) = sculptor.chain_alignment(
        chain = copy,
        mmt = mmtype.PROTEIN,
        trials = self.trials + [ mytrial ],
        params = self.params,
        logger = self.logger,
        )

    self.assertEqual( strial, mytrial )
    self.assertEqual( resiali, rgs[ : 25 ] + [ None ] + rgs[ 26 : ] )
    self.assertEqual( rescodeali, modseq[ : 25 ] + "-" + modseq[ 26 : ] )


class TestAlignWithSequence(unittest.TestCase):

  def setUp(self):

    #from phaser.logoutput_new import console
    #self.handler = console.Handler.FromDefaults( stream = sys.stdout, width = 80 )
    self.logger = logoutput.Writer(
      handlers = [],
      level = tbx_utils.LOGGING_LEVEL_VERBOSE,
      **tbx_utils.LOGGING_LEVELS
      )

    self.target = bioinformatics.sequence( name = "TARGET", sequence = SEQUENCE )

    from mmtbx import msa
    self.msa_muscle_alignment_ordered = msa.get_muscle_alignment_ordered


  def tearDown(self):

    from mmtbx import msa
    msa.get_muscle_alignment_ordered = self.msa_muscle_alignment_ordered


  def test_simple(self):

    ali = bioinformatics.clustal_alignment(
      names = [ "TARGET", "CHAIN_A" ],
      alignments = [ SEQUENCE, SEQUENCE ],
      )
    from mmtbx import msa
    msa.get_muscle_alignment_ordered = lambda sequences: ali

    with self.logger:
      ( alignment, index ) = sculptor.align_with_sequence(
        chain = ROOT.models()[0].chains()[0],
        mmt = mmtype.PROTEIN,
        target = self.target,
        logger = self.logger,
        )

    self.assertEqual( alignment, ali )
    self.assertEqual( index, 1 )

  def test_mismatches(self):

    chain = ROOT.models()[0].chains()[0].detached_copy()
    rgs = chain.residue_groups()
    chain.remove_residue_group( rgs[21] )
    chain.remove_residue_group( rgs[22] )
    chain.remove_residue_group( rgs[23] )
    rgs[10].atom_groups()[0].resname = "ALA"
    rgs[35].atom_groups()[0].resname = "ALA"

    ali = bioinformatics.clustal_alignment(
      names = [ "TARGET", "CHAIN_A" ],
      alignments = [
        "GIAVSDDCVQKFNELKLGHQHRYVTFKMNASNTEVVVEHVGGPNATYEDFKS",
        "GIAVSDDCVQAFNELKLGHQH---TFKMNASNTEVAVEHVGGPNATYEDFKS",
        ],
      )
    from mmtbx import msa
    msa.get_muscle_alignment_ordered = lambda sequences: ali

    with self.logger:
      ( alignment, index ) = sculptor.align_with_sequence(
        chain = chain,
        mmt = mmtype.PROTEIN,
        target = self.target,
        logger = self.logger,
        )

    self.assertEqual( alignment, ali )
    self.assertEqual( index, 1 )


class TestCreateChainSample(unittest.TestCase):

  def setUp(self):

    #from phaser.logoutput_new import console
    #self.handler = console.Handler.FromDefaults( stream = sys.stdout, width = 80 )
    self.logger = logoutput.Writer(
      handlers = [],
      level = tbx_utils.LOGGING_LEVEL_VERBOSE,
      **tbx_utils.LOGGING_LEVELS
      )

    self.ali =  bioinformatics.alignment(
      names = [ "IDENTICAL", "MISMATCHING" ],
      alignments = [ SEQUENCE, SEQUENCE[ : 15 ] + "A" * 10 + SEQUENCE[ 25 : ] ],
      )
    self.trials = [
      sculptor.AlignmentSequence( name = "myalignment", alignment = self.ali, index = i )
      for i in range( self.ali.multiplicity() )
      ]

    self.name = "1ahq_trunc.pdb, chain A"
    self.chain = ROOT.models()[0].chains()[0]
    mparams = sculptor.parsed_master_phil().extract()
    self.chainalign_params = mparams.chain_to_alignment_matching
    self.error_params = mparams.error_search

    self.mmtype_determine = mmtype.determine
    from mmtbx import msa
    self.msa_muscle_alignment_ordered = msa.get_muscle_alignment_ordered


  def tearDown(self):

    from mmtbx import msa
    msa.get_muscle_alignment_ordered = self.msa_muscle_alignment_ordered
    mmtype.determine = self.mmtype_determine


  def test_empty(self):

    res = sculptor.create_chain_sample(
      name = self.name,
      chain = self.chain,
      alignments = [],
      sequence_for = {},
      errors_for = {},
      chainalign_params = self.chainalign_params,
      error_params = self.error_params,
      logger = self.logger,
      )

    self.assertTrue( isinstance( res, chisellib.Sample ) )
    self.assertEqual( res.name, self.name )
    self.assertEqual( res.chain, self.chain )
    self.assertEqual( res.mmtype, mmtype.PROTEIN )
    self.assertTrue( isinstance( res.chainalignment, chisellib.ChainAlignment ) )
    seq = mmtype.PROTEIN.one_letter_sequence_for_rgs( rgs = self.chain.residue_groups() )
    self.assertEqual( "".join( res.chainalignment.target_sequence() ), seq )
    self.assertEqual( "".join( res.chainalignment.model_sequence() ), seq )
    self.assertEqual( res.chainalignment.residue_sequence(), self.chain.residue_groups() )
    self.assertEqual( "".join( res.chainalignment.rescode_sequence() ), seq )


  def test_unknown(self):

    mmtype.determine = lambda chain, mmtypes, confidence = 0.95: mmtype.UNKNOWN
    res = sculptor.create_chain_sample(
      name = self.name,
      chain = self.chain,
      alignments = [],
      sequence_for = {},
      errors_for = [],
      chainalign_params = self.chainalign_params,
      error_params = self.error_params,
      logger = self.logger,
      )

    self.assertTrue( isinstance( res, chisellib.Sample ) )
    self.assertEqual( res.name, self.name )
    self.assertEqual( res.chain, self.chain )
    self.assertEqual( res.mmtype, mmtype.UNKNOWN )
    self.assertEqual( res.chainalignment, chisellib.NoAlignment )


  def test_alignment(self):

    alignment = bioinformatics.alignment(
      names = [ "CHAIN" ],
      alignments = [ SEQUENCE ],
      )
    trials = [
      sculptor.AlignmentSequence( name = "myalignment", alignment = alignment, index = i )
      for i in range( alignment.multiplicity() )
      ]

    res = sculptor.create_chain_sample(
      name = self.name,
      chain = self.chain,
      alignments = trials,
      sequence_for = {},
      errors_for = [],
      chainalign_params = self.chainalign_params,
      error_params = self.error_params,
      logger = self.logger,
      )

    self.assertTrue( isinstance( res, chisellib.Sample ) )
    self.assertEqual( res.name, self.name )
    self.assertEqual( res.chain, self.chain )
    self.assertEqual( res.mmtype, mmtype.PROTEIN )
    self.assertTrue( isinstance( res.chainalignment, chisellib.ChainAlignment ) )
    self.assertEqual( "".join( res.chainalignment.target_sequence() ), SEQUENCE )
    self.assertEqual( "".join( res.chainalignment.model_sequence() ), SEQUENCE )
    self.assertEqual( res.chainalignment.residue_sequence(), self.chain.residue_groups() )
    self.assertEqual( "".join( res.chainalignment.rescode_sequence() ), SEQUENCE )


  def test_mismatching_alignment(self):

    seq = SEQUENCE[ : 15 ] + "A" * 10 + SEQUENCE[ 25 : ]
    alignment = bioinformatics.alignment(
      names = [ "CHAIN" ],
      alignments = [ seq ],
      )
    trials = [
      sculptor.AlignmentSequence( name = "myalignment", alignment = alignment, index = i )
      for i in range( alignment.multiplicity() )
      ]
    chainseq = mmtype.PROTEIN.one_letter_sequence_for_rgs( rgs = self.chain.residue_groups() )

    res = sculptor.create_chain_sample(
      name = self.name,
      chain = self.chain,
      alignments = trials,
      sequence_for = {},
      errors_for = [],
      chainalign_params = self.chainalign_params,
      error_params = self.error_params,
      logger = self.logger,
      )

    self.assertTrue( isinstance( res, chisellib.Sample ) )
    self.assertEqual( res.name, self.name )
    self.assertEqual( res.chain, self.chain )
    self.assertEqual( res.mmtype, mmtype.PROTEIN )
    self.assertTrue( isinstance( res.chainalignment, chisellib.ChainAlignment ) )
    self.assertEqual( "".join( res.chainalignment.target_sequence() ), seq )
    self.assertEqual( "".join( res.chainalignment.model_sequence() ), seq )
    self.assertEqual( res.chainalignment.residue_sequence(), self.chain.residue_groups() )
    self.assertEqual( "".join( res.chainalignment.rescode_sequence() ), chainseq )

    self.chainalign_params.min_sequence_identity = 0.9
    res = sculptor.create_chain_sample(
      name = self.name,
      chain = self.chain,
      alignments = trials,
      sequence_for = {},
      errors_for = [],
      chainalign_params = self.chainalign_params,
      error_params = self.error_params,
      logger = self.logger,
      )

    self.assertTrue( isinstance( res, chisellib.Sample ) )
    self.assertEqual( res.name, self.name )
    self.assertEqual( res.chain, self.chain )
    self.assertEqual( res.mmtype, mmtype.PROTEIN )
    self.assertTrue( isinstance( res.chainalignment, chisellib.ChainAlignment ) )

    self.assertEqual( "".join( res.chainalignment.target_sequence() ), chainseq )
    self.assertEqual( "".join( res.chainalignment.model_sequence() ), chainseq )
    self.assertEqual( res.chainalignment.residue_sequence(), self.chain.residue_groups() )
    self.assertEqual( "".join( res.chainalignment.rescode_sequence() ), chainseq )


  def test_sequence(self):

    target = bioinformatics.sequence( name = "TARGET", sequence = SEQUENCE )

    ali = bioinformatics.clustal_alignment(
      names = [ "TARGET", "CHAIN_A" ],
      alignments = [
        "GIAVSDDCVQKFNELKLGHQHRYVTFKMNASNTEVVVEHVGGPNATYEDFKS",
        "GIAVSDDCVQKFNELKLGHQHRYVTFKMNASNTEVVVEHVGGPNATYEDFKS",
        ],
      )
    from mmtbx import msa
    msa.get_muscle_alignment_ordered = lambda sequences: ali

    res = sculptor.create_chain_sample(
      name = self.name,
      chain = self.chain,
      alignments = [],
      sequence_for = { "A": target },
      errors_for = [],
      chainalign_params = self.chainalign_params,
      error_params = self.error_params,
      logger = self.logger,
      )

    self.assertTrue( isinstance( res, chisellib.Sample ) )
    self.assertEqual( res.name, self.name )
    self.assertEqual( res.chain, self.chain )
    self.assertEqual( res.mmtype, mmtype.PROTEIN )
    self.assertTrue( isinstance( res.chainalignment, chisellib.ChainAlignment ) )
    self.assertEqual( "".join( res.chainalignment.target_sequence() ), SEQUENCE )
    self.assertEqual( "".join( res.chainalignment.model_sequence() ), SEQUENCE )
    self.assertEqual( res.chainalignment.residue_sequence(), self.chain.residue_groups() )
    self.assertEqual( "".join( res.chainalignment.rescode_sequence() ), SEQUENCE )


suite_chain_alignment = unittest.TestLoader().loadTestsFromTestCase(
  TestChainAlignment,
  )
suite_align_with_sequence = unittest.TestLoader().loadTestsFromTestCase(
  TestAlignWithSequence,
  )
suite_create_chain_sample = unittest.TestLoader().loadTestsFromTestCase(
  TestCreateChainSample,
  )

alltests = unittest.TestSuite(
  [
    suite_chain_alignment,
    suite_align_with_sequence,
    suite_create_chain_sample,
    ]
  )


def load_tests(loader, tests, pattern):

  return alltests


if __name__ == "__main__":
    unittest.TextTestRunner( verbosity = 2 ).run( alltests )
