from phaser_regression.test_data import TOXD
from phaser_regression import test_phaser_keyword as keyword

INPUT = keyword.InputData.log_likelihood_gain_function(
    data = TOXD,
    launcher = __file__,
    sol = TOXD.solution,
    extra_keywords = [ TOXD.resolution ,
                       keyword.MacMR_Set( True, True, True, True, False , False, False)
                     ]
    )

if __name__ == "__main__":
    import sys
    from phaser_regression import test_runner
    test_runner.run_test( input = INPUT, args = sys.argv[1:] )
