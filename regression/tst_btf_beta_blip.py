from phaser_regression.test_data import BETABLIP
from phaser_regression import test_phaser_keyword as keyword

RLIST = """
SOLU SET
SOLU 6DIM ENSE beta EULER  200.807   41.336  183.909 FRAC -0.49538 -0.15798 -0.28099
SOLU TRIAL ENSEMBLE blip EULER  43.7   77.3  116.7 RF 76.1 RFZ 3.0
"""

INPUT = keyword.InputData.brute_translation_function(
    data = BETABLIP,
    launcher = __file__,
    rlist = RLIST,
    extra_keywords = [
        keyword.SgAlternative.Current(),
        keyword.Translate.Around( point = ( 0.9, 0.3, 0.9, ), frac = True, range = 3 )
        ]
    )

if __name__ == "__main__":
    import sys
    from phaser_regression import test_runner
    test_runner.run_test( input = INPUT, args = sys.argv[1:] )
