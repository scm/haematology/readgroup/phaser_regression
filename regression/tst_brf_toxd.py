from phaser_regression.test_data import TOXD
from phaser_regression import test_phaser_keyword as keyword

INPUT = keyword.InputData.brute_rotation_function(
    data = TOXD,
    launcher = __file__,
    search = TOXD.ensembles[0],
    extra_keywords = [ TOXD.resolution ]
    )

if __name__ == "__main__":
    import sys
    from phaser_regression import test_runner
    test_runner.run_test( input = INPUT, args = sys.argv[1:] )
