from phaser_regression.test_data import BARNASE
from phaser_regression import test_phaser_keyword as keyword

INPUT = keyword.InputData.refinement_and_phasing_function(
    data = BARNASE,
    launcher = __file__,
    sol = BARNASE.solution,
    extra_keywords = [ BARNASE.resolution,
         #             keyword.Debug(True),
                       keyword.MacMR_Set( True, True, True, True, False , False, False)
                     ],
    disable_tests_in = {
      "solutions": [ "SOLUTION_ANNOTATIONS" ],
      }
    )

if __name__ == "__main__":
    import sys
    from phaser_regression import test_runner
    test_runner.run_test( input = INPUT, args = sys.argv[1:] )
