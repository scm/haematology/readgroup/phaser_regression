from phaser_regression.test_data import T0283
from phaser_regression import test_phaser_keyword as keyword

INPUT = keyword.InputData.pruning(
    data = T0283,
    launcher = __file__,
    sol = T0283.solution,
    #extra_keywords = [ T0283.resolution ]
    )

if __name__ == "__main__":
    import sys
    from phaser_regression import test_runner
    test_runner.run_test( input = INPUT, args = sys.argv[1:] )
