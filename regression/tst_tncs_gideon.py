from phaser_regression.test_data import TNCS_GIDEON
from phaser_regression import test_phaser_keyword as keyword

INPUT = keyword.InputData.mr_auto_function(
    data = TNCS_GIDEON,
    launcher = __file__,
    searches = [ keyword.Search( ensembles = [ TNCS_GIDEON.ensembles[0] ], count = 2 ) ],
    #extra_keywords = [ T0283.resolution ]
    )

if __name__ == "__main__":
    import sys
    from phaser_regression import test_runner
    test_runner.run_test( input = INPUT, args = sys.argv[1:] )
