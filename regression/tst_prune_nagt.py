from phaser_regression.test_data import NAGT
from phaser_regression import test_phaser_keyword as keyword

INPUT = keyword.InputData.pruning(
    data = NAGT,
    launcher = __file__,
    sol = NAGT.solution,
    extra_keywords = [ NAGT.resolution ]
    )

if __name__ == "__main__":
    import sys
    from phaser_regression import test_runner
    test_runner.run_test( input = INPUT, args = sys.argv[1:] )
