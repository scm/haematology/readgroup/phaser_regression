from phaser_regression.test_data import TOXD_PHASED
from phaser_regression import test_phaser_keyword as keyword
INPUT = keyword.InputData.mr_auto_function(
    data = TOXD_PHASED,
    launcher = __file__,
    searches = [ keyword.Search( ensembles = [ TOXD_PHASED.ensembles[0] ], count = 1 ) ],
    extra_keywords = [
        keyword.TargetTra("PHASED")
    ]
)

if __name__ == "__main__":
    import sys
    from phaser_regression import test_runner
    test_runner.run_test( input = INPUT, args = sys.argv[1:] )
