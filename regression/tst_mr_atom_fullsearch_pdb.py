from phaser_regression.test_data import MRATOM
from phaser_regression import test_phaser_keyword as keyword

INPUT = keyword.InputData.mr_atom_function(
    data = MRATOM,
    launcher = __file__,
    searches = [
        keyword.Search( ensembles = [ MRATOM.ensembles[1] ], count = 2),
        ],
    extra_keywords = [
        keyword.LLGComplete_Ncycle( n = 1 ),
        keyword.LLGComplete_Element( "N" ),
        keyword.SgAlternative.Current(),
        MRATOM.resolution,
        keyword.SearchMethod( "FULL" ),
        ]
    )

if __name__ == "__main__":
    import sys
    from phaser_regression import test_runner
    test_runner.run_test( input = INPUT, args = sys.argv[1:] )
