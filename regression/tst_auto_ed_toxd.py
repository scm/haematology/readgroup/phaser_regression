from phaser_regression.test_data import TOXD
from phaser_regression import test_phaser_keyword as keyword

INPUT = keyword.InputData.mr_auto_function(
    data = TOXD,
    launcher = __file__,
    searches = [ keyword.Search( ensembles = [ TOXD.ensembles[1] ], count = 1 ) ],
    extra_keywords = [ TOXD.resolution,
                       keyword.MacMR_Set( True, True, True, True, False , False, False),
                       keyword.SearchMethod( "FAST" ) ]
    )

if __name__ == "__main__":
    import sys
    from phaser_regression import test_runner
    test_runner.run_test( input = INPUT, args = sys.argv[1:] )
