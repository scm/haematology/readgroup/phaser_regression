from phaser_regression.test_data import TOXD
from phaser_regression import test_phaser_keyword as keyword

INPUT = keyword.InputData.refinement_and_phasing_function(
    data = TOXD,
    launcher = __file__,
    sol = TOXD.solution,
    extra_keywords = [
        TOXD.resolution,
        keyword.MacMR_Name( name = "CUSTOM" ),
        keyword.MacMR_Set( rot = True, tra = True, bfac = False, vrms = True),
        ]
    )

if __name__ == "__main__":
    import sys
    from phaser_regression import test_runner
    test_runner.run_test( input = INPUT, args = sys.argv[1:] )
