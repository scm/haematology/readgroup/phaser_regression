from phaser_regression.test_data import BETABLIP
from phaser_regression import test_phaser_keyword as keyword

INPUT = keyword.InputData.gyre_refinement_function(
    data = BETABLIP,
    launcher = __file__,
    rlist = BETABLIP.rlist,
    extra_keywords = [
        keyword.Resolution( high = 6.59489 ),
        ]
    )

if __name__ == "__main__":
    import sys
    from phaser_regression import test_runner
    test_runner.run_test( input = INPUT, args = sys.argv[1:] )
