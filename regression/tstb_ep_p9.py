from phaser_regression.test_data import P9
from phaser_regression import test_phaser_keyword as keyword

INPUT = keyword.InputData.ep_sad_function(
    data = P9,
    launcher = __file__,
    initials = [ P9.atoms_pdb ],
    extra_keywords = [
        keyword.LLGComplete_Complete( value = True ),
        keyword.LLGComplete_Element( element = "Se" ),
        keyword.LLGComplete_Sigma( sigma = 6 ),
        keyword.AtomBfactorWilson( value = True ),
        P9.scattering,
        ]
    )

if __name__ == "__main__":
    import sys
    from phaser_regression import test_runner
    test_runner.run_test( input = INPUT, args = sys.argv[1:] )
