from phaser_regression.test_data import BETABLIP
from phaser_regression import test_phaser_keyword as keyword

INPUT = keyword.InputData.brute_rotation_function(
    data = BETABLIP,
    launcher = __file__,
    search = BETABLIP.ensembles[0],
    extra_keywords = [
        keyword.Resolution( high = 3.0 ),
        keyword.Rotate.Around( euler = ( 80, 40, 180 ), range = 15 )
        ]
    )

if __name__ == "__main__":
    import sys
    from phaser_regression import test_runner
    test_runner.run_test( input = INPUT, args = sys.argv[1:] )
