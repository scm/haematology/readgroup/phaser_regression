from phaser_regression.test_data import IOD
from phaser_regression import test_phaser_keyword as keyword

INPUT = keyword.InputData.ep_sad_function(
    data = IOD,
    launcher = __file__,
    initials = [ IOD.partial ],
    extra_keywords = [
        IOD.resolution,
        keyword.LLGComplete_Complete( value = True ),
        keyword.LLGComplete_Element( element = "I" )
        ]
    )

if __name__ == "__main__":
    import sys
    from phaser_regression import test_runner
    test_runner.run_test( input = INPUT, args = sys.argv[1:] )
