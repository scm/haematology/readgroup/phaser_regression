#!/bin/bash
for file in ./*.py
do
    if [[ -f $file ]]
    then
      out=$(phenix.python $file -e script -n 12 test)
      err=$(echo "$out" | tail -2 | head -1)
      echo "$file" "$err"
      echo "$file" "$err"  >> update_all.log
      if [ "$err" == "ERROR" ]
      then
        echo -e "$out"  >> update_all.log
        echo "updating..."
        echo "updating..." >> update_all.log
        log=$(phenix.python $file update)
      fi
    fi
done
