from phaser_regression.test_data import SAV
from phaser_regression import test_phaser_keyword as keyword

INPUT = keyword.InputData.ep_sad_function(
    data = SAV,
    launcher = __file__,
    initials = [ SAV.atoms_pdb ],
    extra_keywords = [
        keyword.LLGComplete_Complete( value = True ),
        keyword.LLGComplete_Element( element = "Ca" ),
        keyword.LLGComplete_Element( element = "S" ),
        keyword.LLGComplete_Element( element = "Cl" ),
        keyword.LLGComplete_Sigma( sigma = 6 ),
        keyword.AtomBfactorWilson( value = True ),
        ]
    )

if __name__ == "__main__":
    import sys
    from phaser_regression import test_runner
    test_runner.run_test( input = INPUT, args = sys.argv[1:] )
