from phaser_regression.test_data import IOD
from phaser_regression import test_phaser_keyword as keyword

INPUT = keyword.InputData.fast_rotation_function(
    data = IOD,
    launcher = __file__,
    search = IOD.ensembles[0],
    extra_keywords = [ IOD.resolution ]
    )

if __name__ == "__main__":
    import sys
    from phaser_regression import test_runner
    test_runner.run_test( input = INPUT, args = sys.argv[1:] )
