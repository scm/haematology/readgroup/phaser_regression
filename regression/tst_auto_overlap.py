from phaser_regression.test_data import OVERLAP
from phaser_regression import test_phaser_keyword as keyword

INPUT = keyword.InputData.mr_auto_function(
    data = OVERLAP,
    launcher = __file__,
    searches = [ keyword.Search( ensembles = [ OVERLAP.ensembles[0] ], count = 2 ) ],
    extra_keywords = [
                       keyword.Peak_Rota_Down( 0.0 ),
                     ],
    )

if __name__ == "__main__":
    import sys
    from phaser_regression import test_runner
    test_runner.run_test( input = INPUT, args = sys.argv[1:] )
