from libtbx import test_utils
import libtbx.load_env
import os.path

tst_list = (
  "$D/additional_regressions/ensembler/tst_ensembler.py",
  "$D/additional_regressions/ensembler_new/tst_ensembler_new.py",
  "$D/additional_regressions/extract_alignments/tst_extract_alignment.py",
  "$D/additional_regressions/find_alt_orig_sym_mate/test_find_alt_orig_sym_mate.py",
  #"$D/additional_regressions/gyre_and_gimble/tst_gyre_gimble.py",
  "$D/additional_regressions/mr_model_preparation/tst_mr_model_preparation.py",
  "$D/additional_regressions/MRage/tst_MRage.py",
  "$D/additional_regressions/nma_perturb/tst_nma_perturb.py",
  "$D/additional_regressions/sceds/tst_sceds.py",
  "$D/additional_regressions/proq2_pdb_to_sculptor_error_file/tst_proq2_pdb_to_sculptor_error.py",
  "$D/additional_regressions/sculpt_ensembler/tst_sculpt_ensemble.py",
  "$D/additional_regressions/sculptor/tst_sculptor.py",
  "$D/additional_regressions/sculptor_new/tst_sculptor_new.py",
  "$D/additional_regressions/phenix_interface/test_phenix_interface.py",
  )

def run():

  dist_dir = os.path.join(libtbx.env.under_dist("phaser_regression"))
  build_dir = ""

  test_utils.run_tests(build_dir, dist_dir, tst_list)


if (__name__ == "__main__"):
  run()
